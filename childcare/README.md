# Lovely Childcare

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Install dependency

npm install
bower install

## Build & development

Run `grunt` for building and `grunt serve` for preview.
