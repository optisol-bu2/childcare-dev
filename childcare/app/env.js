(function (window) {
  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = 'http://54.153.43.121:9001';

  // Web url
  window.__env.webUrl = 'http://localhost:9000';

  window.__env.hostName = window.location.origin
  window.__env.port = window.location.port

  
}(this));
