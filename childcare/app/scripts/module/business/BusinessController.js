/**
 * @ngdoc function
 * @name menowApp.business.controller:BusinessCtrl
 * @description
 * # BusinessCtrl
 * Controller of the menowApp business related function
 */
(function() {
    'use strict';
    angular
        .module('menowApp.business')
        .controller('BusinessCtrl', businessCtrl);
    businessCtrl.$inject = ['$scope'];

    function businessCtrl($scope) {
        var self = this;
        self.scope = $scope;
    };
})();