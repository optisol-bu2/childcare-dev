/**
 * @ngdoc overview
 * @name menowApp.business
 *
 * Sub-module for creating and listing business
 */
 (function(){
'use strict';
angular
.module('menowApp.business', []);
})();