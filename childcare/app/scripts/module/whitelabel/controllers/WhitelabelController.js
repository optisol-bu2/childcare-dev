/**
 * @ngdoc function
 * @name menowApp.user.controller:UserCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, for user functions
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('WhitelabelCtrl', WhitelabelCtrl);
    WhitelabelCtrl.$inject = ['$scope', 'Auth', '$state', '$filter', '$cookieStore', 'toastr', '$ngBootbox', 'Upload', 'jobService', '$window', '$rootScope', '$q', '$location', 'whitelabelService'];

    function WhitelabelCtrl($scope, Auth, $state, $filter, $cookieStore, toastr, $ngBootbox, Upload, jobService, $window, $rootScope, $q, $location, whitelabelService) {
        var self = this;
        self.scope = $scope;
        self.scope.addEditPartner = addEditPartner
        self.scope.currentName = $state.current.name;
        self.scope.partner = {};
        self.scope.getPartnerList = getPartnerList;
        self.scope.addEditPartner = addEditPartner;
        self.scope.partnerForm = {};
        self.scope.currentPage = 1;
        self.scope.limit = 3;
        self.scope.roundOff = roundOff;
        self.scope.pageChanged = pageChanged;
        self.scope.deletePartner = deletePartner;
        self.scope.currentName = $state.current.name;
        self.scope.checksubdomain = checksubdomain;
        self.scope.domainExist = false;

        // angular.element('#third_toggle').trigger('click')

        // $cookieStore.put('adminuserMode','admin')
        // $rootScope.$broadcast('adminModeChanged')
        var hostName = $cookieStore.get('user_hostName') ? $cookieStore.get('user_hostName') : window.location.origin;

        // if(self.scope.currentName == 'addPartner'){
        //     addEditPartner()
        getPartnerList(1)
        // }

        if (self.scope.currentName == 'createpartner') {
            self.scope.heading = "add partner"
            self.scope.partner.partnership_with = 1
            self.scope.partner.partnership_childcare_logo = 1;

        }

        if (self.scope.currentName == 'editpartner') {
            self.scope.heading = "edit partner"
            getPartnerDetails($state.params.partnerId)
        }


        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }


        /*
         * it is for add and edit white label partners
         */
        function addEditPartner(obj) {


            // console.log(obj)
            // self.scope.partner = angular.copy(obj);



            console.log(self.scope.partnerForm)
            if (self.scope.partnerForm.$valid && !self.scope.domainExist) {
                // toastr.success('success')
                var data = {};
                // if(partnerId){
                //     data['partnerId'] = partnerId
                // }
                data = self.scope.partner;
                // self.scope.generalLoader = true;
                var upload = Upload.upload({
                    url: __env.apiUrl + '/partnerAddUpdate/',
                    data: data,
                });
                upload.then(function(response) {
                    if (response.data.Status == '200') {
                        self.scope.partner = {};
                        toastr.success(response.data.Message)
                        $rootScope.getwhitelabelDetails(hostName)
                        $state.go('listPartner')

                        // return true;
                    } else {
                        toastr.error(response.Message)
                    }
                });
                // return false;
            } else {
                return false;

            }


        }


        function getPartnerList(pageno) {
            if (!pageno) {
                var pageno = 1;
            }
            var data = {};
            data['pageno'] = pageno;
            data['limit'] = self.scope.limit;
            whitelabelService.getPartnerList(data, function(response) {
                if (response.Status == '200') {
                    self.scope.partnerList = response.data;
                    self.scope.totalCount = response.total;
                }
            });
        }


        self.scope.handleName = handleName;

        // $scope.test = function(){

        // }
        function handleName(files, formFile) {
            console.log(formFile.$error)
            var file = files[0];
            if (formFile.$error.minHeight || formFile.$error.minHeight || formFile.$error.maxHeight || formFile.$error.maxHeight || formFile.$error.maxSize) {
                // self.scope.partner.file = " ";
                self.scope.partner.partner_logo = ""
            }

            if (file) {
                var getExtension = file.name.slice((file.name.lastIndexOf(".") - 1 >>> 0) + 2);
                var fName = 'company_logo' + makeid() + '.' + getExtension
                self.scope.partner.partner_logo = fName;
            }
        }

        //for total limit set in pagination
        //author Udhayakumar.s
        function roundOff(total, limit) {
            return Math.ceil(total / limit)
        }


        // pagechanged to obj pass
        // author Udhayakumar.s
        function pageChanged(pageno, type) {
            self.scope.partner.pageno = pageno;
            getPartnerList(self.scope.partner.pageno);

        }

        //delete the partner
        //author udhayakumar.s
        function deletePartner(id) {

            var opt = {
                templateUrl: 'scripts/module/whitelabel/templates/DeleteConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            var data = {};
                            data['id'] = id;
                            whitelabelService.deletePartner(data, function(response) {
                                if (response.Status == "200") {
                                    toastr.success(response.Message)
                                    getPartnerList(self.scope.partner.pageno)
                                } else {
                                    toastr.error(response.Message)
                                }
                            })
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);




        }

        function getPartnerDetails(partnerId) {
            var data = {}
            data['partnerId'] = partnerId
            whitelabelService.getPartnerDetails(data, function(response) {
                if (response.Status == '200') {
                    self.scope.partner = response.partner
                }
            })
        }


        function checksubdomain(partner) {
            var data = {}
            data['partner'] = partner
            data['hideLoader'] = 1;
            whitelabelService.checksubdomain(data, function(response) {
                if (response.Status == '200') {
                    self.scope.domainExist = true;
                } else {
                    self.scope.domainExist = false;
                }
            })
        }

        self.scope.ckoptions = {
            language: 'en',
            allowedContent: true,
            entities: false,
            extraPlugins: 'justify',
            // toolbar: 'Full',
            toolbar_Full: [
                ['Bold', 'Italic', 'Underline', '-', 'JustifyCenter', 'JustifyLeft']
            ],
            toolbar: 'Full',

            // toolbar_Full :
            // [
            //     { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            //     { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            //     { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            //     { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
            //         'HiddenField' ] },
            //     '/',
            //     { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            //     { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
            //     '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            //     { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            //     { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
            //     '/',
            //     { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            //     { name: 'colors', items : [ 'TextColor','BGColor' ] },
            //     { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
            // ]
        };


    };
})();