/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp.auth')
    .service('whitelabelService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
            addEditPartner: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/partnerAddUpdate/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getPartnerList: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getPartnerList/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },

            deletePartner: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletePartner/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getPartnerDetails: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getPartnerDetails/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            whitelabelDetails: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/whitelabelDetails/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            checksubdomain: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/checksubdomain/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {

                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },

        }

    });