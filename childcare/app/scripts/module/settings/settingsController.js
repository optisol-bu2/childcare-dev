(function() {
    'use strict';
    angular
        .module('menowApp')
        .controller('SettingsCtrl', settingsCtrl);
    settingsCtrl.$inject = ['$scope', '$rootScope', '$ngBootbox', 'jobService', '$cookieStore', 'toastr', '$location', '$state', '$window', 'settingService'];
    // controller for For filtering jobs and joblist
    function settingsCtrl($scope, $rootScope, $ngBootbox, jobService, $cookieStore, toastr, $location, $state, $window, settingService) {
        var self = this;
        self.scope = $scope;

        //funtions
        self.scope.changeSetting = changeSetting;
        self.scope.clientChangeSetting = clientChangeSetting;

        self.scope.currentState = $state.current.name;
        self.scope.adminLogged = $cookieStore.get('adminLogged');
        self.scope.purpose = $cookieStore.get('usertype')
        self.scope.settings = {};

        if (self.scope.currentState == 'settings' || self.scope.currentState == 'clientsettings') {
            getSettings();
        }

        function getSettings() {
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['type'] = self.scope.currentState == 'clientsettings' ? 'client' : 'worker';
            data['hideLoader'] = true;
            settingService.getSettings(data, function(response) {
                if (response.Status == "200") {
                    self.scope.settings = response.setting;
                }
            })
        }

        function changeSetting(obj) {
            console.log(obj)
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            Object.assign(data, obj);
            settingService.changeSettings(data, function(response) {
                if (response.Status == "200") {
                    self.scope.settings = obj;
                    // toastr.success(response.message);
                }
            })
        }

        //it is used for client change settings in mail configuration
        function clientChangeSetting() {
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['quote_email'] = self.scope.setting.quote_email;
            Object.assign(data, self.scope.setting.quote_email);
            settingService.changeSettings(data, function(response) {
                if (response.Status == "200") {
                    getSettings();
                    // toastr.success(response.message);
                }
            });
        }
    };
})();