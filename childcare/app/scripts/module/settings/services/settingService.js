/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp')
    .service('settingService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
        	getSettings:function(data,callback){
        		$http({
                        url: __env.apiUrl + '/getsettings/',
                        method: "POST",
                        data:data,
                        headers: { 'Content-Type': 'application/json' }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
        	},
        	changeSettings:function(data,callback){
        		$http({
                        url: __env.apiUrl + '/changesettings/',
                        method: "POST",
                        data:data,
                        headers: { 'Content-Type': 'application/json' }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
        	}
        };
    });
