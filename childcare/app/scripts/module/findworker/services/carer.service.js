/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp')
    .service('carerService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
            getAllusers: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getallusers/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getprofile: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getpublicprofile/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            adminMessage: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/contactworker/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            togglevetted: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/make_vetted/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            changestatus: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/change_req_status/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            }
        };
    });