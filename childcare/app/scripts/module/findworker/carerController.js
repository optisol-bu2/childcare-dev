(function() {
    'use strict';
    angular
        .module('menowApp')
        .controller('carerCtrl', carerCtrl);
    carerCtrl.$inject = ['$scope', '$rootScope', '$ngBootbox', 'jobService', '$cookieStore', 'toastr', '$location', '$state', '$window', 'settingService', 'carerService', '$q', 'userService'];
    // controller for For filtering jobs and joblist
    function carerCtrl($scope, $rootScope, $ngBootbox, jobService, $cookieStore, toastr, $location, $state, $window, settingService, carerService, $q, userService) {
        var self = this;
        self.scope = $scope;
        //funtions

        self.scope.getallUsers = getallUsers;
        self.scope.selectLocation = selectLocation;
        self.scope.loadPages = loadPages;
        self.scope.gotoProfile = gotoProfile;
        self.scope.contactUser = contactUser;
        self.scope.openSignup = openSignup;
        self.scope.toggleState = toggleState;
        self.scope.toggleJobtype = toggleJobtype;
        self.scope.toggleCaregiver = toggleCaregiver;
        self.scope.toggleRequirements = toggleRequirements;
        self.scope.clearAllfilters = clearAllfilters;
        self.scope.clearfilter = clearfilter;
        self.scope.getLocations = getLocations;
        self.scope.setLocation = setLocation;
        self.scope.contactWorker = contactWorker;
        self.scope.openMessagepopup = openMessagepopup;
        self.scope.clearLocationCall = clearLocationCall;
        self.scope.toggleVetted = toggleVetted;
        self.scope.changeStatus = changeStatus;
        self.scope.goBack = goBack;

        //variables
        self.scope.carers = [];
        self.scope.currentPage = 1;
        self.scope.limit = 10;
        self.scope.currentState = $state.current.name;
        self.scope.filters = {};
        self.scope.pageno = 1;
        self.scope.Days = ['0', '1', '2', '3', '4', '5', '6'];
        self.scope.availableCreds = [];
        self.scope.emp_history = [];
        self.scope.colorCodes = ["#62cb31", "#ffb606", "#9b59b6", "#6b59b6"];
        self.scope.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / Educator', 'Cook'];
        self.scope.selectedJobtype = [];
        self.scope.selectedCaregiver = [];
        self.scope.selectedRequirements = [];
        self.scope.toggle_lable = 'Close filter';
        self.scope.filteredLocations = [];
        self.scope.adminLogged = $cookieStore.get('adminLogged');
        self.scope.adminUrl = $cookieStore.get('adminUrl');
        self.scope.userType = $cookieStore.get('usertype');
        self.scope.carercanCalled = false;
        self.scope.fullAddress = {};
        self.scope.searchDisabled = true;
        self.scope.stickyDisabled = false;
        self.scope.backViewUrl = backViewUrl;
        var userid = $cookieStore.get('userid');
        self.scope.ClientQuotedView = $cookieStore.get('ClientQuotedView')
        self.scope.VettedUser = VettedUser;
        self.scope.BlockUser = BlockUser;

        if ($cookieStore.get('name')) {
            self.scope.logged_in = true;
        }


        if (self.scope.currentState == 'findcarer') {
            getallUsers(1);
            getRequirements();
        }
        if (self.scope.currentState == 'publicprofile' || self.scope.currentState == 'admin.viewworker' || self.scope.currentState == 'viewworker') {
            var userid = $state.params.userid;
            // if(self.scope.currentState == "admin.viewworker"){
            viewprofile(userid)
            getProfilecompletess(userid)
            // }
            getPublicprofile(userid);
            window.scrollTo(0, 0);
            if (self.scope.currentState == "viewworker") {
                self.scope.previousJobid = $cookieStore.get('previousJobid');
            }
        }

        /*
        To get all the carers
        */




        function getallUsers(pageno) {
            var data = {};
            data['hideLoader'] = 1;
            data['userid'] = $cookieStore.get('userid');
            data['pageno'] = pageno;
            data['limit'] = self.scope.limit;
            data['job_type'] = self.scope.selectedJobtype;
            data['caregiver'] = self.scope.selectedCaregiver;
            data['requirement'] = self.scope.selectedRequirements;
            self.scope.isLoading = true;
            self.scope.stickyDisabled = false;
            // assign filters to the request
            if (self.scope.fullAddress.viewval) {
                if (self.scope.fullAddress.hasOwnProperty('postcode'))
                    Object.assign(data, self.scope.fullAddress);
                else {
                    toastr.error('Invalid Search location');
                    // return false;
                }
            }
            carerService.getAllusers(data, function(response) {
                // self.scope.stickyDisabled = true;
                if (response.Status == "200") {
                    self.scope.isLoading = false;
                    self.scope.carers = response.data;
                    self.scope.totalCares = response.total_count;
                }
            });
            setTimeout(function() {
                fixContainerheight();
            }, 500)

        }
        // function toggleState() {
        //     self.scope.state = !self.scope.state;
        //     if (self.scope.state) {
        //
        //         self.scope.toggle_lable = 'Close filter';
        //     } else {
        //         self.scope.toggle_lable = 'Open filter';
        //     }
        // }
        function toggleState() {
            self.scope.state = !self.scope.state;
            if (self.scope.state) {

                self.scope.toggle_lable = 'Close filter';
            } else {
                self.scope.toggle_lable = 'Open filter';
            }
        }


        /*
        To set the height limit of the carer container
        **/

        function fixContainerheight() {
            var containers = angular.element(document.querySelectorAll("#carer_container #panel"));
            if (containers.length > 0) {
                self.scope.maxHeight = containers[0].clientHeight;
                angular.forEach(containers, function(container) {
                    if (self.scope.maxHeight < container.clientHeight) {
                        self.scope.maxHeight = container.clientHeight;
                    }
                });
                angular.forEach(containers, function(container) {
                    angular.element(container).css('height', self.scope.maxHeight + 'px');
                });
            }

            // console.log(containers);
        }

        self.scope.searchlocselected = true;
        //autocomplete city state
        $scope.$on('locationFilter', function(event, arg) {
            self.scope.searchlocselected = false;
            self.scope.searchLocations = arg.filter_result;
        });

        function selectLocation(obj) {
            // hm.joblocation.
            self.scope.filters.city = obj.suburb;
            // hm.joblocation.city = obj.city;
            self.scope.filters.state = obj.state;
            self.scope.filters.post_code = obj.postcode;
            self.scope.searchlocselected = true;
            self.scope.fullAddress = obj.suburb + ", " + obj.state + ", " + obj.postcode;
        }


        /*
        To get the users by lazy load
        **/

        function loadPages() {
            if (self.scope.totalCares) {
                if (self.scope.totalCares / self.scope.limit > self.scope.pageno) {
                    // alert();
                    self.scope.carercanCalled = true;
                    self.scope.pageno += 1;
                    appendUsers(self.scope.pageno);
                }
            }
        }

        /*
        Append users to the existing users array
        **/
        function appendUsers(pageno) {
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['pageno'] = pageno;
            data['limit'] = self.scope.limit;
            data['hideLoader'] = 1;
            data['job_type'] = self.scope.selectedJobtype;
            data['caregiver'] = self.scope.selectedCaregiver;
            data['requirement'] = self.scope.selectedRequirements;
            self.scope.stickyDisabled = false;
            // assign filters to the request
            if (self.scope.fullAddress != "")
                Object.assign(data, self.scope.fullAddress);
            carerService.getAllusers(data, function(response) {
                if (response.Status == "200") {
                    self.scope.carercanCalled = false;
                    self.scope.stickyDisabled = true;
                    // self.scope.carers.push(response.data);
                    angular.forEach(response.data, function(data) {
                        self.scope.carers.push(data);
                    })
                    self.scope.totalCares = response.total_count;
                }
            });
            setTimeout(function() {
                fixContainerheight();
            }, 500)
        }


        /*
        Go to the public user profile
        **/
        function gotoProfile(userObj) {
            var data = {};
            data['userid'] = userObj.id;
            data['name'] = userObj.first_name + '_' + userObj.last_name;
            data['location'] = userObj.city + '_' + userObj.state + '_' + userObj.post_code
            data['address'] = userObj.address;
            data['caregivers'] = userObj.preference.caregivers.join(',');
            data['job_type'] = userObj.preference.job_type.join(',');
            // console.log(data);
            $state.go('publicprofile', data)
        }

        /*
        To get the public profile
        **/
        function getPublicprofile(userid) {
            var data = {};
            data['userid'] = userid;
            carerService.getprofile(data, function(response) {
                if (response.Status == '200') {
                    self.scope.personal = response.personal;
                    self.scope.personal.location = response.personal.city + ", " + response.personal.state + " " + response.personal.post_code;
                    self.scope.preference = response.preferences;
                    self.scope.availability = response.availability;
                    self.scope.requirements = response.requirements;
                    self.scope.emp_history = response.emp_history;
                    self.scope.others = response.other_details;
                    self.scope.emp_emergencycontact = response.emp_emergency;
                    self.scope.blocked_user = response.personal.is_block;
                    self.scope.vetted_user = response.personal.is_vetted;
                    // self.scope.preference.caregivers = self.scope.preference.caregivers.join(' / ');
                    self.scope.preference.children_age = self.scope.preference.children_age.join(', ');
                    angular.forEach(self.scope.requirements, function(response) {
                        // console.log(response);
                        var req = response.requirement_id__category;
                        if (self.scope.availableCreds.indexOf(req) == -1) {
                            self.scope.availableCreds.push(req);
                        }
                    });
                };
            });
        }

        /*
        Contact callback
        **/

        function contactUser(userid) {
            toastr.error('You should login to contact this person');
        }
        /*
        To open signup popup
        **/
        function openSignup() {
            self.scope.$broadcast('callSignup', {
                'postjob': false
            })
        }

        /*
        To get all the requirements
        */
        function getRequirements() {
            jobService.getRequirements(function(response) {
                if (response.Status == "200") {
                    self.scope.requirements = response.reqs;
                }
            });
        }

        /*
        To select the jobtype
        */
        function toggleJobtype(option) {
            // alert();
            var idx = self.scope.selectedJobtype.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedJobtype.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedJobtype.push(option);
            }
            getallUsers(1);
            // console.log(hm.selectedCaregiver);
        };


        /*
        To select the caregivers
        */
        function toggleCaregiver(option) {
            // alert();
            var idx = self.scope.selectedCaregiver.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedCaregiver.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedCaregiver.push(option);
            }
            getallUsers(1);
            // console.log(hm.selectedCaregiver);
        };

        /*
        To select the requirements
        */
        function toggleRequirements(option) {
            // alert();
            var idx = self.scope.selectedRequirements.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedRequirements.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedRequirements.push(option);
            }
            getallUsers(1);
            // console.log(hm.selectedCaregiver);
        };

        /*
        To clear all the filters
        */
        function clearAllfilters() {
            self.scope.selectedJobtype = [];
            self.scope.selectedCaregiver = [];
            self.scope.selectedRequirements = [];
            getallUsers(1);
        }

        /*
        To clear the selected filter
        */

        function clearfilter(option) {
            if (option == 'jobtype') {
                self.scope.selectedJobtype = [];
            } else if (option == 'caregiver') {
                self.scope.selectedCaregiver = [];
            } else if (option == 'requirement') {
                self.scope.selectedRequirements = [];
            }
            getallUsers(1);
        }

        /*
        To filter the locations
        **/
        function getLocations(viewValue) {
            // console.log(viewValue);
            self.scope.locationLoading = true;
            if (viewValue == '') {
                getallUsers(1);
            }
            var data = {};
            var result = [];
            self.scope.filteredLocations = [];
            data['searchKey'] = viewValue;
            // var defered = $q.defer();
            self.scope.updated = false;
            // var promise = [];
            var deferred = $q.defer();
            jobService.getlocation(data, function(response) {
                self.scope.locationLoading = false;
                if (response.data.length > 0) {
                    var filteredLocations = response.data;
                    angular.forEach(response.data, function(data, key) {
                        var viewVal = data.suburb + ", " + data.state + " " + data.postcode;
                        filteredLocations[key].viewval = viewVal;
                    });
                    deferred.resolve(filteredLocations);
                } else {
                    toastr.error('Invalid Search location');
                    self.scope.searchDisabled = true;
                }

            });
            return deferred.promise;

        }


        function setLocation(item) {
            self.scope.fullAddress = item;
            self.scope.searchDisabled = false;
        }

        /*
         To contact Worker
        **/
        function contactWorker(workerId, message) {
            var data = {};
            data['from_user'] = $cookieStore.get('userid');
            data['worker_id'] = workerId;
            data['message'] = message;
            var userid = $cookieStore.get('userid');
            if (userid) {
                $ngBootbox.hideAll();
                self.scope.message = "";
                carerService.adminMessage(data, function(response) {
                    if (response.Status == "200") {
                        toastr.success('Message sent');
                    }
                });
            } else {
                // toastr.warning('Please login/ signup to contact the worker');
                // $rootScope.$broadcast('callLogin',{"message":data});
                $ngBootbox.hideAll();
                self.scope.message = "";
                var options = {
                    templateUrl: 'scripts/module/findworker/templates/contact_prompt.html',
                    scope: $scope,
                    title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                    buttons: {
                        Login: {
                            lable: "Login",
                            className: "btn btn-default loginbtn",
                            callback: function() {
                                $rootScope.$broadcast('callLogin', {
                                    "message": data
                                });
                            }
                        },
                        Signup: {
                            label: "Signup",
                            className: "btn btn-primary signupbtn",
                            callback: function() {
                                $rootScope.$broadcast('callSignup', {
                                    "message": data,
                                    "postjob": false
                                });
                            }
                        }

                    }
                };
                $ngBootbox.customDialog(options);
            }
        }


        /*
         To open Worker messaging
        **/
        function openMessagepopup(workerId) {
            self.scope.workerId = workerId;
            var options = {
                templateUrl: 'scripts/module/findworker/templates/sent_message.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
            };
            $ngBootbox.customDialog(options);
        }


        $scope.$on('sentMessagetoworker', function(evt, arg) {
            arg.message.from_user = $cookieStore.get('userid');
            carerService.adminMessage(arg.message, function(response) {
                if (response.Status == "200") {
                    toastr.success('Message sent');
                    $cookieStore.remove('contactMessage');
                }
            });
        });

        function clearLocationCall() {
            if (self.scope.fullAddress.viewval == '') {
                self.scope.searchDisabled = true;
                getallUsers(1);
            }
        }

        function toggleVetted(userid, is_vetted, obj) {
            var delete_alert = {
                'message': "<div class='col-md-12'><p class='final_step_postjob text-center'>Are your sure you want to mark this carer as vetted?</p></div>",
                "title": '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',

                buttons: {
                    cancel: {
                        lable: "No",
                        className: "btn btn-default loginbtn"
                    },
                    confirm: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                    }
                }
            };
            var data = {
                "userid": userid,
                "is_vetted": is_vetted
            };
            $ngBootbox.confirm(delete_alert)
                .then(function() {
                    carerService.togglevetted(data, function(response) {
                        if (response.Status == "200") {
                            obj.is_vetted = is_vetted;
                            if (is_vetted == 1)
                                toastr.success("Worker Marked as vetted");
                            else
                                toastr.success("Worker is non vetted");
                        }
                    });
                }, function() {

                });
        }


        var indexedReqs = [];

        self.scope.reqsToFilter = function() {
            indexedReqs = [];
            return self.scope.requirements;
        }

        self.scope.filterReqs = function(req) {
            var reqIsNew = indexedReqs.indexOf(req.category) == -1;
            if (reqIsNew) {
                indexedReqs.push(req.category);
            }
            return reqIsNew;
        }
        // console.log(angular.element('.findacarer'));
        // angular.element('.findacarer').bind('scroll', function(){
        //     console.log('scrolling is cool!');
        // });

        function changeStatus(obj, status) {
            toastr.clear()
            var data = {};
            data['id'] = obj.id;
            data['status'] = status;
            data['userid'] = $cookieStore.get('adminid');
            carerService.changestatus(data, function(response) {
                if (response.Status == "200") {
                    obj.status = status;

                    if (response.status == '1') {
                        setTimeout(function() {
                            toastr.error('Credential status changed');
                        }, 500)
                    } else {
                        setTimeout(function() {
                            toastr.success('Credential status changed');
                        }, 500)

                    }
                    var userid = $state.params.userid;
                    viewprofile(userid)
                }
            });
        }

        /*
          To get the user profile
          **/
        function viewprofile(userid) {
            var data = {};
            data['userid'] = userid;
            userService.viewProfile(data, function(response) {
                if (response.Status == "200") {
                    self.scope.personal = response.personal;
                    self.scope.personal.fullAddress = response.personal.city + ", " + response.personal.state + " " + response.personal.post_code;
                    self.scope.preference = response.preferences;
                    self.scope.availability = response.availability;
                    self.scope.requirements = response.requirements;
                    self.scope.emp_history = response.emp_history;
                    self.scope.others = response.other_details;
                    self.scope.resumeFile = response.other_details.resume;
                    self.scope.emp_emergencycontact = response.emp_emergency;
                    self.scope.bankDetails = response.bank_details;
                    // self.scope.preference.caregivers = self.scope.preference.caregivers.join(' / ');
                    self.scope.preference.children_age = self.scope.preference.children_age.join(', ');
                    angular.forEach(self.scope.requirements, function(response) {
                        // console.log(response);
                        var req = response.requirement_id__category;
                        if (self.scope.availableCreds.indexOf(req) == -1) {
                            self.scope.availableCreds.push(req);
                        }
                    });
                }
            });
        }

        function backViewUrl() {
            $cookieStore.put('ClientQuotedView', false);
            window.history.back()
        }

        /*
        To get the completeness percentage of the profile
        **/

        function getProfilecompletess(userid) {
            var data = {};
            data['userid'] = userid;
            data['hideLoader'] = true;
            userService.getCompleteness(data, function(response) {
                if (response.Status == "200") {
                    self.scope.profileCompleteness = response.percentage;
                }
            });
        }

        function goBack() {
            window.history.back();
        }

        function BlockUser(ModelValue, userId) {
            self.scope.is_block = ModelValue
            var opt = {
                templateUrl: 'scripts/module/user/templates/blockConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            // console.log(index);
                            // angular.element("#"+index).attr("checked",'true');
                            // req.has_req = true;
                            angular.element('#block').removeClass('ng-hide')
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            angular.element('#block').addClass('ng-hide')
                            var data = {};
                            data['userid'] = userId;
                            data['is_block'] = ModelValue;
                            userService.blockUser(data, function(response) {
                                toastr.clear()
                                // angular.element('#vetted').removeClass('ng-hide')
                                if (response.Status == "200") {
                                    if (response.is_block == 1) {
                                        toastr.error('The user blocked.');
                                    } else {
                                        toastr.success('The user unblocked.');
                                    }
                                    // obj.is_blocked = response.is_block;
                                    self.scope.blocked_user = response.is_block
                                } else {
                                    toastr.error('error')
                                }
                            });
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);


        }

        /*
         It is for vetted and unvetted Check
        */

        function VettedUser(ModelValue, obj) {
            // console.log($scope.vetted_user)
            self.scope.is_vetted = ModelValue
            var opt = {
                templateUrl: 'scripts/module/user/templates/vettedConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            // if(ModelValue == 1){
                            //    self.scope.vetted_user = 0
                            // }else{
                            //     self.scope.vetted_user = 1
                            // }
                            // console.log(index);
                            // angular.element("#"+index).attr("checked",'true');
                            // req.has_req = true;
                            angular.element('#vetted').removeClass('ng-hide')
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            angular.element('#vetted').addClass('ng-hide')
                            var data = {};
                            data['userid'] = obj.id;
                            data['is_vetted'] = ModelValue
                            userService.vettedUser(data, function(response) {
                                // angular.element('#vetted').removeClass('ng-hide')
                                toastr.clear()
                                if (response.Status == "200") {
                                    if (response.is_vetted == 1) {
                                        toastr.success('The user marked as vetted successfully.');
                                    } else {
                                        toastr.error('The user marked as unvetted successfully.');
                                    }
                                    obj.is_vetted = response.is_vetted;
                                    // var userid = $cookieStore.get('userid');
                                    self.scope.vetted_user = response.is_vetted
                                    // self.scope.is_block = response.data.is_block;
                                } else {
                                    // if(ModelValue == 1){
                                    //    self.scope.vetted_user = 0
                                    // }else{
                                    //     self.scope.vetted_user = 1
                                    // }
                                    toastr.error('error')
                                }
                            });
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);


        }


    }


})();