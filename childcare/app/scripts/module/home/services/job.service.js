/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp.auth')
    .service('jobService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
            insertJobtype: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/insertjobtype/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        // console.log(result);
                        if (result.data.Status == "200") {
                            $cookieStore.put('jobTypeid', result.data.job_type_id);
                        }
                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },

            insertJobloc: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/insertjobaddress/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        // console.log(result);
                        // if(result.data.Status == "200"){
                        //     $cookieStore.put('jobTypeid',result.data.job_type_id);
                        // }
                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            insertJobtime: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/insertjobtime/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            credential_req_insertion: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/credential_req_insertion/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        // console.log(result);
                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            insertJobreq: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/insertjobreq/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getpostalcode: function(callback) {
                $http({
                        url: __env.apiUrl + '/getpostalcode/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getStates: function(callback) {
                $http({
                        url: __env.apiUrl + '/getStates/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getCities: function(state, callback) {
                $http({
                        url: __env.apiUrl + '/getcities/',
                        method: "POST",
                        data: {
                            state: state
                        },
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getpostal: function(city, state, callback) {
                $http({
                        url: __env.apiUrl + '/getpostalbycity/',
                        method: "POST",
                        data: {
                            city: city,
                            state: state
                        },
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getBudget: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/getbudget/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getlocation: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/getlocations/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getjobs: function(data, callback) {
                data['hideLoader'] = 1;
                data['adminLogged'] = ($cookieStore.get('adminLogged')) ? true : false;
                $http({
                        url: __env.apiUrl + '/getjobs/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            submitBid: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/submitbid/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getappliedjobs: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/getappliedjobs/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getquotedJobs: function(data, callback) {
                data['hideLoader'] = 1;
                data['adminLogged'] = ($cookieStore.get('adminLogged')) ? true : false;
                $http({
                        url: __env.apiUrl + '/getparentjoblist/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            viewJob: function(data, callback) {
                data['adminLogged'] = ($cookieStore.get('adminLogged')) ? true : false;
                $http({
                        url: __env.apiUrl + '/viewjob/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        // callback(result);
                    });
            },
            editJob: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/previewjob/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            confirmJob: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/confirmjob/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getMessages: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/getmessages/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            sendMessage: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/sendmessage/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getRequirements: function(callback) {
                $http({
                        url: __env.apiUrl + '/getreq/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            completeJob: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/completejob/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            releasePayment: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/releasepayment/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getFeedback: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getfeedback/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveReview: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/savereview/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            sentReply: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/sentreply/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            sentsubReply: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/sentsubreply/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteMsg: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletemsg/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteReply: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletereply/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteSubreply: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletesubreply/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            makeaJobview: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/makeaview/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getquoters: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/getprivates/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            readMessages: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/readmessages/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getdraftjobs: function(data, callback) {
                data['adminLogged'] = ($cookieStore.get('adminLogged')) ? true : false;
                $http({
                        url: __env.apiUrl + '/getdraftjobs/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            closeJob: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/closejob/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            savePaymentStatus: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/savepaymentstatus/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            jobBlock: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/req_block_job/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getUserQuoteDetails: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getUserQuoteDetails/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            jobStatusCheck: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/jobStatusCheck/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            blockJob: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/req_block_job/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            payment_pending: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/payment_pending/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
        };
    });