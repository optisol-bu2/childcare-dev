/**
 * @ngdoc overview
 * @name menowApp.home
 *
 * Sub-module for home management
 */
 (function(){
'use strict';
angular
.module('menowApp.home', []);
})();