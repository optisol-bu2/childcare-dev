/**
 * @ngdoc function
 * @name menowApp.home.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the menowApp listing search and home management
 */
(function() {
    'use strict';
    angular
        .module('menowApp.home')
        .controller('HomeCtrl', homeCtrl);
    homeCtrl.$inject = ['$scope', '$ngBootbox', 'jobService', '$cookieStore', '$rootScope', '$state', '$window', '$document', '$anchorScroll', '$location', 'toastr'];

    function homeCtrl($scope, $ngBootbox, jobService, $cookieStore, $rootScope, $state, $window, $document, $anchorScroll, $location, toastr) {
        //Initial declaration

        var hm = this;
        var self = this;
        self.scope = $scope;
        hm.jopPopup = jopPopup;
        hm.currentName = $state.current.name;
        hm.tabclick = tabclick;
        hm.jobtype = {};
        hm.jobtype.job_type = 0;
        hm.jobTypesubmit = jobTypesubmit;
        //hm.States = ["Australian Capital Territory", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"];
        hm.activeTab = 'home';
        hm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
        hm.jobtype.selectedCaregiver = ['Qualified childcare professional'];
        hm.careSession = ['Before school care', 'After school care'];
        hm.jobtype.careSession_selection = [];
        hm.jobLocsubmit = jobLocsubmit;
        hm.joblocation = {};
        hm.getRange = getRange;
        hm.childrenAge = ['Up to 12 months', '1-3 years', '4-6 years', '7-11 years', '12+ years'];
        hm.joblocation.age_selection = [];
        hm.jobtime = {};
        hm.selectLocation = selectLocation;
        hm.hstep = 1;
        hm.mstep = 1;
        hm.callloginPopup = callloginPopup;
        hm.is_edit = false;
        hm.mindate = new Date();
        hm.userId = $cookieStore.get('userid');
        self.scope.adminLogged = $cookieStore.get('adminLogged')
        $rootScope.userId = $cookieStore.get('userid');
        $rootScope.adminLogged = $cookieStore.get('adminLogged')
        var now = new Date();
        now.setHours(now.getHours() + 3);
        hm.jobtime.time = now;
        hm.jobtime.days_selection = [];
        hm.jobTimesubmit = jobTimesubmit;
        hm.finalSubmit = finalSubmit;
        $scope.jobType_form = {};
        $scope.jobLocation_form = {};
        $scope.jobTime_form = {};
        $scope.jobSettings_form = {};
        $scope.otherreq = {};
        $scope.jobType_form.error = false;
        $scope.jobLocation_form.error = false;
        $scope.jobTime_form.error = false;
        $scope.jobSettings_form.error = false;
        $scope.otherreq.error = false;
        hm.days_required = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        // End Declaration

        /*
         * It's for get job requirements
         * author S.Udhayakumar
         */

        function getRequirements() {
            // alert()
            jobService.getRequirements(function(response) {
                if (response.Status == "200") {
                    hm.requirements = response.reqs;
                    $scope.postjobtooltip = response.tooltip;
                    if (!hm.is_edit) {
                        hm.jobrequirement.opt_selection = [];
                        angular.forEach(hm.requirements, function(req) {
                            if (req.is_default == 1) {
                                hm.jobrequirement.opt_selection.push(req.id);
                            }
                        });
                    }
                }
            });
        }

        //Declartion
        hm.jobrequirement = {};
        hm.jobrequirement.opt_selection = [];
        hm.befre_after_change = befre_after_change;
        hm.jobtype.careType_seletion = [];
        hm.joblocation.before_after = 0;
        hm.togglecareType = togglecareType;
        hm.getpostal = getpostal;
        hm.getBudget = getBudget;
        hm.states = getStates;
        hm.states = getStates;
        hm.getCities = getCities;
        hm.locselected = false;
        $scope.generalLoader = false;
        hm.getLocations = getLocations;

        hm.jobtype.schoolcaretype = 0;
        hm.locationLoading = false;
        $rootScope.adminLogged = $cookieStore.get('adminLogged');
        $rootScope.adminHeaderchange = false;
        // end declaration


        /*
         * It's for admin header change 
         * author S.Udhayakumar
         */

        if (hm.currentName = 'login') {
            if ($cookieStore.get('adminLogged') && !$cookieStore.get('userid')) {
                $rootScope.adminHeaderchange = true;
            } else {
                $rootScope.adminHeaderchange = false;
            }
        }
        // alert()

        if ($location.path() == '/Maillogin') {
            setTimeout(function() {
                $rootScope.$broadcast('callLogin');
            }, 1000)

        }



        /*
         * It's set variable for job posting
         * author S.Udhayakumar
         */

        function initialiseVariables() {
            hm.jobtype = {};
            hm.jobtype.job_type = 0;
            hm.jobTypesubmit = jobTypesubmit;
            hm.States = ["Australian Capital Territory", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"];
            hm.activeTab = 'home';
            hm.caregivers = [];
            hm.jobtype.selectedCaregiver = ['Qualified childcare professional'];
            hm.careSession = ['Before school care', 'After school care'];
            hm.jobtype.careSession_selection = [];
            var now = new Date();
            // hm.jobtime.time = new Date();
            now.setHours(now.getHours() + 3);
            hm.jobtime = {};
            hm.joblocation = {};
            hm.jobrequirement = {};
            hm.jobtime.date = new Date();
            hm.jobtime.time = now;
            hm.joblocation.age_selection = [];
            // hm.jobtime.hours_required = 4;
            // hm.jobtime.rate_per_hour = 4;
            hm.jobtime.days_selection = [];
            hm.days_required = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            hm.requirements = ['Permanent Residency / Valid Work Visa', 'Certificate II / III / IV in Children Services',
                'Diploma / Bachelors in Children Services', 'First Aid Trained', 'Anaphylaxis Trained', 'Police Clearance / Criminal Records Check',
                'Working with Children Check', 'Proof of work experience', 'Car / Driving', 'Drivers License', 'Independent Verification of the above',
                'Address Verified', 'Identity Verified', 'Verified Mobile Number', 'Reference Check'
            ];
            hm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
            hm.specific_req = ['Home Safety Check', "Public Liability / Children's Insurance"];
            hm.regularcare_req = ['Live In', 'School Pick up / Drop'];
            hm.jobrequirement = {};
            hm.jobrequirement.opt_selection = [];
            hm.jobtype.careType_seletion = [];
            hm.joblocation.before_after = 0;
            hm.jobtype.schoolcaretype = 1;

        }



        /*
         * It's for job post functionality
         * author S.Udhayakumar
         */

        function jopPopup() {

            $ngBootbox.hideAll();
            getRequirements();
            hm.activeTab = 'home';
            var options = {
                templateUrl: 'scripts/module/home/templates/postjob_popup.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                onEscape: function() {
                    hm.activeTab = 'home';
                    $cookieStore.remove('jobTypeid');
                    initialiseVariables();
                    $rootScope.$broadcast('getparentJobs', {
                        incomplete: true
                    });
                }
            };
            $ngBootbox.customDialog(options);
            setTimeout(function() {
                angular.element('body').addClass("modal-open");
            }, 1000);

            //alert($("#logo").scrollTop());
            // $( window ).scrollTop( -100 );
        }
        $scope.$on('callJobpopup', function() {
            jopPopup();
        });


        /*
         * It's for redirect to tab change
         * author S.Udhayakumar
         */

        function tabclick(tab) {
            // console.log(tab);

            hm.activeTab = tab;
            if (tab == 'other_details') {
                angular.element('.bootbox').scrollTo();
            }
        }

        var top = 400;
        var duration = 2000; //milliseconds


        /*
         * It's for job type save 
         * author S.Udhayakumar
         */

        function jobTypesubmit(form, submitFinal) {
            // console.log('job_type',submitFinal)
            form.$setSubmitted();
            var userid = $cookieStore.get('userid');
            if (!hm.jobtype.careType_seletion.length) {
                setTimeout(function() {
                    $scope.jobType_form.error = true;
                })
                tabclick('home')
                return false
            }
            if (hm.jobtype.selectedCaregiver.length == 0) {
                setTimeout(function() {
                    $scope.jobType_form.error = true;
                })
                tabclick('home')
                return false

            }
            if (hm.jobtype.careType_seletion.indexOf('day') > -1 && hm.jobtype.schoolcaretype == 1) {
                if (!hm.jobtype.careSession_selection.length) {
                    setTimeout(function() {
                        $scope.jobType_form.error = true;
                    })
                    tabclick('home')
                    return false;
                }
            }
            if (form.$valid) {
                // if (hm.jobtype.careSession_selection.length > 0) {
                if (!submitFinal) {
                    var data = hm.jobtype;
                    data['hideLoader'] = true;
                    data['job_type_id'] = $cookieStore.get('jobTypeid');
                    data['userid'] = userid;
                    $scope.generalLoader = true;
                    jobService.insertJobtype(data, function(response) {
                        $scope.generalLoader = false;
                        if (response.Status == "200") {
                            // getBudget();
                            hm.PreviewJobId = response.job_type_id
                            tabclick('profile');
                            var elem = angular.element('.modal-dialog');
                            angular.element('.bootbox').scrollTo();
                        }

                    });
                }
                //
            } else {
                tabclick('home')
                return false;
            }
        }


        /*
         * It's for caregiver to store in array
         * author S.Udhayakumar
         */

        hm.toggleCaregiver = function toggleCaregiver(option) {
            // alert();
            var idx = hm.jobtype.selectedCaregiver.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                hm.jobtype.selectedCaregiver.splice(idx, 1);
            }

            // Is newly selected
            else {
                hm.jobtype.selectedCaregiver.push(option);
            }
            // console.log(hm.selectedCaregiver);
        };


        /*
         * It's for caresession to store in array
         * author S.Udhayakumar
         */

        hm.toggleCaresession = function toggleCaresession(option) {
            // alert();
            var idx = hm.jobtype.careSession_selection.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                hm.jobtype.careSession_selection.splice(idx, 1);
            }

            // Is newly selected
            else {
                hm.jobtype.careSession_selection.push(option);
            }
            // console.log(hm.careSession_selection);
        };


        /*
         * It's for job location to save
         * author S.Udhayakumar
         */

        function jobLocsubmit(form, submitFinal) {
            // console.log('jobloc',submitFinal)
            var jobtypeId = $cookieStore.get('jobTypeid');
            form.$setSubmitted();
            if (form.$valid) {
                if (jobtypeId) {
                    if (hm.joblocation.location == 0 && hm.joblocation.before_after == 0) {
                        if (!hm.joblocation.city) {
                            $scope.jobLocation_form.error = true;
                            tabclick('profile')
                            return false;
                        } else {
                            $scope.jobLocation_form.error = false;
                        }
                    }
                    if (!submitFinal) {
                        var data = hm.joblocation;
                        data['job_type_id'] = jobtypeId;
                        data['hideLoader'] = true;
                        // console.log(data);
                        $scope.generalLoader = true;
                        jobService.insertJobloc(data, function(response) {
                            $scope.generalLoader = false;
                            if (response.Status == "200") {
                                $scope.postjobtooltip = response.tooltip;
                                tabclick('messages');
                                angular.element('.bootbox').scrollTo();
                            }
                        });
                    }
                } else {
                    alert('you must fill the job type to continue');
                    tabclick('home');
                    return false;
                }
            } else {
                tabclick('profile')
                return false;
            }
        }



        /*
         * It's for range get
         * author S.Udhayakumar
         */

        function getRange(num) {
            var arr = [];
            for (var i = 1; i <= num; i++) {
                arr.push(i);
            }
            return arr;
        }


        /*
         * It's for age of children to store in array
         * author S.Udhayakumar
         */
        hm.toggleAge = function toggleAge(option) {
            // alert();
            var idx = hm.joblocation.age_selection.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                hm.joblocation.age_selection.splice(idx, 1);
            }

            // Is newly selected
            else {
                hm.joblocation.age_selection.push(option);
            }
            // console.log(hm.selectedCaregiver);
        };



        /*
         * It's for days to store in array
         * author S.Udhayakumar
         */

        hm.toggleDays = function toggleDays(option) {
            if (!hm.jobtime.days_selection) {
                hm.jobtime.days_selection = [];
            }
            // alert();
            var idx = hm.jobtime.days_selection.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                hm.jobtime.days_selection.splice(idx, 1);
            }

            // Is newly selected
            else {
                hm.jobtime.days_selection.push(option);
            }
            // console.log(hm.jobtime.days_selection);
        };

        /*
         * It's for req to store in array 
         * author S.Udhayakumar
         */

        hm.toggleReq = function toggleReq(option) {
            // alert();
            var idx = hm.jobrequirement.opt_selection.indexOf(option);
            // Is currently selected
            if (idx > -1) {
                hm.jobrequirement.opt_selection.splice(idx, 1);
            }
            // Is newly selected
            else {
                hm.jobrequirement.opt_selection.push(option);
            }
        };



        hm.jobtime.date = new Date();


        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [{
            date: tomorrow,
            status: 'full'
        }, {
            date: afterTomorrow,
            status: 'partially'
        }];



        /*
         * get dynamic day class 
         * author S.Udhayakumar
         */
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
        }

        /*
         * It's for job time to save 
         * author S.Udhayakumar
         */
        function jobTimesubmit(form, submitFinal) {
            console.log('jobtime', submitFinal)
            var jobtypeId = $cookieStore.get('jobTypeid');
            form.$setSubmitted();
            if (hm.jobtype.job_type == 1) {
                if (hm.jobtime.days_selection.length == 0) {
                    $scope.jobTime_form.error = true;
                    // alert(1)
                    tabclick('messages')
                    return false;

                }
            }
            if (form.$valid && hm.jobtime.hours_required <= 24 && hm.jobtime.rate_per_hour != '0' && hm.jobtime.hours_required != '0') {
                if (jobtypeId) {
                    // const set_date = hm.jobtime.date
                    if (!submitFinal) {
                        // alert()
                        const data = angular.copy(hm.jobtime);
                        data['date'] = moment(data['date']).format('DD-MM-YYYY');
                        data['hours'] = data['time'].getHours();
                        data['minutes'] = data['time'].getMinutes();
                        data['time'] = moment(data['time']).format('HH:mm');
                        // data['cash_on_hand'] = hm.jobtime.cash_on_hand;
                        data['job_type_id'] = jobtypeId;
                        data['hideLoader'] = true;
                        $scope.generalLoader = true;
                        jobService.insertJobtime(data, function(response) {
                            // hm.jobtime.date = set_date;
                            $scope.generalLoader = false;
                            if (response.Status == "200") {

                                hm.jobtime.budget = response.budget;
                                hm.requirements = response.requirements;
                                // if (!hm.is_edit){
                                //     hm.jobrequirement.opt_selection = [];
                                //     angular.forEach(hm.requirements,function(req){
                                //     if(req.is_default == 1){
                                //         hm.jobrequirement.opt_selection.push(req.id);
                                //     }
                                // });
                                // }

                                tabclick('settings');
                                angular.element('.bootbox').scrollTo();
                            }
                        });
                    }
                } else {
                    alert('you must fill the job type to continue');
                    // alert(2)
                    tabclick('home');
                    return false;
                }

            } else {
                tabclick('messages');
                // alert(hm.activeTab)
                return false;
            }
        }


        /*
         * It's for final submit job post function
         * author S.Udhayakumar
         */
        function finalSubmit(form) {
            form.$setSubmitted();
            if (hm.jobTypesubmit($scope.jobType_form, 'submit') != undefined || hm.jobLocsubmit($scope.jobLocation_form, 'submit') != undefined || hm.jobTimesubmit($scope.jobTime_form, 'submit') != undefined) {
                return false;
            }

            if (form.$valid) {
                var userid = $cookieStore.get('userid');
                var jobtypeId = $cookieStore.get('jobTypeid');
                if (jobtypeId) {
                    var data = hm.jobrequirement;
                    data['job_type_id'] = jobtypeId;
                    data['userid'] = userid;
                    data['hideLoader'] = true;
                    data['partnerid'] = $rootScope.whitelabelDetails.id;
                    $scope.generalLoader = true;
                    jobService.insertJobreq(data, function(response) {
                        $scope.generalLoader = false;
                        if (response.Status == "200") {
                            // hm.jobtype = {};
                            // hm.jobtime = {};
                            // hm.joblocation = {};
                            // hm.jobrequirement = {};
                            initialiseVariables();
                            $ngBootbox.hideAll();
                            if (!userid && !hm.is_edit) {
                                var options = {
                                    templateUrl: 'scripts/module/home/templates/fewstep.html',
                                    scope: $scope,
                                    title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                                    onEscape: function() {
                                        tabclick('home');
                                    },
                                    buttons: {
                                        warning: {
                                            label: "Sign Up",
                                            className: "btn btn-default signupbtn ",
                                            callback: function() {
                                                tabclick('home');
                                                $rootScope.$broadcast('callSignup', {
                                                    'postjob': true
                                                });
                                            }
                                        },
                                        success: {
                                            label: "Login",
                                            className: "btn btn-primary loginbtn ",
                                            callback: function() {
                                                tabclick('home');
                                                console.log('login_popup');
                                                $rootScope.$broadcast('callLogin');
                                            }
                                        }
                                    }
                                };
                                $ngBootbox.customDialog(options);
                            } else if (userid && !hm.is_edit) {
                                // $ngBootbox.alert('Job posted and activated!').then(function() {
                                //     // console.log('Alert closed');
                                //     tabclick('home');
                                // });
                                $cookieStore.remove('jobTypeid');
                                var options = {
                                    templateUrl: 'scripts/module/home/templates/job_success.html',
                                    scope: $scope,
                                    title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                                    onclose: function() {
                                        tabclick('home');
                                        $state.go('parentjoblist', {
                                            tabname: "posted"
                                        });
                                    },
                                    buttons: {
                                        warning: {
                                            label: "Close",
                                            className: "btn btn-default signupbtn pull-right",
                                            callback: function() {
                                                tabclick('home');
                                                if ($state.current.name == "parentjoblist") {
                                                    // window.location.reload();
                                                    $rootScope.$broadcast('getparentJobs', {
                                                        incomplete: false
                                                    });
                                                } else {
                                                    $state.go('parentjoblist', {
                                                        tabname: "posted"
                                                    });
                                                }
                                            }
                                        }
                                    }
                                };
                                $ngBootbox.customDialog(options);
                            } else if (userid && hm.is_edit) {
                                console.log('edit job completed!!');
                                $ngBootbox.hideAll();
                                $cookieStore.remove('jobTypeid');
                                tabclick('home');
                                toastr.success('Job updated!!');
                                hm.is_edit = false;
                                $rootScope.$broadcast('getparentJobs', {
                                    incomplete: false
                                });
                            }

                            // tabclick('home');
                        }
                    });
                } else {
                    alert('you must fill the job type to continue')
                    tabclick('home')
                    // alert();
                }
            }
            // var data = hm.jobrequirement.

        }


        /*
         * It's for req save to db
         * author S.Udhayakumar
         */
        hm.insertCredentialReq = function(form) {
            form.$setSubmitted();
            if (form.$valid) {
                var userid = $cookieStore.get('userid');
                var jobtypeId = $cookieStore.get('jobTypeid');
                if (jobtypeId) {
                    var data = hm.jobrequirement;
                    data['job_type_id'] = jobtypeId;
                    data['userid'] = userid;
                    data['hideLoader'] = true;
                    $scope.generalLoader = true;
                    jobService.credential_req_insertion(data, function(response) {
                        $scope.generalLoader = false;
                        if (response.Status == "200") {
                            tabclick('other_details');
                        }
                    });
                } else {
                    alert('you must fill the job type to continue')
                    tabclick('home')
                    // alert();
                }
            }

        }

        /*
         * It's for reset data
         * author S.Udhayakumar
         */

        function befre_after_change() {
            hm.joblocation.before_after_name = null;
            hm.joblocation.loc_id = null;
            hm.joblocation.address = null;
            hm.joblocation.city = null;
            hm.joblocation.post_code = null;
            hm.joblocation.state = null;
            hm.joblocation.care_name = null;
        }


        /*
         * It's for get post code api
         * author S.Udhayakumar
         */

        function getpostalcode() {
            jobService.getpostalcode(function(response) {
                if (response) {
                    hm.postal_code = response.code;
                    // hm.postal_city = response.city;
                }
            });
        }


        /*
         * It's for get cities function
         * author S.Udhayakumar
         */

        function getCities() {
            //
            // alert(hm.joblocation.state)
            if (hm.joblocation.state) {
                jobService.getCities(hm.joblocation.state, function(response) {
                    if (response) {
                        //console.log(response);
                        // hm.postal_code = response.code;
                        hm.postal_city = response.cities;
                        setTimeout(function() {
                            angular.element("#citydropdown").trigger('chosen:updated');
                        }, 100);
                    }
                });
            }
        }

        /*
         * It's for get states function
         * author S.Udhayakumar
         */
        function getStates() {
            jobService.getStates(function(response) {
                if (response) {
                    hm.states = response.states;
                    //hm.state_id = response.states.id;
                }
            });
        }
        //  getCities();


        /*
         * It's for care type store in array
         * author S.Udhayakumar
         */
        function togglecareType(option) {
            // alert();
            var idx = hm.jobtype.careType_seletion.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                hm.jobtype.careType_seletion.splice(idx, 1);
            }

            // Is newly selected
            else {
                hm.jobtype.careType_seletion.push(option);
            }
            if (hm.jobtype.careType_seletion.indexOf('day') == -1) {
                hm.jobtype.schoolcaretype = 0;
                hm.jobtype.careSession_selection = [];
            }
            // console.log(hm.jobtype.careType_seletion);
        }


        function getpostal() {
            if (hm.joblocation.city && hm.joblocation.state) {
                jobService.getpostal(hm.joblocation.city, hm.joblocation.state, function(response) {
                    if (response.Status == "200") {
                        hm.joblocation.post_code = response.post_code;
                    }
                });
            }

        }


        /*
         * It's for budget calculation function
         * author S.Udhayakumar
         */
        function getBudget() {
            if (hm.jobtime.hours_required && hm.jobtime.rate_per_hour) {
                var data = {};
                data['jobtypeId'] = $cookieStore.get('jobTypeid');
                data['hours_required'] = hm.jobtime.hours_required;
                data['rate_per_hour'] = hm.jobtime.rate_per_hour;
                jobService.getBudget(data, function(response) {
                    if (response.Status == "200") {
                        hm.jobtime.budget = response.estimated_budget;
                        hm.jobtime.x = response.x;
                        hm.jobtime.y = response.y;
                        hm.jobtime.z = response.z;
                        hm.jobtime.total_cost = response.total_cost;

                    }
                });
            }

        }

        // Get postal code, state,city
        function getLocation() {
            jobService.getlocation(function(response) {
                if (response.Status == "200") {
                    hm.locations = response.data;
                }
            });
        }

        $scope.$on('locationFilter', function(event, arg) {
            hm.locselected = false;
            hm.locations = arg.filter_result;
        });


        /*
         * It's for select location to save
         * author S.Udhayakumar
         */
        function selectLocation(obj) {
            // hm.joblocation.
            // console.log(obj);
            hm.joblocation.loc_id = obj.suburb + ", " + obj.state + ", " + obj.postcode;
            hm.joblocation.city = obj.suburb;
            hm.joblocation.state = obj.state;
            hm.joblocation.post_code = obj.postcode;
            hm.locselected = true;
            console.log(hm.joblocation);
        }


        /*
         * calling to login popup
         * author S.Udhayakumar
         */

        function callloginPopup() {
            $rootScope.$broadcast('callLogin');
        }


        /*
         * it is for header scroll top to header
         * author S.Udhayakumar
         */

        function scrollTopHeader(elem) {
            // alert('raja');
            var duration = 4000; //milliseconds

            //Scroll to the exact position

            var offset = 0; //pixels; adjust for floating menu, context etc
            //Scroll to #some-id with 30 px "padding"
            //Note: Use this in a directive, not with document.getElementById
            // var someElement = angular.element(document.getElementById('scroll_top'));
            if (elem.length) {
                $document.scrollToElement(elem, offset, duration);
            }
        };

        /*
         * it is for call edit job functionality
         * author S.Udhayakumar
         */
        $scope.$on('editJobscope', function(event, arg) {
            editJob(arg.Jobid);
        });

        /*
         * it is for edit job function
         * author S.Udhayakumar
         */
        function editJob(jobId, preview) {
            hm.is_edit = true;
            $cookieStore.put('jobTypeid', jobId);
            getRequirements()
            var data = {};
            data['jobid'] = jobId;
            jobService.editJob(data, function(response) {
                if (response.Status == "200") {
                    hm.PreviewJobId = response.jobId;
                    hm.jobtype = response.step1;
                    hm.joblocation = response.step2;
                    hm.jobtime = response.step3;
                    hm.jobtime.date = moment(response.step3.date, 'MM-DD-YYYY').toDate();
                    var today = new Date();
                    today.setHours(response.step3.hour);
                    today.setMinutes(response.step3.minute);
                    hm.jobtime.time = today;
                    // hm.jobrequirement.opt_selection = [];
                    hm.jobrequirement = response.step4;
                    // console.log(hm.jobrequirement.opt_selection.length)
                    // if(hm.jobrequirement.opt_selection.length == 0){
                    //     getRequirements()
                    // }
                    // console.log(hm.jobrequirement);

                }
                // initialiseVariables();
            });


            var options = {
                templateUrl: 'scripts/module/home/templates/postjob_popup.html',
                scope: $scope,
                onEscape: function() {
                    // alert('edit')
                    hm.activeTab = 'home';
                    hm.is_edit = false;
                    $cookieStore.remove('jobTypeid');
                    initialiseVariables();
                    if ($cookieStore.get('parentTab') == 'posted') {
                        $rootScope.$broadcast('getparentJobs', {
                            incomplete: false
                        });
                    } else {
                        $rootScope.$broadcast('getparentJobs', {
                            incomplete: true
                        });
                    }

                },
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
            };

            if (!preview) {
                // alert()
                $ngBootbox.hideAll();
                $ngBootbox.customDialog(options);
            }
        }

        var indexedReqs = [];
        hm.filterReqs = function(req) {
            var reqIsNew = indexedReqs.indexOf(req.category) == -1;
            if (reqIsNew) {
                indexedReqs.push(req.category);
            }
            return reqIsNew;
        }

        hm.reqsToFilter = function() {
            indexedReqs = [];
            return hm.requirements;
        }

        hm.backToJobView = function(PreviewJobId, preview) {
            if (!hm.edit) {
                previewJob(PreviewJobId)
            } else {
                editJob(PreviewJobId, preview)
            }
        }

        function previewJob(jobId, preview) {
            // hm.is_edit = true;
            $cookieStore.put('jobTypeid', jobId);
            // getRequirements()
            var data = {};
            data['jobid'] = jobId;
            jobService.editJob(data, function(response) {
                if (response.Status == "200") {
                    hm.PreviewJobId = response.jobId;
                    hm.jobtype = response.step1;
                    hm.joblocation = response.step2;
                    hm.jobtime = response.step3;
                    hm.jobtime.date = moment(response.step3.date, 'MM-DD-YYYY').toDate();
                    var today = new Date();
                    today.setHours(response.step3.hour);
                    today.setMinutes(response.step3.minute);
                    hm.jobtime.time = today;
                    // hm.jobrequirement.opt_selection = [];
                    // 
                    console.log(hm.jobrequirement.opt_selection.length)
                    // if(hm.jobrequirement.opt_selection.length == 0){
                    //     if (!hm.is_edit){
                    //             hm.jobrequirement.opt_selection = [];
                    //             angular.forEach(hm.requirements,function(req){
                    //                 if(req.is_default == 1){
                    //                     hm.jobrequirement.opt_selection.push(req.id);
                    //                 }
                    //             });
                    //     }
                    // }
                    // console.log(hm.jobrequirement);

                }
                // initialiseVariables();
            });
        }

        // function 

        function getLocations(viewValue) {
            console.log(viewValue);
            if (viewValue == '') {
                getJoblist(1);
            }
            var data = {};
            var result = [];
            hm.filteredLocations = [];
            data['searchKey'] = viewValue;
            jobService.getlocation(data, function(response) {
                hm.filteredLocations = response.data;
                angular.forEach(response.data, function(data, key) {
                    var viewVal = data.suburb + ", " + data.state + " " + data.postcode;
                    hm.filteredLocations[key].viewval = viewVal;
                });
            });
            return hm.filteredLocations;
        }
        hm.filteredLocations = [];


        // Editor options.
        $scope.ckoptions = {
            language: 'en',
            allowedContent: true,
            entities: false,
            extraPlugins: 'justify',
            // toolbar: 'Full',
            toolbar_Full: [
                ['Bold', 'Italic', 'Underline', '-', 'JustifyCenter', 'JustifyLeft']
            ],
            toolbar: 'Full',
        };

    };
})();