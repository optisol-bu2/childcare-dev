/**
 * @ngdoc function
 * @name menowApp.job.controller:job
 * @description
 * # job
 * Controller of the menowApp listing search and job management
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('JobCtrl', jobCtrl);
    jobCtrl.$inject = ['$scope', '$http', '$rootScope', '$ngBootbox', 'jobService', '$cookieStore', 'toastr', '$location', '$state', '$window', '$interval','userService','paypal','Suggestion','$q','$anchorScroll','$filter'];
    // controller for For filtering jobs and joblist
    function jobCtrl($scope, $http, $rootScope, $ngBootbox, jobService, $cookieStore, toastr, $location, $state, $window, $interval, userService,paypal,Suggestion,$q, $anchorScroll,$filter) {
        var self = this;
        // getPaypalToken();
        self.scope = $scope;
        self.scope.strtoarr = strtoarr;
        self.scope.loadPages = loadPages;
        self.scope.pageno = 1;
        self.scope.limit = 5;
        self.scope.currentTabpreview = 'jobdetails';
        self.scope.switchTab = switchTab;
        self.scope.job_type = "0";
        self.scope.readOnly = true;
        self.scope.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / Educator', 'Cook'];
        // self.scope.requirements = ['Permanent Residency / Valid Work Visa', 'Certificate II / III / IV in Children Services',
        //     'Diploma / Bachelors in Children Services', 'First Aid Trained', 'Anaphylaxis Trained', 'Police Clearance / Criminal Records Check',
        //     'Working with Children Check', 'Proof of work experience', 'Car / Driving', 'Drivers License', 'Independent Verification of the above',
        //     'Address Verified', 'Identity Verified', 'Verified Mobile Number', 'Reference Check'
        // ];
        self.scope.selectedCaregiver = [];
        self.scope.toggleCaregiver = toggleCaregiver;
        self.scope.toggleCaretype = toggleCaretype;
        self.scope.selectedJobtype = [];
        self.scope.selectedSession = [];
        self.scope.selectedCaretime = [];
        self.scope.togglereq = togglereq;
        self.scope.selectedreq = [];
        self.scope.toggleCaretime = toggleCaretime;
        self.scope.toggleSession = toggleSession;
        self.scope.job_status = 0;
        self.scope.address = {};
        self.scope.searchbyLocation = searchbyLocation;
        self.scope.searchbyStatus = searchbyStatus;
        self.scope.selectLocation = selectLocation;
        self.scope.submitBid = submitBid;
        self.scope.getdayFromdate = getdayFromdate;
        self.scope.sentMessage = sentMessage;
        self.scope.isLoading = true;
        self.scope.state = true;
        self.scope.toggleState = toggleState;
        self.scope.toggle_lable = 'Close filter';
        self.scope.takeBid = takeBid;
        self.scope.pageno1 = 1;
        self.scope.bid = {};
        self.scope.parentJoblist = [];
        self.scope.loadparentJobs = loadparentJobs;
        self.scope.parentJobtab = 'quoted';
        self.scope.currentTab = 'available';
        self.scope.parentTabchange = parentTabchange;
        self.scope.viewJob = viewJob;
        self.scope.confirmBooking = confirmBooking;
        self.scope.paymentSuccess = paymentSuccess;
        self.scope.closePopup = closePopup;
        self.scope.currentName = $state.current.name;
        self.scope.clearAllfilters = clearAllfilters;
        self.scope.clearfilter = clearfilter;
        self.scope.getMessages = getMessages;
        self.scope.sendMessage = sendMessage;
        self.scope.markAscompleted = markAscompleted;
        self.scope.releasePayment = releasePayment;
        self.scope.feedback = {};
        self.scope.feedback.star = 0;
        self.scope.feedbackForm = {};
        self.scope.showmore = [];
        self.scope.messagetype = 'public';
        self.scope.getFeedback = getFeedback;
        self.scope.openFeedbackpopup = openFeedbackpopup;
        self.scope.disableFeedback = false;
        self.scope.showReplybox = [];
        self.scope.showReplies = [];
        self.scope.showsubReplies = [];
        self.scope.sentReply = sentReply;
        self.scope.sentsubReply = sentsubReply;
        self.scope.clientSentmsg = clientSentmsg;
        self.scope.input = {};
        self.scope.noToid = false;
        self.scope.currentMsgtab = 'public';
        self.scope.resetMsgs = resetMsgs;
        self.scope.currentUser = $cookieStore.get('userid');
        self.scope.deleteMessage = deleteMessage;
        self.scope.deleteReply = deleteReply;
        self.scope.deleteSubreply = deleteSubreply;
        self.scope.makeaJobview = makeaJobview;
        self.scope.jopPopup = jopPopup;
        self.scope.address1 = {};
        self.scope.job = {};
        self.scope.getLocations = getLocations;
        self.scope.onDateclear = onDateclear;
        self.scope.filteredLocations = [];
        self.scope.clientfeedback = {};
        self.scope.gotoWorkerTab = gotoWorkerTab;
        self.scope.isScrolled = window.scrollY;
        self.scope.getQuoterslist = getQuoterslist;
        self.scope.locationLoading = false;
        self.scope.viewprofile = viewprofile;
        self.scope.availableCreds = [];
        self.scope.availability = [];
        self.scope.Days = ['0', '1', '2', '3', '4', '5', '6'];
        self.scope.adminLogged = $cookieStore.get('adminLogged');
        self.scope.adminUrl = $cookieStore.get('adminUrl');
        self.scope.messages = [];
        self.scope.checkDeleteTimer = checkDeleteTimer;
        self.scope.messageMode = 'public';
        self.scope.closeJob = closeJob;
        self.scope.paypal = {};
        self.scope.gotoWorkerprofile = gotoWorkerprofile;
        self.scope.jobcanCalled = false;
        self.scope.paymentAlerts = [];
        self.scope.userSuggestions = [];
        self.scope.loggedUser = $cookieStore.get('name');
        self.scope.updateScroll = updateScroll;
        self.scope.jobBlocker = jobBlocker;
        

        //worker or parent authenticate
        if(self.scope.currentName == 'rateForClientMail'){
            if($cookieStore.get('usertype') == 'parent'){
                $state.go('/')
            }
        }else if(self.scope.currentName == 'releasePaymentMail'){
            if($cookieStore.get('usertype') == 'worker'){
                $state.go('/')
            }
        // }
    }else if(self.scope.currentName == 'workerviewjobMail'){
            if($cookieStore.get('usertype') == 'parent'){
                $state.go('/')
            }
        }
        
    ///reference for redirection admin

    //      var path = $location.path()
    //     //worker or parent authenticate
    //     if(self.scope.currentName == 'rateForClientMail'){
    //         if($cookieStore.get('usertype') == 'parent' && !$cookieStore.get('adminLogged')){
    //             $state.go('/')
    //         }else if($cookieStore.get('adminLogged')){
    //             var worker_id = $state.params.worker_id;
    //             setTimeout(function(){
    //                 $rootScope.$broadcast('MailUserDefaultSelect',{"userMode":'worker','user_id':worker_id,'path':path})
    //             },2000)
    //         }
    //     }else if(self.scope.currentName == 'releasePaymentMail'){
    //         if($cookieStore.get('usertype') == 'worker' && !$cookieStore.get('adminLogged')){
    //             $state.go('/')
    //         }else if($cookieStore.get('adminLogged')){
    //             var client_id = $state.params.client_id;
    //             setTimeout(function(){
    //                 $rootScope.$broadcast('MailUserDefaultSelect',{"userMode":'client','user_id':client_id,'path':path})
    //             },2000)
    //         }
    //     }
    // }else if(self.scope.currentName == 'workerviewjobMail' ){

    //         if($cookieStore.get('usertype') == 'parent' && !$cookieStore.get('adminLogged')){
    //             alert()
    //             $state.go('/')
    //         }else if($cookieStore.get('adminLogged')){
    //             alert()
    //             var workerId = $state.params.workerId;
    //            setTimeout(function(){
    //                 $rootScope.$broadcast('MailUserDefaultSelect',{"userMode":'worker','user_id':workerId,'path':path})
    //             },2000)
    //         }
    //     }

        /*It is for admin Impersonate client jobs or worker job
          author Udhayakumar.s */
        $scope.$on('adminSelectUser',function(evt,arg){
            if(arg.userMode == 'client'){
                self.scope.parentJobtab = 'posted';
                getQuotedjoblist(1, 'posted');
                // showSuggestions('worker')
            }else if(arg.userMode == 'worker'){
                self.scope.currentTab = 'available';
                self.scope.job_status = 0;
                getJoblist(1);
                showSuggestions('worker')
            }
        })
        self.scope.closeAlert = function(index) {
    self.scope.paymentAlerts.splice(index, 1);
  };
        var title = '<div class="col-md-12"><div class="logo_section" id="logo"><img src="styles/images/logo.png" alt=""></div></div>';

        var callingCount = $cookieStore.get('callingCount');
        callingCount +=1;
        $cookieStore.put('callingCount',callingCount);
        // switchTab('jobdetails');


        // self.scope.config = {
        //     autoHideScrollbar: false,
        //     theme: '3d-dark',
        //     advanced: {
        //         updateOnContentResize: true
        //     },
        //     setHeight: 300,
        //     scrollInertia: 0,
        //     axis: 'y'
        // };
        self.scope.callback = {};

        self.scope.callback.leftprivate = {
            autoHideScrollbar: false,
            theme: '3d-dark',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 415,
            scrollInertia: 0,
            axis: 'y' // enable 2 axis scrollbars by default
        };

        self.scope.rightprivate = {
            autoHideScrollbar: false,
            theme: '3d-dark',
            advanced: {
                updateOnContentResize: true
            },
            setHeight: 403,
            scrollInertia: 0,
            axis: 'y', // enable 2 axis scrollbars by default
            callbacks: {
                onInit: function () {
                    // alert();
                    // console.log(this)
                    // scrollTo(this.mcs.top, 0, {
                    //     scrollInertia: 300
                    // });
                    // $scope.load = true;
                    // if(this.mcs.topPct == 100 && !this.mcs.already){
                    //     this.mcs.already = true;
                    //     // console.log(this.mcs)
                    //     loadNotifs()

                    // }
                    $location.hash('mCSB_4_container');

                      // call $anchorScroll()
                    $anchorScroll();
                    console.log(this.mcs);
                    // updateStatsDisplay(1, this.mcs);
                },
                whileScrolling: function() {

                    // updateStatsDisplay(1, this.mcs);
                }
        }
        };

        if(self.scope.currentName == 'releasePaymentMail'){
             self.scope.feedback.star = 0;
             var jobId = $state.params.jobid;
             var confirmUserId = $state.params.confirm_userid;
             alert(confirmUserId)
             var clientId = $state.params.client_id;
             var path = $location.path();
            // setTimeout(function(){
            //      $rootScope.$broadcast('MailUserDefaultSelect',{"userMode":'client','user_id':clientId,'path':path})
            // },2000)
             releasePayment(jobId,confirmUserId,'releasePaymentMail')
        }



        self.scope.editJob = editJob;
        // var alertConfirmed = $cookieStore.get('confirmAlert')
        if (self.scope.currentName == "joblist" || (self.scope.currentName == 'admin.workerview' && $cookieStore.get('adminuserMode') == 'worker')) {
            $scope.$watch('address1.date', function(newValue, oldValue) {
                if (!newValue) {
                    getJoblist(1);
                }
            });
            self.scope.currentTab = $state.params.tabname;
            if (self.scope.currentTab == 'available') {
                self.scope.job_status = 0;
                showSuggestions('worker');
            } else if (self.scope.currentTab == 'applied') {
                self.scope.job_status = 1;
                showSuggestions('worker');
            } else if (self.scope.currentTab == 'confirmed') {
                // if(!alertConfirmed)
                if($cookieStore.get('showJobStatusPopup') == true){
                    showJobPop('Job confirmed', 'confirmed');
                }
                self.scope.job_status = 2;
            } else if (self.scope.currentTab == 'completed') {

                self.scope.job_status = 3;
                showSuggestions('worker');
            } else if (self.scope.currentTab == 'paid') {
                self.scope.job_status = 4;
                showSuggestions('worker');
            } else if (self.scope.currentTab == 'closed') {
                self.scope.job_status = 5;
                showSuggestions('worker');
            }
            if (!self.scope.currentTab){
                self.scope.currentTab = 'available'
                showSuggestions('worker');
            }
            $cookieStore.put('workerTab', self.scope.currentTab);
            // if (!self.scope.currentTab) {
            //     self.scope.currentTab = 'available';
            // }
            // getJoblist(self.scope.pageno);
            getRequirements();
        }
        if (self.scope.currentName == "parentjoblist" || (self.scope.currentName == 'admin.clientview' && $cookieStore.get('adminuserMode') == 'client') ) {
            self.scope.parentJobtab = $state.params.tabname;
            if(!self.scope.parentJobtab)
                self.scope.parentJobtab = 'posted'
            $cookieStore.put('parentTab', self.scope.parentJobtab);
            if (self.scope.parentJobtab == 'completed') {
                if($cookieStore.get('showJobStatusPopup') == true){
                    showJobPop('Job Completed', 'completed');
                }
            }
            else{
                showSuggestions('client');
            }
            getQuotedjoblist(self.scope.pageno1, self.scope.parentJobtab);
        }
        if(self.scope.currentName == 'admin.workerview' || self.scope.currentName == 'admin.clientview'){
            var viewId = $state.params.userid;
            var adminOldpath = '#' + $location.path();
            $cookieStore.put('adminState',$state.current.name);
            $cookieStore.put('adminTabname',$state.params.tabname);
            $cookieStore.put('adminUrl',adminOldpath);
            if(self.scope.currentName == 'admin.clientview'){
                self.scope.currentTab = $state.params.tabname;
            }else{
                self.scope.parentJobtab = $state.params.tabname;
            }
        }
        // if($location.path() == "/previewjob"){

        // // }
        //  votingCtrl.currentName     = $state.current.name;
        // votingCtrl.slug                         = $state.params.slug;
        function showJob() {
            // body...
            var data = {};
            data['jobid'] = self.scope.jobId;
            data['userid'] = $cookieStore.get('userid');
            jobService.viewJob(data, function(response) {
                if (response.Status == "200") {

                    self.scope.job = response.jobdetail;
                    self.scope.job_status = response.jobdetail.jobs_status;
                } else if (response.Status == "500") {

                    toastr.error("This Job was confirmed for another worker");
                }
            });
        }
        if (self.scope.currentName == 'previewjob' || self.scope.currentName == 'successPayment' || self.scope.currentName == 'releasePaymentMail' ) {
            self.scope.jobId = $state.params.jobid;
            self.scope.currentTabpreview = $state.params.tabname;
            self.scope.prevtab = $cookieStore.get('parentTab');
            var data = {};
            data['jobid'] = self.scope.jobId;
            data['userid'] = $cookieStore.get('userid');
            if (self.scope.currentName == 'successPayment'){
                var quoteObj = $cookieStore.get('quoteObj');
                if(quoteObj){
                    var payerId = $location.search().PayerID;
                    var paymentId = $location.search().paymentId;
                    executePayment(payerId,paymentId);
                }
                else{
                    jobService.viewJob(data, function(response) {
                        if (response.Status == "200") {
                            self.scope.job = response.jobdetail;
                            self.scope.job_status = response.jobdetail.jobs_status;
                        } else if (response.Status == "500") {
                            toastr.error("This Job was confirmed for another worker");
                        }
                    });
                }
            }
            else{
                jobService.viewJob(data, function(response) {
                if (response.Status == "200") {
                    self.scope.job = response.jobdetail;
                    self.scope.job_status = response.jobdetail.jobs_status;
                } else if (response.Status == "500") {
                    toastr.error("This Job was confirmed for another worker");
                }
            });
            }
            switchTab($state.params.tabname);
        }

        if(self.scope.currentName == 'successPayment'){
            self.scope.jobId = $state.params.jobid;
            // console.log(self.scope.payerId);
            self.scope.currentTabpreview = $state.params.tabname;
            self.scope.prevtab = $cookieStore.get('parentTab');

        }

        if (self.scope.currentName == 'workerviewjob' || self.scope.currentName == 'rateForClientMail' || "workerviewjobMail") {
            // console.log($state.params.jobid);
            self.scope.jobId = $state.params.jobid;
            // self.scope.currentTabpreview = $state.params.previewTab;
            // console.log($state.params.tabname);
            self.scope.workerprevtab = $cookieStore.get('workerTab');
            var data = {};
            data['jobid'] = self.scope.jobId;
            data['userid'] = $cookieStore.get('userid');
            jobService.viewJob(data, function(response) {
                if (response.Status == "200") {
                    self.scope.job = response.jobdetail;
                    self.scope.job_status = response.jobdetail.job_status;
                    self.scope.toId = response.jobdetail.user_id;
                } else if (response.Status == "500") {
                    toastr.error("This Job was confirmed for another worker");
                }
                if(self.scope.currentName == 'rateForClientMail'){
                    var workerId = $state.params.worker_id;
                    if(response.Status == '200')
                        setTimeout(function(){
                                // $rootScope.$broadcast('MailUserDefaultSelect',{"userMode":'worker','user_id':workerId})
                            angular.element('#rateforclient').trigger('click')
                        },1000)

                }
            });
            self.scope.job.jobid = self.scope.jobId;
            switchTab($state.params.tabname);
            if($cookieStore.get('redirectState')){
                $cookieStore.remove('redirectState');
                $cookieStore.remove('redirectParams');
            }
        }


        // preview job vertical tab
        function switchTab(tab,workerid,job_status,jobId) {
            self.scope.currentTabpreview = tab;
            angular.element('#' + tab).show();
            // angular.element('#'+tab).trigger('click');
            if (tab == 'messages') {
                if(job_status < 2 || self.scope.job_status < 2)
                getMessages(self.scope.jobId, 'public');
                else{
                    getMessages(self.scope.jobId, 'private');
                    resetMsgs('private')
                }

            }
            if(tab == 'confirmed_worker'){
                viewprofile(workerid);
            }
            if(tab == "myquotedetail"){
                getUserQuoteDetails(jobId)
            }
            window.scrollTo(0, 0);

        }

        //to get the jobs
        function getJoblist(pageno) {
            self.scope.pageno = pageno;
            self.scope.isLoading = true;
            self.scope.joblist = [];
            self.scope.searchlocselected = true;
            var data = { "pageno": pageno, "limit": self.scope.limit, "job_type": self.scope.job_type };
            var location = angular.copy(self.scope.address1);
            if (self.scope.address1.fullAddress != "" && self.scope.address1.fullAddress) {
                if(self.scope.address1.fullAddress.suburb)
                Object.assign(data, location);
                else{
                    self.scope.joblist = [];
                    self.scope.isLoading = false;
                    return false;
                }
                // data['postcode'] = location.postcode;
            }
            if (location.date) {
                data['date'] = moment(location.date).format('DD-MM-YYYY');
            }
            data['job_status'] = self.scope.job_status;
            data['requirements'] = self.scope.selectedreq;
            data['caregivers'] = self.scope.selectedCaregiver;
            data['jobtype'] = self.scope.selectedJobtype;
            data['caretime'] = self.scope.selectedCaretime;
            data['session'] = self.scope.selectedSession;
            data['userid'] = $cookieStore.get('userid');
            jobService.getjobs(data, function(response) {
                self.scope.isLoading = false;
                if (response.Status == "200") {
                    self.scope.joblist = response.data;
                    self.scope.status_counts = response.status_counts;
                    self.scope.total_general = response.total_count;
                }
            });
        }
        // To get the user applied jobs
        // function getAppliedjobs(){
        //     var data = { "pageno": pageno, "limit": self.scope.limit, "job_type": self.scope.job_type};
        //     data['userid'] = $cookieStore.get('userid');
        //     self.scope.isLoading = true;
        //     jobService.getappliedjobs(data,function(response){
        //         if(response.Status=="200"){
        //             self.scope.isLoading = false;
        //             self.scope.joblist = response.data;
        //             self.scope.total_general = response.total_count;
        //         }
        //     });
        // }

        function strtoarr(str) {
            if (str) {
                var arr = str.split(',');
                return arr;
            }
        }

        function loadPages() {
            if (self.scope.total_general) {
                if (self.scope.total_general / self.scope.limit > self.scope.pageno) {
                    // alert();
                    self.scope.jobcanCalled = true;
                    self.scope.pageno += 1;
                    append_jobs(self.scope.pageno);
                }
            }
        }

        function append_jobs(pageno) {
            // alert(self.scope.total_general);
            if (pageno) {
                var data = { "pageno": pageno, "limit": self.scope.limit, "job_type": self.scope.job_type };
                var location = angular.copy(self.scope.address1);

                if (self.scope.address1.fullAddress != "" && self.scope.address1.fullAddress) {
                    Object.assign(data, location);
                }
                if (location.date) {
                    data['date'] = moment(location.date).format('DD-MM-YYYY');
                }
                data['job_status'] = self.scope.job_status;
                data['requirements'] = self.scope.selectedreq;
                data['caregivers'] = self.scope.selectedCaregiver;
                data['jobtype'] = self.scope.selectedJobtype;
                data['caretime'] = self.scope.selectedCaretime;
                data['session'] = self.scope.selectedSession;
                data['userid'] = $cookieStore.get('userid');
                self.scope.isLoading = true;
                jobService.getjobs(data, function(response) {
                    self.scope.jobcanCalled = false;
                    self.scope.isLoading = false;
                    if (response.Status == "200") {
                        if (response.data.length > 0) {
                            // self.scope.isLoading = false;
                            // alert();
                            self.scope.joblist = self.scope.joblist.concat(response.data);

                        }
                        self.scope.total_general = response.total_count;
                    }
                });
            }
        }

        function toggleCaregiver(option) {
            // alert();
            var idx = self.scope.selectedCaregiver.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedCaregiver.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedCaregiver.push(option);
            }
            getJoblist(1);
            // console.log(hm.selectedCaregiver);
        };

        function toggleCaretype(opt) {
            var idx = self.scope.selectedJobtype.indexOf(opt);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedJobtype.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedJobtype.push(opt);
            }
            getJoblist(1);
        }

        function togglereq(opt) {
            var idx = self.scope.selectedreq.indexOf(opt);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedreq.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedreq.push(opt);
            }
            getJoblist(1);
        }

        function toggleCaretime(opt) {
            var idx = self.scope.selectedCaretime.indexOf(opt);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedCaretime.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedCaretime.push(opt);
            }
            getJoblist(1);
        }

        function toggleSession(opt) {
            var idx = self.scope.selectedSession.indexOf(opt);

            // Is currently selected
            if (idx > -1) {
                self.scope.selectedSession.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.selectedSession.push(opt);
            }
            getJoblist(1);
        }

        function searchbyLocation() {
            // alert();
            getJoblist(1);
        }
        // for date picker
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.maxDate = new Date(2020, 5, 22);

        $scope.open = function($event) {
            $scope.status.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];

        $scope.status = {
            opened: false
        };

        // Search by job status
        function searchbyStatus(status, tabname) {
            // alert();
            self.scope.joblist = [];
            // console.log(self.scope.joblist);
            self.scope.currentTab = tabname;
            $cookieStore.put('workerTab', tabname);
            self.scope.job_status = status;
            getJoblist(1);
        }
        //get cities for lookup
        function getCities() {
            jobService.getCities()
        }

        self.scope.searchlocselected = true;
        //autocomplete city state
        $scope.$on('locationFilter', function(event, arg) {
            self.scope.searchlocselected = false;
            self.scope.searchLocations = arg.filter_result;
        });

        function selectLocation(obj) {
            // hm.joblocation.
            self.scope.address.city = obj.suburb;
            // hm.joblocation.city = obj.city;
            self.scope.address.state = obj.state;
            self.scope.address.post_code = obj.postcode;
            console.log(self.scope.address);
            self.scope.searchlocselected = true;
            self.scope.address1.fullAddress = obj.suburb + ", " + obj.state + ", " + obj.postcode;


        }

        function submitBid(jobid, obj) {
            self.scope.bid = {};
            makeaJobview(jobid);
            self.scope.jobId = jobid;
            self.scope.currentBid = obj;
            var options = {
                templateUrl: 'scripts/module/job/templates/bidsubmit.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="styles/images/logo.png" alt=""></div></div>',
            };
            $ngBootbox.customDialog(options);
        }

        function getdayFromdate(date) {
            date = new Date(date);
            return date.getDay()
        }

        function sentMessage(toId, jobid) {
            self.scope.toId = toId;
            self.scope.messageJobid = jobid;
            var options = {
                templateUrl: 'scripts/module/job/templates/sentmsg.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="styles/images/logo.png" alt=""></div></div>',
            };
            $ngBootbox.customDialog(options);
        }

        function toggleState() {
            self.scope.state = !self.scope.state;
            if (self.scope.state) {

                self.scope.toggle_lable = 'Close filter';
            } else {
                self.scope.toggle_lable = 'Open filter';
            }
        }

        function takeBid(form) {
            form.$setSubmitted();
            // console.log(form);
            // alert();
            if (form.$valid) {
                var data = self.scope.bid;
                data['quote'] = data['rate_per_hour'] * self.scope.currentBid.hours_required;
                data['jobtypeid'] = self.scope.jobId;
                data['userid'] = $cookieStore.get('userid');
                jobService.submitBid(data, function(response) {
                    if (response.Status == "200") {
                        self.scope.bid = {};
                        searchbyStatus(1, 'applied');
                        toastr.success('Your bid has been submitted to the client. We will notify you, when the client approves one of the suitable bids. Please do not start work yet. Kindly wait for our notification.', 'Bid submitted');
                        $ngBootbox.hideAll();
                        getJoblist(1);
                    }
                });
            }
        }

        function getQuotedjoblist(pageno, type) {
            if (pageno) {
                var data = {};
                data['pageno'] = pageno;
                data['limit'] = self.scope.limit;
                data['type'] = type;
                data['userid'] = $cookieStore.get('userid');
                self.scope.isLoading = true;
                jobService.getquotedJobs(data, function(response) {
                    self.scope.isLoading = false;
                    if (response.Status == "200") {
                        self.scope.parentJoblist = response.data;
                        self.scope.parenjobsTotal = response.total_count;
                        self.scope.status_counts = response.status_counts;
                    }
                });
            }
        }

        function loadparentJobs() {
            if (self.scope.parenjobsTotal) {
                if (self.scope.parenjobsTotal / self.scope.limit > self.scope.pageno) {
                    // alert();
                    self.scope.jobcanCalled = true;
                    self.scope.pageno += 1;
                    if(self.scope.parentJobtab!='draft')
                    appendParentjobs(self.scope.pageno, self.scope.parentJobtab);
                }
            }
        }

        function appendParentjobs(pageno, type) {
            // alert();
            if (pageno) {
                var data = {};
                data['pageno'] = pageno;
                data['limit'] = self.scope.limit;
                data['type'] = type;
                data['userid'] = $cookieStore.get('userid');
                jobService.getquotedJobs(data, function(response) {
                    if (response.Status == "200") {
                        self.scope.jobcanCalled = false;
                        self.scope.parentJoblist = self.scope.parentJoblist.concat(response.data);
                        self.scope.parenjobsTotal = response.total_count;
                        self.scope.completed_count = response.completed_count;
                    }
                });
            }
        }

        function parentTabchange(tab) {
            self.scope.pageno = 1;
            self.scope.parentJobtab = tab;
            $cookieStore.put('parentTab', self.scope.parentJobtab);
            if(tab!='draft')
            getQuotedjoblist(self.scope.pageno, tab);
            else
                getDraftedjobs(self.scope.pageno);
            // $state.go('parentjoblist', { "tabname": tab });
        }

        //viewJob
        function viewJob(jobid) {
            $state.go('previewjob', { jobid: jobid ,tabname:'quotes'});
        }

        //show confirm booking popup
        function confirmBooking(obj, jobid) {
            self.scope.quoteobj = obj;
            self.scope.quoteobj['jobid'] = jobid;
            var options = {
                templateUrl: 'scripts/module/job/templates/confirmjob.html',
                scope: $scope,
                closeButton: true,
                title: 'Confirm booking',
            };
            $ngBootbox.customDialog(options);
        }

        //payment success popup
        function paymentSuccess() {
            var obj = self.scope.quoteobj;
            $cookieStore.put('quoteObj',self.scope.quoteobj);
            getPaypalToken(obj.client_quote, obj.jobid);
        }

        function paymentSuccess1(obj) {
            self.scope.quoteobj = obj;
            // body...
            // var obj = self.scope.quoteobj;
            var data = {};
            var options = {
                templateUrl: 'scripts/module/job/templates/paymentsuccess.html',
                scope: $scope,
                closeButton: false,
                title: 'Payment success',
            };
            data['id'] = obj.id;
            data['funds_transfered'] = obj.client_quote;
            jobService.confirmJob(data, function(response) {
                if (response.Status == '200') {
                    $ngBootbox.hideAll();
                    $cookieStore.remove('quoteObj');
                    var data = {};
                    data['jobid'] = self.scope.jobId;
                    data['userid'] = $cookieStore.get('userid');
                    jobService.viewJob(data, function(response) {
                        if (response.Status == "200") {
                            self.scope.prevtab = 'confirmed';
                            self.scope.job = response.jobdetail;
                            self.scope.job_status = response.jobdetail.jobs_status;
                        }
                    });
                    $ngBootbox.customDialog(options);
                }
            });
        }

        function closePopup(userId) {
            $ngBootbox.hideAll();
            switchTab('confirmed_worker',userId)
        }

        // To clear the filters
        function clearAllfilters() {
            self.scope.job_status = '';
            self.scope.selectedreq = [];
            self.scope.selectedCaregiver = [];
            self.scope.selectedJobtype = [];
            self.scope.selectedCaretime = [];
            self.scope.selectedSession = [];
            getJoblist(self.scope.pageno);
        }

        // To clear the given filter
        function clearfilter(filter) {
            if (filter == 'req') {
                self.scope.selectedreq = [];
            } else if (filter == 'job_status') {
                self.scope.job_status = '';
            } else if (filter == 'caregiver') {
                self.scope.selectedCaregiver = [];
            } else if (filter == 'jobtype') {
                self.scope.selectedJobtype = [];
            } else if (filter == 'caretime') {
                self.scope.selectedCaretime = [];
            } else if (filter == 'session') {
                self.scope.selectedSession = [];
            }
            getJoblist(self.scope.pageno);
        }

        // Get messages
        // function getMessages(jobId) {
        //     var data = {};
        //     data['userid'] = $cookieStore.get('userid');
        //     data['jobId'] = jobId;
        //     jobService.getMessages(data, function(response) {
        //         if (response.Status == "200") {
        //             self.scope.messages = response.data;
        //         }
        //     });
        // }

        //sendMessage

        function sendMessage(to_id, jobid) {
            if(self.scope.message){
                var data = {};
                data['from_id'] = $cookieStore.get('userid');
                data['to_id'] = to_id
                data['jobId'] = jobid;
                data['message'] = self.scope.message;
                data['message_type'] = self.scope.messagetype;
                if (data['from_id'] && data['to_id']) {
                    jobService.sendMessage(data, function(response) {
                        if (response.Status == "200") {
                            toastr.success('Message sent');
                            self.scope.message = "";
                            self.scope.messagetype = 'public';
                            $ngBootbox.hideAll();
                        } else {
                            self.scope.message = "";
                            self.scope.messagetype = 'public';
                            toastr.error(response.message);
                        }
                    });
                }
            }
        }

        function editJob(Jobid) {
            $rootScope.$broadcast('editJobscope', { "Jobid": Jobid, "is_edit": 1 });
        }

        function getRequirements() {
            jobService.getRequirements(function(response) {
                if (response.Status == "200") {
                    self.scope.requirements = response.reqs;
                }
            });
        }

        /*
        To mark a job as completed
        **/
        function markAscompleted(jobid) {
            var opt = {
                templateUrl: 'scripts/module/job/templates/job_complete_popup.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            var data = {};
                            data['jobtypeid'] = jobid;
                            data['hideLoader'] = 1;
                            self.scope.generalLoader = true;
                            jobService.completeJob(data, function(response) {
                                self.scope.generalLoader = false;
                                if (response.Status == "200") {
                                    toastr.success("Job completed");
                                    // self.scope.currentTab = 'paid'
                                    // self.scope.job_status = 4;
                                    // getJoblist(1);
                                    $state.go('joblist', { "tabname": 'completed' })

                                }
                            });

                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);


        }
        /*
        To release the payment for a job
        **/
        function releasePayment(jobId, to_userid,releasePaymentMail) {
            var opt = {
                templateUrl: 'scripts/module/job/templates/release_payment_client.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {

                            openFeedbackpopup(jobId, to_userid, 'parent');
                            // var data = {};
                            // data['jobtypeid'] = jobId;
                            // jobService.releasePayment(data, function(response) {
                            //     if (response.Status == "200") {
                            //         toastr.success("Payment Released","success");
                            //     }
                            // });

                        }
                    }

                }
            };

            if(!releasePaymentMail){
                $ngBootbox.customDialog(opt);
            }else{
                openFeedbackpopup(jobId, to_userid, 'parent','',releasePaymentMail); 
            }

        }

        /*
        To get the feedback from the user
        **/
        function openFeedbackpopup(jobId, to_userid, saveType, obj,releasePaymentMail,rateCareClick) {
           // console.log($cookieStore.get('usertype'))
            self.scope.disableFeedback = false;
            setTimeout(function() {
                angular.element('body').addClass('modal-open');
            }, 500);

            self.scope.feedback = {};
            var opt = {
                templateUrl: 'scripts/module/job/templates/feedback_form.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                buttons: {
                    Cancel: {
                        lable: "Cancel",
                        className: "btn btn-default loginbtn rightside_btn",
                        callback: function() {
                             if(saveType == 'parent'){
                                $state.go('parentjoblist', { "tabname": 'paid' });
                             }
                                return true
                        }
                    },
                    success: {
                        label: "Submit",
                        className: "btn btn-primary signupbtn rightside_btn",
                        callback: function() {
                            var form = self.scope.feedbackForm;
                            self.scope.feedbackForm.$setSubmitted();
                            if (form.$valid) {
                                var data = {};
                                data['jobtypeid'] = jobId;
                                self.scope.feedback.from_user = $cookieStore.get('userid');
                                self.scope.feedback.jobtypeid = jobId
                                self.scope.feedback.to_user = to_userid;
                                data['review'] = angular.copy(self.scope.feedback);
                                if (saveType == 'worker') {
                                    self.scope.generalLoader = true;
                                    jobService.saveReview(data, function(response) {
                                        self.scope.generalLoader = false;
                                        if (response.Status == "200") {
                                            toastr.success('Review Saved!!');
                                            obj.has_review_given = true;
                                        } else {
                                            toastr.error(response.errors);
                                        }
                                    });
                                }else{
                                    data['jobtypeid'] = jobId;
                                    data['hideLoader'] = 1;

                                    self.scope.generalLoader = true;
                                    jobService.releasePayment(data, function(response) {
                                        self.scope.generalLoader = false;
                                        if (response.Status == "200") {
                                            toastr.success("Review Saved", "success");
                                            obj.has_review_given = true;
                                            if(releasePaymentMail){
                                                $state.go('parentjoblist', { "tabname": 'paid' });
                                            }
                                            
                                        }
                                    });
                                }

                            } else {
                                $scope.$apply(function(){
                                    self.scope.feedbackForm.$setSubmitted();
                                });
                                // toastr.error('Please complete the review form');
                                return false;
                            }


                        }
                    }

                }
            };

            var cookieUsertype = ($cookieStore.get('usertype') == 0 || $cookieStore.get('usertype') == 'parent') ? 'parent': 'worker';
            if(saveType == 'parent' && !rateCareClick){
                var data = {};
                data['jobtypeid'] = jobId;
                data['apiType']  = (releasePaymentMail) ? releasePaymentMail : rateCareClick;
                jobService.releasePayment(data, function(response) {
                    if (response.Status == "200") {
                        toastr.success("Payment Released", "success");
                    }else{
                        if(!$cookieStore.get('adminLogged') && !self.scope.currentName == 'releasePaymentMail'){
                            toastr.error(response.message)
                        }
                        
                    }
                    
                    $ngBootbox.customDialog(opt);
                    
                });
            }else if(saveType == 'worker' || rateCareClick){
                $ngBootbox.customDialog(opt); 
            }
            
           
        }

        /*
        To get the feedback given
        **/
        function getFeedback(jobid, usertype, type) {
            var data = {};
            data['jobid'] = jobid;
            data['from_user'] = $cookieStore.get('userid');
            data['usertype'] = usertype;
            data['hideLoader'] = 1;
            self.scope.generalLoader = true;
            jobService.getFeedback(data, function(response) {
                self.scope.generalLoader = false;
                if (response.Status == "200") {
                    self.scope.disableFeedback = true;
                    self.scope.feedback = response.feedback;
                    if (type) {
                        self.scope.clientfeedback = response.feedback;
                    }
                    var opt = {
                        templateUrl: 'scripts/module/job/templates/feedback_form.html',
                        scope: $scope,
                        closeButton: true,
                        title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                        buttons: {
                            Close: {
                                lable: "Cancel",
                                className: "btn btn-default loginbtn rightside_btn",
                                callback: function() {
                                    return true
                                }
                            }
                        }
                    };
                    if (!type) {
                        $ngBootbox.customDialog(opt);
                    }

                }
            });
        }


        /*
        To get the messages
        **/
        function getMessages(jobId, type, toId, index) {
            // alert(self.scope.currentName)
            var data = {};
            data['jobId'] = jobId;
            data['message_type'] = type;
            if (type == 'private') {
                if (toId != '' || !toId) {
                    self.scope.selectedUser = toId;
                    self.scope.messages = [];
                    self.scope.noToid = true;
                    self.scope.activePrivateuser = toId;
                } else {
                    self.scope.noToid = false;
                }
            }
            data['to_id'] = toId;
            data['user_id'] = $cookieStore.get('userid');
            jobService.getMessages(data, function(response) {
                if (response.Status == "200") {
                    if(type == 'private'){
                        // alert();
                        readMessages(jobId,toId, 0);
                        // setTimeout(function(){
                        //     updateScroll('mCSB_4');
                        // },1000);

                    }
                    self.scope.noToid = false;
                    self.scope.messages = response.data;
                } else if (response.Status == "500") {
                    self.scope.noToid = true;
                }
            });
        }

        function sentReply(msgId, obj) {
            if (self.scope.input.reply) {
                var data = {};
                data['messageId'] = msgId;
                data['reply'] = self.scope.input.reply;
                data['userid'] = $cookieStore.get('userid');
                jobService.sentReply(data, function(response) {
                    if (response.Status == '200') {
                        self.scope.input.reply = '';
                        obj.reply_count += 1;
                        obj.replies.splice(0, 0, response.reply);
                    }
                });
            }

        }


        function sentsubReply(replyId, obj) {
            if (self.scope.input.subreply) {
                var data = {};
                data['replyId'] = replyId;
                data['subreply'] = self.scope.input.subreply;
                data['userid'] = $cookieStore.get('userid');
                jobService.sentsubReply(data, function(response) {
                    if (response.Status == '200') {
                        self.scope.input.subreply = '';
                        // obj.reply_count+=1;
                        // console.log(response.subreply);
                        if(obj.subreplies)
                        obj.subreplies.splice(0, 0, response.subreply);
                        else{
                            obj.subreplies = [];
                            obj.subreplies.push(response.subreply);
                        }

                    }
                });
            }

        }
        // sent message by client
        function clientSentmsg(type, jobid, to_id) {
            var data = {};
            data['from_id'] = $cookieStore.get('userid');
            data['jobId'] = jobid;
            data['message'] = angular.copy(self.scope.input.message);
            self.scope.input.message = "";
            data['message_type'] = type;
            if (type == 'private') {
                if (to_id != '' || !to_id) {
                    self.scope.noToid = true;
                }
            }
            data['to_id'] = to_id;
            if (data['message']) {
                jobService.sendMessage(data, function(response) {
                    if (response.Status == '200') {
                        self.scope.noToid = false;
                        self.scope.input.message = '';
                        if(type == 'public')
                            self.scope.messages.splice(0,0,response.message);
                        else{
                            self.scope.messages.push(response.message);
                            console.log(self.scope.callback.updateScrollbar);
                            // self.scope.callback.updateScrollbar("scrollTo", 'bottom');

                            // updateScroll();
                            // var id = '#privateMsg'+(self.scope.messages.length-1);
                            // setTimeout(function(){
                            //     self.scope.updatepvtScrollbar('scrollTo',)
                            //     console.log(angular.element("#pvtSection"));
                            //     angular.element(id)[0].scrollTop = angular.element("#pvtSection")[0].scrollHeight;
                            // },500);

                        }
                        if (response.message.message_type == 'private') {
                            self.scope.selectedUser = to_id;
                        }
                    }
                });
            }
            else{
                self.scope.messageError = true;
                return false;
            }

        }
        function updateScroll(){
            // alert();
            var element = angular.element('#mCSB_4');
            console.log(element);
            setTimeout(function(){
                element[0].scrollTop = element[0].scrollHeight;
            },500);

            self.scope.callback.updateScrollbar("update");
        }


        // To reset the users messages
        function resetMsgs(type, toId) {
            // console.log(type);
            self.scope.messageMode = type;
            if (type == 'private') {
                if (self.scope.currentName != 'workerviewjob') {
                    getQuoterslist(self.scope.job.jobid);
                    setTimeout(function(){
                        if(self.scope.quoters.length > 0){
                            var firstquoter =  self.scope.quoters[0].userid__id
                            getMessages(self.scope.job.jobid, 'private',firstquoter)
                            self.scope.activePrivateuser = firstquoter;
                            // updateScroll('mCSB_4');
                        }
                    },800);
                    self.scope.messages = [];
                    self.scope.noToid = true;
                } else {
                    getMessages(self.scope.job.jobid, 'private', toId);
                }

            } else {
                getMessages(self.scope.job.jobid, 'public');
            }
            self.scope.currentMsgtab = type;
        }

        self.scope.autoCompleteOptions = {
            minimumChars: 1,
            data: function(term) {
                return $http.get('usa_states.json')
                    .then(function(response) {
                        // ideally filtering should be done on server
                        term = term.toUpperCase();
                        var match = _.filter(response.data, function(value) {
                            return value.name.startsWith(term);
                        });
                        return _.pluck(match, 'name');
                    });
            }
        };


        function deleteMessage(msgId, from_id,type) {
            var data = {};
            data['msgId'] = msgId;
            if (from_id === $cookieStore.get('userid')) {
                data['delType'] = 'from';
            } else {
                data['delType'] = 'to';
            }

            var opt = {
                    templateUrl: 'scripts/module/job/templates/deleteMessage.html',
                    scope: $scope,
                    closeButton: false,
                    title: title,
                    buttons: {
                        Close: {
                            lable: "Cancel",
                            className: "btn btn-default loginbtn",
                            callback: function() {
                                return true
                            }
                        },
                        Ok:{
                            lable: "Yes",
                            className: "btn btn-primary signupbtn",
                            callback: function() {
                                // return true
                                 data['hideLoader'] = 1;
                                 self.scope.generalLoader = true;
                                 jobService.deleteMsg(data, function(response) {
                                    self.scope.generalLoader = false;
                                    if (response.Status == "200") {
                                        toastr.success('Has been deleted', 'Message');
                                        if(!type)
                                        getMessages(self.scope.job.jobid, 'public');
                                        else
                                            getMessages(self.scope.job.jobid, 'private',self.scope.activePrivateuser);
                                    }
                                });
                            }
                        }
                    }
                };
            $ngBootbox.customDialog(opt);
    //         var delete_alert = {'template':'Are you sure you want to delete this message?',
    //         "title":title,
    //          buttons: {
    //                     cancel: {
    //                         lable: "No",
    //                         className: "btn btn-default loginbtn"
    //                     },
    //                     confirm: {
    //                         label: "Yes",
    //                         className: "btn btn-primary signupbtn",
    //                     }

    //                 }};
    //         $ngBootbox.confirm(delete_alert)
    // .then(function() {
    //     jobService.deleteMsg(data, function(response) {
    //             if (response.Status == "200") {
    //                 toastr.success('Has been deleted', 'Message');
    //                 if(!type)
    //                 getMessages(self.scope.job.jobid, 'public');
    //                 else
    //                     getMessages(self.scope.job.jobid, 'private',self.scope.activePrivateuser);
    //             }
    //         });
    // }, function() {

    // });

        }

        function deleteReply(replyId) {
            var data = {};
            data['replyId'] = replyId;
            jobService.deleteReply(data, function(response) {
                if (response.Status == "200") {
                    toastr.success('Has been deleted', 'Reply');
                    getMessages(self.scope.job.jobid, 'public');
                }
            });
        }


        function deleteSubreply(subreplyId) {
            var data = {};
            data['subreplyId'] = subreplyId;
            jobService.deleteSubreply(data, function(response) {
                if (response.Status == "200") {
                    toastr.success('Has been deleted', 'Subreply');
                    getMessages(self.scope.job.jobid, 'public');
                }
            });
        }

        $scope.$on('getparentJobs', function(eve,arg) {
            if(arg.incomplete){
                self.scope.parentJobtab = 'draft';
                getQuotedjoblist(1, 'draft');
            }else{
                self.scope.parentJobtab = 'posted';
                getQuotedjoblist(1, 'posted');
            }

        });

        /*
        To make a job viewed
        **/
        function makeaJobview(jobtypeid) {
            // body...
            var data = {};
            data['job_type_id'] = jobtypeid;
            data['user_id'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            jobService.makeaJobview(data, function(response) {
                if (response.Status == "200") {
                    self.scope.submitbidTooltip = response.tooltip;
                }
            });
        }

        function jopPopup() {
            $rootScope.$broadcast('callJobpopup');
        }


        function getLocations(viewValue) {
            self.scope.locationLoading = true;
            if (viewValue == '') {
                getJoblist(1);
            }
            var data = {};
            var result = [];
            self.scope.filteredLocations = [];
            data['searchKey'] = viewValue;
            jobService.getlocation(data, function(response) {
                self.scope.locationLoading = false;
                self.scope.filteredLocations = response.data;
                angular.forEach(response.data, function(data, key) {
                    var viewVal = data.suburb + ", " + data.state + " " + data.postcode;
                    self.scope.filteredLocations[key].viewval = viewVal;
                });
            });
            return self.scope.filteredLocations;
        }

        self.scope.filteredLocations = [];
        // self.scope.filteredLocations = [{"id":1,"postcode":200,"suburb":"Australian National University","state":"ACT","latitude":-35.28,"longitude":149.12},{"id":2,"postcode":221,"suburb":"Barton","state":"ACT","latitude":-35.2,"longitude":149.1},{"id":6,"postcode":810,"suburb":"Alawa","state":"NT","latitude":-12.38,"longitude":130.88},{"id":23,"postcode":812,"suburb":"Anula","state":"NT","latitude":-12.4,"longitude":130.91},{"id":58,"postcode":822,"suburb":"Acacia Hills","state":"NT","latitude":-12.8,"longitude":131.13},{"id":59,"postcode":822,"suburb":"Angurugu","state":"NT","latitude":-12.8,"longitude":131.13},{"id":60,"postcode":822,"suburb":"Anindilyakwa","state":"NT","latitude":-12.8,"longitude":131.13},{"id":61,"postcode":822,"suburb":"Annie River","state":"NT","latitude":-12.8,"longitude":131.13},{"id":168,"postcode":830,"suburb":"Archer","state":"NT","latitude":-12.49,"longitude":130.97},{"id":202,"postcode":846,"suburb":"Adelaide River","state":"NT","latitude":-13.23,"longitude":131.1}];


        /*
        To Refresh the jobs list after 10 minutes
        **/
        var loggedUsertype = $cookieStore.get('usertype');
        if (loggedUsertype == 'worker') {
            $interval(function() {
                getJoblist(1);
            }, 600000)
        } else {

        }

        function onDateclear() {
            if (self.scope.address1.date == '') {
                getJoblist(1);
            }
        }

        function gotoWorkerTab(tab) {
            // self.scope.currentTab = tab;
            // $cookieStore.get('confirmAlert')
            $ngBootbox.hideAll();
            showSuggestions('worker');
        }

        function showJobPop(title, type) {
            // console.log(title);
            if (callingCount == 1){
                if (type == "confirmed") {
                self.scope.popupContent = "Hi, You have a confirmed job. Check it and get started with the job";
            } else if (type == 'completed') {
                self.scope.popupContent = "Hi you have a completed job please check and release payment to the carer";
            }
            var opt = {
                templateUrl: 'scripts/module/job/templates/have_confirm_job.html',
                scope: $scope,
                closeButton: false,
                title: title,
                onEscape:function(){
                    showSuggestions('worker');
                },
                buttons: {
                    Ok: {
                        lable: "Ok",
                        className: "btn btn-default signupbtn rightside_btn",
                        callback: function() {

                            if(type == "completed"){
                                showSuggestions('client');
                            }
                            else{
                                self.scope.job_status = 2;
                                showSuggestions('worker');
                            }
                            return true
                        }
                    }
                }
            };
            $ngBootbox.customDialog(opt);
            }

        }
        // console.log(document.querySelector('.upToScroll'));
        //     angular.element(document.querySelector('.upToScroll')).bind('scroll', function(){
        //             alert('scrolling is cool!');

        // });
        function getQuoterslist(jobId,searchkey) {
            var data = {};
            data['jobtypeid'] = jobId;
            data['searchKey'] = searchkey;
            jobService.getquoters(data, function(response) {
                if (response.Status == "200") {
                    self.scope.quoters = response.data;
                }
            });
        }
        self.scope.quoters = [];

        function readMessages(jobtypeid,userid,index){
            var data = {};
            data['jobtypeid'] = jobtypeid;
            data['userid'] = userid;
            jobService.readMessages(data,function(response){
                if(response.Status == "200"){
                    if(self.scope.quoters.length>0)
                    self.scope.quoters[index].un_read_count = 0;
                }
            });
        }

        function getDraftedjobs(pageno){
            var data = {};
            data['pageno'] = pageno;
            data['userid'] = $cookieStore.get('userid');
            self.scope.isLoading = true;
            jobService.getdraftjobs(data,function(response){
                if(response.Status == "200"){
                    self.scope.isLoading = false;
                    if (response.Status == "200") {
                        self.scope.parentJoblist = response.data;
                        self.scope.parenjobsTotal = response.total_count;
                        self.scope.status_counts = response.status_count;
                    }
                }
            });
        }


        /*
        To get the user profile
        **/
        function viewprofile(userid) {
            var data = {};
            data['userid'] = userid;
            userService.viewProfile(data, function(response) {
                if (response.Status == "200") {
                    self.scope.personal = response.personal;
                    self.scope.personal.location = response.personal.city + ", " + response.personal.state + " " + response.personal.post_code;
                    self.scope.preference = response.preferences;
                    self.scope.availability = response.availability;
                    self.scope.requirements = response.requirements;
                    self.scope.emp_history = response.emp_history;
                    self.scope.others = response.other_details;
                    self.scope.emp_emergencycontact = response.emp_emergency;
                    self.scope.bankDetails = response.bank_details;
                    // self.scope.preference.caregivers = self.scope.preference.caregivers.join(' / ');
                    self.scope.preference.children_age = self.scope.preference.children_age.join(', ');
                    angular.forEach(self.scope.requirements, function(response) {
                        // console.log(response);
                        var req = response.requirement_id__category;
                        if (self.scope.availableCreds.indexOf(req) == -1) {
                            self.scope.availableCreds.push(req);
                        }
                    });
                }
            });
        }

        /*
        To check the time difference for deleting a message
        **/
        function checkDeleteTimer(msgTime){
            var messageTime = moment(msgTime);
            var timeNow = moment();
            var dif = timeNow.diff(messageTime,'minutes');
            if(dif <= 10){
                return true;
            }
            return false;

        }

        /*
        To update messages after 10 minutes
        **/
        $interval(function(){
            if(self.scope.messageMode == 'public' && self.scope.currentTabpreview == 'messages'){
                getMessages(self.scope.job.jobid, 'public');
            }
        },60000);


        /*
        To close a job by admin
        **/
        function closeJob(jobId,obj){
            var adminLogged = $cookieStore.get('adminLogged');
            var adminId = $cookieStore.get('adminid');
            if (adminLogged && adminId){
                var data = {};
                data['jobId'] = jobId;
                data['userid'] = adminId;
                var opt = {
                    templateUrl: 'scripts/module/job/templates/admin_closejob.html',
                    scope: $scope,
                    closeButton: false,
                    title: title,
                    buttons: {
                        Close: {
                            lable: "Cancel",
                            className: "btn btn-default loginbtn",
                            callback: function() {
                                return true
                            }
                        },
                        Ok:{
                            lable: "OK",
                            className: "btn btn-primary loginbtn",
                            callback: function() {
                                // return true
                                data['hideLoader'] = 1;
                                self.scope.generalLoader = true;
                                jobService.closeJob(data,function(response){
                                    self.scope.generalLoader = false;
                                    if(response.Status == "200"){
                                        obj.job_status = 7;
                                        toastr.success('Job closed',"Success");
                                    }
                                });
                            }
                        }
                    }
                };
            $ngBootbox.customDialog(opt);
            }
            else{
                toastr.error('Only admin can close a job!!');
                return false;
            }




        }

        /*
        To get the paypal token
        **/
        function  getPaypalToken(quote, jobId) {
            var paypalClientid = paypal.CLIENT_ID;
            $http({
                    url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Basic '+ paypalClientid,
                    },
                    data: 'grant_type=client_credentials',
                    uploadEventHandlers: {
                            progress: function (e) {
                                $ngBootbox.hideAll();
                                var opt = {
                                    templateUrl: 'scripts/module/job/templates/paymentmessage.html',
                                    scope: $scope,
                                    closeButton: false,
                                    title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                                    // buttons: {
                                    //     // "Ok": {
                                    //     //     callback: function() {
                                    //     //        return true
                                    //     //     }
                                    //     // }
                                    // }
                                };
                                $ngBootbox.customDialog(opt);
                            }
                    },
                })
            .then(function successCallback(result) {
                    if(result.status == 200){
                        self.scope.paypal.paymentTokentype = result.data.token_type;
                        self.scope.paypal.accessToken = result.data.access_token;
                        $cookieStore.put('paypalAccesstoken',self.scope.paypal.accessToken);
                        self.scope.paypal.appId = result.data.app_id;
                        makePayment(quote, jobId);
                    }

                }, function errorCallback(result) {
                    toastr.error(result.statusText,"Failed to get token");
                });
        }


        /*
        To make the paypal payment
        **/
        function makePayment(quote,jobId) {
            // var cancelUrl = $location.path();
            // var obj = {
            //     "intent": "sale",
            //     "redirect_urls":
            //     {
            //         "return_url": window.location.origin + "/#/successPayment/" + jobId + "/quotes",
            //         "cancel_url": window.location.origin + "#/previewjob/" + jobId + "/quotes"
            //     },
            //     "payer":
            //     {
            //         "payment_method": "paypal"
            //     },
            //     "transactions": [
            //     {
            //         "amount":
            //         {
            //             "total": "29.11",
            //             "currency": "USD",
            //             "details": {
            //               "subtotal": "30.00",
            //               "tax": "0.07",
            //               "shipping": "0.03",
            //               "handling_fee": "0.00",
            //               "shipping_discount": "-1.00",
            //               "insurance": "0.01"
            //             }
            //         },
            //         "description": "This is the payment transaction description.",
            //         "payment_options": {
            //             "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
            //         },
            //     }],
            //     "note_to_payer": "Contact us for any questions on your order.",
            //     "redirect_urls": {
            //       "return_url": "http://54.153.43.121:9000",
            //       "cancel_url": "http://54.153.43.121:9000"
            //     }
            // };
            var obj = {
              "intent": "sale",
              "redirect_urls": {
                "return_url": window.location.origin + "/#/successPayment/" + jobId + "/quotes",
                "cancel_url": window.location.origin + "#/previewjob/" + jobId + "/quotes"
              },
              "payer": {
                "payment_method": "paypal"
              },
              "transactions": [{
                "amount": {
                  "total": quote,
                  "currency": "AUD"
                }
              }]
            }
            $http({
                url: 'https://api.sandbox.paypal.com/v1/payments/payment',
                method: 'Post',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + self.scope.paypal.accessToken,
                },
                data: obj
            })
            .then(function successCallback(result) {
                    window.location.href = result.data.links[1].href;

            }, function errorCallback(result) {
                toastr.error(result.statusText,"Failed");
            });
        }

        /*
        Go to the worker profile
        **/
        function gotoWorkerprofile(quotedUser,jobid) {
            // body...
            $cookieStore.put('ClientQuotedView',true); 
            $cookieStore.put("previousJobid",jobid);
            $location.path('/viewcarer/'+quotedUser.userid__id+'-'+angular.lowercase(quotedUser.userid__first_name)+'-'+angular.lowercase(quotedUser.userid__last_name)) 
            // $state.go('viewworker',{"userid-":userid});
        }

        /*
        To execute the paypal payment
        **/
        function executePayment(payerId,paymentId){
            var accessToken = $cookieStore.get('paypalAccesstoken');
            // alert(accessToken);
            var quoteobj = $cookieStore.get('quoteObj');
            // console.log(quoteobj);
            var obj = {
                'payer_id':payerId
            };
            if (accessToken){
                $http({
                url: 'https://api.sandbox.paypal.com/v1/payments/payment/' + paymentId  + '/execute',
                method: 'Post',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + accessToken,
                    // 'PayPal-Mock-Response':'{"mock_application_codes":"INSUFFICIENT_FUNDS"}'
                },
                data: obj,
                }).then(function (data, status, headers, config) {
                    console.log(data);
                    if(data.data.state == "approved"){
                        // console.log(data);
                        saveSuccessPaymentStatus(data.data);
                        $cookieStore.remove('paypalAccesstoken');
                        paymentSuccess1(quoteobj);
                    }
                    else{
                        saveSuccessPaymentStatus(data.data);
                        showJob();
                    }
                }).catch(function (data) {
                    $cookieStore.remove('paypalAccesstoken');
                    saveErrorPaymentStatus(data.data, payerId);
                    // $state.go('previewjob',{'jobid':self.scope.jobId,"tabname":"quotes"});
                });

            }

        }

        /*
        To save the failure payment Status
        **/
        function saveErrorPaymentStatus(obj,payerId) {
            var data = {};
            data['status'] = obj.name;
            data['description'] = obj.message;
            data['payer_id'] = payerId;
            data['client_id'] = $cookieStore.get('userid');
            data['job_id'] = self.scope.jobId;

            jobService.savePaymentStatus(data,function(response){
                if(response.Status == "200"){
                    $cookieStore.remove('quoteObj');
                    // toastr.error(obj.message,obj.name);
                    var error = {type:"danger",msg:obj.message,name:obj.name}
                    self.scope.paymentAlerts.push(error);
                    showJob();
                }
            });

        }

        /*
        To save the success payment Status
        **/
        function saveSuccessPaymentStatus(obj) {
            // body...
            var data = {};
            data['status'] = obj.state;
            data['description'] = obj.message;
            data['payer_id'] = payerId;
            data['client_id'] = $cookieStore.get('userid');
            data['job_id'] = self.scope.jobId;

            jobService.savePaymentStatus(data,function(response){
                if(response.Status == "200"){
                    $cookieStore.remove('quoteObj');
                    // toastr.error(obj.message,obj.name);
                    var message = {type:"success"}
                    self.scope.paymentAlerts.push(message);
                    showJob();
                }
            });

        }

        /*
        To get the profile completeness and show suggesstions
        **/
        function showSuggestions(usertype){
            var login_count = $cookieStore.get('login_count');
            getCompleteness().then(function(value){
                var welcomeMsg = "Welcome!! <span class='welcome_name'>"+ self.scope.loggedUser + "</span><br>";
                var redirectUrl;
                var redirectParam;
                if(usertype == 'worker'){
                    if(login_count == 1){
                        if(value < Suggestion.ALERT_PERCENT){
                            redirectUrl = 'updateprofile';
                            redirectParam = {profiletab:'personal'};
                            self.scope.startMessage = welcomeMsg + Suggestion.PROFILE_FIRST+' get job opportunities';
                        }
                    }
                    else{

                        if(value < Suggestion.ALERT_PERCENT){
                            self.scope.startMessage = Suggestion.PROFILE_FIRST + ' get job opportunities';
                            redirectUrl = 'updateprofile';
                            redirectParam = {profiletab:'personal'};
                        }
                        else{
                            redirectUrl = undefined;
                            redirectParam = {};
                        }
                    }

                }
                else if(usertype == 'client'){
                    redirectUrl = undefined;
                    // if(login_count == 1){
                    //     // $cookieStore.put('login_count',undefined);
                    //     self.scope.startMessage = welcomeMsg + Suggestion.PROFILE_FIRST+' get valuable workers';
                    //     redirectUrl = 'parentprofile';
                    //     redirectParam = {};
                    // }
                    // else{
                    //     redirectUrl = undefined;
                    //     redirectParam = {};
                    // }

                }
                var opt = {
                    templateUrl: 'scripts/module/job/templates/startupMsg.html',
                    scope: $scope,
                    closeButton: false,
                    title: '<div class="col-md-12"><div class="logo_section"><img src="styles/images/logo.png" alt=""></div></div>',
                    buttons: {
                        // Skip: {
                        //     lable: "Skip",
                        //     className: "btn btn-default loginbtn text-center",
                        //     callback: function() {
                        //         return true
                        //     }
                        // },
                        "Update Profile": {
                            lable: "Update profile",
                            className: "btn btn-primary signupbtn text-center",
                            callback: function() {
                               $state.go(redirectUrl,redirectParam);
                            }
                        }
                    }
                };
                if(redirectUrl && !self.scope.adminLogged){
                    $ngBootbox.customDialog(opt);
                }

            });
            // console.log(completeness);


        }

        /*
        To close the suggestion
        **/
        self.scope.closeSuggestions = function(index) {
            self.scope.userSuggestions.splice(index, 1);
        };

      function getCompleteness(){
        var q = $q.defer();
        self.scope.userPercent = 0;
        var data = {};
        var percent;
        data['userid'] = $cookieStore.get('userid');
        data['hideLoader'] = 1;
        userService.getCompleteness(data,function(response){
            if(response){
                self.scope.userPercent = response.percentage;
                q.resolve(self.scope.userPercent);
            }
            else{
                q.reject('Error');
            }

        });
        return q.promise;
      }

      function jobBlocker(jobid,action){
        var data = {};
        data['jobid'] = jobid;
        data['action'] = action;
        jobService.jobBlock(data,function(response){
            if(response.Status == "200"){

            }
        });
      }

      var indexedReqs = [];

    self.scope.reqsToFilter = function() {
        indexedReqs = [];
        return self.scope.requirements;
    }

    self.scope.filterReqs = function(req) {
        var reqIsNew = indexedReqs.indexOf(req.category) == -1;
        if (reqIsNew) {
            indexedReqs.push(req.category);
        }
        return reqIsNew;
    }

    function getUserQuoteDetails(jobId){
        var data = {};
        data['jobId'] = jobId;
        data['workerid'] = $cookieStore.get('userid');
        jobService.getUserQuoteDetails(data,function(response){
            if(response.Status == "200"){
                 self.scope.userQuoteDetails = response.data;
            }
        }); 
        
    }

//       function add(x, y) {
//     let q = $q.defer();
//     $timeout(function () {
//         let result = x + y;
//         if (result < 0) {
//             q.reject("Negative error");
//         }
//         else
//             q.resolve(result);
//     }, 500);
//     return q.promise;
// };


    }
})();
