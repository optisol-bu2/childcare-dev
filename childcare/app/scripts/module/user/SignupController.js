/**
 * @ngdoc function
 * @name menowApp.user.controller:UserCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, for user functions
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('SignupCtrl', signupCtrl);
    signupCtrl.$inject = ['$scope', '$ngBootbox', '$cookieStore', 'jobService', 'userService', 'toastr', '$timeout', '$rootScope', '$state', '$document'];

    function signupCtrl($scope, $ngBootbox, $cookieStore, jobService, userService, toastr, $timeout, $rootScope, $state, $document) {
        var sm = this;
        sm.openSignup = openSignup;
        sm.user = {};
        sm.addEmail = addEmail;
        sm.addName = addName;
        sm.getCountrycodes = getCountrycodes;
        sm.selectCode = selectCode;
        sm.signupStep = 'step1';
        // sm.step1 = true;
        // sm.step2 = false;
        // sm.step3 = false;
        sm.countryCodes = [];
        // sm.user.purpose = 0;
        sm.emailunique = true;
        sm.signup = signup;
        sm.checkemail = checkemail;
        sm.from_postjob = false;
        // sm.user.profile_suits = 'profile';
        sm.user.selectedProfile = [];
        sm.triggerSubmit = triggerSubmit;
        sm.toggleCaregiver = toggleCaregiver;
        sm.callLoginpopup = callLoginpopup;
        sm.user.selectedProfile = [];
        sm.user.purpose = '0';
        sm.contactWorker = false;
        sm.contactMessage = {};
        sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / Educator', 'Cook'];
        // getCountrycodes();


        sm.selectedCode = {};
        // sm.selectedCode.flag_class= 'flag flag-au';
        // sm.selectedCode.code = '+61';

        if ($state.current.name == 'login') {

        }

        function triggerSubmit(form, id) {
            sm.user.purpose = id;
            // alert();
            addName(form);
        }
        // opens signup popup
        function openSignup() {
            $ngBootbox.hideAll();
            sm.signupStep = 'step1';
            // sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
            // getCountrycodes();
            sm.selectedCode = {};
            getCountrycodes();
            var options = {
                templateUrl: 'scripts/module/user/templates/signup.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                onEscape: callConfirmbox
            };
            $ngBootbox.customDialog(options);
            setTimeout(function() {
                angular.element('body').addClass("modal-open");
            }, 1000);
        }

        function callConfirmbox() {
            // you can do anything here you want when the user dismisses dialog
            //
            if (sm.user.hasOwnProperty('email') && sm.user.hasOwnProperty('password')) {
                var opt = {
                    templateUrl: 'scripts/module/user/templates/confirmbox.html',
                    scope: $scope,
                    closeButton: false,
                    title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                    buttons: {
                        cancel: {
                            lable: "cancel",
                            className: "btn btn-default loginbtn",
                            callback: function() {
                                openSignup();
                            }
                        },
                        success: {
                            label: "OK",
                            className: "btn btn-primary signupbtn",
                            callback: function() {
                                sm.user = {};
                                sm.signupStep = 'step1';
                                sm.user.purpose = '1';
                                sm.user.selectedProfile = [];
                                sm.selectedCode = {};
                                sm.user.terms = '0';
                                sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
                                return true;
                            }
                        }

                    }
                };
                $ngBootbox.customDialog(opt);
                setTimeout(function() {
                    angular.element('body').addClass("modal-open");
                }, 1000);
            }


        }

        function addEmail(form) {
            form.$setSubmitted();
            var data = {};
            if (sm.user.email) {
                data['email'] = sm.user.email;
                sm.checkemailLoad = true;
                userService.checkemail(data, function(response) {
                    sm.checkemailLoad = false;
                    if (response.Status == "500") {
                        sm.emailunique = false;
                        // sm.user.email = '';
                        // toastr.error(response.message);
                    } else if (response.Status == "200") {
                        sm.emailunique = true;
                        if (form.$valid && sm.emailunique) {
                            sm.signupStep = 'step2';
                        }
                    }
                });
            }

        }

        function addName(form) {
            // alert();
            // console.log(form);
            // sm.user.purpose = id;
            // alert();
            form.$setSubmitted();
            // console.log(sm.step3);
            if (form.$valid && sm.user.country_code) {
                // sm.user.purpose = 0;
                sm.signupStep = 'step3';
            } else {
                console.log(form);
            }
        }

        function signup(form) {
            form.$setSubmitted();
            // console.log(form);
            if (form.$valid && (sm.user.selectedProfile.length || sm.user.profile_suits)) {
                var data = sm.user;
                if ($cookieStore.get('jobTypeid')) {
                    data['jobtypeid'] = $cookieStore.get('jobTypeid');
                }
                data['hostName'] = window.location.origin;
                // console.log(data);
                // return false;
                userService.signup(data, function(response) {
                    if (response.Status == "200") {
                        // console.log(sm.user);
                        if (sm.user.purpose == "0" || sm.user.purpose == 'parent') {
                            if (sm.contactWorker) {
                                $cookieStore.put("contactMessage", sm.contactMessage);
                            }
                        }

                        $ngBootbox.hideAll();
                        $cookieStore.remove('jobTypeid');
                        var options = {
                            templateUrl: 'scripts/module/user/templates/signup_success.html',
                            scope: $scope,
                            title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                            onEscape: function() {
                                sm.user = {};
                                sm.signupStep = 'step1';
                                sm.user.purpose = '1';
                                sm.user.selectedProfile = [];
                                sm.selectedCode = {};
                                sm.users.terms = '0';
                                sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
                                return true;
                            },

                        };
                        options.buttons = {
                            warning: {
                                label: "Close",
                                className: "btn btn-default signupbtn",
                                callback: function() {
                                    sm.user = {};
                                    sm.signupStep = 'step1';
                                    sm.user.purpose = '1';
                                    sm.user.selectedProfile = [];
                                    sm.selectedCode = {};
                                    sm.users.terms = '0';
                                    sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
                                    return true;
                                }
                            },
                            success: {
                                label: "Login",
                                className: "btn btn-primary loginbtn",
                                callback: function() {
                                    sm.user = {};
                                    sm.signupStep = 'step1';
                                    sm.user.purpose = '1';
                                    sm.user.selectedProfile = [];
                                    sm.selectedCode = {};
                                    sm.users.terms = '0';
                                    sm.caregivers = ['Qualified childcare professional', 'Babysitter', 'Nanny', 'Au pair', 'Tutor / educator', 'Cook'];
                                    callLoginpopup();
                                }
                            },
                        };
                        $ngBootbox.customDialog(options);
                        // toastr.success('Signup successful!!!');

                    } else {
                        toastr.error(response.message);
                    }
                });
            }
        }

        function checkemail() {
            var data = {};
            data['email'] = sm.user.email;
            data['hideLoader'] = true;
            sm.emailunique = true;
            if (data['email']) {
                userService.checkemail(data, function(response) {
                    if (response.Status == "500") {
                        sm.emailunique = false;
                        // sm.user.email = '';
                        // toastr.error(response.message);
                    } else if (response.Status == "200") {
                        sm.emailunique = true;
                    }
                });
            }
        }

        function toggleCaregiver(option) {
            // alert();
            var idx = sm.user.selectedProfile.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                sm.user.selectedProfile.splice(idx, 1);
            }

            // Is newly selected
            else {
                sm.user.selectedProfile.push(option);
            }
            // console.log(sm.user.selectedProfile);
        }

        $scope.$on('callSignup', function(e, arg) {
            // alert('signup');
            sm.from_postjob = arg.postjob;
            if (arg.postjob)
                sm.user.purpose = '0'
            if (arg.hasOwnProperty('message')) {
                sm.contactWorker = true;
                sm.contactMessage = arg.message;
            }
            setTimeout(function() {
                openSignup();
            }, 500);

        });

        function callLoginpopup() {
            $rootScope.$broadcast('callLogin');
        }


        //To get the country codes
        function getCountrycodes() {
            userService.getcountrycodes(function(response) {
                if (response.Status == "200") {
                    sm.countryCodes = response.data;
                    angular.forEach(sm.countryCodes, function(cc, key) {
                        if (cc.id == 9) {
                            selectCode(cc, key);
                        }
                    });
                }
            });
        }

        //To select the country code
        function selectCode(obj, index) {
            var elementLists = angular.element(document.querySelectorAll('.country-codes li'));
            angular.forEach(elementLists, function(element) {
                angular.element(element).removeClass('selected');
            });
            sm.selectedCode = obj;
            // sm.personal.countrycode = obj.id;
            console.log(obj.id);
            sm.user.country_code = obj.id
            angular.element('.country-codes #' + index + 'option').addClass('selected');
        }


    }
})();