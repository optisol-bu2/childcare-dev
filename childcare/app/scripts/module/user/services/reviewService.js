/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
  angular
        .module('menowApp.auth')
		.service('reviewService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
    var postDomain = window.location.host;
    var postProtocol = window.location.protocol;
    return {
    	getReviews:function(data,callback){
            // data['userid'] = $cookieStore.get('userid');
    		$http({
                    url: __env.apiUrl + '/getReviews/',
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' }
                })
                .then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
    	},
    }
});