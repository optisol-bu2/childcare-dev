/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp.auth')
    .service('userService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
            signup: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/signup/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        // console.log(result);
                        // if(result.data.Status == "200"){
                        //     $cookieStore.put('jobTypeid',result.data.job_type_id);
                        // }
                        callback(result.data);

                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            checkemail: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                        url: __env.apiUrl + '/checkemail/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            updatepasword: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/changepassword/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            updateNewpasword: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/setnewpassword/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            viewProfile: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/viewprofile/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            uploadPic: function(data, callback) {
                var upload = Upload.upload({
                    url: __env.apiUrl + '/uploaddp/',
                    data: data,
                });
                upload.then(function(resp) {
                    callback(resp.data);
                    // file is uploaded successfully
                    console.log('file ' + resp.config.data.file + 'is uploaded successfully. Response: ' + resp.data);
                }, function(resp) {
                    // handle error
                }, function(evt) {
                    // progress notify
                    console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :' + evt.config.data.file);
                });
            },
            saveProfile: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/updateprofile/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            savePreference: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/updatepreference/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveAvailability: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/updateavailability/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getProofreqs: function(callback) {
                $http({
                        url: __env.apiUrl + '/get_proof_req/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveDocument: function(data, callback) {
                console.log(data);
                // data['hideLoader'] = true;
                var upload = Upload.upload({
                    url: __env.apiUrl + '/updatedocuments/',
                    data: data,
                });
                upload.then(function(resp) {
                    callback(resp.data);
                    // file is uploaded successfully
                }, function(resp) {
                    // handle error
                }, function(evt) {
                    // progress notify
                    var progress = parseInt(100.0 * evt.loaded / evt.total);
                    console.log(progress);
                    $rootScope.progress = progress;

                });
            },
            addHistory: function(data, callback) {
                var upload = Upload.upload({
                    url: __env.apiUrl + '/updatehistory/',
                    data: data,
                });
                upload.then(function(resp) {
                    callback(resp.data);
                    // file is uploaded successfully
                }, function(resp) {
                    // handle error
                }, function(evt) {
                    // progress notify

                });
            },
            getEmployeehistory: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getemphistory/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getLanguages: function(callback) {
                $http({
                        url: __env.apiUrl + '/getlanguages/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveOthers: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/saveothers/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getReferences: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getreferences/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveReference: function(data, callback) {
                var upload = Upload.upload({
                    url: __env.apiUrl + '/savereference/',
                    data: data,
                });
                upload.then(function(resp) {
                    callback(resp.data);
                    // file is uploaded successfully
                }, function(resp) {
                    // handle error
                }, function(evt) {
                    // progress notify

                });
            },
            getCompleteness: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getcompleteness/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteDocument: function(data, callback) {
                data['hideLoader'] = true;
                $http({
                        url: __env.apiUrl + '/removedocument/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            saveEmergency: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/saveemergencydetail/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getEmergencycontacts: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getemergencycontacts/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            uploadresume: function(data, callback) {
                var upload = Upload.upload({
                    url: __env.apiUrl + '/uploadresume/',
                    data: data,
                });
                upload.then(function(resp) {
                    callback(resp.data);
                    // file is uploaded successfully
                }, function(resp) {
                    // handle error
                }, function(evt) {
                    // progress notify

                });
            },
            getcountrycodes: function(callback) {
                $http({
                        url: __env.apiUrl + '/getcountrycodes/',
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            savebankDetails: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/savebankdetail/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getclientProfile: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/getclientprofile/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deletehistory: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletehistory/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deletereference: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deletereference/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteContact: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deleteemergencycontact/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deleteresume: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deleteresume/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            deletereferenceDoc: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/deleterefdoc/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            blockUser: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/userBlock/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            vettedUser: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/make_vetted/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
        }

    });