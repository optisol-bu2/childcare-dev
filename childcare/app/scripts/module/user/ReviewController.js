/**
 * @ngdoc function
 * @name menowApp.user.controller:UserCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, for user functions
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('ReviewCtrl', ReviewCtrl);
    ReviewCtrl.$inject = ['$scope', 'Auth', '$state', 'reviewService', '$filter', '$cookieStore', 'toastr', '$ngBootbox', 'Upload', 'jobService', '$window', '$rootScope', '$q', '$location', 'carerService'];

    function ReviewCtrl($scope, Auth, $state, reviewService, $filter, $cookieStore, toastr, $ngBootbox, Upload, jobService, $window, $rootScope, $q, $location, carerService) {
        var self = this;
        self.scope = $scope;
        self.scope.reviewCalled = false;
        self.scope.limit = 3;
        self.scope.pageno = 1;
        self.scope.loadPages = loadPages;
        self.scope.backViewUrl = backViewUrl;
        self.scope.currentName = $state.current.name;
        if (self.scope.currentName == 'reviews') {
            self.scope.jobId = $state.params.jobId
            getReviews()
        }

        function getReviews() {
            var data = {};
            data['limit'] = self.scope.limit;
            data['pageno'] = self.scope.pageno;
            data['userid'] = $state.params.userid;
            reviewService.getReviews(data, function(response) {
                if (response.Status == "200") {
                    self.scope.reviews = response.reviews;
                    self.scope.total_general = response.total_count;
                    self.scope.overall_reviews = response.overall_reviews.toFixed(1);
                } else {
                    self.scope.overall_reviews = 0
                    self.scope.noReviews = true;
                }
            });

        }

        function loadPages() {
            console.log(self.scope.total_general / self.scope.limit, self.scope.pageno)
            if (self.scope.total_general) {
                if (self.scope.total_general / self.scope.limit > self.scope.pageno) {
                    // alert();
                    self.scope.reviewCalled = true;
                    self.scope.pageno += 1;
                    append_reviews(self.scope.pageno);
                }
            }
        }

        function append_reviews(pageno) {
            // alert(self.scope.total_general);
            if (pageno) {
                var data = {
                    "pageno": pageno,
                    "limit": self.scope.limit,
                    "userid": $state.params.userid
                };
                self.scope.isLoading = true;
                reviewService.getReviews(data, function(response) {
                    self.scope.reviewCalled = false;
                    self.scope.isLoading = false;
                    if (response.Status == "200") {
                        if (response.reviews.length > 0) {
                            self.scope.reviews = self.scope.reviews.concat(response.reviews);

                        }
                        self.scope.total_general = response.total_count;
                    }
                });
            }
        }

        function backViewUrl() {
            window.history.back()
        }




    };
})();