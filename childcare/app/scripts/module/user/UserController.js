/**
 * @ngdoc function
 * @name menowApp.user.controller:UserCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, for user functions
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('UserCtrl', userCtrl);
    userCtrl.$inject = ['$scope', 'Auth', '$state', 'userService', '$filter', '$cookieStore', 'toastr', '$ngBootbox', 'Upload', 'jobService', '$window', '$rootScope', '$q', '$location', 'carerService'];

    function userCtrl($scope, Auth, $state, userService, $filter, $cookieStore, toastr, $ngBootbox, Upload, jobService, $window, $rootScope, $q, $location, carerService) {
        var self = this;
        self.scope = $scope;
        // console.log($state)
        //function
        self.scope.updatePassword = updatePassword;
        self.scope.resetPassword = resetPassword;
        self.scope.updateNewpassword = updateNewpassword;
        self.scope.updateDP = updateDP;
        self.scope.saveProfile = saveProfile;
        self.scope.selectLocation = selectLocation;
        self.scope.savePref = savePref;
        self.scope.togglecaregiver = togglecaregiver;
        self.scope.toggleAge = toggleAge;
        self.scope.toggleBeforesch = toggleBeforesch;
        self.scope.toggleAftersch = toggleAftersch;
        self.scope.toggleDaycare = toggleDaycare;
        self.scope.toggleOvernight = toggleOvernight;
        self.scope.saveAvailability = saveAvailability;
        self.scope.uploadProof = uploadProof;
        self.scope.addExperience = addExperience;
        self.scope.saveOthers = saveOthers;
        self.scope.saveHistory = saveHistory;
        self.scope.switchTab = switchTab;
        self.scope.proofUpload = proofUpload;
        self.scope.editHistory = editHistory;
        self.scope.updateHistory = updateHistory;
        self.scope.cancelHistory = cancelHistory;
        self.scope.toggleChores = toggleChores;
        self.scope.toggleLanguages = toggleLanguages;
        self.scope.calculateAge = calculateAge;
        self.scope.openReference = openReference;
        self.scope.saveReference = saveReference;
        self.scope.cancelReference = cancelReference;
        self.scope.refDocumentupload = refDocumentupload;
        self.scope.saveDoc = saveDoc;
        self.scope.toggleJobtype = toggleJobtype;
        self.scope.openEcontact = openEcontact;
        self.scope.saveEmergencycontact = saveEmergencycontact;
        self.scope.cancelEmergency = cancelEmergency;
        self.scope.saveResume = saveResume;
        self.scope.selectCode = selectCode;
        self.scope.saveBankdetails = saveBankdetails;
        self.scope.updateClientdata = updateClientdata;
        self.scope.getLocations = getLocations;
        self.scope.getToyear = getToyear;
        self.scope.removeCookie = removeCookie;
        self.scope.setTag = setTag;
        self.scope.cancelChangepwd = cancelChangepwd;
        self.scope.setHistorylocation = setHistorylocation;
        self.scope.deleteHistory = deleteHistory;
        self.scope.deleteReference = deleteReference;
        self.scope.deleteEmerContact = deleteEmerContact;
        self.scope.clientDatavalidation = clientDatavalidation;
        self.scope.removeDoc = removeDoc;
        self.scope.deleteReferenceDoc = deleteReferenceDoc;
        self.scope.removerefFile = removerefFile;
        self.scope.goBack = goBack;

        self.scope.changePassword = {};
        self.scope.passmatch = false;
        self.scope.wrngOldpwd = false;
        self.scope.currentState = $state.current.name;
        self.scope.userid = $cookieStore.get('userid');
        self.scope.personal = {};
        self.scope.personal.year_of_birth = 0;
        self.scope.personalForm = {};
        self.scope.preference = {};
        self.scope.preference.job_type = [];
        self.scope.preference.caregivers = [];
        self.scope.preference.children_age = [];
        self.scope.availability = {};
        self.scope.availability.before_school = [];
        self.scope.availability.day_care = [];
        self.scope.availability.after_school = [];
        self.scope.availability.overnight = [];
        self.scope.caregiversType = ['Qualified childcare professional',
            'Babysitter', 'Nanny', 'Au pair', 'Tutor / Educator', 'Cook'
        ];
        self.scope.childrenAge = ['Up to 12 months',
            '1-3 years', '4-6 years', '7-11 years', '12+ years'
        ];
        self.scope.Days = ['0', '1', '2', '3', '4', '5', '6'];
        self.scope.updatedDoc = [];
        self.scope.currentTab = 'personal';
        self.scope.history = {};
        self.scope.history.currentlyWorking = 0;
        self.scope.history.headlineSwitch = 0;
        self.scope.months = [{
                "name": "January",
                "value": "1"
            }, {
                "name": "Febrauray",
                "value": "2"
            },
            {
                "name": "March",
                "value": "3"
            }, {
                "name": "April",
                "value": "4"
            }, {
                "name": "May",
                "value": "5"
            },
            {
                "name": "June",
                "value": "6"
            }, {
                "name": "July",
                "value": "7"
            }, {
                "name": "August",
                "value": "8"
            },
            {
                "name": "September",
                "value": "9"
            }, {
                "name": "October",
                "value": "10"
            }, {
                "name": "November",
                "value": "11"
            },
            {
                "name": "December",
                "value": "12"
            }
        ];
        self.scope.proof_file = null;
        self.scope.years = [];
        self.scope.others = {};
        // $scope.$apply(function() {
        self.scope.others.household_chores = [];
        self.scope.others.languages = [];
        self.scope.availableCreds = [];
        self.scope.reference = {};
        self.scope.emergency = {};
        self.scope.croppingImage = false;
        self.scope.resumeFile = {};
        self.scope.selectedCode = {};
        self.scope.bankDetails = {};
        self.scope.clientUser = {};
        self.scope.file = {};
        self.scope.locLoading = false;
        self.scope.adminLogged = $cookieStore.get('adminLogged');
        self.scope.adminUrl = $cookieStore.get('adminUrl');
        self.scope.userType = $cookieStore.get('usertype');
        self.scope.userMode = $state.params.userMode;
        self.scope.getTomonths = getTomonths;
        self.scope.backViewUrl = backViewUrl;
        self.scope.BlockUser = BlockUser;
        // self.scope.blocked_user = '0';
        self.scope.VettedUser = VettedUser;
        self.scope.changeStatus = changeStatus;
        console.log(self.scope.userMode)
        // });
        var today = new Date();
        var i;
        for (i = 1960; i <= today.getFullYear(); i++) {
            self.scope.years.push(i);
        }
        // For form validation
        /*
            To validate Personal form
        **/
        self.scope.personalOptions = {
            rules: {
                first_name: {
                    required: true,

                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name",

                },
                last_name: {
                    required: "Please enter last name",
                },
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            }
        }
        if ($cookieStore.get('name')) {
            self.scope.logged_in = true;
        }
        if (self.scope.currentState == 'myprofile') {
            var userid = $cookieStore.get('userid');
            viewprofile(userid);
            getProfilecompletess(userid);
            getReferences();
        }
        if (self.scope.currentState == "parentprofile") {
            var userid = $cookieStore.get('userid');
            getclientProfile(userid);
            getCountrycodes();
        }
        if (self.scope.currentState == 'updateprofile') {
            var userid = $cookieStore.get('userid');
            if ($state.params.profiletab) {
                var tab = $state.params.profiletab;
                switchTab(tab, userid);
            }
            // getProfiledetails(userid);
            // getProofreqs();
            getlanguages();
            getReferences();
            getProfilecompletess(userid);
            getCountrycodes();
        }

        /**
         * Reset user password
         */
        // function updatePassword(formArray) {
        //     if (formArray.$valid) {
        //         User.changePassword(self.scope.changePassword, function(response) {
        //             if (response.success) {
        //                 self.scope.successChangePassword = response.returnArr;
        //                 $timeout(function() {
        //                     self.scope.successChangePassword = null
        //                 }, 10000);
        //                 self.scope.failChangePassword = null;
        //                 self.scope.changePassword = {};
        //                 self.scope.passwordsubmit = false;
        //                 $("passwordError").hide();
        //             } else {
        //                 self.scope.successChangePassword = null;
        //                 self.scope.failChangePassword = response.returnArr;
        //                 $timeout(function() {
        //                     self.scope.failChangePassword = null
        //                 }, 10000);
        //             }
        //         });
        //     }
        // };

        function updatePassword(form) {
            form.$setSubmitted();
            var data = self.scope.changePassword;
            if (self.scope.userMode == 'admin') {
                var userid = $cookieStore.get('adminid');
            } else {
                var userid = $cookieStore.get('userid');
            }
            data['userid'] = userid;
            if (data['password'] != data['confirmPassword']) {
                self.scope.passmatch = true;
            }
            if (form.$valid) {
                if (data['password'] == data['confirmPassword']) {
                    self.scope.passmatch = false;
                    self.scope.wrngOldpwd = false;
                    userService.updatepasword(data, function(response) {
                        $window.scrollTo(0, 0);
                        if (response.Status == "200") {
                            toastr.success("Password has been changed successfully!!");
                            form.$setPristine();
                            self.scope.changePassword = {};
                        } else if (response.Status == "404") {
                            self.scope.wrngOldpwd = true;
                            // self.scope.changePassword.oldpassword = '';
                        }
                    });
                } else {
                    self.scope.passmatch = true;
                }

            }
        }

        function resetPassword(form) {
            // console.log(form);
            form.$setPristine();
            self.scope.changePassword = {};
        }

        function updateNewpassword(form) {
            form.$setSubmitted();
            var data = self.scope.changePassword;
            var userid = $cookieStore.get('userid');
            data['userid'] = userid;
            if (data['password'] != data['confirmPassword']) {
                self.scope.passmatch = true;
            }
            if (form.$valid) {
                if (data['password'] == data['confirmPassword']) {
                    self.scope.passmatch = false;
                    self.scope.wrngOldpwd = false;
                    userService.updateNewpasword(data, function(response) {
                        if (response.Status == "200") {
                            toastr.success("Password changed!!");
                            $state.go('login');
                            $rootScope.$broadcast('callloginPopup');
                            form.$setPristine();
                            self.scope.changePassword = {};
                        } else if (response.Status == "404") {
                            toastr.error(response.message);
                        }
                    });
                } else {
                    self.scope.passmatch = true;
                }

            }
        }


        /*
        To get the user profile
        **/
        function viewprofile(userid) {
            var data = {};
            data['userid'] = userid;
            userService.viewProfile(data, function(response) {
                if (response.Status == "200") {
                    self.scope.personal = response.personal;
                    self.scope.personal.fullAddress = response.personal.city + ", " + response.personal.state + " " + response.personal.post_code;
                    self.scope.preference = response.preferences;
                    self.scope.availability = response.availability;
                    self.scope.requirements = response.requirements;
                    self.scope.emp_history = response.emp_history;
                    self.scope.others = response.other_details;
                    self.scope.resumeFile = response.other_details.resume;
                    self.scope.emp_emergencycontact = response.emp_emergency;
                    self.scope.bankDetails = response.bank_details;
                    self.scope.blocked_user = response.personal.is_blocked;
                    self.scope.vetted_user = response.personal.is_vetted;
                    // self.scope.preference.caregivers = self.scope.preference.caregivers.join(' / ');
                    self.scope.preference.children_age = self.scope.preference.children_age.join(', ');
                    angular.forEach(self.scope.requirements, function(response) {
                        // console.log(response);
                        var req = response.requirement_id__category;
                        if (self.scope.availableCreds.indexOf(req) == -1) {
                            self.scope.availableCreds.push(req);
                        }
                    });
                }
            });
        }

        self.scope.myImage = "";
        self.scope.myCroppedImage = null;

        angular.element(document.querySelector('#fileInput')).on('change', updateDP);

        /*
        To update display picture
        **/
        function updateDP(evt) {
            var userid = $cookieStore.get('userid');
            self.scope.croppingImage = true;
            console.log(evt.currentTarget.files);
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    self.scope.myImage = evt.target.result;
                });
            };
            reader.readAsDataURL(file);
            var opt = {
                templateUrl: 'scripts/module/user/templates/cropimage.html',
                scope: $scope,
                closeButton: true,
                size: 'md',
                onEscape: function() {
                    self.scope.$apply(function() {
                        self.scope.croppingImage = false;
                        self.scope.myCroppedImage = null;
                        document.getElementById("fileInput").value = "";
                    });
                    return true;
                },
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    cancel: {
                        lable: "cancel",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            self.scope.$apply(function() {
                                self.scope.croppingImage = false;
                                self.scope.myCroppedImage = null;
                                document.getElementById("fileInput").value = "";
                            });
                        }
                    },
                    success: {
                        label: "OK",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            self.scope.croppingImage = true;
                            var data = {};
                            data.userid = $cookieStore.get('userid');
                            data.profpic = self.scope.myCroppedImage;
                            data['hideLoader'] = 1;
                            self.scope.generalLoader = true;
                            userService.uploadPic(data, function(response) {
                                self.scope.generalLoader = false;
                                if (response.Status == "200") {
                                    $cookieStore.put('user_profpic', response.file);
                                    $rootScope.$broadcast('profilepicUpdate', {
                                        pic: response.file
                                    });
                                    toastr.success("Profile picture uploaded");
                                    document.getElementById("fileInput").value = "";
                                    getProfilecompletess(userid);
                                }
                            });

                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);
            // setTimeout(function(){
            //     angular.element('canvas').css({"cursor":"move"});
            //     console.log(angular.element('canvas').triggerHandler('click'));
            // },2000);
            // angular.element('canvas').click();


        }

        /*
        To save the personal details
        **/
        function saveProfile(form, tab) {
            // alert();
            console.log(form);
            form.$setSubmitted();
            form.contactno.$invalid = false;
            // console.log();
            // if (form.$error.ngIntlTelInput) {
            //     form.$setPristine();
            //     form.contactno.$error = {};
            //     form.$valid = true;
            // }
            if (!self.scope.personal.post_code) {
                return false;
            }
            if (form.$valid) {
                var data = {};
                data['hideLoader'] = true;
                data['userid'] = $cookieStore.get('userid');
                if ($cookieStore.get('admin_id')) {
                    data['updated_by'] = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data['updated_by'] = $cookieStore.get('client_id')
                } else {
                    data['updated_by'] = $cookieStore.get('userid')
                }
                Object.assign(data, self.scope.personal);
                userService.saveProfile(data, function(response) {
                    if (response.Status == "200") {
                        $cookieStore.put('name', self.scope.personal.first_name + " " + self.scope.personal.last_name);
                        $rootScope.$broadcast('updateUsername');
                        getProfilecompletess(userid);
                        switchTab(tab);
                    }
                });
            }
        }

        /*
         To get User details
        **/
        function getProfiledetails(userid) {
            var data = {};
            data['hideLoader'] = true;
            data['userid'] = userid;
            userService.viewProfile(data, function(response) {
                if (response.Status == "200") {
                    self.scope.personal = response.personal;
                    self.scope.selectedCode = response.personal.selected_code;
                    if (self.scope.personal.contactno) {
                        var trimContact = self.scope.personal.contactno;
                        trimContact = trimContact.replace(/^(\+\d{0,2})/, "");
                        // alert(trimContact);
                        // setTimeout(function(){
                        //     self.scope.$apply(function(){
                        //         self.scope.personal.contactno = trimContact;
                        //     });

                        // },500);

                    }
                    if (response.personal.city && response.personal.state && response.personal.post_code)
                        self.scope.personal.fullAddress = response.personal.city + ", " + response.personal.state + " " + response.personal.post_code;
                    self.scope.preference = response.preferences;
                    self.scope.availability = response.availability;
                    self.scope.requirements = response.requirements;
                    self.scope.emp_history = response.emp_history;
                    self.scope.others = response.other_details;
                    self.scope.bankDetails = response.bank_details;

                    if (self.scope.others.languages.indexOf('Others') > -1) {
                        var lang = self.scope.others.languages;
                        var languages = [];
                        angular.forEach(self.scope.languages, function(data) {
                            languages.push(data.language);
                        });
                        var val = $filter('intersect')(lang, languages);
                        self.scope.others.otherLanguage = val[0];
                    }
                    self.scope.emp_emergencycontact = response.emp_emergency;
                    self.scope.bankDetails = response.bank_details;
                    self.scope.updatedDoc = [];
                    // angular.forEach(self.scope.requirements, function(req) {
                    //     self.scope.updatedDoc.push(req);
                    // });
                    // setTimeout(function(){
                    angular.forEach(self.scope.reqs, function(req, key) {
                        angular.forEach(self.scope.requirements, function(obj, i) {
                            // self.scope.reqs[key].has_req = false;
                            if (req.id === obj.requirement_id__id) {
                                self.scope.reqs[key].document_name = obj.document_name;
                                self.scope.reqs[key]['has_req'] = true;
                            }
                        });
                    });
                    // },200);


                }
                self.scope.generalLoader = false;
            });
        }
        /*
        To check the id is in user documents objects
        */
        self.scope.haveRequirement = haveRequirement;

        function haveRequirement(reqid) {
            // var id = self.scope.updatedDoc.some(function(obj, i) {
            //     // return obj.Country === 'Austria' ? index = i : false;
            //     console.log(obj.requirement_id__id+ "Req id");
            //     console.log(reqid+ "user Req id");
            //     return obj.requirement_id__id == reqid ? true:false;
            // });
            // console.log(id);
            // return id;
        }

        /*
        To get the locations
        **/
        $scope.$on('locationFilter', function(event, arg) {
            self.scope.locselected = false;
            self.scope.locations = arg.filter_result;
        });

        function selectLocation(obj) {
            // hm.joblocation.
            self.scope.personal.location = obj.suburb + ", " + obj.state + " " + obj.postcode;
            self.scope.personal.city = obj.suburb;
            self.scope.personal.state = obj.state;
            self.scope.personal.post_code = obj.postcode;
            self.scope.locselected = true;
            self.scope.history.location = obj.suburb + ", " + obj.state + " " + obj.postcode;
        }

        /*
         To update the user's preferences
        **/
        function savePref(form, tab) {
            form.$setSubmitted();
            if (form.$valid) {
                var data = angular.copy(self.scope.preference);
                data['hideLoader'] = true;
                data['userid'] = $cookieStore.get('userid');
                if ($cookieStore.get('admin_id')) {
                    data['updated_by'] = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data['updated_by'] = $cookieStore.get('client_id')
                } else {
                    data['updated_by'] = $cookieStore.get('userid')
                }
                // Object.assign(data,self.scope.preference);
                userService.savePreference(data, function(response) {
                    if (response.Status == "200") {
                        getProfilecompletess(userid);
                        switchTab(tab);
                    }
                });
            }
        }

        function togglecaregiver(option) {
            // alert();
            var idx = self.scope.preference.caregivers.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.preference.caregivers.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.preference.caregivers.push(option);
            }

        }

        function toggleAge(option) {
            var idx = self.scope.preference.children_age.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.preference.children_age.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.preference.children_age.push(option);
            }
        }

        function toggleBeforesch(option) {
            var idx = self.scope.availability.before_school.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.availability.before_school.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.availability.before_school.push(option);
            }
        }

        function toggleAftersch(option) {
            var idx = self.scope.availability.after_school.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.availability.after_school.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.availability.after_school.push(option);
            }
        }

        function toggleDaycare(option) {
            var idx = self.scope.availability.day_care.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.availability.day_care.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.availability.day_care.push(option);
            }
        }

        function toggleOvernight(option) {
            var idx = self.scope.availability.overnight.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.availability.overnight.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.availability.overnight.push(option);
            }
        }

        /*
         TO SAVE THE USER AVAILABILITY
        **/

        function saveAvailability(form, tab) {
            form.$setSubmitted();
            if (form.$valid) {
                var data = {};
                data['hideLoader'] = true;
                data['userid'] = $cookieStore.get('userid');
                Object.assign(data, self.scope.availability);
                data['before_school'] = data['before_school'].toString();
                data['day_care'] = data['day_care'].toString();
                data['after_school'] = data['after_school'].toString();
                data['overnight'] = data['overnight'].toString();
                if ($cookieStore.get('adminid')) {
                    data['updated_by'] = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data['updated_by'] = $cookieStore.get('client_id')
                } else {
                    data['updated_by'] = $cookieStore.get('userid')
                }
                userService.saveAvailability(data, function(response) {
                    if (response.Status == "200") {
                        getProfilecompletess(userid);
                        switchTab(tab);
                    }
                });
            }
        }

        /*
        To get the requirements that need proof
        **/

        function getProofreqs() {
            userService.getProofreqs(function(response) {
                if (response.Status == "200") {
                    self.scope.reqs = response.reqs;
                }
            });
        }

        /*
         To upload the requirement
        **/
        var filetypes = ['png', 'pdf', 'doc', 'docx', 'jpeg', 'jpg', 'svg'];

        function uploadProof(reqId, req, index) {
            // console.log(self.scope.updatedDoc);
            if (req.has_req) {
                removeDoc($cookieStore.get('userid'), req, index)
            } else {
                if (reqId) {
                    var data = {};
                    data['hideLoader'] = true;
                    data.userid = $cookieStore.get('userid');
                    data.requirement_id = reqId;
                    if ($cookieStore.get('admin_id')) {
                        data.updated_by = $cookieStore.get('adminid')
                    } else if ($cookieStore.get('client_id')) {
                        data.updated_by = $cookieStore.get('client_id')
                    } else {
                        data.updated_by = $cookieStore.get('userid')
                    }

                    var upload = Upload.upload({
                        url: __env.apiUrl + '/updatedocuments/',
                        data: data,
                    });
                    upload.then(function(resp) {
                        // callback(resp.data);
                        // self.scope.updatedDoc.push(reqId);
                        req.has_req = true;
                        getProfilecompletess(userid);
                        // toastr.success('Requirement Added', 'Success');
                        // file is uploaded successfully
                    }, function(resp) {
                        // handle error
                    }, function(evt) {
                        // progress notify
                        var progress = parseInt(100.0 * evt.loaded / evt.total);

                    });
                }
            }

        }

        function saveDoc(doc, req) {
            // console.log(self.scope.updatedDoc);
            // console.log(req);
            var data = {};
            if (doc && req.id) {
                var doc_name = doc.name;
                // alert(doc_name);
                var extn = doc_name.split(".").pop();
                // alert(extn.indexOf(filetypes));
                if (filetypes.indexOf(extn) !== -1) {
                    data.document = doc;
                    data.userid = $cookieStore.get('userid')
                    data.requirement_id = req.id;
                    data.hideLoader = true;
                    if ($cookieStore.get('admin_id')) {
                        data.updated_by = $cookieStore.get('adminid')
                    } else if ($cookieStore.get('client_id')) {
                        data.updated_by = $cookieStore.get('client_id')
                    } else {
                        data.updated_by = $cookieStore.get('userid')
                    }
                    // console.log(data)
                    // userService.saveDocument(data, function(response) {
                    //     if (response.Status == "200") {
                    //         self.scope.updatedDoc.push(reqId);
                    //         getProfilecompletess(userid);
                    //         toastr.success('Requirement Added', 'Success');
                    //     }
                    // });
                    var upload = Upload.upload({
                        url: __env.apiUrl + '/updatedocuments/',
                        data: data,
                    });
                    req.uploading = true;
                    upload.then(function(resp) {
                        // callback(resp.data);
                        // self.scope.updatedDoc.push(req.id);
                        req['has_req'] = true;
                        if (resp.data.hasOwnProperty('doc_name')) {
                            req['document_name'] = resp.data.doc_name;
                        }
                        req.uploading = false;
                        getProfilecompletess(userid);
                        // toastr.success('Requirement Added', 'Success');
                        // file is uploaded successfully
                    }, function(resp) {
                        // handle error
                    }, function(evt) {
                        // progress notify
                        var progress = parseInt(100.0 * evt.loaded / evt.total);
                        req.progress = progress;

                    });

                } else {
                    toastr.error('invalid file type, Acceptable files are ' + filetypes.toString(), 'Extension error');
                }
            }

        }

        function removeDoc(userid, req, index) {
            var opt = {
                templateUrl: 'scripts/module/user/templates/docDelete.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            // console.log(index);
                            angular.element("#" + index).attr("checked", 'true');
                            req.has_req = true;
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            var data = {};
                            data['userid'] = userid;
                            data['requirement_id'] = req.id;
                            data['hideLoader'] = true;
                            if ($cookieStore.get('admin_id')) {
                                data.updated_by = $cookieStore.get('adminid')
                            } else if ($cookieStore.get('client_id')) {
                                data.updated_by = $cookieStore.get('client_id')
                            } else {
                                data.updated_by = $cookieStore.get('userid')
                            }
                            data['hideLoader'] = 1
                            self.scope.generalLoader = true;
                            userService.deleteDocument(data, function(response) {
                                self.scope.generalLoader = false;

                                if (response.Status == '200') {
                                    req.has_req = false;
                                    req.document_name = false;
                                    getProfilecompletess(userid);
                                }
                            });
                        }
                    }

                }
            };
            if (req.document_name) {
                $ngBootbox.customDialog(opt);
            } else {
                var data = {};
                data['userid'] = userid;
                data['requirement_id'] = req.id;
                data['hideLoader'] = true;
                if ($cookieStore.get('admin_id')) {
                    data.updated_by = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data.updated_by = $cookieStore.get('client_id')
                } else {
                    data.updated_by = $cookieStore.get('userid')
                }
                userService.deleteDocument(data, function(response) {
                    if (response.Status == '200') {
                        req.has_req = false;
                        req.document_name = false;
                        getProfilecompletess(userid);
                    }
                });
            }



        }

        function addExperience() {
            var opt = {
                templateUrl: 'scripts/module/user/templates/experience_history.html',
                scope: $scope,
                closeButton: true,
                className: 'experience-class',
                size: 'md',
                onEscape: function() {
                    self.scope.history = {};
                    self.scope.history.currentlyWorking = 0;
                    self.scope.history.headlineSwitch = 0;
                },
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                // buttons: {
                //     cancel:{
                //         lable: "cancel",
                //         className:"btn btn-default loginbtn",
                //         callback:function(){
                //         }
                //     },
                //     success: {
                //         label: "OK",
                //         className: "btn btn-primary signupbtn",
                //         callback: function() {
                //         }
                //     }
                //
                // }
            };
            $ngBootbox.customDialog(opt);
            setTimeout(function() {
                angular.element('.experience-class').focus();
            }, 800);

        }

        /* TO
        SAVE OTHERS FORM
        **/
        function saveOthers(form, tab) {
            form.$setSubmitted();
            if (form.$valid) {
                var data = {};
                data = angular.copy(self.scope.others);
                data['hideLoader'] = true;
                data.userid = $cookieStore.get('userid');
                if ($cookieStore.get('admin_id')) {
                    data['updated_by'] = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data['updated_by'] = $cookieStore.get('client_id')
                } else {
                    data['updated_by'] = $cookieStore.get('userid')
                }
                userService.saveOthers(data, function(response) {
                    if (response.Status == "200") {
                        getProfilecompletess(userid);
                        switchTab(tab);
                    }
                });
            }

        }
        /* TO
        SAVE Bank details FORM
        **/
        function saveBankdetails(form, tab) {
            form.$setSubmitted();
            if (form.$valid) {
                var data = {};
                data = angular.copy(self.scope.bankDetails);
                data['hideLoader'] = true;
                data.userid = $cookieStore.get('userid');
                userService.savebankDetails(data, function(response) {
                    if (response.Status == "200") {
                        uploadResume();
                        getProfilecompletess(userid);
                        switchTab(tab);
                    }
                });
            }

        }

        /*
        TO SAVE THE HISTORY AND SWITCH TAB
        **/
        self.scope.historySaving = false;

        function saveHistory(form, tab) {
            form.$setSubmitted();
            var userid = $cookieStore.get('userid');
            self.scope.historySaving = true;
            if (form.$valid) {
                var data = angular.copy(self.scope.history);
                // Object.assign(data,self.scope.history.location);
                data.userid = userid;
                data.proof_file = self.scope.proof_file;
                data['hideLoader'] = true;
                if ($cookieStore.get('admin_id')) {
                    data.updated_by = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data.updated_by = $cookieStore.get('client_id')
                } else {
                    data.updated_by = $cookieStore.get('userid')
                }
                userService.addHistory(data, function(response) {
                    self.scope.historySaving = false;
                    if (response.Status == "200") {
                        $ngBootbox.hideAll();
                        setTimeout(function() {
                            self.scope.history = {};
                            self.scope.history.currentlyWorking = 0;
                            self.scope.history.headlineSwitch = 0;
                        }, 200);

                        // toastr.success("Employment history added!!");
                        // self.scope.emp_history.push(self.scope.history);
                        getEmployeehistory();
                        getProfilecompletess(userid);
                    }
                });
            } else {
                self.scope.historySaving = false;
            }
            // switchTab(tab);
        }

        /*
        TO SWITCH TABS
        **/

        function switchTab(tab) {
            self.scope.generalLoader = true;
            getProofreqs();
            getProfiledetails(userid);
            self.scope.currentTab = tab;
            angular.element('#' + tab).show();
            // angular.element('#'+tab).trigger('click');
            window.scrollTo(0, 0);
            if (tab == 0) {

                $state.go('myprofile');
            }
            self.scope.generalLoader = false;
        }


        function proofUpload(file) {
            var rand = getRandomInt(0, 9999);
            var extn = file.name.split(".").pop();
            var userid = $cookieStore.get('userid');
            var filename = "doc-" + rand.toString() + userid.toString() + "." + extn
            // console.log(filename);
            self.scope.history.proof = filename;
            self.scope.proof_file = file;


        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        /*
        TO GET EMPLOYEE HISTORY
        **/
        function getEmployeehistory() {
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            userService.getEmployeehistory(data, function(response) {
                if (response.Status == "200") {
                    self.scope.emp_history = response.data;
                }
            });
        }


        /*
        TO EDIT THE SAVED HISTORY
        **/

        function editHistory(obj) {
            self.scope.history = angular.copy(obj);
            // console.log(self.scope.history);
            // self.scope.history.from_year = self.scope.history.from_year.toString();
            // self.scope.history.from_month = self.scope.history.from_month.toString();
            setTimeout(function() {
                angular.element("#from_year").trigger('chosen:updated');
                angular.element("#from_month").trigger('chosen:updated');
                angular.element("#to_year").trigger('chosen:updated');
                angular.element("#to_month").trigger('chosen:updated');
            }, 500);
            if (self.scope.history.to_month && self.scope.history.to_year) {
                self.scope.history.currentlyWorking = 0;
            } else {
                self.scope.history.currentlyWorking = 1;
            }
            if (self.scope.history.headline) {
                self.scope.history.headlineSwitch = 1;
            } else {
                self.scope.history.headlineSwitch = 0;
            }
            var opt = {
                templateUrl: 'scripts/module/user/templates/updatehistory.html',
                scope: $scope,
                closeButton: true,
                size: 'md',
                onEscape: function() {
                    self.scope.history = {};
                    self.scope.history.currentlyWorking = 0;
                    self.scope.history.headlineSwitch = 0;
                },
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                // buttons: {
                //     cancel:{
                //         lable: "cancel",
                //         className:"btn btn-default loginbtn",
                //         callback:function(){
                //         }
                //     },
                //     success: {
                //         label: "OK",
                //         className: "btn btn-primary signupbtn",
                //         callback: function() {
                //         }
                //     }
                //
                // }
            };
            $ngBootbox.customDialog(opt);
        }

        function updateHistory(form) {
            form.$setSubmitted();
            self.scope.historySaving = true;
            var userid = $cookieStore.get('userid');
            if (form.$valid) {
                var data = angular.copy(self.scope.history);
                data.userid = userid;
                data.proof_file = self.scope.proof_file;
                data['hideLoader'] = true;
                if ($cookieStore.get('admin_id')) {
                    data.updated_by = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data.updated_by = $cookieStore.get('client_id')
                } else {
                    data.updated_by = $cookieStore.get('userid')
                }
                console.log(data)
                userService.addHistory(data, function(response) {
                    self.scope.historySaving = false;
                    if (response.Status == "200") {
                        // self.scope.historySaving = false;
                        $ngBootbox.hideAll();
                        getEmployeehistory();
                        form.$setPristine();
                        setTimeout(function() {
                            self.scope.history = {};
                            self.scope.history.currentlyWorking = 0;
                        }, 200);
                        // toastr.success("Employment history updated!!");
                    }
                });
            } else {
                self.scope.historySaving = false;
            }
        }
        /*
        TO CANCEL THE HISTORY POPUP
        **/
        function cancelHistory() {
            $ngBootbox.hideAll();
            self.scope.history = {};
            self.scope.history.currentlyWorking = 0;
        }


        /*
        TO GET THE CHORES AND LANGUAGES
        **/
        function getlanguages() {
            userService.getLanguages(function(response) {
                if (response.Status == "200") {
                    self.scope.languages = response.languages;
                    self.scope.householdChores = response.chores;
                }
            });
        }

        /*
        TOGGLE LANGUAGES
        **/
        function toggleLanguages(option, is_other) {
            if (option) {
                if (is_other) {
                    // self.scope.others.languages = ['Others'];
                    var lang = self.scope.others.languages;
                    var languages = [];
                    angular.forEach(self.scope.languages, function(data) {
                        languages.push(data.language);
                    });
                    var val = $filter('intersect')(lang, languages);
                    var idx = self.scope.others.languages.indexOf(val[0]);
                    if (idx > -1) {
                        self.scope.others.languages.splice(idx, 1);
                    }
                    self.scope.others.languages.push(option);
                    return false;
                }
                if (option == 'Others') {
                    if (self.scope.others.languages.indexOf('Others') > -1) {
                        var lang = self.scope.others.languages;
                        var languages = [];
                        angular.forEach(self.scope.languages, function(data) {
                            languages.push(data.language);
                        });
                        var val = $filter('intersect')(lang, languages);
                        var idx = self.scope.others.languages.indexOf(val[0]);
                        if (idx > -1) {
                            self.scope.others.languages.splice(idx, 1);
                        }
                    }
                }
                var idx = self.scope.others.languages.indexOf(option);
                // Is currently selected
                if (idx > -1) {
                    self.scope.others.languages.splice(idx, 1);
                }
                // Is newly selected
                else {
                    self.scope.others.languages.push(option);
                }
            }

        }

        /*
        TOGGLE Chore
        **/
        function toggleChores(option) {
            var idx = self.scope.others.household_chores.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.others.household_chores.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.others.household_chores.push(option);
            }
        }

        /*
        Calculate Age from the given year
        **/
        function calculateAge(year) {
            if (year) {
                var today = new Date();
                self.scope.personal.age = today.getFullYear() - year;
            } else {
                self.scope.personal.age = 0;
            }

        }

        /*
        TO Open reference popup
        */
        function openReference(action, obj) {
            self.scope.refAction = action;
            if (action == 'edit') {
                self.scope.reference = angular.copy(obj);
            }
            var opt = {
                templateUrl: 'scripts/module/user/templates/reference.html',
                scope: $scope,
                closeButton: true,
                size: 'md',
                onEscape: function() {
                    self.scope.reference = {};
                },
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
            };
            $ngBootbox.customDialog(opt);
        }

        /*
        To Save the reference
        **/
        self.scope.referenceDoc = {};

        function saveReference(form) {
            form.$setSubmitted();
            if (form.$valid) {
                self.scope.referenceSaving = true;
                var data = angular.copy(self.scope.reference);
                data['userid'] = $cookieStore.get('userid');
                data['file'] = self.scope.file;
                data['hideLoader'] = true;
                if ($cookieStore.get('adminid')) {
                    data.updated_by = $cookieStore.get('adminid')
                } else if ($cookieStore.get('client_id')) {
                    data.updated_by = $cookieStore.get('client_id')
                } else {
                    data.updated_by = $cookieStore.get('userid')
                }
                userService.saveReference(data, function(response) {
                    self.scope.referenceSaving = false;
                    if (response.Status == '200') {
                        form.$setPristine();
                        setTimeout(function() {
                            self.scope.reference = {};
                            self.scope.file = {};
                        }, 200);
                        getProfilecompletess(userid);
                        getReferences();
                    }
                });
            } else {
                self.scope.referenceSaving = false;
            }
        }

        /*
        To Get the user references
        **/
        function getReferences() {
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            userService.getReferences(data, function(response) {

                if (response.Status == '200') {
                    $ngBootbox.hideAll();
                    self.scope.references = response.data;
                } else {
                    toastr.error(response.message);
                }
            });
        }

        /*
        Cancel callback of reference
        **/

        function cancelReference(form) {
            $ngBootbox.hideAll();
            self.scope.reference = {};
            self.scope.file = {};
        }

        function refDocumentupload(file) {
            // self.scope.reference.document = file.name;
            self.scope.file = file;
        }

        /*
        To get the completeness percentage of the profile
        **/

        function getProfilecompletess(userid) {
            var data = {};
            data['userid'] = userid;
            data['hideLoader'] = true;
            userService.getCompleteness(data, function(response) {
                if (response.Status == "200") {
                    self.scope.profileCompleteness = response.percentage;
                }
            });
        }


        /*
        To toggle jobtype
        **/
        function toggleJobtype(option) {
            var idx = self.scope.preference.job_type.indexOf(option);

            // Is currently selected
            if (idx > -1) {
                self.scope.preference.job_type.splice(idx, 1);
            }

            // Is newly selected
            else {
                self.scope.preference.job_type.push(option);
            }

        }

        /*
        To open the emergency contact popup
        **/
        function openEcontact(action, obj) {
            self.scope.EmergencyAction = action;
            if (action == 'Edit') {
                self.scope.emergency = angular.copy(obj);
            }
            var opt = {
                templateUrl: 'scripts/module/user/templates/emergency.html',
                scope: $scope,
                closeButton: true,
                size: 'md',
                onEscape: function() {
                    self.scope.history = {};
                    self.scope.history.currentlyWorking = 0;
                    self.scope.history.headlineSwitch = 0;
                },
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                // buttons: {
                //     cancel:{
                //         lable: "cancel",
                //         className:"btn btn-default loginbtn",
                //         callback:function(){
                //         }
                //     },
                //     success: {
                //         label: "OK",
                //         className: "btn btn-primary signupbtn",
                //         callback: function() {
                //         }
                //     }
                //
                // }
            };
            $ngBootbox.customDialog(opt);
        }

        /*
        To update Emergency contact of an user
        **/
        function saveEmergencycontact(form) {
            form.$setSubmitted();
            var data = angular.copy(self.scope.emergency);
            data['userid'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            self.scope.emerSaving = true;
            if (form.$valid) {
                userService.saveEmergency(data, function(response) {
                    self.scope.emerSaving = false;
                    if (response.Status == '200') {
                        self.scope.emergency = {};
                        $ngBootbox.hideAll();
                        getEmergencycontacts(data['userid']);
                        form.$setPristine();
                        getProfilecompletess(data['userid']);
                    }
                });
            } else {
                self.scope.emerSaving = false;
            }

        }

        /*
        Cancel callback of emergency contact
        **/
        function cancelEmergency(form) {
            self.scope.emergency = {};
            $ngBootbox.hideAll();
            form.$setPristine();
        }

        /*
        To get the emergency contacts of the user
        **/
        function getEmergencycontacts(userid) {
            var data = {};
            data['userid'] = userid;
            data['hideLoader'] = true;
            userService.getEmergencycontacts(data, function(response) {
                if (response.Status == "200") {
                    self.scope.emp_emergencycontact = response.data;
                }
            });
        }

        /*
        To check user's resume
        */
        function saveResume(file) {
            if (file) {
                var filename = file.name;
                var extn = filename.split(".").pop();
                self.scope.resumeFile = file;
                uploadResume();
            } else {
                toastr.error("Invalid file type, Acceptable types are " + filetypes.toString(), "File type error");
            }

        }

        /*
        To Upload user's resume
        **/
        function uploadResume() {
            var data = {};
            data['file'] = self.scope.resumeFile;
            data['userid'] = $cookieStore.get('userid');
            data['hideLoader'] = true;
            if ($cookieStore.get('admin_id')) {
                data.updated_by = $cookieStore.get('adminid')
            } else if ($cookieStore.get('client_id')) {
                data.updated_by = $cookieStore.get('client_id')
            } else {
                data.updated_by = $cookieStore.get('userid')
            }
            var upload = Upload.upload({
                url: __env.apiUrl + '/uploadresume/',
                data: data,
            });
            self.scope.others.resumeUploading = true;
            upload.then(function(resp) {
                if (resp.data.Status == "200") {
                    //     toastr.success('Resume updated');
                    getProfilecompletess(data['userid']);
                    self.scope.others.resume = resp.data.filename;
                    self.scope.others.resumeUploading = false;
                    self.scope.resumeFile = {};
                }
                // file is uploaded successfully
            }, function(resp) {
                // handle error
            }, function(evt) {
                // progress notify
                var progress = parseInt(100.0 * evt.loaded / evt.total);
                self.scope.others.progress = progress;
            });
        }

        //To get the country codes
        function getCountrycodes() {
            userService.getcountrycodes(function(response) {
                if (response.Status == "200") {
                    self.scope.countryCodes = response.data;
                }
            });
        }

        //To select the country code
        function selectCode(obj, index) {
            var elementLists = angular.element(document.querySelectorAll('.country-codes li'));
            angular.forEach(elementLists, function(element) {
                angular.element(element).removeClass('selected');
            });
            self.scope.selectedCode = obj;
            self.scope.personal.country_code = obj.id;
            angular.element('.country-codes #' + index + 'option').addClass('selected');
        }

        //Cancel change password
        function cancelChangepwd() {
            var oldState = $cookieStore.get('oldLocation');
            var oldAttr = $cookieStore.get('oldAttr');
            $state.go(oldState, oldAttr);
        }

        function getclientProfile(userid) {
            var data = {};
            data['userid'] = userid;
            userService.getclientProfile(data, function(response) {
                if (response.Status == "200") {
                    self.scope.clientUser = response.data;
                    self.scope.blocked_user = response.data.blocked_user;
                    self.scope.vetted_user = response.data.vetted_user;
                }
            });
        }

        function updateClientdata(obj) {
            // console.log(data);
            var data = {};
            data = obj;
            data['updated_by'] = ($cookieStore.get('adminid')) ? $cookieStore.get('adminid') : $cookieStore.get('userid');
            var deferred = $q.defer();
            angular.forEach(obj, function(value, key) {
                console.log(key + "-> " + value);
                if (!value) {
                    if (key != 'profpic') {
                        deferred.reject('Value cannot be empty!!');
                        // toastr.error('Value cannot be empty.')
                        getclientProfile(userid);
                        return deferred.promise;
                    }

                }
            });
            // console.log(self.scope.clientUser.country_code);
            data['userid'] = $cookieStore.get('userid');
            self.scope.clientUser['country_code'] = self.scope.clientUser.country_code__id;
            Object.assign(data, self.scope.clientUser);
            userService.saveProfile(data, function(response) {
                if (response.Status == "200") {
                    deferred.resolve('crct');
                    $cookieStore.put('name', obj.first_name + " " + obj.last_name);
                    $rootScope.$broadcast('updateUsername');
                    self.scope.clientUser.first_name = obj.first_name;
                    self.scope.clientUser.last_name = obj.last_name;
                    // getProfilecompletess(userid);
                    // switchTab(tab);
                }
                getclientProfile(obj.id);
            });
            // return deferred.promise;

        }

        function clientDatavalidation(obj) {
            // self.scope.clientBackupdata = obj;

        }


        /*
        To filter the locations
        **/
        function getLocations(viewValue) {
            // console.log(viewValue);
            self.scope.locationLoading = true;
            // if (viewValue == '') {
            //     getallUsers(1);
            // }
            var data = {};
            var result = [];
            data['searchKey'] = viewValue;
            self.scope.locLoading = true;
            var deferred = $q.defer();
            jobService.getlocation(data, function(response) {
                self.scope.locLoading = false;
                var locations = response.data;
                angular.forEach(response.data, function(data, key) {
                    var viewVal = data.suburb + ", " + data.state + " " + data.postcode;
                    locations[key].viewval = viewVal;
                });
                deferred.resolve(locations);
            });
            return deferred.promise;
        }

        // function clearLocationCall(){
        //     if (self.scope.personal.fullAddress == '') {
        //         // self.scope.searchDisabled = true;
        //         // getallUsers(1);
        //     }
        // }

        function setTag(item) {
            self.scope.personal.city = item.suburb;
            self.scope.personal.state = item.state;
            self.scope.personal.post_code = item.postcode;
        }
        $scope.$watch('personal.fullAddress', function(newval, oldval) {
            if (!newval) {
                self.scope.personal.city = '';
                self.scope.personal.state = '';
                self.scope.personal.post_code = '';
            }
        });

        function setHistorylocation(item) {
            self.scope.history.city = item.suburb;
            self.scope.history.state = item.state;
            self.scope.history.post_code = item.postcode;
            self.scope.history.location = item.suburb + ',' + item.state + " " + item.postcode;
        }

        /*
        To get the experience To year
        **/

        function getToyear(fromYear) {
            if (fromYear) {
                // console.log(fromYear);
                var i;
                var arr = [];
                var currentYear = (new Date()).getFullYear();
                for (i = fromYear; i <= currentYear; i++) {
                    arr.push(i);
                }
                setTimeout(function() {
                    angular.element('#to_year').trigger('chosen:updated');
                }, 500);
                // console.log(arr);
                return arr;
            }
        }

        /*
        To get the experience To month
        **/

        function getTomonths(fromMonth) {
            if (fromMonth) {
                // console.log(fromMonth);
                var i;
                var arr = [];
                var currentMonth = self.scope.months;
                for (i = fromMonth; i <= currentMonth.length; i++) {
                    arr.push(self.scope.months[i - 1])
                }
                setTimeout(function() {
                    angular.element('#to_month').trigger('chosen:updated');
                }, 500);
                // console.log(arr);
                return arr;
            }
        }
        /*
        to remove a cookie
        **/
        function removeCookie(cookieid) {
            // body...
            $cookieStore.remove(cookieid);
        }



        $scope.$watch('clientUser.country_code__id', function(newVal, oldVal) {
            if (newVal !== oldVal) {
                var selected = $filter('filter')(self.scope.countryCodes, {
                    id: newVal
                });
                if (selected) {
                    self.scope.clientUser.country_code = selected.length ? selected[0].id : null;
                    self.scope.clientUser.country_code__code = selected.length ? selected[0].code : null;
                    self.scope.clientUser.country_code__flag_class = selected.length ? selected[0].flag_class : null;
                }
            }
        });

        $scope.$watch('history.location', function(newVal, oldVal) {
            // console.log(newVal);
            if (newVal == undefined) {
                self.scope.history.location = '';
                self.scope.history.suburb = '';
                self.scope.history.city = '';
                self.scope.history.post_code = '';
            }
        });


        /*
        To delete the employee history
        **/
        function deleteHistory(id, index) {
            // body...
            $ngBootbox.confirm({
                    "message": "<p class='m-t-20'>Are you sure you want to delete this employment history?</p>",
                    "title": '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                    "className": 'final_step_postjob text-center',
                    buttons: {
                        cancel: {
                            label: "Cancel",
                            className: "btn btn-default loginbtn"
                        },
                        confirm: {
                            label: "Ok",
                            className: "btn btn-default signupbtn"
                        }
                    }
                })
                .then(function() {
                    var data = {};
                    data['id'] = id;
                    data['hideLoader'] = true;
                    if ($cookieStore.get('admin_id')) {
                        data.updated_by = $cookieStore.get('adminid')
                    } else if ($cookieStore.get('client_id')) {
                        data.updated_by = $cookieStore.get('client_id')
                    } else {
                        data.updated_by = $cookieStore.get('userid')
                    }
                    userService.deletehistory(data, function(response) {
                        if (response.Status == "200") {
                            self.scope.emp_history.splice(index, 1);
                            getProfilecompletess(userid);
                        }
                    });
                }, function() {

                });

        }

        /*
        To delete the employee reference
        **/
        function deleteReference(id, index) {
            // body...
            var data = {};
            data['id'] = id;
            data['hideLoader'] = true;
            if ($cookieStore.get('admin_id')) {
                data.updated_by = $cookieStore.get('adminid')
            } else if ($cookieStore.get('client_id')) {
                data.updated_by = $cookieStore.get('client_id')
            } else {
                data.updated_by = $cookieStore.get('userid')
            }
            userService.deletereference(data, function(response) {
                if (response.Status == "200") {
                    self.scope.references.splice(index, 1);
                }
            });
        }
        /*
        To delete user's resume
        **/
        self.scope.deleteResume = deleteResume;

        function deleteResume() {
            $ngBootbox.confirm({
                    "message": "<p class='text-center m-t-20'>Are you sure you want to delete resume?</p>",
                    "title": '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                    "className": 'final_step_postjob',
                    buttons: {
                        cancel: {
                            label: "Cancel",
                            className: "btn btn-default loginbtn"
                        },
                        confirm: {
                            label: "Ok",
                            className: "btn btn-default signupbtn"
                        }
                    }
                })
                .then(function() {
                    var data = {};
                    data['userid'] = self.scope.userid;
                    data['hideLoader'] = true;
                    if ($cookieStore.get('admin_id')) {
                        data.updated_by = $cookieStore.get('adminid')
                    } else if ($cookieStore.get('client_id')) {
                        data.updated_by = $cookieStore.get('client_id')
                    } else {
                        data.updated_by = $cookieStore.get('userid')
                    }
                    userService.deleteresume(data, function(response) {
                        if (response.Status == "200") {
                            self.scope.others.resume = null;
                            self.scope.resumeFile = {};
                            getProfilecompletess(userid);
                        }
                    });
                }, function() {

                });
        }

        /*
        To delete the employee emergency contact
        **/
        function deleteEmerContact(id, index) {
            // body...
            var data = {};
            data['id'] = id;
            data['hideLoader'] = true;
            if ($cookieStore.get('admin_id')) {
                data.updated_by = $cookieStore.get('adminid')
            } else if ($cookieStore.get('client_id')) {
                data.updated_by = $cookieStore.get('client_id')
            } else {
                data.updated_by = $cookieStore.get('userid')
            }
            userService.deleteContact(data, function(response) {
                if (response.Status == "200") {
                    self.scope.emp_emergencycontact.splice(index, 1);
                    getProfilecompletess(userid);
                }
            });
        }

        function backViewUrl() {
            window.history.back()
        }

        /*
        To delete the document of the reference
        **/
        function deleteReferenceDoc(id, ref) {
            var data = {};
            data['id'] = id;
            if ($cookieStore.get('admin_id')) {
                data.updated_by = $cookieStore.get('adminid')
            } else if ($cookieStore.get('client_id')) {
                data.updated_by = $cookieStore.get('client_id')
            } else {
                data.updated_by = $cookieStore.get('userid')
            }
            userService.deletereferenceDoc(data, function(response) {
                if (response.Status == "200") {
                    self.scope.reference.document = null;
                    self.scope.file = {};
                }
            });
        }

        function removerefFile() {
            self.scope.file = {};
        }

        // Editor options.
        self.scope.ckoptions = {
            language: 'en',
            allowedContent: true,
            entities: false,
            extraPlugins: 'justify',
            // toolbar: 'Full',
            toolbar_Full: [
                ['Bold', 'Italic', 'Underline', '-', 'JustifyCenter', 'JustifyLeft']
            ],
            toolbar: 'Full',

            // toolbar_Full :
            // [
            //     { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            //     { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            //     { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            //     { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
            //         'HiddenField' ] },
            //     '/',
            //     { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            //     { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
            //     '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            //     { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            //     { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
            //     '/',
            //     { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            //     { name: 'colors', items : [ 'TextColor','BGColor' ] },
            //     { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
            // ]
        };

        function goBack() {
            window.history.back();
        }

        /*
         It is for Block and Unblock Check
        */

        function BlockUser(ModelValue, userId) {
            // alert()
            console.log(userId)
            self.scope.is_block = ModelValue
            var opt = {
                templateUrl: 'scripts/module/user/templates/blockConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            // console.log(index);
                            // angular.element("#"+index).attr("checked",'true');
                            // req.has_req = true;
                            angular.element('#block').removeClass('ng-hide')
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            angular.element('#block').addClass('ng-hide')
                            var data = {};
                            data['userid'] = userId;
                            data['is_block'] = ModelValue
                            data['hideLoader'] = 1;
                            self.scope.generalLoader = true;
                            userService.blockUser(data, function(response) {
                                self.scope.generalLoader = false;
                                // angular.element('#block').removeClass('ng-hide')
                                toastr.clear()
                                if (response.Status == "200") {
                                    if (response.is_block == 1) {
                                        toastr.error('The user blocked.');
                                    } else {
                                        toastr.success('The user unblocked.');
                                    }
                                    self.scope.blocked_user = response.is_block
                                } else {
                                    toastr.error('error')
                                }
                            });
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);


        }

        /*
         It is for vetted and unvetted Check
        */

        function VettedUser(ModelValue, userId) {
            // console.log($scope.vetted_user)
            self.scope.is_vetted = ModelValue
            // self.scope.vetted_user = ModelValue
            var opt = {
                templateUrl: 'scripts/module/user/templates/vettedConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            // if(ModelValue == 1){
                            //    self.scope.vetted_user = 0
                            // }else{
                            //     self.scope.vetted_user = 1
                            // }
                            // console.log(index);
                            // angular.element("#"+index).attr("checked",'true');
                            // req.has_req = true;
                            angular.element('#vetted').removeClass('ng-hide')
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            angular.element('#vetted').addClass('ng-hide')
                            var data = {};
                            data['userid'] = userId;
                            data['is_vetted'] = ModelValue
                            data['hideLoader'] = 1;
                            self.scope.generalLoader = true;
                            userService.vettedUser(data, function(response) {
                                self.scope.generalLoader = false;
                                // angular.element('#vetted').removeClass('ng-hide')
                                toastr.clear()
                                if (response.Status == "200") {
                                    if (response.is_vetted == 1) {
                                        toastr.success('The user marked as vetted successfully.');
                                    } else {
                                        toastr.error('The user marked as unvetted successfully.');
                                    }
                                    var userid = $cookieStore.get('userid');
                                    viewprofile(userid);
                                    self.scope.vetted_user = response.is_vetted
                                    // self.scope.is_block = response.data.is_block;
                                } else {
                                    //  if(ModelValue == 1){
                                    //    self.scope.vetted_user = 0
                                    // }else{
                                    //     self.scope.vetted_user = 1
                                    // }
                                    toastr.error('error')
                                }
                            });
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);


        }

        function changeStatus(obj, status) {
            toastr.clear()
            var data = {};
            data['id'] = obj.id;
            data['status'] = status;
            data['userid'] = $cookieStore.get('adminid');
            carerService.changestatus(data, function(response) {
                if (response.Status == "200") {
                    obj.status = status;
                    if (response.status == '1') {
                        setTimeout(function() {
                            toastr.error('Credential status changed');
                        }, 500)
                    } else {
                        setTimeout(function() {
                            toastr.success('Credential status changed');
                        }, 500)

                    }
                    // toastr.success('Credential status changed!!');
                    var userid = $cookieStore.get('userid');
                    viewprofile(userid);
                }
            });
        }




    };
})();