/**
 * @ngdoc overview
 * @name menowApp.user
 *
 * Sub-module user management
 */
 (function(){
'use strict';
angular
.module('menowApp.user', []);
})();