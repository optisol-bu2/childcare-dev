/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular
    .module('menowApp.auth')
    .service('adminService', function($http, $rootScope, $timeout, $cookieStore, Upload) {
        var postDomain = window.location.host;
        var postProtocol = window.location.protocol;
        return {
            adminLogin: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/adminlogin/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            viewalljobs: function(data, callback) {
                $http({
                        url: __env.apiUrl + '/viewalljobs/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            getuserList: function(data, callback) {
                data.hideLoader = 1;
                $http({
                        url: __env.apiUrl + '/getuserlist/',
                        method: "POST",
                        data: data,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                    .then(function successCallback(result) {
                        callback(result.data);
                    }, function errorCallback(result) {
                        callback(result);
                    });
            },
            adminGetPaymentReport: function(data, callback) {

                $http({
                    method: 'post',
                    url: __env.apiUrl + '/adminGetPaymentReport/',
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
            },

            getCarerReport: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                    method: 'post',
                    url: __env.apiUrl + '/getCarerReport/',
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
            },
            getClientReport: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                    method: 'post',
                    url: __env.apiUrl + '/getClientReport/',
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
            },


            getJobReport: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                    method: 'post',
                    url: __env.apiUrl + '/getJobReport/',
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
            },




            getPaymentReport: function(data, callback) {
                data['hideLoader'] = 1;
                $http({
                    method: 'post',
                    url: __env.apiUrl + '/getPaymentReport/',
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).then(function successCallback(result) {
                    callback(result.data);
                }, function errorCallback(result) {
                    callback(result);
                });
            },


        };
    });