/**
 * @ngdoc function
 * @name menowApp.user.controller:UserCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, for user functions
 */
(function() {
    'use strict';
    angular
        .module('menowApp.user')
        .controller('adminCtrl', adminCtrl);
    adminCtrl.$inject = ['$scope', 'Auth', '$state', 'userService', '$filter', '$cookieStore', 'toastr', '$ngBootbox', 'Upload', 'adminService', '$http', '$controller', '$route', '$rootScope', '$location'];

    function adminCtrl($scope, Auth, $state, userService, $filter, $cookieStore, toastr, $ngBootbox, Upload, adminService, $http, $controller, $route, $rootScope, $location) {
        var self = this;
        self.scope = $scope;

        self.scope.adminUser = {};
        self.scope.currentState = $state.current.name;
        self.scope.adminLogged = $cookieStore.get('adminLogged');
        self.scope.limit = 10;
        self.scope.job_status = 1;
        self.scope.selectedUser = $cookieStore.get('userid');
        self.scope.useridReq = false;
        self.scope.adminProfilePic = $cookieStore.get('admin_profpic');
        self.scope.toggleVetted = toggleVetted;
        if (!$cookieStore.get('adminuserMode')) {
            self.scope.userMode = 'client';
        } else {
            self.scope.userMode = $cookieStore.get('adminuserMode');
        }
        self.scope.userListLoad = true;
        self.scope.adminLogin = adminLogin;
        self.scope.adminLogout = adminLogout;
        self.scope.getJoblist = getJoblist;
        self.scope.loadPages = loadPages;
        self.scope.submitUser = submitUser;
        self.scope.getUserlist = getUserlist;
        self.scope.MailuserMode = $cookieStore.get('usertypeCheckMail')

        self.scope.open = open;
        self.scope.carerLoading = false;
        self.scope.clientLoading = false;
        self.scope.jobLoading = false;
        self.scope.paymentLoading = false;
        self.scope.carerReport = {}
        self.scope.clientReport = {}
        self.scope.jobReport = {}
        self.scope.paymentReport = {}
        self.scope.carerReport.dateError = false;
        self.scope.clientReport.dateError = false;
        self.scope.jobReport.dateError = false;
        self.scope.getCarerReport = getCarerReport;
        self.scope.getClientReport = getClientReport;
        self.scope.getJobReport = getJobReport;
        self.scope.getPaymentReport = getPaymentReport;
        self.scope.DownloadButtonHide = DownloadButtonHide;
        self.scope.DateCheck = DateCheck;
        self.scope.toggle_value = $cookieStore.get('checked_toggle_value');
        $cookieStore.put('admin', false);
        $scope.dateOptions = {
            
           // minDate: new Date(),
            maxDate: new Date(),
            showWeeks: true
        };
        self.scope.admin = $cookieStore.get('admin') ? true : false;
        $scope.altInputFormats = ['M!/d!/yyyy'];
        // self.scope.adminPages = ['reports', 'listPartner', 'createpartner', 'editpartner', 'adminChangepassword']

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
        $scope.popup3 = {
            opened: false
        };

        $scope.popup4 = {
            opened: false
        };
        $scope.popup5 = {
            opened: false
        };

        $scope.popup6 = {
            opened: false
        };
        $scope.popup7 = {
            opened: false
        };

        $scope.popup8 = {
            opened: false
        };

        function open(option) {
            option = true;
        }

        // if (self.scope.adminPages.indexOf(self.scope.currentState) > -1) {

        //     getUserlist('admin', false)
        // }

        // if(self.scope.currentState == 'reports'){
        //     // $cookieStore.put('adminuserMode','admin')
        //     angular.element('#admintoggle').trigger('click');
        // }



        /*
         * It check for fromdate and todate validation 
         * author S.Udhayakumar
         */
        function DateCheck(from_date, to_date, datetype) {
            // alert(errorScope)
            if (from_date && to_date) {
                if (from_date > to_date && datetype == 'carerReport') {
                    // alert()
                    self.scope.carerReport.dateError = true;
                } else {
                    self.scope.carerReport.dateError = false;
                }
                if (from_date > to_date && datetype == 'clientReport') {
                    // alert()
                    self.scope.clientReport.dateError = true;
                } else {
                    self.scope.clientReport.dateError = false;
                }
                if (from_date > to_date && datetype == 'jobReport') {
                    // alert()
                    self.scope.jobReport.dateError = true;
                } else {
                    self.scope.jobReport.dateError = false;
                }
            }
        }

        /*
         *getCarer list Report form admin
         * author S.Udhayakumar
         */
        function getCarerReport(form) {

            if (form.$valid && !self.scope.carerReport.dateError) {
                var data = {};
                data = angular.copy(self.scope.carerReport);
                data['usertype'] = 'Admin';
                data['adminId'] = $cookieStore.get('adminid')
                data['from_date'] = moment(self.scope.carerReport.from_date).format('DD-MM-YYYY');
                // data['from_date'] = self.scope.clientReport.from_date.format('DD-MM-YYYY')
                data['to_date'] = moment(self.scope.carerReport.to_date).format('DD-MM-YYYY')
                self.scope.carerLoading = true;
                adminService.getCarerReport(data, function(response) {
                    self.scope.carerLoading = false;
                    var blob = new Blob([response], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    });
                    var objectUrl = URL.createObjectURL(blob);
                    // var link = angular.element('#paymentReportId');
                    self.scope.carerReport.download = true;
                    self.scope.carerselectedFile = objectUrl
                    // if(response.Status == "200"){

                    // }
                });

            }

        }


        /*
         *getClient list Report form admin
         * author S.Udhayakumar
         */
        function getClientReport(form) {
            if (form.$valid && !self.scope.clientReport.dateError) {
                var data = {};
                data = angular.copy(self.scope.clientReport);
                data['usertype'] = 'Admin';
                data['adminId'] = $cookieStore.get('adminid')
                data['from_date'] = moment(self.scope.clientReport.from_date).format('DD-MM-YYYY');
                // data['from_date'] = self.scope.clientReport.from_date.format('DD-MM-YYYY')
                data['to_date'] = moment(self.scope.clientReport.to_date).format('DD-MM-YYYY')
                self.scope.clientLoading = true;
                adminService.getClientReport(data, function(response) {
                    self.scope.clientLoading = false;
                    var blob = new Blob([response], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    });
                    var objectUrl = URL.createObjectURL(blob);
                    // var link = angular.element('#paymentReportId');
                    self.scope.clientReport.download = true;
                    self.scope.clientselectedFile = objectUrl
                    // if(response.Status == "200"){

                    // }
                });

            } else {
                form.$setSubmitted();
            }
        }

        /*
         *getJob list Report form admin
         * author S.Udhayakumar
         */
        function getJobReport(form) {
            // form.$setSubmitted();
            if (form.$valid && !self.scope.jobReport.dateError) {
                var data = {};
                data = angular.copy(self.scope.jobReport);
                data['usertype'] = 'Admin';
                data['adminId'] = $cookieStore.get('adminid')
                data['from_date'] = moment(self.scope.jobReport.from_date).format('DD-MM-YYYY');
                // data['from_date'] = self.scope.clientReport.from_date.format('DD-MM-YYYY')
                data['to_date'] = moment(self.scope.jobReport.to_date).format('DD-MM-YYYY')
                self.scope.jobLoading = true;
                adminService.getJobReport(data, function(response) {
                    self.scope.jobLoading = false;
                    var blob = new Blob([response], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    });
                    var objectUrl = URL.createObjectURL(blob);
                    // var link = angular.element('#paymentReportId');
                    self.scope.jobReport.download = true;
                    self.scope.jobselectedFile = objectUrl
                    // if(response.Status == "200"){

                    // }
                });
            }
        }


        /*
         *getPayment list Report form admin
         * author S.Udhayakumar
         */
        function getPaymentReport(form) {
            // form.$setSubmitted();
            if (form.$valid) {
                var data = {};
                data = angular.copy(self.scope.paymentReport);
                data['usertype'] = 'Admin';
                data['adminId'] = $cookieStore.get('adminid')
                data['from_date'] = moment(self.scope.paymentReport.from_date).format('DD-MM-YYYY');
                data['to_date'] = moment(self.scope.paymentReport.to_date).format('DD-MM-YYYY')
                self.scope.paymentLoading = true;

                adminService.getPaymentReport(data, function(response) {
                    self.scope.paymentLoading = false;
                    var blob = new Blob([response], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    });
                    var objectUrl = URL.createObjectURL(blob);
                    var link = angular.element('#paymentReportId');
                    self.scope.paymentReport.download = true;
                    self.scope.paymentselectedFile = objectUrl
                    // link.download = "Payment reports.xls";
                    // link.href = objectUrl;

                });

            }
        }


        function DownloadButtonHide(obj) {
            obj.download = false;
            // obj.execute = true;
        }




        // if(self.scope.currentState == 'admin.home' || self.scope.currentState == 'admin.workerview' || self.scope.currentState == 'admin.clientview'){
        var userId = $cookieStore.get('userid');
        if (!self.scope.adminLogged) {
            $state.go('admin.login');
        }
        // getJoblist(1);
        getUserlist(self.scope.userMode);
        // submitUser(userId);
        // }
        if (self.scope.currentState == 'admin.login') {
            if (self.scope.adminLogged) {
                $state.go('admin.home');
            }
        }

        function adminLogin(form) {
            form.$setSubmitted();
            var data = angular.copy(self.scope.adminUser);
            if (form.$valid) {
                adminService.adminLogin(data, function(response) {
                    if (response.Status == "200") {
                        $cookieStore.put('adminLogged', true);
                        $cookieStore.put('adminid', response.userid);
                        $state.go('admin.home');
                        self.scope.currentState = 'admin.home';
                        self.scope.adminUser = {};
                    } else if (response.Status == "404") {
                        toastr.error(response.message);
                    }
                });
            }
        }

        /*admin logout*/
        function adminLogout() {
            //console.log($rootScope.whitelabelDetails.partner_logo)

            var opt = {
                templateUrl: 'scripts/sharedModule/templates/logoutConfirm.html',
                scope: $scope,
                closeButton: true,
                title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                buttons: {
                    No: {
                        lable: "No",
                        className: "btn btn-default loginbtn",
                        callback: function() {
                            return true
                        }
                    },
                    success: {
                        label: "Yes",
                        className: "btn btn-primary signupbtn",
                        callback: function() {
                            $cookieStore.put('adminLogged', false);
                            $cookieStore.remove('userid');
                            $cookieStore.remove('adminid');
                            $cookieStore.remove('adminuserMode');
                            $cookieStore.remove('client_id')
                            $cookieStore.remove('user_profpic');
                            $cookieStore.remove('usertype');
                            $cookieStore.remove('usertypeCheckMail')
                            $cookieStore.remove('name');
                            $cookieStore.remove('user_hostName');
                            $cookieStore.remove('encripted_email');
                            $rootScope.getwhitelabelDetails(window.location.origin)
                            // headerBin
                            $rootScope.adminLogged = '';
                            self.scope.currentState = 'login';
                            self.scope.adminLogged = false;
                            $rootScope.adminLogged = false;
                            $state.go('/');
                        }
                    }

                }
            };
            $ngBootbox.customDialog(opt);
        }


        //to get the jobs
        function getJoblist(pageno) {
            // alert();
            if (self.scope.job_status) {
                self.scope.pageno = pageno;
                self.scope.isLoading = true;
                self.scope.searchlocselected = true;
                var data = {
                    "pageno": pageno,
                    "limit": self.scope.limit,
                    "job_type": self.scope.job_type
                };
                var location = angular.copy(self.scope.address1);
                // if (self.scope.address1.fullAddress != "" && self.scope.address1.fullAddress) {
                //     Object.assign(data, location);
                //     // data['postcode'] = location.postcode;
                // }
                // if (location.date) {
                //     data['date'] = moment(location.date).format('DD-MM-YYYY');
                // }
                data['is_admin'] = true;
                data['job_status'] = self.scope.job_status;
                // data['requirements'] = self.scope.selectedreq;
                // data['caregivers'] = self.scope.selectedCaregiver;
                // data['jobtype'] = self.scope.selectedJobtype;
                // data['caretime'] = self.scope.selectedCaretime;
                // data['session'] = self.scope.selectedSession;
                // data['userid'] = $cookieStore.get('userid');
                adminService.viewalljobs(data, function(response) {
                    self.scope.isLoading = false;
                    if (response.Status == "200") {
                        self.scope.joblist = response.data;

                        self.scope.status_counts = response.status_counts;
                        self.scope.total_general = response.total_count;
                    }
                });

            } else {
                self.scope.joblist = [];
                self.scope.total_general = 0;
            }

        }

        function loadPages() {
            // alert();
            // //console.log(self.scope.total_general);
            // //console.log(self.scope.pageno);
            if (self.scope.total_general) {
                if (self.scope.total_general / self.scope.limit > self.scope.pageno) {
                    // alert();
                    self.scope.pageno += 1;
                    append_jobs(self.scope.pageno);
                }
            }
        }

        function append_jobs(pageno) {
            // alert(self.scope.total_general);
            if (pageno) {
                if (self.scope.job_status) {
                    var data = {
                        "pageno": pageno,
                        "limit": self.scope.limit,
                        "job_type": self.scope.job_type
                    };
                    // var location = angular.copy(self.scope.address1);

                    // if (self.scope.address1.fullAddress != "" && self.scope.address1.fullAddress) {
                    //     Object.assign(data, location);
                    // }
                    // if (location.date) {
                    //     data['date'] = moment(location.date).format('DD-MM-YYYY');
                    // }
                    data['job_status'] = self.scope.job_status;
                    data['is_admin'] = true;
                    // data['requirements'] = self.scope.selectedreq;
                    // data['caregivers'] = self.scope.selectedCaregiver;
                    // data['jobtype'] = self.scope.selectedJobtype;
                    // data['caretime'] = self.scope.selectedCaretime;
                    // data['session'] = self.scope.selectedSession;
                    // data['userid'] = $cookieStore.get('userid');
                    self.scope.isLoading = true;
                    adminService.viewalljobs(data, function(response) {
                        self.scope.isLoading = false;
                        if (response.Status == "200") {
                            if (response.data.length > 0) {
                                // self.scope.isLoading = false;
                                // alert();
                                self.scope.joblist = self.scope.joblist.concat(response.data);

                            }
                            self.scope.total_general = response.total_count;
                        }
                    });

                }

            }
        }



        /*
        To get the users list
        **/
        function getUserlist(mode, changed) {

            var data = {};
            self.scope.userMode = mode
            if (mode == 'admin') {
                angular.element('.navbar-default').addClass('ng-hide');
                // alert('admin')
                $cookieStore.put('admin', true);
                self.scope.admin = true;
                // if(changed){
                //     $location.path('/reports');
                // }
               
                $cookieStore.put('checked_toggle_value', mode);
                self.scope.toggle_value = $cookieStore.get('checked_toggle_value');
                if(changed){
                    $cookieStore.put('adminuserMode', mode);
                    $cookieStore.remove('user_profpic')
                    $cookieStore.remove('usertype');
                    $cookieStore.remove('usertypeCheckMail');
                    $cookieStore.remove('name');
                    $cookieStore.remove('userid');
                    $cookieStore.remove('previousPath');
                    $rootScope.$broadcast('ModeChanged')
                    $cookieStore.remove('workerTab')
                    $cookieStore.remove('parentTab')
                    $state.go('login')
                    // $cookieStore.put('admin', 'false');
                    return true;
                }

            }
            if (mode == 'client') {
                self.scope.admin = false;
                $cookieStore.put('admin', false);
                $cookieStore.put('checked_toggle_value', mode);
                self.scope.toggle_value = $cookieStore.get('checked_toggle_value');

            }
            if (mode == 'worker') {
                self.scope.admin = false;
                $cookieStore.put('admin', false);
                $cookieStore.put('checked_toggle_value', mode);
                self.scope.toggle_value = $cookieStore.get('checked_toggle_value');
            }
            if (changed) {
                $('#id').val(null).trigger('change.select2');
                self.scope.selectedUser = null;
                $cookieStore.remove('user_profpic')
                $cookieStore.remove('usertype');
                $cookieStore.remove('usertypeCheckMail');
                $cookieStore.remove('name');
                $cookieStore.remove('userid');
                $cookieStore.remove('previousPath');
                $rootScope.$broadcast('ModeChanged')
                $cookieStore.remove('workerTab')
                $cookieStore.remove('parentTab')
                $cookieStore.put('admin', 'false');
                $state.go('login')
            }
            
            $cookieStore.put('adminuserMode', mode);
            if (mode != 'admin') {
                self.scope.queryOptions = {
                    placeholder: 'Please select an user',
                    query: function(query, scope) {
                        data['mode'] = $rootScope.userMode ? $rootScope.userMode : $cookieStore.get('adminuserMode');
                        data['search_key'] = query.term;
                        data['hideLoader'] = 1;
                        if ($cookieStore.get('userid') && $cookieStore.get('name')) {
                            data['selectUserId'] = $cookieStore.get('userid');
                            data['name'] = $cookieStore.get('name');
                        }
                        $http({
                                url: __env.apiUrl + '/getuserlist/',
                                method: "POST",
                                data: data,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            })
                            .then(function successCallback(result) {
                                // //console.log(result.data);
                                query.callback(result.data);

                            }, function errorCallback(result) {

                            });

                    }
                };
            }

        }
        /*
        Get a user's view
        **/
        function submitUser(userid, path) {
            // alert("adminClick");
            // //console.log(userid);
            if (!userid) {
                self.scope.useridReq = true;
                return false;
            } else {
                self.scope.useridReq = false;
            }
            $cookieStore.put('userid', userid);

            self.scope.selectedUser = $cookieStore.get('userid');
            self.scope.userMode = (!$rootScope.userMode) ? $cookieStore.get('adminuserMode') : $rootScope.userMode;
            var data = {};
            data['userid'] = $cookieStore.get('userid');
            $http({
                url: __env.apiUrl + '/getUserDetails/',
                method: "POST",
                data: data,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function success(result) {
                var response = result.data;
                if (response.Status == '200') {
                    var name = response.user_details.first_name + " " + response.user_details.last_name
                    $cookieStore.put('user_profpic', response.user_details.profpic)
                    $cookieStore.put('usertype', response.user_details.purpose);
                    $cookieStore.put('name', name);
                    $cookieStore.put('userid', response.user_details.id);
                    if (result.data.user_details.purpose == 'worker' || result.data.user_details.purpose == "1") {
                        $cookieStore.put('usertypeCheckMail', "worker");
                    } else {
                        $cookieStore.put('usertypeCheckMail', "client");
                    }
                    if (path) {
                        // alert($cookieStore.get("toPath") !=  __env.frontendUrl+path)
                        // if($cookieStore.get("toPath") != __env.frontendUrl+path || !$cookieStore.get("toPath")){
                        $location.path(path)

                        // $rootScope.$broadcast('selectUserUpdated',{ 'userMode':self.scope.userMode,'mailView':true})
                        // }
                        // window.location.reload()
                    } else {
                        $rootScope.$broadcast('selectUserUpdated', {
                            'userMode': self.scope.userMode
                        })
                    }

                }
            }, function error(result) {
                // //console.log(result);
            });
            // if(self.scope.currentState == 'admin.home'){
            // if(self.scope.userMode == 'worker'){
            //     self.scope.templateUrl = "scripts/module/job/views/joblist.html";
            //     // $state.reload();
            //     $state.go('admin.workerview',{userid:self.scope.selectedUser,tabname:"available"});

            // }
            // else if(self.scope.userMode == 'client'){
            //     self.scope.templateUrl = "scripts/module/job/views/parentjoblist.html";
            //     // $state.reload();
            //     $state.go('admin.clientview',{userid:self.scope.selectedUser,tabname:"posted"});
            //     // $controller('JobCtrl',{$scope: $scope});
            // }
            // }
            // //console.log(divData);
            // angular.element('#content_sec').append(divData);
            // setTimeout(function(){

            // },2000);
        }


        function toggleVetted(userid) {
            $ngBootbox.confirm({
                'message': "Are your sure you want this worker as vetted?"
            }, function() {}, function() {

            });
        }

        $rootScope.$on('adminModeChanged', function() {
            // alert()
            getUserlist('admin', true)
        });


        // alert('admin')
        $rootScope.$on('MailUserDefaultSelect', function(evt, arg) {
            // alert(arg.user_id)
            $cookieStore.put('userid', arg.user_id)
            self.scope.selectedUser = arg.user_id;
            self.scope.userMode = arg.userMode;
            // setTimeout(function() {
            self.scope.userMode = arg.userMode;
            $cookieStore.put('adminuserMode', self.scope.userMode)
            // },2000);

            self.scope.MailcurrentName = arg.currentName;
            submitUser(arg.user_id, arg.path)
        })

        setTimeout(function() {
            // if(self.scope.currentState == 'admin.home'){
            //alert()
            if (self.scope.currentState == 'parentjoblist' || self.scope.currentState == 'joblist') {
                if ($location.path() == '/parentjoblist/posted' || $location.path() == '/joblist/available') {
                    angular.element('#submitUser').trigger('click');
                }

            }
            // angular.element('#submitUser').trigger('click');
            // }
        }, 1000);

    }
})();