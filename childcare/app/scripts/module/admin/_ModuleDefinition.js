/**
 * @ngdoc overview
 * @name menowApp.auth
 *
 * Sub-module for login and auth management
 */
 (function(){
'use strict';
angular
.module('menowApp.user', []);
})();