/**
 * @ngdoc service
 * @name menowApp.auth
 * @description user registration and login
 * # user
 * Service in the menowApp.
 */
(function() {
    'use strict';
    angular
        .module('menowApp.auth')
        .service('Auth', auth);
    auth.$inject = ['$http', 'apiLink', '$cookies', '$rootScope'];

    function auth($http, apiLink, $cookies, $rootScope) {

        /**
         * user Login
         */
        this.login = function(user, callback) {
            var response = [];
            $http({
                    url: apiLink + '/login',
                    method: "POST",
                    data: {
                        email: user.email,
                        password: user.password,
                        userType: [1,2]
                    },
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .then(function successCallback(result) {
                    $cookies.put('metoken', result.data.token);
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + result.token;
                    response = {
                        success: true,
                        result: result.data.user
                    };
                    callback(response);
                }, function errorCallback(result) {
                    $cookies.remove('metoken');
                    response = {
                        success: false,
                        result: result.data.error_msg
                    };
                    callback(response);
                });
        };

        /**
         * user register
         */
        this.register = function(user, callback) {
            var response = [];
            $http({
                    url: apiLink + '/registerUser',
                    method: "POST",
                    data: user,
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .then(function successCallback(result) {
                    $cookies.put('token', result.data.token);
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + result.token;
                    response = {
                        success: true,
                        result: result.data.user
                    };
                    callback(response);
                }, function errorCallback(res) {
                    var result = res.data.error_msg;
                    if (result !== null && angular.isDefined(result.invalidAttributes)) {
                        var returnVal = [];
                        angular.forEach(result.invalidAttributes, function(value, key) {
                            returnVal.push({
                                keyName: key,
                                msg: value[0].message
                            });
                        });
                        response = {
                            success: false,
                            returnArr: returnVal
                        };
                        callback(response);
                    }
                });
        };
        /**
         * set cookie on login
         */
        this.setCredentials = function(arr) {
            $rootScope.globals = {
                currentUser: arr
            };
            $cookies.putObject('meglobals', $rootScope.globals);
        };
        /**
         * remove cookie on logout
         */
        this.clearCredentials = function(callback) {
            $rootScope.globals = {};
            $cookies.remove('metoken');
            $cookies.remove('meglobals');
            $http.defaults.headers.common.Authorization = 'Bearer ';
            callback();
        };

        /* forgot password*/
        this.forgotPass = function(user, callback) {
            var response = [];
            $http({
                    url: apiLink + '/forgotPassword',
                    method: "post",
                    data: {
                        email: user.email
                    },
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .then(function successCallback(result) {
                    response = {
                        success: true,
                        returnArr: result.data.message
                    };
                    callback(response);
                }, function errorCallback(result) {
                    response = {
                        error: false,
                        returnArr: result.data.error_msg
                    };
                    callback(response);
                });
        };

        /* update new password by dhibakar */
        this.updateNewpassword = function(user, token, callback) {
            var response = [];
            $http({
                    url: apiLink + '/resetForgotPassword',
                    method: "PUT",
                    data: {
                        newPassword: user.password,
                        token: token
                    },
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .then(function successCallback(result) {
                    response = {
                        success: true,
                        returnArr: result.data.message
                    };
                    callback(response);
                }, function errorCallback(result) {
                    response = {
                        error: false,
                        returnArr: result.data.error_msg
                    };
                    callback(response);
                });
        };

        /**
         * user Login by jwt
         */
        this.loginByToken = function(id, callback) {
            var response = [];
            $http({
                    url: apiLink + '/loginToken',
                    method: "PUT",
                    data: {
                        userToken: id,
                    },
                    withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .then(function successCallback(result) {
                    $cookies.put('metoken', result.data.token);
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + result.token;
                    response = {
                        success: true,
                        result: result.data.user
                    };
                    callback(response);
                }, function errorCallback(result) {
                    $cookies.remove('metoken');
                    response = {
                        success: false,
                        result: result.data.error_msg
                    };
                    callback(response);
                });
        };
    }
})();