/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
  angular
        .module('menowApp.auth')
		.service('Auth', function($http, $rootScope, $timeout, $cookieStore, Upload, toastr, $state,blockUI) {
    var postDomain = window.location.host;
    var postProtocol = window.location.protocol;
    return {
    	login:function(data,callback){
            // aler(0)
    		$http({
                    url: __env.apiUrl + '/validate/',
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                })
                .then(function successCallback(result) {
                	if(result.data.Status == "200"){
                        blockUI.start();
                        $cookieStore.put('user_hostName',result.data.user_details.partnerid);
                        $cookieStore.put('encripted_email',result.data.token)
                        $rootScope.getwhitelabelDetails($cookieStore.get('user_hostName'))
                        if(result.data.is_admin){
                            $cookieStore.put('adminid',result.data.admin_id);
                            $cookieStore.put('adminLogged',true);
                            $cookieStore.put('userid',result.data.latest_client);
                            if(result.data.user_details){
                            $cookieStore.put('admin_profpic',result.data.user_details.profpic);
                          }
                            if(!result.data.latest_client){
                                toastr.error('No clients are available');
                                // $state.go("login");
                                // blockUI.stop()
                                // return false;
                            }
                            if($cookieStore.get('user_hostName') != window.location.origin){
                                if($cookieStore.get('encripted_email')){
                                  window.location.href = $cookieStore.get('user_hostName')+'/#/tokenlogin/'+$cookieStore.get('encripted_email');
                                }else{
                                   window.location.href = $cookieStore.get('user_hostName');
                                }
                            }
                            blockUI.stop()
                            callback(result.data);
                        }
                        else{
                            $cookieStore.put('userid',result.data.user_details.id);
                            $cookieStore.put('login_token',result.data.user_details.login_token);
                            var name = result.data.user_details.first_name + " " +result.data.user_details.last_name
                            $cookieStore.put('name',name);
                            if(result.data.user_details.purpose == 'worker' || result.data.user_details.purpose == "1" ){
                                $cookieStore.put('usertypeCheckMail',"worker");
                            }else{
                                $cookieStore.put('usertypeCheckMail',"client"); 
                            }
                            $cookieStore.put('usertype',result.data.user_details.purpose);
                            $cookieStore.put('user_profpic',result.data.user_details.profpic);
                            $cookieStore.put('login_count',result.data.login_count);
                            $cookieStore.put("callingCount",0);
                            $cookieStore.put('adminLogged',false);
                            $cookieStore.put('showJobStatusPopup',result.data.showJobStatusPopup)
                            if($cookieStore.get('user_hostName') != window.location.origin){
                                if($cookieStore.get('encripted_email')){
                                  window.location.href = $cookieStore.get('user_hostName')+'/#/tokenlogin/'+$cookieStore.get('encripted_email');
                                }else{
                                  window.location.href = $cookieStore.get('user_hostName');
                                }
                            }
                            blockUI.stop()
                            callback(result.data,result.data.user_details.purpose,result.data.has_quoted);
                        }

                	}
                    else{
                        // if($cookieStore.get('user_hostName') != window.location.origin){
                        //     window.location.href = $cookieStore.get('user_hostName');
                        // }
                        blockUI.stop()
                        callback(result.data);
                    }

                }, function errorCallback(result) {
                    callback(result);
                });
    	},
        activateUser:function(data,callback){
            $http({
                    url: __env.apiUrl + '/activateuser/',
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' }
                })
                .then(function successCallback(result) {
                    callback(result.data);

                }, function errorCallback(result) {
                    callback(result);
                });
        },
        tokenLogin:function(data,callback){
            $http({
                    url: __env.apiUrl + '/loginfromtoken/',
                    method: "POST",
                    data: data,
                    headers: { 'Content-Type': 'application/json' }
                })
                .then(function successCallback(result) {
                    if(result.data.is_admin){
                            $cookieStore.put('adminid',result.data.admin_id);
                            $cookieStore.put('adminLogged',true);
                            $cookieStore.put('userid',result.data.latest_client);
                            $cookieStore.put('user_hostName',result.data.user_details.partnerid)
                            callback(result.data);
                    }
                    else{
                        if(result.data.Status == '200'){
                            $cookieStore.put('user_hostName',result.data.user_details.partnerid)
                            $cookieStore.put('userid',result.data.user_details.id);
                            $cookieStore.put('login_token',result.data.user_details.login_token);
                            var name = result.data.user_details.first_name + " " +result.data.user_details.last_name
                            $cookieStore.put('name',name);
                            $cookieStore.put('usertype',result.data.user_details.purpose);
                            $cookieStore.put('user_profpic',result.data.user_details.profpic);
                            $cookieStore.put('login_count',result.data.login_count);
                            $cookieStore.put("callingCount",0);
                            $cookieStore.put('adminLogged',false);
                            callback(result.data,result.data.user_details.purpose,result.data.has_quoted);
                        }
                    }
                    callback(result.data);

                }, function errorCallback(result) {
                    callback(result);
                });
        },
        isAuthenticated:function(){
            if($cookieStore.get('userid') || $cookieStore.get('adminid'))
                return true;
            else
                return false;
        },
        forgetPassword:function(data,callback){
            $http({
                url: __env.apiUrl + '/forgetpassword/',
                method: "POST",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function successCallback(result) {
                callback(result.data);

            }, function errorCallback(result) {
                callback(result);
            });

        },
        checktoken:function(data,callback){
            $http({
                url: __env.apiUrl + '/checktoken/',
                method: "POST",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function successCallback(result) {
                callback(result.data);

            }, function errorCallback(result) {
                callback(result);
            });

        },
        logStatus:function(data){
            $http({
                url: __env.apiUrl + '/saveloggingstatus/',
                method: "POST",
                data: data,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function successCallback(result) {
                // callback(result.data);

            }, function errorCallback(result) {
                // callback(result);
            });

        }
    };
});
