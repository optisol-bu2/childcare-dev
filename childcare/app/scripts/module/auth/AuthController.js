/**
 * @ngdoc function
 * @name menowApp.auth.controller:AuthCtrl
 * @description
 * # AuthCtrl
 * Controller of the menowApp, login and auth controler functions and methods
 */
(function() {
    'use strict';
    angular
        .module('menowApp.auth')
        .controller('AuthCtrl', authCtrl);
    authCtrl.$inject = ['$scope', 'Auth', 'menowApp.Constant', '$cookieStore', '$cookies', '$state', 'toastr', '$location', '$timeout' , '$ngBootbox', '$rootScope', 'jobService','carerService'];

    function authCtrl($scope, Auth, constant, $cookieStore, $cookies, $state, toastr, $location, $timeout, $ngBootbox, $rootScope,jobService, carerService) {
        var vm = this;
        vm.scope = $scope;
        vm.login = login;
        vm.logout = logout;
        vm.success = false;
        vm.error = false;
        vm.logged_in = false;
        vm.callloginPopup = callloginPopup;
        vm.login_user = $cookieStore.get('name');
        vm.user = {};
        vm.callpostjob = callpostjob;
        vm.callforgetPopup = callforgetPopup;
        vm.forgetemail = forgetemail;
        vm.currentState = $state.current.name;
        vm.contactWorker = false;
        vm.contactMessage = {};
        $scope.generalLoader = false;
        $scope.firstRate = 0;
            $scope.secondRate = 3;
            $scope.readOnly = true;
        // console.log($cookieStore.get('contactMessage'));
        $scope.onItemRating = function(rating){
          alert('On Rating: ' + rating);
        };
        var token = $location.search().token;
        var emp_id = $location.search().id;
        var email = $location.search().email;
        if(vm.currentState == 'confirmemail' && token){
            // alert()
            var data = {};
            var email = $location.search().email;
            var id = $location.search().id;
            var login_token = $location.search().login_token;
            var name = $location.search().name;
            var have_job = $location.search().have_job;
            var jobid = $location.search().jobid;
            // console.log(have_job);
            data['email'] = email;
            data['jobid'] = jobid
            if(!$cookieStore.get('userid')){
                Auth.activateUser(data,function(response){
                 if(response.Status == "200"){
                    vm.logged_in = true;
                    $cookieStore.put('userid',id);
                    $cookieStore.put('login_token',login_token);
                    $cookieStore.put('name',response.user_details.first_name + " "+response.user_details.last_name);
                    $cookieStore.put('usertype',response.user_details.purpose);
                    $cookieStore.put('login_count',response.user_details.login_count);
                    toastr.clear();
                    toastr.success('Your account is activated');
                    var contactMessage = $cookieStore.get('contactMessage');
                    if (response.user_details.purpose == "1" || response.user_details.purpose == 'worker'){
                        $cookieStore.put('usertype','worker');
                        $state.go('joblist',{tabname:"available"});
                        console.log(contactMessage);
                    }else if (response.user_details.purpose == "0" || response.user_details.purpose == 'parent'){
                        // $location.path('/parentjoblist/posted');
                        if(have_job == 'True'){
                            toastr.success("Job posted successfully",'Success')
                        }
                        if(contactMessage){
                            contactMessage.from_user = $cookieStore.get('userid');
                            // $rootScope.$broadcast('sentMessagetoworker',{"message":contactMessage});
                            carerService.adminMessage(contactMessage,function(response){
                                if(response.Status == "200"){
                                    toastr.success('Message sent');
                                    $cookieStore.remove('contactMessage');
                                }
                            });
                        }
                        $cookieStore.put('usertype','parent');
                        $state.go('parentjoblist',{tabname:"posted"});
                    }

                    // angular.element('#loginPopup').click();
                 }
                 else if(response.Status == "201" && !vm.login_user){
                    toastr.warning(response.message,'Already activated');
                    $state.go('login');
                 }
            });
            }
            else{
                // $state.go('login');
                var usertype = $cookieStore.get('usertype');
                if(usertype == 'worker'){
                    $state.go('joblist',{tabname:"available"});
                }
                else{
                    $state.go('parentjoblist',{tabname:"posted"});
                }
            }

        }

        if(vm.currentState == 'tokenlogin' || vm.currentState == 'releasepaymentTokenlogin'){
            var data = {};
            var token = $state.params.token;

            if(vm.currentState == 'releasepaymentTokenlogin'){
                data['release_token'] = token
                data['release_payment_id'] = $state.params.release_payment_id;
            }else{
                    data['token'] = token;
                }
            Auth.tokenLogin(data,function(response){
             if(response.Status == "200"){
                if(response.is_admin){
                       $state.go('parentjoblist',{tabname:'posted'});
                }
                vm.logged_in = true;
                $cookieStore.put('userid',response.user_details.id);
                $cookieStore.put('login_token',response.user_details.login_token);
                $cookieStore.put('name',response.user_details.first_name + " "+ response.user_details.last_name);
                $cookieStore.put('usertype',response.user_details.purpose);
                $cookieStore.put('login_count',response.login_count);
                var contactMessage = $cookieStore.get('contactMessage');
                if (response.user_details.purpose == 1 || response.user_details.purpose == 'worker'){
                    $cookieStore.put('usertype','worker');
                    // if($cookieStore.get('redirectState') && vm.currentState == 'releasepaymentTokenlogin'){
                    //     // alert()
                    //         var state = angular.copy($cookieStore.get('redirectState'));
                    //         var params = angular.copy($cookieStore.get('redirectParams'));
                    //         $state.go(state,params)
                    // }else{
                        $state.go('joblist',{tabname:"available"});
                    // }
                    // if(contactMessage.message){
                    //         toastr.error('Sorry!! you cannot contact another worker');
                    //         $cookieStore.remove('contactMessage');
                    // }
                }else if (response.user_details.purpose == 0 || response.user_details.purpose == 'parent'){
                    // alert($cookieStore.get('redirectClientState'))
                    // $location.path('/parentjoblist/posted');
                    if(contactMessage){
                        $rootScope.$broadcast('sentMessagetoworker',{"message":contactMessage});
                    }
                    $cookieStore.put('usertype','parent');
                    if($cookieStore.get('redirectClientState')){
                            var state = angular.copy($cookieStore.get('redirectClientState'));
                            var params = angular.copy($cookieStore.get('redirectClientParams'));
                            $state.go(state,params)
                    }else{
                        $state.go('parentjoblist',{tabname:"posted"});
                    }
                }

                // angular.element('#loginPopup').click();
             }else if(response.Status == '501'){
                 toastr.error(response.message)
                 $state.go('login')
             }
        });

        }



        if ($cookieStore.get('name')) {
            vm.logged_in = true;
        }

        function login() {
            if(vm.user.email && vm.user.password){
                // alert()

            if($cookieStore.get('jobTypeid')){
                var jobTypeId = $cookieStore.get('jobTypeid');
                var data = 'email=' + encodeURIComponent(vm.user.email) + '&password=' + encodeURIComponent(vm.user.password) +'&jobtypeId='+ encodeURIComponent(jobTypeId) + '&partnerid='+ encodeURIComponent($cookieStore.get('partnerid'));
            }else{
                var data = 'email=' + encodeURIComponent(vm.user.email) + '&password=' + encodeURIComponent(vm.user.password) +'&partnerid=' + encodeURIComponent($cookieStore.get('partnerid'));
            }
            Auth.login(data, function(response,usertype,quotes) {
                toastr.clear();
                if (response.Status == "200") {
                    if(response.is_admin){
                       $state.go('parentjoblist',{tabname:'posted'});
                    }
                    $cookieStore.remove('jobTypeid');
                    $ngBootbox.hideAll();
                    // window.location.reload();
                    // angular.element('#loginmodal').click();
                    vm.login_user = $cookieStore.get('name');
                    vm.logged_in = true;
                    vm.success = true;
                    vm.error = false;
                    if (response.jobtypeId){
                        if(response.job_status == '200'){
                            toastr.success('Job posted successfully',"Success");
                        }
                        else if(response.job_status == '500'){
                            toastr.error(response.job_message);
                        }
                    }
                    if (usertype == 'worker' || usertype == '1'){
                        if($cookieStore.get('redirectState')){
                            var state = angular.copy($cookieStore.get('redirectState'));
                            var params = angular.copy($cookieStore.get('redirectParams'));
                            //console.log(state+'----'+params)
                            // $cookieStore.remove('redirectUrl')
                            
                            if (vm.contactWorker){
                                // $rootScope.$broadcast('sentMessagetoworker',{"message":vm.contactMessage});
                                toastr.error('Sorry!! you cannot contact another worker');
                                vm.contactMessage = {};
                                vm.contactWorker = false;
                            }
                        $state.go(state,params);
                        }
                        else{
                            if (vm.contactWorker){
                                // $rootScope.$broadcast('sentMessagetoworker',{"message":vm.contactMessage});
                                toastr.error('Worker cannot send message to another worker');
                                vm.contactMessage = {};
                                vm.contactWorker = false;
                            }
                            $state.go('joblist',{tabname:response.current_tab});
                        }


                    }else if(usertype == 'parent' || usertype == '0'){
                        if (vm.contactWorker){
                            $rootScope.$broadcast('sentMessagetoworker',{"message":vm.contactMessage});
                        }
                        $cookieStore.put('client_id',$cookieStore.get('userid'))
                        if($cookieStore.get('redirectClientState')){
                            var clientState = angular.copy($cookieStore.get('redirectClientState'));
                            var clientParams = angular.copy($cookieStore.get('redirectClientParams'));
                            $state.go(clientState,clientParams);
                        }else{
                            $state.go('parentjoblist',{tabname:response.current_tab});
                        }
                    }

                }
                else if(response.Status == "404"){
                    // toastr.clear();
                    toastr.error(response.message,'',{preventOpenDuplicates:true});
                }
                else if(response.Status == "403"){
                    var options = {
                    templateUrl: 'scripts/module/auth/templates/not_activated_msg.html',
                    scope: $scope,
                    title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/'+$rootScope.whitelabelDetails.partner_logo+'" alt=""></div></div>',
                    buttons:{
                        close:{
                            label:'Close',
                            className: "btn btn-default signupbtn ",
                            callback: function() {
                                return true;
                                // tabclick('home');
                                $rootScope.$broadcast('callSignup', { 'postjob': true });
                            }
                        }
                    }
                    };
                    $ngBootbox.customDialog(options);
                    setTimeout(function(){
                        angular.element('body').addClass("modal-open");
                    },1000);
                }
                 else {
                    // return false;
                    vm.error = true;
                    vm.success = false;
                }
            });
        }
        else{
            return false;
        }

        }

        function logout() {
            alert();
            var userid = $cookieStore.get('userid');
            saveLogoutStatus(userid);
            vm.logged_in = false;
            vm.user = {};
            vm.success = false;
            vm.error = false;
            $cookieStore.remove('name');
            $cookieStore.remove('userid');
            $cookieStore.remove('client_id');
            $cookieStore.remove('adminid');
            $cookieStore.remove('login_token');
            $location.path('/login');
        }

        function callloginPopup(arg){
            vm.user = {};
            if (arg)
            if (arg.hasOwnProperty('message')){
                vm.contactWorker = true;
                vm.contactMessage = arg.message
            }
            $ngBootbox.hideAll();
            var options = {
                templateUrl: 'scripts/module/auth/templates/login.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/'+$rootScope.whitelabelDetails.partner_logo+'" alt=""></div></div>',
                onEscape:function(){
                    vm.user = {};
                }
            };
            $ngBootbox.customDialog(options);
            setTimeout(function(){
                angular.element('body').addClass("modal-open");
            },1000);
        }

        function callforgetPopup(){
            $ngBootbox.hideAll();
            var options = {
                templateUrl: 'scripts/module/auth/templates/forgetpassword.html',
                scope: $scope,
                title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/'+$rootScope.whitelabelDetails.partner_logo+'" alt=""></div></div>',

            };
            $ngBootbox.customDialog(options);
            setTimeout(function(){
                angular.element('body').addClass("modal-open");
            },1000);
        }

        $scope.$on('callLogin',function(evnt,arg){
            var argu = {};
            if (arg)
            if (arg.hasOwnProperty('message')){
                argu = arg;
            }
            callloginPopup(argu);
        });

        function callpostjob(){
            $rootScope.$broadcast('callJobpopup');
        }

        function forgetemail(form){
            var data = {};
            data['email'] = vm.emailid;
            if (data['email']){
                Auth.forgetPassword(data,function(response){
                if(response.Status == "200"){
                    $ngBootbox.hideAll();
                    var options = {
                        templateUrl: 'scripts/module/auth/templates/successforget.html',
                        scope: $scope,
                        title: '<div class="col-md-12"><div class="logo_section" id="logo"><img src="/../partner_logo/'+$rootScope.whitelabelDetails.partner_logo+'" alt=""></div></div>',
                        buttons:{
                        close:{
                            label:'Close',
                            className: "btn btn-default signupbtn",
                            callback: function() {
                                return true;
                                // tabclick('home');
                            }
                        }
                    }
                        };
                    $ngBootbox.customDialog(options);
                }
                else{
                    toastr.error(response.message);
                }
                vm.emailid = '';
                form.$setPristine();
            });
            }

        }

        if(vm.currentState == "checktoken"){
            var data = {};
            var email = $location.search().email;
            var id = $location.search().id;
            $cookieStore.put('userid',id);
            var token = $location.search().token;
            data['token'] = token;
            data['userid'] = id;
            Auth.checktoken(data,function(response){
                if(response.Status == "Correct"){
                    $state.go("newpassword");
                }
                else{
                    toastr.error('Invalid token found!!');
                    $state.go("/");
                }
            });


        }



    }
})();
