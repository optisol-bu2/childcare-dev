// angular
//     .module('menowApp')
//     .constant('paypal',{
//     	// CLIENT_ID : 'QWZXTTRmVEZIWVhYdFY0RHdUWFZocG4yTnljUUp5U2tWX2ZfSTdnTGI2S21iS1N0MGZZdEU0NjJUN0VyUEU3d2pFVi1RUU01MlpPeDIxaUk=',
//     	CLIENT_ID : 'QVJWMkZXNEpVSkNjNVhPSVlrTWRmZlB0Z2UxOXdNcVFJUkc5SFNLeHNDT3BibGI2NTRZZE8tdlVreGZfRUZkelRSN010bGY1cXJJOUFlOUk=',
//     	SECRET_ID : 'EIJE8DsROPjwomxhFXVD5u2-0vuNCxKJ1FcRU34-K7jMKCF6HNgnPSu_9bIyY5euQkI3pDGl5Rcf7Fpe',
//     	SANDBOX_ACCOUNT_ID : 'balaji.om-facilitator@optisolbusiness.com',   	
//     });



angular
    .module('menowApp')
    .constant('Suggestion',{
    	PROFILE_FIRST: "Please complete your profile now to",  	
    	ALERT_PERCENT: 80
    });

if(__env.apiUrl != "https://app.lovelychildcare.com/service"){
    // //Office sandbox account
    angular
        .module('menowApp')
        .constant('paypal',{
        	// CLIENT_ID : 'QWZXTTRmVEZIWVhYdFY0RHdUWFZocG4yTnljUUp5U2tWX2ZfSTdnTGI2S21iS1N0MGZZdEU0NjJUN0VyUEU3d2pFVi1RUU01MlpPeDIxaUk=',
        	CLIENT_ID : 'QVF3Q0lzQXJBMXVaaFRJOXM3ckZlbkVlc2hjSmFHWksyN0h3SDJwMlVRb0tlRGxzT3RSc2lnenhjckZKU19idUxBcmJPRWpCWGY4WHB0c3Q=',
        	SECRET_ID : 'EIJE8DsROPjwomxhFXVD5u2-0vuNCxKJ1FcRU34-K7jMKCF6HNgnPSu_9bIyY5euQkI3pDGl5Rcf7Fpe',
        	SANDBOX_ACCOUNT_ID : 'admin@lovelychildcare.com',
            TOKEN_URL: 'https://api.sandbox.paypal.com/v1/oauth2/token',
            PAYMENT_URL: 'https://api.sandbox.paypal.com/v1/payments/payment'
        });
}


if(__env.apiUrl == "https://app.lovelychildcare.com/service"){
    //Office live account
    angular
    .module('menowApp')
    .constant('paypal',{
        // CLIENT_ID : 'QWZXTTRmVEZIWVhYdFY0RHdUWFZocG4yTnljUUp5U2tWX2ZfSTdnTGI2S21iS1N0MGZZdEU0NjJUN0VyUEU3d2pFVi1RUU01MlpPeDIxaUk=',
        CLIENT_ID : 'QVNWN3N5X3NEYzdHNlFlcWlib1RhNGpZMC13TGM1SzhjbVYtUGhMWW5STGdMREFMREp0ZTREZFBMZlBlTlFGRmNWRXVkUmZtNjh1eWdwWnM=',
        SECRET_ID : 'EIJE8DsROPjwomxhFXVD5u2-0vuNCxKJ1FcRU34-K7jMKCF6HNgnPSu_9bIyY5euQkI3pDGl5Rcf7Fpe',
        SANDBOX_ACCOUNT_ID : 'invoices@optisolbusiness.com',
        TOKEN_URL: 'https://api.paypal.com/v1/oauth2/token',
        PAYMENT_URL: 'https://api.paypal.com/v1/payments/payment'
    });
}