/* Directive to compair two field in a form*/
(function(){
 'use strict';

 angular
 .module('menowApp')
 .directive('compareTo',compareTo)
 .directive('jqdatepicker',jqdatepicker);
jqdatepicker.$inject = ['$filter'];
  function compareTo() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue === scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};



function jqdatepicker($filter) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            dateOption: "=dateOptions"
        },
        link: function(scope, element, attrs, ngModelCtrl) {
         
            element.datepicker({
                dateFormat: 'dd/mm/yy',
                maxDate: scope.dateOption.maxDate,
                changeYear: true,
                changeMonth: true,
                onSelect: function(date) {
                    var ar = date.split("/");
                    //date = new Date(ar[2],ar[1],ar[0]);
                    date = new Date(ar[2] + "-" + ar[1] + "-" + ar[0]+'T00:00:00Z');
                    ngModelCtrl.$setViewValue(date.getTime());
                    scope.$apply();
                }
            });
            ngModelCtrl.$formatters.unshift(function(v) {
            return $filter('date')(v,'dd/MM/yyyy');
            });

        }
    };
};

})();