(function() {
    'use strict';
    angular
        .module('menowApp')
        .directive('autocomplete1', autoComplete);


    function autoComplete($log,jobService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                autocompleteLoading:"="
            },
            link: function(scope, elm, attr, ctrl) {
                if (attr.type !== 'text') {
                    $log.warn('You can apply autocomplete only on Text input');
                    return;
                }
                scope.output = [];
                // console.log(scope.location);
                elm.on("keyup", function(event) {
                    var searchKey = elm[0].value;
                    if(searchKey != "" && (event.keyCode > 47 && event.keyCode < 106)){
                        var data = {};
                        data['searchKey'] = searchKey;
                        scope.autocompleteLoading = true;
                        jobService.getlocation(data,function(response){
                            scope.autocompleteLoading = false;
                            if(response.Status == "200"){
                                scope.output = response.data;
                                scope.$emit('locationFilter', {'filter_result':scope.output});
                            }
                        });
                    }
                    else{
                        scope.$emit('locationFilter', {'filter_result':[]});
                    }


                });

            },
            // templateUrl: "scripts/sharedModule/views/header.html",
            controller: ['$scope', 'Auth', '$state', '$cookieStore',
                function($scope, Auth, $state, $cookieStore,scope) {
                    var self = this;
                    self.scope = $scope;
                    // console.log(self.scope.output);
                    // console.log(self.scope.cities);
                }
            ]
        };
    };


    angular
        .module('menowApp').directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function(val) {
                return val != null ? '' + val : null;
            });
        }
    };
});

})();



 angular
        .module('menowApp').filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});




 angular
        .module('menowApp').directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);



// trim directive
angular.module('menowApp').filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });

angular.module('menowApp').directive('autoScrollEnd', ['$timeout', function ($timeout) {return {
    restrict: 'AC',
    link: function (scope, el, attrs) {
    $timeout(function() {
            scope.$apply(function () {
            scope.updateScrollbar && scope.updateScrollbar('scrollTo', 'bottom');
            });
                    },1000);
        }
    };
}]);