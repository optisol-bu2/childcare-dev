angular.module('menowApp').directive('setClassWhenAtTop', function ($window) {
  var $win = angular.element($window); // wrap window object as jQuery object

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
          offsetTop = element.offset().top; // get element's offset top relative to document
      var w = window.innerWidth;
      if (w > 767){
        $win.on('scroll', function (e) {
        if ($win.scrollTop() >= offsetTop) {
          var top = $win.scrollTop()-offsetTop + "px";
          element.addClass(topClass);
          // element.css('top',top);
        } else {
          element.removeClass(topClass);
          // element.css('top','0px');
        }
      });
        // on load
        if ($win.scrollTop() >= offsetTop) {
          var top = $win.scrollTop()-offsetTop + "px";
          element.addClass(topClass);
          // element.css('top',top);
        }
      }
      
    }
  };
})
.directive('bindHeight',function($window){
  var $win = angular.element($window); 
  return {
        restrict: 'A',
        link: function(scope, element, attr) {
            $win.on('scroll', function (e) {
              var height = element[0].scrollHeight;
              scope.stickyHeight = height+"px";
            });
        }
    };
});


angular
        .module('menowApp').directive('scrollToBottom', function($timeout, $window) {
    return {
        scope: {
            scrollToBottom: "="
        },
        restrict: 'A',
        link: function(scope, element, attr) {
            scope.$watchCollection('scrollToBottom', function(newVal) {
                if (newVal) {
                    $timeout(function() {
                        element[0].scrollTop =  element[0].scrollHeight;
                    }, 0);
                }

            });
        }
    };
});


angular.module('menowApp').filter('intersect', function(){
    return function(arr1, arr2){
        return arr1.filter(function(n) {
                   return arr2.indexOf(n) == -1
               });
    };
});