(function() {
    'use strict';
    angular
        .module('menowApp')
        .directive('header', header)
        .directive('footer', footer);
    //Directive of header can be called in html page with <div header></div>
    function header() {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: "scripts/sharedModule/views/header.html",
            controller: ['$scope', 'Auth', '$state', '$cookieStore', '$window', '$rootScope', '$location', '$ngBootbox', '$document', 'adminService',
                function($scope, Auth, $state, $cookieStore, $window, $rootScope, $location, $ngBootbox, $document, adminService) {
                    var self = this;
                    // $scope.modeChanged = '';
                    // $scope.$watch($scope.modeChanged,function(value,key){

                    // })
                    /*
                      It is for admin user Impersonate view for client or worker
                      author S.Udhayakumar.
                    */

                    $rootScope.$on('selectUserUpdated', function(evt, arg) {
                        // alert(0)
                        $rootScope.adminHeaderchange = false;
                        HeaderBind();
                        $scope.$broadcast('adminSelectUser', {
                            "userMode": arg.userMode
                        })
                        if (arg.userMode == 'client' && !arg.mailView) {
                            $cookieStore.put('parentTab', 'posted')
                            $state.go('parentjoblist', {
                                tabname: 'posted'
                            });
                        } else if (arg.userMode == 'worker' && !arg.mailView) {
                            $cookieStore.put('workerTab', 'available')
                            $state.go('joblist', {
                                tabname: 'available'
                            });
                        }
                    })

                    self.scope = $scope;
                    self.scope.logout = logout;
                    self.scope.logged_in = false;
                    self.scope.scrollTopHeader = scrollTopHeader;
                    self.scope.currentState = $state.current.name;
                    self.scope.myjobhref = '';
                    self.scope.underProgress = underProgress;
                    self.scope.callsignupPopup = callsignupPopup;
                    self.scope.gotoChangepasswd = gotoChangepasswd;
                    self.scope.gotoPaymentReport = gotoPaymentReport;
                    self.scope.isScrolled = window.scrollY;
                    var hostName = $cookieStore.get('user_hostName') ? $cookieStore.get('user_hostName') : window.location.origin;


                    /*
                        To bind a dynamic header for worker and client
                        author S.Udhayakumar
                    */
                    HeaderBind();

                    function HeaderBind() {
                        self.scope.login_user = $cookieStore.get('name');
                        self.scope.loginuser_profilepic = $cookieStore.get('user_profpic');
                        if (self.scope.currentState == 'myprofile') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'updateprofile') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'reviews') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'publicprofile') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        //  if(self.scope.currentState == 'findcarer'){
                        //     angular.element('body').addClass('body_bg_gray');
                        // }
                        if (self.scope.currentState == 'previewjob') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'workerviewjob') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'parentprofile') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'viewworker') {
                            angular.element('body').addClass('body_bg_gray');
                        }
                        if (self.scope.currentState == 'reports') {
                            angular.element('body').addClass('body_bg_gray');
                        }

                        var purpose = $cookieStore.get('usertype');

                        if (purpose == 'worker' || purpose == 1) {
                            self.scope.purpose = 'worker';
                            self.scope.myjobhref = '#/joblist/available';
                            $cookieStore.put('worketTab', 'available')
                        } else if (purpose == 'parent' || purpose == 0 || purpose == 'client') {
                            self.scope.purpose = 'parent';
                            self.scope.myjobhref = '#/parentjoblist/posted';
                            $cookieStore.put('parentTab', 'posted')
                        }
                        self.scope.callloginPopup = callloginPopup;
                        if ($cookieStore.get('name')) {
                            self.scope.logged_in = true;
                        }
                        self.scope.path = $location.path();
                        /*Functions*/
                        self.scope.logout = logout;
                        if ($cookieStore.get('name')) {
                            self.scope.logged_in = true;
                        }
                    }

                    /*Functions*/
                    function callloginPopup() {
                        $rootScope.$broadcast('callLogin');
                    }

                    function callsignupPopup() {
                        $rootScope.$broadcast('callSignup', {
                            'postjob': false
                        });
                    }
                    $scope.$on('profilepicUpdate', function(evt, arg) {
                        self.scope.loginuser_profilepic = arg.pic;
                    });
                    $scope.$on('updateUsername', function(evt, arg) {
                        self.scope.login_user = $cookieStore.get('name');
                    });
                    /*logout*/
                    function logout() {
                        // alert();
                        var opt = {
                            templateUrl: 'scripts/sharedModule/templates/logoutConfirm.html',
                            scope: $scope,
                            closeButton: true,
                            title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                            buttons: {
                                No: {
                                    lable: "No",
                                    className: "btn btn-default loginbtn",
                                    callback: function() {
                                        return true
                                    }
                                },
                                success: {
                                    label: "Yes",
                                    className: "btn btn-primary signupbtn",
                                    callback: function() {
                                        var userid = $cookieStore.get('userid');
                                        saveLogoutStatus(userid);
                                        self.scope.logged_in = false;
                                        self.scope.user = {};
                                        self.scope.success = false;
                                        self.scope.error = false;
                                        $cookieStore.remove('name');
                                        $cookieStore.remove('userid');
                                        $cookieStore.remove('client_id');
                                        $cookieStore.remove('adminid');
                                        $cookieStore.remove('login_token');
                                        $cookieStore.remove('usertype');
                                        $cookieStore.remove('usertypeCheckMail');
                                        $cookieStore.remove('jobTypeid');
                                        $cookieStore.remove('showJobStatusPopup')
                                        $cookieStore.remove('redirectClientState')
                                        $cookieStore.remove('redirectClientParams')
                                        $cookieStore.remove('redirectState')
                                        $cookieStore.remove('redirectParams')
                                        $cookieStore.remove('user_hostName');
                                        $cookieStore.remove('encripted_email');
                                        $rootScope.userId = false;
                                        $rootScope.getwhitelabelDetails(window.location.origin)
                                        // HeaderBind()
                                        window.location.href = '#/login';
                                    }
                                }

                            }
                        };
                        $ngBootbox.customDialog(opt);
                    };

                    /*
        To save the logging status after logout
        **/
                    function saveLogoutStatus(userid) {
                        // alert();
                        var data = {};
                        data['userid'] = userid;
                        data['status'] = "LOGOUT";
                        Auth.logStatus(data);
                    }

                    // scroll top
                    function scrollTopHeader() {
                        // alert('raja');
                        var duration = 4000; //milliseconds

                        //Scroll to the exact position

                        var offset = 0; //pixels; adjust for floating menu, context etc
                        //Scroll to #some-id with 30 px "padding"
                        //Note: Use this in a directive, not with document.getElementById
                        var someElement = angular.element(document.getElementById('scroll_top'));

                        console.log(someElement);
                        if (someElement.length) {
                            $document.scrollToElement(someElement, offset, duration);
                        }
                    };

                    $window.onscroll = function() {
                        $scope.$apply(function() {
                            self.scope.isScrolled = window.scrollY;
                        });

                    };



                    // Popup for parent my profile

                    function underProgress() {
                        var opt = {
                            templateUrl: 'scripts/sharedModule/templates/stillprogress.html',
                            scope: $scope,
                            closeButton: true,
                            title: '<div class="col-md-12"><div class="logo_section"><img src="/../partner_logo/' + $rootScope.whitelabelDetails.partner_logo + '" alt=""></div></div>',
                            buttons: {
                                cancel: {
                                    lable: "Cancel",
                                    className: "btn btn-default loginbtn",
                                    callback: function() {
                                        return true
                                    }
                                },
                                success: {
                                    label: "OK",
                                    className: "btn btn-primary signupbtn",
                                    callback: function() {
                                        return true
                                    }
                                }

                            }
                        };
                        $ngBootbox.customDialog(opt);
                    };


                    //To store the current state and goto changepassword page
                    //author S.Udhayakumar
                    function gotoChangepasswd(usertype) {
                        $cookieStore.put('oldLocation', $state.current.name);
                        $cookieStore.put('oldAttr', $state.params);
                        if (usertype != 'admin') {
                            $state.go('changepassword', {
                                userMode: usertype
                            });
                        } else {
                            $state.go('adminChangepassword', {
                                userMode: usertype
                            });
                        }
                    }



                    function gotoReports() {
                        $location.path('/reports')
                    }
                    // To get admin get payment report page
                    //author S.Udhayakumar
                    function gotoPaymentReport() {
                        var data = {};
                        data['usertype'] = 'Admin';
                        data['adminId'] = $cookieStore.get('adminid')
                        adminService.adminGetPaymentReport(data, function(response) {
                            var blob = new Blob([response], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                            });
                            var objectUrl = URL.createObjectURL(blob);
                            var link = document.createElement("a");
                            link.download = "Payment reports.xls";
                            link.href = objectUrl;
                            document.body.appendChild(link);
                            link.click();
                            return false;
                        });
                    }
                }
            ]
        };
    };
    //Directive of header can be called in html page with <div footer></div>
    function footer() {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: "scripts/sharedModule/views/footer.html",
            controller: [function() {}]
        };
    };


})();