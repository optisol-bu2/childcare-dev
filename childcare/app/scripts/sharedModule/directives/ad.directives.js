(function() {
    'use strict';
    angular
        .module('menowApp')
        .directive('ad1', ad1)
        .directive('ad2', ad2)
        .directive('ad3', ad3);

        function ad1() {
        	return {
	            restrict: 'A',
	            replace: true,
	            templateUrl: "scripts/sharedModule/views/app-ads/ad1.html",
	            controller: [function() {
	            	// alert()
	            }]
	        };
        }

        function ad2() {
        	return {
	            restrict: 'A',
	            replace: true,
	            templateUrl: "scripts/sharedModule/views/app-ads/ad2.html",
	            controller: [function() {}]
	        };
        }

        function ad3() {
        	return {
	            restrict: 'A',
	            replace: true,
	            templateUrl: "scripts/sharedModule/views/app-ads/ad3.html",
	            controller: [function() {}]
	        };
        }
    })();