'use strict';

var app =  angular.module('menowApp');

app.directive('windowDir', ['$window', function ($window) {

     return {
        link: link,
        restrict: 'A'           
     };

     function link(scope, element, attrs){
     	if($window.innerWidth > 0 && $window.innerWidth < 767){
           	scope.state = false;
        }
       angular.element($window).bind('resize', function(){
           scope.windowWidth = $window.innerWidth;
           if($window.innerWidth < 767){
           	scope.state = false;
           }
           else if($window.innerWidth > 767){
           	scope.state = true;
           }
       });    
     }    
 }]);