'use strict';
/**
 * @ngdoc overview
 * @name menowApp
 * @contains all route
 * # menowApp
 *
 * Main module of the application.
 */
var env = {};

if (typeof Object.assign != 'function') {
    Object.assign = function(target) {
        'use strict';
        if (target == null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        target = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source != null) {
                for (var key in source) {
                    if (Object.prototype.hasOwnProperty.call(source, key)) {
                        target[key] = source[key];
                    }
                }
            }
        }
        return target;
    };
}

 

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
    // console.log(__env.frontendUrl)
}

angular
    .module('menowApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'toastr',
        'ng.httpLoader',
        'ui.bootstrap',
        'ngFileUpload',
        'menowApp.auth',
        'menowApp.user',
        'menowApp.home',
        'ngBootbox',
        'angular.chosen',
        'vAccordion',
        'jkAngularRatingStars',
        'ngIntlTelInput',
        'infinite-scroll',
        'angular-input-stars',
        'video-background',
        'duScroll',
        'textAngular',
        'ngFileUpload',
        'ngValidate',
        'ngImgCrop',
        'blockUI',
        'angular-svg-round-progressbar',
        'autoCompleteModule',
        'ngImage',
        'ngScrollbars',
        'autoCompleteModule',
        'toggle-switch',
        'xeditable',
        'rt.select2',
        'ngCkeditor',
        'hl.sticky',
        'bw.paging'
        //'menowApp.menowAdminApp'
    ])
    .config(function($urlRouterProvider, $stateProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('login', {
                url: '/login',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/home/views/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('Maillogin', {
                url: '/Maillogin',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/home/views/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('joblist', {
                url: '/joblist/:tabname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/joblist.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('howitworks', {
                url: '/howitworks',
                views: {
                    '@': {
                        templateUrl: 'scripts/sharedModule/views/howitwork.html',
                        // controller: 'JobCtrl',
                        // controllerAs: 'vm',
                        authenticate: false
                    }
                }
            })
            .state('confirmemail', {
                url: '/confirmemail',
                views: {
                    '@': {
                        // templateUrl: 'scripts/module/home/views/home.html',
                        controller: 'AuthCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('parentjoblist', {
                url: '/parentjoblist/:tabname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/parentjoblist.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('previewjob', {
                url: '/previewjob/:jobid/:tabname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/previewjob.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('releasePaymentMail', {
                url: '/previewjob/:jobid/:confirm_userid/:client_id/:tabname/:token/release_payment',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/previewjob.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('rateForClientMail', {
                url: '/worker/previewjob/:jobid/:worker_id/:tabname/rate_for_client',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/worker_preview_job.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('workerviewjob', {
                url: '/worker/previewjob/:jobid/:tabname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/worker_preview_job.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('workerviewjobMail', {
                url: '/worker/previewjob/:jobid/:workerId/:tabname/workerviewjobMail',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/worker_preview_job.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('previewJobClientMail', {
                url: '/previewjob/:jobid/:client_id/:tabname/previewJobClientMail',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/previewjob.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })




            // .state('settings', {
            //     url: '/settings',
            //     views: {
            //         '@': {
            //             templateUrl: 'scripts/sharedModule/views/settings.html',
            //             controller: '',
            //             controllerAs: 'vm'
            //         }
            //     }
            // })

            .state('changepassword', {
                url: '/changepassword/:userMode',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/changepassword.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('adminChangepassword', {
                url: '/changepassword/:userMode',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/changepassword.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('findcarer', {
                url: '/findcarer',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/findworker/views/findcarer.html',
                        controller: 'carerCtrl',
                        controllerAs: 'vm',
                        authenticate: false
                    }
                }
            })


            .state('reviews', {
                url: '/reviews/:userid/:jobId',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/Reviews/reviews.html',
                        controller: 'ReviewCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('checktoken', {
                url: '/checktoken',
                views: {
                    '@': {
                        // templateUrl: 'scripts/sharedModule/views/newpassword.html',
                        controller: 'AuthCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('newpassword', {
                url: '/newpassword',
                views: {
                    '@': {
                        templateUrl: 'scripts/sharedModule/views/newpassword.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('myprofile', {
                url: '/myprofile',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/myprofile.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('updateprofile', {
                url: '/updateprofile/:profiletab',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/updateprofile.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/settings/views/settings.html',
                        controller: 'SettingsCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('publicprofile', {
                url: '/publicprofile/:userid/?name/?location/?address/?caregivers/?job_type',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/public_profile.html',
                        controller: 'carerCtrl',
                        controllerAs: 'vm',
                        authenticate: false
                    }
                }
            })
            .state('admin', {
                views: {
                    '@': {
                        templateUrl: 'scripts/module/admin/views/admin_template.html',
                        controller: 'adminCtrl',
                        authenticate: true
                        // controllerAs: 'vm'
                    }
                }
            })
            .state('admin.login', {
                url: '/admin/login',
                controller: 'adminCtrl',
                templateUrl: 'scripts/module/admin/views/admin_login.html',
                authenticate: true
            })
            .state('admin.changepassword', {
                url: '/admin/changepassword',
                templateUrl: 'scripts/module/user/views/changepassword.html',
                controller: 'UserCtrl',
                controllerAs: 'vm',
                authenticate: true
                // }
            })
            .state('admin.home', {
                url: '/admin/home',
                controller: 'adminCtrl',
                templateUrl: 'scripts/module/admin/views/admin_home.html',
                authenticate: true
            })
            .state('admin.clientview', {
                url: '/admin/clientview/:userid/:tabname',
                controller: 'JobCtrl',
                templateUrl: 'scripts/module/job/views/parentjoblist.html',
                authenticate: true
            })
            .state('admin.workerview', {
                url: '/admin/workerview/:userid/:tabname',
                controller: 'JobCtrl',
                templateUrl: 'scripts/module/job/views/joblist.html',
                authenticate: true
            })

            .state('parentprofile', {
                url: '/parentprofile',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/parentprofile.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('viewworker', {
                url: '/viewcarer/:userid-:fname-:lname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/user/views/public_profile.html',
                        controller: 'carerCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })

            .state('admin.viewworker', {
                url: '/admin/viewcarer/:userid',
                templateUrl: 'scripts/module/user/views/myprofile.html',
                controller: 'carerCtrl',
                controllerAs: 'vm',
                authenticate: true
            })

            .state('successPayment', {
                url: '/successPayment/:jobid/:tabname',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/job/views/previewjob.html',
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('tokenlogin', {
                url: '/tokenlogin/:token',
                views: {
                    '@': {
                        controller: 'AuthCtrl',
                        controllerAs: 'vm',
                        authenticate: false
                    }
                }
            })
            .state('clientsettings', {
                url: '/clientsettings',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/settings/views/settings.html',
                        controller: 'SettingsCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('/', {
                url: '/',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/home/views/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'hm'
                        // controller: 'AuthCtrl',
                        // controllerAs: 'vm'
                    }
                }
            })
            .state('releasepaymentTokenlogin', {
                url: '/tokenlogin/:token/:release_payment_id/release_payment',
                views: {
                    '@': {
                        controller: 'AuthCtrl',
                        controllerAs: 'vm',
                        // authenticate:false
                    }
                }
            })

            .state('reports', {
                url: '/reports',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/admin/views/reports.html',
                        controller: 'adminCtrl',
                        controllerAs: 'vm',
                        // authenticate:false
                    }
                }
            })

            .state('listPartner', {
                url: '/listPartner',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/whitelabel/views/ListPartner.html',
                        controller: 'WhitelabelCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('createpartner', {
                url: '/createpartner',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/whitelabel/views/addpartner.html',
                        controller: 'WhitelabelCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })
            .state('editpartner', {
                url: '/editpartner/:partnerId',
                views: {
                    '@': {
                        templateUrl: 'scripts/module/whitelabel/views/addpartner.html',
                        controller: 'WhitelabelCtrl',
                        controllerAs: 'vm',
                        authenticate: true
                    }
                }
            })


        $locationProvider.hashPrefix('');
    });

angular
    .module('menowApp')
    .config([
        'httpMethodInterceptorProvider', '$httpProvider',
        function(httpMethodInterceptorProvider, $httpProvider, localStorageServiceProvider) {
            $httpProvider.interceptors.push('myHttpInterceptor');
        }
    ]);


    
angular
    .module('menowApp')
    .run(['$rootScope', '$location', '$http', '$cookies', 'menowApp.Constant', '$window', '$state', 'whitelabelService', '$cookieStore',

        function($rootScope, $location, $http, $cookies, constant, $window, $state, whitelabelService, $cookieStore) {
            //autorization token is sent on every request based on cookie token
            if ($cookies.get('metoken')) {
                var token = $cookies.get('metoken');
                $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            }
            if ($cookies.getObject('meglobals')) {
                var global_usr = $cookies.getObject('meglobals');
                // console.log(global_usr);
            }
            $rootScope.globals = global_usr || {};
            $rootScope.whitelabelDetails = {};
            $rootScope.whitelabelDetails.partner_logo = "loading.gif"
            
            $rootScope.maxDate= new Date();
            // $rootScope.getwhitelabelDetails = whitelabelDetails;
            var hostName = window.location.origin;

            

            /*
             * It's get for whitelabel content update
             * author S.Udhayakumar
             */

            $rootScope.getwhitelabelDetails = function(hostName) {
                var data = {};
                data['hostName'] = hostName;
                data['port'] = __env.port;
                // data['hideLoader'] = 1;
                whitelabelService.whitelabelDetails(data, function(response) {
                    if (response.Status == '200') {
                        $rootScope.whitelabelDetails = response.data;
                        $cookieStore.put('whitelabelDetails', $rootScope.whitelabelDetails)
                        $cookieStore.put('partnerid', response.data.id)
                        __env.test = $cookieStore.get('whitelabelDetails')
                        // $localStorage.whitelabelDetails = response.data;
                        // if ($rootScope.whitelabelDetails.partner_subdomain != window.location.origin) {
                        //     if ($cookieStore.get('encripted_email')) {
                        //         window.location.href = $rootScope.whitelabelDetails.partner_subdomain + '/#/tokenlogin/' + $cookieStore.get('encripted_email');
                        //     } else {
                        //         window.location.href = $rootScope.whitelabelDetails.partner_subdomain;
                        //     }
                        // }
                    }
                })
            }

            $rootScope.getwhitelabelDetails(hostName);
        }
    ]);



/*
 * It's str to number conversion directive
 * author S.Udhayakumar
 */

angular
    .module('menowApp').directive('convertToNumber', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(val) {
                    return val != null ? parseInt(val, 10) : null;
                });
                ngModel.$formatters.push(function(val) {
                    return val != null ? '' + val : null;
                });
            }
        };
    });


/*
 * It's country code dropdown plugin
 * author S.Udhayakumar
 */

angular.module('menowApp')
    .config(function(ngIntlTelInputProvider) {
        ngIntlTelInputProvider.set({
            allowDropdown: true,
            initialCountry: 'au',
            separateDialCode: true,
            customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
                return "Mobile number";
            }

        });
    });

/*
 * It's for sidebar directive
 * author S.Udhayakumar
 */

angular.module('menowApp').directive('sidebarDirective', function() {

    return {
        link: function(scope, element, attr) {
            scope.$watch(attr.sidebarDirective, function(newVal) {
                // console.log(newVal);
                if (newVal) {
                    element.addClass('show');
                    return;
                }
                element.removeClass('show');
            });
        }
    };
});




/*
 * It's for ajax loader plugin configuration
 * author S.Udhayakumar
 */

angular.module('menowApp').config(["blockUIConfig", function(blockUIConfig) {
    // Change the default overlay message
    blockUIConfig.message = 'Loading....';

    // Change the default delay to 100ms before the blocking is visible
    blockUIConfig.delay = 100;
    blockUIConfig.template = '<div class="block-ui-overlay"><div class="mycusomloader text-center"><img src="styles/images/loader_page.gif" alt="loader_global" class="" width="130"></div></div>';
    // Tell the blockUI service to ignore certain requests
    blockUIConfig.requestFilter = function(config) {
        // document.cookie = "username=John Doe";

        // console.log($rootScope.whitelabelDetails)
        if (config.data) {
            // console.log(window.__env.test)

            var partner = document.cookie.split(';');
            console.log(partner)
            // searchText = searchText.toLowerCase();
            angular.forEach(partner, function(item, key) {
                // console.log(item.indexOf('whitelabelDetails'))
                if (item.indexOf('partnerid') > -1) {

                    var id_find = item.indexOf("partnerid");
                    var Id = item.slice(id_find + 10, item.length)
                    //       alert(Id)
                    if (config.url != __env.apiUrl + '/validate/' && config.url.indexOf("https://api.sandbox.paypal.com") == -1 && config.url.indexOf("https://api.paypal.com") == -1) {
                        // alert()
                        config.data.partnerid = Id;
                    }

                }
                if (item.indexOf('show_partner_portals') > -1) {

                    var portal_check_find = item.indexOf("show_partner_portals");
                    var portal_check = item.slice(portal_check_find + 26, portal_check_find + 27)
                    //       alert(Id)
                    
                    if (config.url != __env.apiUrl + '/validate/' && config.url != __env.apiUrl + '/partnerAddUpdate/' && config.url.indexOf("https://api.sandbox.paypal.com") == -1 && config.url.indexOf("https://api.paypal.com") == -1) {
                        // alert()
                        config.data.show_partner_portals = portal_check;
                    }

                }
                
            });
            if (config.data.hideLoader > 0 || config.url == "https://api.sandbox.paypal.com/v1/payments/payment" || config.url == "https://api.paypal.com/v1/payments/payment") {
                return false;
            }
        }

    };

}]);

/*
 * It's for toastr plugin configuration
 * author S.Udhayakumar
 */

angular.module('menowApp').config(function(toastrConfig) {
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: true,
        target: 'body'
    });
});


/*
 * It's for editable text plugin configuration
 * author S.Udhayakumar
 */

angular.module('menowApp').run(['editableOptions', function(editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);


/*
 * It's for reports page xls file download
 * author S.Udhayakumar
 */

// Hook in a directive
angular.module('menowApp').directive('fileDownload', function() {
    return {
        restrict: 'E', // applied on 'element'
        scope: {
            fileurl: '@fileurl',
            linktext: '@linktext',
            filename: '@filename'
        },
        template: '<a class="btn btn-default submit_btn_report hvr-float-shadow" href="{{ fileurl }}" download="{{filename}}">{{ linktext }}</a>', // need this so that the inner HTML can be re-used
        link: function(scope, elem, attrs) {
            /* Ref: https://thinkster.io/egghead/isolate-scope-at
             * This block is used when we have
             * scope: {
                 fileurl: '=fileurl',
                 linktext: '=linktext'
               }
             scope.fileurl = attrs.fileurl;
             scope.linktext = attrs.linktext;*/
        }
    }
})



/*
 * It's for mail to view app restriction path
 * author S.Udhayakumar
 */

angular.module("menowApp").run(function($rootScope, $state, Auth, $transitions, $cookieStore, $location) {



    $transitions.onStart({}, function($transition$, state) {
        var toState = $transition$.$to();

         var adminPages = ['reports', 'listPartner', 'createpartner', 'editpartner', 'adminChangepassword']

         if (adminPages.indexOf(toState.name) > -1) {
            $cookieStore.put('adminuserMode', 'admin');
            $cookieStore.remove('user_profpic')
            $cookieStore.remove('usertype');
            $cookieStore.remove('usertypeCheckMail');
            $cookieStore.remove('name');
            $cookieStore.remove('userid');
            $cookieStore.remove('previousPath');
            $rootScope.$broadcast('ModeChanged')
            $cookieStore.remove('workerTab')
            $cookieStore.remove('parentTab')
         }

        if ((toState.name == 'workerviewjob' || toState.name == 'settings' || toState.name == 'rateForClientMail') && !Auth.isAuthenticated()) {
            var params = {};
            params['jobid'] = $transition$.params('to').jobid;
            params['tabname'] = $transition$.params('to').tabname;
            if ($transition$.params('to').hasOwnProperty('confirm_userid')) {
                params['confirm_userid'] = $transition$.params('to').confirm_userid;
            }
            if ($transition$.params('to').hasOwnProperty('worker_id')) {
                params['worker_id'] = $transition$.params('to').worker_id;
            }

            $cookieStore.put('redirectState', toState.name);
            $cookieStore.put('redirectParams', params);
            $state.transitionTo("login");
            event.preventDefault();
        }

        if (toState.views.hasOwnProperty('@')) {
            if ((toState.views['@'].authenticate) && !Auth.isAuthenticated()) {
                // User isn’t authenticated
                if ((toState.name == 'releasePaymentMail') || (toState.name == 'previewJobClientMail') || (toState.name == 'rateForClientMail') || (toState.name == 'workerviewjobMail') || (toState.name == 'previewjob')) {
                    if (!$cookieStore.get('adminLogged')) {
                        $cookieStore.put('redirectClientState', toState.name);
                        if ($transition$.params('to')) {
                            $cookieStore.put('redirectClientParams', $transition$.params('to'));
                        }
                    }


                    if (toState.name == 'releasePaymentMail') {
                        // alert()
                        var params = {};
                        params['token'] = $transition$.params('to').token;
                        params['release_payment_id'] = $transition$.params('to').jobid;
                        // $location.path('releasepaymentTokenlogin/'+params['token'])
                        $state.go('releasepaymentTokenlogin', params)
                    } else {
                        $state.transitionTo("Maillogin");
                    }
                    // $state.transitionTo("Maillogin");
                } else {
                    $state.transitionTo("login");
                }
                event.preventDefault();
            }
        } else {
            if (toState.self.authenticate && !Auth.isAuthenticated()) {
                $state.transitionTo("login");
                event.preventDefault();
            }
        }

    });

    // Go to top whenever new state occurs
    $transitions.onSuccess({}, function(transition) {
        window.scrollTo(0, 0);
    })
});