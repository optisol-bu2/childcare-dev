/**
 * @ngdoc service
 * @name menowApp.Constant
 * @description constants for form validation and msgs
 * constant in the menowApp.
 */
(function() {
    'use strict';
    var apiUrlConstant = env.apiUrl;
    var webUrlConstant = env.webUrl;

    angular
        .module('menowApp')
        .constant('apiLink', apiUrlConstant)
        /*For Displaying loader */
        .config([
            'httpMethodInterceptorProvider',
            function(httpMethodInterceptorProvider) {
                httpMethodInterceptorProvider.whitelistDomain(apiUrlConstant);
            }
        ])
        .constant('menowApp.Constant', {
            publicPath: ['/login','/resetpassword','/admin/login'],
            apiUrl: apiUrlConstant,
            webUrl: webUrlConstant,
            formValidation: {
                required: 'This field is required',
                emailInvalid: 'Invalid Email address',
                passwordMin: 'Password must contain at least 6 characters', //min val 6
                passwordMax: 'Password should not be higher than 50 characters', //max val 50
                passwordValidation: 'Password needs to be at least 8 characters and include a letter and a number',
                passwordValidationChild: 'Password  needs to be at least 3 characters',
                confirmPassword: 'Confirm password do not match',
                categoryName: 'Category name can contain only letters and `&`',
                categoryNameMax: 'Category name should not be higher than 65 characters',
                categoryRequired: 'Category name cannot be empty',
                noCategory: 'Category list empty',
                tagName: 'Tag name can contain only letters and `&`',
                tagNameMax: 'Tag name should not be higher than 65 characters',
                tagRequired: 'Tag name cannot be empty',
                noTag: 'Tag list empty'
            }
        });
})();