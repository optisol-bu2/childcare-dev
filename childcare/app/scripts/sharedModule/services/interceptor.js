/*Interceptor to check every http transaction*/
(function() {
    'use strict';
    angular
        .module('menowApp')
        .factory("myHttpInterceptor", myHttpInterceptor);
    myHttpInterceptor.$inject = ['$q', '$location', '$cookies', '$rootScope'];

    function myHttpInterceptor($q, $location, $cookies, $rootScope) {
        return {
            // optional method
            'response': function(response) {
                // do something on success
                return response;
            },
            // optional method
            'responseError': function(rejection) {
                if ((rejection.status === 401 || rejection.status === 400) && (rejection.data.error_msg === 'Invalid Token!' || rejection.data.error_msg === 'No Authorization header was found')) {
                    console.log('yes');
                    var config = {
                        path: '/'
                    };
                    $rootScope.globals = {};
                    $cookies.remove('metoken');
                    $cookies.remove('meglobals');
                    $http.defaults.headers.common.Authorization = 'Bearer ';
                    $location.path('/');
                }
                return $q.reject(rejection);
            }


        };
    }

})();