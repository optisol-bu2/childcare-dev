// document.onscroll=onScroll;function onScroll(){var list=document.querySelectorAll("[x-sticky]");if(list.length>0){for(var i=0,item;item=list[i];i++){var bound=getBoundary(item);var edge=bound.getBoundingClientRect().bottom;var nextItem=findNextInBoundary(list,i,bound);if(nextItem){if(nextItem.parentNode.hasAttribute("x-sticky-placeholder")){nextItem=nextItem.parentNode;}edge=nextItem.getBoundingClientRect().top;}var hasHolder=item.parentNode.hasAttribute("x-sticky-placeholder");var rect=item.getBoundingClientRect();var height=rect.bottom-rect.top;var width=rect.right-rect.left;var top=hasHolder?item.parentNode.getBoundingClientRect().top:rect.top;if(top<0){if(edge>height){item.style.position="fixed";item.style.top="55px";}else{item.style.position="relative";item.style.top=-((top-edge)+height)+"px";}if(!hasHolder){var d=document.createElement("div");d.setAttribute("x-sticky-placeholder","");d.setAttribute("class","rate-card-sticky-box");d.style.height=height+"px";d.style.width=width+"px";item.parentNode.insertBefore(d,item);d.appendChild(item);}}else{item.style.position="relative";item.style.top="auto";if(hasHolder){item=item.parentNode;item.parentNode.insertBefore(item.firstChild,item);item.parentNode.removeChild(item);}}}}}function findNextInBoundary(arr,i,boundary){i++;for(var item;item=arr[i];i++){if(getBoundary(item)==boundary){return item;}}}function getBoundary(n){while(n=n.parentNode){if(n.hasAttribute("x-sticky-boundary")){return n;}}return document.body||document.documentElement;}


document.onscroll = onScroll;

function onScroll() {
    var list = document.querySelectorAll("[x-sticky]");
    if (list.length > 0) {
        for (var i = 0, item; item = list[i]; i++) {
            var bound = getBoundary(item);
            var edge = bound.getBoundingClientRect().bottom;
            var nextItem = findNextInBoundary(list, i, bound);
            if (nextItem) {
                if (nextItem.parentNode.hasAttribute("x-sticky-placeholder")) {
                    nextItem = nextItem.parentNode;
                }
                edge = nextItem.getBoundingClientRect().top;
            }
            var hasHolder = item.parentNode.hasAttribute("x-sticky-placeholder");
            var rect = item.getBoundingClientRect();
            var height = rect.bottom - rect.top;
            var width = rect.right - rect.left;
            var top = hasHolder ? item.parentNode.getBoundingClientRect().top : rect.top;
            if (top < 0) {
                if (edge > height) {
                    item.style.position = "fixed";
                    item.style.top = "55px";                 
                    item.style.width=   width + "px";  
                } else {
                    item.style.position = "relative";                    
                    item.style.top = -((top - edge) + height) + "px";   

                    // item.style.marginRight = "0px";
                }
                if (!hasHolder) {
                    var d = document.createElement("div");
                    d.setAttribute("x-sticky-placeholder", "");
                    d.setAttribute("class", "rate-card-sticky-box");
                    d.style.height = height + "px";
                    d.style.width = width + "px";
                    item.parentNode.insertBefore(d, item);
                    d.appendChild(item);
                }
            } else {
                item.style.position = "relative";
                item.style.top = "auto";
                if (hasHolder) {
                    item = item.parentNode;
                    item.parentNode.insertBefore(item.firstChild, item);
                    item.parentNode.removeChild(item);
                }
            }
        }
    }
}

function findNextInBoundary(arr, i, boundary) {
    i++;
    for (var item; item = arr[i]; i++) {
        if (getBoundary(item) == boundary) {
            return item;
        }
    }
}

function getBoundary(n) {
    while (n = n.parentNode) {
        if (n.hasAttribute("x-sticky-boundary")) {
            return n;
        }
    }
    return document.body || document.documentElement;
}