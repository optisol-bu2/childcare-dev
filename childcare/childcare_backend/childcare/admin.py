# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from childcare.models import Users

# Register your models here.

admin.site.register(Users)
