from rest_framework import serializers
from datetime import datetime
from childcare.models import (
    Users, JobType, JobAddress,
    JobTime_Budget, JobRequirements,
    Postal_code, Jobbid, Users_messages, User_settings,
    User_preference, User_availability, User_employee_document,
    User_employee_history, User_other_detail, User_reference,
    User_emergency_contact, User_review, User_job_views,
    User_bank_details, admin_messages, Payment_status,
    User_login_status, User_referal, User_reward_status,Job_status_log, Bulk_mail_send,Partners,
    client_offer_form)

# from childcare.models import()


class UsersSerializer(serializers.ModelSerializer):
    partnerid = serializers.StringRelatedField()
    class Meta:
        model = Users
        fields = ('id', 'first_name', 'email',
                  'last_name', 'login_token', 'purpose', 'profpic','login_count','partnerid')


class TrackListingField(serializers.RelatedField):
    def to_representation(self, value):
        # duration = time.strftime('%M:%S', time.gmtime(value.duration))
        return '%s' % (value)


class SignupSerializer(serializers.ModelSerializer):
    # country_code = serializers.StringRelatedField(many=False,read_only=False)

    class Meta:
        model = Users
        fields = '__all__'


class JobtypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobType
        fields = '__all__'


class JobAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobAddress
        fields = '__all__'


class JobTime_BudgetSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobTime_Budget
        fields = '__all__'


class JobRequirementsSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobRequirements
        fields = '__all__'


class PostalcodeSerialzier(serializers.ModelSerializer):

    class Meta:
        model = Postal_code
        fields = '__all__'


class JobbidSerialzier(serializers.ModelSerializer):

    class Meta:
        model = Jobbid
        fields = '__all__'


class GetMessages(serializers.ModelSerializer):

    class Meta:
        model = Users_messages
        fields = '__all__'


class Settings(serializers.ModelSerializer):

    class Meta:
        model = User_settings
        fields = '__all__'


class Preferences(serializers.ModelSerializer):

    class Meta:
        model = User_preference
        fields = '__all__'


class Availabilty(serializers.ModelSerializer):

    class Meta:
        model = User_availability
        fields = '__all__'


class Documents(serializers.ModelSerializer):

    class Meta:
        model = User_employee_document
        fields = '__all__'


class Employee_history(serializers.ModelSerializer):

    class Meta:
        model = User_employee_history
        fields = '__all__'


class Otherdetails(serializers.ModelSerializer):

    class Meta:
        model = User_other_detail
        fields = '__all__'


class Userreference(serializers.ModelSerializer):

    class Meta:
        model = User_reference
        fields = '__all__'


class UserEmergency(serializers.ModelSerializer):

    class Meta:
        model = User_emergency_contact
        fields = '__all__'


class Reviews(serializers.ModelSerializer):

    class Meta:
        model = User_review
        fields = '__all__'


class JobviewSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = User_job_views
        fields = '__all__'


class BankSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = User_bank_details
        fields = '__all__'


class adminmsgSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = admin_messages
        fields = '__all__'

    
class PaymentSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = Payment_status
        fields = '__all__'


class LoginstatusSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = User_login_status
        fields = '__all__'



class referalSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here

    class Meta:
        model = User_referal
        fields = '__all__'


class rewardSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here
    
    class Meta:
        model = User_reward_status
        fields = '__all__'

class JobStatusLogSerializer(serializers.ModelSerializer):
    # TODO: Define serializer fields here
    
    class Meta:
        model = Job_status_log
        fields = '__all__'


class BulkemailSerializer(serializers.ModelSerializer):

    class Meta(object):
        """docstring for Meta"""
        model = Bulk_mail_send
        fields = '__all__'
            

class ClientofferSerializer(serializers.ModelSerializer):

    class Meta:
        model = client_offer_form
        fields = '__all__'

class PartnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Partners
        fields = '__all__'