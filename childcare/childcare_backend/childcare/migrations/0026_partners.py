# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-07-10 13:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('childcare', '0025_auto_20180622_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partners',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('partner_name', models.CharField(max_length=200)),
                ('partner_email', models.CharField(max_length=200)),
                ('partner_mobileno', models.BigIntegerField()),
                ('partner_address', models.TextField()),
                ('partner_about_us', models.TextField()),
                ('partner_logo', models.CharField(blank=True, max_length=200, null=True)),
                ('partner_url_privacy_policy', models.CharField(max_length=200)),
                ('partner_url_terms_and_conditions', models.CharField(max_length=200)),
                ('partner_subdomain', models.CharField(max_length=200)),
                ('is_active', models.IntegerField(default=1)),
                ('is_delete', models.IntegerField(default=0)),
                ('partnership_with', models.IntegerField(default=0)),
                ('partnership_childcare_logo', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ('id',),
            },
        ),
    ]
