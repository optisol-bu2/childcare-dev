from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email
from decimal import Decimal
from childcare.serializers import GetMessages
from childcare.models import Message_reply, Message_subreply


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


# Sent message to the user
@csrf_exempt
def sent_message(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'from_id' in data:
            to_user = False
            try:
                from_user = Users.objects.get(id=data['from_id'])
                if data['message_type'] == 'private':
                    to_user = Users.objects.get(id=data['to_id'])
            except Users.DoesNotExist:
                result = {"Status": "404",
                          "message": "Either from or to user not exists"}
                return JSONResponse(result)
            serializer = GetMessages(data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                returnData = {}
                returnData['from_id__first_name'] = from_user.first_name
                returnData['from_id__id'] = from_user.id
                returnData['from_id__last_name'] = from_user.last_name
                try:
                    returnData['from_id_profpic'] = from_user.profpic.url
                except ValueError:
                    returnData['from_id_profpic'] = None
                if to_user:
                    returnData['to_id__first_name'] = to_user.first_name
                    returnData['to_id__last_name'] = to_user.last_name
                returnData['message'] = serializer.data['message']
                returnData['on_date_time'] = serializer.data['on_date_time']
                returnData['id'] = serializer.data['id']
                returnData['replies'] = []
                returnData['reply_count'] = 0
            else:
                result = {"Status": "500", "message": "Message Not added!!",
                          "errors": serializer.errors}
                return JSONResponse(result)
            result = {"Status": "200", "message": returnData}
            return JSONResponse(result)
        else:
            result = {"Status": "500",
                      "message": "From id required!!"}
            return JSONResponse(result)


# get user messages
@csrf_exempt
def get_messages(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'message_type' in data:
            if data['message_type'] == 'public':
                order_by = '-on_date_time'
                type_query = Q(message_type=data['message_type'])
            elif data['message_type'] == 'private':
                order_by = 'on_date_time'
                if 'to_id' in data:
                    type_query = Q(message_type=data['message_type'])\
                        & ((Q(to_id=data['to_id']) & Q(from_id=data['user_id'])) |
                           (Q(to_id=data['user_id']) & Q(from_id=data['to_id'])))
                else:
                    return JSONResponse({"Status": "500",
                                         "message": "To id required for private messaging"})
        else:
            return JSONResponse({"Status": "500",
                                 "message": "Message type is required"})
        model = Users_messages.objects\
            .filter(type_query, jobId=data['jobId'], is_delete=0).order_by(order_by)
        if model.count() > 0:
            res = []
            for m in model:
                temp = {}
                temp['id'] = m.id
                temp['from_id__id'] = m.from_id.id
                temp['from_id__first_name'] = m.from_id.first_name
                temp['from_id__last_name'] = m.from_id.last_name
                try:
                    temp['from_id_profpic'] = m.from_id.profpic.url
                except ValueError:
                    temp['from_id_profpic'] = None
                temp['on_date_time'] = m.on_date_time
                temp['message'] = m.message
                temp['replies'] = getReplies(m.id)
                temp['reply_count'] = len(temp['replies'])
                res.append(temp)
            result = {"Status": "200", "data": res}
            return JSONResponse(result)
        else:
            result = {"Status": "200",
                      "message": "No messages found!!", "data": []}
            return JSONResponse(result)


def getReplies(msgId):
    if msgId:
        model = Message_reply.objects.filter(message_id=msgId, is_delete=0).order_by('on_date_time')
        res = []
        if model.count > 0:
            for m in model:
                temp = {}
                temp['from_user_first_name'] = m.from_id.first_name
                temp['from_user_last_name'] = m.from_id.last_name
                temp['from_id__id'] = m.from_id.id
                try:
                    temp['from_id_profpic'] = m.from_id.profpic.url
                except ValueError:
                    temp['from_id_profpic'] = None
                temp['on_date_time'] = m.on_date_time
                temp['reply'] = m.reply
                temp['id'] = m.id
                temp['subreplies'] = getSubreplies(m.id)
                res.append(temp)
        return res
    else:
        return []


def getSubreplies(replyId):
    if replyId:
        model = Message_subreply.objects.filter(reply_id=replyId, is_delete=0)
        res = []
        if model.count > 0:
            for m in model:
                temp = {}
                temp['id'] = m.id
                temp['from_user_first_name'] = m.from_id.first_name
                temp['from_user_last_name'] = m.from_id.last_name
                temp['from_id__id'] = m.from_id.id
                try:
                    temp['from_id_profpic'] = m.from_id.profpic.url
                except ValueError:
                    temp['from_id_profpic'] = None
                temp['on_date_time'] = m.on_date_time
                temp['subreply'] = m.subreply
                res.append(temp)
        return res


@csrf_exempt
def sent_reply(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'messageId' in data:
            try:
                model = Users_messages.objects.get(id=data['messageId'])
            except Users_messages.DoesNotExist:
                return JSONResponse({"Status": "404",
                                     "message": "Message id not exists"})
            reply = Message_reply()
            reply.message_id = model
            try:
                reply.from_id = Users.objects.get(id=data['userid'])
            except Users.DoesNotExist:
                return JSONResponse({"Status": "404",
                                     "message": "user id not exists"})
            reply.reply = data['reply']
            reply.save()
            returnData = {}
            returnData['reply'] = data['reply']
            returnData['id'] = reply.id
            returnData['from_id__id'] = reply.from_id.id
            returnData['from_user_first_name'] = reply.from_id.first_name
            returnData['from_user_last_name'] = reply.from_id.last_name
            returnData['on_date_time'] = reply.on_date_time
            # returnData['from_id_profpic'] = reply.from_id.profpic.url
            try:
                returnData['from_id_profpic'] = reply.from_id.profpic.url
            except ValueError:
                returnData['from_id_profpic'] = None
            result = {"Status": "200",
                      "message": "reply saved",
                                 "reply": returnData}
            return JSONResponse(result)

        else:
            return JSONResponse({"Status": "500",
                                 "message": "message id required"})


@csrf_exempt
def sent_subreply(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'replyId' in data:
            try:
                model = Message_reply.objects.get(id=data['replyId'])
            except Users_messages.DoesNotExist:
                return JSONResponse({"Status": "404",
                                     "message": "reply id not exists"})
            reply = Message_subreply()
            reply.reply_id = model
            try:
                reply.from_id = Users.objects.get(id=data['userid'])
            except Users.DoesNotExist:
                return JSONResponse({"Status": "404",
                                     "message": "user id not exists"})
            reply.subreply = data['subreply']
            reply.save()
            returnData = {}
            returnData['subreply'] = data['subreply']
            returnData['from_id__id'] = reply.from_id.id
            returnData['from_user_first_name'] = reply.from_id.first_name
            returnData['from_user_last_name'] = reply.from_id.last_name
            returnData['on_date_time'] = reply.on_date_time
            returnData['id'] = reply.id
            # returnData['from_id_profpic'] = reply.from_id.profpic.url
            try:
                returnData['from_id_profpic'] = reply.from_id.profpic.url
            except ValueError:
                returnData['from_id_profpic'] = None
            return JSONResponse({"Status": "200",
                                 "message": "subreply saved", "subreply": returnData})

        else:
            return JSONResponse({"Status": "500",
                                 "message": "reply id required"})


@csrf_exempt
def delete_msg(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            message = Users_messages.objects.get(
                id=data['msgId'])
        except Users_messages.DoesNotExist:
            return JSONResponse({"Status": "404",
                                 "message": "Message id not exists"})
        message.is_delete = 1
        message.save()
        return JSONResponse({"Status": "200",
                             "message": "Message deleted"})


@csrf_exempt
def delete_reply(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            message = Message_reply.objects.get(
                id=data['replyId'])
        except Message_reply.DoesNotExist:
            return JSONResponse({"Status": "404",
                                 "message": "reply id not exists"})
        message.is_delete = 1
        message.save()
        return JSONResponse({"Status": "200",
                             "message": "Message deleted"})


@csrf_exempt
def delete_subreply(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            message = Message_subreply.objects.get(
                id=data['subreplyId'])
        except Message_subreply.DoesNotExist:
            return JSONResponse({"Status": "404",
                                 "message": "Subreply id not exists"})
        message.is_delete = 1
        message.save()
        return JSONResponse({"Status": "200",
                             "message": "Message deleted"})


@csrf_exempt
def get_private_users(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'jobtypeid' not in data:
            result = {"Status": "403",
                      "message": "Attribute jobtypeid required"}
            return JSONResponse(result)
        search_query = Q()
        if 'searchKey' in data:
            if data['searchKey'] != '':
                search_query = Q(userid__first_name__icontains=data['searchKey'])\
                    | Q(userid__last_name__icontains=data['searchKey'])

        model = Jobbid.objects\
            .values(
                'on_date_time', 'userid__id',
                'userid__first_name', 'userid__last_name', 'is_confirmed',
                'id', 'userid__profpic')\
            .annotate(
                    sort_order_true=Case(
                        When(is_confirmed=1, then=('is_confirmed')),
                        default=None
                    ),
                    sort_order_false=Case(
                        When(is_confirmed=0, then=('on_date_time')),
                        default=None
                    )
            )\
            .filter(search_query, jobtypeid=data['jobtypeid']).order_by(
                '-sort_order_true',
                '-sort_order_false',
            )
        for key, m in enumerate(model):
            model[key]['un_read_count'] = Users_messages.objects\
                .filter(from_id=m['userid__id'], is_read=0,
                        message_type='private', jobId=data['jobtypeid']).count()
        result = {"Status": "200", "data": model}
        return JSONResponse(result)


@csrf_exempt
def read_messages(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'jobtypeid' not in data and 'userid' not in data:
            result = {"Status": "403",
                      "message": "Attribute jobtypeid and userid required"}
            return JSONResponse(result)
        Users_messages.objects.filter((Q(from_id=data['userid'])
                                       | Q(to_id=data['userid'])) & Q(jobId=data['jobtypeid']))\
            .update(is_read=1)
        return JSONResponse({"Status": "200",
                             "message": "Read successful"})
