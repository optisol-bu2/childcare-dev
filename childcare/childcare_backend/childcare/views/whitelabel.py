from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email, userprofile
from random import randint

from childcare.models import (
    User_availability, User_preference,
    User_employee_document, User_employee_history, User_other_detail,
    Household_chores, Languages, User_reference, User_emergency_contact,
    admin_messages)

from childcare.serializers import (Preferences, Availabilty,
                                   Documents, Employee_history, Otherdetails,
                                   Userreference, UserEmergency, adminmsgSerializer)

import json


from django.contrib.auth.decorators import user_passes_test

from django.shortcuts import redirect

# def our_decorator(func):
#     def function_wrapper(x):
#         data=JSONParser().parse(x)
#         data['url'] = 'getPartnerDetails/'
#         try:
#             user = Users.objects.get(id=10,login_token='9lzXUTyOyRag7PkojB0QF399Pd38AG9t')
#         except Users.DoesNotExist:
#             return HttpResponse(False)
#         return redirect("/getPartnerList/")
#     return function_wrapper
     


# def foo(x):
#     print("Hi, foo has been called with " + str(x))

# foo("Hi")

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


#server add/update   
@csrf_exempt
def partnerAddUpdate(request,format=None):
    if request.method== "POST":
        data = request.POST
        
        # return HttpResponse('test')
        # is_valid = users.authenticate(data['userId'],data['login_token'])
        action = " "
        if  True:
            if 'file' in request.FILES:
                file = request.FILES.get('file')
                # filename, extension = os.path.splitext(file.name)
                # file.name = 'partner_logo-' + str(randint(0, 9999)) + extension
                # # email.attach(file.name, file.read(), file.content_type)
                # final_filename = file.name
                res = handle_uploaded_file(
                        request.FILES['file'], data['partner_logo'])
            model = False
            # data['partner_logo'] = final_filename
            if 'id' in data:
                if data['id'] is not None and data['id']!='':
                    try:
                        model = Partners.objects.get(id=data['id'],is_active=1,is_delete=0)
                    except Partners.DoesNotExist:
                        result = {"Status": "404",
                              "Message": "Partner not found."}
                        return JSONResponse(result) 
                    serializers = PartnerSerializer(model, data=data, many=False)
                    action = "updated"
                else:
                    result = {"Status": "404",
                              "Message": "PartnerId required."}
                    return JSONResponse(result)  
            else:
                datas = data
                # print(datas)
                serializers = PartnerSerializer(data=datas, many=False)
                action = "added"
            if serializers.is_valid():
                if model:
                    # dir_name = os.getcwd() + "/../app/partner_logo/"
                    csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
                    if os.path.isfile(csv_path):
                        # print(csv_path)
                        os.remove(csv_path)
                serializers.save()
                result = {"Status": "200",
                          "Message": "Partner details  "+action+" successfully."}
                return JSONResponse(result)
            else:
                result = {"Status": "404",
                          "Message": "Partner Doest not added.","serializer_error":serializers.errors}
                return JSONResponse(result)
        else:
            result={'Status':'500','Message':'User is not valid'}
            return JSONResponse(result)
    else:
        result={'Status':'500','Message':'Method not allowed.'}
        return JSONResponse(result)


#get partners list   
@csrf_exempt
# @our_decorator
def getPartnerList(request,format=None):
    if request.method== "POST":
        data=JSONParser().parse(request)
        # str1 = data['partner'].find('company_logo')
        # str2 =data['partner'].find('company_logo')+21
        # logo_img = data['partner'][str1:str2]
        # session = Session()
        # session['logo_img'] = logo_img
        if 'pageno' not in data:
            result = {"Status": '500',
                      "Message": "pagenumber cannot be empty"}
            return JSONResponse(result)
        else:
            page_no = data['pageno']
            limit = data['limit']
            if page_no == 1:
                start_limit = 0
                end_limit = limit
                # return HttpResponse(limit)
            elif page_no > 1:
                start_limit = limit * (page_no - 1)
                end_limit = limit + start_limit
        model = Partners.objects\
                .filter(is_active=1,is_delete=0).values()[start_limit:end_limit]
        total_count = Partners.objects\
                .filter(is_active=1,is_delete=0).count()
        if model.exists():
            result={'Status':'200','Message':'Success','data':list(model),"total":total_count}
            return JSONResponse(result)
        else:
            result={'Status':'404','Message':'No record found'}
            return JSONResponse(result) 
        # return serializer.data
    else:
        result={'Status':'500','Message':'Method not allowed.'}
        return JSONResponse(result)


# def handle_uploaded_file(f, image_name):
#     dir_name = os.getcwd() + "/../partner_logo/"
#     with open(dir_name + image_name, "wb") as fh:
#         fh.write(f.decode('base64'))
#         fh.close()
#     return True
# from PIL import Image
# basewidth = 364
# dir_name = os.getcwd() + "/../app/partner_logo/"
# img = Image.open(dir_name + 'no_image_1.png')
# wpercent = (basewidth/float(img.size[0]))
# hsize = 87
# img = img.resize((basewidth,hsize), Image.ANTIALIAS)
# img.save(dir_name + 'no_image_2.png')

# from PIL import Image
def handle_uploaded_file(f, image_name):
    dir_name = os.getcwd() + "/../dist/partner_logo/"
    with open(dir_name + image_name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()
    
    # # it is for backend
    # dir_name = os.getcwd() + "/images/"
    # with open(dir_name + image_name, 'wb+') as destination:
    #     for chunk in f.chunks():
    #         destination.write(chunk)
    #     destination.close()
    # basewidth = 150
    # img = Image.open(dir_name + image_name)
    # wpercent = (basewidth / float(img.size[0]))
    # hsize = int((float(img.size[1]) * float(wpercent)))
    # img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    # img.save(dir_name +'resized_image12.jpg')


    # baseheight = 87
    # img = Image.open(dir_name + image_name)
    # hpercent = (baseheight / float(img.size[1]))
    # wsize = int((float(img.size[0]) * float(hpercent)))
    # img = img.resize((wsize, baseheight), Image.ANTIALIAS)
    # img.save(dir_name +'resized_image1.jpg')
    return HttpResponse(f)

@csrf_exempt
def deletePartner(request):
    if request.method == "POST":
        data=JSONParser().parse(request)
        try:
            model = Partners.objects.get(id=data['id'])
        except Partners.DoesNotExist:
            result = {"Status": "500", "Message": "Partner not found."}
            return JSONResponse(result)
        model.is_delete = 1
        model.is_active = 1
        model.save()
        result = {"Status": "200", "Message": "Partner deleted successfully."}
        return JSONResponse(result)
    else:
        result = {"Status": "500", "Message": "Method not allowed."}
        return JSONResponse(result)


@csrf_exempt
def getPartnerDetails(request):
    if request.method == "POST":
        data=JSONParser().parse(request)

        try:
            model = Partners.objects.values().get(id=data['partnerId'])
        except Partners.DoesNotExist:
            result = {"Status": "500", "Message": "Partner not found."}
            return JSONResponse(result)
        result = {"Status": "200", "Message": "Success",'partner':model}
        return JSONResponse(result)
    else:
        result = {"Status": "500", "Message": "Method not allowed."}
        return JSONResponse(result)
        

@csrf_exempt
def whitelabelDetails(request):
    if request.method == "POST":
        data=JSONParser().parse(request)
        try:
            model = Partners.objects.values().get(partner_subdomain=data['hostName'])
        except Partners.DoesNotExist:
            result = {"Status": "500", "Message": "Partner not found."}
            return JSONResponse(result)
        dir_name = os.getcwd() + "/../dist/partner_logo/" + model['partner_logo']
        # reader = list(csv.reader(f))
        # ofile = open(dir_name, "r")
        # handle_uploaded_file1(ofile.read(),'logo.png')
        result = {"Status": "200", "Message": "Success",'data':model}
        return JSONResponse(result)
    else:
        result = {"Status": "500", "Message": "Method not allowed."}
        return JSONResponse(result)


@csrf_exempt
def checksubdomain(request):
    if request.method == "POST":
        data=JSONParser().parse(request)
        if 'partner' not in data:
            result = {"Status": "500", "Message": "Datas required."}
            return JSONResponse(result)
        if 'id' in data['partner']:
            editQuery = ~Q(id=data['partner']['id'])
        else:
            editQuery = Q()
        try:
            model = Partners.objects.get(editQuery,partner_subdomain=data['partner']['partner_subdomain'])
        except Partners.DoesNotExist:
            result = {"Status": "500", "Message": "Partner not found."}
            return JSONResponse(result)
        # dir_name = os.getcwd() + "/../app/partner_logo/" + model['partner_logo']
        # reader = list(csv.reader(f))
        # ofile = open(dir_name, "r")
        # handle_uploaded_file1(ofile.read(),'logo.png')
        result = {"Status": "200", "Message": "Success"}
        return JSONResponse(result)
    else:
        result = {"Status": "500", "Message": "Method not allowed."}
        return JSONResponse(result)

# def handle_uploaded_file1(f, image_name):
#     dir_name = os.getcwd() + "/images/"
#     with open(dir_name + image_name, 'wb+') as destination:
#         for chunk in f.chunks():
#             destination.write(chunk)
#         destination.close()

#     # basewidth = 80
#     # img = Image.open(dir_name + 'no_image_1.png')
#     # wpercent = (basewidth/float(img.size[0]))
#     # hsize = 80
#     # img = img.resize((basewidth,hsize), Image.ANTIALIAS)
#     # img.save(dir_name + 'no_image_2.png')
#     return HttpResponse(f)






