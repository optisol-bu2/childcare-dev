from .imports import *
import zipfile 
from childcare.views import userlogin as users
from childcare.views import send_email
from decimal import Decimal

from django.db.models import Case, Sum, Max, When, Value, IntegerField, F, CharField
import datetime as dt
from childcare.models import admin_user, User_referal, User_employee_document

from childcare.serializers import PaymentSerializer, referalSerializer, ClientofferSerializer
import re

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def admin_login(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            model = admin_user.objects.get(
                email=data['email'], is_delete=0)
            password = check_password(
                data['password'], model.password)
        except admin_user.DoesNotExist:
            data = {'Status': '404',
                    'message': 'email is incorrect'}
            return JSONResponse(data)
        if password:
            if model.is_active == 0:
                result = {
                    "Status": "500", "message": "Admin is not activated please contact admin"}
                return JSONResponse(result)
            elif model.is_delete == 1:
                result = {"Status": "500",
                          "message": "Admin is deleted please contact admin"}
                return JSONResponse(result)
            else:
                result = {"Status": "200",
                          "message": "Login successful", "userid": model.id}
                return JSONResponse(result)
        else:
            data = {'Status': '404',
                    'message': 'password is incorrect'}
            return JSONResponse(data)


@csrf_exempt
def view_all_jobs(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'pageno' not in data or 'limit' not in data:
            result = {"Status": '500',
                      "Message": "pagenumber & limit cannot be empty"}
            return JSONResponse(result)
        else:
            limit = data['limit']
            page_no = data['pageno']
            if page_no == 1:
                start_limit = 0
                end_limit = limit
            elif page_no > 1:
                start_limit = limit * (page_no - 1)
                end_limit = limit + start_limit
        if data['is_admin'] == True:
            userid = Q()
            job_status = data['job_status']
            order_by = ['-job_type_id__on_date_time']
            default = ~Q(job_type_id__user_id=None) & Q(job_type_id__is_active=1) & Q(
                job_type_id__is_delete=0)
            if 'userid' in data:
                if data['userid']:
                    userid = Q(job_type_id__jobbid__userid=data[
                        'userid'])
            if job_status == 1:  # applied jobs
                filter_applied = Q(job_type_id__job_status=1)
                order_by = ['job_type_id__job_status', '-job_type_id__updated']
            elif job_status == 2:  # confirmed jobs
                filter_applied = Q(job_type_id__job_status=2,
                                   job_type_id__jobbid__is_confirmed=1) & Q(job_type_id__is_mailed=1)
            elif job_status == 3:  # Completed jobs
                filter_applied = Q(
                    job_type_id__jobbid__is_confirmed=1) & Q(
                    job_type_id__job_status=3) & Q(
                    job_type_id__jobbid__is_confirmed=1) & Q(job_type_id__is_mailed=1)
            elif job_status == 4:  # Paid jobs
                filter_applied = Q(
                    job_type_id__jobbid__is_confirmed=1)\
                    & Q(job_type_id__job_status=4) & Q(job_type_id__is_mailed=1)
            elif job_status == 7:  # Closed jobs
                filter_applied = Q(job_type_id__job_status=7) & Q(
                    job_type_id__is_mailed=1)
            elif job_status == -1:
                filter_applied = Q()
            else:
                filter_applied = ~Q(
                    job_type_id__jobbid__is_confirmed=0)\
                    & Q(job_type_id__is_confirmed=0) & Q(job_type_id__is_mailed=1)
            # print(filter_applied)

            address_model = JobAddress.objects.filter(
                default & userid & filter_applied)\
                .select_related()\
                .order_by(*order_by)[start_limit:end_limit]
            total_count = JobAddress.objects.filter(
                default & userid & filter_applied).count()
            res = []
            percent = Percent_Config.objects.all().first()
            for m in address_model:
                temp = {}
                temp['jobid'] = m.job_type_id.id
                try:
                    temp['user_id'] = m.job_type_id.user_id.id
                except Users.DoesNotExist:
                    continue
                temp['user_name'] = m.job_type_id.user_id.first_name + \
                    " " + m.job_type_id.user_id.last_name
                if m.job_type_id.user_id.profpic:
                    temp['client_profilepic'] = m.job_type_id.user_id.profpic.url
                else:
                    temp['client_profilepic'] = ""
                temp['applied_job'] = 0
                temp['quoted'] = 0
                temp['all_jobs'] = 0
                temp['is_bidded'] = False
                temp['is_confirmed'] = 0
                temp['job_status'] = m.job_type_id.job_status
                temp['unsuccessful'] = 0
                if m.before_after == 1:
                    temp['care_name'] = m.care_name
                elif m.before_after == 0:
                    temp['care_name'] = "Caregiver's home(Family Daycare)"
                if m.location == "0":
                    temp['care_name'] = "Job posted by a parent"
                    if m.before_after == 1:
                        temp['care_name'] = m.care_name
                    temp['address'] = m.address
                    temp['city'] = m.city
                    temp['post_code'] = m.post_code
                    temp['state'] = m.state
                temp['location'] = m.location
                temp['bidded_users'] = len(getbiddedUsers(m.job_type_id.id))
                temp['job_view_count'] = get_job_viewers(m.job_type_id.id)
                # single line if else
                temp[
                    'job_type'] = m.job_type_id.job_type
                caregivers = m.job_type_id.caregiver_type.split(
                    ',')
                caregivers = map(lambda x: x.title(), caregivers)
                temp['caregiver_type'] = ' / '.join(caregivers)
                temp['case_session'] = m.job_type_id.care_session.split(',')
                temp['care_type'] = m.job_type_id.care_type.split(',')
                temp['no_of_children'] = m.no_of_children
                temp['children_age'] = m.children_age
                temp['time_budget'] = gettime_budget(m.job_type_id.id)
                rate_per_hour = (Decimal(percent.x) / Decimal(100)) * \
                    temp['time_budget']['rate_per_hour']
                if temp['time_budget'] is not None:
                    total_cost = Decimal(temp['time_budget'][
                                         'hours_required']) * Decimal(temp['time_budget']['rate_per_hour'])
                    # cus_service = (Decimal(percent.x) / Decimal(100)) * total_cost
                    # online_pay1 = (Decimal(percent.y) / Decimal(100)) * total_cost
                    # gst = (Decimal(percent.z) / Decimal(100)) * total_cost
                    # client_profit = (Decimal(5) / Decimal(100)) * \
                    #     Decimal(temp['time_budget']['budget'])
                    # Estimated = round(temp['time_budget'][
                    #                   'budget'] - float(client_profit), 2)
                    # temp['time_budget']['budget'] = Estimated
                    temp['time_budget']['rate_per_hour'] = Decimal(
                        temp['time_budget']['rate_per_hour']) - rate_per_hour
                    temp['time_budget']['budget'] = total_cost
                try:
                    requirements = JobRequirements.objects.get(
                        job_type_id=m.job_type_id.id)
                    reqs = requirements.requirements.split(',')
                    # req_serializer = JobRequirementsSerializer(
                    #     requirements, many=False)
                    req_model = Requirements.objects.values(
                        'requirement', 'icon_class').filter(id__in=reqs)
                    temp['req'] = req_model
                    temp['other_requirement'] = requirements.other_requirement
                except JobRequirements.DoesNotExist:
                    temp['req'] = None
                res.append(temp.copy())
            result = {"Status": "200", "data": res,
                      "total_count": total_count, "count": len(res)}
            return JSONResponse(result)

        else:
            result = {"Status": "500",
                      "message": "Logged user must be admin to continue"}
            return JSONResponse(result)


# Get bidded users
def getbiddedUsers(jobtypeid):
    percent = get_taxes()
    model = Jobbid.objects.values(
        'rate_per_hour', 'quote', 'comment',
        'on_date_time', 'userid__id',
        'userid__first_name', 'userid__last_name',
        'userid__contactno', 'userid__email', 'is_confirmed',
        'id', 'userid__profpic')\
        .annotate(cus_service=(Decimal(percent.x) / Decimal(100)) * F('quote'),
                  online_pay=(Decimal(percent.y) / Decimal(100)) * F('quote'),
                  gst=(Decimal(percent.z) / Decimal(100)) * F('quote'))\
        .annotate(funds_transfered=F('cus_service') + F('online_pay') + F('gst') + F('quote'))\
        .filter(jobtypeid=jobtypeid)
    return model


def get_taxes():
    return Percent_Config.objects.all().first()


def get_job_viewers(jobid):
    return User_job_views.objects.filter(job_type_id=jobid).count()


# get Time and buget for a given job type id

def gettime_budget(jobid):
    if jobid:
        try:
            model = JobTime_Budget.objects.values(
                'date', 'time', 'days_required', 'rate_per_hour', 'hours_required', 'budget').get(job_type_id=jobid)
        except JobTime_Budget.DoesNotExist:
            return None
        model['days_required'] = model['days_required'].split(',')
        model['time'] = time.strftime(model['time'], '%H:%M')
        print(model)
        return model


# Get bid information
def getBidinfo(jobid):
    if jobid:
        try:
            model = Jobbid.objects.values(
                'rate_per_hour', 'quote', 'comment', 'on_date_time').get(jobtypeid=jobid)
        except Jobbid.DoesNotExist:
            return {}
        return model


# get users list
@csrf_exempt
def get_user_list(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'mode' in data:
            search_query = Q()
            mode_query = Q()
            selectUserquery = Q()
            end_limit = 10
            selectUser = False
            default = Q(is_active=1, is_delete=0, is_admin=0,partnerid=data['partnerid'])
            if data['mode'] == 'client':
                mode_query = Q(purpose='0') | Q(purpose='parent')
            elif data['mode'] == 'worker':
                mode_query = Q(purpose='1') | Q(purpose='worker')
            else:
                result = {"Status": "500", "message": "Invalid User mode"}
                return JSONResponse(result)
            # if "selectUserId" in data:
            #     default = default | Q(id=int(data['selectUserId']))
            if 'search_key' in data:
                # I = data['search_key']
                # s = re.search(r"\d+(\.\d+)?", I)
                # if s is not None:
                #     if s.group(0) != '':
                #         # print(len(s.group(0)))
                #         intLength = int(len(s.group(0))) + 1
                #         # print(len(str(data['search_key'])))
                #         # data['search_key'] = str(data['search_key'])[intLength:len(str(data['search_key']))]
                #         idsearch = Q(id__icontains=s.group(0)) & (Q(first_name__icontains=data['search_key'])\
                #     | Q(last_name__icontains=data['search_key']))
                #     else:
                #         idsearch = Q()
                # else:
                    # idsearch = Q()

                search_query = Q(first_name__icontains=data['search_key'])\
                    | Q(last_name__icontains=data['search_key']) | Q(user_name__istartswith=data['search_key'])
            if 'selectUserId' in data:
                selectUser = Users.objects.annotate(text=Concat(Value('#'),'id', Value(' '),'first_name',Value(' '),'last_name',output_field=CharField())).filter(default,id=data['selectUserId']).values()[0]
                selectUserquery = ~Q(id__in=[data['selectUserId']])

                # end_limit = end_limit + 1
            model = Users.objects.values('id', 'first_name', 'last_name')\
                .annotate(user_name=Concat('id', Value(' '),'first_name',Value(' '),'last_name',output_field=CharField()))\
                .filter(search_query & mode_query & default & selectUserquery).order_by('-updated')[:end_limit]
            # return HttpResponse(model.query)
            totalcount = model.count()
            model = list(model)
            for m in model:

                m['text'] = '# ' + str(m['id']) + " " + \
                    str(m['first_name']) + " " + str(m['last_name'])
            if selectUser:
                model.append(selectUser)

            result = {"Status": "200", "results": model}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "Invalid User mode"}
            return JSONResponse(result)


@csrf_exempt
def close_job(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'jobId' in data:
            try:
                default = Q(is_active=1, is_delete=0, is_mailed=1)
                model = JobType.objects.get(default, id=data['jobId'])
            except JobType.DoesNotExist:
                result = {"Status": "404",
                          "message": "Job id does not exists"}
                return JSONResponse(result)
            if 'userid' in data:
                user = Users.objects.get(
                    id=data['userid'], is_admin=1, is_active=1, is_delete=0)
            else:
                result = {"Status": "500",
                          "message": "Userid required to continue"}
                return JSONResponse(result)
            model.job_status = 7
            model.save()
            result = {"Status": "200", "message": "Job Closed!!"}
            return JSONResponse(result)
        else:
            result = {"Status": "500",
                      "message": "Job id is required to continue"}
            return JSONResponse(result)


@csrf_exempt
def save_payment_status(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        payment_serializer = PaymentSerializer(data=data, many=False)
        if payment_serializer.is_valid():
            payment_serializer.save()
            result = {"Status": "200", "message": "Payment status saved"}
            return JSONResponse(result)
        else:
            result = {"Status": "500",
                      "errors": payment_serializer.errors}
            return JSONResponse(result)


class BlockUnblockjob():

    def block_job(self, jobid):
        try:
            model = JobType.objects.get(id=jobid,is_blocked=0)
        except JobType.DoesNotExist:
            result = {"Status": "404",
                      "message": "Jobid doesnot exists"}
            return (result)
        model.is_blocked = 1
        model.save()
        result = {"Status": "200",
                  "message": "Job blocked","block_status":model.is_blocked}
        return (result)

    def un_block_job(self, jobid):
        try:
            model = JobType.objects.get(id=jobid,is_blocked=1)
        except JobType.DoesNotExist:
            result = {"Status": "404",
                      "message": "Jobid doesnot exists"}
            return (result)
        model.is_blocked = 0
        model.save()
        result = {"Status": "200",
                  "message": "Job unblocked","block_status":model.is_blocked}
        return (result)


@csrf_exempt
def req_block_job(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'jobid' in data and 'action' in data:
            blocker = BlockUnblockjob()
            if data['action'] == 1:
                result = blocker.block_job(data['jobid'])
            elif data['action'] == 0:
                result = blocker.un_block_job(data['jobid'])
            else:
                result = {"Status": "400","message": "UNKNOWN_ACTION"}
            return JSONResponse(result)
        else:
            result = {"Status": "200",
                      "message": "Jobid and action is required"}
            return JSONResponse(result)

@csrf_exempt
def change_cred_status(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'status' in data and 'userid' in data:
            model = User_employee_history.objects.get(id=data['id'])
            model.status = data['status']
            model.save()
            result = {"Status": "200",
                      "message": "Credentials status changed"}
            return JSONResponse(result)
        else:
            result = {"Status": "400","message": "Status, userid and id is required"}
            return JSONResponse(result)


@csrf_exempt
def refer_friend(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'user_email' in data:
            try:
                user = Users.objects.get(email=data['user_email'],is_delete=0,is_active=1)
            except Users.DoesNotExist:
                result = {"Status": "404",
                        "message": "Your referral request has been declined user does not exist"}
                return JSONResponse(result)
            friend = Users.objects.filter(email=data['friend_email'],is_delete=0,is_active=1)
            already_refered = User_referal.objects.filter(friend_email=data['friend_email'])
            if not friend.exists():
                if not already_refered:
                    data['referer'] = user.id
                    profile_suits = data['profile_suits']
                    data['profile_suits'] = ','.join(str(x) for x in profile_suits)
                    serializer = referalSerializer(data=data,many=False)
                    if serializer.is_valid():
                        serializer.save()
                        signer = Signer()
                        signed_value = signer.sign(serializer.data['id'])
                        referal_id = ''.join(signed_value.split(':')[1:])
                        data['id'] = serializer.data['id']
                        data['referal_id'] = referal_id
                        send_email.referal_email(data)
                        result = {"Status":"200","message":"Your referral request has been sent successfully",
                                "referal_id":referal_id}
                        return JSONResponse(result)
                    else:   
                        result = {"Status": "500","errors":serializer.errors}
                        return JSONResponse(result)
                else:
                    result = {"Status":"400",
                            "message":"Your referral request has been declined the user already referred"}
                    return JSONResponse(result)
            else:
                result = {"Status":"400","message":"Your referred email is already in our network."}
                return JSONResponse(result)
        else:
            result = {"Status": "400","message": "User email is required"}
            return JSONResponse(result)

@csrf_exempt
def make_vetted(request):
    if request.method == "POST":
        data = JSONParser().parse(request)

        if 'userid' in data:
            try:
                user = Users.objects.get(id=data['userid'],is_active=1,is_delete=0)
            except Users.DoesNotExist:
                result = {"Status": "404","message": "User id Does not exists"}
                return JSONResponse(result)
            user.is_vetted = data['is_vetted']
            user.save()
            result = {"Status": "200","message": "User marked as vetted",'is_vetted':user.is_vetted}
            return JSONResponse(result)
        else:
            result = {"Status": "400","message": "User id is required"}
            return JSONResponse(result)

@csrf_exempt
def change_req_status(request):
    if request.method == "POST":
        data = JSONParser().parse(request)

        if 'userid' in data:
            try:
                user = Users.objects.get(id=data['userid'],is_active=1,is_delete=0)
            except Users.DoesNotExist:
                result = {"Status": "404","message": "User id Does not exists"}
                return JSONResponse(result)
            if user.is_admin == 1:
                try:
                    model = User_employee_document.objects.get(id=data['id'])
                except User_employee_document.DoesNotExist:
                    result = {"Status": "400","message": "Requirement id does not exists"}
                    return JSONResponse(result)
                model.status = data['status']
                model.updated_by = int(data['userid'])
                model.save()
                result = {"Status": "200","message": "Requirement status updated!!","status":model.status}
                return JSONResponse(result)
            else:
                result = {"Status": "400","message": "User is not an admin"}
                return JSONResponse(result)
        else:
            result = {"Status": "400","message": "User id is required"}
            return JSONResponse(result)

@csrf_exempt
def getUserDetails(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'userid' in data:
            try:
                user = Users.objects.get(id=data['userid'],is_active=1,is_delete=0)
            except Users.DoesNotExist:
                result = {"Status": "404","message": "User id Does not exists"}
                return JSONResponse(result)
            if user.login_token is None:
                user.login_token = get_random_string(length=32)
                user.save()
            serializer = UsersSerializer(user)
            result = {"Status": "200","message": "Success",'user_details':serializer.data}
            return JSONResponse(result)
        else:
            result = {"Status": "400","message": "User id is required"}
            return JSONResponse(result)

@csrf_exempt
def userBlock(request):
    if request.method == "POST":
        data = JSONParser().parse(request)

        if 'userid' in data and 'is_block' in data:
            try:
                user = Users.objects.get(id=data['userid'],is_active=1,is_delete=0)
            except Users.DoesNotExist:
                result = {"Status": "404","message": "User id Does not exists"}
                return JSONResponse(result)
            user.is_blocked = data['is_block']
            # if user.is_blocked == 1:
            #     user.is_active = 0
            # else:
            #     user.is_active = 1
            user.save()
            result = {"Status": "200","message": "User Block Status Changed","is_block":user.is_blocked}
            return JSONResponse(result)
        else:
            result = {"Status": "400","message": "User id is required"}
            return JSONResponse(result)


# @csrf_exempt
# #emport excel for the payment report page
def exportXlsForCarer(queryset):
    
    carerreport = []
    filename = "Carer reports.xls"
    # filename = today.strftime('%d-%m-%Y %H:%M:%S') + " monthreport.csv"
    # print(usertable)
    # i = 0
    # usertable = list(usertable)
    keys = []
    default_req = Requirements.objects.all()
    print(default_req.exists())
    # print(default_req)
    for key in queryset:
        temp = {}
        temp['Carer Id'] = key['id']
        temp['Carer Name'] = key['first_name']+ " "+key['last_name']
        temp['Email Id'] = key['email']
        temp['Mobile Number'] = key['contactno']
        temp['Profile completeness'] = key['completeness']
        user_rvw = User_review.objects.filter(to_user=key['id']).annotate(count=Count('id')).aggregate(Sum('star'))
        total_count = User_review.objects.filter(to_user=key['id']).count()
        if user_rvw['star__sum'] is not None:
            overall_reviews = user_rvw['star__sum'] / total_count
        else:
            overall_reviews = 0
        temp['Carer Rating'] = overall_reviews
        temp['Signup date'] =  dt.datetime.strftime(key['signup_date'], '%d-%m-%Y')
        if key['updated'] is not None:
            temp['Last sign in date & time'] = dt.datetime.strftime(key['updated'], '%d-%m-%Y %H:%M:%S')
        else:
            temp['Last sign in date & time'] = " "
        # temp['Last sign in date & time'] = str(key['updated'])
        temp['Profile picture']  = 'No' if key['profpic'] is None or key['profpic'] == '' else 'Yes'
        temp['Vetted'] = key['vetted']
        temp['Blocked'] = key['blocked']
        temp['Year of Birth'] = key['year_of_birth']
        temp['Gender'] = key['gender']
        temp['Age'] = key['age']
        temp['Address'] = key['address']
        temp['City'] = key['city']
        temp['Relevant Experience year'] = key['relevant_year']
        temp['Preferred Age group of children'] = key['user_preference__children_age']
        temp['Caregiver Type'] = key['user_preference__caregivers']
        jobtype = ''
        if key['user_preference__job_type'] != '' and key['user_preference__job_type'] is not None:
            jobtypes = key['user_preference__job_type'].split(',')
        # print('casual' in jobtypes,'regular' in jobtypes)
        if 'casual' in jobtypes:
            jobtype = jobtype +'Casual Care, '
        if 'regular' in jobtypes:
            jobtype = jobtype+','+'Regular Care, '
        temp['Job type'] = jobtype
        Availability = User_availability.objects.filter(userid=key['id'])
        if Availability.exists(): 
            before_schoolArr = Availability[0].before_school.split(',')
            for b in before_schoolArr:
                if 'Before School Care Availability' in temp:
                    if temp['Before School Care Availability'] is not None:
                        temp['Before School Care Availability'] = temp['Before School Care Availability'] +getWeekDay(b)+ ', '
                else:
                    # print(getWeekDay(b))
                    temp['Before School Care Availability'] = getWeekDay(b)+ ', '


            day_careArr = Availability[0].day_care.split(',')
            for dy in day_careArr:
                if 'Day care Availability' in temp:
                    if temp['Day care Availability'] is not None:
                        temp['Day care Availability'] = temp['Day care Availability'] +getWeekDay(dy)+ ', '
                else:
                    temp['Day care Availability'] = getWeekDay(dy)+ ', '


            after_schoolArr = Availability[0].after_school.split(',')
            for a in after_schoolArr:
                if 'After School Care Availability' in temp:
                    if temp['After School Care Availability'] is not None:
                        temp['After School Care Availability'] = temp['After School Care Availability'] +getWeekDay(a)+ ', '
                else:
                    temp['After School Care Availability'] = getWeekDay(a)+ ', '


            overnightArr = Availability[0].overnight.split(',')
            overnight = " "
            for o in overnightArr:
                if 'Overnight Care Availability' in temp:
                    if temp['Overnight Care Availability'] is not None:
                        temp['Overnight Care Availability'] = temp['Overnight Care Availability'] +getWeekDay(o)+ ', '
                else:
                    temp['Overnight Care Availability'] = getWeekDay(o)+ ', '

            if 'Overnight Care Availability' not in temp:
                temp['Overnight Care Availability'] = " "
            if 'After School Care Availability' not in temp:
                temp['After School Care Availability'] = " "
            if 'Day care Availability' not in temp:
                temp['Day care Availability'] = " "
            if 'Before School Care Availability' not in temp:
                temp['Before School Care Availability'] = " "

        else:
            temp['Before School Care Availability'] = " "
            temp['Day care Availability'] = " "
            temp['After School Care Availability'] = " "
            temp['Overnight Care Availability'] = " "


        bankDetails = User_bank_details.objects.filter(userid=key['id'])
        if bankDetails.exists(): 
            temp['Bank Name']  = bankDetails[0].bank_name
            temp['BSB Number'] = str(bankDetails[0].bsb_no)
            temp['Account Number'] = bankDetails[0].account_no
        else:
            temp['Bank Name'] = " "
            temp['BSB Number'] = " "
            temp['Account Number'] = " "

        requirements = User_employee_document.objects.filter(userid=key['id'],is_delete=0)

        for d in default_req:
            temp[d.requirement] = "No"
            temp[d.requirement + ' Verification'] = 'Not Verified'
            if requirements.exists():
                for r in requirements:
                    try:
                        r.requirement_id.id
                        if d.id == r.requirement_id.id:
                            temp[r.requirement_id.requirement] = 'Yes' 
                        if r.status == 3 and d.id == r.requirement_id.id:
                            temp[r.requirement_id.requirement + ' Verification'] =  'Need Info' 
                        elif r.status == 1 and d.id == r.requirement_id.id:
                            temp[r.requirement_id.requirement + ' Verification'] = 'Not Verified'
                        elif r.status == 2 and d.id == r.requirement_id.id:
                            temp[r.requirement_id.requirement + ' Verification'] = 'Verified'
                    except Requirements.DoesNotExist:
                        pass
                        
        carerreport.append(temp)
    requirement = []
    requirement_verification = []
    for d in default_req:
        requirement_verification.append(d.requirement + ' Verification')
        requirement.append(d.requirement)

    keys = ['Carer Id','Carer Name','Email Id','Mobile Number','Carer Rating','Signup date','Profile completeness','Last sign in date & time','Profile picture','Vetted','Blocked',\
    'Year of Birth','Gender','Age','Address','City','Relevant Experience year',\
    'Preferred Age group of children','Caregiver Type','Job type','Before School Care Availability','Day care Availability','After School Care Availability','Overnight Care Availability',\
    'Bank Name','BSB Number','Account Number'] + requirement + requirement_verification
 
    # # print(paymentreport)
    # # # keys = ['S No','Job id', 'Client id', 'Client Name',\
    # # #  'Carer Id', 'Carer Name', 'Job start date', 'Job start time', 'Job status', 'Approved quote', \
    # # #  'Fund transfered','Completed date','Completed time','Confirmed date','Confirmed time', 'Payment released date', 'Payment released time']
    # # with open(os.getcwd() + "/../optisolhr/Excel/Month/"+filename, 'wb') as output_file:
    # #     dict_writer = csv.DictWriter(output_file, mergedlist)
    # #     dict_writer.writeheader()
    # #     dict_writer.writerows(monthreport)
    # # return '/Excel/Month/'+filename
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')


    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = keys

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = carerreport
    # print(monthreport[0])
    for row in rows:

        row_num += 1
        i = 0 
        for col_num in columns:
            ws.write(row_num, i, row[col_num], font_style)
            i = i + 1

    wb.save(response)
    
    # response = HttpResponse(content_type='application/zip')
    # response['Content-Disposition'] = 'attachment; filename=backup.csv.zip'

    # z = zipfile.ZipFile(response,'w')   ## write zip to response
    # z.writestr("filename.csv", output.getvalue())  ## write csv file to zip
    return response


def exportXlsForClient(queryset):
    clientreport = []
    filename = "Payment reports.xls"
    # filename = today.strftime('%d-%m-%Y %H:%M:%S') + " monthreport.csv"
    # print(usertable)
    # i = 0
    # print(list(usertable).keys())
    keys = []
    default_req = Requirements.objects.all()
    for key in queryset:
        temp = {}
        temp['Client Id'] = key.id
        temp['Client Name'] = key.first_name+ " "+key.last_name
        temp['Email Id'] = key.email
        temp['Mobile Number'] = key.contactno
        temp['Signup date'] = dt.datetime.strftime(key.signup_date, '%d-%m-%Y')
        user_rvw = User_review.objects.filter(to_user=key.id).annotate(count=Count('id')).aggregate(Sum('star'))
        total_count = User_review.objects.filter(to_user=key.id).count()
        if user_rvw['star__sum'] is not None:
            overall_reviews = user_rvw['star__sum'] / total_count
        else:
            overall_reviews = 0
        temp['Client Rating'] = overall_reviews
        if key.updated is not None:
            temp['Last sign in date & time'] = dt.datetime.strftime(key.updated, '%d-%m-%Y %H:%M:%S')
        else:
            temp['Last sign in date & time'] = " "
        temp['Profile picture']  = 'No' if key.profpic is None or key.profpic == '' else 'Yes'
        temp['Blocked'] = 'Unblocked' if key.is_blocked == 0 else 'Blocked'
        job_post = JobType.objects.filter(is_blocked=0,user_id=key.id,is_active=1,is_delete=0)
        if job_post.exists():
            temp['Number of jobs posted'] = job_post.count()
        else:
            temp['Number of jobs posted'] = 0
        # temp['User type'] = 'Parent' if key.purpose == '0' or key.purpose == 'parent' else 'Worker'
        clientreport.append(temp)    

    # for k,t in paymentreport[0].iteritems():
    #     keys.append(k)
    # requirement = []
    # requirement_verification = []
    # for d in default_req:
    #     requirement_verification.append(d.requirement + ' Verification')
    #     requirement.append(d.requirement)

    keys = ['Client Id','Client Name','Email Id','Mobile Number','Client Rating','Signup date','Last sign in date & time','Profile picture','Blocked','Number of jobs posted']\
           

 
    # print(paymentreport)
    # # keys = ['S No','Job id', 'Client id', 'Client Name',\
    # #  'Carer Id', 'Carer Name', 'Job start date', 'Job start time', 'Job status', 'Approved quote', \
    # #  'Fund transfered','Completed date','Completed time','Confirmed date','Confirmed time', 'Payment released date', 'Payment released time']
    # with open(os.getcwd() + "/../optisolhr/Excel/Month/"+filename, 'wb') as output_file:
    #     dict_writer = csv.DictWriter(output_file, mergedlist)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(monthreport)
    # return '/Excel/Month/'+filename
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')


    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = keys

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = clientreport
    # print(monthreport[0])
    for row in rows:

        row_num += 1
        i = 0 
        for col_num in columns:
            ws.write(row_num, i, row[col_num], font_style)
            i = i + 1

    wb.save(response)
    return response
#emport excel for the payment report page
def exportPaymentXls(queryset):
    paymentreport = []
    filename = "Payment reports.xls"
    # filename = today.strftime('%d-%m-%Y %H:%M:%S') + " monthreport.csv"
    i = 0
    for m in queryset:
        temp = {}
        i = i + 1
        temp['S No'] = i
        temp['Job id'] = m['job_id']
        temp['Client id'] = m['client_id']
        temp['Client Name'] = m['client_name']
        temp['Carer Id'] = m['carer_id']
        temp['Carer Name'] = m['carer_name']
        temp['Job start date'] = dt.datetime.strftime(m['job_start_date'], '%d-%m-%Y')
        temp['Job start time'] = str(m['job_start_time'])
        if m['job_status'] == 2:
            temp['Job status'] = 'Confirmed'
        elif m['job_status'] == 3:
            temp['Job status'] = 'Completed'
        elif m['job_status'] == 4:
            temp['Job status'] = 'Paid'
        else:
            temp = {}
            i = i - 1
            continue
        temp['Approved quote'] = m['approved_quote']
        temp['Fund transfered'] = m['funds_transfered'] 

        if m['completed_date_time'] is not None:
            temp['Completed date'] = dt.datetime.strftime(
                    m['completed_date_time'], '%d-%m-%Y')
            temp['Completed time'] = str(m['completed_date_time'].time())
        else:
            temp['Completed date'] = '-'
            temp['Completed time'] = '-'

        if m['confirmed_date_time'] is not None:
            temp['Confirmed date'] = dt.datetime.strftime(
                    m['confirmed_date_time'], '%d-%m-%Y')
            temp['Confirmed time'] = str(m['confirmed_date_time'].time())
        else:
            temp['Confirmed date'] = '-'
            temp['Confirmed time'] = '-'

        if m['paid_date_time'] is not None:
            temp['Payment released date'] = dt.datetime.strftime(
                    m['paid_date_time'], '%d-%m-%Y')
            temp['Payment released time'] = str(m['paid_date_time'].time())
        else:
            temp['Payment released date'] = '-'
            temp['Payment released time'] = '-'

        # else:
        #     temp['Completed date'] = '-'
        #     temp['Completed time'] = '-'
        # if m['job_status'] == 4:
        #     temp['Payment released date'] = dt.datetime.strftime(
        #             m['on_date_time'], '%d-%m-%Y')
        #     temp['Payment released time'] = str(m['on_date_time'].time())
        # else:
        #     temp['Payment released date'] = '-'
        #     temp['Payment released time'] = '-'
        paymentreport.append(temp)
    keys = ['S No','Job id', 'Client id', 'Client Name',\
     'Carer Id', 'Carer Name', 'Job start date', 'Job start time', 'Job status', 'Approved quote', \
     'Fund transfered','Completed date','Completed time','Confirmed date','Confirmed time', 'Payment released date', 'Payment released time']
    # with open(os.getcwd() + "/../optisolhr/Excel/Month/"+filename, 'wb') as output_file:
    #     dict_writer = csv.DictWriter(output_file, mergedlist)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(monthreport)
    # return '/Excel/Month/'+filename
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = keys

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = paymentreport
    # print(monthreport[0])
    for row in rows:

        row_num += 1
        i = 0 
        for col_num in columns:
            ws.write(row_num, i, row[col_num], font_style)
            i = i + 1

    wb.save(response)
    return response

@csrf_exempt
def add_client_offer(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        serializer = ClientofferSerializer(data=data,many=False)
        if serializer.is_valid():
            serializer.save()
            send_email.client_offer_mail(data)
            result = {"Status": "200","message": "Client offer added"}
            return JSONResponse(result)
        else:
            result = {"Status": "500","message": "Errors found!! Not added","errors":serializer.errors}
            return JSONResponse(result)


@csrf_exempt
def getCarerReport(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        date_query = Q()
        if 'from_date' in data and 'to_date' in data:
            data['from_date'] = dt.datetime.strptime(data['from_date'],'%d-%m-%Y')
            data['to_date'] = dt.datetime.strptime(data['to_date'],'%d-%m-%Y')
            date_query = Q(signup_date__range=[data['from_date'],data['to_date']])
        usertable = Users.objects.filter(date_query,Q(purpose='1') | Q(purpose='worker'),is_active=1,is_delete=0,user_preference__is_delete=0).values('id','first_name','last_name','email','contactno','signup_date','completeness','profpic','year_of_birth', 'gender','age','city',\
                                                        'address', 'relevant_year','updated','user_preference__job_type','user_preference__caregivers',\
                                                        'user_preference__children_age','user_other_detail__interest','user_other_detail__household_chores','user_other_detail__languages').annotate(partner_sub_domain=F('partnerid__partner_subdomain'),vetted=Case(When(Q(is_vetted=1),
                                                               then=Value('Vetted')),
                                                          When(Q(is_vetted=0),then=Value('unvetted')),
                                                          default=Value('unvetted'),
                                                          output_field=CharField()
                                                          ),blocked=Case(When(Q(is_blocked=1),
                                                               then=Value('Blocked')),
                                                          When(Q(is_blocked=0),then=Value('Unblocked')),
                                                          default=Value('Unblocked'),
                                                          output_field=CharField()
                                                          ))
        if usertable.exists():
            return exportXlsForCarer(usertable)
        else:
            result = {"No Records Found"}
            return JSONResponse(result)

@csrf_exempt
def getClientReport(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        date_query = Q()
        if 'from_date' in data and 'to_date' in data:
            data['from_date'] = dt.datetime.strptime(data['from_date'],'%d-%m-%Y')
            data['to_date'] = dt.datetime.strptime(data['to_date'],'%d-%m-%Y')
            date_query = Q(signup_date__range=[data['from_date'],data['to_date']])
        usertable = Users.objects.filter(date_query,Q(purpose='0') | Q(purpose='parent'),is_active=1,is_delete=0).annotate(partner_sub_domain=F('partnerid__partner_subdomain'),blocked=Case(When(Q(is_blocked=1),
                                                               then=Value('Blocked')),
                                                          When(Q(is_blocked=0),then=Value('Unblocked')),
                                                          default=Value('Unblocked'),
                                                          output_field=CharField()
                                                          ))
        if usertable.exists():
            return exportXlsForClient(usertable)
        else:
            result = {"No Records Found"}
            return JSONResponse(result)
        # return HttpResponse(True)


def get_job_viewers(jobid):
    return User_job_views.objects.filter(job_type_id=jobid).count()



@csrf_exempt
def getPaymentReport(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'usertype' in data and 'adminId' in data: 
            jobbid_status = Jobbid.objects.values('id').annotate(client_name=Concat('jobtypeid__user_id__first_name', Value(' '),'jobtypeid__user_id__last_name'),\
                carer_name= Concat('userid__first_name', Value(' '),'userid__last_name'),\
                job_id= F('jobtypeid'),\
                job_start_date = F('jobtypeid__jobtime_budget__date'),
                job_start_time = F('jobtypeid__jobtime_budget__time'),
                job_status = F('jobtypeid__job_status'),
                client_id = F('jobtypeid__user_id__id'),
                carer_id = F('userid'),
                on_date_time = F('on_date_time'),
                approved_quote = F('quote'),
                funds_transfered=F('funds_transfered'),
                completed_date_time=F('completed_date_time'),
                confirmed_date_time=F('confirmed_date_time'),
                paid_date_time=F('paid_date_time')
                ).filter(is_confirmed=1)
            
            if jobbid_status.exists():
                exportExcel = exportPaymentXls(jobbid_status)
            # serializer = JobStatusLogSerializer(job_status,many=True)
                return exportExcel
            else:
                result = {"No Records Found"}
                return JSONResponse(result)
        else:
            result = {"Status": "400","message": "adminId is required"}
            return JSONResponse(result)

def getWeekDay(day=None):
    if day is None and day == "":
        return " "
    else:
        if day in ['0','1','2','3','4','5','6']:
            day = int(day)
        else:
            return " "
        if day == 0:
            return "Mon"
        if day == 1:
            return "Tue"
        if day == 2:
            return "Wed"
        if day == 3:
            return "Thur"
        if day == 4:
            return "Fri"
        if day == 5:
            return "Sat"
        if day == 6:
            return "Sun"


@csrf_exempt
def getJobReport(request):
    # print('this condition')
    if request.method == "POST":
        data = JSONParser().parse(request)
        print('tst')
        date_query = Q()
        if 'from_date' in data and 'to_date' in data:
            data['from_date'] = dt.datetime.strptime(data['from_date'],'%d-%m-%Y')
            data['to_date'] = dt.datetime.strptime(data['to_date'],'%d-%m-%Y')
            ondate_filter_range = (
                        # The start_date with the minimum possible time
                        dt.datetime.combine(data['from_date'], dt.datetime.min.time()),
                        # The start_date with the maximum possible time
                        dt.datetime.combine(data['to_date'], dt.datetime.max.time())
                    )
            date_query = Q(on_date_time__range=ondate_filter_range)
        jobtypetable = JobType.objects.filter(date_query,is_active=1,is_delete=0).values().annotate(posted_date=F('on_date_time'),\
                        job_start_date = F('jobtime_budget__date'),
                        job_start_time = F('jobtime_budget__time'),client_id=F('user_id__id'),client_name=Concat('user_id__first_name', Value(' '), 'user_id__last_name'),\
                        client_email= F('user_id__email'),client_mobile_no=F('user_id__contactno'),\
                        profpic=F('user_id__profpic'),
                        job_type=F('job_type'),
                        caregiver_type=F('caregiver_type'),
                        care_type=F('care_type'),
                        care_session=F('care_session'),
                        childcare_centre=F('jobaddress__before_after'),
                        childcare_centre_name=F('jobaddress__care_name'),
                        childcare_centre_address=F('jobaddress__address'),
                        client_address=F('jobaddress__address'),
                        location_city=F('jobaddress__city'),
                        location_state=F('jobaddress__state'),
                        location__post_code=F('jobaddress__post_code'),
                        # childcare_centre_location=Concat('jobaddress__city', Value(' '), 'jobaddress__post_code',Value(' '), 'jobaddress__state',output_field=CharField()),
                        # client_location=Concat('jobaddress__city', Value(' '), 'jobaddress__post_code',Value(' '), 'jobaddress__state' ,output_field=CharField()),
                        no_of_children=F('jobaddress__no_of_children'),
                        age_group_children=F('jobaddress__children_age'),
                        no_of_hours_required=F('jobtime_budget__hours_required'),
                        rate_per_hour=F('jobtime_budget__rate_per_hour'),
                        other_details=F('jobrequirements__other_requirement'),
                        requirements=F('jobrequirements__requirements'),
                        care_provide=F('jobaddress__location'),
                        days_required=F('jobtime_budget__days_required')
                        )
        # print(jobtypetable)
        # return HttpResponse('test')
        if jobtypetable.exists():
            # return JSONResponse(list(jobtypetable))
            return exportXlsForJobType(jobtypetable)
        else:
            result = {"No Records Found"}
            return JSONResponse(result)


def exportXlsForJobType(queryset):
    # return queryset
    jobreport = []
    filename = "Carer reports.xls"
    # filename = today.strftime('%d-%m-%Y %H:%M:%S') + " monthreport.csv"
    # print(usertable)
    # i = 0
    # usertable = list(usertable)
    keys = []
    default_req = Requirements.objects.all()
    for key in queryset:
        temp = {}
        temp['Job ID'] = key['id']
        temp['Posted Date & Time'] = dt.datetime.strftime(key['on_date_time'],'%d-%m-%Y %H:%M:%S')
        if key['job_start_date'] is not None and  key['job_start_time'] is not None:
            temp['Start Date & Time'] = dt.datetime.strftime(key['job_start_date'],'%d-%m-%Y')+" "+str(key['job_start_time'])
            # temp['Start Date & Time'] = ' '
        else:
            temp['Start Date & Time'] = ' '
        temp['Number of job views'] = get_job_viewers(key['id'])
        bidded_users = Jobbid.objects.filter(jobtypeid=key['id'])
        if bidded_users.exists():
            temp['Number of applicants'] = bidded_users.count()
        else:
            temp['Number of applicants'] = 0
        temp['Client ID'] = key['client_id']
        temp['Client Name'] = key['client_name']
        temp['Email Id'] = key['client_email']
        # temp['Mobile Number']
        temp['Mobile Number'] = key['client_mobile_no']
        user_rvw = User_review.objects.filter(to_user=key['client_id']).annotate(count=Count('id')).aggregate(Sum('star'))
        total_count = User_review.objects.filter(to_user=key['client_id']).count()
        if user_rvw['star__sum'] is not None:
            overall_reviews = user_rvw['star__sum'] / total_count
        else:
            overall_reviews = 0
        temp['Client Rating'] = overall_reviews
        temp['Profile picture']  = 'No' if key['profpic'] is None or key['profpic'] == '' else 'Yes'
        temp['Job'] = 'Blocked' if key['is_blocked'] == 1 else "Unblocked"
        # if key['job_type'] != '' and key['job_type'] is not None:
        jobtype = 'Casual Care' if key['job_type'] == '0' else "Regular Care"
        # print('casual' in jobtypes,'regular' in jobtypes)
        # if 'casual' in jobtypes:
        #     jobtype = jobtype +'Casual Care, '
        # if 'regular' in jobtypes:
        #     jobtype = jobtype+','+'Regular Care, '
        temp['Job type'] = jobtype
        temp['Caregiver Type'] = key['caregiver_type']
        temp['Care type'] = key['care_type']
        temp['Care session'] = key['care_session']
        temp['Childcare Centre'] = 'Yes' if key['childcare_centre'] == 1 else 'No'
        temp['Childcare Centre Name'] = key['childcare_centre_name']
        temp['Childcare Centre Address'] = key['childcare_centre_address']
        if key['location_city'] is not None and key['location_city'] != '':
            temp['Childcare Centre location'] = key['location_city']+', '+key['location_state']+', '+str(key['location__post_code'])
            temp['Client location'] = key['location_city']+', '+key['location_state']+', '+str(key['location__post_code'])
        else:
            temp['Childcare Centre location'] = " "
            temp['Client location'] = " "

        temp['Care provide'] = 'Caregivers home' if key['care_provide'] == '1' else 'Our Location'
        temp['Client Address'] = key['client_address']
        temp['No of children'] = key['no_of_children']
        temp['Age group children'] = key['age_group_children']
        temp['Days Required'] = key['days_required']
        temp['No of hours required'] = key['no_of_hours_required']
        temp['Rate for hour'] = key['rate_per_hour']
        temp['Other Details'] = key['other_details']
        requirements = key['requirements'].split(
                    ',')
        temp['requirements'] = ''
        for d in default_req:
            if str(d.id) in requirements:
                if 'requirements' in temp:
                    if temp['requirements'] is not None:
                        temp['requirements'] = temp['requirements'] +str(d.requirement)+ ', '
                else:
                    temp['requirements'] = str(d.requirement)+ ', '
        if temp['Childcare Centre'] == 'Yes':
            temp['Client Address'] = ""    
            temp['Client location'] = " "
        else:
            temp['Childcare Centre Address'] = ""
            temp['Childcare Centre location'] = ""



        # temp
        jobreport.append(temp.copy())
    keys = ['Job ID','Posted Date & Time','Start Date & Time','Number of job views','Number of applicants','Client ID','Client Name','Email Id',\
    'Mobile Number','Client Rating','Profile picture','Job','Job type','Caregiver Type','Care type','Care session','Childcare Centre',\
    'Childcare Centre Name','Childcare Centre Address','Childcare Centre location','Care provide','Client Address','Client location','No of children',\
    'Age group children','Days Required','No of hours required','Rate for hour','requirements']
 
    # # print(paymentreport)
    # # # keys = ['S No','Job id', 'Client id', 'Client Name',\
    # # #  'Carer Id', 'Carer Name', 'Job start date', 'Job start time', 'Job status', 'Approved quote', \
    # # #  'Fund transfered','Completed date','Completed time','Confirmed date','Confirmed time', 'Payment released date', 'Payment released time']
    # # with open(os.getcwd() + "/../optisolhr/Excel/Month/"+filename, 'wb') as output_file:
    # #     dict_writer = csv.DictWriter(output_file, mergedlist)
    # #     dict_writer.writeheader()
    # #     dict_writer.writerows(monthreport)
    # # return '/Excel/Month/'+filename
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="users.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')


    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = keys

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = jobreport
    # print(monthreport[0])
    for row in rows:

        row_num += 1
        i = 0 
        for col_num in columns:
            ws.write(row_num, i, row[col_num], font_style)
            i = i + 1

    wb.save(response)
    return response




