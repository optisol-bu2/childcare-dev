from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email

from childcare.models import User_preference, User_referal
from childcare.serializers import adminmsgSerializer, rewardSerializer, Preferences, Settings


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def user_signup(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            data['password'] = make_password(data['password'])
            try:
                partner_domain = Partners.objects.get(partner_subdomain=data['hostName'])     
            except Partners.DoesNotExist:
                data['partnerid'] = 1
            data['partnerid'] = partner_domain.id
            # profile_suits = ''
            message = True
            if 'selectedProfile' in data:
                if len(data['selectedProfile']):
                    care_type = list(data['selectedProfile'])
                    data['profile_suits'] = ','.join(care_type)
                    profile_suits = data['profile_suits']
            try:
                model = Users.objects.get(
                    email=data['email'], activation_email=1)
            except Users.DoesNotExist:
                serializer = SignupSerializer(data=data, many=False)
                if serializer.is_valid():
                    have_job = False
                    serializer.save()
                    if data['purpose'] == '1':
                        pref = {
                            "userid":serializer.data['id'],
                            "caregivers":profile_suits
                        }
                        prefSerializer = Preferences(data=pref,many=False)
                        if prefSerializer.is_valid():
                            prefSerializer.save()
                        else:
                            print(prefSerializer.errors)
                    if 'jobtypeid' in data:
                        data['jobtypeid'] = data['jobtypeid']
                        have_job = True
                        # have_job = update_job_type(
                        #     serializer.data['id'], data['jobtypeid'])
                    else:
                        data['jobtypeid'] = -1
                    if 'message' in data:
                        data['message']['from_user'] = serializer.data['id']
                        adminSerializer = adminmsgSerializer(
                            data=data['message'], many=False)
                        if adminSerializer.is_valid():
                            adminSerializer.save()
                            message = True
                        else:
                            message = adminSerializer.errors
                    reward_status = 0
                    is_mailed = sent_email(
                        data['email'], have_job, serializer.data['id'],data['jobtypeid'])
                    if is_mailed['Status'] == '200':
                        if 'referal_id' in data:
                            referal_id = data['referal_id']
                            try:
                                referal = User_referal.objects\
                                    .get(referal_url=referal_id,
                                         friend_email=data['email'],
                                         status=0)
                            except User_referal.DoesNotExist:
                                pass
                            referal.status = 1
                            referal.save()
                            reward = {}
                            reward['userid'] = referal.referer.id
                            reward['reward_type'] = 1
                            reward['referal_id'] = referal.id
                            reward['status'] = 1
                            reward['amount'] = 20
                            reward_status = save_user_reward(reward)
                        if 'signup_mode' in data:
                            if data['signup_mode'] == 'ad1' or data['signup_mode'] == 'ad2':
                                reward = {}
                                reward['userid'] = serializer.data['id']
                                reward['reward_type'] = 0
                                reward['status'] = 1
                                reward['amount'] = 50
                                reward['ad_url'] = data['signup_url']
                                reward_status = save_user_reward(reward)
                        set_user_settings(serializer.data['id'],data['purpose'])
                        
                        result = {"Status": "200",
                                  "message": "Signup successful mail sent",
                                  "havejob": have_job, "message_admin": message,
                                  "reward_status": reward_status}
                        return JSONResponse(result)
                    elif is_mailed['Status'] == "400":
                        return JSONResponse(is_mailed)
                    elif is_mailed['Status'] == "404":
                        return JSONResponse(is_mailed)

                else:
                    result = {"Status": "500", "errors": serializer.errors}
                    return JSONResponse(result)
            result = {"Status": "500",
                      "message": "User email already exists!!"}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def checkemail(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # return JSONResponse(data)
        try:
            Users.objects.get(email=data['email'], activation_email=1,partnerid=data['partnerid'])
        except Users.DoesNotExist:
            result = {"Status": "200", "message": "valid email"}
            return JSONResponse(result)
        result = {"Status": "500", "message": "email already exists"}
        return JSONResponse(result)


def sent_email(email, have_job, id, jobId):
    try:
        model = Users.objects.get(email=email, is_active=0, is_delete=0)
    except Users.DoesNotExist:
        return {"Status": "404", "message": "User Does Not Exist", "exists": False}
    except Users.MultipleObjectsReturned:
        duplicate_data = Users.objects.get(id=id)
        duplicate_data.delete()
        return {"Status": "400", "message": "User email id is already exists", "exists": True}
    email = model.email
    emp_id = model.id
    signer = Signer()
    signed_value = signer.sign(email)
    key = ''.join(signed_value.split(':')[1:])
    act_link = model.partnerid.partner_subdomain+'/' + "#/confirmemail?id=" + \
        str(emp_id) + "&token=" + key + "&email=" + email + "&name=" + \
        str(model.first_name) + "&login_token=" + str(model.login_token)\
        + "&have_job=" + str(have_job) + "&jobid=" + str(jobId)
    emp_name = model.first_name + " " + model.last_name
    from_email = 'admin@lovelychildcare.com'

    logo_image = os.getcwd() + "/../dist/partner_logo/"+model.partnerid.partner_logo
    logo_top_image = os.getcwd() + "/../dist/partner_logo/"+model.partnerid.partner_logo
    # banner_img = os.getcwd() + "/images/bigImg.jpg"
    # fb = os.getcwd() + "/images/facebook.png"
    # gplus = os.getcwd() + "/images/googleplus.png"
    # linkedin = os.getcwd() + "/images/linkedin.png"
    # twitter = os.getcwd() + "/images/twitter.png"
    # center_img = os.getcwd() + "/images/center.png"
    # left_img = os.getcwd() + "/images/left.png"
    # shadow = os.getcwd() + "/images/shadow.png"
    with open(logo_image, 'rb') as logo:
        image_logo = logo.read()
    logo_inline_image = InlineImage(
        filename="logo.png", content=image_logo)
    with open(logo_top_image, 'rb') as logo:
        image_logo_top = logo.read()
    logo_top_image = InlineImage(
        filename="logo.png", content=image_logo_top)
    # with open(banner_img, 'rb') as bg:
    #     image_bg = bg.read()
    # banner_im = InlineImage(
    #     filename="bigImg.png", content=image_bg)
    # with open(fb, 'rb') as bg:
    #     image_fb = bg.read()
    # fb_im = InlineImage(
    #     filename="facebook.png", content=image_fb)
    # with open(gplus, 'rb') as gp:
    #     image_gplus = gp.read()
    # gplus_im = InlineImage(
    #     filename="googleplus.png", content=image_gplus)
    # with open(linkedin, 'rb') as lk:
    #     image_linkedin = lk.read()
    # linkedin_im = InlineImage(
    #     filename="linkedin.png", content=image_linkedin)
    # with open(twitter, 'rb') as tw:
    #     image_twitter = tw.read()
    # twitter_im = InlineImage(
    #     filename="twitter.png", content=image_twitter)
    # with open(center_img, 'rb') as tw:
    #     image_center = tw.read()
    # center_img_im = InlineImage(
    #     filename="center.png", content=image_center)
    # with open(left_img, 'rb') as tw:
    #     image_left = tw.read()
    # left_img_im = InlineImage(
    #     filename="left.png", content=image_left)
    # with open(shadow, 'rb') as tw:
    #     image_shadow = tw.read()
    # shadow_im = InlineImage(
    #     filename="left.png", content=image_shadow)
    # print(bg_image)
    send_templated_mail(
        template_name='signup_welcome_latest',
        from_email=from_email,
        recipient_list=[email],
        context={
            'username': email,
            'emp_name': emp_name,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'unsubscribe_url': model.partnerid.partner_subdomain+'/'+"#/settings"
            # 'banner_img': banner_im,
            # 'fb': fb_im,
            # 'twitter': twitter_im,
            # 'linkedin': linkedin_im,
            # 'gplus': gplus_im,
            # 'center':center_img_im,
            # 'left':left_img_im,
            # 'shadow':shadow_im
        },
    )
    model.activation_email = 1
    model.save()
    return {"Status": "200", "message": "mail sent successfully"}


@csrf_exempt
def activate_user(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'email' in data:
            try:
                model = Users.objects.get(email=data['email'])
            except Users.DoesNotExist:
                result = {"Status": "500", "message": "User not found!!!"}
                return JSONResponse(result)
            if model.is_active == 0:
                model.is_active = 1
                model.login_count += 1
                model.save()
                if 'jobid' in data:
                    job_update =update_job_type(model.id,data['jobid'],data['partnerid'])
                serializer = UsersSerializer(model)
                result = {"Status": "200", "message": "User activated!!",
                          "user_details": serializer.data}
                return JSONResponse(result)
            elif model.is_active == 1:
                result = {"Status": "201",
                          "message": "User account already activated!!"}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "email is required"}
            return JSONResponse(result)


def update_job_type(usrid, jobtypeid):
    if usrid and jobtypeid:
        try:
            model = JobType.objects.get(id=jobtypeid)
        except JobType.DoesNotExist:
            return "jobid Not found"
        model.user_id = Users.objects.get(id=usrid)
        model.is_active = 1
        model.save()
        mail_status = send_email.job_mail(jobtypeid)
        if mail_status is True:
            model.is_mailed = 1
        # print(mail_status)

        return True


def save_user_reward(data):
    if data:
        serializer = rewardSerializer(data=data, many=False)
        if serializer.is_valid():
            serializer.save()
            return True
        else:
            return serializer.errors


# Enable all the email whenever user signup
def set_user_settings(userid,purpose):
    if userid and purpose:
        data = {
            'userid':userid
        }
        serializer = Settings(data=data,many=False)
        if serializer.is_valid():
            serializer.save()
            return True
        else:
            print serializer.errors
            return serializer.errors
    else:
        return False