from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email
from childcare.serializers import Settings


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def getReviews(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'pageno' not in data or 'limit' not in data:
            result = {"Status": '500',
                      "Message": "pagenumber & limit cannot be empty"}
            return JSONResponse(result)
        else:
            limit = data['limit']
            page_no = data['pageno']
            if page_no == 1:
                start_limit = 0
                end_limit = limit
                # return HttpResponse(limit)
            elif page_no > 1:
                start_limit = limit * (page_no - 1)
                end_limit = limit + start_limit
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if 'userid' in data:
            try:
                model = User_review.objects.values().annotate(username=Concat('from_user__first_name', Value(' '), 'from_user__last_name'),profpic=F('from_user__profpic'),jobid=F('jobtypeid__id'),rate_per_hour=F('jobtypeid__jobtime_budget__rate_per_hour'),touser_profpic=F('to_user__profpic'))\
                        .filter(to_user=data['userid']).order_by('-on_date_time')[start_limit:end_limit]
            except User_review.DoesNotExist:
                model = None
            if model:
                user_rvw = User_review.objects.filter(to_user=data['userid']).annotate(count=Count('id')).aggregate(Sum('star'))
                total_count = User_review.objects.filter(to_user=data['userid']).count()
                overall_reviews = user_rvw['star__sum'] / total_count
                result = {"Status": "200", "reviews": model,"total_count":total_count,"overall_reviews":overall_reviews}
            else:
                result = {"Status": "404", "reviews": {}}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "Userid is required."}
            return JSONResponse(result)


