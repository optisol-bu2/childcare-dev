from .imports import *

from childcare.models import Country_codes
from childcare.views import send_email


from django.db.models import Case, Sum, Max, When, Value, IntegerField
import datetime as dt

from django.core.signing import Signer
from django.core import signing

from childcare.views import logging_status
from childcare.serializers import adminmsgSerializer
MASTER_KEY=get_random_string(32)

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def authenticate(usrid, login_token):
    try:
        Users.objects.get(id=usrid, login_token=login_token)
        return True
    except Users.DoesNotExist:
        return False


@csrf_exempt
def validate(request, format=None):
    if request.POST:
        # if 'partnerid' not in request.POST:
        #     partner_id = 1
        # else:
        #     partner_id = request.POST.get('partnerid')
        try:
            model = Users.objects.get(
                email=request.POST.get('email'), is_delete=0)
            password = check_password(
                request.POST.get('password'), model.password)
        except Users.DoesNotExist:
            data = {'Status': '404',
                    'message': 'Either username or password is incorrect'}
            return JSONResponse(data)
        if model and password:
            encrypted_email = get_email_token(model.email,model.id)
            if model.is_admin == 1 and model.purpose == '2':
                try:
                    latest_client = Users.objects\
                        .filter(is_active=1, is_delete=0, is_admin=0,purpose='0',partnerid=request.POST.get('partnerid'))\
                        .latest('updated')
                    serializer = UsersSerializer(model)
                    result = {"Status": "200",
                              "message": "Admin login successful", "is_admin": True,
                              "latest_client": latest_client.id,
                              "admin_id":model.id,"token":encrypted_email,"user_details":serializer.data}
                except Users.DoesNotExist:
                    serializer = UsersSerializer(model)
                    result = {"Status": "200",
                              "message": "Admin login successful", "is_admin": True,
                              "latest_client": None,
                              "admin_id":model.id,"token":encrypted_email,"user_details":serializer.data}
                return JSONResponse(result)

            if model.is_blocked == 1:
                result = {"Status": "404",
                          "message": "Your account has been blocked.Please contact admin."}
                return JSONResponse(result)    
            # if model.is_forget == 1:
            #     result = {"Status": "404",
            #               "message": "Forget password link has been sent to your email."}
            #     return JSONResponse(result)
            if model.is_active == 0:
                result = {"Status": "403",
                          "message": "Your account is not yet activated. Please verify email to activate your account."}
                return JSONResponse(result)
            unique_id = get_random_string(length=32)
            Users.objects.filter(email=request.POST.get(
                'email')).update(login_token=unique_id)
            # data1=request.POST.get('email')
            user_details = Users.objects.get(email=request.POST.get('email'))
            serializer = UsersSerializer(user_details)
            # signer = Signer()
            # encrypted_email = signer.sign(email)
            # tmp=JSONResponse(serializer.data)
            if model.purpose == '0' or model.purpose == 'parent':
                draft_query = Q(user_id=user_details.id, job_status=0,
                                is_active=0, is_delete=0, is_mailed=0)
                quoted_query = Q(is_confirmed=0,
                                 jobtypeid__user_id=user_details.id, jobtypeid__is_confirmed=0, jobtypeid__is_mailed=1)
                quoted_count = Jobbid.objects.filter(quoted_query).count()
                # print(quoted_count)
                completed_count = JobType.objects.filter(
                    user_id=user_details.id, is_confirmed=1, job_status=3, is_mailed=1).count()
                # draft_count = JobType.objects.filter(draft_query).count()
                # print(completed_count,quoted_count,draft_count)
                tab = 'posted'
                if quoted_count > 0 and completed_count == 0:
                    tab = 'quoted'
                elif quoted_count == 0 and completed_count > 0:
                    tab = 'completed'
                elif quoted_count == 0 and completed_count == 0:
                    tab = 'draft'
                elif quoted_count > 0 and completed_count > 0:
                    latest = []
                    # latest_quoted
                    latest.append(Jobbid.objects.filter(
                        quoted_query).latest('on_date_time').on_date_time)
                    # latest_completed
                    latest.append(JobType.objects.filter(
                        user_id=user_details.id, job_status=3).latest('updated').updated)
                    # latest_drafted
                    # latest.append(JobType.objects.filter(
                    #     draft_query).latest('updated').updated)
                    max_date_index = latest.index(max(latest))
                    # print(max_date_index)
                    if max_date_index == 0:
                        tab = 'quoted'
                    elif max_date_index == 1:
                        tab = 'completed'
                    # elif max_date_index == 2:
                    #     tab = 'draft'
            else:
                tab = "available"
                counts = get_jobtype_counts(user_details.id, 'worker')
                if counts['confirmed_count__sum'] > 0 and counts['paid_count__sum'] == 0:
                    print("intent 1")
                    tab = 'confirmed'
                elif counts['confirmed_count__sum'] == 0 and counts['paid_count__sum'] > 0:
                    print("intent 2")
                    tab = 'paid'
                elif counts['confirmed_count__sum'] > 0 and counts['paid_count__sum'] > 0:
                    print("intent 3")
                    # tab = 'paid'
                    try:
                        latest_paid = Jobbid.objects.filter(
                            userid=user_details.id, jobtypeid__job_status=4, is_confirmed=1).select_related('jobtypeid').latest('jobtypeid__updated')
                    except Jobbid.DoesNotExist:
                        tab = 'available'
                    try:
                        latest_confirmed = Jobbid.objects.filter(
                            userid=user_details.id, jobtypeid__job_status=2, is_confirmed=1).select_related('jobtypeid').latest('jobtypeid__updated')
                    except Jobbid.DoesNotExist:
                        tab = 'available'

                    if latest_paid.jobtypeid.updated > latest_confirmed.jobtypeid.updated:
                        tab = 'paid'
                    else:
                        tab = 'confirmed'
            model.login_count += 1
            model.save()
            data = {'Status': '200', 'message': 'Login successful',
                    'user_details': serializer.data, 'current_tab': tab,"is_admin":False,
                    "token":encrypted_email,"login_count":model.login_count}

            if request.POST.get('jobtypeId'):
                tab = 'posted'
                if model.purpose == '0' or model.purpose == 'parent':
                    update_job_type(model.id, request.POST.get('jobtypeId'))
                    mail_status = send_email.job_mail(
                        request.POST.get('jobtypeId'))
                    data = {'Status': '200', 'message': 'Login successful',
                            'user_details': serializer.data, "jobtypeId": request.POST.get('jobtypeId'),
                            'current_tab': tab, 'job_message': "job posted successfully", "job_status": '200',
                            "token":encrypted_email,"login_count":model.login_count}
                else:
                    data = {'Status': '200', 'message': 'Login successful',
                            'user_details': serializer.data, "jobtypeId": request.POST.get('jobtypeId'),
                            'current_tab': tab, 'job_message': "Posting job is not applicable for this account", "job_status": '500',
                            "token":encrypted_email,"login_count":model.login_count}
            if request.POST.get('message_worker_id'):
                msg = {}
                msg['message'] = request.POST.get('message_text');
                msg['worker_id'] = request.POST.get('message_worker_id');
                msg['from_user'] = model.id;
                serializer = adminmsgSerializer(data=msg,many=False)
                if serializer.is_valid():
                    serializer.save()
                    send_email.contact_mail_to_admin(msg,serializer.data)
                    data['message'] = True
                else:
                    data['message'] = serializer.errors
            log_data = {}
            log_data['userid'] = model.id
            log_data['status'] = "LOGIN"
            logging_status.logstatus(log_data)

            if 'current_tab' in data:
                if data['current_tab'] == 'confirmed':
                    status = 2
                    job_statusIds = Job_status_view.objects.values_list('job_id',flat=True).filter(userid=model.id,job_status=status)
                    print(job_statusIds)
                    job_status = Jobbid.objects.filter(~Q(jobtypeid__in=job_statusIds),userid=model.id,jobtypeid__job_status=status)
                    if job_status.exists():
                        data['showJobStatusPopup'] = True
                    else:
                        data['showJobStatusPopup'] = False
                elif data['current_tab'] == 'completed':
                    status = 3
                    job_statusIds = Job_status_view.objects.values_list('job_id',flat=True).filter(userid=model.id,job_status=status)
                    query = Q(
                      job_type_id__user_id=model.id,
                      job_type_id__is_confirmed=1,
                      job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__job_status=3) & ~Q(job_type_id__in=job_statusIds)
                    job_status = JobAddress.objects.filter(query)
                    if job_status.exists():
                        data['showJobStatusPopup'] = True
                    else:
                        data['showJobStatusPopup'] = False


            return JSONResponse(data)
        else:
            data = {'Status': '404',
                    'message': 'Username and password were incorrect'}
            return JSONResponse(data)
    else:
        data = {'Status': 'fail', 'message': 'Invalid'}
        return JSONResponse(data)


def update_job_type(usrid, jobtypeid):
    if usrid and jobtypeid:
        model = JobType.objects.get(id=jobtypeid)
        model.user_id = Users.objects.get(id=usrid)
        model.is_active = 1
        model.save()
        return True


# change password of a user
@csrf_exempt
def change_password(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'oldpassword' in data:
            # Encrypt the password
            password = make_password(data['password'])
            # check the old password is valid or not
            try:
                model = Users.objects.get(id=data['userid'])
            except Users.DoesNotExist:
                result = {"Status": "404", "message": "user not found!!"}
                return JSONResponse(result)
            # assign the new password for the user
            if model:
                if check_password(data['oldpassword'], model.password):
                    model.password = password
                    model.save()
                    # print(old_pwd)
                    # print(model.password)
                    # password updated
                    result = {"Status": "200", "message": "Password changed!!"}
                    return JSONResponse(result)
                else:
                    result = {"Status": "404",
                              "message": "Wrong old password!!"}
                    return JSONResponse(result)
        else:
            result = {"Status": "404", "message": "old password required"}
            return JSONResponse(result)

# check token and update password


@csrf_exempt
def checktoken(request, format=None):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # return HttpResponse(data)
        token = data['token']
        cutstring = token[len(token)-6:len(token)]
        token = token.split(cutstring)[0]
        # print(token)
        emp_id = data['userid']
        signer = Signer()
        try:
            model = Users.objects.get(id=emp_id,forget_token=data['token'])
        except Users.DoesNotExist:
            result = {'Status': '404', "message": "User not found!!"}
            return JSONResponse(result)
        email = model.email
        # token = "lspvn46fTMXz53jEtThEynpo8Jc"
        # email="selvamanian@optisolbusiness.com"
        try:
            dcrypt_email = signer.unsign('{0}:{1}'.format(email, token))
        except:
            result = {'Status': 'Failed'}
            return JSONResponse(result)
        result = {'Status': 'Correct'}
        return JSONResponse(result)
        # return HttpResponse(dcrypt_email)
        # if dcrypt_email == email:
        # return HttpResponse("Success" + dcrypt_email)
        # else:
        # return HttpResponse("Fail" + dcrypt_email)
        # email="selvamanian@optisolbusiness.com"
        # dcrypt_key = signer.unsign('{0}:{1}')
        # dcrypt_key = signer.unsign('{0}:{1}'.format(email, key))
        # usrid=request.POST.get('id')
        return HttpResponse(emp_id)


# Set new password after checking token for forget password
@csrf_exempt
def set_new_password(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # Encrypt the password
        password = make_password(data['password'])
        # check the old password is valid or not
        try:
            model = Users.objects.get(id=data['userid'])
        except Users.DoesNotExist:
            result = {"Status": "404", "message": "User not found!!"}
            return JSONResponse(result)
        # assign the new password for the user
        if model:
            # if model.is_forget == 0:
            #     result = {"Status": "404",
            #               "message": "Forget password already satisfied"}
            #     return JSONResponse(result)
            model.password = password
            # model.is_forget = 0
            model.save()
            # password updated
            result = {"Status": "200", "message": "Password changed!!"}
            return JSONResponse(result)


@csrf_exempt
def get_country_codes(request):
    if request.method == 'POST':
        model = Country_codes.objects.all().values(
            'id', 'country_name', 'code', 'flag_class')
        result = {"Status": "200", "data": model}
        return JSONResponse(result)


def get_jobtype_counts(userid, usertype):
    # job_statusIds = Job_status_view.objects.values_list('job_id',flat=True).filter(userid=userid)
    if usertype == '0' or usertype == 'parent':
        default = Q(user_id=userid) & Q(is_active=1, is_delete=0, is_mailed=1,is_blocked=0)
        counts = JobType.objects.values('user_id').annotate(
            completed_count=Sum(Case(
                When(Q(job_status=3), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            quoted_count=Sum(Case(
                When(Q(job_status=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            jobId=F('id')
        ).filter(default).aggregate(Sum('completed_count'),
                                    Sum('quoted_count'))
    elif usertype == '1' or usertype == 'worker':
        default = Q(userid=userid)\
            & Q(jobtypeid__is_active=1, jobtypeid__is_delete=0,jobtypeid__is_blocked=0)
        counts = Jobbid.objects.values('userid').annotate(
            applied_count=Sum(Case(
                When(Q(jobtypeid__job_status=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            confirmed_count=Sum(Case(
                When(Q(jobtypeid__job_status=2, is_confirmed=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            paid_count=Sum(Case(
                When(Q(jobtypeid__job_status=4, is_confirmed=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
        ).filter(default).aggregate(Sum('applied_count'),
                                    Sum('confirmed_count'),
                                    Sum('paid_count'))

        default = ~Q(job_type_id__user_id=None) & Q(job_type_id__job_status=0)\
            & Q(job_type_id__is_active=1) & Q(
            job_type_id__is_delete=0) & Q(jobtypeid__is_blocked=0)

    return counts


def get_email_token(email,userid):
    if email and userid:
        value = signing.dumps((email,userid))
        return value

@csrf_exempt
def partner_update(request):
    user_tbl = Users.objects.all()
    i = 0 
    for u in user_tbl:
        us_domain = User_domain()
        us_domain.userid = Users.objects.get(id=u.id)
        us_domain.partnerid = Partners.objects.get(id=1)
        us_domain.save()
        i = i + 1;

    return HttpResponse(i)


# To check and validate the signed token
@csrf_exempt
def login_from_token(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'release_payment_id' in data:
            release_payment_check = JobType.objects.filter(id=data['release_payment_id'])
            if release_payment_check.exists():
                if release_payment_check[0].job_status != 3: 
                    result = {"Status": "501", "message": "This job already paid."}
                    return JSONResponse(result)
            else:
                result = {"Status": "404", "message": "Job not found."}
                return JSONResponse(result)

        if 'token' in data or 'release_token' in data:
            if 'release_token' in data:
                unsigned_token = []
                unsigned_token.append(base64.urlsafe_b64decode(str(data['release_token'])))

                print(unsigned_token)
                try:
                    model = Users.objects.get(
                        email=unsigned_token[0], is_delete=0)
                except Users.DoesNotExist:
                    result = {"Status": "404", "message": "Email not found"}
                    return JSONResponse(result)
            else:
                unsigned_token = signing.loads(data['token'])
                # return JSONResponse(unsigned_token)
                try:
                    model = Users.objects.get(
                        email=unsigned_token[0],id=unsigned_token[1], is_delete=0)
                except Users.DoesNotExist:
                    result = {"Status": "404", "message": "Email not found"}
                    return JSONResponse(result)
            user_details = Users.objects.get(email=unsigned_token[0])
            serializer = UsersSerializer(user_details)
            if model:
                if model.is_admin == 1 and model.purpose == '2':
                    try:
                        latest_client = Users.objects\
                            .filter(is_active=1, is_delete=0, is_admin=0)\
                            .latest('updated')
                        result = {"Status": "200",
                                  "message": "Admin login successful", "is_admin": True,
                                  "latest_client": latest_client.id,
                                  "admin_id":model.id,'user_details':serializer.data}
                    except Users.DoesNotExist:
                        result = {"Status": "200",
                                  "message": "Admin login successful", "is_admin": True,
                                  "latest_client": None,
                                  "admin_id":model.id, 'user_details':serializer.data}
                    return JSONResponse(result)
                if model.is_forget == 1:
                    result = {"Status": "404",
                              "message": "Forget password link has been sent to your email!!"}
                    return JSONResponse(result)
                if model.is_active == 0:
                    result = {"Status": "403",
                              "message": "Your account is not yet activated. Please verify email to activate your account."}
                    return JSONResponse(result)
                unique_id = get_random_string(length=32)
                Users.objects.filter(email=request.POST.get(
                    'email')).update(login_token=unique_id)
                # data1=request.POST.get('email')
                
                # signer = Signer()
                # encrypted_email = signer.sign(email)
                # tmp=JSONResponse(serializer.data)
                if model.purpose == '0' or model.purpose == 'parent':
                    draft_query = Q(user_id=user_details.id, job_status=0,
                                    is_active=0, is_delete=0, is_mailed=0)
                    quoted_query = Q(is_confirmed=0,
                                     jobtypeid__user_id=user_details.id, jobtypeid__is_confirmed=0, jobtypeid__is_mailed=1)
                    quoted_count = Jobbid.objects.filter(quoted_query).count()
                    # print(quoted_count)
                    completed_count = JobType.objects.filter(
                        user_id=user_details.id, is_confirmed=1, job_status=3, is_mailed=1).count()
                    # draft_count = JobType.objects.filter(draft_query).count()
                    # print(completed_count,quoted_count,draft_count)
                    tab = 'posted'
                    if quoted_count > 0 and completed_count == 0:
                        tab = 'quoted'
                    elif quoted_count == 0 and completed_count > 0:
                        tab = 'completed'
                    elif quoted_count == 0 and completed_count == 0:
                        tab = 'draft'
                    elif quoted_count > 0 and completed_count > 0:
                        latest = []
                        # latest_quoted
                        latest.append(Jobbid.objects.filter(
                            quoted_query).latest('on_date_time').on_date_time)
                        # latest_completed
                        latest.append(JobType.objects.filter(
                            user_id=user_details.id, job_status=3).latest('updated').updated)
                        # latest_drafted
                        # latest.append(JobType.objects.filter(
                        #     draft_query).latest('updated').updated)
                        max_date_index = latest.index(max(latest))
                        # print(max_date_index)
                        if max_date_index == 0:
                            tab = 'quoted'
                        elif max_date_index == 1:
                            tab = 'completed'
                        # elif max_date_index == 2:
                        #     tab = 'draft'
                else:
                    tab = "available"
                    counts = get_jobtype_counts(user_details.id, 'worker')
                    if counts['confirmed_count__sum'] > 0 and counts['paid_count__sum'] == 0:
                        tab = 'confirmed'
                    elif counts['confirmed_count__sum'] == 0 and counts['paid_count__sum'] > 0:
                        tab = 'paid'
                    elif counts['confirmed_count__sum'] > 0 and counts['paid_count__sum'] > 0:
                        # tab = 'paid'
                        latest_paid = Jobbid.objects.filter(
                            userid=user_details.id, jobtypeid__job_status=4).select_related('jobtypeid').latest('jobtypeid__updated')
                        latest_confirmed = Jobbid.objects.filter(
                            userid=user_details.id, jobtypeid__job_status=2).select_related('jobtypeid').latest('jobtypeid__updated')
                        if latest_paid.jobtypeid.updated > latest_confirmed.jobtypeid.updated:
                            tab = 'paid'
                        else:
                            tab = 'confirmed'
                model.login_count += 1
                model.save()
                data = {'Status': '200', 'message': 'Login successful',
                        'user_details': serializer.data, 'current_tab': tab,
                        "is_admin":False,"login_count":model.login_count}
                return JSONResponse(data)
        else:
            result = {"Status": "404", "message": "Token is required"}
            return JSONResponse(result)
def decrypt_val(cipher_text):
    dec_secret = AES.new(MASTER_KEY[:32])
    raw_decrypted = dec_secret.decrypt(base64.b64decode(cipher_text))
    clear_val = raw_decrypted.decode().rstrip("\0")
    return clear_val





