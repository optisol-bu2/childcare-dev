from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email
from random import randint

from childcare.models import (
    User_availability, User_preference,
    User_employee_document, User_employee_history, User_other_detail,
    Household_chores, Languages, User_reference, User_emergency_contact, User_bank_details)

from childcare.serializers import (Preferences, Availabilty,
                                   Documents, Employee_history, Otherdetails,
                                   Userreference, UserEmergency, BankSerializer)

from django.db.models import Case, Sum, Max, When, Value, IntegerField, F
import json
import base64
import cStringIO
from base64 import b64decode
from django.core.files.base import ContentFile



# if __name__=='__main__':
#     print(__name__,'ssss')

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def view_profile(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = Users.objects.get(
                    id=data['userid'], is_active=1, is_delete=0, is_forget=0)
            except Users.DoesNotExist:
                result = {"Status": "404", "message": "User not found!!"}
                return JSONResponse(result)
            temp = {}
            temp['first_name'] = model.first_name
            temp['last_name'] = model.last_name
            temp['email'] = model.email
            temp['purpose'] = 'parent' if model.purpose == '0' else 'worker'
            if model.profpic:
                temp['profpic'] = model.profpic.url
            temp['selected_code'] = {}
            if model.country_code:
                temp['selected_code']['code'] = model.country_code.code
                temp['selected_code'][
                    'flag_class'] = model.country_code.flag_class
                temp['selected_code']['id'] = model.country_code.id
                temp['selected_code'][
                    'country_name'] = model.country_code.country_name
            temp['contactno'] = model.contactno
            temp['profile_suits'] = model.profile_suits
            temp['city'] = model.city
            temp['state'] = model.state
            temp['post_code'] = model.post_code
            temp['address'] = model.address
            temp['about'] = model.about
            temp['gender'] = model.gender
            temp['relevant_year'] = model.relevant_year
            temp['year_of_birth'] = str(model.year_of_birth)
            temp['age'] = model.age
            temp['id'] = model.id
            temp['is_blocked'] = model.is_blocked
            temp['is_vetted'] = model.is_vetted
            availability = get_user_availability(data['userid'])
            preferences = get_user_preference(data['userid'])
            requirements = get_user_documents(data['userid'])
            emp_history = get_user_history(data['userid'])
            emp_emergency = User_emergency_contact\
                .objects.values('id', 'first_name', 'last_name',
                                'email', 'contactno')\
                .filter(userid=data[
                    'userid'],is_delete=0)
            bank_details = get_user_bankdetails(data['userid'])
            other_details = get_user_otherdetails(data['userid'])
            result = {"Status": "200", "personal": temp,
                      "availability": availability,
                      "preferences": preferences,
                      "requirements": requirements,
                      "emp_history": emp_history,
                      "other_details": other_details,
                      "emp_emergency": emp_emergency,
                      "bank_details": bank_details}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


def get_user_availability(userid):
    if userid:
        try:
            model = User_availability.objects.get(userid=userid,is_delete=0)
        except User_availability.DoesNotExist:
            temp = {}
            temp['before_school'] = []
            temp['after_school'] = []
            temp['day_care'] = []
            temp['overnight'] = []
            # result = {"data": temp}
            return temp
        temp = {}
        temp['before_school'] = model.before_school.split(',')
        temp['after_school'] = model.after_school.split(',')
        temp['day_care'] = model.day_care.split(',')
        temp['overnight'] = model.overnight.split(',')
        return temp


def get_user_preference(userid):
    if userid:
        try:
            model = User_preference.objects.get(userid=userid,is_delete=0)
        except User_preference.DoesNotExist:
            temp = {}
            temp['caregivers'] = []
            temp['children_age'] = []
            temp['job_type'] = []
            return temp
        temp = {}
        if model.caregivers:
            temp['caregivers'] = model.caregivers.split(',')
        else:
            temp['caregivers'] = []
        if model.children_age:
            temp['children_age'] = model.children_age.split(',')
        else:
            temp['children_age'] = []
        if model.job_type:
            temp['job_type'] = model.job_type.split(',')
        else:
            temp['job_type'] = []
        return temp


@csrf_exempt
def update_personal_details(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = Users.objects.get(
                    id=data['userid'], is_active=1, is_delete=0, is_forget=0)
            except Users.DoesNotExist:
                result = {"Status": "404", "message": "User not found!!"}
                return JSONResponse(result)
            data['password'] = model.password
            data['email'] = model.email
            if 'profpic' in data:
                del data['profpic']
            serializer = SignupSerializer(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                update_history = Update_profile_check()
                update_history.userid = Users.objects.get(id=data['userid'])
                update_history.action = 'update'
                update_history.updated_by = int(data['updated_by'])
                update_history.save()
                result = {"Status": "200",
                          "message": "Personal details updated"}
                return JSONResponse(result)
            else:
                result = {"Status": "500", "errors": serializer.errors}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


def handle_uploaded_file(f, image_name):
    dir_name = os.getcwd() + "/../dist/images/profilepic/"
    with open(dir_name + image_name, "wb") as fh:
        fh.write(f.decode('base64'))
        fh.close()
    return True


@csrf_exempt
def uploaddp(request):
    if request.method == "POST":
        data = request.POST
        try:
            model = Users.objects.get(id=data.get('userid'))
        except Users.DoesNotExist:
            result = {"Status": "404", "message": "User Does not exists!!"}
            return JSONResponse(result)
        image = data.get('profpic')
        imagename = 'pic-' + str(randint(0, 9999)) + \
            data['userid']
        format1, imgstr = image.split(';base64,')
        ext = format1.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr), imagename + '.' + ext)
        if data:
            model.profpic = data
            model.save()
            result = {"Status": "200", "message": "Image saved!!",
                      "file": imagename + '.' + ext}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "Internal errors"}
            return JSONResponse(result)


@csrf_exempt
def save_preferences(request):
    """
      TO UPDATE THE USER'S PREFERENCES

    """
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            data['children_age'] = ','.join(filter(None, data['children_age']))
            data['caregivers'] = ','.join(filter(None, data['caregivers']))
            data['job_type'] = ','.join(filter(None, data['job_type']))
            try:
                model = User_preference.objects.get(
                    userid=data['userid'], userid__is_active=1,
                    userid__is_delete=0, userid__is_forget=0)
            except User_preference.DoesNotExist:
                serializer = Preferences(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200",
                              "message": "Preference details added!!"}
                    return JSONResponse(result)
                else:
                    result = {"Status": "500", "errors": serializer.errors}
                    return JSONResponse(result)
            serializer = Preferences(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200",
                          "message": "Preference details added!!"}
                return JSONResponse(result)
            else:
                result = {"Status": "500", "errors": serializer.errors}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def save_availability(request):
    """
      TO UPDATE THE USER'S AVAILABILITY

    """
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = User_availability.objects.get(
                    userid=data['userid'], userid__is_active=1,
                    userid__is_delete=0, userid__is_forget=0)
            except User_availability.DoesNotExist:
                serializer = Availabilty(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200",
                              "message": "Availabilty details added!!"}
                    return JSONResponse(result)
                else:
                    result = {"Status": "500", "errors": serializer.errors}
                    return JSONResponse(result)
            serializer = Availabilty(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200",
                          "message": "Availabilty details added!!"}
                return JSONResponse(result)
            else:
                result = {"Status": "500", "errors": serializer.errors}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def save_documents(request):
    """
      TO UPDATE USER DOCUMENTS

    """
    if request.method == "POST":
        data = request.POST
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            document = request.FILES.get('document')
            del_status = False
            try:
                model = User_employee_document.objects.get(
                    userid=data.get('userid'),
                    requirement_id=data.get('requirement_id'), is_delete=0)
            except User_employee_document.DoesNotExist:
                instance = User_employee_document()
                try:
                    instance.userid = Users.objects.get(id=data.get('userid'))
                    instance.requirement_id = Requirements.objects\
                        .get(id=data.get('requirement_id'))
                except Users.DoesNotExist:
                    result = {"Status": "404",
                              "message": "User Doesnot exists!!"}
                    return JSONResponse(result)
                except Requirements.DoesNotExist:
                    result = {"Status": "404",
                              "message": "Requirement Doesnot exists!!"}
                    return JSONResponse(result)
                if document:
                    documentname, extension = os.path.splitext(document.name)
                    document.name = 'req-' + str(randint(0, 9999)) + \
                        data.get('userid') + extension
                    if document_upload('requirement', document, document.name):
                        instance.document_name = document.name
                        instance.updated_by = int(data.get('updated_by'))
                        instance.save()
                        result = {"Status": "200",
                                  "message": "Document saved!!", "doc_name": document.name}
                    else:
                        result = {"Status": "500",
                                  "message": "Document not saved!!"}
                    return JSONResponse(result)
                else:
                    instance.updated_by = int(data.get('updated_by'))
                    instance.save()
                    result = {"Status": "200",
                              "message": "Document saved!!"}
                    return JSONResponse(result)
            if model:
                try:
                    model.userid = Users.objects.get(id=data.get('userid'))
                    model.requirement_id = Requirements.objects\
                        .get(id=data.get('requirement_id'))
                except Users.DoesNotExist:
                    result = {"Status": "404",
                              "message": "User Doesnot exists!!"}
                    return JSONResponse(result)
                except Requirements.DoesNotExist:
                    result = {"Status": "404",
                              "message": "Requirement Doesnot exists!!"}
                    return JSONResponse(result)
                if document:
                    documentname, extension = os.path.splitext(document.name)
                    document.name = 'req-' + str(randint(0, 9999)) + \
                        data.get('userid') + extension
                    if model.document_name:
                        del_status = delete_doc('req', model.document_name)
                    if document_upload('requirement', document, document.name):
                        model.document_name = document.name
                        model.updated_by = int(data.get('updated_by'))
                        model.save()
                        result = {"Status": "200",
                                  "message": "Document saved!!",
                                  "del_status": del_status, "doc_name": document.name}
                    else:
                        result = {"Status": "500",
                                  "message": "Document not saved!!"}
                    return JSONResponse(result)
                else:
                    model.updated_by = int(data.get('updated_by'))
                    model.save()
                    result = {"Status": "200",
                              "message": "Document saved!!"}
                    return JSONResponse(result)

        else:
            result = {"Status": "500", "message": "User is  invalid!!"}
            return JSONResponse(result)


def get_user_documents(userid):
    """

      TO GET THE USER DOCUMENTS

    """
    if userid:
        model = User_employee_document.objects\
            .values('requirement_id__requirement',
                    'document_name',
                    'requirement_id__id',
                    'requirement_id__category',
                    'requirement_id__icon_class','status','id')\
            .filter(userid=userid, is_delete=0)
        # model = Requirements.objects\
        #     .values('id','category','icon_class','requirement')\
        #     .filter(user_employee_document__userid=11)\
        #     .order_by('id')
        return model


@csrf_exempt
def save_employee_history(request):
    """
      TO UPDATE USER DOCUMENTS

    """
    if request.method == "POST":
        data = request.POST
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            proof_doc = request.FILES.get('proof_file')
            try:
                model = User_employee_history.objects.get(
                    id=data.get('id'))
            except User_employee_history.DoesNotExist:
                if proof_doc:
                    document_upload('history', proof_doc, data.get('proof'))
                serializer = Employee_history(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    # return JSONResponse(serializer)
                    result = {"Status": "200",
                              "message": "Employeement history saved!!"}
                else:
                    result = {"Status": "500", "errors": serializer.errors}
                return JSONResponse(result)
            if proof_doc:
                document_upload('history', proof_doc, data.get('proof'))
            del_status = delete_doc('history', model.proof)
            print(data)
            serializer = Employee_history(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200",
                          "message": "Employeement history updated!!",
                          "del_status": del_status}
            else:
                result = {"Status": "500", "errors": serializer.errors}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def get_user_history(userid):
    """

      TO GET THE USER EMPLOYEMENT HISTORY

    """
    if userid:
        model = User_employee_history.objects\
            .annotate(fromyear=F('from_year'),frommonth=F('from_month'))\
            .values('company_name',
                    'city',
                    'state', 'post_code',
                    'to_year', 'description',
                    'proof', 'id', 'title',
                    'to_month', 'headline', 'location', 'frommonth','fromyear','from_month','from_year')\
            .filter(userid=userid,is_delete=0).order_by('-id')
        # sorted_data = Sorting()
        for m in model:
            if m['post_code']:
                m['location'] = str(m['city']) + "," + \
                    str(m['state']) + " " + str(m['post_code'])
        return model


@csrf_exempt
def get_user_history_req(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'userid' not in data:
            return JSONResponse({"Status": "500",
                                 "message": "Userid required"})
        model = User_employee_history.objects\
            .annotate(fromyear=F('from_year'),frommonth=F('from_month'))\
            .values('company_name',
                    'city',
                    'state', 'post_code', 'fromyear',
                    'to_year', 'description',
                    'proof', 'id', 'title',
                    'frommonth', 'to_month', 'headline', 'location','from_month','from_year')\
            .filter(userid=data['userid'],is_delete=0).order_by('-id')
        for m in model:
            if m['post_code']:
                m['location'] = str(m['city']) + "," + \
                    str(m['state']) + " " + str(m['post_code'])
        result = {"Status": "200", "data": model}
        return JSONResponse(result)


def document_upload(doctype, doc, name):
    if doctype == 'history':
        dir_name = os.getcwd() + "/../dist/assets/documents/history_proof/"
    elif doctype == 'requirement':
        dir_name = os.getcwd() + "/../dist/assets/documents/requirements/"
    elif doctype == 'reference':
        dir_name = os.getcwd() + "/../dist/assets/documents/references/"
    elif doctype == 'resume':
        dir_name = os.getcwd() + "/../dist/assets/documents/resumes/"

    with open(dir_name + name, 'wb+') as destination:
        for chunk in doc.chunks():
            destination.write(chunk)
        destination.close()
    return True


@csrf_exempt
def get_users_requirements(request):
    if request.method == 'POST':
        reqs = Requirements.\
            objects.values('id', 'requirement',
                           'icon_class',
                           'category', 'has_proof', 'has_info','info')\
                           .filter(~Q(category=None))
        result = {"Status": "200",
                  "reqs": reqs}
        return JSONResponse(result)


def delete_doc(doctype, filename):
    if filename:
        if doctype == "req":
            dir_name = os.getcwd() + "/../dist/assets/documents/requirements/" + filename
        elif doctype == 'history':
            dir_name = os.getcwd() + "/../dist/assets/documents/history_proof/" + filename
        elif doctype == 'reference':
            dir_name = os.getcwd() + "/../dist/assets/documents/references/" + filename
        elif doctype == 'resume':
            dir_name = os.getcwd() + "/../dist/assets/documents/resumes/" + filename
        try:
            os.remove(dir_name)
        except OSError:
            return None
        return True


@csrf_exempt
def delete_history(request):
    data = JSONParser().parse(request)
    try:
        model = User_employee_history.objects.get(id=data['id'])
    except User_employee_history.DoesNotExist:
        result = {"Status": "404", "message": "History does not exists!!"}
        return JSONResponse(result)
    model.is_delete = 1
    model.updated_by = int(data['updated_by'])
    model.save()
    result = {"Status": "200", "message": "History Deleted!!"}
    return JSONResponse(result)


@csrf_exempt
def save_others(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        data['household_chores'] = ','.join(
            filter(None, data['household_chores']))
        data['languages'] = ','.join(filter(None, data['languages']))
        try:
            model = User_other_detail.objects.get(userid=data['userid'])
        except User_other_detail.DoesNotExist:
            serializer = Otherdetails(data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200", "message": "Information saved!!"}
            else:
                result = {"Status": "500", "errors": serializer.errors}
            return JSONResponse(result)
        serializer = Otherdetails(model, data=data, many=False)
        if serializer.is_valid():
            serializer.save()
            result = {"Status": "200", "message": "Information updated!!"}
        else:
            result = {"Status": "500", "errors": serializer.errors}
        return JSONResponse(result)


def get_user_otherdetails(userid):
    try:
        model = User_other_detail.objects\
            .values('interest', 'household_chores', 'languages', 'resume')\
            .get(userid=userid,is_delete=0)
    except User_other_detail.DoesNotExist:
        return {'household_chores': [], 'languages': []}
    if model['household_chores']:
        model['household_chores'] = model['household_chores'].split(',')
    else:
        model['household_chores'] = []
    if model['languages']:
        model['languages'] = model['languages'].split(',')
    else:
        model['languages'] = []
    return model


@csrf_exempt
def get_chore_and_languages(request):
    if request.method == "POST":
        # data = JSONParser().parse(request)
        chore = Household_chores.objects\
            .values('chore').filter(is_delete=0)
        languages = Languages.objects\
            .values('language').filter(is_delete=0)

        result = {"Status": "200", "chores": chore,
                  "languages": languages}
        return JSONResponse(result)


@csrf_exempt
def get_user_reference(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        model = User_reference.objects\
            .values('first_name', 'last_name', 'email',
                    'contactno', 'id', 'document')\
            .filter(userid=data['userid'],is_delete=0)
        result = {"Status": "200", "data": model}
        return JSONResponse(result)


@csrf_exempt
def save_user_reference(request):
    """
      TO UPDATE USER REFERENCES

    """
    if request.method == "POST":
        data = request.POST
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            document = request.FILES.get('filenamesss')
            try:
                model = User_reference.objects.get(
                    id=data.get('id'))
            except User_reference.DoesNotExist:
                if document:
                    document_upload('reference', document,
                                    document.name)
                    data['document'] = document.name
                serializer = Userreference(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200",
                              "message": "Reference saved!!"}
                else:
                    result = {"Status": "500", "errors": serializer.errors}
                return JSONResponse(result)
            if document:
                document_upload('reference', document, document.name)
                data['document'] = document.name
            del_status = delete_doc('reference', model.document)
            serializer = Userreference(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200",
                          "message": "Reference updated!!",
                          "del_status": del_status}
            else:
                result = {"Status": "500", "errors": serializer.errors}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def get_profile_completeness(request):
    """
    TO GET THE PROFILE COMPLETENESS PERCENTAGE
    ACCODRING TO THE GIVEN WEIGHTAGE
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        FIRST_NAME = 3
        LAST_NAME = 2
        COUNTRY_CODE = 2
        MOBILE = 3
        ADDRESS = 5
        # LOCATION = 5
        CITY = 3
        POSTCODE = 1
        STATE = 1
        GENDER = 3
        ABOUT = 10
        PHOTO = 10
        JOBTYPES = 3
        CAREGIVERS = 3
        CHILDREN_AGE = 3
        AVAILABILITY = 5
        ID_PROOF = 5
        ADDRESS_PROOF = 5
        RESIDENCY_PROOF = 5
        HISTORY_PROOF = 5
        INTRESTS = 5
        EMERGENCY_CONTACT = 3
        RESUME = 10
        REFERENCE = 5
        total = 100
        availability = None
        others = None
        try:
            model = Users.objects.get(id=data['userid'])
            others = User_other_detail.objects.get(userid=data['userid'],is_delete=0)
        except Users.DoesNotExist:
            return JSONResponse({"percentage": 0})
        except User_other_detail.DoesNotExist:
            # return HttpResponse('111')
            others = None
            total = total - (INTRESTS)
            total = total - (RESUME)
            pass
        try:
            availability = User_availability.objects.get(userid=data['userid'],is_delete=0)
        except User_availability.DoesNotExist:
            availability = None
            total = total - AVAILABILITY
            pass
        if model.first_name == '' or model.first_name is None:
            total = total - FIRST_NAME
        if model.last_name == '' or model.last_name is None:
            total = total - LAST_NAME
        if model.country_code == '' or model.country_code is None:
            total = total - COUNTRY_CODE
        if model.contactno == '' or model.contactno is None:
            total = total - MOBILE
        if model.address == '' or model.address is None:
            total = total - ADDRESS
        if model.about == '' or model.about is None:
            total = total - ABOUT
        if model.gender == '' or model.gender is None:
            total = total - GENDER
        if model.city == '' or model.city is None:
            total = total - CITY
        if model.state == '' or model.state is None:
            total = total - STATE
        if model.post_code == '' or model.post_code is None:
            total = total - POSTCODE
        if model.profpic == '' or model.profpic is None:
            total = total - PHOTO
        if others:
            if others.interest == '' or others.interest is None:
                total = total - INTRESTS
            if others.resume == '' or others.resume is None:
                total = total - RESUME
        query = Q(userid=data['userid'],is_delete=0)
        preference = User_preference.objects.filter(query)

        history = User_employee_history.objects.filter(query).exists()
        reference = User_reference.objects.filter(query).exists()
        identity_proof_id = 14
        address_proof_id = 13
        id_proof = User_employee_document.objects\
            .filter(query, requirement_id=identity_proof_id, document_name__isnull=False, is_delete=0).exists()
        address_proof = User_employee_document.objects\
            .filter(query, requirement_id=address_proof_id, document_name__isnull=False, is_delete=0).exists()
        residency = User_employee_document.objects\
            .filter(query, requirement_id__category='Residency', document_name__isnull=False, is_delete=0).exists()
        emergency = User_emergency_contact.objects\
            .filter(query).exists()
        if not preference.exists():
            total = total - (JOBTYPES + CAREGIVERS + CHILDREN_AGE)
        else:
            if not preference[0].job_type:
                total = total - JOBTYPES
            if not preference[0].caregivers:
                total = total - CAREGIVERS
            if not preference[0].children_age:
                total = total - CHILDREN_AGE
        if availability:
            if not availability.before_school\
                and not availability.day_care\
                and not availability.after_school\
                    and not availability.overnight:
                total = total - AVAILABILITY
        if not history:
            total = total - HISTORY_PROOF
        if not reference:
            total = total - REFERENCE
        if not id_proof:
            total = total - ID_PROOF
        if not address_proof:
            total = total - ADDRESS_PROOF
        if not residency:
            total = total - RESIDENCY_PROOF
        if not emergency:
            total = total - EMERGENCY_CONTACT

        Users.objects.filter(id=data['userid']).update(completeness=total)

        result = {"Status": "200", "percentage": total}
        return JSONResponse(result)


@csrf_exempt
def remove_user_requirement(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = User_employee_document.objects.get(
                    userid=data['userid'],
                    requirement_id=data['requirement_id'],
                    is_delete=0)
            except User_employee_document.DoesNotExist:
                result = {"Status": "404",
                          "message": "Requirement does not exists"}
                return JSONResponse(result)
            if model.document_name:
                delete_doc('req', model.document_name)
                model.updated_by = int(data['updated_by'])
                model.is_delete = 1
                model.save()
                action = "soft"
            else:
                model.updated_by = int(data['updated_by'])
                model.is_delete = 1
                model.save()
                action = "hard"
            result = {"Status": "200",
                      "message": "Model deleted!!", "delete_mode": action}
            return JSONResponse(result)


@csrf_exempt
def save_emergency(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'id' in data:
                data_id = Q(id=data['id'])
                try:
                    model = User_emergency_contact.objects\
                        .get(data_id,
                             userid=data['userid'])
                    serializer = UserEmergency(
                        model, data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Emergency contact updated"}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500",
                                  "errors": serializer.errors}
                        return JSONResponse(result)
                except User_emergency_contact.DoesNotExist:
                    serializer = UserEmergency(data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Emergency contact added"}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500",
                                  "errors": serializer.errors}
                        return JSONResponse(result)
            else:
                serializer = UserEmergency(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200",
                              "message": "Emergency contact added"}
                    return JSONResponse(result)
                else:
                    result = {"Status": "500",
                              "errors": serializer.errors}
                    return JSONResponse(result)


@csrf_exempt
def get_emergency_contacts(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        model = User_emergency_contact\
            .objects.values('id', 'first_name', 'last_name',
                            'email', 'contactno')\
            .filter(userid=data[
                'userid'],is_delete=0)
        result = {"Status": "200", "data": model}
        return JSONResponse(result)


@csrf_exempt
def uploadresume(request):
    if request.method == "POST":
        file = request.FILES.get('file')
        if file:
            documentname, extension = os.path.splitext(file.name)
            filename = "resume-" + str(request.POST.get('userid')) + str(randint(0, 9999)) \
                + extension
            file.name = filename
            try:
                model = User_other_detail.objects.select_related().get(
                    userid=request.POST.get('userid'))
            except User_other_detail.DoesNotExist:
                model1 = User_other_detail()
                model1.userid = Users.objects.get(id=request.POST.get('userid'))
                model1.resume = filename
                model1.updated_by = int(request.POST.get('updated_by'))
                model1.save()
                result = {"Status": "200", "Message": "User other details added and resume uploaded",
                          "filename": filename}
                return JSONResponse(result)
            document_upload('resume', file, filename)
            model.updated_by = int(request.POST.get('updated_by'))
            model.resume = filename
            model.save()
            result = {"Status": "200",
                      "Message": "Resume uploaded", "filename": filename}
            return JSONResponse(result)
        else:
            result = {"Status": "400", "Message": "file required"}
            return JSONResponse(result)


@csrf_exempt
def save_bank_details(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            model = User_bank_details.objects.get(userid=data['userid'])
        except User_bank_details.DoesNotExist:
            serializer = BankSerializer(data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                result = {"Status": "200",
                          "message": "Bank Information saved!!"}
            else:
                result = {"Status": "500", "errors": serializer.errors}
            return JSONResponse(result)
        serializer = BankSerializer(model, data=data, many=False)
        if serializer.is_valid():
            serializer.save()
            result = {"Status": "200", "message": "Bank Information updated!!"}
        else:
            result = {"Status": "500", "errors": serializer.errors}
        return JSONResponse(result)


def get_user_bankdetails(userid):
    try:
        model = User_bank_details.objects\
            .get(userid=userid)
    except User_bank_details.DoesNotExist:
        return {}
    serializer = BankSerializer(model, many=False)
    return serializer.data


@csrf_exempt
def get_client_profile(request):
    data = JSONParser().parse(request)
    try:
        model = Users.objects.values('first_name', 'last_name',
                                     'country_code__id', 'country_code__country_name', 'country_code__flag_class',
                                     'email', 'contactno', 'country_code__code', 'profpic').annotate(id=F('id'),blocked_user=F('is_blocked'),vetted_user=F('is_vetted')).get(
            id=data['userid'], is_active=1, is_delete=0)
    except Users.DoesNotExist:
        result = {"Status": "404", "message": "User not found!!"}
        return JSONResponse(result)
    result = {"Status": "200", "data": model}
    return JSONResponse(result)


@csrf_exempt
def delete_reference(request):
    data = JSONParser().parse(request)
    try:
        model = User_reference.objects.get(id=data['id'])
    except User_reference.DoesNotExist:
        result = {"Status": "404", "message": "Reference does not exists!!"}
        return JSONResponse(result)
    model.updated_by = int(data['updated_by'])
    model.is_delete = 1
    model.save()
    result = {"Status": "200", "message": "Reference Deleted!!"}
    return JSONResponse(result)


@csrf_exempt
def delete_emergency_contact(request):
    data = JSONParser().parse(request)
    try:
        model = User_emergency_contact.objects.get(id=data['id'])
    except User_emergency_contact.DoesNotExist:
        result = {"Status": "404",
                  "message": "Emergency contact does not exists!!"}
        return JSONResponse(result)
    model.updated_by = int(data['updated_by'])
    model.is_delete=1
    model.save()
    result = {"Status": "200", "message": "Emergency contact Deleted!!"}
    return JSONResponse(result)


@csrf_exempt
def deleteresume(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'userid' in data:
            try:
                model = User_other_detail.objects.get(userid=data['userid'])
            except others.DoesNotExist:
                result = {"Status": "404", "message": "id does not exists"}
                return JSONResponse(result)
            delete_doc('resume', model.resume)
            model.resume = None
            # model.is_delete = 1
            model.save()
            result = {"Status": "200", "message": "Resume removed"}
            return JSONResponse(result)
        else:
            result = {"Status": "400", "message": "userid is required"}
            return JSONResponse(result)


@csrf_exempt
def delete_ref_doc(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'id' in data:
            try:
                model = User_reference.objects.get(id=data['id'])
            except User_reference.DoesNotExist:
                result = {"Status": "404", "message": "Id does not exists"}
                return JSONResponse(result)
            doc_name = model.document
            delete_doc('reference', doc_name)
            model.document = None
            model.is_delete = 1
            model.save()
            result = {"Status": "400",
                      "message": "Reference document was deleted!!"}
            return JSONResponse(result)
        else:
            result = {"Status": "400", "message": "ID is required to continue"}
            return JSONResponse(result)


@csrf_exempt
def default_append_completeness(request):
    """
    TO GET THE PROFILE COMPLETENESS PERCENTAGE
    ACCODRING TO THE GIVEN WEIGHTAGE
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        
        users = Users.objects.filter(is_active=1,is_delete=0)
        for u in users:
            FIRST_NAME = 3
            LAST_NAME = 2
            COUNTRY_CODE = 2
            MOBILE = 3
            ADDRESS = 5
            # LOCATION = 5
            CITY = 3
            POSTCODE = 1
            STATE = 1
            GENDER = 3
            ABOUT = 10
            PHOTO = 10
            JOBTYPES = 3
            CAREGIVERS = 3
            CHILDREN_AGE = 3
            AVAILABILITY = 5
            ID_PROOF = 5
            ADDRESS_PROOF = 5
            RESIDENCY_PROOF = 5
            HISTORY_PROOF = 5
            INTRESTS = 5
            EMERGENCY_CONTACT = 3
            RESUME = 10
            REFERENCE = 5
            total = 100
            availability = None
            others = None
            data['userid'] = u.id
            try:
                model = Users.objects.get(id=data['userid'])
                others = User_other_detail.objects.get(userid=data['userid'],is_delete=0)
            except Users.DoesNotExist:
                return JSONResponse({"percentage": 0})
            except User_other_detail.DoesNotExist:
                # return HttpResponse('111')
                others = None
                total = total - (INTRESTS)
                total = total - (RESUME)
                pass
            try:
                availability = User_availability.objects.get(userid=data['userid'],is_delete=0)
            except User_availability.DoesNotExist:
                availability = None
                total = total - AVAILABILITY
                pass
            if model.first_name == '' or model.first_name is None:
                total = total - FIRST_NAME
            if model.last_name == '' or model.last_name is None:
                total = total - LAST_NAME
            if model.country_code == '' or model.country_code is None:
                total = total - COUNTRY_CODE
            if model.contactno == '' or model.contactno is None:
                total = total - MOBILE
            if model.address == '' or model.address is None:
                total = total - ADDRESS
            if model.about == '' or model.about is None:
                total = total - ABOUT
            if model.gender == '' or model.gender is None:
                total = total - GENDER
            if model.city == '' or model.city is None:
                total = total - CITY
            if model.state == '' or model.state is None:
                total = total - STATE
            if model.post_code == '' or model.post_code is None:
                total = total - POSTCODE
            if model.profpic == '' or model.profpic is None:
                total = total - PHOTO
            if others:
                if others.interest == '' or others.interest is None:
                    total = total - INTRESTS
                if others.resume == '' or others.resume is None:
                    total = total - RESUME
            query = Q(userid=data['userid'],is_delete=0)
            preference = User_preference.objects.filter(query)

            history = User_employee_history.objects.filter(query).exists()
            reference = User_reference.objects.filter(query).exists()
            identity_proof_id = 14
            address_proof_id = 13
            id_proof = User_employee_document.objects\
                .filter(query, requirement_id=identity_proof_id, document_name__isnull=False, is_delete=0).exists()
            address_proof = User_employee_document.objects\
                .filter(query, requirement_id=address_proof_id, document_name__isnull=False, is_delete=0).exists()
            residency = User_employee_document.objects\
                .filter(query, requirement_id__category='Residency', document_name__isnull=False, is_delete=0).exists()
            emergency = User_emergency_contact.objects\
                .filter(query).exists()
            if not preference.exists():
                total = total - (JOBTYPES + CAREGIVERS + CHILDREN_AGE)
            else:
                if not preference[0].job_type:
                    total = total - JOBTYPES
                if not preference[0].caregivers:
                    total = total - CAREGIVERS
                if not preference[0].children_age:
                    total = total - CHILDREN_AGE
            if availability:
                if not availability.before_school\
                    and not availability.day_care\
                    and not availability.after_school\
                        and not availability.overnight:
                    total = total - AVAILABILITY
            if not history:
                total = total - HISTORY_PROOF
            if not reference:
                total = total - REFERENCE
            if not id_proof:
                total = total - ID_PROOF
            if not address_proof:
                total = total - ADDRESS_PROOF
            if not residency:
                total = total - RESIDENCY_PROOF
            if not emergency:
                total = total - EMERGENCY_CONTACT

            Users.objects.filter(id=data['userid']).update(completeness=total)

        result = {"Status": "200", "percentage": total}
        return JSONResponse(result)