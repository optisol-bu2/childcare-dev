from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email, userprofile
from random import randint

from childcare.models import (
    User_availability, User_preference,
    User_employee_document, User_employee_history, User_other_detail,
    Household_chores, Languages, User_reference, User_emergency_contact,
    admin_messages)

from childcare.serializers import (Preferences, Availabilty,
                                   Documents, Employee_history, Otherdetails,
                                   Userreference, UserEmergency, adminmsgSerializer)

import json


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def get_all_users(request):
    """

    To get all the users by giving locations and paginations

    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'pageno' not in data or 'limit' not in data:
                result = {"Status": '500',
                          "Message": "pagenumber & limit cannot be empty"}
                return JSONResponse(result)
            else:
                limit = data['limit']
                page_no = data['pageno']
                if page_no == 1:
                    start_limit = 0
                    end_limit = limit
                    # return HttpResponse(limit)
                elif page_no > 1:
                    start_limit = limit * (page_no - 1)
                    end_limit = limit + start_limit
            query_city = Q()
            query_state = Q()
            query_postcode = Q()
            default = (Q(purpose='worker') | Q(
                purpose='1')) & Q(is_active=1, is_delete=0, activation_email=1,is_blocked=0,partnerid=data['partnerid'])
            if 'suburb' in data:
                query_city = Q(city=data['suburb'])
            if 'state' in data:
                query_state = Q(state=data['state'])
            if 'postcode' in data:
                query_postcode = Q(post_code=data['postcode'])

            jobtype_query = Q()
            if 'job_type' in data:
                if isinstance(data['job_type'],list):
                    if len(data['job_type']) > 0:
                        for m in data['job_type']:
                            jobtype_query = jobtype_query | Q(user_preference__job_type__icontains=m)

            caregiver_query = Q()
            if 'caregiver' in data:
                if isinstance(data['caregiver'],list):
                    if len(data['caregiver']) > 0:
                        for m in data['caregiver']:
                            caregiver_query = caregiver_query | Q(user_preference__caregivers__icontains=m)


            requirement_query = Q()
            if 'requirement' in data:
                if isinstance(data['requirement'],list):
                    if len(data['requirement']) > 0:
                        requirement_query = Q(user_employee_document__requirement_id__in=data['requirement'])

            model = Users.objects.filter(
                default, query_city, query_state, query_postcode,
                jobtype_query, caregiver_query, requirement_query)\
                .annotate(ucount=Count('id')).order_by('order_by')[start_limit:end_limit]
                # Work_Order.objects.order_by(F('dateWORequired').desc(nulls_last=True), F('anotherfield')
            total_count = Users.objects.filter(
                default, query_city, query_state, query_postcode,
                jobtype_query, caregiver_query, requirement_query).count()
            users_data = []
            for m in model:
                temp = {}
                temp['id'] = m.id
                temp['first_name'] = m.first_name
                temp['last_name'] = m.last_name
                temp['age'] = m.age
                temp['address'] = m.address
                temp['city'] = m.city
                temp['state'] = m.state
                if m.profpic:
                    temp['profilepic'] = m.profpic.url
                else:
                    temp['profilepic'] = None
                temp['about'] = m.about
                temp['post_code'] = m.post_code
                temp['gender'] = m.gender
                temp['preference'] = userprofile.get_user_preference(m.id)
                temp['requirements_length'] = len(userprofile
                                                  .get_user_documents(
                                                      m.id))
                temp['is_vetted'] = m.is_vetted
                temp['relevant_year'] = m.relevant_year
                temp['completeness'] = m.completeness
                users_data.append(temp.copy())
            result = {"Status": "200", "data": users_data,
                      "total_count": total_count, "count": model.count()}
            return JSONResponse(result)

        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def getpublicprofile(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = Users.objects.get(
                    id=data['userid'], is_active=1, is_delete=0)
            except Users.DoesNotExist:
                result = {"Status": "404", "message": "User not found!!"}
                return JSONResponse(result)
            temp = {}
            temp['first_name'] = model.first_name
            temp['last_name'] = model.last_name
            # temp['email'] = model.email
            temp['purpose'] = 'parent' if model.purpose == '0' else 'worker'
            if model.profpic:
                temp['profpic'] = model.profpic.url
            else:
                temp['profpic'] = None
            # temp['contactno'] = model.contactno
            temp['id'] = model.id
            temp['profile_suits'] = model.profile_suits
            temp['city'] = model.city
            temp['state'] = model.state
            temp['post_code'] = model.post_code
            temp['address'] = model.address
            temp['about'] = model.about
            temp['gender'] = model.gender
            temp['relevant_year'] = model.relevant_year
            temp['year_of_birth'] = str(model.year_of_birth)
            temp['age'] = model.age
            temp['is_vetted'] = model.is_vetted
            temp['is_block'] = model.is_blocked
            availability = userprofile.get_user_availability(data['userid'])
            preferences = userprofile.get_user_preference(data['userid'])
            requirements = userprofile.get_user_documents(data['userid'])
            emp_history = userprofile.get_user_history(data['userid'])
            # emp_emergency = User_emergency_contact\
            #     .objects.values('id', 'first_name', 'last_name',
            #                     'email', 'contactno')\
            #     .filter(userid=data[
            #         'userid'])
            other_details = userprofile.get_user_otherdetails(data['userid'])
            result = {"Status": "200", "personal": temp,
                      "availability": availability,
                      "preferences": preferences,
                      "requirements": requirements,
                      "other_details": other_details,
                      "emp_history": emp_history}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def contact_worker(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'worker_id' in data and 'from_user' in data:
            serializer = adminmsgSerializer(data=data,many=False)
            if serializer.is_valid():
                serializer.save()
                send_email.contact_mail_to_admin(data,serializer.data)
                result = {"Status": "200", "message": "Your message has been sent to the worker"}
                return JSONResponse(result)
            else:
                result = {"Status": "400", "errors": serializer.errors}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "user id and worker id is required to continue"}
            return JSONResponse(result)
