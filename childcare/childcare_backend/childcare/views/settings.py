from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email
from childcare.serializers import Settings


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def get_settings(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = User_settings.objects.values(
                    "job_email",'quote_email','completed_email','job_confirmed','job_sorry','job_paid').get(userid=data['userid'])
            except User_settings.DoesNotExist:
                model = None
            if model:
                result = {"Status": "200", "setting": model}
            else:
                result = {"Status": "200", "setting": {}}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def change_settings(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            try:
                model = User_settings.objects.get(userid=data['userid'])
            except User_settings.DoesNotExist:
                data['job_email'] = 1
                serializer = Settings(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    errors = None
                    result = {"Status": "200", "message": "Settings added!!"}
                else:
                    errors = serializer.errors
                    result = {"Status": "500", "errors": errors}
                return JSONResponse(result)
            serializer = Settings(model, data=data, many=False)
            if serializer.is_valid():
                serializer.save()
                errors = None
                result = {"Status": "200", "message": "Settings updated!!"}
            else:
                errors = serializer.errors
                result = {"Status": "500", "errors": errors}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)
