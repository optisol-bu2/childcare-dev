from .imports import *

from django.db.models import F, Value, DateField, CharField
from django.db.models.functions import Concat, Cast, TruncSecond

from childcare.models import User_referal
from django.conf import settings
import datetime as dt
import ast

from childcare.mailer import Mailer

WORDPRESS_URL = "https://www.lovelychildcare.com/"

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

# contents for sending email
from_email = 'admin@lovelychildcare.com'
logo_image = os.getcwd() + "/images/logo.png"
logo_top_image = os.getcwd() + "/images/logo-top.png"

with open(logo_top_image, 'rb') as logo:
    image_logo_top = logo.read()
logo_top_image = InlineImage(
    filename="logo.png", content=image_logo_top)
with open(logo_image, 'rb') as logo:
    image_logo = logo.read()
logo_inline_image = InlineImage(
    filename="logo.png", content=image_logo)
# send mail to users when a job is posted and activated


def job_mail(jobId):
    """
            send jobId here when a job is posted
    """
    try:
        job = JobType.objects\
            .annotate(rate_per_hour=F('jobtime_budget__rate_per_hour'),
                    job_time=F('jobtime_budget__time'),
                    job_date=F('jobtime_budget__date'),
                    location=F('jobaddress__location'),
                    post_code = F('jobaddress__post_code'),
                    city = F('jobaddress__city'),
                    state = F('jobaddress__state')
                    ).values()\
            .get(id=jobId)
        job['job_date'] = job['job_date'].strftime("%d-%m-%Y")
        job['job_time'] = job['job_time'].strftime("%H:%M")
        job['on_date_time'] = job['on_date_time'].strftime("%d-%m-%Y %H:%M")
        if job['updated']:
            job['updated'] = job['updated'].strftime("%d-%m-%Y %H:%M")
    except JobType.DoesNotExist:
        return 'Job not found!!'
    if job['is_mailed'] == 0:
        worker_filter = Q(job_email=1, userid__is_active=1, userid__is_delete=0,userid__is_blocked=0) &\
            (Q(userid__purpose='1') | Q(userid__purpose='worker'))
        user_emails = User_settings.objects\
            .values('userid__email', 'userid__first_name', 'userid__last_name')\
            .filter(worker_filter)
        if len(user_emails) > 0:
            act_link = FRONT_URL + \
                "#/worker/previewjob/{}/jobdetails".format(jobId)
            toSendEmails = []
            userNames = [] 
            contexts = []
            context_str = ""
            for m in user_emails:
                toSendEmails.append(m['userid__email'])
                userNames.append(m['userid__first_name'] + " " + m['userid__last_name'])
                # send_templated_mail(
                #     template_name='new_job_email',
                #     from_email=from_email,
                #     recipient_list=[m['userid__email']],
                #     context={
                #         'username': m['userid__email'],
                #         'emp_name': m['userid__first_name'] + " " + m['userid__last_name'],
                #         'act_link': act_link,
                #         'logo_image': logo_inline_image,
                #         'logo_top_image': logo_top_image,
                #         'job': job,
                #         'unsubscribe_url':FRONT_URL +"#/settings"
                #     },
                #     bcc=[settings.PROJECT_ADMIN_EMAIL],
                # )
                context={
                        "username": m['userid__email'],
                        "emp_name": m['userid__first_name'] + " " + m['userid__last_name'],
                        "act_link": act_link,
                        # "logo_image": logo_inline_image,
                        # "logo_top_image": logo_top_image,
                        "job": job,
                        "unsubscribe_url":FRONT_URL +"#/settings"
                }
                # contexts.append(json.dumps(context))
                context_str += json.dumps(context)+';'

            Bulk_mail = Bulk_mail_send()
            Bulk_mail.username = ', '.join(userNames)
            Bulk_mail.recipient_list = ','.join(toSendEmails)
            Bulk_mail.action_link = act_link
            # Bulk_mail.context = ','.join(contexts)
            Bulk_mail.context = context_str
            Bulk_mail.template_name='new_job_email'
            Bulk_mail.save()
            # print(job.job_date,'dddddddd')
            # job.is_mailed = 1
            # job.save()
            return True
        else:
            job.is_mailed = 1
            job.save()
            return True
    else:
        return 'Job mail already sent!!'


# To retrive the forget password
@csrf_exempt
def forget_password(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if "email" not in data:
            result = {"Status": "200",
                      "message": "Email is required to continue!!"}
            return JSONResponse(result)
        try:
            model = Users.objects.get(email=data['email'])
        except Users.DoesNotExist:
            result = {
                "Status": "404", "message": "We couldn't find an account associated with this email address"}
            return JSONResponse(result)
        if model.is_forget == 1:
            result = {"Status": "500",
                      "message": "An email with new password link has been sent already!!"}
            return JSONResponse(result)
        if model.is_active == 0:
            result = {"Status": "500",
                      "message": "Your account is not activated yet, please check your email for the activation link"}
            return JSONResponse(result)
        logo_image = os.getcwd() + "/images/logo.png"
        logo_top_image = os.getcwd() + "/images/logo-top.png"
        # banner_img = os.getcwd() + "/images/bigImg.jpg"
        # fb = os.getcwd() + "/images/facebook.png"
        # gplus = os.getcwd() + "/images/googleplus.png"
        # linkedin = os.getcwd() + "/images/linkedin.png"
        # twitter = os.getcwd() + "/images/twitter.png"
        with open(logo_image, 'rb') as logo:
            image_logo = logo.read()
        logo_inline_image = InlineImage(
            filename="logo.png", content=image_logo)
        with open(logo_top_image, 'rb') as logo:
            image_logo_top = logo.read()
        logo_top_image = InlineImage(
            filename="logo.png", content=image_logo_top)
        # with open(banner_img, 'rb') as bg:
        #     image_bg = bg.read()
        # banner_im = InlineImage(
        #     filename="bigImg.png", content=image_bg)
        # with open(fb, 'rb') as bg:
        #     image_fb = bg.read()
        # fb_im = InlineImage(
        #     filename="facebook.png", content=image_fb)
        # with open(gplus, 'rb') as gp:
        #     image_gplus = gp.read()
        # gplus_im = InlineImage(
        #     filename="googleplus.png", content=image_gplus)
        # with open(linkedin, 'rb') as lk:
        #     image_linkedin = lk.read()
        # linkedin_im = InlineImage(
        #     filename="linkedin.png", content=image_linkedin)
        # with open(twitter, 'rb') as tw:
        #     image_twitter = tw.read()
        # twitter_im = InlineImage(
        #     filename="twitter.png", content=image_twitter)
        signer = Signer()
        # gives 'email@email.com:signed_things', extract signed_things'
        signed_value = signer.sign(data['email'])
        print(signed_value)
        key = ''.join(signed_value.split(':')[1:])
        act_link = FRONT_URL + "#/checktoken?id=" + \
            str(model.id) + "&token=" + key + "&email=" + model.email
        model.is_forget = 1
        model.save()
        send_templated_mail(
            template_name='forgetemail',
            from_email=from_email,
            recipient_list=[model.email],
            context={
                'username': model.email,
                'emp_name': model.first_name + " " + model.last_name,
                'act_link': act_link,
                'logo_image': logo_inline_image,
                'logo_top_image': logo_top_image,
                # 'banner_img': banner_im,
                # 'fb': fb_im,
                # 'twitter': twitter_im,
                # 'linkedin': linkedin_im,
                # 'gplus': gplus_im
                'unsubscribe_url':FRONT_URL +"#/settings"
            },
        )
        result = {"Status": "200", "message": "An email with new password link has been sent to your email id."}
        return JSONResponse(result)


def sorry_email(to_ids, jobid):
    subject = "Sorry! your quote was not accepted by the client"
    jobId = jobid
    act_link = FRONT_URL + "#/joblist/available"
    for to_id in to_ids:
        user = Users.objects.filter(email=to_id,user_settings__job_sorry=1)
        if user.exists():
            user = user[0]
            emp_name = user.first_name + " " + user.last_name
            send_templated_mail(
                template_name='sorry_email',
                from_email=from_email,
                recipient_list=[to_id],
                context={
                    # 'username': model.email,
                    'subject': subject,
                    'emp_name': emp_name,
                    'act_link': act_link,
                    'jobId': jobId,
                    'logo_top_image': logo_top_image,
                    'logo_image': logo_inline_image,
                    'unsubscribe_url':FRONT_URL +"#/clientsettings"
                },
            )
    return True
 

def confirm_email(to_id, jobid):
    try:
        client = JobType.objects\
            .annotate(
                full_name=Concat('user_id__first_name',
                                 Value(' '), 'user_id__last_name'),
                job_address=F('jobaddress__address'),
                client_city=F('jobaddress__city'),
                client_state=F('jobaddress__state'),
                client_post_code=F('jobaddress__post_code'),
                job_time=F('jobtime_budget__time'),
                job_date=F('jobtime_budget__date'),
            )\
            .get(id=jobid)
        # client = Jobbid.objects\
        #     .annotate(
        #         full_name=Concat('jobtypeid__user_id__first_name',
        #                          Value(' '), 'jobtypeid__user_id__last_name'),
        #         job_address=F('jobtypeid__jobaddress__address'),
        #         client_city=F('jobtypeid__jobaddress__city'),
        #         client_state=F('jobtypeid__jobaddress__state'),
        #         client_post_code=F('jobtypeid__jobaddress__post_code'),
        #         job_time=F('jobtypeid__jobtime_budget__time'),
        #         job_date=F('jobtypeid__jobtime_budget__date'),
        #     )\
        #     .get(jobtypeid=jobid)
    except JobType.DoesNotExist:
        return 'Job not found!!'
    user = Users.objects.values('first_name','last_name','id').annotate(emailEnabled=F('user_settings__job_confirmed')).filter(email=to_id)
    if user.exists():
        user = user[0]
        emp_name = user['first_name'] + " " + user['last_name']
        act_link = FRONT_URL + "#/worker/previewjob/{}/{}/jobdetails/workerviewjobMail".format(jobid,user['id'])
        subject = "Successful. Your job booking has been confirmed!"
        if user['emailEnabled'] == 1:
            send_templated_mail(
                template_name='confirm_email',
                from_email=from_email,
                recipient_list=[to_id],
                context={
                    # 'username': model.email,
                    'subject': subject,
                    # 'emp_name': model.first_name + " " + model.last_name,
                    'act_link': act_link,
                    'logo_image': logo_inline_image,
                    'logo_top_image': logo_top_image,
                    'client': client,
                    'emp_name':emp_name,
                    'unsubscribe_url':FRONT_URL +"#/settings"
                    # 'banner_img': banner_im,
                    # 'fb': fb_im,
                    # 'twitter': twitter_im,
                    # 'linkedin': linkedin_im,
                    # 'gplus': gplus_im
                },
                bcc=[settings.PROJECT_ADMIN_EMAIL],
            )
        else:
            send_templated_mail(
                template_name='confirm_email',
                from_email=from_email,
                recipient_list=[settings.PROJECT_ADMIN_EMAIL],
                context={
                    # 'username': model.email,
                    'subject': subject,
                    # 'emp_name': model.first_name + " " + model.last_name,
                    'act_link': act_link,
                    'logo_image': logo_inline_image,
                    'logo_top_image': logo_top_image,
                    'client': client,
                    'emp_name':emp_name,
                    'unsubscribe_url':FRONT_URL +"#/settings"
                    # 'banner_img': banner_im,
                    # 'fb': fb_im,
                    # 'twitter': twitter_im,
                    # 'linkedin': linkedin_im,
                    # 'gplus': gplus_im
                },
            )

        return True
    else:
        return False

# user quote job to trigger client email

def quote_mail(userId, jobid):
    try:
        # client = JobType.objects\
        #     .annotate(
        #         full_name=Concat('user_id__first_name',
        #                          Value(' '), 'user_id__last_name'),
        #         email=F('user_id__email'),
        #         user_job_bid_time=F('jobbid__on_date_time')
        #     )\
        #     .get(id=jobid,jobbid__userid=userId,user_id__user_settings__quote_email=1)
        client = Jobbid.objects\
                .annotate(
                    full_name=Concat('jobtypeid__user_id__first_name',
                                        Value(' '), 'jobtypeid__user_id__last_name'),
                    email=F('jobtypeid__user_id__email'),
                    user_job_bid_time=F('on_date_time')
                )\
                .get(jobtypeid=jobid, userid=userId, jobtypeid__user_id__user_settings__quote_email=1)
    except Jobbid.DoesNotExist:
        return 'Job not found!!'
    user = Users.objects.filter(id=userId)[0]
    emp_name = user.first_name + " " + user.last_name
    act_link = FRONT_URL + "#/previewjob/{}/quotes".format(jobid)
    subject = "Successful. Your job was quoted!"
    send_templated_mail(
        template_name='quote_email',
        from_email=from_email,
        recipient_list=[client.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': client,
            'worker':user,
            'emp_name':emp_name,
            'unsubscribe_url':FRONT_URL +"#/settings"
        },
    )
    return True


def release_payment_mail(jobid):
    try:
        # client = JobType.objects\
        #     .annotate(
        #         full_name=Concat('user_id__first_name',
        #                          Value(' '), 'user_id__last_name'),
        #         email=F('user_id__email'),
        #         confirm_user_id=F('jobbid__userid'),
        #         client_id=F('user_id')
        #     )\
        #     .get(id=jobid,jobbid__is_confirmed=1)
        client = Jobbid.objects\
            .annotate(
                full_name=Concat('jobtypeid__user_id__first_name',
                                 Value(' '), 'jobtypeid__user_id__last_name'),
                email=F('jobtypeid__user_id__email'),
                confirm_user_id=F('userid'),
                client_id=F('jobtypeid__user_id__id')
            )\
            .get(jobtypeid=jobid,is_confirmed=1, jobtypeid__user_id__user_settings__completed_email=1)
    except Jobbid.DoesNotExist:
        return 'Job not found!!'
    act_link = FRONT_URL + "#/previewjob/{}/{}/{}/jobdetails/release_payment".format(jobid,client.confirm_user_id,client.client_id)
    subject = "Successful. Your job was completed!"
    print("Sending completed email!!")
    send_templated_mail(
        template_name='release_payment_email',
        from_email=from_email,
        recipient_list=[client.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': client,
            'unsubscribe_url':FRONT_URL +"#/settings"
        },
    )
    return True


def rate_for_client_mail(jobid):
    try:
        worker = Jobbid.objects\
            .annotate(
                full_name=Concat('userid__first_name',
                                 Value(' '), 'userid__last_name'),
                email=F('userid__email'),
                worker_id=F('userid')
            )\
            .get(jobtypeid=jobid,is_confirmed=1, jobtypeid__user_id__user_settings__job_paid=1)
    except JobType.DoesNotExist:
        return 'Job not found!!'
    act_link = FRONT_URL + "#/worker/previewjob/{}/{}/jobdetails/rate_for_client".format(jobid,worker.worker_id)
    subject = "Successful. Your job payment released!"
    send_templated_mail(
        template_name='rate_for_client_email',
        from_email=from_email,
        recipient_list=[worker.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': worker,
            'unsubscribe_url':FRONT_URL +"#/settings"
        },
    )
    return True


def referal_email(data):
    subject = "You are invited to join lovely childcare network by your friend"
    act_link = WORDPRESS_URL + '?page=signup&referal_id=' +data['referal_id']
    if data['friend_email']:
        send_templated_mail(
            template_name='referal_email',
            from_email=from_email,
            recipient_list=[data['friend_email']],
            context={
                # 'username': model.email,
                'subject': subject,
                # 'emp_name': model.first_name + " " + model.last_name,
                'act_link': act_link,
                'logo_image': logo_inline_image,
                'logo_top_image': logo_top_image,
                'friend_name':data['friend_full_name'],
                'user_name':data['user_full_name'],
                'unsubscribe_url':FRONT_URL +"#/settings"
            }
        )
        User_referal.objects.filter(id=data['id']).update(referal_url=data['referal_id'])
        return True
    else:
        return False

@csrf_exempt
def test(request):
    testing = job_mail(request.POST.get('jobid'))
    Bulk_mail_GET = Bulk_mail_send.objects\
                    .filter(is_mailed=0)
    if Bulk_mail_GET:
        for B in Bulk_mail_GET:
            to_emails = B.recipient_list.split(',')
            return HttpResponse(len(to_emails))
            for to_email in to_emails:
                contexts = B.context.split(';')
                del contexts[-1]
                for context in contexts:
                    print(context)
                    # contextText = json.loads("{'username': 'Nick@yopmail.com', 'unsubscribe_url': 'http://localhost:9005/#/settings'}")
                    contextText = json.loads(context)
                    contextText['logo_image'] = logo_inline_image
                    contextText['logo_top_image'] = logo_top_image
                    send_templated_mail(
                        recipient_list= [to_email],
                        context=contextText,
                        from_email=from_email,
                        template_name= B.template_name
                    )
                    B.is_mailed = 1
                    B.save()
                    return HttpResponse("Mail sent")
        print("Mail sent successfully")
        return HttpResponse("Mail sent successfully")
                

    # return HttpResponse(testing)

@csrf_exempt
def bulk_queue_mail(request):
    if request.method == "POST":
        # return HttpResponse(send_link())
        users = Users.objects.values_list('email',flat=True).all()[:3]
        # return JSONResponse(users)
        mail = Mailer()
        mail.send_messages(subject='My App account verification',
                   template=os.getcwd()+'/childcare/templates/templated_email/hello.html',
                   context={'customer': "1"},
                   to_emails=users)
        return HttpResponse("Mail sent via celery!!!")

# from celery import Celery
# from django.core.mail import EmailMessage

# app = Celery('tasks', backend='amqp', broker='amqp://guest:guest@localhost:5672/')

# # @app.task
# def send_link():
#     subject = 'Thanks'
#     message = 'body'
#     recipients = ['balu@yopmail.com']
#     email = EmailMessage(subject=subject, body=message, from_email="admin@lovelychildcare.com", to=recipients)
#     email.send()
#     return "FUUUUUUUUUUUU"
#     # return HttpResponse("HElloo")