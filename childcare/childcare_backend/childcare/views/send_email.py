from .imports import *

from django.db.models import F, Value
from django.db.models.functions import Concat

from childcare.models import User_referal
from django.conf import settings
import datetime as dt

from childcare.serializers import BulkemailSerializer

WORDPRESS_URL = "https://www.lovelychildcare.com/"

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

# contents for sending email
from_email = 'admin@lovelychildcare.com'
logo_image = os.getcwd() + "/images/logo.png"
# session = Session()
# test = session['logo_img']
# print(test)
# banner_img = os.getcwd() + "/images/bigImg.jpg"
# fb = os.getcwd() + "/images/facebook.png"
# gplus = os.getcwd() + "/images/googleplus.png"
# linkedin = os.getcwd() + "/images/linkedin.png"
# twitter = os.getcwd() + "/images/twitter.png"
# youtube = os.getcwd() + "/images/youtube.png"
# center_img = os.getcwd() + "/images/center.png"
# left_img = os.getcwd() + "/images/left.png"
# shadow = os.getcwd() + "/images/shadow.png"
logo_top_image = os.getcwd() + "/images/logo-top.png"

with open(logo_top_image, 'rb') as logo:
    image_logo_top = logo.read()
logo_top_image = InlineImage(
    filename="logo.png", content=image_logo_top)
with open(logo_image, 'rb') as logo:
    image_logo = logo.read()
logo_inline_image = InlineImage(
    filename="logo.png", content=image_logo)
# with open(banner_img, 'rb') as bg:
#     image_bg = bg.read()
# banner_im = InlineImage(
#     filename="bigImg.png", content=image_bg)
# with open(fb, 'rb') as bg:
#     image_fb = bg.read()
# fb_im = InlineImage(
#     filename="facebook.png", content=image_fb)
# with open(gplus, 'rb') as gp:
#     image_gplus = gp.read()
# gplus_im = InlineImage(
#     filename="googleplus.png", content=image_gplus)
# with open(linkedin, 'rb') as lk:
#     image_linkedin = lk.read()
# linkedin_im = InlineImage(
#     filename="linkedin.png", content=image_linkedin)
# with open(twitter, 'rb') as tw:
#     image_twitter = tw.read()
# twitter_im = InlineImage(
#     filename="twitter.png", content=image_twitter)
# with open(youtube, 'rb') as tw:
#     image_youtube = tw.read()
# youtube_im = InlineImage(
#     filename="youtube.png", content=image_youtube)
# with open(center_img, 'rb') as tw:
#     image_center = tw.read()
# center_img_im = InlineImage(
#     filename="center.png", content=image_center)
# with open(left_img, 'rb') as tw:
#     image_left = tw.read()
# left_img_im = InlineImage(
#     filename="left.png", content=image_left)
# with open(shadow, 'rb') as tw:
#     image_shadow = tw.read()
# shadow_im = InlineImage(
#     filename="left.png", content=image_shadow)

# send mail to users when a job is posted and activated


def job_mail(jobId,partnerid):
    """
            send jobId here when a job is posted
    """
    try:
        # job = JobType.objects\
        #     .annotate(rate_per_hour=F('jobtime_budget__rate_per_hour'),
        #             job_time=F('jobtime_budget__time'),
        #             job_date=F('jobtime_budget__date'),
        #             location=F('jobaddress__location'),
        #             post_code = F('jobaddress__post_code'),
        #             city = F('jobaddress__city'),
        #             state = F('jobaddress__state')
        #             )\
        #     .get(id=jobId)

        job = JobType.objects\
            .annotate(rate_per_hour=F('jobtime_budget__rate_per_hour'),
                    job_time=F('jobtime_budget__time'),
                    job_date=F('jobtime_budget__date'),
                    location=F('jobaddress__location'),
                    post_code = F('jobaddress__post_code'),
                    city = F('jobaddress__city'),
                    state = F('jobaddress__state'),
                    mailed = F('is_mailed'),
                    jobid=F('id'),
                    caregiver=F('caregiver_type'),
                    session = F("care_session"),
                    caretype=F("care_type")
                    ).values()\
            .get(id=jobId)
        job['job_date'] = job['job_date'].strftime("%d-%m-%Y")
        job['job_time'] = job['job_time'].strftime("%H:%M")
        job['on_date_time'] = job['on_date_time'].strftime("%d-%m-%Y %H:%M")
        if job['updated']:
            job['updated'] = job['updated'].strftime("%d-%m-%Y %H:%M")
    except JobType.DoesNotExist:
        return 'Job not found!!'
    if job['mailed'] == 0:
        worker_filter = Q(job_email=1, userid__is_active=1, userid__is_delete=0,userid__is_blocked=0,userid__partnerid=partnerid) &\
            (Q(userid__purpose='1') | Q(userid__purpose='worker'))
        user_emails = User_settings.objects\
            .values('userid__id','userid__email', 'userid__first_name', 'userid__last_name','userid__partnerid__partner_subdomain')\
            .filter(worker_filter)
        if len(user_emails) > 0:

            listData = []
            for m in user_emails:
                saveData = {}
                act_link = m['userid__partnerid__partner_subdomain']+'/' + \
                "#/worker/previewjob/{}/{}/jobdetails/workerviewjobMail".format(jobId,m['userid__id'])
                saveData['action_link'] = act_link
                saveData['username'] = m['userid__first_name'] + " " + m['userid__last_name']
                saveData['recipient_list'] = m['userid__email']
                saveData['template_name'] = 'new_job_email'
                context={
                        "username": m['userid__email'],
                        "emp_name": (m['userid__first_name'] + " " + m['userid__last_name']).title(),
                        "act_link": act_link,
                        # "logo_image": logo_inline_image,
                        # "logo_top_image": logo_top_image,
                        "job": job,
                        "unsubscribe_url":m['userid__partnerid__partner_subdomain']+'/' +"#/settings"
                }
                saveData['unsubscribe_url'] = m['userid__partnerid__partner_subdomain']+'/' +"#/settings"
                saveData['job_type_id'] = job['id'] 
                saveData['context'] = json.dumps(context)
                # saveData['on_date_time'] = 

                listData.append(saveData.copy())
            # print(listData[0]['context'])
            contextData = json.loads(listData[0]['context'])
            # print(contextData[''])
            saveData = {}
            act_link = listData[0]['action_link']
            unsubscribe_url = listData[0]['unsubscribe_url']
            saveData['action_link'] = act_link
            saveData['username'] = 'Admin'
            saveData['recipient_list'] = settings.PROJECT_ADMIN_EMAIL
            saveData['template_name'] = 'new_job_email'
            context={
                    "username":settings.PROJECT_ADMIN_EMAIL,
                    "emp_name": 'Admin',
                    "act_link": act_link,
                    # "logo_image": logo_inline_image,
                    # "logo_top_image": logo_top_image,
                    "job": job,
                    "unsubscribe_url":unsubscribe_url
            }
            saveData['job_type_id'] = job['id'] 
            saveData['context'] = json.dumps(context)
            listData.append(saveData.copy())

            serializer = BulkemailSerializer(data=listData,many=True)
            if serializer.is_valid():
                serializer.save()
            else:
                return serializer.errors
                # send_templated_mail(
                #     template_name='new_job_email',
                #     from_email=from_email,
                #     recipient_list=[m['userid__email']],
                #     context={
                #         'username': m['userid__email'],
                #         'emp_name': m['userid__first_name'] + " " + m['userid__last_name'],
                #         'act_link': act_link,
                #         'logo_image': logo_inline_image,
                #         'logo_top_image': logo_top_image,
                #         'job': job,
                #         'unsubscribe_url':FRONT_URL +"#/settings"
                #     },
                #     bcc=[settings.PROJECT_ADMIN_EMAIL],
                # )
            # job.is_mailed = 1
            # job.save()
        JobType.objects.filter(id=job['jobid']).update(is_mailed=1)
        return True
    else:
        return 'Job mail already sent!!'


# To retrive the forget password
@csrf_exempt
def forget_password(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if "email" not in data:
            result = {"Status": "200",
                      "message": "Email is required to continue!!"}
            return JSONResponse(result)
        try:
            model = Users.objects.get(email=data['email'])
        except Users.DoesNotExist:
            result = {
                "Status": "404", "message": "We couldn't find an account associated with this email address"}
            return JSONResponse(result)
        # if model.is_forget == 1:
        #     result = {"Status": "500",
        #               "message": "An email with new password link has been sent already!!"}
        #     return JSONResponse(result)
        if model.is_active == 0:
            result = {"Status": "500",
                      "message": "Your account is not activated yet, please check your email for the activation link"}
            return JSONResponse(result)
        logo_image = os.getcwd() + "/../dist/partner_logo/"+model.partnerid.partner_logo
        logo_top_image = os.getcwd() + "/../dist/partner_logo/"+model.partnerid.partner_logo
        displayname1 = model.partnerid.partner_logo
        displayname2 = model.partnerid.partner_logo
        # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
        if not os.path.isfile(logo_image):
            logo_image = os.getcwd() + "/images/logo.png"
            logo_top_image = os.getcwd() + "/images/logo-top.png"
            displayname1 = 'logo.png'
            displayname2 = 'logo-top.png'
        # banner_img = os.getcwd() + "/images/bigImg.jpg"
        # fb = os.getcwd() + "/images/facebook.png"
        # gplus = os.getcwd() + "/images/googleplus.png"
        # linkedin = os.getcwd() + "/images/linkedin.png"
        # twitter = os.getcwd() + "/images/twitter.png"
        with open(logo_image, 'rb') as logo:
            image_logo = logo.read()
        logo_inline_image = InlineImage(
            filename=displayname1, content=image_logo)
        with open(logo_top_image, 'rb') as logo:
            image_logo_top = logo.read()
        logo_top_image = InlineImage(
            filename=displayname1, content=image_logo_top)
        signer = Signer()
        # gives 'email@email.com:signed_things', extract signed_things'
        signed_value = signer.sign(data['email'])
        print(signed_value)
        key = ''.join(signed_value.split(':')[1:])+get_random_string(6)
        act_link = model.partnerid.partner_subdomain+'/' + "#/checktoken?id=" + \
            str(model.id) + "&token=" + key + "&email=" + model.email
        model.forget_token = key
        model.save()
        send_templated_mail(
            template_name='forgetemail',
            from_email=from_email,
            recipient_list=[model.email],
            context={
                'username': model.email,
                'emp_name': (model.first_name + " " + model.last_name).title(),
                'act_link': act_link,
                'logo_image': logo_inline_image,
                'logo_top_image': logo_top_image,
                # 'banner_img': banner_im,
                # 'fb': fb_im,
                # 'twitter': twitter_im,
                # 'linkedin': linkedin_im,
                # 'gplus': gplus_im
                'unsubscribe_url':model.partnerid.partner_subdomain+'/' +"#/settings"
            },
        )
        result = {"Status": "200", "message": "An email with new password link has been sent to your email id."}
        return JSONResponse(result)


def sorry_email(to_ids, jobid):
    subject = "Sorry! your quote was not accepted by the client"
    jobId = jobid
    
    resData = []
    for to_id in to_ids:
        user = Users.objects.filter(email=to_id,user_settings__job_sorry=1)
        if user.exists():
            user = user[0]
            emp_name = (user.first_name + " " + user.last_name).title()
            act_link = user.partnerid.partner_subdomain+'/' + "#/joblist/available"
            saveData = {}
            saveData['username'] = emp_name
            saveData['recipient_list'] = to_id
            saveData['template_name'] = 'sorry_email'
            context={
                    # "username": m['userid__email'],
                    "emp_name": emp_name,
                    "act_link": act_link,
                    'jobId': jobId,
                    "unsubscribe_url":user.partnerid.partner_subdomain+'/' +"#/clientsettings",
                    "subject":subject,
                    # 'logo_top_image': logo_top_image,
                    # 'logo_image': logo_inline_image
            }
            saveData['context'] = json.dumps(context)
            resData.append(saveData.copy())
    serializer = BulkemailSerializer(data=resData,many=True)
    if serializer.is_valid():
        serializer.save()
    else:
        return serializer.errors
    return True
            # send_templated_mail(
            #     template_name='sorry_email',
            #     from_email=from_email,
            #     recipient_list=[to_id],
            #     context={
            #         # 'username': model.email,
            #         'subject': subject,
            #         'emp_name': emp_name,
            #         'act_link': act_link,
            #         'jobId': jobId,
            #         'logo_top_image': logo_top_image,
            #         'logo_image': logo_inline_image,
            #         'unsubscribe_url':FRONT_URL +"#/clientsettings"
            #     },
            # )



def confirm_email(to_id, jobid):
    try:
        client = JobType.objects\
            .annotate(
                full_name=Concat('user_id__first_name',
                                 Value(' '), 'user_id__last_name'),
                job_address=F('jobaddress__address'),
                client_city=F('jobaddress__city'),
                client_state=F('jobaddress__state'),
                client_post_code=F('jobaddress__post_code'),
                job_time=F('jobtime_budget__time'),
                job_date=F('jobtime_budget__date'),
            )\
            .get(id=jobid)
    except JobType.DoesNotExist:
        return 'Job not found!!'
    # print("confirm time",client.job_time)
    client.job_time = str(client.job_time)
    user = Users.objects.values('first_name','last_name','id').annotate(emailEnabled=F('user_settings__job_confirmed'),partner_logo=F("partnerid__partner_logo"),partner_subdomain=F('partnerid__partner_subdomain')).filter(email=to_id)
    if user.exists():
        user = user[0]
        emp_name = (user['first_name'] + " " + user['last_name']).title()
        act_link = user['partner_subdomain']+'/' + "#/worker/previewjob/{}/{}/jobdetails/workerviewjobMail".format(jobid,user['id'])
        subject = "Successful. Your job booking has been confirmed!"
        logo_image = os.getcwd() + "/../dist/partner_logo/"+user['partner_logo']
        logo_top_image = os.getcwd() + "/../dist/partner_logo/"+user['partner_logo']
        displayname1 = user['partner_logo']
        displayname2 = user['partner_logo']
        # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
        # print(os.path.isfile(logo_image))
        if os.path.isfile(logo_image) == False:
            print('test')
            logo_image = os.getcwd() + "/images/logo.png"
            logo_top_image = os.getcwd() + "/images/logo-top.png"
            displayname1 = 'logo.png'
            displayname2 = 'logo-top.png'
        # print(logo_image)
        with open(logo_image, 'rb') as logo:
            image_logo = logo.read()
        logo_inline_image = InlineImage(
            filename=displayname1, content=image_logo)
        with open(logo_top_image, 'rb') as logo:
            image_logo_top = logo.read()
        logo_top_image = InlineImage(
        filename=displayname2, content=image_logo_top)
        if user['emailEnabled'] == 1:
            send_templated_mail(
                template_name='confirm_email',
                from_email=from_email,
                recipient_list=[to_id],
                context={
                    # 'username': model.email,
                    'subject': subject,
                    # 'emp_name': model.first_name + " " + model.last_name,
                    'act_link': act_link,
                    'logo_image': logo_inline_image,
                    'logo_top_image': logo_top_image,
                    'client': client,
                    'emp_name':emp_name,
                    'unsubscribe_url':user['partner_subdomain']+'/' +"#/settings"
                    # 'banner_img': banner_im,
                    # 'fb': fb_im,
                    # 'twitter': twitter_im,
                    # 'linkedin': linkedin_im,
                    # 'gplus': gplus_im
                },
                bcc=[settings.PROJECT_ADMIN_EMAIL],
            )
        else:
            send_templated_mail(
                template_name='confirm_email',
                from_email=from_email,
                recipient_list=[settings.PROJECT_ADMIN_EMAIL],
                context={
                    # 'username': model.email,
                    'subject': subject,
                    # 'emp_name': model.first_name + " " + model.last_name,
                    'act_link': act_link,
                    'logo_image': logo_inline_image,
                    'logo_top_image': logo_top_image,
                    'client': client,
                    'emp_name':emp_name,
                    'unsubscribe_url':FRONT_URL +"#/settings"
                    # 'banner_img': banner_im,
                    # 'fb': fb_im,
                    # 'twitter': twitter_im,
                    # 'linkedin': linkedin_im,
                    # 'gplus': gplus_im
                },
            )

        return True
    else:
        return False

# user quote job to trigger client email

def quote_mail(userId, jobid):
    try:
        # client = JobType.objects\
        #     .annotate(
        #         full_name=Concat('user_id__first_name',
        #                          Value(' '), 'user_id__last_name'),
        #         email=F('user_id__email'),
        #         user_job_bid_time=F('jobbid__on_date_time')
        #     )\
        #     .get(id=jobid,jobbid__userid=userId,user_id__user_settings__quote_email=1)
        client = Jobbid.objects\
                .annotate(
                    full_name=Concat('jobtypeid__user_id__first_name',
                                        Value(' '), 'jobtypeid__user_id__last_name'),
                    email=F('jobtypeid__user_id__email'),
                    user_job_bid_time=F('on_date_time'),
                    client_id=F('jobtypeid__user_id__id'),
                    client_subdomain=F('jobtypeid__user_id__partnerid__partner_subdomain'),
                    client_logo=F('jobtypeid__user_id__partnerid__partner_logo')
                )\
                .get(jobtypeid=jobid, userid=userId, jobtypeid__user_id__user_settings__quote_email=1)
    except Jobbid.DoesNotExist:
        return 'Job not found!!'
    print("quote email",client.user_job_bid_time)
    user = Users.objects.filter(id=userId)[0]
    emp_name = (user.first_name + " " + user.last_name).title()
    act_link = client.client_subdomain+'/' + "#/previewjob/{}/{}/quotes/previewJobClientMail".format(jobid,client.client_id)
    subject = "New quote received for your job"
    logo_image = os.getcwd() + "/../dist/partner_logo/"+client.client_logo
    logo_top_image = os.getcwd() + "/../dist/partner_logo/"+client.client_logo
    displayname1 = client.client_logo
    displayname2 = client.client_logo
    # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
    if not os.path.isfile(logo_image):
        logo_image = os.getcwd() + "/images/logo.png"
        logo_top_image = os.getcwd() + "/images/logo-top.png"
        displayname1 = 'logo.png'
        displayname2 = 'logo-top.png'
    with open(logo_image, 'rb') as logo:
        image_logo = logo.read()
    logo_inline_image = InlineImage(
        filename=displayname1, content=image_logo)
    with open(logo_top_image, 'rb') as logo:
        image_logo_top = logo.read()
    logo_top_image = InlineImage(
    filename=displayname1, content=image_logo_top)
    send_templated_mail(
        template_name='quote_email',
        from_email=from_email,
        recipient_list=[client.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': client,
            'worker':user,
            'emp_name':emp_name,
            'unsubscribe_url':client.client_subdomain+'/' +"#/settings"
        },
    )
    return True


def release_payment_mail(jobid):
    try:
        client = Jobbid.objects\
            .annotate(
                full_name=Concat('jobtypeid__user_id__first_name',
                                 Value(' '), 'jobtypeid__user_id__last_name'),
                email=F('jobtypeid__user_id__email'),
                confirm_user_id=F('userid'),
                client_id=F('jobtypeid__user_id__id'),
                client_subdomain=F('jobtypeid__user_id__partnerid__partner_subdomain'),
                client_logo=F('jobtypeid__user_id__partnerid__partner_logo')
            )\
            .get(jobtypeid=jobid,is_confirmed=1, jobtypeid__user_id__user_settings__completed_email=1)
    except Jobbid.DoesNotExist:
        return 'Job not found!!'

    # signer = Signer()
    # signed_value = signer.sign(str(client.email))
    key = base64.urlsafe_b64encode(str(client.email))
    act_link = client.client_subdomain+'/' + "#/previewjob/{}/{}/{}/jobdetails/{}/release_payment".format(jobid,client.confirm_user_id,client.client_id,key)
    subject = "Job Completed. Please release payment"
    logo_image = os.getcwd() + "/../dist/partner_logo/"+client.client_logo
    logo_top_image = os.getcwd() + "/../dist/partner_logo/"+client.client_logo
    displayname1 = client.client_logo
    displayname2 = client.client_logo
    # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
    if not os.path.isfile(logo_image):
        logo_image = os.getcwd() + "/images/logo.png"
        logo_top_image = os.getcwd() + "/images/logo-top.png"
        displayname1 = 'logo.png'
        displayname2 = 'logo-top.png'
    with open(logo_image, 'rb') as logo:
        image_logo = logo.read()
    logo_inline_image = InlineImage(
        filename=displayname1, content=image_logo)
    with open(logo_top_image, 'rb') as logo:
        image_logo_top = logo.read()
    logo_top_image = InlineImage(
    filename=displayname1, content=image_logo_top)
    print("Sending completed email!!")
    send_templated_mail(
        template_name='release_payment_email',
        from_email=from_email,
        recipient_list=[client.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': client,
            'jobId': jobid,
            'unsubscribe_url':client.client_subdomain+'/' +"#/settings"
        },
    )
    return True


def rate_for_client_mail(jobid):
    try:
        worker = Jobbid.objects\
            .annotate(
                full_name=Concat('userid__first_name',
                                 Value(' '), 'userid__last_name'),
                email=F('userid__email'),
                worker_id=F('userid'),
                worker_subdomain=F('userid__partnerid__partner_subdomain'),
                worker_logo=F('userid__partnerid__partner_logo')
            )\
            .get(jobtypeid=jobid,is_confirmed=1, jobtypeid__user_id__user_settings__job_paid=1)
    except JobType.DoesNotExist:
        return 'Job not found!!'
    act_link = worker.worker_subdomain+'/' + "#/worker/previewjob/{}/{}/jobdetails/rate_for_client".format(jobid,worker.worker_id)
    subject = "Payment released."
    logo_image = os.getcwd() + "/../dist/partner_logo/"+worker.worker_logo
    logo_top_image = os.getcwd() + "/../dist/partner_logo/"+worker.worker_logo
    displayname1 = worker.worker_logo
    displayname2 = worker.worker_logo
    # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
    if not os.path.isfile(logo_image):
        logo_image = os.getcwd() + "/images/logo.png"
        logo_top_image = os.getcwd() + "/images/logo-top.png"
        displayname1 = 'logo.png'
        displayname2 = 'logo-top.png'
    with open(logo_image, 'rb') as logo:
        image_logo = logo.read()
    logo_inline_image = InlineImage(
        filename=displayname1, content=image_logo)
    with open(logo_top_image, 'rb') as logo:
        image_logo_top = logo.read()
    logo_top_image = InlineImage(
    filename=displayname1, content=image_logo_top)
    send_templated_mail(
        template_name='rate_for_client_email',
        from_email=from_email,
        recipient_list=[worker.email],
        context={
            'subject': subject,
            'act_link': act_link,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'client': worker,
            'unsubscribe_url':worker.worker_subdomain+'/' +"#/settings"
        },
    )
    return True


def referal_email(data):
    subject = "You are invited to join lovely childcare network by your friend"
    act_link = WORDPRESS_URL + '?page=signup&referal_id=' +data['referal_id']
    if data['friend_email']:
        send_templated_mail(
            template_name='referal_email',
            from_email=from_email,
            recipient_list=[data['friend_email']],
            context={
                # 'username': model.email,
                'subject': subject,
                # 'emp_name': model.first_name + " " + model.last_name,
                'act_link': act_link,
                'logo_image': logo_inline_image,
                'logo_top_image': logo_top_image,
                'friend_name':data['friend_full_name'],
                'user_name':data['user_full_name'],
                'unsubscribe_url':FRONT_URL +"#/settings"
            }
        )
        User_referal.objects.filter(id=data['id']).update(referal_url=data['referal_id'])
        return True
    else:
        return False


# cron for bulk emails

@csrf_exempt
def cron_bulk_mail(request):
    Bulk_mail_GET = Bulk_mail_send.objects\
                    .filter(is_mailed=0)
    if Bulk_mail_GET.exists():
        has_bcc = []
        for B in Bulk_mail_GET:
            # contextText = json.loads("{'username': 'Nick@yopmail.com', 'unsubscribe_url': 'http://localhost:9005/#/settings'}")
            contextText = json.loads(B.context)
            try:
                user = Users.objects.get(email=B.recipient_list)
            except:
                continue
            logo_image = os.getcwd() + "/../dist/partner_logo/"+user.partnerid.partner_logo
            logo_top_image = os.getcwd() + "/../dist/partner_logo/"+user.partnerid.partner_logo
            displayname1 = user.partnerid.partner_logo
            displayname2 = user.partnerid.partner_logo
            # csv_path = os.getcwd() + "/../dist/partner_logo/"+model.partner_logo
            if not os.path.isfile(logo_image):
                logo_image = os.getcwd() + "/images/logo.png"
                logo_top_image = os.getcwd() + "/images/logo-top.png"
                displayname1 = 'logo.png'
                displayname2 = 'logo-top.png'
            # banner_img = os.getcwd() + "/images/bigImg.jpg"
            # fb = os.getcwd() + "/images/facebook.png"
            # gplus = os.getcwd() + "/images/googleplus.png"
            # linkedin = os.getcwd() + "/images/linkedin.png"
            # twitter = os.getcwd() + "/images/twitter.png"
            with open(logo_image, 'rb') as logo:
                image_logo = logo.read()
            logo_inline_image = InlineImage(
                filename=displayname1, content=image_logo)
            with open(logo_top_image, 'rb') as logo:
                image_logo_top = logo.read()
            logo_top_image = InlineImage(
                filename=displayname1, content=image_logo_top)
            contextText['logo_image'] = logo_inline_image
            contextText['logo_top_image'] = logo_top_image
            if B.template_name == 'new_job_email':
                has_bcc.append(contextText)
            send_templated_mail(
                recipient_list= [B.recipient_list],
                context=contextText,
                from_email=from_email,
                template_name= B.template_name
            )
            B.is_mailed = 1
            B.on_date_time = dt.datetime.now()
            B.save()
        # if len(has_bcc):
        #     print('admin')
        #     contextText['username'] = 'Admin'
        #     send_templated_mail(
        #         recipient_list= [settings.PROJECT_ADMIN_EMAIL],
        #         context=contextText,
        #         from_email=from_email,
        #         template_name= 'new_job_email',
        #     )
            print("Job mail sent to admin!!!")
        # return HttpResponse("Mail sent")
    print("Mail sent successfully")
    sorting_workers()
    return HttpResponse("Mail sent successfully")


def client_offer_mail(data):
    subject = "New offer claim"
    country_code = Country_codes.objects.filter(id=data['country_code'])
    data['country_name'] = country_code[0].country_name
    send_templated_mail(
        template_name='client_offer_email',
        from_email=from_email,
        recipient_list=[settings.ADMIN_EMAIL],
        context={
            # 'username': model.email,
            'subject': subject,
            # 'emp_name': model.first_name + " " + model.last_name,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'data':data,
        }
    )
    return True
# @csrf_exempt
# def test(request):
#     t = "Print paste"

def contact_mail_to_admin(data,messageDetails):
    subject = "New message to the carer"
    user_details = Users.objects.annotate(full_name=Concat('first_name',
                                 Value(' '), 'last_name')).values().filter(Q(id=data['from_user']) | Q(id=data['worker_id']))
    temp = {}
    for user in user_details:
        if user['id'] == data['from_user']:
            temp['client_name'] = user['full_name']
            temp['client_email'] = user['email']
        elif user['id'] == data['worker_id']:
            temp['worker_name'] = user['full_name']
            temp['worker_email'] = user['email']
    # print("userDetails",temp)
    send_templated_mail(
        template_name='contact_message',
        from_email=from_email,
        recipient_list=[settings.ADMIN_EMAIL],
        context={
            # 'username': model.email,
            'subject': subject,
            # 'emp_name': model.first_name + " " + model.last_name,
            'logo_image': logo_inline_image,
            'logo_top_image': logo_top_image,
            'data':temp,
            "message":messageDetails,
        }
    )


from Crypto.Cipher import AES
import base64

MASTER_KEY=get_random_string(32)

def encrypt_val(clear_text):
    enc_secret = AES.new(MASTER_KEY[:32])
    tag_string = (str(clear_text) +
                  (AES.block_size -
                   len(str(clear_text)) % AES.block_size) * "\0")
    cipher_text = base64.b64encode(enc_secret.encrypt(tag_string))
    return cipher_text

def decrypt_val(cipher_text):
    dec_secret = AES.new(MASTER_KEY[:32])
    raw_decrypted = dec_secret.decrypt(base64.b64decode(cipher_text))
    clear_val = raw_decrypted.decode().rstrip("\0")
    return clear_val


def sorting_workers():
    users = Users.objects.filter(is_active=1,is_delete=0).order_by('-login_count')
    # login_count =Users.objects.values_list('login_count',flat=True).filter(is_active=1,is_delete=0)
    # return(login_count)
    # sorting_login_count = list(login_count).sort()
    # print(sorting_login_count)
    i = 4
    for u in users:
        if u.completeness >=80 and u.completeness <= 100 and u.profpic is not None:
            u.order_by = 1
        elif u.completeness >=80 and u.completeness <=100:
            u.order_by = 2
        elif u.completeness >=15 and u.completeness < 80 and u.profpic is not None:
            u.order_by = 3
        else:
            u.order_by = i
            i = i + 1
        u.save()


    return True

@csrf_exempt
def sort_workers(request):
    return JSONResponse(sorting_workers())