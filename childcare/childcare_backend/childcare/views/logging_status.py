from childcare.models import User_login_status
from childcare.serializers import LoginstatusSerializer

from .imports import *


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def save_logging_status(request):
	data = JSONParser().parse(request)
	serializer = LoginstatusSerializer(data=data,many=False)
	if serializer.is_valid():
		serializer.save()
		result = {"Status":"200","Message":"Login status saved"}
		return JSONResponse(result)
	else:
		result = {"Status":"400","errors":serializer.errors}
		return JSONResponse(result)



def logstatus(data):
	serializer = LoginstatusSerializer(data=data,many=False)
	if serializer.is_valid():
		serializer.save()
		result = {"Status":"200","Message":"Login status saved"}
		return JSONResponse(result)
	else:
		result = {"Status":"400","errors":serializer.errors}
		return JSONResponse(result)