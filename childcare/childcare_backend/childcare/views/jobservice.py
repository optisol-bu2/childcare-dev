from .imports import *
from childcare.views import userlogin as users
from childcare.views import send_email
from decimal import Decimal

from django.db.models import Case, Sum, Max, When, Value, IntegerField, F
from django.db.models import CharField

from django.db.models.functions import Concat
import datetime as dt


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def job_type_insertion(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            care_giver_type = data['selectedCaregiver']
            care_session = data['careSession_selection']
            care_type = data['careType_seletion']
            data['caregiver_type'] = ','.join(str(x) for x in care_giver_type)
            data['care_session'] = ','.join(str(x) for x in care_session)
            data['care_type'] = ','.join(str(x) for x in care_type)
            if 'userid' in data:
                data['user_id'] = data['userid']
            if 'job_type_id' in data:
                jobtypeId = data['job_type_id']
                try:
                    model = JobType.objects.get(id=jobtypeId)
                except JobType.DoesNotExist:
                    serializer = JobtypeSerializer(data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200", "message": "Job type saved!!!",
                                  "job_type_id": serializer.data['id']}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
                    # return JSONResponse(data['caregiver_type'])
                serializer = JobtypeSerializer(model, data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200", "message": "Job type saved!!!",
                              "job_type_id": serializer.data['id']}
                    return JSONResponse(result)
                else:
                    result = {"Status": "500", "error": serializer.errors}
                    return JSONResponse(result)
            else:
                serializer = JobtypeSerializer(data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    result = {"Status": "200", "message": "Job type saved!!!",
                              "job_type_id": serializer.data['id']}
                    return JSONResponse(result)
                else:
                    result = {"Status": "500", "error": serializer.errors}
                    return JSONResponse(result)

        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def job_address_insertion(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'job_type_id' in data:
                jobtypeId = data['job_type_id']
                children_age = data['age_selection']
                tooltipGet = getTooltip(0,'parent')
                if 'loc_id' in data:

                    if isinstance(data['loc_id'], dict):
                        print(data['loc_id'])
                        data['city'] = data['loc_id']['suburb']
                        data['state'] = data['loc_id']['state']
                        data['post_code'] = data['loc_id']['postcode']
                data['children_age'] = ','.join(
                    str(x) for x in children_age)
                try:
                    model = JobAddress.objects.get(job_type_id=jobtypeId)
                except JobAddress.DoesNotExist:
                    serializer = JobAddressSerializer(data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Job Address saved!!!",'tooltip':tooltipGet['tooltip'],'label':tooltipGet['label']}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
                if model:
                    serializer = JobAddressSerializer(
                        model, data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Job Address saved!!!",'tooltip':tooltipGet['tooltip']}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
            else:
                result = {"Status": "500", "message": "job type id required"}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def job_req_insertion(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'job_type_id' in data:
                jobtypeId = data['job_type_id']
                req = data['opt_selection']
                data['requirements'] = ','.join(str(x) for x in req)

                if 'partnerid' not in data:
                    data['partnerid'] = 1
                # return JSONResponse(data)
                try:
                    model = JobRequirements.objects.get(job_type_id=jobtypeId)
                except JobRequirements.DoesNotExist:
                    if 'userid' in data:
                        job_model = JobType.objects.get(id=jobtypeId)
                        job_model.user_id = Users.objects.get(
                            id=data['userid'])
                        job_model.is_active = 1
                        job_model.save()
                    serializer = JobRequirementsSerializer(
                        data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        # return JSONResponse(serializer.data)
                        mail_status = False
                        if 'userid' in data:
                            mail_status = send_email.job_mail(jobtypeId,data['partnerid'])
                        result = {"Status": "200",
                                  "message": "Job requirement saved!!!",
                                  "mail_status": mail_status}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
                if model:
                    if 'userid' in data:
                        print('intha condition la adha')
                        job_model = JobType.objects.get(id=jobtypeId)
                        job_model.user_id = Users.objects.get(
                            id=data['userid'])
                        job_model.is_active = 1
                        job_model.save()
                    serializer = JobRequirementsSerializer(model,
                                                           data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        mail_status = False
                        if 'userid' in data:
                            mail_status = send_email.job_mail(jobtypeId,data['partnerid'])

                        result = {"Status": "200",
                                  "message": "Job requirement saved!!!",
                                  "mail_status": mail_status}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
            else:
                result = {"Status": "500", "message": "job type id required"}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)

@csrf_exempt
def credential_req_insertion(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'job_type_id' in data:
                jobtypeId = data['job_type_id']
                req = data['opt_selection']
                data['requirements'] = ','.join(str(x) for x in req)
                # return JSONResponse(data)
                try:
                    model = JobRequirements.objects.get(job_type_id=jobtypeId)
                except JobRequirements.DoesNotExist:
                    serializer = JobRequirementsSerializer(
                        data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Job requirement saved!!!"}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
                if model:
                    serializer = JobRequirementsSerializer(model,
                                                           data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Job requirement saved!!!"}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
            else:
                result = {"Status": "500", "message": "job type id required"}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)



@csrf_exempt
def job_time_insertion(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        # is_valid = users.authenticate(data['userid'], data['login_token'])
        if True:
            if 'job_type_id' in data:
                jobtypeId = data['job_type_id']
                if 'days_selection' in data:
                    if data['days_selection'] != "":
                        days_required = data['days_selection']
                        data['days_required'] = ','.join(
                            str(x) for x in days_required)
                # data['budget'] = 5.0
                # data['budget'] = evaluate_budget(jobtypeId)
                # return HttpResponse(data['budget'])
                data['date'] = datetime.strptime(
                    data['date'], "%d-%m-%Y")
                # return JSONResponse(data['time'])
                time = datetime.strptime(data['time'], '%H:%M').time()
                data['time'] = time
                # data['time'] = datetime.strptime(
                #     time, "%H:%M:%S")
                data['date'] = data['date'].strftime("%Y-%m-%d")
                try:
                    model = JobTime_Budget.objects.get(job_type_id=jobtypeId)
                except JobTime_Budget.DoesNotExist:
                    serializer = JobTime_BudgetSerializer(
                        data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        reqs = get_requirements(jobtypeId,'jobtime')
                        result = {"Status": "200",
                                  "message": "Job Time/budget saved!!!",
                                  "budget": data['budget'],
                                  "requirements": reqs}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
                if model:
                    serializer = JobTime_BudgetSerializer(model,
                                                          data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        reqs = get_requirements(jobtypeId)
                        result = {"Status": "200",
                                  "message": "Job Time/budget saved!!!",
                                  "budget": data['budget'],
                                  "requirements": reqs}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500", "error": serializer.errors}
                        return JSONResponse(result)
            else:
                result = {"Status": "500", "message": "job type id required"}
                return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "User is invalid!!"}
            return JSONResponse(result)


@csrf_exempt
def get_postal_code(request):
    if request.method == "POST":
        model = Postal_code.objects.all()
        # res = []
        city = []
        code = []
        for m in model:
            # temp = {}
            city.append(m.city)
            code.append(m.code)
            # res.append(temp.copy())
        return JSONResponse({"city": city, "code": set(code)})


@csrf_exempt
def get_state_list(request):
    if request.method == "POST":
        model = States.objects.all()
        res = []
        for m in model:
            temp = {}
            temp["name"] = m.name
            temp["id"] = m.id
            res.append(temp.copy())
        result = {"Status": "200", "states": res}
        return JSONResponse(result)


@csrf_exempt
def get_cities(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        model = Postal_code.objects.filter(state_id=data['state'])
       # model = Cities.objects.all()
        res = []
        for m in model:
            temp = {}
            temp["city"] = m.city
           # temp["id"] = m.id
            res.append(temp.copy())
        result = {"Status": "200", "cities": res}
        return JSONResponse(result)


def update_job_type(usrid, jobtypeid):
    if usrid and jobtypeid:
        model = JobType.objects.get(id=jobtypeid)
        model.user_id = Users.objects.get(id=usrid)
        model.is_active = 1
        model.save()
        send_email.job_mail(jobtypeid)
        return True


@csrf_exempt
def getpostalbycity(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        try:
            model = Postal_code.objects.get(
                city=data['city'], state_id=data['state'])
        except Postal_code.DoesNotExist:
            result = {"Status": "404", "Message": "City not found"}
            return JSONResponse(result)
        # temp = {}
        # temp['city'] = model.city
        # temp["code"] = model.code
        # res = []
        # for m in model:
        #     temp = {}
        #     temp["name"] = m.name
        #     temp["id"] = m.id
        #     res.append(temp.copy())
        result = {"Status": "200", "post_code": model.code}
        return JSONResponse(result)


def evaluate_budget(jobtypeId):
    # data = JSONParser().parse(request)
    # jobtypeId = data['jobtypeId']
    try:
        model = JobTime_Budget.objects.get(job_type_id=jobtypeId)
    except JobTime_Budget.DoesNotExist:
        return 0
    designation = model.job_type_id.caregiver_type
    desig = designation.split(',')
    # return JSONResponse(desig)
    Number_of_hours = data['hours_required']
    percent = Percent_Config.objects.all().first()
    rate_per_hour = data['rate_per_hour']
    # return JSONResponse(rate_per_hour)
    # Rate_per_hour = model.
    if rate_per_hour is not None:
        Estimated = (Number_of_hours * rate_per_hour['rate_per_hour__sum']) + ((percent.x / 100) *
                                                                               percent.platform_cust_service) + ((percent.y / 100) * percent.online_payment) + ((percent.z / 100) * percent.gst)
        return int(Estimated)
    else:
        return 0


@csrf_exempt
def get_evaluated_budget(request):
    data = JSONParser().parse(request)

    # jobtypeId = data['jobtypeId']
    # try:
    #     model = JobType.objects.get(id=jobtypeId)
    # except JobTime_Budget.DoesNotExist:
    #     result = {"Status": "500", "message": "Job type id does not exists"}
    #     return JSONResponse(result)
    # designation = model.caregiver_type
    # desig = designation.split(',')
    Number_of_hours = data['hours_required']
    percent = Percent_Config.objects.all().first()
    rate_per_hour = data['rate_per_hour']
    if rate_per_hour is not None:
        total_cost = Decimal(Number_of_hours) * Decimal(rate_per_hour)
        # cus_service = total_cost + \
        #     (Decimal(percent.x) / Decimal(100)) * total_cost
        # online_pay1 = cus_service + \
        #     (Decimal(percent.y) / Decimal(100)) * cus_service
        # gst = online_pay1 + (Decimal(percent.z) / Decimal(100)) * online_pay1
        # Estimated = round(gst, 2)
        online_cost = total_cost * (Decimal(percent.y)/Decimal(100)) 
        cost_with_online = online_cost + total_cost
        gst_cost = cost_with_online * (Decimal(percent.z)/Decimal(100))
        estimated_budget = cost_with_online + gst_cost
        # print("%.2f" % round(estimated_budget,2))
        result = {"Status": "200", "estimated_budget": round(estimated_budget,2),
                  "x": percent.x, "y": percent.y, "z": percent.z, "total_cost": total_cost}
        return JSONResponse(result)
    else:
        result = {"Status": "500", "message": "Rate per hour does not found"}
        return JSONResponse(result)


@csrf_exempt
def getLocations(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'searchKey' not in data:
            result = {"Status": "500",
                      "message": "Search key is not available"}
            return JSONResponse(result)
        state = Q(state__istartswith=data['searchKey'])
        city = Q(suburb__istartswith=data['searchKey'])
        postcode = Q(postcode__istartswith=data['searchKey'])
        model = Postal_code.objects.filter(state | city | postcode)[:10]
        if model.count() > 0:
            serializer = PostalcodeSerialzier(model, many=True)
            result = {"Status": "200", "data": serializer.data}
            return JSONResponse(result)
        else:
            result = {"Status": "200", "data": []}
            return JSONResponse(result)

# get jobs according to the given filter condition


@csrf_exempt
def getjobs(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        job_status = data['job_status']
        job_portal_check_Query = Q()
        if data['show_partner_portals'] == '0':
            job_portal_check_Query = Q(job_type_id__user_id__partnerid=data['partnerid'])
        default = ~Q(job_type_id__user_id=None) & Q(job_type_id__is_active=1) & Q(
            job_type_id__is_delete=0) & job_portal_check_Query
        paymentPendingQuery = Q()
        # if 'userid' in data:
        #     paymentPendingQuery = Q(Q(job_type_id__jobbid__is_confirmed=0,job_type_id__payment_pending=0) | Q(job_type_id__jobbid__is_confirmed=1,job_type_id__payment_pending=1))
        # paymentPendingQuery = Q()
        # if 'userid' in data:
        #     paymentPendingQuery = ~Q(job_type_id__jobbid__userid__in=data['userid']) & Q(job_type_id__jobbid__is_confirmed=1)
        # if 'adminLogged' in data:
        #     if data['adminLogged'] == True:
        #         # blockQuery = Q()
        #         blockQuery = Q()
        #     else:
        #         blockQuery = Q(job_type_id__is_blocked=0)
        # else:
        blockQuery = Q(job_type_id__is_blocked=0)
        today = datetime.now()
        a_week_before = today - dt.timedelta(days=7)
        # for pagination
        if 'pageno' not in data or 'limit' not in data:
            result = {"Status": '500',
                      "Message": "pagenumber & limit cannot be empty"}
            return JSONResponse(result)
        else:
            limit = data['limit']
            page_no = data['pageno']
            if page_no == 1:
                start_limit = 0
                end_limit = limit
            elif page_no > 1:
                start_limit = limit * (page_no - 1)
                end_limit = limit + start_limit
        # Filter conditions
        caretime_filter = Q()
        jobtype_filter = Q()
        requirement_fil = Q()
        session_filter = Q()
        care_giver1 = Q()
        if len(data['caretime']):
            for x in data['caretime']:
                caretime_filter = caretime_filter | Q(
                    job_type_id__care_type__icontains=x)
        if len(data['jobtype']):
            jobtype_filter = Q(job_type_id__job_type__in=data['jobtype'])
        if len(data['requirements']):
            requirement_fil = Q()
            for x in data['requirements']:
                requirement_fil = requirement_fil | Q(
                    requirements__icontains=x)
        if len(data['session']):
            for x in data['session']:
                session_filter = session_filter | Q(
                    job_type_id__care_session__icontains=x)
        if len(data['caregivers']):

            for x in data['caregivers']:
                care_giver1 = care_giver1 | Q(
                    job_type_id__caregiver_type__icontains=x)
        location_filter = Q()
        if 'fullAddress' in data:
            if isinstance(data['fullAddress'], dict):
                if data['fullAddress']['postcode'] != "":
                    post_code = int(data['fullAddress']['postcode'])
                    post_code_range = range(post_code - 10, post_code + 10)
                    location_filter = location_filter | Q(
                        post_code__in=post_code_range)
        # user_ids = []
        date_ids = []
        if 'date' in data:
            if data['date'] != "":
                print(data['date'])
                date_given = datetime.strptime(data['date'], "%d-%m-%Y")
                # print(date_given)
                date_filter = JobTime_Budget.objects.filter(
                    date__range=[date_given, date_given])
                # print(date_filter.query)
                date_ids = [x.job_type_id.id for x in date_filter]
                req_model = JobRequirements.objects.filter(
                    requirement_fil & Q(job_type_id__in=date_ids))
                req_ids = [x.job_type_id.id for x in req_model]
        else:
            req_model = JobRequirements.objects.filter(
                requirement_fil)
            req_ids = [x.job_type_id.id for x in req_model]
            # print(req_ids)
            # print(user_ids)
        # applied_jobs = Jobbid.objects.values_list(
        #     'jobtypeid').filter(userid=data['userid'], is_confirmed=0)
        order_by = ['-job_type_id__on_date_time']
        if job_status == 1:  # applied jobs
            filter_applied = Q(job_type_id__jobbid__userid=data[
                               'userid'],  job_type_id__is_mailed=1) & Q(Q(job_type_id__jobbid__is_confirmed=0) | Q(job_type_id__jobbid__is_confirmed=1,job_type_id__payment_pending=1))
            order_by = ['job_type_id__job_status', '-job_type_id__updated']
        elif job_status == 2:  # confirmed jobs
            filter_applied = Q(job_type_id__job_status=2,
                               job_type_id__jobbid__is_confirmed=1,
                               job_type_id__jobbid__userid=data['userid']) & Q(job_type_id__is_mailed=1)
        elif job_status == 3:  # Completed jobs
            filter_applied = Q(job_type_id__jobbid__userid=data['userid'],
                               job_type_id__jobbid__is_confirmed=1) & Q(
                job_type_id__job_status=3) & Q(
                job_type_id__jobbid__is_confirmed=1) & Q(job_type_id__is_mailed=1)
        elif job_status == 4:  # Paid jobs
            filter_applied = Q(job_type_id__jobbid__userid=data[
                               'userid'],
                               job_type_id__jobbid__is_confirmed=1)\
                & Q(job_type_id__job_status=4) & Q(job_type_id__is_mailed=1)
        elif job_status == 7:  # Closed jobs
            filter_applied = Q(job_type_id__job_status=7, job_type_id__jobbid__userid=data[
                               'userid'], job_type_id__jobbid__is_confirmed=1) & Q(
                job_type_id__is_mailed=1)
        elif job_status == -1:
            filter_applied = Q()
        else:
            filter_applied = ~Q(job_type_id__jobbid__userid=data['userid'])\
                & Q(job_type_id__is_confirmed=0) & Q(job_type_id__is_mailed=1) & Q(job_type_id__payment_pending=0)

            # filter_applied = ~Q(job_type_id__jobbid__userid=data['userid'],
            #                     job_type_id__jobbid__is_confirmed=0)\
            #     & Q(job_type_id__is_confirmed=0) & Q(job_type_id__is_mailed=1)

        filter_available = ~Q(job_type_id__jobbid__userid=data['userid'],
                              job_type_id__jobbid__is_confirmed=0)\
            & Q(job_type_id__is_confirmed=0) & Q(job_type_id__is_mailed=1)
        # join query from address table
        if len(req_ids):
            address_model = JobAddress.objects.filter(blockQuery &
                default & filter_applied & caretime_filter & location_filter
                & jobtype_filter & session_filter & care_giver1,
                job_type_id__in=req_ids)\
                .select_related()\
                .order_by(*order_by)[start_limit:end_limit]
            total_count = JobAddress.objects.filter(blockQuery &
                default & filter_applied & caretime_filter
                & location_filter & jobtype_filter & session_filter
                & care_giver1, job_type_id__in=req_ids).count()

            
            available_count = JobAddress.objects.filter(paymentPendingQuery & blockQuery &
                default & filter_available & caretime_filter & location_filter &
                jobtype_filter & session_filter & care_giver1, job_type_id__in=req_ids,job_type_id__payment_pending=0).count()
        else:
            address_model = JobAddress.objects.filter(blockQuery &
                default & filter_applied & caretime_filter
                & location_filter & jobtype_filter
                & session_filter & care_giver1)\
                .select_related()\
                .order_by(*order_by)[start_limit:end_limit]
            total_count = JobAddress.objects.filter(blockQuery &
                default & filter_applied & caretime_filter & location_filter & jobtype_filter & session_filter & care_giver1).count()
            available_count = JobAddress.objects.filter(paymentPendingQuery & blockQuery &
                default & filter_available & caretime_filter & location_filter &
                jobtype_filter & session_filter & care_giver1,job_type_id__payment_pending=0).count()

        # return HttpResponse(address_model.query)
        res = []
        # return HttpResponse(address_model.query)
        if req_model.count() > 0:
            percent = Percent_Config.objects.all().first()
            for m in address_model:
                temp = {}
                temp['jobid'] = m.job_type_id.id
                try:
                    temp['user_id'] = m.job_type_id.user_id.id
                except Users.DoesNotExist:
                    continue
                temp['user_name'] = m.job_type_id.user_id.first_name + \
                    " " + m.job_type_id.user_id.last_name
                temp['profile_suits'] = m.job_type_id.user_id.profile_suits
                if m.job_type_id.user_id.profpic:
                    temp['client_profilepic'] = m.job_type_id.user_id.profpic.url
                else:
                    temp['client_profilepic'] = ""
                temp['applied_job'] = 0
                temp['quoted'] = 0
                temp['all_jobs'] = 0
                jobbidModel = Jobbid.objects.filter(
                            jobtypeid=m.job_type_id.id, userid=data['userid'])
                if jobbidModel.exists():
                    temp['is_bidded'] = True
                else:
                    temp['is_bidded'] = False

                temp['is_confirmed'] = 0
                temp['job_status'] = m.job_type_id.job_status
                temp['unsuccessful'] = 0

                if job_status == 1:
                    bid_info = getBidinfo(
                        m.job_type_id.id, data['userid'])
                    if m.job_type_id.job_status != 1 and m.job_type_id.job_status != 0:
                        # temp['unsuccessful'] = 1
                        if bid_info['on_date_time'].replace(tzinfo=None) >= a_week_before:
                            # return JSONResponse(date)
                            temp['unsuccessful'] = 1
                        else:
                            continue
                    if m.job_type_id.job_status == 1:
                        if temp['is_bidded']:
                            # if jobbidModel[0].is_confirmed != 1 and m.job_type_id.payment_pending == 1 and bid_info['on_date_time'].replace(tzinfo=None) >= a_week_before:
                            #     temp['unsuccessful'] = 1
                            # else:
                            #     temp['unsuccessful'] = 0
                            temp['quoted'] = 1
                            temp['bid_info'] = bid_info
                            temp['applied_job'] = 1
                elif job_status == 2 or job_status == 3 or job_status >= 4:
                    temp['has_review_given'] = False
                    temp['is_confirmed'] = 1
                    temp['quoted'] = 1
                    temp['bid_info'] = getBidinfo(
                        m.job_type_id.id, data['userid'])
                    if job_status >= 4:
                        temp['has_review_given'] = User_review.objects\
                            .filter(from_user=data['userid'], jobtypeid=m.job_type_id.id).exists()

                elif job_status == -1:
                    if m.job_type_id.job_status == 1:
                        if temp['is_bidded']:
                            temp['quoted'] = 1
                            temp['bid_info'] = getBidinfo(
                                m.job_type_id.id, data['userid'])
                        temp['all_jobs'] = 1
                        temp['bidded_users'] = getbiddedUsers(m.job_type_id.id)
                if m.before_after == 1:
                    temp['care_name'] = m.care_name
                elif m.before_after == 0:
                    temp['care_name'] = "Caregiver's home(Family Daycare)"
                if m.location == "0":
                    temp['care_name'] = "Job posted by a parent"
                    if m.before_after == 1:
                        temp['care_name'] = m.care_name
                    temp['address'] = m.address
                    temp['city'] = m.city
                    temp['post_code'] = m.post_code
                    temp['state'] = m.state
                temp['location'] = m.location
                temp['bidded_users'] = len(getbiddedUsers(m.job_type_id.id))
                temp['job_view_count'] = get_job_viewers(m.job_type_id.id)
                # single line if else
                temp[
                    'job_type'] = m.job_type_id.job_type
                caregivers = m.job_type_id.caregiver_type.split(
                    ',')
                caregivers = [x for x in caregivers if x != ' ' and x]
                caregivers = map(lambda x: x.title(), caregivers)
                temp['caregiver_type'] = ' / '.join(caregivers)
                temp['case_session'] = m.job_type_id.care_session.split(',')
                temp['care_type'] = m.job_type_id.care_type.split(',')
                temp['no_of_children'] = m.no_of_children
                temp['children_age'] = m.children_age
                temp['time_budget'] = gettime_budget(m.job_type_id.id)
                rate_per_hour = (Decimal(percent.x) / Decimal(100)) * \
                    Decimal(temp['time_budget']['rate_per_hour'])
                if temp['time_budget'] is not None:
                    # total_cost = Decimal(temp['time_budget'][
                    #                      'hours_required']) * Decimal(temp['time_budget']['rate_per_hour'])
                    carer_quote = get_carer_budget(temp['time_budget']['hours_required'],temp['time_budget']['rate_per_hour'])
                    # temp['time_budget']['rate_per_hour'] = Decimal(
                    #     temp['time_budget']['rate_per_hour']) - rate_per_hour
                    temp['time_budget']['budget'] = carer_quote
                try:
                    requirements = JobRequirements.objects.get(
                        job_type_id=m.job_type_id.id)
                    if requirements.requirements:
                        reqs = requirements.requirements.split(',')
                        # req_serializer = JobRequirementsSerializer(
                        #     requirements, many=False)
                        req_model = Requirements.objects.values(
                            'requirement', 'icon_class').filter(id__in=reqs)
                        temp['req'] = req_model
                    else:
                        temp['req'] = []
                    temp['other_requirement'] = requirements.other_requirement
                except JobRequirements.DoesNotExist:
                    temp['req'] = None
                if 'job_status' not in data:
                    data['job_status'] = m.job_type_id.job_status
                tooltipGet = getTooltip(data['job_status'],'worker')
                if 'tooltip' in tooltipGet:
                    temp['tooltip'] = tooltipGet['tooltip']
                    temp['label'] = tooltipGet['label']
                res.append(temp.copy())
            counts = get_jobtype_counts(data['userid'], 'worker',data['adminLogged'])
        else:
            res = []
            counts = get_jobtype_counts(data['userid'], 'worker',data['adminLogged'])

        counts['available_count'] = available_count

        result = {"Status": "200", "data": res,
                  "total_count": total_count, "pageno": data['pageno'], "count": len(res), "status_counts": counts, "start_limit": start_limit, "end_limit": end_limit}
        return JSONResponse(result)


# get Time and buget for a given job type id

def gettime_budget(jobid):
    if jobid:
        try:
            model = JobTime_Budget.objects.values(
                'date', 'time', 'days_required', 'rate_per_hour', 'hours_required', 'budget').get(job_type_id=jobid)
        except JobTime_Budget.DoesNotExist:
            return None
        model['days_required'] = model['days_required'].split(',')
        model['time'] = time.strftime(model['time'], '%H:%M')
        model['required_days'] = model['date'].weekday()
        print(model)
        return model


# submit a job bid
@csrf_exempt
def submitbid(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'userid' not in data or 'jobtypeid' not in data:
            result = {"Status": "500",
                      "Message": "Job id and userid cannot be empty"}
            return JSONResponse(result)
        try:
            model = JobType.objects.get(id=data['jobtypeid'])
        except JobType.DoesNotExist:
            result = {"Status": "500",
                      "Message": "Job id was not found"}
            return JSONResponse(result)
        serializer = JobbidSerialzier(data=data, many=False)
        if serializer.is_valid():
            serializer.save()
            model.job_status = 1
            model.save()
            if 'userid' in data:
                mail_status = send_email.quote_mail(data['userid'],model.id)
            result = {"Status": "200",
                      "Message": "Job bidded!!"}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "errors": serializer.errors}
            return JSONResponse(result)


# Get bid information
def getBidinfo(jobid, userid):
    if jobid and userid:
        try:
            model = Jobbid.objects.values(
                'rate_per_hour', 'quote', 'comment', 'on_date_time').get(jobtypeid=jobid, userid=userid)
        except Jobbid.DoesNotExist:
            return {}
        return model


# Get bidded users
def getbiddedUsers(jobtypeid):
    percent = get_taxes()
    model = Jobbid.objects.values(
        'rate_per_hour', 'quote', 'comment',
        'on_date_time', 'userid__id',
        'userid__first_name', 'userid__last_name',
        'userid__contactno', 'userid__email', 'is_confirmed',
        'id', 'userid__profpic')\
        .annotate(username=Concat('userid__first_name', Value(' '), 'userid__last_name'))\
        .filter(jobtypeid=jobtypeid)
    for m in model:
        m['funds_transfered'] = get_funds_transfered(m['quote'])
        m['client_quote'] = get_client_quote(m['quote'])
        tooltipGet = getTooltip(1,'parent','bidviewclient job')
        m['tooltip'] = tooltipGet['tooltip']
        m['label']  = tooltipGet['label']
        # if m['jobtypeid__payment_pending'] == 1:
        #     m['payment_pending_jobbid_id'] = m['id']
    return model


# get quoted / posted job posted by the parent
@csrf_exempt
def get_parent_jobs(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        # for pagination
        if 'pageno' not in data or 'limit' not in data:
            result = {"Status": '500',
                      "Message": "pagenumber & limit cannot be empty"}
            return JSONResponse(result)
        else:
            limit = data['limit']
            page_no = data['pageno']
            if page_no == 1:
                start_limit = 0
                end_limit = limit
                # return HttpResponse(limit)
            elif page_no > 1:
                start_limit = limit * (page_no - 1)
                end_limit = limit + start_limit
        confirm_check = Q()
        order = '-id'
        if data['type'] == 'quoted':
            query = Q(job_type_id__user_id=data[
                      'userid'], job_type_id__job_status=1,
                      job_type_id__is_active=1, job_type_id__is_delete=0,
                      job_type_id__is_mailed=1)
            confirm_check = ~Q(job_type_id__is_confirmed=1)
            order = 'job_type_id__updated'
        elif data['type'] == 'posted':
            query = Q(job_type_id__user_id=data[
                      'userid'], job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__is_mailed=1,
                      job_type_id__job_status=0)
        elif data['type'] == 'confirmed':
            query = Q(job_type_id__user_id=data['userid'],
                      job_type_id__is_confirmed=1,
                      job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__job_status=2)
        elif data['type'] == 'draft':
            query = Q(job_type_id__user_id=data['userid'],
                      job_type_id__is_active=0,
                      job_type_id__is_delete=0,
                      job_type_id__is_mailed=0)
        elif data['type'] == 'completed':
            query = Q(job_type_id__user_id=data['userid'],
                      job_type_id__is_confirmed=1,
                      job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__job_status=3)
        elif data['type'] == 'paid':
            query = Q(job_type_id__user_id=data['userid'],
                      job_type_id__is_confirmed=1,
                      job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__job_status=4)
        elif data['type'] == 'closed':
            query = Q(job_type_id__user_id=data['userid'],
                      job_type_id__is_confirmed=1,
                      job_type_id__is_active=1,
                      job_type_id__is_delete=0,
                      job_type_id__is_mailed=1,
                      job_type_id__job_status=7)
        if 'adminLogged' in data:
            if data['adminLogged'] == True:
                # blockQuery = Q()
                blockQuery = Q()
            else:
                blockQuery = Q(job_type_id__is_blocked=0)
        else:
            blockQuery = Q(job_type_id__is_blocked=0)
        model = JobAddress.objects.filter(blockQuery, query, confirm_check
                                          ).order_by(order)[start_limit:end_limit]
        # return HttpResponse(model.query)
        total_count = JobAddress.objects.filter(blockQuery, query).count()
        # completed_count = JobType.objects.filter(
        #     user_id=data['userid'], job_status=3, is_confirmed=1).count()
        res = []
        if model.count() > 0:
            for m in model:
                temp = {}
                temp['jobid'] = m.job_type_id.id
                temp['user_id'] = m.job_type_id.user_id.id
                temp['profile_suits'] = m.job_type_id.user_id.profile_suits
                temp['user_name'] = m.job_type_id.user_id.first_name
                if m.job_type_id.user_id.profpic:
                    temp['client_profilepic'] = m.job_type_id.user_id.profpic.url
                else:
                    temp['client_profilepic'] = ""
                temp['is_bidded'] = True
                temp['is_blocked'] = m.job_type_id.is_blocked
                temp['bidded_users'] = getbiddedUsers(m.job_type_id.id)
                temp['is_confirmed'] = m.job_type_id.is_confirmed
                temp['job_status'] = m.job_type_id.job_status
                temp['quote_count'] = len(temp['bidded_users'])
                if m.before_after == 1:
                    temp['care_name'] = m.care_name
                else:
                    temp['care_name'] = "Caregiver's home (Family Daycare)"
                if m.location == "0":
                    temp['care_name'] = "Job posted by a parent"
                    if m.before_after == 1:
                        temp['care_name'] = m.care_name
                    temp['address'] = m.address
                    temp['city'] = m.city
                    temp['post_code'] = m.post_code
                    temp['state'] = m.state
                temp['location'] = m.location
                temp['job_view_count'] = get_job_viewers(m.job_type_id.id)
    
                # single line if else
                temp[
                    'job_type'] = m.job_type_id.job_type
                caregivers = m.job_type_id.caregiver_type.split(
                    ',')
                caregivers = map(lambda x: x.title(), caregivers)
                temp['caregiver_type'] = ' / '.join(caregivers)
                temp['case_session'] = m.job_type_id.care_session.split(',')
                temp['care_type'] = m.job_type_id.care_type.split(',')
                temp['no_of_children'] = m.no_of_children
                temp['children_age'] = m.children_age
                temp['time_budget'] = gettime_budget(m.job_type_id.id)
                temp['payment_pending'] = m.job_type_id.payment_pending
                temp['has_review_given'] = User_review.objects\
                .filter(from_user=data['userid'], jobtypeid=m.job_type_id.id).exists()
                jobbid_confirm = Jobbid.objects.filter(jobtypeid=m.job_type_id.id,is_confirmed=1)
                if jobbid_confirm.exists():
                    temp['payment_pending_jobbid_id'] = jobbid_confirm[0].id
                # if m.confirm_userid is not None:
                #     if m.job_type_id.payment_pending == 1:
                #         temp['payment_pending_jobbid_id'] = m.confirm_userid


                # temp['has_worker_review'] = True if len(worker_review) > 0 else False
                try:
                    requirements = JobRequirements.objects.get(
                        job_type_id=m.job_type_id.id)
                    if requirements.requirements:
                        reqs = requirements.requirements.split(',')
                        # req_serializer = JobRequirementsSerializer(
                        #     requirements, many=False)
                        req_model = Requirements.objects.values(
                            'requirement', 'icon_class').filter(id__in=reqs)
                        temp['req'] = req_model
                    else:
                        temp['req'] = []
                    temp['other_requirement'] = requirements.other_requirement
                except JobRequirements.DoesNotExist:
                    temp['req'] = None
                # try:
                #     temp['confirmed_info'] = Jobbid.objects\
                #         .select_related('userid')\
                #         .values('rate_per_hour', 'quote', 'comment',
                #                 'on_date_time', 'userid__id',
                #                 'userid__first_name',
                #                 'userid__last_name',
                #                 'userid__contactno', 'userid__email', 'id',
                #                 'userid__about', 'userid__age',
                #                 'userid__relevant_year', 'userid__country_code__code', 'funds_transfered')\
                #         .filter(jobtypeid=m.job_type_id.id, is_confirmed=1)
                # except Jobbid.DoesNotExist:
                #     temp['confirmed_info'] = {}
                confirm_user = Jobbid.objects\
                        .select_related('userid')\
                        .values('rate_per_hour', 'quote', 'comment',
                                'on_date_time', 'userid__id',
                                'userid__first_name',
                                'userid__last_name',
                                'userid__contactno', 'userid__email', 'id',
                                'userid__about', 'userid__age',
                                'userid__relevant_year', 'userid__country_code__code', 'funds_transfered')\
                        .filter(jobtypeid=m.job_type_id.id, is_confirmed=1)
                if confirm_user.exists():
                    temp['confirmed_info'] = confirm_user[0]
                else:
                    temp['confirmed_info'] = {}

                if 'job_status' not in data:
                    data['job_status'] = m.job_type_id.job_status
                tooltipGet = getTooltip(m.job_type_id.job_status,'parent')
                if 'tooltip' in tooltipGet:
                    temp['tooltip'] = tooltipGet['tooltip']
                    temp['label'] = tooltipGet['label']
                res.append(temp.copy())
        counts = get_jobtype_counts(data['userid'], 'parent',data['adminLogged'])
        post_job_exist = JobType.objects.filter(user_id=data['userid'])[:1].exists()
        result = {"Status": "200", "data": res,
                  "total_count": total_count, "count": model.count(),"post_job_exist":post_job_exist,
                  "status_counts": counts}
        return JSONResponse(result)
        # return JSONResponse(result)
# To view a job


@csrf_exempt
def viewjob(request):
    # return HttpResponse('test')
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'userid' not in data:
            result = {"Status": "500",
                      "message": "Userid required to continue"}
            return JSONResponse(result)
        user_obj = Users.objects.get(id=data['userid'])
        percent = Percent_Config.objects.all().first()
        try:
            model = JobAddress.objects.select_related().get(
                job_type_id=data['jobid'])
        except JobAddress.DoesNotExist:
            result = {"Status": "404", "message": "Jobid not found!!"}
            return JSONResponse(result)
        bid_user_check = Jobbid.objects.filter(
                jobtypeid=data['jobid'],userid=data['userid']).exists()
        # print('worker',bid_user_check)
        if model.job_type_id.user_id.id != int(data['userid']) and (user_obj.purpose == "parent" or user_obj.purpose == '0'):
            result = {
                    "Status": "501",
                    "message": "Your not allow to view this job."}
            return JSONResponse(result)
        elif bid_user_check == False and (user_obj.purpose == "worker" or user_obj.purpose == '1') and model.job_type_id.job_status > 1:
            result = {
                    "Status": "501",
                    "message": "Your not allow to view this job."}
            return JSONResponse(result)
        if model.job_type_id.is_confirmed == 1:
            confirmed_user = Jobbid.objects.select_related().get(
                jobtypeid=data['jobid'], is_confirmed=1).userid.id
            if model.job_type_id.user_id.id != int(data['userid']) and confirmed_user != int(data['userid']):
                result = {
                    "Status": "500",
                    "message": "This Job was confirmed it cannot be viewed to everyone"}
                return JSONResponse(result)
        if 'adminLogged' in data:
            if model.job_type_id.is_blocked == 1 and data['adminLogged'] == False:
                result = {
                        "Status": "501",
                        "message": "This Job was blocked by admin."}
                return JSONResponse(result)
        try:
            job_status = Job_status_view.objects.get(userid=data['userid'],job_id=data['jobid'],job_status=model.job_type_id.job_status)
        except Job_status_view.DoesNotExist:
            job_status = Job_status_view()
            job_status.userid = user_obj
            job_status.job_id = model.job_type_id
            job_status.job_status = model.job_type_id.job_status
            job_status.save()
            pass
        except Job_status_view.MultipleObjectsReturned:
            pass
        temp = {}
        temp['jobid'] = model.job_type_id.id
        temp[
            'job_type'] = model.job_type_id.job_type
        temp['user_id'] = model.job_type_id.user_id.id
        temp['profile_suits'] = model.job_type_id.user_id.profile_suits
        temp['is_confirmed'] = model.job_type_id.is_confirmed
        temp['job_status'] = model.job_type_id.job_status
        temp['is_blocked'] = model.job_type_id.is_blocked
        temp['has_review_given'] = False
        temp['payment_pending'] = model.job_type_id.payment_pending
        temp['is_bidded'] = Jobbid.objects.filter(
                            jobtypeid=model.job_type_id.id, userid=data['userid']).exists()
        jobbid_confirm = Jobbid.objects.filter(jobtypeid=model.job_type_id.id,is_confirmed=1)
        if jobbid_confirm.exists():
            temp['payment_pending_jobbid_id'] = jobbid_confirm[0].id
        if model.job_type_id.job_status >= 4:
            reviews = User_review.objects\
                .filter(Q(from_user=data['userid'])| Q(to_user=data['userid']), jobtypeid=model.job_type_id.id).values()
            # client_review = User_review.objects\
            #     .filter(to_user=data['userid'], jobtypeid=model.job_type_id.id).values()
            print(reviews)
            temp['has_review_given'] = User_review.objects\
                .filter(from_user=data['userid'], jobtypeid=model.job_type_id.id).exists()
            # if len(review) > 0 and len(client_review):
            if reviews.exists():
                reviews = list(reviews)
                # print(review)
                # return JSONResponse(review[0])
                for review in reviews:
                    if review['from_user_id'] == int(data['userid']):
                        temp['from_review'] = review
                    else:
                        temp['to_review'] = review
                    # temp['client_review'] = list(client_review)[0]
        temp['user_name'] = model.job_type_id.user_id.first_name + \
            " " + model.job_type_id.user_id.last_name
        temp['user_email'] = model.job_type_id.user_id.email
        if model.job_type_id.user_id.profpic:
            temp['user_profilepic'] = model.job_type_id.user_id.profpic.url
        else:
            temp['user_profilepic'] = ""
        temp['user_contactno'] = model.job_type_id.user_id.country_code.code + \
            "" + model.job_type_id.user_id.contactno
        if model.before_after == 1:
            temp['care_name'] = model.care_name
        else:
            temp['care_name'] = "Caregiver's home (Family Daycare)"
        if model.location == "0":
            temp['care_name'] = "Job posted by a parent"
            temp['address'] = model.address
            temp['city'] = model.city
            temp['post_code'] = model.post_code
            temp['state'] = model.state
        temp['location'] = model.location
        caregivers = model.job_type_id.caregiver_type.split(
            ',')
        caregivers = map(lambda x: x.title(), caregivers)
        temp['caregiver_type'] = ' / '.join(caregivers)
        temp['case_session'] = model.job_type_id.care_session.split(',')
        temp['care_type'] = model.job_type_id.care_type.split(',')
        temp['no_of_children'] = model.no_of_children
        temp['children_age'] = model.children_age
        temp['bidded_users'] = getbiddedUsers(model.job_type_id.id)
        temp['quote_count'] = len(temp['bidded_users'])
        temp['time_budget'] = gettime_budget(model.job_type_id.id)
        if user_obj.purpose == '1' or user_obj.purpose == 'worker':
            if 'job_status' not in data:
                data['job_status'] = model.job_type_id.job_status
            tooltipGet = getTooltip(data['job_status'],'worker')
            if 'tooltip' in tooltipGet:
                temp['tooltip'] = tooltipGet['tooltip']
                temp['label'] = tooltipGet['label']
            rate_per_hour = (Decimal(percent.x) / Decimal(100)) * \
                    Decimal(temp['time_budget']['rate_per_hour'])
            if temp['time_budget'] is not None:
                    # total_cost = Decimal(temp['time_budget'][
                    #                      'hours_required']) * Decimal(temp['time_budget']['rate_per_hour'])
                    carer_quote = get_carer_budget(temp['time_budget']['hours_required'],temp['time_budget']['rate_per_hour'])
                    # temp['time_budget']['rate_per_hour'] = Decimal(
                    #     temp['time_budget']['rate_per_hour']) - rate_per_hour
                    temp['time_budget']['budget'] = carer_quote
            # if temp['time_budget'] is not None:
            #     total_cost = Decimal(temp['time_budget'][
            #                          'hours_required']) * Decimal(temp['time_budget']['rate_per_hour'])
            #     temp['time_budget']['rate_per_hour'] = Decimal(
            #         temp['time_budget']['rate_per_hour']) - rate_per_hour
            #     temp['time_budget']['budget'] = total_cost
        else:
            if 'job_status' not in data:
                data['job_status'] = model.job_type_id.job_status
            tooltipGet = getTooltip(data['job_status'],'parent')
            if 'tooltip' in tooltipGet:
                temp['tooltip'] = tooltipGet['tooltip']
                temp['label'] = tooltipGet['label']
        try:
            requirements = JobRequirements.objects.get(
                job_type_id=model.job_type_id.id)
            if requirements.requirements:
                reqs = requirements.requirements.split(',')
                # req_serializer = JobRequirementsSerializer(
                #     requirements, many=False)
                req_model = Requirements.objects.values(
                    'requirement', 'icon_class').filter(id__in=reqs)
                temp['req'] = req_model
            else:
                temp['req'] = []
            temp['other_requirement'] = requirements.other_requirement
        except JobRequirements.DoesNotExist:
            temp['req'] = None
        if temp['is_confirmed'] == 1:
            try:
                temp['confirmed_info'] = Jobbid.objects\
                    .select_related('userid')\
                    .values('rate_per_hour', 'quote', 'comment',
                            'on_date_time', 'userid__id',
                            'userid__first_name',
                            'userid__last_name',
                            'userid__contactno', 'userid__email', 'id',
                            'userid__about', 'userid__age',
                            'userid__relevant_year', 'userid__profpic',
                            'userid__country_code__code',
                            'funds_transfered')\
                    .get(jobtypeid=model.job_type_id.id, is_confirmed=1)
            except Jobbid.DoesNotExist:
                temp['confirmed_info'] = {}
        else:
            temp['confirmed_info'] = {}

        result = {"Status": "200", "jobdetail": temp}
        return JSONResponse(result)

# View job to edit


@csrf_exempt
def previewjob(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            model1 = JobType.objects.get(id=data['jobid'])
            jobtype = {}
            jobtype['jobId'] = model1.id
            jobtype['job_type'] = model1.job_type
            jobtype['selectedCaregiver'] = model1.caregiver_type.split(
                ',')
            jobtype['careSession_selection'] = model1.care_session.split(',')
            jobtype['schoolcaretype'] = model1.schoolcaretype
            jobtype['careType_seletion'] = model1.care_type.split(',')
        except JobType.DoesNotExist:
            jobtype = {}
            jobtype['selectedCaregiver'] = []
            jobtype['careSession_selection'] = []
            jobtype['careType_seletion'] = []
        try:
            model2 = JobAddress.objects.get(job_type_id=data['jobid'])
            jobloc = {}
            jobloc['age_selection'] = model2.children_age.split(',')
            jobloc['before_after'] = str(model2.before_after)
            jobloc['location'] = model2.location
            jobloc['address'] = model2.address
            # jobloc['loc_id'] = model2.city + "," + \
            #     model2.state + "," + model2.post_code
            jobloc['city'] = model2.city
            jobloc['state'] = model2.state
            jobloc['post_code'] = model2.post_code
            if model2.city is not None and model2.state is not None and model2.post_code is not None:
                jobloc['loc_id'] = model2.city + ", " + \
                    model2.state + " " + str(model2.post_code)
            jobloc['care_name'] = model2.care_name
            jobloc['no_of_children'] = str(model2.no_of_children)
        except JobAddress.DoesNotExist:
            jobloc = {}
            jobloc['age_selection'] = []

        try:
            model3 = JobTime_Budget.objects.get(job_type_id=data['jobid'])
            jobtime = {}
            jobtime['time'] = model3.time
            jobtime['hour'] = model3.time.hour
            jobtime['minute'] = model3.time.minute
            jobtime['days_selection'] = model3.days_required.split(',')
            jobtime['date'] = model3.date.strftime("%m-%d-%Y")
            jobtime['hours_required'] = model3.hours_required
            jobtime['rate_per_hour'] = model3.rate_per_hour
            jobtime['budget'] = model3.budget
            jobtime['cash_on_hand'] = model3.cash_on_hand
        except JobTime_Budget.DoesNotExist:
            jobtime = {}
            jobtime['hour'] = 0
            jobtime['minute'] = 0
            jobtime['date'] = datetime.now().date().strftime("%m-%d-%Y")

        try:
            model4 = JobRequirements.objects.get(job_type_id=data['jobid'])
            jobreq = {}
            jobreq['opt_selection'] = []
            if model4.requirements:
                req_arr = model4.requirements.split(',')
                jobreq['opt_selection'] = list(map(int, req_arr))
            jobreq['other_requirement'] = model4.other_requirement
        except JobRequirements.DoesNotExist:
            jobreq = {}
            jobreq['opt_selection'] = []
        if 'jobId' in jobtype:
            jobId = jobtype['jobId']
        else:
            jobId = ""
        result = {"Status": "200", "step1": jobtype,
                  "step2": jobloc, "step3": jobtime, "step4": jobreq,'jobId':jobId}
        return JSONResponse(result)


# To confirm a job quote


@csrf_exempt
def confirm_job(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            model = Jobbid.objects.get(
                id=data['id'])
        except Jobbid.DoesNotExist:
            result = {"Status": "404",
                      "message": "Job bid id does not exists!!"}
            return JSONResponse(result)
        model.is_confirmed = 1
        model.funds_transfered = data['funds_transfered']
        payment_quote = 0
        pay_type = 0
        if 'pay_quote' in data:
            payment_quote = data['pay_quote']
        if 'payment_type' in data:
            pay_type = int(data['payment_type'])
        model.save()
        JobType.objects.filter(id=model.jobtypeid.id).update(
            is_confirmed=1, job_status=2,payment_pending=0,pay_quote=payment_quote,payment_type=pay_type)
        # model.jobtypeid__is_confirmed = 1
        send_email.confirm_email(model.userid.email, model.jobtypeid.id)
        quoted_users = Jobbid.objects.select_related()\
            .values_list('userid__email', flat=True)\
            .filter(~Q(userid=model.userid.id),
                    jobtypeid=model.jobtypeid.id)
        if quoted_users.count() > 0:
            send_email.sorry_email(quoted_users, model.jobtypeid.id)
        result = {"Status": "200",
                  "message": "job confirmed"}
        return JSONResponse(result)


# To get the type of requirements for the job
def get_requirements(jobId,jobtime=None):
    if jobId:
        location_filter = Q(for_agency_location=0)
        caretype_filter = Q(for_regular=0)
        if jobtime is None:
            model = JobAddress.objects.get(job_type_id=jobId)
            if model.location == '1':
                location_filter = Q()
            if model.job_type_id.job_type == '1':
                caretype_filter = Q()
        reqs = Requirements.objects.values('id', 'requirement', 'icon_class',
                                           'is_default', 'category',
                                           'has_proof').filter(
            location_filter, caretype_filter)
        return reqs


@csrf_exempt
def get_all_requirements(request):
    if request.method == 'POST':
        reqs = Requirements.\
            objects.values('id', 'requirement',
                           'icon_class', 'is_default',
                           'category', 'has_proof').all()
        tooltipGet = getTooltip(0,'parent')
        result = {"Status": "200",
                  "reqs": reqs,'tooltip':tooltipGet['tooltip']}
        return JSONResponse(result)


# To complete a job by worker
@csrf_exempt
def complete_job(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            model = JobType.objects.get(
                id=data['jobtypeid'], is_confirmed=1)
        except JobType.DoesNotExist:
            result = {"Status": "404",
                      "message": "Job id does not exists!!"}
            return JSONResponse(result)
        model.job_status = 3
        model.save()
        mail_status = send_email.release_payment_mail(model.id)
        result = {"Status": "200",
                  "message": "job was completed"}
        return JSONResponse(result)

# To release payment to the worker by a client


@csrf_exempt
def release_payment(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            model = JobType.objects.get(Q(job_status=3)| Q(job_status=4),
                id=data['jobtypeid'], is_confirmed=1)
        except JobType.DoesNotExist:
            result = {"Status": "404",
                      "message": "Job id does not Matches the requirement"}
            return JSONResponse(result)
        
        if 'review' in data:
            reviewSerializer = Reviews(data=data['review'], many=False)
            if reviewSerializer.is_valid():
                reviewSerializer.save()
                result = {"Status": "200",
                          "message": "Released payment for the job",
                          "review": "saved"}
                return JSONResponse(result)
            else:
                result = {"Status": "500", "message": "Review not saved",
                          "errors": reviewSerializer.errors}
                return JSONResponse(result)
        else:
            if model.job_status == 4:
                result = {"Status": "404",
                          "message": "This job already paid."}
                return JSONResponse(result)
            model.job_status = 4
            model.save()
            mail_status = send_email.rate_for_client_mail(model.id)
            result = {"Status": "200",
                      "message": "Released payment for the job",
                      "review": "saved","job_status":model.job_status}
            return JSONResponse(result)
# Save user review


@csrf_exempt
def save_review(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        reviewSerializer = Reviews(data=data['review'], many=False)
        if reviewSerializer.is_valid():
            reviewSerializer.save()
            result = {"Status": "200",
                      "Message": "Review saved!!"}
            return JSONResponse(result)
        else:
            result = {"Status": "500", "message": "Review not saved",
                      "errors": reviewSerializer.errors}
            return JSONResponse(result)

# To get the feedback given by the client


@csrf_exempt
def get_feedback(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            # if data['usertype'] == 'client':
            model = User_review.objects.get(
                from_user=data['from_user'], jobtypeid=data['jobid'])
            # else:
            #     model = User_review.objects.get(
            #         to_user=data['from_user'], jobtypeid=data['jobid'])
        except User_review.DoesNotExist:
            result = {"Status": "404",
                      "message": "Job id does not Matches the requirement"}
            return JSONResponse(result)
        serializer = Reviews(model, many=False)
        result = {"Status": "200", "feedback": serializer.data}
        return JSONResponse(result)

# This will be called when Worker viewing a job


@csrf_exempt
def make_a_job_viewed(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'job_type_id' in data and 'user_id' in data:
            try:
                user = Users.objects.get(id=data['user_id'])
            except Users.DoesNotExist:
                result = {"Status": "500",
                          "message": "User does not exists"}
                return JSONResponse(result)
            if user.purpose == '1' or user.purpose == 'worker':
                tooltipGet = getTooltip(0,'worker','bid job')
                try:
                    User_job_views.objects.get(
                        job_type_id=data['job_type_id'], user_id=data['user_id'])
                except User_job_views.DoesNotExist:
                    serializer = JobviewSerializer(data=data, many=False)
                    if serializer.is_valid():
                        serializer.save()
                        result = {"Status": "200",
                                  "message": "Job view added",'tooltip':tooltipGet['tooltip'],'label':tooltipGet['label']}
                        return JSONResponse(result)
                    else:
                        result = {"Status": "500",
                                  "errors": serializer.errors}
                        return JSONResponse(result)
                result = {"Status": "200",
                          "message": "Job already viewed",'tooltip':tooltipGet['tooltip']}
                return JSONResponse(result)
            else:
                result = {"Status": "200",
                          "message": "Only worker can make a view"}
                return JSONResponse(result)
        else:
            result = {"Status": "500",
                      "message": "Job type id and userid is mandatory to continue"}
            return JSONResponse(result)


def get_job_viewers(jobid):
    return User_job_views.objects.filter(job_type_id=jobid).count()


def get_jobtype_counts(userid, usertype,adminLogged=False):
    # job_statusIds = Job_status_view.objects.values_list('job_id',flat=True).filter(userid=userid)
    # if usertype == '0' or usertype == 'parent':
    #     default = Q(user_id=userid) & Q(is_active=1, is_delete=0, is_mailed=1)
    #     counts = JobType.objects.values('user_id').annotate(
    #         completed_count=Sum(Case(
    #             When(Q(job_status=3) & ~Q(id__in=job_statusIds), then=Value(1)),
    #             default=Value(0),
    #             output_field=IntegerField())
    #         ),
    #         quoted_count=Sum(Case(
    #             When(Q(job_status=1) & ~Q(id__in=job_statusIds), then=Value(1)),
    #             default=Value(0),
    #             output_field=IntegerField())
    #         ),
    #         jobId=F('id')
    #     ).filter(default).aggregate(Sum('completed_count'),
    #                                 Sum('quoted_count'))
    # elif usertype == '1' or usertype == 'worker':
    #     # print('intha condition kulla varutha')
    #     default = Q(userid=userid)\
    #         & Q(jobtypeid__is_active=1, jobtypeid__is_delete=0, jobtypeid__is_mailed=1,jobtypeid__is_blocked=0)
    #     counts = Jobbid.objects.values('userid').annotate(
    #         # applied_count=Sum(Case(
    #         #     When(jobtypeid__job_status=1, then=Value(1)),
    #         #     default=Value(0),
    #         #     output_field=IntegerField())
    #         # ),
    #         confirmed_count=Sum(Case(
    #             When(Q(jobtypeid__job_status=2, is_confirmed=1) & ~Q(jobtypeid__in=job_statusIds), then=Value(1)),
    #             default=Value(0),
    #             output_field=IntegerField())
    #         ),
    #         # paid_count=Sum(Case(
    #         #     When(Q(jobtypeid__job_status=4, is_confirmed=1), then=Value(1)),
    #         #     default=Value(0),
    #         #     output_field=IntegerField())
    #         # ),
    #     ).filter(default).aggregate(Sum('confirmed_count'))

    #     default = ~Q(job_type_id__user_id=None) & Q(job_type_id__job_status=0) & Q(job_type_id__is_active=1) & Q(
    #         job_type_id__is_delete=0)

    # return counts
    try:
        user = Users.objects.get(id=userid)
    except:
        return 0

    if usertype == '0' or usertype == 'parent':
        if adminLogged == True:
            blockQuery = Q()
        else:
            blockQuery = Q(is_blocked=0)
        default = Q(user_id=userid) & Q(is_active=1, is_delete=0, is_mailed=1,user_id__partnerid=user.partnerid.id)
        counts = JobType.objects.values('user_id').annotate(
            completed_count=Sum(Case(
                When(Q(job_status=3), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            quoted_count=Sum(Case(
                When(Q(job_status=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            jobId=F('id')
        ).filter(blockQuery,default).aggregate(Sum('completed_count'),
                                    Sum('quoted_count'))
    elif usertype == '1' or usertype == 'worker':
        # print('intha condition kulla varutha')
        if adminLogged == True:
            blockQuery = Q()
        else:
            blockQuery = Q(jobtypeid__is_blocked=0)
        default = Q(userid=userid)\
            & Q(jobtypeid__is_active=1, jobtypeid__is_delete=0, jobtypeid__is_mailed=1,jobtypeid__is_blocked=0,jobtypeid__user_id__partnerid=user.partnerid.id)
        counts = Jobbid.objects.values('userid').annotate(
            # applied_count=Sum(Case(
            #     When(jobtypeid__job_status=1, then=Value(1)),
            #     default=Value(0),
            #     output_field=IntegerField())
            # ),
            confirmed_count=Sum(Case(
                When(Q(jobtypeid__job_status=2, is_confirmed=1), then=Value(1)),
                default=Value(0),
                output_field=IntegerField())
            ),
            # paid_count=Sum(Case(
            #     When(Q(jobtypeid__job_status=4, is_confirmed=1), then=Value(1)),
            #     default=Value(0),
            #     output_field=IntegerField())
            # ),
        ).filter(blockQuery,default).aggregate(Sum('confirmed_count'))

        # default = ~Q(job_type_id__user_id=None) & Q(job_type_id__job_status=0) & Q(job_type_id__is_active=1) & Q(
        #     job_type_id__is_delete=0) & Q(job_type_id__is_blocked=0)

    return counts



# To get the drafted jobs
# @csrf_exempt
# def get_draft_jobs(request):
#     if request.method == "POST":
#         model =
def get_taxes():
    return Percent_Config.objects.all().first()


@csrf_exempt
def get_drafted_jobs(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        if 'userid' in data:
            query = Q(user_id=data['userid'],
                      is_active=0,
                      is_delete=0,
                      is_mailed=0)
            if 'adminLogged' in data:
                if data['adminLogged'] == True:
                    blockQuery = Q()
                else:
                    blockQuery = Q(is_blocked=0)
            else:
                blockQuery = Q(is_blocked=0)
            model = JobType.objects.filter(blockQuery,query)\
                .annotate(care_name=F('jobaddress__care_name'),
                          before_after=F('jobaddress__before_after'),
                          location=F('jobaddress__location'),
                          address=F('jobaddress__address'),
                          city=F('jobaddress__city'),
                          post_code=F('jobaddress__post_code'),
                          state=F('jobaddress__state'),
                          no_of_children=F('jobaddress__no_of_children'),
                          children_age=F('jobaddress__children_age'))\
                .order_by('-updated')
            res = []
            for m in model:
                temp = {}
                temp['drafted'] = True
                temp['jobid'] = m.id
                temp['user_id'] = m.user_id.id
                temp['user_name'] = m.user_id.first_name
                if m.user_id.profpic:
                    temp['client_profilepic'] = m.user_id.profpic.url
                else:
                    temp['client_profilepic'] = ""
                # temp['is_bidded'] = True
                # temp['bidded_users'] = getbiddedUsers(m.id)
                temp['is_confirmed'] = m.is_confirmed
                temp['job_status'] = m.job_status
                # temp['quote_count'] = len(temp['bidded_users'])

                if m.before_after == 1:
                    temp['care_name'] = m.care_name
                else:
                    temp['care_name'] = "Caregiver's home (Family Daycare)"
                if m.location == "0":
                    temp['care_name'] = "Job posted by a parent"
                    if m.before_after == 1:
                        temp['care_name'] = m.care_name
                    temp['address'] = m.address
                    temp['city'] = m.city
                    temp['post_code'] = m.post_code
                    temp['state'] = m.state
                temp['location'] = m.location
                # temp['job_view_count'] = get_job_viewers(m.id)
                # single line if else
                temp[
                    'job_type'] = m.job_type
                caregivers = m.caregiver_type.split(
                    ',')
                caregivers = map(lambda x: x.title(), caregivers)
                temp['caregiver_type'] = ' / '.join(caregivers)
                temp['case_session'] = m.care_session.split(',')
                temp['care_type'] = m.care_type.split(',')
                temp['no_of_children'] = m.no_of_children
                temp['children_age'] = m.children_age
                temp['time_budget'] = gettime_budget(m.id)
                tooltipGet = getTooltip(0,'parent')
                temp['tooltip'] = tooltipGet['tooltip']
                temp['label'] = tooltipGet['label'] 
                try:
                    requirements = JobRequirements.objects.get(
                        job_type_id=m.id)
                    if requirements.requirements:
                        reqs = requirements.requirements.split(',')
                        # req_serializer = JobRequirementsSerializer(
                        #     requirements, many=False)
                        req_model = Requirements.objects.values(
                            'requirement', 'icon_class').filter(id__in=reqs)
                        temp['req'] = req_model
                    else:
                        temp['req'] = []
                    temp['other_requirement'] = requirements.other_requirement
                except JobRequirements.DoesNotExist:
                    temp['req'] = None
                try:
                    temp['confirmed_info'] = Jobbid.objects\
                        .select_related('userid')\
                        .values('rate_per_hour', 'quote', 'comment',
                                'on_date_time', 'userid__id',
                                'userid__first_name',
                                'userid__last_name',
                                'userid__contactno', 'userid__email', 'id',
                                'userid__about', 'userid__age',
                                'userid__relevant_year', 'userid__country_code__code', 'funds_transfered')\
                        .get(jobtypeid=m.id, is_confirmed=1)
                except Jobbid.DoesNotExist:
                    temp['confirmed_info'] = {}
                res.append(temp.copy())
            counts = get_jobtype_counts(data['userid'], 'parent',data['adminLogged'])
            result = {"Status": "200", "data": res,
                      "total_count": len(res), "status_count": counts}
            return JSONResponse(result)
        else:
            result = {"Status": "403",
                      "message": "Attribute userid is required"}
            return JSONResponse(result)


def get_funds_transfered(quote):
    if quote > 0:
        percent = Percent_Config.objects.all().first()
        total_cost = Decimal(quote)
        cus_service = total_cost + \
            (Decimal(percent.x) / Decimal(100)) * total_cost
        online_pay1 = cus_service + \
            (Decimal(percent.y) / Decimal(100)) * cus_service
        gst = online_pay1 + (Decimal(percent.z) / Decimal(100)) * online_pay1
        estimated = round(gst, 2)
        return estimated
    else:
        return 0


def get_carer_budget(hours_required,rate_per_hour):
    total_cost = Decimal(hours_required) * Decimal(rate_per_hour)
    percent = Percent_Config.objects.all().first()
    service_charge = percent.x
    deducted_cost = total_cost * (Decimal(service_charge)/Decimal(100))
    budget = total_cost - deducted_cost
    return round(budget,2)


def get_client_quote(quote):
    percent = Percent_Config.objects.all().first()
    service_charge = percent.x
    service_cost = Decimal(quote)/Decimal(Decimal(1) - (Decimal(service_charge)/100))
    online_payment_cost = Decimal(service_cost) * (Decimal(1) + (Decimal(percent.y)/100))
    gst_cost = Decimal(online_payment_cost) * (Decimal(1) + (Decimal(percent.z)/100))
    return round(gst_cost,2)

def getTooltip(jobStatus,userType,jobstate=None):
    status = ''
    if jobStatus == 0:
        status = 'available' if userType == 'worker' else 'post'
    elif jobStatus == 1:
        status = 'applied' if userType == 'worker' else 'quote' 
    elif jobStatus == 2:
        status = "confirm"
    elif jobStatus == 3:
        status = 'complete'
    elif jobStatus == 4:
        status = 'paid'
    elif jobStatus == 7:
        status = 'close'
    jobstateQuery = Q()
    if jobstate is not None:
        jobstateQuery = Q(job_state_name=jobstate)
    else:
        jobstateQuery = Q(job_state_name=status+' job')

    try:
        tooltipGet = Job_tooltip.objects.values('tooltip','label').get(jobstateQuery,job_status=jobStatus,usertype=userType)
    except Job_tooltip.DoesNotExist:
        temp = {}
        temp['tooltip'] = ''
        temp['label'] = ''
        return temp
    return tooltipGet

@csrf_exempt
#get worker quote details
def getUserQuoteDetails(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'workerid' in data and 'jobId' in data:
            try:
                JobbidDetails = Jobbid.objects.annotate(noofHour=F('jobtypeid__jobtime_budget__hours_required')).values().get(userid=data['workerid'],jobtypeid=data['jobId'])
            except Jobbid.DoesNotExist:
                result = {"Status": "404",
                      "message": "Jobbid model not found!!"}
                return JSONResponse(result)
            result = {"Status": "200", "data": JobbidDetails}
            return JSONResponse(result)
        else:
            result = {"Status": "500","message":"workerid and jobId required!!"}
            return JSONResponse(result)

#check for job status change
@csrf_exempt
def jobStatusCheck(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'userid' in data and 'jobId' in data and 'job_status' in data:
            JobtypeDetails = JobType.objects.values_list('job_status',flat=True).filter(~Q(job_status=data['job_status']),id=data['jobId'])
            if JobtypeDetails.exists():
                result = {"Status": "200", "JobstatusChange": True,"job_status":list(JobtypeDetails)[0]}
                return JSONResponse(result)
            else:
                result = {"Status": "200", "JobstatusChange": False}
                return JSONResponse(result)
        else:
            result = {"Status": "500","message":"userid and jobId required!!"}
            return JSONResponse(result)

#check for job status in payment_pending change
@csrf_exempt
def payment_pending(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        if 'userid' in data and 'jobId' in data and 'jobbid_userid':
            JobtypeUpdate = JobType.objects.filter(id=data['jobId']).update(payment_pending=1)
            JobbidUpdate = Jobbid.objects.filter(userid=data['jobbid_userid']).update(is_confirmed=1)
            # if JobtypeDetails.exists():
            result = {"Status": "200", "message":"Payment Pending add successfully."}
            return JSONResponse(result)
        else:
            result = {"Status": "500","message":"userid and jobId required!!"}
            return JSONResponse(result)

