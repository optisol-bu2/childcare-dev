# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.utils.timezone import timezone
from django.db import models

# Create your models here.


class Country_codes(models.Model):
    country_name = models.CharField(max_length=200)
    code = models.CharField(max_length=20)
    flag_class = models.CharField(max_length=100)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ('id',)


class Partners(models.Model):
    partner_name = models.CharField(max_length=200)
    partner_email = models.CharField(max_length=200)
    partner_mobileno = models.BigIntegerField()
    partner_address = models.TextField()
    partner_about_us = models.TextField()
    partner_logo = models.CharField(blank=True,null=True,max_length=200)
    partner_url_privacy_policy = models.TextField(max_length=200)
    partner_url_terms_and_conditions = models.TextField()
    partner_subdomain = models.TextField()
    is_active = models.IntegerField(default=1)
    is_delete = models.IntegerField(default=0)
    partnership_with = models.IntegerField(default=0)
    partnership_childcare_logo = models.IntegerField(default=0)
    show_partner_portals = models.IntegerField(default=0)
    def __str__(self):
        return self.partner_subdomain

    class Meta(object):
        ordering = ('id',)


# user model


class Users(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, default='null')
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=200)
    purpose = models.CharField(max_length=100, default='')
    contactno = models.CharField(max_length=100, default='')
    country_code = models.ForeignKey(
        Country_codes, blank=True, null=True)
    profile_suits = models.CharField(max_length=500, default='')
    profpic = models.ImageField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    post_code = models.CharField(max_length=200, blank=True, null=True)
    login_token = models.CharField(max_length=100, blank=True, null=True)
    gender = models.CharField(max_length=100, blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    year_of_birth = models.CharField(max_length=100, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)
    relevant_year = models.CharField(max_length=50, blank=True, null=True)
    is_active = models.IntegerField(default=0)
    is_delete = models.IntegerField(default=0)
    is_forget = models.IntegerField(default=0)
    is_admin = models.IntegerField(default=0)
    is_vetted = models.IntegerField(default=0)
    is_blocked = models.IntegerField(default=0)
    activation_email = models.IntegerField(default=0)
    login_count = models.IntegerField(default=0)
    updated = models.DateTimeField(auto_now=True)
    signup_url = models.TextField(blank=True,null=True)
    completeness = models.IntegerField(default=0,blank=True,null=True)
    forget_token = models.CharField(max_length=200,blank=True,null=True)
    signup_date = models.DateField(auto_now=True)
    partnerid = models.ForeignKey(Partners,default=1)
    order_by = models.IntegerField(blank=True,null=True)
   


    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ('id',)

# user preferences


class User_preference(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    job_type = models.CharField(max_length=100, blank=True, null=True)
    caregivers = models.TextField(blank=True, null=True)
    children_age = models.TextField(blank=True, null=True)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users, on_delete=models.CASCADE,blank=True,null=True,related_name="user_preference_updated_by_user")
    is_delete = models.IntegerField(default=0)
    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


# job details


class JobType(models.Model):
    user_id = models.ForeignKey(Users, blank=True, null=True, on_delete=models.CASCADE)
    job_type = models.CharField(max_length=100, blank=True, null=True)
    caregiver_type = models.CharField(max_length=100, blank=True, null=True)
    care_type = models.CharField(
        max_length=200, default="", blank=True, null=True)
    care_session = models.CharField(max_length=100, blank=True, null=True)
    on_date_time = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    job_status = models.IntegerField(default=0)
    schoolcaretype = models.IntegerField(default=0)
    is_confirmed = models.IntegerField(default=0)
    is_active = models.IntegerField(default=0)
    is_delete = models.IntegerField(default=0)
    is_mailed = models.IntegerField(default=0)
    is_blocked = models.IntegerField(default=0)
    payment_pending = models.IntegerField(default=0)
    payment_type = models.IntegerField(default=1)
    pay_quote = models.FloatField(blank=True,null=True)

    def __str__(self):
        return self.job_type

    class Meta:
        ordering = ('id',)

# job address


class JobAddress(models.Model):
    job_type_id = models.OneToOneField(
        JobType, on_delete=models.CASCADE)
    location = models.CharField(
        max_length=50, default=0, blank=True, null=True)
    before_after = models.IntegerField(default=0)
    care_name = models.CharField(
        default="", max_length=200, null=True, blank=True)
    address = models.TextField(blank=True,null=True)
    city = models.CharField(max_length=100, default="", blank=True, null=True)
    post_code = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=100, default="", blank=True, null=True)
    no_of_children = models.CharField(max_length=100, blank=True, null=True)
    children_age = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.job_type_id.job_type

    class Meta:
        ordering = ('id',)


class JobTime_Budget(models.Model):
    job_type_id = models.OneToOneField(
        JobType, on_delete=models.CASCADE)
    date = models.DateField(blank=True)
    time = models.TimeField(blank=True, null=True)
    days_required = models.CharField(max_length=100, default="", blank=True)
    rate_per_hour = models.FloatField(blank=True)
    hours_required = models.IntegerField(blank=True)
    budget = models.FloatField()
    total_cost = models.FloatField(default=0.0)
    cash_on_hand = models.IntegerField(default=0)

    def __str__(self):
        return self.job_type_id.job_type

    class Meta:
        ordering = ('id',)

# Requirements table


class Requirements(models.Model):
    requirement = models.CharField(max_length=200)
    icon_class = models.CharField(max_length=100)
    for_regular = models.IntegerField(default=0)
    for_agency_location = models.IntegerField(default=0)
    is_default = models.IntegerField(default=0)
    category = models.CharField(max_length=200, blank=True, null=True)
    has_proof = models.IntegerField(default=0)
    sorting_order = models.IntegerField(blank=True,null=True)
    has_info = models.IntegerField(default=0)
    info = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('sorting_order',)

# job requirements


class JobRequirements(models.Model):
    job_type_id = models.OneToOneField(
        JobType, on_delete=models.CASCADE)
    requirements = models.CharField(max_length=500, default="", blank=True)
    other_requirement = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.job_type_id.job_type

    class Meta:
        ordering = ('id',)


class Price_Config(models.Model):
    designation = models.CharField(max_length=100, default="")
    degree = models.CharField(
        max_length=100, default="", blank=True, null=True)
    rate_per_hour = models.IntegerField()

    def __str__(self):
        return self.designation

    class Meta:
        ordering = ('id',)


class Percent_Config(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    z = models.IntegerField()
    platform_cust_service = models.IntegerField(default=0)
    online_payment = models.IntegerField(default=0)
    gst = models.IntegerField(default=0)

    def __str__(self):
        return self.x

    class Meta:
        ordering = ('id',)


class Postal_code(models.Model):
    postcode = models.IntegerField()
    suburb = models.CharField(max_length=100)
    state = models.CharField(max_length=4)
    latitude = models.FloatField()
    longitude = models.FloatField()

    def __str__(self):
        return self.suburb

    class Meta:
        ordering = ('id',)


class Jobbid(models.Model):
    jobtypeid = models.ForeignKey(
        JobType, on_delete=models.CASCADE)
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    rate_per_hour = models.FloatField()
    quote = models.FloatField()
    funds_transfered = models.FloatField(blank=True,null=True)
    comment = models.TextField(blank=True, null=True)
    on_date_time = models.DateTimeField(auto_now_add=True)
    is_confirmed = models.IntegerField(default=0)
    confirmed_date_time = models.DateTimeField(blank=True,null=True)
    completed_date_time = models.DateTimeField(blank=True,null=True)
    paid_date_time = models.DateTimeField(blank=True,null=True)

    def __str__(self):
        return self.rate_per_hour

    class Meta:
        ordering = ('id',)
        unique_together = ('jobtypeid', 'userid',)


class User_settings(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    job_email = models.IntegerField(default=1)
    quote_email = models.IntegerField(default=1)
    job_confirmed = models.IntegerField(default=1)
    job_sorry = models.IntegerField(default=1)
    completed_email = models.IntegerField(default=1)
    job_paid = models.IntegerField(default=1)

    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


class Users_messages(models.Model):
    from_id = models.ForeignKey(Users, related_name='fromid')
    to_id = models.ForeignKey(
        Users, related_name='toid', blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    message_type = models.CharField(max_length=100, default='public')
    is_read = models.IntegerField(default=0)
    on_date_time = models.DateTimeField(auto_now=True)
    from_delete = models.IntegerField(default=0)
    to_delete = models.IntegerField(default=0)
    is_delete = models.IntegerField(default=0)
    jobId = models.ForeignKey(JobType, blank=True, null=True)

    def __str__(self):
        return self.message

    class Meta:
        ordering = ('id',)


class Message_reply(models.Model):
    message_id = models.ForeignKey(Users_messages, on_delete=models.CASCADE)
    from_id = models.ForeignKey(Users)
    reply = models.CharField(max_length=200)
    on_date_time = models.DateTimeField(auto_now=True)
    is_delete = models.IntegerField(default=0)

    def __str__(self):
        return self.reply

    class Meta:
        ordering = ('id',)


class Message_subreply(models.Model):
    reply_id = models.ForeignKey(Message_reply, on_delete=models.CASCADE)
    from_id = models.ForeignKey(Users)
    subreply = models.CharField(max_length=200)
    on_date_time = models.DateTimeField(auto_now=True)
    is_delete = models.IntegerField(default=0)

    def __str__(self):
        return self.subreply

    class Meta:
        ordering = ('id',)


class User_availability(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    before_school = models.CharField(max_length=200, blank=True, null=True)
    day_care = models.CharField(max_length=200, blank=True, null=True)
    after_school = models.CharField(max_length=200, blank=True, null=True)
    overnight = models.CharField(max_length=200, blank=True, null=True)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users, on_delete=models.CASCADE,blank=True,null=True,related_name="User_availability_updated_by")
    is_delete = models.IntegerField(default=0)
    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


class User_employee_document(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    requirement_id = models.ForeignKey(Requirements)
    document_name = models.CharField(max_length=400, blank=True, null=True)
    # 1-> not verified 2-> verified 3 -> Need info
    status = models.IntegerField(default=1)
    is_delete = models.IntegerField(default=0)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users, on_delete=models.CASCADE,blank=True,null=True,related_name="User_employee_document_updated_by")
    
    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


class User_employee_history(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    title = models.CharField(max_length=500, blank=True, null=True)
    company_name = models.CharField(max_length=200)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    post_code = models.CharField(max_length=200, blank=True, null=True)
    from_year = models.CharField(max_length=50, blank=True, null=True)
    from_month = models.CharField(max_length=50, blank=True, null=True)
    to_year = models.CharField(max_length=50, blank=True, null=True)
    to_month = models.CharField(max_length=50, blank=True, null=True)
    headline = models.CharField(max_length=300, blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    proof = models.CharField(max_length=200, blank=True, null=True)
    is_delete = models.IntegerField(default=0)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users, on_delete=models.CASCADE,blank=True,null=True,related_name="User_employee_history_updated_by")

    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


class User_other_detail(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    interest = models.TextField(blank=True, null=True)
    household_chores = models.TextField(blank=True, null=True)
    languages = models.TextField(blank=True, null=True)
    resume = models.CharField(max_length=200, blank=True, null=True)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users,on_delete=models.CASCADE,related_name="User_other_detail_updated_by",blank=True,null=True)
    is_delete = models.IntegerField(default=0)
    def __str__(self):
        return self.userid.first_name

    class Meta:
        ordering = ('id',)


class Household_chores(models.Model):
    chore = models.CharField(max_length=200)
    is_delete = models.IntegerField(default=0)

    def __str__(self):
        return self.chore

    class Meta:
        ordering = ('id',)


class Languages(models.Model):
    language = models.CharField(max_length=200)
    is_delete = models.IntegerField(default=0)

    def __str__(self):
        return self.language

    class Meta:
        ordering = ('id',)


class User_reference(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    document = models.TextField(blank=True, null=True)
    contactno = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    is_delete = models.IntegerField(default=0)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users,on_delete=models.CASCADE,related_name="User_reference_updated_by",blank=True,null=True)


    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ('id',)


class User_emergency_contact(models.Model):
    userid = models.ForeignKey(Users, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300, blank=True, null=True)
    email = models.CharField(max_length=200, blank=True, null=True)
    contactno = models.CharField(max_length=50)
    is_delete = models.IntegerField(default=0)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users,on_delete=models.CASCADE,related_name="User_emergency_contact_updated_by",blank=True,null=True)


    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ('id',)


class User_review(models.Model):
    from_user = models.ForeignKey(
        Users, on_delete=models.CASCADE, related_name="from_user")
    to_user = models.ForeignKey(
        Users, on_delete=models.CASCADE, related_name="for_user")
    jobtypeid = models.ForeignKey(JobType)
    star = models.FloatField(default=0.0)
    review = models.TextField(blank=True, null=True)
    on_date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return {"from_user": self.from_user.first_name,
                "to_user": self.to_user.first_name}

    class Meta:
        ordering = ('id',)



class User_job_views(models.Model):
    job_type_id = models.ForeignKey(JobType, on_delete=models.CASCADE)
    user_id = models.ForeignKey(Users)

    def __str__(self):
        return self.user_id.first_name

    class Meta:
        ordering = ('id',)
        unique_together = ('job_type_id','user_id',)


class User_login_status(models.Model):
    userid = models.ForeignKey(Users,on_delete=models.CASCADE)
    on_date_time = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20)

    def __str__(self):
        return self.user_id.first_name

    class Meta:
        ordering = ('id',)
        # unique_together = ('job_type_id','user_id',)


class User_bank_details(models.Model):
    userid = models.ForeignKey(Users,on_delete=models.CASCADE)
    on_date_time = models.DateTimeField(auto_now=True)
    bsb_no = models.BigIntegerField()
    account_no = models.BigIntegerField()
    bank_name = models.CharField(max_length=100)

    def __str__(self):
        return self.user_id.first_name

    class Meta:
        ordering = ('id',)

class admin_user(models.Model):
    email = models.EmailField()
    password = models.CharField(max_length=200)
    is_active = models.IntegerField(default=0)
    is_delete = models.IntegerField(default=0)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ('id',)

class admin_messages(models.Model):
    from_user = models.ForeignKey(Users,on_delete=models.CASCADE, related_name="client")
    worker_id = models.ForeignKey(Users,on_delete=models.CASCADE, related_name="worker")
    message = models.TextField()
    on_date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ('id',)


class Payment_status(models.Model):
    client_id = models.ForeignKey(Users)
    job_id = models.ForeignKey(JobType)
    payer_id = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    description = models.TextField(blank=True,null=True)
    on_date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ('id',)


class User_referal(models.Model):
    referer = models.ForeignKey(Users)
    user_full_name = models.CharField(max_length=200)
    friend_full_name = models.CharField(max_length=200)
    user_country_code = models.ForeignKey(Country_codes, related_name="user_countrycode")
    friend_country_code = models.ForeignKey(Country_codes, related_name="friend_countrycode")
    user_contact_no = models.CharField(max_length=50)
    friend_contact_no = models.CharField(max_length=50)
    user_email = models.EmailField()
    friend_email = models.EmailField()
    profile_suits = models.CharField(max_length=500, default='')
    referal_url = models.TextField(blank=True, null=True)
    # 0-> Referal not succeed 1-> Referal succeed
    status = models.IntegerField(default=0)

    def __str__(self):
        return self.friend_email

    class Meta:
        ordering = ('id',)


class User_reward_status(models.Model):
    userid = models.ForeignKey(Users, related_name="reward_getter")
    # 0 -> ad 1 -> referal
    reward_type = models.IntegerField()
    referal_id = models.ForeignKey(User_referal, blank=True, null=True)
    ad_url = models.TextField(blank=True, null=True)
    amount = models.FloatField()
    # 0 -> not completed 1 -> completed 2 -> payment given
    status = models.IntegerField()

    def __str__(self):
        return self.userid.email

    class Meta:
        ordering = ('id',)

class Update_profile_check(models.Model):
    userid = models.ForeignKey(Users,related_name="worker_userid")
    action = models.CharField(max_length=200,blank=True,null=True)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users,related_name="update_user_type")

    def __str__(self):
        return self.userid.email

    class Meta:
        ordering = ('id',)

class profile_check_history(models.Model):
    userid = models.ForeignKey(Users,related_name="delete_profile_userid")
    action = models.CharField(max_length=200,blank=True,null=True)
    type_name = models.CharField(max_length=200,blank=True,null=True)
    type_id = models.IntegerField(blank=True,null=True)
    updated_by = models.IntegerField(blank=True,null=True)
    # updated_by = models.ForeignKey(Users,related_name="delete_profile_user_type")
    on_date_time = models.DateTimeField(auto_now_add=True,blank=True)

    def __str__(self):
        return self.userid.email

    class Meta:
        ordering = ('id',)

class Job_status_view(models.Model):
    userid = models.ForeignKey(Users)
    job_id = models.ForeignKey(JobType)
    job_status = models.IntegerField(blank=True)

    def __str__(self):
        return self.userid.email

    class Meta:
        ordering = ('id',)

class Job_status_log(models.Model):
    client_id = models.ForeignKey(Users,related_name="job_status_log_client_id")
    job_id = models.ForeignKey(JobType,related_name="job_status_log_id")
    job_status = models.IntegerField(blank=True)
    user_id = models.ForeignKey(Users,related_name="job_status_log_user_id")
    on_date_time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.on_date_time

    class Meta:
        ordering = ('id',)

#Job tooltip
# author S.Udhayakumar

class Job_tooltip(models.Model):
    job_status = models.IntegerField(blank=True)
    job_state_name = models.CharField(max_length=200)
    tooltip = models.TextField(blank=True, null=True)
    label = models.TextField(blank=True, null=True)
    usertype = models.CharField(max_length=200)
    percent_config = models.IntegerField(default=0)
    
    def __str__(self):
        return self.tooltip

    class Meta:
        ordering = ('id',)

class Bulk_mail_send(models.Model):
    recipient_list = models.TextField(blank=True,null=True)
    action_link = models.CharField(max_length=200,blank=True,null=True)
    context = models.TextField(blank=True,null=True)
    username = models.CharField(max_length=200,blank=True,null=True)
    template_name = models.CharField(max_length=200,blank=True,null=True)
    is_mailed = models.IntegerField(default=0)
    on_date_time = models.DateTimeField(blank=True,null=True)
    job_type_id = models.IntegerField(blank=True,null=True)
    def __str__(self):
        return self.username

    class Meta:
        ordering = ('id',)


class client_offer_form(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    country_code = models.ForeignKey(Country_codes)
    mobile_no = models.CharField(max_length=100)
    fifty_doller_voucher = models.IntegerField()
    hundred_doller_voucher = models.IntegerField()

    def __str__(self):
        return self.first_name

    class Meta(object):
        ordering = ('id',)




