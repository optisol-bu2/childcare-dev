angular.module('menowApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('scripts/module/admin/views/admin_home.html',
    "<div class=\"content_section\"> <div id=\"content_sec\" ng-if=\"selectedUser\"> <!-- <div ng-if=\"userMode == 'client' && selectedUser\" ng-include=\"templateUrl\" ng-controller=\"JobCtrl\">\n" +
    "        </div> --> <div ng-if=\"userMode == 'worker'\" ng-include=\"templateUrl\" ng-controller=\"JobcopyCtrl\"> </div> <div ng-if=\"userMode == 'client'\" ng-include=\"templateUrl\"> </div> </div> </div> <!-- <div class=\"col-md-3\">\n" +
    "     <div class=\"\">\n" +
    "                    <div class=\"ad_section\">\n" +
    "                        <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "    </div> -->"
  );


  $templateCache.put('scripts/module/admin/views/admin_login.html',
    "<div> <div class=\"bgvideoimage\"> <!-- <img src=\"styles/images/banner1.png\" class=\"cover\" /> --> <!-- <img src=\"styles/images/ba2.png\" class=\"cover\" /> --> <!-- <img src=\"styles/images/3.png\" class=\"cover\" /> --> <video-background class=\"vid_bgr\" source=\"{ mp4: 'scripts/module/home/views/kinder.mp4' }\" autoplay loop show-time=\"false\"></video-background> <div class=\"overlay_dot\"></div> </div> <!-- right side text --> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-6 col-md-offset-6 col-sm-12 col-xs-12\"> <div class=\"cca-hero-content\"> <h1>Your Leading Resource to Find Quality Child Care Staff</h1> <p>Lovely Childcare is the only online service devoted entirely to helping child care centres connect with qualified staff.</p> <a class=\"btn cca-btn-fill hvr-wobble-horizontal\" ng-click=\"vm.callpostjob()\">Post a Job</a> <p class=\"cca-secondary-cta\"> Are you a job seeker? Click <a href=\"javascript:void(0);\" ng-click=\"vm.callloginPopup()\">here.</a> </p> </div> </div> </div> <!-- <div class=\"row\">\n" +
    "         <div class=\"homepage_search clearfix\">\n" +
    "           <div class=\"col-md-5\">\n" +
    "\n" +
    "           </div>\n" +
    "           <div class=\"col-md-7\">\n" +
    "             <div class=\"cca-hero-content\">\n" +
    "             <h1>Your Leading Resource to Find Quality Child Care Staff</h1>\n" +
    "             <p>Lovely Childcare is the only online service devoted entirely to helping child care centres connect with qualified staff.</p>\n" +
    "             <div class=\"search_form_home\">\n" +
    "\n" +
    "                <div class=\"ctn-btn-section m-t-30\">\n" +
    "                <a class=\"btn cca-btn-fill hvr-wobble-vertical btn_gray_home\" ng-click=\"vm.callpostjob()\">Post a Job</a>\n" +
    "                <a class=\"btn cca-btn-fill hvr-wobble-vertical \" ng-click=\"vm.callpostjob()\">Search Job</a>\n" +
    "                </div>\n" +
    "\n" +
    "             </div>\n" +
    "           </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "   </div> --> </div> <!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"></div> --> <!-- <footer></footer> --> <style>.navigation_section{\n" +
    "   box-shadow: none;\n" +
    "   }\n" +
    "   .navigation_section, .navigation_section .navbar-default{\n" +
    "   background: rgba(255, 255, 255, 0.6);\n" +
    "   /*background: #fff;*/\n" +
    "   }\n" +
    "   .pre-header{\n" +
    "   background: rgba(0, 0, 0, 0.2);\n" +
    "   /*background: transparent;*/\n" +
    "   }\n" +
    "   .pre-header li {\n" +
    "   color:#fff;\n" +
    "   border-right:1px solid #fff;\n" +
    "   }</style></div>"
  );


  $templateCache.put('scripts/module/admin/views/admin_template.html',
    "<header id=\"scroll_top\"> <div class=\"pre-header hidden-xs\"> <div class=\"container-fluid\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 additional-nav\"> <ul class=\"list-unstyled list-inline pull-right\"> <li><i class=\"fa fa-envelope\"></i><span>lovelychildcare@gmail.com</span></li> <li><i class=\"fa fa-phone\"></i><span>+1 659 6597 365</span></li> </ul> </div> </div> </div> </div> <!-- ================================================================================================ --> <!-- ========================================Navigation bar============================================--> <!-- ================================================================================================ --> <div class=\"navigation_section\" set-class-when-at-top=\"fixed-top-header\" ng-controller=\"HomeCtrl\"> <div class=\"navbar navbar-default\"> <div class=\"navbar-header\"> <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\"> <span class=\"icon-bar\"> </span> <span class=\"icon-bar\"> </span> <span class=\"icon-bar\"> </span> </button> <a class=\"navbar-brand\" href=\"#/login\"> <img src=\"styles/images/logo.png\" alt=\"logo\" class=\"img-responsive\"> </a> </div> <div class=\"navbar-collapse collapse admin_form_login\"> <div ng-if=\"currentState == 'admin.login' && !adminLogged\"> <form name=\"adminLoginform\" class=\"navbar-form navbar-right\" ng-submit=\"adminLogin(adminLoginform)\" novalidate> <div class=\"input-group m-r-15\"> <span class=\"input-group-addon\"><i class=\"icon-user\"></i></span> <input type=\"email\" class=\"form-control\" name=\"email1\" placeholder=\"Email Address\" ng-pattern=\"/^(([^<>()[\\]\\\\.,;:\\s@\\&quot;]+(\\.[^<>()[\\]\\\\.,;:\\s@\\&quot;]+)*)|(\\&quot;.+\\&quot;))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/\" ng-model=\"adminUser.email\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(adminLoginform.$submitted || adminLoginform.email1.$dirty) && adminLoginform.email1.$invalid\"> <p ng-if=\"adminLoginform.email1.$error.required\">Please enter email</p> <p ng-if=\"adminLoginform.email1.$error.pattern\">Invalid email</p> </div> </div> <div class=\"input-group\"> <span class=\"input-group-addon\"><i class=\"icon-lock\"></i></span> <input id=\"password\" type=\"password\" class=\"form-control\" name=\"password\" ng-model=\"adminUser.password\" placeholder=\"Password\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(adminLoginform.$submitted || adminLoginform.password.$dirty) && adminLoginform.password.$invalid\"> <p ng-if=\"adminLoginform.password.$error.required\">Please enter password</p> </div> </div> <div class=\"input-group m-r-15\"> <a href=\"javascript:void(0);\" class=\"forgot_password_admin\"><i class=\"icon-question\" title=\"forgot password\"></i></a> </div> <button type=\"submit\" class=\"btn btn-primary navbar_submit_btn hvr-wobble-horizontal\">Login</button> </form> </div> <div ng-if=\"currentState != 'admin.login' || adminLogged\"> <form class=\"navbar-form navbar-left form-inline clearfix\"> <div class=\"form-group\"> <label class=\"select_mode_label\">Select Mode</label> <span class=\"toggle_button_switch navbar_toggle_switch\"> <div class=\"toggleWrapper\"> <input type=\"checkbox\" ng-model=\"userMode\" ng-true-value=\"'client'\" ng-false-value=\"'worker'\" id=\"dn\" class=\"dn\" ng-click=\"getUserlist(userMode,true);\"> <label for=\"dn\" class=\"toggle\"><span class=\"toggle__handler\"></span></label> </div> </span> </div> <div class=\"form-group\"> <select2 class=\"form-control\" ng-model=\"selectedUser\" options=\"queryOptions\"> <!-- <option ng-repeat=\"user in userList\" value=\"user.id\">#{{user.id}} {{user.first_name}} {{user.last_name}}</option> --> </select2> <div class=\"errormsg\" ng-if=\"useridReq && !selectedUser\"> <p>Please select an user to view</p> </div> </div> <button type=\"submit\" id=\"submitUser\" class=\"btn btn-primary navbar_submit_btn res_float_left\" ng-click=\"submitUser(selectedUser)\">Submit</button> <a href=\"#/viewworker/{{selectedUser}}\" ng-if=\"userMode == 'worker'\" class=\"btn btn-primary navbar_submit_btn res_float_left\">View Profile</a> <a href=\"#/parentprofile\" ng-if=\"userMode == 'client'\" class=\"btn btn-primary navbar_submit_btn res_float_left\">View Profile</a> </form> <!-- <form id=\"signin\" class=\"navbar-form navbar-left admin_form_login\" role=\"form\">\n" +
    "                    <div class=\"input-group m-r-15 res_float_left\">\n" +
    "                        <toggle-switch ng-model=\"switchStatus\" knob-label=\"Change\" on-label=\"Worker\" off-label=\"Client\">\n" +
    "                        </toggle-switch>\n" +
    "                    </div>\n" +
    "                    <div class=\"input-group m-r-15 res_float_left\">\n" +
    "                        <select chosen class=\"form-control\">\n" +
    "                            <option>Enter ID / Name</option>\n" +
    "                            <option>#14590 kalidass</option>\n" +
    "                            <option>#14590 kasthuriraja</option>\n" +
    "                            <option>#14590 suresh</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                    <button type=\"submit\" class=\"btn btn-primary navbar_submit_btn hvr-wobble-horizontal res_float_left\">Submit</button>\n" +
    "                </form> --> <ul class=\"nav navbar-nav navbar-right\"> <li class=\"dropdown\"> <a href=\"javascript:void(0);\" class=\"dropdown-toggle user_name_header\" data-toggle=\"dropdown\"> <span class=\"thumb-sm avatar pull-left\" ng-if=\"loginuser_profilepic\"> <img src=\"images/profilepic/{{loginuser_profilepic}}\"> </span> <span class=\"thumb-sm avatar pull-left\" ng-if=\"!loginuser_profilepic\"> <img ngf-thumbnail=\"'/images/no_image_found.png'\"> </span> Admin <b class=\"caret\"></b> </a> <ul class=\"dropdown-menu customdropdown\"> <span class=\"arrow top\"></span> <!-- <li><a href=\"javascript:void(0);\"><i class=\"icon-user p-r-5\"></i>My Profile</a></li> --> <li><a href=\"#/settings\"><i class=\"icon-lock p-r-5\"></i>Change Password</a></li> <li><a href=\"javascript:void(0);\" ng-click=\"adminLogout()\"><i class=\"icon-power p-r-5\"></i>Logout</a></li> </ul> </li> </ul> </div> </div> </div> </div> <!--=================================== End Navigation Bar ==========================================================--> </header> <div ui-view> </div> <div ng-if=\"currentState != 'admin.login'\" footer></div>"
  );


  $templateCache.put('scripts/module/auth/templates/forgetpassword.html',
    "<div> <form class=\"login_form\" name=\"forget_form\" novalidate ng-submit=\"vm.forgetemail(forget_form)\"> <!-- <div class=\"logo_section\">\n" +
    "                     <img src=\"styles/images/logo.png\" alt=\"\">\n" +
    "                     </div> --> <!-- <div class=\"modal-header\">\n" +
    "                     <button type=\"button\" class=\"close close_icon\" data-dismiss=\"modal\">&times;\n" +
    "                     </button>\n" +
    "                     <h4 class=\"modal-title\">Login\n" +
    "                     </h4>\n" +
    "                     <p ng-if=\"vm.error\" class=\"text-danger\">Please enter correct credentials\n" +
    "                     </p>\n" +
    "                  </div> --> <div class=\"modal-body\"> <!-- <p ng-if=\"vm.error\" class=\"text-danger\">Please enter correct credentials\n" +
    "                     </p> --> <h3 class=\"title_modal\">Forgot password</h3> <!-- <p ng-if=\"vm.error\" class=\"errormsg\">Please enter correct credentials\n" +
    "                        </p> --> <!-- <h5 class=\"sub-header-popup\">Enter your details</h5> --> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"email\" ng-required=\"true\" name=\"email\" ng-model-options=\"{'updateOn':'blur'}\" ng-model=\"vm.emailid\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label ng-class=\"{'label_email_valid':(forget_form.$submitted || forget_form.email.$dirty) && forget_form.email.$invalid && !forget_form.email.$error.required}\">Email</label> <div class=\"errormsg\" ng-if=\"(forget_form.$submitted || forget_form.email.$dirty) &&forget_form.email.$invalid\"> <p ng-show=\"forget_form.email.$error.required\">Email is required!!</p> <p ng-show=\"forget_form.email.$error.email\">Email is not valid!!</p> </div> </div> </div> <input type=\"submit\" class=\"hvr-wobble-horizontal login_button\" value=\"Continue\"> <!-- <a href=\"javascript:void(0);\" class=\"hvr-wobble-horizontal login_button\" ng-click=\"vm.login()\" ng-enter=\"vm.login()\">Continue</a> --> <!-- <button class=\"btn btn-default\" ng-enter=\"vm.login()\">login</button> --> <!-- <input type=\"submit\" class=\"btn btn-primary login_button\"  ng-click=\"vm.login()\" value=\"Continue\"> --> </div> <div class=\"modal-footer\"> <p class=\"no_account_link\">Back to <span class=\"signup_link\" ng-click=\"vm.callloginPopup()\">Login </span> </p> </div> </form> </div> <style>.bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/auth/templates/login.html',
    "<div> <form class=\"login_form\" name=\"login_form\" novalidate ng-submit=\"vm.login()\"> <!-- <div class=\"logo_section\">\n" +
    "                     <img src=\"styles/images/logo.png\" alt=\"\">\n" +
    "                     </div> --> <!-- <div class=\"modal-header\">\n" +
    "                     <button type=\"button\" class=\"close close_icon\" data-dismiss=\"modal\">&times;\n" +
    "                     </button>\n" +
    "                     <h4 class=\"modal-title\">Login\n" +
    "                     </h4>\n" +
    "                     <p ng-if=\"vm.error\" class=\"text-danger\">Please enter correct credentials\n" +
    "                     </p>\n" +
    "                  </div> --> <div class=\"modal-body\"> <!-- <p ng-if=\"vm.error\" class=\"text-danger\">Please enter correct credentials\n" +
    "                     </p> --> <h3 class=\"title_modal\">Login</h3> <p ng-if=\"vm.error\" class=\"errormsg\">Please enter correct credentials </p> <!-- <h5 class=\"sub-header-popup\">Enter your details</h5> --> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"email\" ng-model-options=\"{ 'updateOn': 'blur'}\" ng-required=\"true\" name=\"email\" ng-model=\"vm.user.email\" autocomplete=\"off\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label ng-class=\"{'label_email_valid':(login_form.$submitted || login_form.email.$dirty) && login_form.email.$invalid && !login_form.email.$error.required}\">Email</label> <div class=\"errormsg\" ng-if=\"(login_form.$submitted || login_form.email.$dirty) && login_form.email.$invalid\"> <p ng-show=\"login_form.email.$error.required\">Email is required!!</p> <p ng-show=\"login_form.email.$error.email\">Email is not valid!!</p> </div> </div> </div> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"password\" ng-model-options=\"{ 'updateOn': 'blur'}\" ng-model=\"vm.user.password\" name=\"password\" ng-required=\"true\" autocomplete=\"off\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Password</label> <div class=\"errormsg\" ng-if=\"(login_form.$submitted || login_form.password.$dirty) &&login_form.password.$invalid\"> <p ng-show=\"login_form.password.$error.required\">Password is required!!</p> </div> </div> </div> <p class=\"text-right forgot_password_link\"> <a href=\"javascript:void(0)\" ng-click=\"vm.callforgetPopup()\">Forgot password? </a> </p> <input type=\"submit\" class=\"hvr-wobble-horizontal login_button\" value=\"Continue\"> <!-- <a href=\"javascript:void(0);\" class=\"hvr-wobble-horizontal login_button\" ng-click=\"vm.login()\" ng-enter=\"vm.login()\">Continue</a> --> <!-- <button class=\"btn btn-default\" ng-enter=\"vm.login()\">login</button> --> <!-- <input type=\"submit\" class=\"btn btn-primary login_button\"  ng-click=\"vm.login()\" value=\"Continue\"> --> </div> <div class=\"modal-footer\"> <p ng-controller=\"SignupCtrl as sm\" class=\"no_account_link\">Don't have an account? <span class=\"signup_link\" ng-click=\"sm.openSignup()\">Signup </span> </p> <h2 class=\"line_section hide\"> <span class=\"line-center\">OR </span> </h2> <div class=\"social_signup_section text-center m-t-10 hide\"> <p>Sign In using your social media accounts </p> <ul class=\"clearfix\"> <li class=\"social-facebook cursor-pointer\"> <em class=\"fa fa-facebook\"> </em> </li> <li class=\"social-gplus cursor-pointer\"> <em class=\"fa fa-google-plus\"> </em> </li> <li class=\"social-linkedin cursor-pointer\"> <em class=\"fa fa-linkedin\"> </em> </li> </ul> </div> </div> </form> </div> <style>.bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/auth/templates/not_activated_msg.html',
    "<div class=\"modal-body\"> <h2 class=\"almost_there text-center\">Not activated</h2> <p class=\"final_step_postjob text-justify\">The current account is not activated yet. Please verify your email to activate account</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/auth/templates/successforget.html',
    "<div class=\"modal-body\"> <h2 class=\"almost_there text-center\">Success!!</h2> <p class=\"final_step_postjob text-center\">An email with new password link has been sent to your email id.</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/auth/views/login.html',
    "<div class=\"login-form\"> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0\"> <div class=\"\"> <img src=\"styles/images/logo.png\" class=\"img-responsive\"> </div> <div ng-show=\"currentForm == 'login'\"> <h2 class=\"cmn-heading\">welcome</h2> <a href=\"{{constant.apiUrl}}/auth/facebook\"> <button type=\"button\" class=\"btn btn-fb btn-block\"><img src=\"/styles/images/fb-icon.png\">CONNECT WITH FACEBOOK</button> </a> <span class=\"txt-or\"><b>or</b></span> <div class=\"validation-message\" ng-show=\"error != null\"> <label class=\"error\">{{vm.error}}</label> </div> <form ng-submit=\"login(LoginForm)\" name=\"LoginForm\" method=\"post\" novalidate> <div class=\"form-login\"> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input type=\"email\" placeholder=\"Email\" ng-model=\"user.email\" required name=\"email\" class=\"form-control text-box\" ng-pattern=\"/^(([^<>()[\\]\\\\.,;:\\s@\\&quot;]+(\\.[^<>()[\\]\\\\.,;:\\s@\\&quot;]+)*)|(\\&quot;.+\\&quot;))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/\"> </div> </div> <label for=\"email_rq\" ng-show=\"submit && LoginForm.email.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"email_pattern\" ng-show=\"submit && LoginForm.email.$error.pattern\" class=\"error\">{{formValidation.emailInvalid}}</label> <label for=\"email_api\" ng-show=\"userError.email\" class=\"error\">{{userError.email}}</label> <div class=\"form-group bor-no\"> <div class=\"col-xs-12 p-0\"> <input name=\"password\" placeholder=\"Password\" ng-model=\"user.password\" type=\"password\" autocomplete=\"off\" required class=\"form-control text-box\"> </div> </div> <label for=\"password\" ng-show=\"submit && LoginForm.password.$error.required\" class=\"error\">{{formValidation.required}}</label> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <button type=\"submit\" ng-click=\"submit = true\" class=\"btn btn-black btn-block\">sign in</button> </div> <div class=\"col-xs-6\"> <button type=\"button\" class=\"btn btn-gray btn-block\" ng-click=\"switchForm('register')\">register</button> </div> </div> <div class=\"link-txt\"> <ul> <li><a href=\"\" ng-click=\"switchForm('forgot')\">Forgot your password?</a></li> <li><a href=\"\">Skip for now</a></li> </ul> </div> </form> </div> <div ng-show=\"currentForm == 'register'\"> <h2 class=\"cmn-heading\">join me:now</h2> <form ng-submit=\"register(regForm)\" name=\"regForm\" method=\"post\" novalidate> <div class=\"form-login\"> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input type=\"email\" placeholder=\"Email\" ng-model=\"newUser.email\" required name=\"email\" class=\"form-control text-box\" ng-pattern=\"/^(([^<>()[\\]\\\\.,;:\\s@\\&quot;]+(\\.[^<>()[\\]\\\\.,;:\\s@\\&quot;]+)*)|(\\&quot;.+\\&quot;))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/\"> </div> </div> <label for=\"email_rq\" ng-show=\"newUserSubmit && regForm.email.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"email_pattern\" ng-show=\"newUserSubmit && regForm.email.$error.pattern\" class=\"error\">{{formValidation.emailInvalid}}</label> <label for=\"email_api\" ng-show=\"userError.email\" class=\"error\">{{userError.email}}</label> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input name=\"password\" placeholder=\"Password\" ng-model=\"newUser.password\" type=\"password\" ng-maxlength=\"50\" ng-pattern=\"/^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^&*-_]{8,}$/\" required class=\"form-control text-box\"> </div> </div> <label for=\"password\" ng-show=\"newUserSubmit && regForm.password.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"password_pattern\" ng-show=\"newUserSubmit && regForm.password.$error.pattern\" class=\"error\">{{formValidation.passwordValidation}}</label> <label for=\"password_min\" ng-show=\"newUserSubmit && regForm.password.$touched && regForm.password.$error.minlength\" class=\"error\">{{formValidation.passwordMin}}</label> <label for=\"password_max\" ng-show=\"newUserSubmit && regForm.password.$touched && regForm.password.$error.maxlength\" class=\"error\">{{formValidation.passwordMax}}</label> <div class=\"form-group bor-no\"> <div class=\"col-xs-12 p-0\"> <input name=\"repeat_password\" placeholder=\"Confirm Password\" ng-model=\"newUser.repeat_password\" type=\"password\" required compare-to=\"newUser.password\" class=\"form-control text-box\"> </div> </div> <label for=\"compare_password\" ng-show=\"newUserSubmit && regForm.repeat_password.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"compare_password\" ng-show=\"newUserSubmit && !regForm.repeat_password.$error.required && regForm.repeat_password.$error.compareTo && regForm.password.$valid\" class=\"error\">{{formValidation.confirmPassword}}</label> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <button type=\"submit\" ng-click=\"newUserSubmit = true\" class=\"btn btn-black\">register</button> </div> <div class=\"col-xs-6\"> <button type=\"button\" class=\"btn btn-gray btn-block\" ng-click=\"cancelForm('register')\">cancel</button> </div> </div> <div class=\"link-txt\"> <ul> <li><a href=\"\" ng-click=\"switchForm('login')\">Already signed up?</a></li> </ul> </div> </form> </div> <div ng-show=\"currentForm == 'forgot'\"> <h2 class=\"cmn-heading\">forgot password</h2> <div class=\"validation-message\" ng-show=\"forgotError != null\"> <label class=\"error\">{{forgotError}}</label> </div> <form ng-submit=\"forgotPassword(forgotForm)\" name=\"forgotForm\" method=\"post\" novalidate> <div class=\"validation-message\" ng-show=\"errorRes != null\"> <label class=\"error\">{{errorRes}}</label> </div> <div class=\"form-login\"> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input type=\"email\" placeholder=\"Email\" ng-model=\"forgot.email\" required name=\"email\" class=\"form-control text-box\" ng-pattern=\"/^(([^<>()[\\]\\\\.,;:\\s@\\&quot;]+(\\.[^<>()[\\]\\\\.,;:\\s@\\&quot;]+)*)|(\\&quot;.+\\&quot;))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/\"> </div> </div> <label for=\"email_req\" ng-show=\"forgotSubmit && forgotForm.email.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"email_pattern\" ng-show=\"forgotSubmit && forgotForm.email.$error.pattern\" class=\"error\">{{formValidation.emailInvalid}}</label> <label for=\"email_api\" ng-show=\"userError.email\" class=\"error\">{{userError.email}}</label> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <button type=\"submit\" ng-click=\"forgotSubmit = true\" class=\"btn btn-black btn-block\">submit</button> </div> <div class=\"col-xs-6\"> <button type=\"button\" class=\"btn btn-gray btn-block\" ng-click=\"switchForm('login')\">cancel</button> </div> </div> </form> </div> </div> </div> <!-- <div footer></div> -->"
  );


  $templateCache.put('scripts/module/auth/views/resetPassword.html',
    "<div class=\"login-form\"> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0\"> <div class=\"\"> <img src=\"styles/images/logo.png\" class=\"img-responsive\"> </div> <h2 class=\"cmn-heading\">reset password</h2> <form ng-submit=\"resetPassword(resetPassForm)\" name=\"resetPassForm\" method=\"post\" novalidate> <div class=\"form-login\"> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input name=\"password\" placeholder=\"New Password\" ng-model=\"user.password\" type=\"password\" ng-maxlength=\"50\" ng-pattern=\"/^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^&*-_]{8,}$/\" required class=\"form-control text-box\"> </div> </div> <label for=\"password\" ng-show=\"submit && resetPassForm.password.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"password_pattern\" ng-show=\"submit && resetPassForm.password.$error.pattern\" class=\"error\">{{formValidation.passwordValidation}}</label> <label for=\"password_min\" ng-show=\"submit && resetPassForm.password.$touched && resetPassForm.password.$error.minlength\" class=\"error\">{{formValidation.passwordMin}}</label> <label for=\"password_max\" ng-show=\"submit && resetPassForm.password.$touched && resetPassForm.password.$error.maxlength\" class=\"error\">{{formValidation.passwordMax}}</label> <div class=\"form-group\"> <div class=\"col-xs-12 p-0\"> <input name=\"repeat_password\" placeholder=\"Confirm Password\" ng-model=\"user.repeat_password\" type=\"password\" required compare-to=\"user.password\" class=\"form-control text-box\"> </div> </div> <label for=\"compare_password\" ng-show=\"submit && resetPassForm.repeat_password.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"compare_password\" ng-show=\"submit && !resetPassForm.repeat_password.$error.required && resetPassForm.repeat_password.$error.compareTo && resetPassForm.password.$valid\" class=\"error\">{{formValidation.confirmPassword}}</label> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <button type=\"submit\" ng-click=\"submit = true\" class=\"btn btn-black btn-block\">submit</button> </div> <div class=\"col-xs-6\"> <button type=\"button\" class=\"btn btn-gray btn-block\" ui-sref=\"login\">cancel</button> </div> </div> </form> </div> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/business/views/available.html',
    "<div header></div> <section> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h2 class=\"cmn-heading\">you're all set!</h2> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0 myaccount-page\"> <div class=\"col-md-12\"> <p class=\"text-center\">You have not listed any availability.</p> <button type=\"button\" class=\"btn btn-green btn-block\"><i class=\"fa fa-plus\"></i> add availability</button> </div> </div> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/business/views/business.html',
    "<div header></div> <section> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h2 class=\"cmn-heading\"><i class=\"fa fa-gears\"></i>My Business</h2> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0 myaccount-page\"> <button type=\"button\" class=\"btn btn-green btn-block\"><i class=\"fa fa-plus\"></i> add availability</button> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"mybusiness-sec-detail active\"> <h1>published</h1> <h2>tuesday</h2> <h3><img src=\"/styles/images/clock-icon.png\">10AM-12.30PM</h3> <h4>Senior stylist available</h4> <h5><img src=\"/styles/images/send-icon.png\">7 Boundary Street, London E2 7JE</h5> <p>One of our senior stylist has availability between 10am-12.30pm today. All bookings in this time will also receive a 30% discount. Appointments are expected to go very quickly, call to book now!</p> <ul> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon1.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon1-hover.png\"> </a> </li> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon2.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon2-hover.png\"> </a> </li> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon3.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon3-hover.png\"> </a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <div class=\"mybusiness-sec-detail\"> <h1>unpublished</h1> <h2>tuesday</h2> <h3><img src=\"/styles/images/clock-icon.png\">10AM-12.30PM</h3> <h4>Senior stylist available</h4> <h5><img src=\"/styles/images/send-icon.png\">7 Boundary Street, London E2 7JE</h5> <p>One of our senior stylist has availability between 10am-12.30pm today. All bookings in this time will also receive a 30% discount. Appointments are expected to go very quickly, call to book now!</p> <ul> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon1.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon1-hover.png\"> </a> </li> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon2.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon2-hover.png\"> </a> </li> <li> <a href=\"\"> <img class=\"image\" src=\"/styles/images/tab-icon3.png\"> <img class=\"hover\" src=\"/styles/images/tab-icon3-hover.png\"> </a> </li> </ul> </div> </div> </div> </div> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/business/views/register.html',
    "<div header></div> <section> <div class=\"container\"> <div class=\"col-xs myaccount-page\"> <h2 class=\"cmn-heading\">register as business</h2> <div class=\"form-login register-form\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-8 col-sm-8\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Business name</label> </div> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Telephone no.</label> </div> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Email address</label> </div> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Website</label> </div> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Additional links</label> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Address</label> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <input type=\"text\" placeholder=\"Address line 1\" class=\"form-control text-box\"> <input type=\"text\" placeholder=\"Address line 2\" class=\"form-control text-box\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <input type=\"text\" placeholder=\"Town/City\" class=\"form-control text-box\"> <input type=\"text\" placeholder=\"Post code\" class=\"form-control text-box\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">How would you prefer to be contacted?</label> </div> <div class=\"col-xs-12\"> <select class=\"form-control\"> <option>Phone</option> </select> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Select category</label> </div> <div class=\"col-xs-12\"> <select class=\"form-control\"> <option>-Select category-</option> </select> </div> </div> </div> <div class=\"col-md-12\"> <label class=\"control-label\">Select tags</label> <div class=\"col-md-12 text-center\"> <button class=\"btn btn-black\" type=\"button\">add tags</button> </div> <div class=\"search-result\"> <ul class=\"p-0 m-0\"> <li><a href=\"\">Street food <i class=\"glyphicon glyphicon-remove\"></i></a></li> <li><a href=\"\">Indian cusine <i class=\"glyphicon glyphicon-remove\"></i></a></li> </ul> </div> </div> </div> </div> <div class=\"col-md-4 col-sm-4\"> <div class=\"row\"> <div class=\"col-md-12\"> <label class=\"control-label\">Profile image</label> <div class=\"col-md-12 text-center m-t-15\"> <img src=\"/styles/images/no-image.png\" class=\"img-responsive mar-auto\"> </div> <div class=\"col-md-12 text-center\"> <label class=\"btn btn-black btn-file\"> upload image <input type=\"file\" style=\"display: none\"> </label> <span class=\"btn-block\">Recommended size 600 x 600px</span> <div class=\"link-txt\"> <a href=\"\">Or choose from our library</a> </div> </div> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 text-center\"> <button type=\"button\" class=\"btn btn-green\">next</button> </div> </div> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/business/views/tags.html',
    "<div header></div> <section> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h2 class=\"cmn-heading\">select tags</h2> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0 myaccount-page\"> <div class=\"col-md-12\"> <div class=\"funkyradio\"> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox1\" checked> <label for=\"checkbox1\">African cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox2\" checked> <label for=\"checkbox2\">Asian cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox3\" checked> <label for=\"checkbox3\">Caribbean cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox4\" checked> <label for=\"checkbox4\">Chinese cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox5\" checked> <label for=\"checkbox5\">Curry</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox6\" checked> <label for=\"checkbox6\">Desserts</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox7\" checked> <label for=\"checkbox7\">Drinks</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox8\" checked> <label for=\"checkbox8\">Fusion cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox9\" checked> <label for=\"checkbox9\">Grill</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox10\" checked> <label for=\"checkbox10\">Halal</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox11\" checked> <label for=\"checkbox11\">Healthy</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox12\" checked> <label for=\"checkbox12\">Indian cusine</label> </div> <div class=\"funkyradio-default\"> <input type=\"checkbox\" name=\"checkbox\" id=\"checkbox13\" checked> <label for=\"checkbox13\">Italian cusine</label> </div> </div> </div> </div> <div class=\"col-xs-12 text-center\"> <button type=\"button\" class=\"btn btn-search\">done</button> </div> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/findworker/templates/contact_prompt.html',
    "<div class=\"modal-body\"> <h2 class=\"text-center almost_there\">You are almost there!!</h2> <p class=\"final_step_postjob text-center\"> Please login / signup to contact this worker</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/findworker/templates/sent_message.html',
    "<div class=\"modal-body\"> <form novalidate> <div class=\"form-group postjob_type_section\"> <h4>Please enter your message</h4> <textarea class=\"form-control textarea_style1\" ng-model=\"message\" placeholder=\"Message...\"></textarea> </div> <a href=\"javascript:void(0);\" class=\"hvr-wobble-horizontal login_button\" ng-click=\"contactWorker(workerId,message)\">Send message</a> </form> </div> <style>.bootbox .modal-dialog {\n" +
    "   width: 500px;\n" +
    "   }\n" +
    "   /* @media screen and (max-width: 767px) {\n" +
    "   .bootbox .modal-dialog {\n" +
    "   width: auto;\n" +
    "   }\n" +
    "   } */\n" +
    "   .switch {\n" +
    "   position: relative;\n" +
    "   display: inline-block;\n" +
    "   vertical-align: top;\n" +
    "   width: 110px;\n" +
    "   height: 24px;\n" +
    "   padding: 3px;\n" +
    "   background-color: transparent;\n" +
    "   font-family: 'Conv_Hel_lt_normal';\n" +
    "   border-radius: 18px;\n" +
    "   cursor: pointer;\n" +
    "   }\n" +
    "   .switch-input {\n" +
    "   position: absolute;\n" +
    "   top: 0;\n" +
    "   left: 0;\n" +
    "   opacity: 0;\n" +
    "   }\n" +
    "   .switch-label {\n" +
    "   position: relative;\n" +
    "   display: block;\n" +
    "   height: inherit;\n" +
    "   font-size: 10px;\n" +
    "   background: #eceeef;\n" +
    "   border-radius: inherit;\n" +
    "   box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);\n" +
    "   -webkit-transition: 0.15s ease-out;\n" +
    "   -moz-transition: 0.15s ease-out;\n" +
    "   -o-transition: 0.15s ease-out;\n" +
    "   transition: 0.15s ease-out;\n" +
    "   -webkit-transition-property: opacity background;\n" +
    "   -moz-transition-property: opacity background;\n" +
    "   -o-transition-property: opacity background;\n" +
    "   transition-property: opacity background;\n" +
    "   }\n" +
    "   .switch-label:before,\n" +
    "   .switch-label:after {\n" +
    "   position: absolute;\n" +
    "   top: 50%;\n" +
    "   margin-top: -4px;\n" +
    "   line-height: 1;\n" +
    "   -webkit-transition: inherit;\n" +
    "   -moz-transition: inherit;\n" +
    "   -o-transition: inherit;\n" +
    "   transition: inherit;\n" +
    "   }\n" +
    "   .switch-label:before {\n" +
    "   content: attr(data-off);\n" +
    "   right: 11px;\n" +
    "   color: #7e7e7e;\n" +
    "   text-shadow: 0 1px rgba(255, 255, 255, 0.5);\n" +
    "   text-transform: capitalize;\n" +
    "   }\n" +
    "   .switch-label:after {\n" +
    "   content: attr(data-on);\n" +
    "   left: 11px;\n" +
    "   color: white;\n" +
    "   /*text-shadow: 0 1px rgba(0, 0, 0, 0.2);*/\n" +
    "   opacity: 0;\n" +
    "   text-transform: capitalize;\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label {\n" +
    "   background: #fe6601;\n" +
    "   /*box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);*/\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label:before {\n" +
    "   opacity: 0;\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label:after {\n" +
    "   opacity: 1;\n" +
    "   }\n" +
    "   .switch-handle {\n" +
    "   position: absolute;\n" +
    "   top: 6px;\n" +
    "   left: 6px;\n" +
    "   width: 18px;\n" +
    "   height: 18px;\n" +
    "   background: white;\n" +
    "   border-radius: 10px;\n" +
    "   box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "   background-image: -webkit-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: -moz-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: -o-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: linear-gradient(to bottom, white 40%, #f0f0f0);\n" +
    "   -webkit-transition: left 0.15s ease-out;\n" +
    "   -moz-transition: left 0.15s ease-out;\n" +
    "   -o-transition: left 0.15s ease-out;\n" +
    "   transition: left 0.15s ease-out;\n" +
    "   /*border:2px solid #dedede;*/\n" +
    "   }\n" +
    "   .switch-handle:before {\n" +
    "   content: '';\n" +
    "   position: absolute;\n" +
    "   top: 50%;\n" +
    "   left: 50%;\n" +
    "   margin: -6px 0 0 -6px;\n" +
    "   width: 12px;\n" +
    "   height: 12px;\n" +
    "   background: #f9f9f9;\n" +
    "   border-radius: 6px;\n" +
    "   box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);\n" +
    "   background-image: -webkit-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: -moz-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: -o-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: linear-gradient(to bottom, #eeeeee, white);\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-handle {\n" +
    "   left: 80px;\n" +
    "   box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "   }\n" +
    "   .switch-green > .switch-input:checked ~ .switch-label {\n" +
    "   background: #fe6601;\n" +
    "   }\n" +
    "   .custom_chosen .chosen-single div b {\n" +
    "   display: none !important;\n" +
    "   }\n" +
    "   .sent_msg_type\n" +
    "   {\n" +
    "   padding-left:15px;\n" +
    "   margin-top:5px;\n" +
    "   font-size:16px;\n" +
    "   color:#232323;\n" +
    "   font-family: 'Conv_helvetica_normal';\n" +
    "   }\n" +
    "   .sent_message_select\n" +
    "   {\n" +
    "   margin-top:20px;\n" +
    "   margin-bottom:5px;\n" +
    "   }</style>"
  );


  $templateCache.put('scripts/module/findworker/views/findcarer.html',
    "<div header></div> <div class=\"upToScroll buttonShow\" ng-if=\"isScrolled > 0\"> <a class=\"scrollToTop icon-up-open-big\" title=\"Back to top\" ng-click=\"scrollTopHeader();\"> </a> </div> <div class=\"content_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <ol class=\"breadcrumb\"> <li><a href=\"#/login\">Home</a></li> <li class=\"active\">Find a carer</li> </ol> </div> </div> <!-- section start --> <section class=\"findacarer\" x-sticky-boundary=\"\"> <div class=\"row\"> <div class=\"col-md-9\" ng-init=\"state = true\"> <div class=\"row toggleclass_side\" sidebar-directive=\"state\" window-dir> <div class=\"col-sm-3 left_side_filter_section\"> <h3 class=\"filter_area\"> <img src=\"styles/images/filters-icon.png\" ng-click=\"toggleState()\" alt=\"\" width=\"20\" class=\"fa-filters\"> <span class=\"hide_section\">Filters</span> <span class=\"clear_all_filter\" ng-click=\"clearAllfilters()\"><span class=\"clear_label_filter\">Clear all </span><i class=\"leftarrow\" title=\"{{toggle_lable}}\" ng-class=\"{'icon-arrow-left-circle':state,'icon-arrow-right-circle':!state}\" ng-click=\"toggleState()\"></i></span> </h3> <div class=\"display_none_section\"> <v-accordion class=\"vAccordion--default\" multiple> <!-- add expanded attribute to open the section --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('jobtype')\">clear filter</span> <v-pane-header> Type of job </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> Casual / One-off care <input type=\"checkbox\" value=\"casual\" ng-click=\"toggleJobtype('casual')\" ng-checked=\"selectedJobtype.indexOf('casual') > -1\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> Regular care <input type=\"checkbox\" value=\"regular\" ng-click=\"toggleJobtype('regular')\" ng-checked=\"selectedJobtype.indexOf('regular') > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('caregiver')\">clear filter</span> <v-pane-header> Type of caregiver </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"cg in caregivers\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"selectedCaregiver.indexOf(cg) > -1\" ng-click=\"toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <!-- <v-pane expanded>\r" +
    "\n" +
    "                                    <span class=\"clear_link_filter\" ng-click=\"clearfilter('caretime')\">clear filter</span>\r" +
    "\n" +
    "                                    <v-pane-header>\r" +
    "\n" +
    "                                        Type of care\r" +
    "\n" +
    "                                    </v-pane-header>\r" +
    "\n" +
    "                                    <v-pane-content>\r" +
    "\n" +
    "                                        <div class=\"check_box_section\">\r" +
    "\n" +
    "                                            <div class=\"control-group\">\r" +
    "\n" +
    "                                                <label class=\"control control--checkbox\">\r" +
    "\n" +
    "                                                    Night time care\r" +
    "\n" +
    "                                                    <input type=\"checkbox\" value=\"night\" ng-checked=\"selectedCaretime.indexOf('night') > -1\" ng-click=\"toggleCaretime('night')\" />\r" +
    "\n" +
    "                                                    <div class=\"control__indicator\"></div>\r" +
    "\n" +
    "                                                </label>\r" +
    "\n" +
    "                                                <label class=\"control control--checkbox\">\r" +
    "\n" +
    "                                                    Day time care\r" +
    "\n" +
    "                                                    <input type=\"checkbox\" value=\"day\" ng-checked=\"selectedCaretime.indexOf('day') > -1\" ng-click=\"toggleCaretime('day')\" />\r" +
    "\n" +
    "                                                    <div class=\"control__indicator\"></div>\r" +
    "\n" +
    "                                                </label>\r" +
    "\n" +
    "                                                <div class=\"sub_checkbox_section\" ng-if=\"selectedCaretime.indexOf('day') > -1\">\r" +
    "\n" +
    "                                                    <div class=\"check_box_section\">\r" +
    "\n" +
    "                                                        <label class=\"control control--checkbox\">\r" +
    "\n" +
    "                                                            Before school care\r" +
    "\n" +
    "                                                            <input type=\"checkbox\" value=\"Before school care\" ng-checked=\"selectedSession.indexOf('Before school care') > -1\" ng-click=\"toggleSession('Before school care')\" />\r" +
    "\n" +
    "                                                            <div class=\"control__indicator\"></div>\r" +
    "\n" +
    "                                                        </label>\r" +
    "\n" +
    "                                                        <label class=\"control control--checkbox\">\r" +
    "\n" +
    "                                                            After school care\r" +
    "\n" +
    "                                                            <input type=\"checkbox\" value=\"After school care\" ng-checked=\"selectedSession.indexOf('After school care') > -1\" ng-click=\"toggleSession('After school care')\" />\r" +
    "\n" +
    "                                                            <div class=\"control__indicator\"></div>\r" +
    "\n" +
    "                                                        </label>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </v-pane-content>\r" +
    "\n" +
    "                                </v-pane> --> <!-- ============== --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('requirement')\"> clear filter</span> <v-pane-header> Requirements </v-pane-header> <v-pane-content> <!-- <div class=\"check_box_section\">\r" +
    "\n" +
    "                                            <div class=\"control-group\">\r" +
    "\n" +
    "                                                <label class=\"control control--checkbox\" ng-repeat=\"req in requirements\">\r" +
    "\n" +
    "                                                    {{req.requirement}}\r" +
    "\n" +
    "                                                    <input type=\"checkbox\" value=\"{{req.id}}\" ng-checked=\"selectedRequirements.indexOf(req.id) > -1\" ng-click=\"toggleRequirements(req.id)\" />\r" +
    "\n" +
    "                                                    <div class=\"control__indicator\"></div>\r" +
    "\n" +
    "                                                </label>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div> --> <div ng-repeat=\"reqCategory in reqsToFilter() | filter:filterReqs\"> <p class=\"requirement_category_name\">{{reqCategory.category}}</p> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"req in requirements | filter:{category: reqCategory.category}\"> {{req.requirement}} <input type=\"checkbox\" value=\"{{req.id}}\" ng-checked=\"selectedRequirements.indexOf(req.id) > -1\" ng-click=\"toggleRequirements(req.id)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </v-pane-content> </v-pane> </v-accordion> </div> </div> <div class=\"col-md-9 col-sm-9 right_side_cen\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"search_carer_new search_section_joblist\"> <div class=\"form-group margin-bottom-30 search-shadow\"> <!-- <div class=\"input-group\">\r" +
    "\n" +
    "                                            <input type=\"text\" autocomplete name=\"\" value=\"\" class=\"form-control input-xl search-input\" ng-model=\"fullAddress\" placeholder=\"Search by suburb, state and pincode \" ng-blur=\"self.scope.searchlocselected = true\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                            <ul class=\"dropdown-menu state_city_sub\" ng-if=\"!searchlocselected && searchLocations.length > 0\">\r" +
    "\n" +
    "                                                <li ng-repeat=\"loc in searchLocations\"><a href=\"javascript:void(0)\" ng-click=\"selectLocation(loc)\">{{loc.suburb}}, {{loc.state}} {{loc.postcode}}</a></li>\r" +
    "\n" +
    "                                            </ul>\r" +
    "\n" +
    "                                            <span class=\"input-group-btn\"><button class=\"btn btn-primary br-0 \" type=\"submit\" ng-click=\"getallUsers(1)\"><i class=\"fa fa-search\"></i></button>\r" +
    "\n" +
    "                        </span>\r" +
    "\n" +
    "                                        </div> --> <div class=\"input-group pad-0 search_input\"> <input type=\"text\" class=\"form-control\" ng-model=\"fullAddress.viewval\" name=\"tag\" class=\"form-control text-box\" uib-typeahead=\"location as location.viewval for location in getLocations($viewValue)\" typeahead-wait-ms=\"500\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"setLocation($item)\" ng-change=\"clearLocationCall()\"> <img src=\"styles/images/search.png\" alt=\"\" class=\"search_icon\"> <span class=\"ajax_loader\" ng-if=\"locationLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> <span class=\"input-group-btn\"> <button class=\"btn btn-primary br-0\" type=\"submit\" ng-click=\"getallUsers(1)\" ng-disabled=\"searchDisabled\"><i class=\"fa fa-search\"></i></button> </span> </div> </div> </div> </div> </div> <div ng-if=\"carers.length == 0 && !isLoading\"> <h1 class=\"main_heading_nojob\"><i class=\"fa fa-frown-o\"></i></h1> <h1 class=\"main_heading_nojob p-t-0\">No workers found!!!</h1></div> <div class=\"row\" infinite-scroll=\"loadPages()\" infinite-scroll-distance=\"0\" infinite-scroll-parent=\"false\" infinite-scroll-disabled=\"carercanCalled\"> <div class=\"col-md-6 col-sm-6 col-xs-12\" id=\"carer_container\" ng-repeat=\"carer in carers track by $index\"> <div class=\"flip-container\"> <div class=\"flipper\"> <div class=\"front\"> <div class=\"text-center\"> <div ng-if=\"carer.profilepic\"> <img ng-src=\"/images/profilepic/{{carer.profilepic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\" width=\"80\" class=\"img-circle\"> </div> <div ng-if=\"!carer.profilepic\"> <img src=\"/images/no_image_1.png\" alt=\"\" width=\"80\" class=\"img-circle\"> </div> <h3 class=\"carer_name\">{{carer.first_name}} {{carer.last_name}} </h3> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> <h4 class=\"gender_age\"><span ng-if=\"carer.age\">{{carer.age}} years old</span> {{carer.gender | capitalize}}</h4> </div> </div> <div class=\"back\"> <div class=\"text-center\"> <h3 class=\"carer_role\" ng-if=\"carer.preference.caregivers.length > 0\"><i class=\"icon-people\"></i>&nbsp<span ng-bind=\"carer.preference.caregivers.join(' / ')\"></span> </h3> <h3 class=\"carer_role\" ng-show=\"carer.city\"><i class=\"icon-location-pin\"></i>&nbsp; {{carer.city}}, {{carer.state}} {{carer.post_code}}</h3> <button class=\"btn btn-default contact_btn\" ng-click=\"gotoProfile(carer)\">View profile</button> </div> </div> </div> </div> </div> </div> <div class=\"text-center\" ng-if=\"isLoading\"> <div id=\"loadFacebookG\"> <div id=\"blockG_1\" class=\"facebook_blockG\"></div> <div id=\"blockG_2\" class=\"facebook_blockG\"></div> <div id=\"blockG_3\" class=\"facebook_blockG\"></div> </div> <p class=\"text-center loader_text\">Loading...</p> </div> </div> </div> </div> <div class=\"col-md-3 col-sm-3 col-xs-hidden\"> <!-- <div sticky disabled-sticky=\"stickyDisabled\" offset=\"60\" >\r" +
    "\n" +
    "              <div ad1 ></div>\r" +
    "\n" +
    "              <div ad2 ></div>\r" +
    "\n" +
    "              {{stickyDisabled}}\r" +
    "\n" +
    "            </div> --> <div x-sticky=\"\" set-class-when-at-top=\"ad-fixed\"> <div ad2></div> </div> </div> </div> </section> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/home/templates/fewstep.html',
    "<div class=\"modal-body\"> <h2 class=\"text-center almost_there\">You are almost there!!</h2> <p class=\"final_step_postjob text-center\"> Please login to complete this job posting. If you do not have an account, Please sign up now</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/home/templates/job_success.html',
    "<div class=\"modal-body\"> <!-- <h2 class=\"almost_there text-center\">Success!!</h2> --> <p class=\"final_step_postjob text-justify\">Your job has been posted successfully. You can expect our valuable workers to submit their quotes for the job shortly</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/home/templates/postjob_popup.html',
    "<div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-12\"> <h4 class=\"text-center title_modal\">Post Job</h4> <!-- Nav tabs --> <div class=\"card\"> <ul class=\"nav nav-tabs\" role=\"tablist\"> <li role=\"presentation\" ng-class=\"{'active':hm.activeTab == 'home'}\" uib-tooltip=\"Type\"><a href=\"javascript:void(0);\"><i class=\"icon-people\"></i></a> </li> <li role=\"presentation\" ng-class=\"{'active':hm.activeTab == 'profile'}\" uib-tooltip=\"Location\"><a href=\"javascript:void(0);\"><i class=\"icon-location-pin\"></i></a> </li> <li role=\"presentation\" ng-class=\"{'active':hm.activeTab == 'messages'}\" uib-tooltip=\"Time & Budget\"><a href=\"javascript:void(0);\"><i class=\"icon-clock\"></i></a> </li> <li role=\"presentation\" ng-class=\"{'active':hm.activeTab == 'settings'}\" uib-tooltip=\"Requirements\"><a href=\"javascript:void(0);\"><i class=\"icon-docs\"></i></a> </li> <li role=\"presentation\" ng-class=\"{'active':hm.activeTab == 'other_details'}\" uib-tooltip=\"Other Details\"><a href=\"javascript:void(0);\"><i class=\"icon-note\"></i></a> </li> </ul> <!-- Tab panes --> <div class=\"tab-content postjob_popup\"> <div class=\"loader_div ng-hide\" ng-show=\"generalLoader\"> <div class=\"child_loader\"> <i class=\"fa fa-circle-o-notch fa-spin\" style=\"font-size: 30px\"></i> </div> </div> <div role=\"tabpanel\" class=\"tab-pane\" ng-class=\"{'active':hm.activeTab == 'home'}\" id=\"home\"> <ng-form name=\"jobType_form\" novalidate> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <p class=\"question_name\">Type</p> <h4>What type of job you want to post?</h4> <!-- Radio Section --> <div class=\"radio_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\"> <label class=\"control control--radio\"> Casual / one off care <input type=\"radio\" name=\"job_type\" value=\"0\" ng-model=\"hm.jobtype.job_type\" ng-checked=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-6\"> <label class=\"control control--radio\"> Regular care <input type=\"radio\" value=\"1\" ng-model=\"hm.jobtype.job_type\" name=\"job_type\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <!-- End Radio section --> <h4>Please select the type of care giver you want.</h4> <!-- Checkbox section --> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cg in hm.caregivers\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG\" value=\"{{cg}}\" ng-checked=\"hm.jobtype.selectedCaregiver.indexOf(cg) > -1\" ng-click=\"hm.toggleCaregiver(cg)\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> </div> <div class=\"errormsg col-md-12 m-b-10\" ng-if=\"jobType_form.$submitted && !hm.jobtype.selectedCaregiver.length\"> At least 1 option needs to be selected </div> </div> </div> <!--  End Checkbox section --> <!-- Sub child checkbox section --> <div class=\"\"> <h4>What type of care you need?</h4> <!-- Radio Section <--> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\"> <label class=\"control control--checkbox\"> Day time care <input type=\"checkbox\" ng-checked=\"hm.jobtype.careType_seletion.indexOf('day') > -1\" name=\"care_type\" value=\"1\" ng-click=\"hm.togglecareType('day')\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-6\"> <label class=\"control control--checkbox\"> Night time care <input type=\"checkbox\" name=\"care_type\" value=\"0\" ng-required=\"true\" ng-checked=\"hm.jobtype.careType_seletion.indexOf('night') > -1\" ng-click=\"hm.togglecareType('night')\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> <div class=\"errormsg\" ng-if=\"jobType_form.$submitted && !hm.jobtype.careType_seletion.length\"> Please select type of care you need. </div> </div> </div> <h4 ng-if=\"hm.jobtype.careType_seletion.indexOf('day') > -1\">Do you need Before / After school care?</h4> <div class=\"radio_section\" ng-if=\"hm.jobtype.careType_seletion.indexOf('day') > -1\"> <div class=\"row\"> <div class=\"col-sm-4\"> <label class=\"control control--radio\"> Yes <input type=\"radio\" ng-value=\"1\" name=\"schoolcaretype\" ng-model=\"hm.jobtype.schoolcaretype\" ng-required=\"hm.jobtype.careType_seletion.indexOf('day') > -1\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-8\"> <label class=\"control control--radio\"> No (9am-3pm) <input type=\"radio\" ng-value=\"0\" name=\"schoolcaretype\" ng-model=\"hm.jobtype.schoolcaretype\" ng-required=\"hm.jobtype.careType_seletion.indexOf('day') > -1\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-4\"> </div> </div> </div> <!-- End Radio section --> <div ng-if=\"hm.jobtype.schoolcaretype == 1 && hm.jobtype.careType_seletion.indexOf('day') > -1\"> <h4>Please select the type of daytime care.</h4> <div class=\"check_box_section\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cs in hm.careSession\"> <label class=\"control control--checkbox\"> {{cs}} <input type=\"checkbox\" ng-checked=\"hm.jobtype.careSession_selection.indexOf(cs) > -1\" value=\"{{cs}}\" name=\"care_session\" ng-click=\"hm.toggleCaresession(cs)\" ng-required=\"hm.jobtype.careType_seletion.indexOf('day') > -1 && hm.jobtype.schoolcaretype == 1\"> <div class=\"control__indicator\"></div> </label> </div> </div> <div class=\"errormsg\" ng-if=\"jobType_form.$submitted && !hm.jobtype.careSession_selection.length\"> Please select the type of day time care </div> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <a href=\"javascript:void(0);\" class=\"m-t-30 btn postjob_button hvr-wobble-horizontal\" ng-click=\"hm.jobTypesubmit(jobType_form)\">Continue</a> </div> </div> </ng-form> </div> <div role=\"tabpanel\" class=\"tab-pane\" ng-class=\"{'active':hm.activeTab == 'profile'}\" id=\"profile\"> <div class=\"location_postjob_section\"> <ng-form name=\"jobLocation_form\" novalidate> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <p class=\"question_name\">Location & Children</p> <!-- Radio Section --> <div class=\"radio_section\"> <div class=\"control-group\"> <h4>Is this job from a Childcare centre (or) Kindergarten (or) Before care / After care Centre?</h4> <div class=\"row\"> <div class=\"col-sm-4\"> <label class=\"control control--radio\"> No <input type=\"radio\" ng-model=\"hm.joblocation.before_after\" value=\"0\" name=\"before_after\" ng-checked=\"hm.joblocation.before_after == 0\" ng-click=\"hm.befre_after_change()\" ng-checked=\"hm.joblocation.before_after == 0\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-8\"> <label class=\"control control--radio\"> Yes <input type=\"radio\" ng-model=\"hm.joblocation.before_after\" ng-click=\"hm.befre_after_change()\" value=\"1\" name=\"before_after\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <!-- <div class=\"radio_section\" ng-if=\"hm.joblocation.before_after == 1\">\n" +
    "                                    <div class=\"control-group\">\n" +
    "                                    <h4>What is the name of the Childcare / Before care / After care Center?</h4>\n" +
    "                                        <label class=\"control control--radio\">\n" +
    "                                            No\n" +
    "                                            <input type=\"radio\" ng-model=\"hm.joblocation.before_after_name\" value=\"0\" name=\"before_after_name\" checked=\"checked\" ng-required=\"hm.joblocation.before_after == 1\"/>\n" +
    "                                            <div class=\"control__indicator\"></div>\n" +
    "                                        </label>\n" +
    "                                        <label class=\"control control--radio\">\n" +
    "                                            Yes\n" +
    "                                            <input type=\"radio\" ng-model=\"hm.joblocation.before_after_name\" value=\"1\" name=\"before_after_name\" checked=\"checked\" ng-required=\"hm.joblocation.before_after == 1\" ng-click=\"hm.joblocation.location=''\"/>\n" +
    "                                            <div class=\"control__indicator\"></div>\n" +
    "                                        </label>\n" +
    "                                        <div class=\"errormsg\" ng-if=\"jobLocation_form.$submitted && jobLocation_form.before_after.$error.required\">\n" +
    "                                            Please select this field\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    </div> --> <h4 ng-show=\"hm.joblocation.before_after == 1\" class=\"m-b-0\">What is the name of your Childcare centre (or) Kindergarden (or) Before care / After care Centre?</h4> <div class=\"m-b-30\" ng-show=\"hm.joblocation.before_after == 1\"> <div class=\"group\"> <input type=\"text\" ng-model=\"hm.joblocation.care_name\" ng-required=\"hm.joblocation.before_after == 1\" name=\"care_name\" data-ng-pattern=\"/^[a-zA-Z\\s]*$/\" ng-maxlength=\"50\" placeholder=\"Please enter the name of the centre \"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <!-- <label>Care name</label> --> <div class=\"errormsg\" ng-if=\"jobLocation_form.$submitted && jobLocation_form.care_name.$invalid\"> <p ng-if=\"jobLocation_form.care_name.$error.required\">Please enter care name</p> <p ng-if=\"jobLocation_form.care_name.$error.pattern\">Field can contain only alphabet values.</p> <p ng-show=\"jobLocation_form.care_name.$error.maxlength\">Childcare centre name should not exceed 50 characters.</p> </div> </div> </div> <!-- Address for Childcare center --> <div class=\"m-t-10 m-b-10\" ng-if=\"hm.joblocation.before_after == 1\"> <h4 class=\"m-b-0\">Address</h4> <div class=\"group\"> <input type=\"text\" ng-model=\"hm.joblocation.address\" ng-required=\"hm.joblocation.before_after == 1\" name=\"address1\" data-ng-pattern=\"/^[a-zA-Z0-9\\s]*$/\" ng-maxlength=\"500\" placeholder=\"Please enter the address of the centre\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <!-- <label>Centre Address</label> --> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.address1.$dirty) && jobLocation_form.address1.$invalid\"> <p ng-if=\"jobLocation_form.address1.$error.required\">Please enter a valid address</p> <p ng-if=\"jobLocation_form.address1.$error.pattern\">Must be Alphanumeric</p> <p ng-if=\"jobLocation_form.address1.$error.maxlength\">Must below 500 charecters</p> </div> </div> </div> <div class=\"row\" ng-if=\"hm.joblocation.before_after == 1\"> <div class=\"col-md-12\"> <h4 class=\"m-b-0\">Please select your location</h4> <div class=\"m-b-10\"> <div class=\"group\"> <p class=\"icon_load_spin\"> <input type=\"text\" ng-model=\"hm.joblocation.loc_id\" name=\"autoaddress\" uib-typeahead=\"location as location.viewval for location in hm.filteredLocations\" typeahead-wait-ms=\"600\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"hm.selectLocation($item,true)\" ng-change=\"hm.getLocations(hm.joblocation.loc_id)\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <span class=\"ajax_loader\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> <span class=\"ajax_loader color_red\" ng-if=\"hm.joblocation.loc_id\"><i class=\"fa fa-times-circle\" ng-click=\"hm.joblocation.loc_id='';hm.joblocation.city='';hm.joblocation.state='';hm.joblocation.post_code=''\"></i></span> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.autoaddress.$dirty) && jobLocation_form.autoaddress.$invalid\"> <p ng-if=\"jobLocation_form.autoaddress.$error.required\">Please enter a valid location</p> </div> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.autoaddress.$dirty) && (!hm.joblocation.city || !hm.joblocation.state || !hm.joblocation.post_code) && !jobLocation_form.autoaddress.$error.required && hm.filteredLocations.length == 0\"> <p>Invalid city, state and post code</p> </div> </p> </div> </div> </div> </div> <div class=\"radio_section\" ng-if=\"hm.joblocation.before_after == 0\"> <h4>Where do you want the care to be provided at?</h4> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-4\"> <label class=\"control control--radio\"> Our location <input type=\"radio\" ng-model=\"hm.joblocation.location\" value=\"0\" name=\"location\" ng-required=\"hm.joblocation.before_after == 0\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-7\"> <label class=\"control control--radio\"> Caregiver's home (Family Daycare) <input type=\"radio\" ng-model=\"hm.joblocation.location\" value=\"1\" name=\"location\" ng-click=\"hm.joblocation.address = '';hm.joblocation.loc_id = '';\" ng-checked=\"hm.joblocation.before_after == 0\" ng-required=\"hm.joblocation.before_after== 0\"> <div class=\"control__indicator\"></div> </label> </div> </div> <div class=\"errormsg col-md-12 m-b-10\" ng-if=\"jobLocation_form.$submitted && jobLocation_form.location.$error.required\"> Please select location </div> </div> </div> <!-- End Radio section --> <div class=\"m-t-10 m-b-10\" ng-if=\"hm.joblocation.location == 0 && hm.joblocation.before_after == 0\"> <div class=\"group\"> <h4 class=\"m-b-0\">Address</h4> <input type=\"text\" ng-model=\"hm.joblocation.address\" ng-required=\"hm.joblocation.location == 0 && hm.joblocation.before_after == 0\" name=\"address\" ng-maxlength=\"500\" placeholder=\"Please enter the address\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <!-- <label>Address</label> --> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.address.$dirty) && jobLocation_form.address.$invalid\"> <p ng-if=\"jobLocation_form.address.$error.required\">Please enter a valid address</p> <p ng-if=\"jobLocation_form.address.$error.pattern\">Must be Alphanumeric</p> <p ng-if=\"jobLocation_form.address.$error.maxlength\">Must below 500 charecters</p> </div> </div> </div> <div class=\"row\" ng-if=\"hm.joblocation.location == 0 && hm.joblocation.before_after == 0\"> <div class=\"col-md-12\"> <h4>Suburb, State, Postcode</h4> <div class=\"m-b-10 plugin_dropdown_country\"> <div class=\"group\"> <p class=\"icon_load_spin\"> <!-- <input type=\"text\" autocomplete1 ng-model=\"hm.joblocation.loc_id\" name=\"autoaddress\" ng-required=\"hm.joblocation.location == 0 && hm.joblocation.before_after == 0\" placeholder=\"Suburb, State, Postal code\" autocomplete-loading=\"locLoading\">\n" +
    "                                                            <span class=\"highlight\"></span>\n" +
    "                                                            <span class=\"bar\"></span>\n" +
    "                                                            <span class=\"ajax_loader\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span>\n" +
    "                                                            <span class=\"ajax_loader color_red\"><i class=\"fa fa-times-circle\" ng-click=\"hm.joblocation.loc_id='';hm.joblocation.city='';hm.joblocation.state='';hm.joblocation.post_code=''\"></i></span>\n" +
    "                                                            <ul class=\"dropdown-menu state_city_sub\" ng-if=\"hm.locations.length && !hm.locselected && !jobLocation_form.autoaddress.$error.required\" aria-labelledby=\"IDREF\">\n" +
    "                                                                <li tabindex=\"-1\" ng-repeat=\"loc in hm.locations\"><a href=\"javascript:void(0)\" ng-click=\"hm.selectLocation(loc)\">{{loc.suburb}}, {{loc.state}}, {{loc.postcode}}</a></li>\n" +
    "                                                            </ul> --> <input type=\"text\" ng-model=\"hm.joblocation.loc_id\" name=\"autoaddress\" uib-typeahead=\"location as location.viewval for location in hm.filteredLocations\" typeahead-wait-ms=\"600\" placeholder=\"Please type here\" typeahead-on-select=\"hm.selectLocation($item,true)\" ng-change=\"hm.getLocations(hm.joblocation.loc_id)\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <span class=\"ajax_loader\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> <span class=\"ajax_loader color_red\" ng-if=\"hm.joblocation.loc_id\"><i class=\"fa fa-times-circle\" ng-click=\"hm.joblocation.loc_id='';hm.joblocation.city='';hm.joblocation.state='';hm.joblocation.post_code=''\"></i></span> </p> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.autoaddress.$dirty) && jobLocation_form.autoaddress.$invalid\"> <p ng-if=\"jobLocation_form.autoaddress.$error.required\">Please enter a valid location</p> </div> <div class=\"errormsg\" ng-if=\"(jobLocation_form.$submitted || jobLocation_form.autoaddress.$dirty) && (!hm.joblocation.city || !hm.joblocation.state || !hm.joblocation.post_code) && !jobLocation_form.autoaddress.$error.required && hm.filteredLocations.length == 0\"> <p>Invalid city, state and post code</p> </div> </div> </div> </div> </div> <div class=\"m-b-30 chosen_style\"> <h4>Number of children?</h4> <select chosen class=\"form-control\" ng-model=\"hm.joblocation.no_of_children\" ng-required=\"true\" name=\"no_of_children\" disable-search=\"true\" data-placeholder=\"Select\"> <option value=\"\"></option> <option value=\"1\">1</option> <option value=\"2\">2</option> <option value=\"3\">3</option> <option value=\"4\">4</option> <option value=\"5\">5</option> <option value=\"6\">6</option> <option value=\"7\">7</option> <option value=\"8\">8</option> <option value=\"9\">9</option> <option value=\"9+\">9+</option> </select> <div class=\"errormsg\" ng-if=\"jobLocation_form.$submitted && jobLocation_form.no_of_children.$error.required\"> Please select number of children </div> </div> <h4>Age group of children?</h4> <div class=\"\"> <div class=\"check_box_section\"> <div class=\"row\"> <div class=\"col-sm-4\" ng-repeat=\"cs in hm.childrenAge\"> <label class=\"control control--checkbox\"> {{cs}} <input type=\"checkbox\" ng-checked=\"hm.joblocation.age_selection.indexOf(cs) > -1\" value=\"{{cs}}\" name=\"care_session[]\" ng-click=\"hm.toggleAge(cs)\"> <div class=\"control__indicator\"></div> </label> </div> </div> <div class=\"errormsg\" ng-if=\"jobLocation_form.$submitted && !hm.joblocation.age_selection.length\"> Please select age group of children </div> <!-- <label class=\"control control--checkbox\">\n" +
    "                                          After school care\n" +
    "                                          <input type=\"checkbox\" value=\"{{cs}}\" name=\"care_session[]\"/>\n" +
    "                                          <div class=\"control__indicator\"></div>\n" +
    "                                          </label> --> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"m-t-30\"> <div class=\"col-md-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"hm.tabclick('home')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> <!-- <a href=\"javascript:void(0)\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"hm.tabclick('home')\">Previous</a> --> </div> <div class=\"col-md-6\"> <a href=\"javascript:void(0)\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"hm.jobLocsubmit(jobLocation_form)\">Continue</a> </div> </div> </div> </ng-form> </div> </div> <div role=\"tabpanel\" ng-class=\"{'active':hm.activeTab == 'messages'}\" class=\"tab-pane\" id=\"messages\"> <ng-form name=\"jobTime_form\" novalidate> <div class=\"row\"> <div class=\"col-md-12 datepicker_style_uib postjob_type_section\"> <div class=\"row\"> <div class=\"col-sm-12\"> <p class=\"question_name\">Time & Budget</p> <div class=\"col-container-date\"> <div class=\"col-date-label\"> <h4 ng-show=\"hm.jobtype.job_type==0\" class=\"custom_label\">Required date</h4> <h4 ng-show=\"hm.jobtype.job_type==1\" class=\"custom_label\">Required start Date</h4> </div> <div class=\"col-picler\"> <p class=\"input-group\"> <input type=\"text\" placeholder=\"DD/MM/YYYY\" ng-click=\"popup1.opened = !popup1.opened\" class=\"form-control\" uib-datepicker-popup=\"{{format}}\" ng-model=\"hm.jobtime.date\" name=\"date\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" close-text=\"Close\" alt-input-formats=\"altInputFormats\" ng-required=\"true\" ng-readonly=\"true\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default ad-onn\" ng-click=\"open1()\"><i class=\"fa fa-calendar\"></i></button> </span> <div class=\"errormsg\" ng-if=\"jobTime_form.date.$error.required\"> Please enter a valid date </div> </p> </div> </div> </div> </div> </div> </div> <div ng-if=\"hm.jobtype.job_type == 1\" class=\"postjob_type_section m-t-20\"> <h4>Please select the days required?</h4> <!-- Checkbox section --> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-4\" ng-repeat=\"cg in hm.days_required\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" ng-required=\"hm.jobtype.job_type == 1\" name=\"selectedDays[]\" value=\"{{cg}}\" ng-checked=\"hm.jobtime.days_selection.indexOf(cg) > -1\" ng-click=\"hm.toggleDays(cg)\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"errormsg col-sm-12\" ng-if=\"jobTime_form.$submitted && !hm.jobtime.days_selection.length\"> Please select Number of days required </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"col-container-date postjob_type_section\"> <div class=\"col-date-label\"> <h4 class=\"custom_label\">Start time</h4> </div> <div class=\"col-picler\"> <div uib-timepicker ng-model=\"hm.jobtime.time\" ng-change=\"changed()\" hour-step=\"hm.hstep\" minute-step=\"hm.mstep\" show-meridian=\"true\" name=\"time\" ng-readonly=\"true\"></div> <div class=\"errormsg\" ng-if=\"(jobTime_form.$submitted || jobTime_form.time.$dirty) && jobTime_form.time.$invalid\"> Please enter a valid time </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" ng-pattern=\"/^[0-9]\\d*$/\" ng-model=\"hm.jobtime.hours_required\" ng-maxlength=\"2\" ng-change=\"hm.getBudget()\" ng-required=\"true\" name=\"hours_required\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label ng-if=\"hm.jobtype.job_type == 0\">Number of hours required</label> <label ng-if=\"hm.jobtype.job_type == 1\">Hours required per day</label> <div class=\"errormsg\" ng-if=\"hm.jobtime.hours_required > 24\"> <p>Number of hours must not be greater than 24</p> </div> <div class=\"errormsg\" ng-if=\"hm.jobtime.hours_required == '0'\"> <p>Number of hours must not be zero</p> </div> <div class=\"errormsg\" ng-if=\"(jobTime_form.$submitted || jobTime_form.hours_required.$dirty) && jobTime_form.hours_required.$invalid\"> <p ng-if=\"jobTime_form.hours_required.$error.required\">Please enter number of hours required.</p> <p ng-if=\"jobTime_form.hours_required.$error.pattern\">Hours must be in numbers and should not be 0</p> <p ng-if=\"jobTime_form.hours_required.$error.maxlength\">Hours must below or equal to 2 digits</p> </div> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" ng-pattern=\"/d{0,2}(\\.\\d{1,2})?/\" ng-model=\"hm.jobtime.rate_per_hour\" ng-maxlength=\"5\" ng-change=\"hm.getBudget()\" ng-required=\"true\" name=\"rate_per_hour\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Rate per hour ($)</label> <div class=\"errormsg\" ng-if=\"hm.jobtime.rate_per_hour == '0'\"> <p>Rate per hour must not be zero</p> </div> <div class=\"errormsg\" ng-if=\"(jobTime_form.$submitted || jobTime_form.rate_per_hour.$dirty) && jobTime_form.rate_per_hour.$invalid\"> <p ng-if=\"jobTime_form.rate_per_hour.$error.required\">Please enter rate per hour required.</p> <p ng-if=\"jobTime_form.rate_per_hour.$error.pattern\">Rate per hour must be in number and should not be 0</p> <p ng-if=\"jobTime_form.rate_per_hour.$error.maxlength\">Rate per hour should not exceed 5 digits</p> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <p class=\"pull-right price_section\"><a href=\"#\" uib-tooltip=\"Qualified Professional - Certificate III: $32 per hour\n" +
    "                       Qualified Professional - Diploma: $36 per hour\n" +
    "                       Qualified Professional - Certificate III: $48 per hour\n" +
    "                       Baby Sitter: $28 per hour\n" +
    "                       Nanny : $28 per hour\n" +
    "                       Au Pair: $28 per hour\n" +
    "                       Tutor / Educator: $36 per hour\">Price Guide?</a>\n" +
    "                              </p> --> <div class=\"price_section col-xs-12 clearfix\"> <p class=\"pull-right\" uib-popover-html=\"'<table>\n" +
    "                                <tr>\n" +
    "                                  <th>Caregiver type</th>\n" +
    "                                  <th>Rate per hour</th>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Qualified Professional-Certificate III</td>\n" +
    "\n" +
    "                                  <td>$ 32 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Qualified Professional - Diploma</td>\n" +
    "\n" +
    "                                  <td>$ 36 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Qualified Professional - Bachelor </td>\n" +
    "\n" +
    "                                  <td>$ 48 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Baby Sitter</td>\n" +
    "\n" +
    "                                  <td>$ 28 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Nanny</td>\n" +
    "\n" +
    "                                  <td>$ 28 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Au Pair</td>\n" +
    "\n" +
    "                                  <td>$ 28 </td>\n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                  <td>Tutor / Educator</td>\n" +
    "\n" +
    "                                  <td>$ 36 </td>\n" +
    "                                </tr>\n" +
    "                              </table>'\" popover-trigger=\"'mouseenter'\"><a href=\"javascript:void(0)\">Price Guide?</a> <!-- price guide modal content --> <!-- end price guide modal content --> </p> </div> <h4 class=\"estimated_section clearfix\" ng-if=\"hm.jobtype.job_type == 1 && hm.jobtime.rate_per_hour && hm.jobtime.hours_required\">Estimated budget per day<span>:${{hm.jobtime.budget}}<i class=\"icon-info circle_info\" uib-tooltip=\" Estimated Budget = (Number of hours required * Rate per hour) + X% Platform Customer Service Fees + Y% Online Payment Fees (Paypal)+ Z% GST\n" +
    "                      \"></i></span></h4> <!-- <h4 class=\"estimated_section\" ng-if=\"hm.jobtype.job_type == 0\">Estimated budget<span>:&nbsp;${{hm.jobtime.budget}} <i class=\"icon-info circle_info\" uib-tooltip=\"Estimated Budget = (Number of hours required * Rate per hour) + {{hm.jobtime.x}} % Platform Customer Service Fees +  {{hm.jobtime.y}} % Online Payment Fees +  {{hm.jobtime.z}} % GST\n" +
    "                      \" ></i></span></h4> --> <h4 class=\"estimated_section\" ng-if=\"hm.jobtype.job_type == 0 && hm.jobtime.rate_per_hour && hm.jobtime.hours_required\">Estimated budget<span>:&nbsp;${{hm.jobtime.budget}} <i class=\"icon-info circle_info\" uib-popover=\"Estimated Budget = (Number of hours required * Rate per hour) + {{hm.jobtime.x}} % Platform Customer Service Fees +  {{hm.jobtime.y}} % Online Payment Fees +  {{hm.jobtime.z}} % GST\" popover-trigger=\"'mouseenter'\"></i></span></h4> </div> </div> <div class=\"row\"> <div class=\"m-t-30\"> <div class=\"col-md-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"hm.tabclick('profile')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> <!-- <a href=\"javascript:void(0);\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"hm.tabclick('profile')\">Previous</a> --> </div> <div class=\"col-md-6\"> <a href=\"javascript:void(0);\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"hm.jobTimesubmit(jobTime_form)\">Continue</a> </div> </div> </div> </ng-form> </div> <div role=\"tabpanel\" ng-class=\"{'active':hm.activeTab=='settings'}\" class=\"tab-pane\" id=\"settings\"> <ng-form name=\"jobSettings_form\" novalidate> <div class=\"row icono_requirement\"> <div class=\"col-md-12\"> <p class=\"question_name\">requirements</p> <div class=\"form-group clearfix postjob_requirement\" ng-repeat=\"req in hm.requirements\"> <label class=\"control-label col-sm-9\"><i class=\"{{req.icon_class}} pull-left\"></i><span class=\"reqvalue\">{{req.requirement}}</span></label> <div class=\"col-sm-3\"> <div> <!--  <label class=\"switch\">\n" +
    "                                       <input type=\"checkbox\" name=\"onoffswitch[]\"  value=\"{{req}}\" ng-checked=\"hm.jobrequirement.opt_selection.indexOf(req) > -1\" ng-click=\"hm.toggleReq(req)\">\n" +
    "                                       <div class=\"slider\"></div>\n" +
    "                                       </label> --> <label class=\"switch\"> <input type=\"checkbox\" class=\"switch-input\" name=\"onoffswitch[]\" value=\"{{req.id}}\" ng-checked=\"hm.jobrequirement.opt_selection.indexOf(req.id) > -1\" ng-click=\"hm.toggleReq(req.id)\"> <span class=\"switch-label\" data-on=\"Required\" data-off=\"Not Required\"></span> <span class=\"switch-handle\"></span> </label> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"m-t-30\"> <div class=\"col-md-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"hm.tabclick('messages')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> <!-- <a href=\"javascript:void(0);\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"hm.tabclick('messages')\">Previous</a> --> </div> <div class=\"col-md-6\"> <a href=\"javascript:void(0);\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"hm.tabclick('other_details')\">Continue</a> </div> </div> </div> </ng-form> </div> <div role=\"tabpanel\" ng-class=\"{'active':hm.activeTab=='other_details'}\" class=\"tab-pane\" id=\"other_details\"> <div class=\"row\" ng-form=\"otherreq\"> <div class=\"col-md-12 editor_global_style\"> <p class=\"question_name\">Other Details</p> <span class=\"postjob_type_section\"> <h4>Describe the job and any other requirements in details</h4> </span> <!-- <div text-angular ta-toolbar=\"[['justifyLeft','justifyCenter'],['bold','italics','underline']]\" ng-model=\"hm.jobrequirement.other_requirement\" placeholder=\"Enter other details\"></div> --> <div class=\"m-t-20\"> <textarea ckeditor=\"ckoptions\" ng-model=\"hm.jobrequirement.other_requirement\" ng-required=\"false\" name=\"other_requirement\" height=\"100\"></textarea> </div> <div class=\"errormsg\" ng-if=\"otherreq.$submitted && otherreq.$invalid\"> <p ng-if=\"otherreq.other_requirement.$error.required\">Please enter other requirements</p> </div> </div> </div> <div class=\"row\"> <div class=\"m-t-30\"> <div class=\"col-md-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"hm.tabclick('settings')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> <!-- <a href=\"javascript:void(0);\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"hm.tabclick('settings')\">Previous</a> --> </div> <div class=\"col-md-6\"> <a href=\"javascript:void(0);\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"hm.finalSubmit(otherreq)\">Submit</a> </div> </div> </div> </div> </div> </div> </div> </div> </div> <style type=\"text/css\">.switch {\n" +
    "    position: relative;\n" +
    "    display: inline-block;\n" +
    "    vertical-align: top;\n" +
    "    width: 110px;\n" +
    "    height: 24px;\n" +
    "    padding: 3px;\n" +
    "    background-color: transparent;\n" +
    "    /*font-family: 'Conv_helvetica_normal';*/\n" +
    "    font-family: 'Conv_Hel_lt_normal';\n" +
    "    border-radius: 18px;\n" +
    "    /*box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);*/\n" +
    "    cursor: pointer;\n" +
    "    /*background-image: -webkit-linear-gradient(top, #eeeeee, white 25px);\n" +
    "   background-image: -moz-linear-gradient(top, #eeeeee, white 25px);\n" +
    "   background-image: -o-linear-gradient(top, #eeeeee, white 25px);\n" +
    "   background-image: linear-gradient(to bottom, #eeeeee, white 25px);*/\n" +
    "}\n" +
    "\n" +
    ".switch-input {\n" +
    "    position: absolute;\n" +
    "    top: 0;\n" +
    "    left: 0;\n" +
    "    opacity: 0;\n" +
    "}\n" +
    "\n" +
    ".switch-label {\n" +
    "    position: relative;\n" +
    "    display: block;\n" +
    "    height: inherit;\n" +
    "    font-size: 9px;\n" +
    "    background: #eceeef;\n" +
    "    border-radius: inherit;\n" +
    "    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);\n" +
    "    -webkit-transition: 0.15s ease-out;\n" +
    "    -moz-transition: 0.15s ease-out;\n" +
    "    -o-transition: 0.15s ease-out;\n" +
    "    transition: 0.15s ease-out;\n" +
    "    -webkit-transition-property: opacity background;\n" +
    "    -moz-transition-property: opacity background;\n" +
    "    -o-transition-property: opacity background;\n" +
    "    transition-property: opacity background;\n" +
    "}\n" +
    "\n" +
    ".switch-label:before,\n" +
    ".switch-label:after {\n" +
    "    position: absolute;\n" +
    "    top: 50%;\n" +
    "    margin-top: -4px;\n" +
    "    line-height: 1;\n" +
    "    -webkit-transition: inherit;\n" +
    "    -moz-transition: inherit;\n" +
    "    -o-transition: inherit;\n" +
    "    transition: inherit;\n" +
    "}\n" +
    "\n" +
    ".switch-label:before {\n" +
    "    content: attr(data-off);\n" +
    "    right: 11px;\n" +
    "    color: #7e7e7e;\n" +
    "    text-shadow: 0 1px rgba(255, 255, 255, 0.5);\n" +
    "    text-transform: capitalize;\n" +
    "}\n" +
    "\n" +
    ".switch-label:after {\n" +
    "    content: attr(data-on);\n" +
    "    left: 11px;\n" +
    "    color: white;\n" +
    "    /*text-shadow: 0 1px rgba(0, 0, 0, 0.2);*/\n" +
    "    opacity: 0;\n" +
    "    text-transform: capitalize;\n" +
    "}\n" +
    "\n" +
    ".switch-input:checked ~ .switch-label {\n" +
    "    background: #fe6601;\n" +
    "    /*box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);*/\n" +
    "}\n" +
    "\n" +
    ".switch-input:checked ~ .switch-label:before {\n" +
    "    opacity: 0;\n" +
    "}\n" +
    "\n" +
    ".switch-input:checked ~ .switch-label:after {\n" +
    "    opacity: 1;\n" +
    "}\n" +
    "\n" +
    ".switch-handle {\n" +
    "    position: absolute;\n" +
    "    top: 6px;\n" +
    "    left: 6px;\n" +
    "    width: 18px;\n" +
    "    height: 18px;\n" +
    "    background: white;\n" +
    "    border-radius: 10px;\n" +
    "    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "    background-image: -webkit-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "    background-image: -moz-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "    background-image: -o-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "    background-image: linear-gradient(to bottom, white 40%, #f0f0f0);\n" +
    "    -webkit-transition: left 0.15s ease-out;\n" +
    "    -moz-transition: left 0.15s ease-out;\n" +
    "    -o-transition: left 0.15s ease-out;\n" +
    "    transition: left 0.15s ease-out;\n" +
    "    /*border:2px solid #dedede;*/\n" +
    "}\n" +
    "\n" +
    ".switch-handle:before {\n" +
    "    content: '';\n" +
    "    position: absolute;\n" +
    "    top: 50%;\n" +
    "    left: 50%;\n" +
    "    margin: -6px 0 0 -6px;\n" +
    "    width: 12px;\n" +
    "    height: 12px;\n" +
    "    background: #f9f9f9;\n" +
    "    border-radius: 6px;\n" +
    "    box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);\n" +
    "    background-image: -webkit-linear-gradient(top, #eeeeee, white);\n" +
    "    background-image: -moz-linear-gradient(top, #eeeeee, white);\n" +
    "    background-image: -o-linear-gradient(top, #eeeeee, white);\n" +
    "    background-image: linear-gradient(to bottom, #eeeeee, white);\n" +
    "}\n" +
    "\n" +
    ".switch-input:checked ~ .switch-handle {\n" +
    "    left: 80px;\n" +
    "    box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "}\n" +
    "\n" +
    ".switch-green > .switch-input:checked ~ .switch-label {\n" +
    "    background: #fe6601;\n" +
    "}\n" +
    "\n" +
    ".custom_chosen .chosen-single div b {\n" +
    "    display: none !important;\n" +
    "}</style> <script>$(\"html,body\").scrollTop(0);</script>"
  );


  $templateCache.put('scripts/module/home/views/home.html',
    "<div header></div> <div> <div class=\"bgvideoimage\"> <!-- <img src=\"styles/images/banner1.png\" class=\"cover\" /> --> <!-- <img src=\"styles/images/ba2.png\" class=\"cover\" /> --> <!-- <img src=\"styles/images/3.png\" class=\"cover\" /> --> <video-background class=\"vid_bgr\" source=\"{ mp4: 'styles/video/kinder.mp4' }\" autoplay loop show-time=\"false\"></video-background> <div class=\"overlay_dot\"></div> </div> <!-- right side text --> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-6 col-md-offset-6 col-sm-12 col-xs-12\"> <div class=\"cca-hero-content\"> <h1>Connecting childcare centers, Parents & Agencies </h1> <p>With qualified childcare professionals & casual workers for free! </p> <a class=\"btn cca-btn-fill hvr-wobble-horizontal\" onclick=\"document.getElementById('signupLink').click()\">Join Now</a> <!-- <p class=\"cca-secondary-cta\">\n" +
    "     Are you a job seeker? Click\n" +
    "     <a href=\"javascript:void(0);\" onclick=\"document.getElementById('loginLink').click()\">here.</a>\n" +
    "     </p> --> </div> </div> </div> <!-- <div class=\"row\">\n" +
    "         <div class=\"homepage_search clearfix\">\n" +
    "           <div class=\"col-md-5\">\n" +
    "\n" +
    "           </div>\n" +
    "           <div class=\"col-md-7\">\n" +
    "             <div class=\"cca-hero-content\">\n" +
    "             <h1>Your Leading Resource to Find Quality Child Care Staff</h1>\n" +
    "             <p>Lovely Childcare is the only online service devoted entirely to helping child care centres connect with qualified staff.</p>\n" +
    "             <div class=\"search_form_home\">\n" +
    "\n" +
    "                <div class=\"ctn-btn-section m-t-30\">\n" +
    "                <a class=\"btn cca-btn-fill hvr-wobble-vertical btn_gray_home\" ng-click=\"vm.callpostjob()\">Post a Job</a>\n" +
    "                <a class=\"btn cca-btn-fill hvr-wobble-vertical \" ng-click=\"vm.callpostjob()\">Search Job</a>\n" +
    "                </div>\n" +
    "\n" +
    "             </div>\n" +
    "           </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "   </div> --> </div> <!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"></div> --> <!-- <footer></footer> --> <style>.navigation_section{\n" +
    "   box-shadow: none;\n" +
    "   }\n" +
    "   .navigation_section, .navigation_section .navbar-default{\n" +
    "   background: rgba(255, 255, 255, 0.6);\n" +
    "   /*background: #fff;*/\n" +
    "   }\n" +
    "   .pre-header{\n" +
    "   background: rgba(0, 0, 0, 0.2);\n" +
    "   /*background: transparent;*/\n" +
    "   }\n" +
    "   .pre-header li {\n" +
    "   color:#fff;\n" +
    "   border-right:1px solid #fff;\n" +
    "   }</style></div>"
  );


  $templateCache.put('scripts/module/job/templates/admin_closejob.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">Are you sure you want to Close this job?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/job/templates/bidsubmit.html',
    "<div class=\"modal-body\"> <h3 class=\"title_modal secondary_heading_modal\">Submit a Bid</h3> <form novalidate name=\"bidsubmitForm\" ng-submit=\"takeBid(bidsubmitForm)\"> <div class=\"m-t-10\"> <div class=\"group m-b-10\"> <input name=\"rate\" required data-ng-pattern=\"/^[0-9]+(.[0-9]{1,2})?$/\" type=\"text\" ng-model=\"bid.rate_per_hour\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Enter your hourly rate ($)</label> <div class=\"errormsg\" ng-if=\"(bidsubmitForm.$submitted || bidsubmitForm.rate.$dirty) && bidsubmitForm.rate.$invalid\"> <p ng-if=\"bidsubmitForm.rate.$error.required\">Please enter hourly rate</p> <!-- <p ng-if=\"bidsubmitForm.rate.$error.pattern && bid.rate != 0\">Must be Numeric</p> --> <p ng-if=\"bidsubmitForm.rate.$error.pattern\">Rate must greater than zero with 2 decimal points</p> </div> </div> </div> <div class=\"row postjob_type_section\"> <div class=\"col-sm-12\"> <h4 class=\"sub_bg_label\">Number of hours required:<span>&nbsp;{{currentBid.hours_required}}</span></h4> </div> <div class=\"col-sm-12\"> <h4 class=\"sub_bg_label\">My Quote:<span ng-if=\"bid.rate_per_hour\">&nbsp;${{bid.rate_per_hour * currentBid.hours_required}}</span></h4> </div> </div> <div class=\"form-group postjob_type_section\"> <h4>Please enter your comments:</h4> <textarea class=\"form-control textarea_style1\" name=\"comment\" ng-model=\"bid.comment\" placeholder=\"comment...\"></textarea> <!-- <div class=\"errormsg\" ng-if=\"(bidsubmitForm.$submitted || bidsubmitForm.comment.$dirty) && bidsubmitForm.comment.$invalid\">\n" +
    "                    <p ng-if=\"bidsubmitForm.comment.$error.required\">Please enter your comments</p>\n" +
    "                    <p ng-if=\"bidsubmitForm.comment.$error.pattern\">Must be Alphanumeric</p>\n" +
    "                </div> --> </div> <input type=\"submit\" class=\"hvr-wobble-horizontal login_button\" value=\"Submit Bid\"> </form> </div> <style>.bootbox .modal-dialog {\n" +
    "    width: 500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "    .bootbox .modal-dialog {\n" +
    "        width: auto;\n" +
    "    }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/job/templates/confirmjob.html',
    "<!-- <h3 class=\"title_modal \">Sent message</h3> --><!-- <h2 class=\"text-left almost_there\">Confirm booking</h2> --> <h4 class=\"id_section\"><span>Job ID:</span> {{quoteobj.jobid}}</h4> <p class=\"payment_text\">In order to confirm booking, you need to transfer the required funds to our secured refundable holding account. We will transfer payments to workers only when they complete their job and after receiving your approval to release payment. In an unlikely scenario, if the worker does not deliver any services, we will refund your payment in full. You are in full control of your money. You will be transferred to our online payment services provider shortly. Please do not refresh or close this page.</p> <!-- user and job info here --> <!-- <div>\n" +
    "  {{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}  ID :{{quoteobj.userid__id}}<br>\n" +
    "  JobID : {{quoteobj.jobid}}\n" +
    "  Amount : ${{quoteobj.quote}}\n" +
    "</div> --> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"row\"> <div class=\"col-sm-8 col-xs-12 custom_width\"> <div class=\"row\"> <div class=\"col-md-3 col-sm-3\"> <a href=\"#\" class=\"\"> <img class=\"img-circle\" src=\"images/profilepic/{{quoteobj.userid__profpic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\" width=\"80\"> </a> </div> <div class=\"col-md-9 col-sm-9\"> <h4 class=\"quote_confirm_name\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4> <h4 class=\"id_section\"><span>User ID:</span> {{quoteobj.userid__id}}</h4> </div> </div> <!-- <a href=\"#\" class=\"pull-left p-r-10\">\n" +
    "            <img class=\"img-circle pull-right\" src=\"styles/images/user.jpg\" alt=\"\" width=\"80\">\n" +
    "        </a>\n" +
    "\n" +
    "        <h4 class=\"quote_confirm_name\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4>\n" +
    "        <h4 class=\"id_section \"><span>User ID:</span> {{quoteobj.userid__id}}</h4> --> </div> <div class=\"col-sm-4 col-xs-12 m-t-10 custom_width\"> <h5 class=\"media-heading ng-binding\">Amount</h5> <h4 class=\"id_section fs-26 m-t-0\">${{quoteobj.funds_transfered | number : 2}}</h4> </div> <div class=\"col-md-12\"> <button data-bb-handler=\"success\" type=\"button\" class=\"btn btn btn-primary signupbtn pull-right\" ng-click=\"paymentSuccess()\">OK</button> </div> </div> <!-- <div class=\"\">\n" +
    "      <a href=\"#\">\n" +
    "          <img class=\"img-circle\" src=\"styles/images/user.jpg\" alt=\"\" width=\"60\">\n" +
    "      </a>\n" +
    "    </div>\n" +
    "\n" +
    "    <h4 class=\"quote_confirm_name\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4>\n" +
    "\n" +
    "    <h4 class=\"id_section\"><span>User ID:</span> {{quoteobj.userid__id}}</h4>\n" +
    "    <h4 class=\"id_section\"><span>Job ID:</span> {{quoteobj.jobid}}</h4>\n" +
    "      <h4 class=\"id_section fs-26 pull-right\"><span>Amount:</span>${{quoteobj.funds_transfered | number : 2}}</h4>\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"col-sm-12\">\n" +
    "        <button data-bb-handler=\"success\" type=\"button\" class=\"btn btn btn-primary signupbtn pull-right\" ng-click=\"paymentSuccess()\">OK</button>\n" +
    "\n" +
    "      </div>\n" +
    "    </div> --> <!-- <div class=\"price_container\">\n" +
    "    <div class=\"rating_popup\">\n" +
    "      <div class=\"user_rating\">\n" +
    "                    <div class=\"media\">\n" +
    "                        <div class=\"media-left media-middle\">\n" +
    "                            <a href=\"#\">\n" +
    "                                <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\">\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"media-body\">\n" +
    "                            <h4 class=\"media-heading ng-binding\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4>\n" +
    "\n" +
    "                          <h4 class=\"id_section\">ID: {{quoteobj.userid__id}}</h4>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "    </div>\n" +
    "    <div class=\"jobid_popup\">\n" +
    "      <h4 class=\"media-heading ng-binding\">Job ID</h4>\n" +
    "\n" +
    "    <h4 class=\"id_section\">ID: {{quoteobj.jobid}}</h4>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"amount_popup\">\n" +
    "      <h4 class=\"media-heading ng-binding\">Amount</h4>\n" +
    "\n" +
    "    <h4 class=\"id_section fs-26\">${{quoteobj.funds_transfered | number : 2}}</h4>\n" +
    "    </div>\n" +
    "    <div class=\"action_popup\">\n" +
    "      <button data-bb-handler=\"success\" type=\"button\" class=\"btn btn btn-primary signupbtn\" ng-click=\"paymentSuccess()\">OK</button>\n" +
    "    </div>\n" +
    "    </div> --> </div> </div> <style>.bootbox .modal-header {\n" +
    "  padding:10px;\n" +
    "\tborder:none;\n" +
    "  background: #4e4e4e;\n" +
    "}\n" +
    ".modal-content{\n" +
    "  border:none !important;\n" +
    "}\n" +
    ".modal-content .modal-header .modal-title{\n" +
    "  color:#fff !important;\n" +
    "  font-family: 'Conv_comic';\n" +
    "  font-size:20px;\n" +
    "  text-transform:capitalize;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/job/templates/feedback_form.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\"> <form name=\"feedbackForm\"> <div class=\"postjob_type_section\"> <div class=\"form-group\"> <h4>Please rate the carer</h4> <span class=\"ratingstars\"><input-stars max=\"5\" ng-attr-readonly=\"{{disableFeedback}}\" name=\"star\" ng-model=\"feedback.star\" allow-half ng-required=\"true\"></input-stars></span> <div class=\"errormsg\" ng-if=\"(feedbackForm.$submitted || feedbackForm.password.$dirty) &&feedbackForm.star.$invalid\"> <p ng-show=\"feedbackForm.star.$error.required\">Your rating is required</p> </div> </div> <div class=\"form-group\"> <h4>Please provide your review feedback about the carer</h4> <span><textarea class=\"form-control post_forum_textarea\" height=\"100\" name=\"review\" ng-model=\"feedback.review\" ng-readonly=\"disableFeedback\" ng-required=\"true\"></textarea></span> <div class=\"errormsg\" ng-if=\"feedbackForm.$submitted && feedbackForm.review.$invalid\"> <p ng-if=\"feedbackForm.review.$error.required\">Please enter your review</p> </div> </div> </div> </form> </p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    ".ratingstars ul li .fa-star-o {\n" +
    "    c\n" +
    "}\n" +
    ".ratingstars .angular-input-stars > li >i,\n" +
    ".ratingstars .angular-input-stars > li >i:hover{\n" +
    "\tcolor: #000000;\n" +
    "}\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/job/templates/have_confirm_job.html',
    "<!-- <h3 class=\"title_modal \">Sent message</h3> --><!-- <h2 class=\"text-left almost_there\">Confirm booking</h2> --> <p class=\"payment_text\">{{popupContent}}</p> <!-- user and job info here --> <!-- <div>\n" +
    "  {{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}  ID :{{quoteobj.userid__id}}<br>\n" +
    "  JobID : {{quoteobj.jobid}}\n" +
    "  Amount : ${{quoteobj.quote}}\n" +
    "</div> --> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"price_container\"> <!-- <div class=\"rating_popup\">\n" +
    "      <div class=\"user_rating\">\n" +
    "                    <div class=\"media\">\n" +
    "                        <div class=\"media-left media-middle\">\n" +
    "                            <a href=\"#\">\n" +
    "                                <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\">\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"media-body\">\n" +
    "                            <h4 class=\"media-heading ng-binding\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4>\n" +
    "                            <jk-rating-stars rating=\"secondRate\" read-only=\"readOnly\" ></jk-rating-stars>\n" +
    "                          <h4 class=\"id_section\">ID: {{quoteobj.userid__id}}</h4>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "    </div> --> <div class=\"action_popup text-right\"> <!-- <button data-bb-handler=\"success\" type=\"button\" class=\"btn btn btn-primary signupbtn\" ng-click=\"gotoWorkerTab();job_status=2;\">OK</button> --> </div> </div> </div> </div> <style>.bootbox .modal-header {\n" +
    "  padding:10px;\n" +
    "\tborder:none;\n" +
    "  background: #4e4e4e;\n" +
    "}\n" +
    ".modal-content{\n" +
    "  border:none !important;\n" +
    "}\n" +
    ".modal-content .modal-header .modal-title{\n" +
    "  color:#fff !important;\n" +
    "  font-family: 'Conv_comic';\n" +
    "  font-size:20px;\n" +
    "  text-transform:capitalize;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/job/templates/job_complete_popup.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">Are you sure you want to mark this job as completed?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/job/templates/paymentsuccess.html',
    "<!-- <h3 class=\"title_modal \">Sent message</h3> --><!-- <h2 class=\"text-center almost_there\">Payment success</h2> --> <p class=\"payment_text\"> Thanks for transferring the required funds to our secured holding account. We will notify the worker shortly. Please feel free to contact the worker directly, if required. You are redirected to our platform to view updates to your job. </p> <!-- user and job info here --> <!-- <div>\n" +
    "  {{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}<br>\n" +
    "  ID :{{quoteobj.userid__id}} <br>\n" +
    "  {{quoteobj.userid__email}}\n" +
    "  {{quoteobj.userid__contactno}}\n" +
    "</div> --> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"container_success_pay\"> <div class=\"first_section success_container\"> <div class=\"user_rating\"> <div class=\"media\"> <div class=\"media-left media-top\"> <a href=\"#\"> <img class=\"media-object\" src=\"images/profilepic/{{quoteobj.userid__profpic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\"> </a> </div> <div class=\"media-body\"> <h4 class=\"media-heading\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4> <h4 class=\"id_section1\">Worker ID :{{quoteobj.userid__id}}</h4> <h5 class=\"id_section1\">{{quoteobj.userid__email}}</h5> <h5 class=\"id_section1\">{{quoteobj.userid__contactno}}</h5> </div> </div> </div> <!-- <h4 class=\"media-heading\">{{quoteobj.userid__first_name}} {{quoteobj.userid__last_name}}</h4>\n" +
    "        <h4 class=\"id_section \">ID :{{quoteobj.userid__id}}</h4>\n" +
    "        <h5 class=\"id_section1 \">{{quoteobj.userid__email}}</h5>\n" +
    "        <h5 class=\"id_section1 \">{{quoteobj.userid__contactno}}</h5> --> </div> <div class=\"second_section action_popup\"> <button data-bb-handler=\"success\" type=\"button\" class=\"btn btn btn-primary signupbtn\" ng-click=\"closePopup()\">OK</button> </div> </div> </div> </div> <style>.bootbox .modal-header {\n" +
    "  padding:10px;\n" +
    "\tborder:none;\n" +
    "  background: #4e4e4e;\n" +
    "}\n" +
    ".modal-content{\n" +
    "  border:none !important;\n" +
    "}\n" +
    ".modal-content .modal-header .modal-title{\n" +
    "  color:#fff !important;\n" +
    "  font-family: 'Conv_comic';\n" +
    "  font-size:20px;\n" +
    "  text-transform:capitalize;\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/job/templates/release_payment_client.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">Are you sure you want to release payment to the carer?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/job/templates/sentmsg.html',
    "<div class=\"modal-body\"> <form novalidate> <div class=\"radio_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"form-group clearfix postjob_requirement sent_message_select sent_message_selectnew\"> <label class=\"control-label col-sm-8 col-xs-8 sent_msg_type\"><i class=\"pull-left\"></i><span class=\"reqvalue\">Please select the message type</span></label> <div class=\"col-sm-4 col-xs-4 for_res_tc text-right\"> <div> <label class=\"switch\"> <input type=\"checkbox\" name=\"messagetype\" ng-model=\"messagetype\" ng-true-value=\"'public'\" ng-false-value=\"'private'\" ng-disabled=\"currentName == 'parentjoblist' || currentName == 'joblist'\n" +
    "\" ng-checked=\"messagetype == 'public'\" class=\"switch-input\"> <span class=\"switch-label\" data-on=\"Public\" data-off=\"Private\"></span> <span class=\"switch-handle\"></span> </label> </div> </div> </div> </div> </div> </div> <div class=\"form-group postjob_type_section\"> <h4>Please enter your message</h4> <textarea class=\"form-control textarea_style1\" ng-model=\"message\" placeholder=\"Message...\"></textarea> </div> <a href=\"javascript:void(0);\" class=\"hvr-wobble-horizontal login_button\" ng-click=\"sendMessage(toId,messageJobid)\">Send message</a> </form> </div> <style>.bootbox .modal-dialog {\n" +
    "   width: 500px;\n" +
    "   }\n" +
    "   /* @media screen and (max-width: 767px) {\n" +
    "   .bootbox .modal-dialog {\n" +
    "   width: auto;\n" +
    "   }\n" +
    "   } */\n" +
    "   .switch {\n" +
    "   position: relative;\n" +
    "   display: inline-block;\n" +
    "   vertical-align: top;\n" +
    "   width: 110px;\n" +
    "   height: 24px;\n" +
    "   padding: 3px;\n" +
    "   background-color: transparent;\n" +
    "   font-family: 'Conv_Hel_lt_normal';\n" +
    "   border-radius: 18px;\n" +
    "   cursor: pointer;\n" +
    "   }\n" +
    "   .switch-input {\n" +
    "   position: absolute;\n" +
    "   top: 0;\n" +
    "   left: 0;\n" +
    "   opacity: 0;\n" +
    "   }\n" +
    "   .switch-label {\n" +
    "   position: relative;\n" +
    "   display: block;\n" +
    "   height: inherit;\n" +
    "   font-size: 10px;\n" +
    "   background: #eceeef;\n" +
    "   border-radius: inherit;\n" +
    "   box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);\n" +
    "   -webkit-transition: 0.15s ease-out;\n" +
    "   -moz-transition: 0.15s ease-out;\n" +
    "   -o-transition: 0.15s ease-out;\n" +
    "   transition: 0.15s ease-out;\n" +
    "   -webkit-transition-property: opacity background;\n" +
    "   -moz-transition-property: opacity background;\n" +
    "   -o-transition-property: opacity background;\n" +
    "   transition-property: opacity background;\n" +
    "   }\n" +
    "   .switch-label:before,\n" +
    "   .switch-label:after {\n" +
    "   position: absolute;\n" +
    "   top: 50%;\n" +
    "   margin-top: -4px;\n" +
    "   line-height: 1;\n" +
    "   -webkit-transition: inherit;\n" +
    "   -moz-transition: inherit;\n" +
    "   -o-transition: inherit;\n" +
    "   transition: inherit;\n" +
    "   }\n" +
    "   .switch-label:before {\n" +
    "   content: attr(data-off);\n" +
    "   right: 11px;\n" +
    "   color: #7e7e7e;\n" +
    "   text-shadow: 0 1px rgba(255, 255, 255, 0.5);\n" +
    "   text-transform: capitalize;\n" +
    "   }\n" +
    "   .switch-label:after {\n" +
    "   content: attr(data-on);\n" +
    "   left: 11px;\n" +
    "   color: white;\n" +
    "   /*text-shadow: 0 1px rgba(0, 0, 0, 0.2);*/\n" +
    "   opacity: 0;\n" +
    "   text-transform: capitalize;\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label {\n" +
    "   background: #fe6601;\n" +
    "   /*box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);*/\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label:before {\n" +
    "   opacity: 0;\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-label:after {\n" +
    "   opacity: 1;\n" +
    "   }\n" +
    "   .switch-handle {\n" +
    "   position: absolute;\n" +
    "   top: 6px;\n" +
    "   left: 6px;\n" +
    "   width: 18px;\n" +
    "   height: 18px;\n" +
    "   background: white;\n" +
    "   border-radius: 10px;\n" +
    "   box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "   background-image: -webkit-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: -moz-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: -o-linear-gradient(top, white 40%, #f0f0f0);\n" +
    "   background-image: linear-gradient(to bottom, white 40%, #f0f0f0);\n" +
    "   -webkit-transition: left 0.15s ease-out;\n" +
    "   -moz-transition: left 0.15s ease-out;\n" +
    "   -o-transition: left 0.15s ease-out;\n" +
    "   transition: left 0.15s ease-out;\n" +
    "   /*border:2px solid #dedede;*/\n" +
    "   }\n" +
    "   .switch-handle:before {\n" +
    "   content: '';\n" +
    "   position: absolute;\n" +
    "   top: 50%;\n" +
    "   left: 50%;\n" +
    "   margin: -6px 0 0 -6px;\n" +
    "   width: 12px;\n" +
    "   height: 12px;\n" +
    "   background: #f9f9f9;\n" +
    "   border-radius: 6px;\n" +
    "   box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);\n" +
    "   background-image: -webkit-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: -moz-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: -o-linear-gradient(top, #eeeeee, white);\n" +
    "   background-image: linear-gradient(to bottom, #eeeeee, white);\n" +
    "   }\n" +
    "   .switch-input:checked ~ .switch-handle {\n" +
    "   left: 80px;\n" +
    "   box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);\n" +
    "   }\n" +
    "   .switch-green > .switch-input:checked ~ .switch-label {\n" +
    "   background: #fe6601;\n" +
    "   }\n" +
    "   .custom_chosen .chosen-single div b {\n" +
    "   display: none !important;\n" +
    "   }\n" +
    "   .sent_msg_type\n" +
    "   {\n" +
    "   padding-left:15px;\n" +
    "   margin-top:5px;\n" +
    "   font-size:16px;\n" +
    "   color:#232323;\n" +
    "   font-family: 'Conv_helvetica_normal';\n" +
    "   }\n" +
    "   .sent_message_select\n" +
    "   {\n" +
    "   margin-top:20px;\n" +
    "   margin-bottom:5px;\n" +
    "   }</style>"
  );


  $templateCache.put('scripts/module/job/templates/startupMsg.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\" ng-bind-html=\"startMessage\"></p> </div>"
  );


  $templateCache.put('scripts/module/job/views/joblist.html',
    "<!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> -->  <div class=\"upToScroll buttonShow\" ng-if=\"isScrolled > 0\"> <a class=\"scrollToTop icon-up-open-big\" title=\"Back to top\" ng-click=\"scrollTopHeader();\"> </a> </div> <div header ng-if=\"!adminLogged\"></div> <section id=\"job_list_section\" x-sticky-boundary=\"\"> <div class=\"container-fluid\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"first_row clearfix\"> <div class=\"col-sm-3\"> <!-- <h3 class=\"title_joblist\">My Jobs  <span class=\"fa fa-filter pull-right toggle_icon_pink\" ng-click=\"state=!state\" data-ng-init=\"state=true\" uib-tooltip=\"close filter\"></span></h3> --> <h3 class=\"title_joblist\">My Jobs <!-- <span class=\"link_filter\" ng-click=\"state=!state\" data-ng-init=\"state=false\" uib-tooltip=\"close filter\">\n" +
    "                <i class=\"fa fa-filter filter_toggle\"></i></span> --> </h3> </div> <div uib-alert ng-repeat=\"alert in userSuggestions\" close=\"closeSuggestions($index)\" class=\"uib-alert-design alert-warning\"> <!--  {{alert.name}}  {{alert.msg}} --> <h4>App suggestions</h4> <!-- <h4 ng-if=\"alert.type=='success'\">Payment Success</h4> --> <!-- <p class=\"alert_name\">{{alert.name}}</p> --> <p class=\"alert_msg\">{{alert.message}}</p> </div> <div class=\"col-sm-9 top_tab_material\"> <ul class=\"nav nav-tabs\" role=\"tablist\"> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'available'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(0, 'available')\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Available <span class=\"badge orange_badge\">{{status_counts.available_count}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'applied'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(1, 'applied')\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Applied <span class=\"badge orange_badge\" ng-if=\"status_counts.applied_count__sum != 0\">{{status_counts.applied_count__sum}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'confirmed'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(2, 'confirmed')\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Confirmed <span class=\"badge orange_badge\" ng-if=\"status_counts.confirmed_count__sum != 0\">{{status_counts.confirmed_count__sum}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'completed'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(3, 'completed')\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Completed </a></li> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'paid'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(4, 'paid')\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Paid <span class=\"badge orange_badge\" ng-if=\"status_counts.paid_count__sum != 0\">{{status_counts.paid_count__sum}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':currentTab == 'closed'}\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(7, 'closed')\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Closed</a></li> <!-- <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(-1);currentTab='all'\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">All</a></li> --> </ul> </div> </div> </div> <!-- filter and main contain row --> <div class=\"row toggleclass_side\" sidebar-directive=\"state\" window-dir> <div class=\"col-sm-3 left_side_filter_section\" ng-if=\"currentTab == 'available'\"> <h3 class=\"filter_area\"> <!-- <i class=\"fa fa-filter\"></i> --> <img src=\"styles/images/filters-icon.png\" alt=\"\" width=\"20\" class=\"fa-filters\" ng-click=\"toggleState()\"> <span class=\"hide_section\">Filters</span> <span class=\"clear_all_filter\" ng-click=\"clearAllfilters()\"><span class=\"clear_label_filter\">Clear all </span><i class=\"leftarrow\" title=\"{{toggle_lable}}\" ng-class=\"{'icon-arrow-left-circle':state,'icon-arrow-right-circle':!state}\" ng-click=\"toggleState()\"></i></span> <!-- <i class=\"fa fa-caret-left  leftarrow\" data-ng-init=\"state=true\" ng-click=\"state=!state\"></i> --> <!-- <i class=\"leftarrow\" ng-if=\"state\" uib-tooltip=\"close filter\" ng-class=\"{'fa fa-chevron-circle-left':state,'fa fa-chevron-circle-right':!state}\" data-ng-init=\"state=true\" ng-click=\"state=!state\" ></i> --> </h3> <div class=\"display_none_section\"> <v-accordion class=\"vAccordion--default\" multiple> <!-- add expanded attribute to open the section --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('jobtype')\">clear filter</span> <v-pane-header> Type of job </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> Casual / One-off care <input type=\"checkbox\" value=\"0\" ng-click=\"toggleCaretype(0)\" ng-checked=\"selectedJobtype.indexOf(0) > -1\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> Regular care <input type=\"checkbox\" value=\"1\" ng-click=\"toggleCaretype(1)\" ng-checked=\"selectedJobtype.indexOf(1) > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('caregiver')\">clear filter</span> <v-pane-header> Type of caregiver </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"cg in caregivers\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"selectedCaregiver.indexOf(cg) > -1\" ng-click=\"toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> <!-- <label class=\"control control--checkbox\">\n" +
    "                        Baby sitter\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Nanny\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Au pair\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Tutor / educator\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                       Cook\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label> --> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('caretime')\">clear filter</span> <v-pane-header> Type of care </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> Night time care <input type=\"checkbox\" value=\"night\" ng-checked=\"selectedCaretime.indexOf('night') > -1\" ng-click=\"toggleCaretime('night')\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> Day time care <input type=\"checkbox\" value=\"day\" ng-checked=\"selectedCaretime.indexOf('day') > -1\" ng-click=\"toggleCaretime('day')\"> <div class=\"control__indicator\"></div> </label> <div class=\"sub_checkbox_section\" ng-if=\"selectedCaretime.indexOf('day') > -1\"> <div class=\"check_box_section\"> <label class=\"control control--checkbox\"> Before school care <input type=\"checkbox\" value=\"Before school care\" ng-checked=\"selectedSession.indexOf('Before school care') > -1\" ng-click=\"toggleSession('Before school care')\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> After school care <input type=\"checkbox\" value=\"After school care\" ng-checked=\"selectedSession.indexOf('After school care') > -1\" ng-click=\"toggleSession('After school care')\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane expanded> <span class=\"clear_link_filter\" ng-click=\"clearfilter('req')\"> clear filter</span> <v-pane-header> Requirements </v-pane-header> <v-pane-content> <div ng-repeat=\"reqCategory in reqsToFilter() | filter:filterReqs\"> <p class=\"requirement_category_name\">{{reqCategory.category}}</p> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"req in requirements | filter:{category: reqCategory.category}\"> {{req.requirement}} <input type=\"checkbox\" value=\"{{req.id}}\" ng-checked=\"selectedreq.indexOf(req.id) > -1\" ng-click=\"togglereq(req.id)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </v-pane-content> </v-pane> </v-accordion> </div> </div> <div class=\"col-sm-9 right_side_cen\" ng-class=\"{'job_full_width': currentTab != 'available'}\" infinite-scroll=\"loadPages()\" infinite-scroll-distance=\"0\" infinite-scroll-parent=\"false\" infinite-scroll-disabled=\"jobcanCalled\"> <!--search  --> <div class=\"search_section_joblist clearfix plugin_dropdown_country\" ng-if=\"currentTab == 'available'\"> <div class=\"col-sm-7 pad-0 search_input\"> <input type=\"text\" class=\"form-control\" ng-model=\"address1.fullAddress\" name=\"tag\" class=\"form-control text-box\" uib-typeahead=\"location as location.viewval for location in filteredLocations\" typeahead-wait-ms=\"600\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"setTag($item,true)\" ng-keyup=\"getLocations(address1.fullAddress)\"> <img src=\"styles/images/search.png\" alt=\"\" class=\"search_icon\"> <span class=\"ajax_loader\" ng-if=\"locationLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> </div> <div class=\"col-sm-3 pad-0 search_input\"> <input type=\"text\" uib-datepicker-popup=\"{{format}}\" ng-model=\"address1.date\" ng-click=\"status.opened = !status.opened\" is-open=\"status.opened\" min-date=\"minDate\" max-date=\"maxDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" ng-required=\"true\" close-text=\"Close\" placeholder=\"DD/MM/YYYY\" class=\"form-control\" ng-readonly=\"true\"> <img src=\"styles/images/date_ico.png\" alt=\"\" class=\"search_icon\" ng-click=\"open($event)\"> </div> <div class=\"col-sm-2 pad-r-0\"> <button class=\"btn btn-success search_btn\" ng-click=\"searchbyLocation()\">Search</button> </div> </div> <div ng-if=\"joblist.length == 0 && !isLoading && currentTab != 'available' \"> <h1 class=\"main_heading_nojob\"><i class=\"fa fa-frown-o\"></i></h1> <h1 class=\"main_heading_nojob p-t-0\">No Jobs found!!!</h1></div> <div class=\"no_job_worker\" ng-if=\"currentTab == 'available' && joblist.length == 0 && !isLoading\"> <h1 class=\"main_heading_nojob\"> <i class=\"fa fa-frown-o\"></i> </h1> <p>Sorry! Currently there is no availble jobs.</p><p>Once a new job posted by client we will sent you a notification.</p> </div> <!-- single job list --> <div class=\"single_joblist\" ng-repeat=\"job in joblist\" ng-class=\"{'unsuccessful_job':job.unsuccessful == 1}\"> <div class=\"shape\" ng-if=\"job.quoted == 1 && job.is_confirmed == 0 && job.unsuccessful == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 2 && job.unsuccessful == 0\"> <div class=\"shape-text1\"> Confirmed </div> </div> <div class=\"shape1\" ng-if=\"job_status == 3 && job.unsuccessful == 0\"> <div class=\"shape-text1 w_complt\"> Completed </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 4 && job.unsuccessful == 0\"> <div class=\"shape-text1 paid_txt\"> Paid </div> </div> <div class=\"shape1\" ng-if=\"job.unsuccessful == 1\"> <div class=\"shape-text1 w_complt\"> Unapproved </div> </div> <div class=\"shape2\" ng-if=\"job.job_status == 7\"> <div class=\"shape-text1 paid_txt\"> Closed </div> </div> <p class=\"job_id\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\">{{job.caregiver_type}} <!-- <span class=\"approved_label\">Approved</span> --> </h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / one-off care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <!-- <h5 ng-if=\"job.care_type.indexOf('night') > -1\" class=\"care_type\">Night time care</h5> --> <h4 class=\"caregiver_name\">{{job.care_name}}</h4> <!-- label section --> <!-- <div class=\"label_section_tags\">\n" +
    "                    <ul>\n" +
    "                      <li><span class=\"label label-default\">Night time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Day time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Before school care</span></li>\n" +
    "                    </ul>\n" +
    "                  </div> --> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-nightcare_gray pull-left\"></i><span class=\"p-l-5\">Night time care</span></span> </li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-daycare_gray pull-left\"></i><span class=\"p-l-5\">Day time care</span></span> </li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-beforeschool_gray pull-left\"></i><span class=\"p-l-5\">Before school care</span></span> </li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-afterschool_gray pull-left\"></i><span class=\"p-l-5\">After school care</span></span> </li> </ul> </div> <!-- end label section --> <!-- location section --> <div class=\"location_section\" ng-show=\"job.post_code\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span> {{job.city}}, {{job.state}} {{job.post_code}}</span> </div> <!-- End location section --> </div> <!-- end left detils section--> <div class=\"right_btn_section\" ng-if=\"job.unsuccessful == 0\"> <div class=\"text-center\" ng-if=\"job_status == 0\"> <h5 class=\"budget_label\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> <div class=\"apply_btn text-center\"> <button class=\"btn btn-default\" ng-click=\"submitBid(job.jobid,job.time_budget)\">Submit Bid</button> </div> </div> <div class=\"text-center\" ng-if=\"job.quoted == 1 && job.is_confirmed == 0\"> <h5 class=\"budget_label\">My quote</h5> <h4 class=\"text-center salary_job\">${{job.bid_info.quote}}</h4> </div> <div class=\"text-center\" ng-if=\"job.job_status == 2 || job.job_status == 3 || job.job_status == 4\"> <h5 class=\"budget_label\" ng-if=\"job.job_status != 4\">Funds available</h5> <h5 class=\"budget_label bl_payment\" ng-if=\"job.job_status == 4\">Payment released</h5> <h4 class=\"text-center salary_job\">${{job.bid_info.quote}}</h4> <div class=\"apply_btn text-center\" ng-if=\"job.job_status == 2\"> <button class=\"btn btn-default\" ng-click=\"markAscompleted(job.jobid)\">Mark as completed</button> </div> <div class=\"apply_btn text-center\" ng-if=\"job.job_status == 4\"> <!-- <button class=\"btn btn-default\" ng-click=\"getFeedback(job.jobid,'worker')\">Client Feedback</button> --> <button class=\"btn btn-default\" ng-if=\"!job.has_review_given\" ng-click=\"openFeedbackpopup(job.jobid,job.user_id,'worker',job)\">Rate client</button> <button class=\"btn btn-default\" ng-if=\"job.has_review_given\" ng-click=\"getFeedback(job.jobid,'client')\">My Feedback</button> </div> </div> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container rating_containernew_res\"> <div class=\"user_rating\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <a href=\"javascript:void(0)\"> <span ng-if=\"job.client_profilepic != ''\"> <img class=\"media-object\" ng-src=\"images/profilepic/{{job.client_profilepic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\"> </span> <span ng-if=\"job.client_profilepic == ''\"> <img class=\"media-object\" ng-src=\"/images/no_image_1.png\" alt=\"\"> </span> </a> </div> </div> </div> <div class=\"date_section\"> <div class=\"date_section_inner\"> <div class=\"user_rating user_ratinginner\"> <div class=\"media\"> <div class=\"media-body\"> <h4 class=\"media-heading\" title=\"{{job.user_name}}\">{{job.user_name | cut:true:20:' ...'}}</h4> <!-- <jk-rating-stars rating=\"secondRate\" read-only=\"readOnly\" ></jk-rating-stars> --> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half ng-attr-readonly=\"true\"></input-stars> </div> </div> </div> <div class=\"midtablecntt\"> <div class=\"day_selector\" ng-if=\"job.job_type == 1\"> <ul> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li> </ul> </div> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> <div class=\"job_time_date\"> <ul> <li><span class=\"datetime_job_list\"><i class=\"fa fa-calendar\"></i>{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></li> <li><span class=\"datetime_job_list\"><i class=\"fa fa-clock-o\"></i>{{job.time_budget.time | date:\"HH:MM\"}}</span></li> </ul> </div> </div> <div class=\"view_detail_sec\" ng-if=\"currentTab == 'available'\"> <div class=\"pull-right text-center detail_job_link\"> <a href=\"javascript:void(0)\" ng-click=\"makeaJobview(job.jobid)\" class=\"view_job_link\" data-toggle=\"collapse\" data-target=\"#demo{{$index}}\">View job details<i class=\"fa fa-angle-down\"></i></a> </div> </div> <div class=\"view_detail_sec\" ng-if=\"currentTab != 'available' && job.unsuccessful == 0\"> <div class=\"pull-right text-center detail_job_link\"> <a href=\"#/worker/previewjob/{{job.jobid}}/jobdetails\" class=\"btn btn-xs btn-warning view_job_btn\">View job</a> </div> </div> </div> </div> </div> </div> <!-- end single job list --> <!-- view job details --> <div id=\"demo{{$index}}\" class=\"collapse\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"rate_section\"> <div class=\"col-md-7\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-5 rightside_boot_panel\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\">Rate per hour<span>${{job.time_budget.rate_per_hour}}</span></div> <div class=\"panel-body\">Number of hours<span>{{job.time_budget.hours_required}}</span></div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"job_detail_heading\">Requirements</h3> <div class=\"tag_section\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><b class=\"req_list_b\">{{x.requirement}}</b></span></li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h2 class=\"line_section\"> <span class=\"line-center\">Other Details </span> </h2> <div class=\"otherdetail_content\"> <p ng-bind-html=\"job.other_requirement\"></p> </div> </div> </div> <div class=\"col-md-12 hide\"> <div class=\"other_details m-t-20\"> <h3 class=\"job_detail_heading\">Job owner notes</h3> <!-- <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p>\n" +
    "                  <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</p>\n" +
    "                  <p>- He/She should understand the<span> UI/UX considerations.</span></p> --> <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p> <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</span></span></p> <p>- He/She should understand the<span> UI/UX considerations.</span></p> </div> </div> </div> </div> <div class=\"footer_job_detail clearfix\"> <!-- <div class=\"col-md-12\">\n" +
    "                <ul class=\"main_footer_section\">\n" +
    "                  <li>JOB VIEWS <span>256</span></li>\n" +
    "                  <li>JOB APPLICANTS <span>90</span></li>\n" +
    "                </ul>\n" +
    "              </div> --> <div class=\"row\"> <div class=\"full_bottom clearfix\"> <div class=\"col-md-12 text-center\"> <ul class=\"left_side_detail_ul left_side_detail_ulnew\"> <li>JOB VIEWS <span class=\"badge\">{{job.job_view_count}}</span></li> <li>JOB APPLICANTS <span class=\"badge\">{{job.bidded_users}}</span></li> </ul> <ul class=\"right_side_detail_ul right_side_detail_ulnew\"> <li><a href=\"#/worker/previewjob/{{job.jobid}}/messages\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-envelope-open\"></span> View Message</a></li> <li><a href=\"javascript:void(0)\" class=\"btn btn-xs btn-warning\" ng-click=\"sentMessage(job.user_id,job.jobid)\"><span class=\"fa fa-paper-plane\"></span> Send Message</a></li> <!-- <li ng-if=\"job_status != 1 && ! job.is_bidded\"><a href=\"javascript:void(0)\" class=\"btn btn-xs btn-warning\" ng-click=\"submitBid()\"><span class=\"fa fa-check-circle\"></span> Submit Bid</a></li> --> </ul> </div> </div> <!-- <div class=\"col-sm-6\">\n" +
    "                  <ul class=\"main_footer_section\">\n" +
    "                    <li>JOB VIEWS <span>256</span></li>\n" +
    "                    <li>JOB APPLICANTS <span>90</span></li>\n" +
    "                  </ul>\n" +
    "\n" +
    "                </div> --> </div> </div> </div> </div> <div class=\"text-center\" ng-if=\"isLoading\"> <div id=\"loadFacebookG\"> <div id=\"blockG_1\" class=\"facebook_blockG\"></div> <div id=\"blockG_2\" class=\"facebook_blockG\"></div> <div id=\"blockG_3\" class=\"facebook_blockG\"></div> </div> <p class=\"text-center loader_text\">Loading...</p> </div> </div> <!-- Loader --> <!-- <div class=\"text-center\"> --> <!-- <img src=\"../../../images/30.gif\" ng-if=\"isLoading\"/> --> <!-- </div> --> <!-- end of filter and main contain row --> </div> </div> <div class=\"col-sm-3 hidden-sm hidden-xs\"> <!-- <div class=\"row\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "\n" +
    "                </div>\n" +
    "              </div> --> <div class=\"my_profile_view clearfix m-t-30\" ng-if=\"userPercent < 80\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">profile Completeness&nbsp;<span class=\"profile_complete_percent\">{{userPercent}}%</span></h2> </div> <div class=\"complete_progress\"> <!-- <h5>Profile complete on {{profileCompleteness}}%</h5> --> <div class=\"progress\"> <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"{{userPercent}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" ng-style=\"{ 'width': (userPercent+'%')}\"> </div> </div> </div> </div> <div class=\"right_side_section_joblist\"> <!-- <div class=\"ad_section\">\n" +
    "                        <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "                    </div> --> <div x-sticky=\"\" set-class-when-at-top=\"ad-fixed\"> <div ad2></div> </div> </div> </div> </div> </div></section> <!-- footer section comes here --> <div footer ng-if=\"!adminLogged\"></div>"
  );


  $templateCache.put('scripts/module/job/views/joblist_old.html',
    "<div header></div> <section id=\"job_list_section\"> <div class=\"container-fluid\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"first_row clearfix\"> <div class=\"col-sm-3\"> <!-- <h3 class=\"title_joblist\">My Jobs  <span class=\"fa fa-filter pull-right toggle_icon_pink\" ng-click=\"state=!state\" data-ng-init=\"state=true\" uib-tooltip=\"close filter\"></span></h3> --> <h3 class=\"title_joblist\">My Jobs <!-- <span class=\"link_filter\" ng-click=\"state=!state\" data-ng-init=\"state=false\" uib-tooltip=\"close filter\">\n" +
    "                <i class=\"fa fa-filter filter_toggle\"></i></span> --> </h3> </div> <div class=\"col-sm-9 top_tab_material\"> <ul class=\"nav nav-tabs\" role=\"tablist\"> <li role=\"presentation\" class=\"active\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(0)\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Available</a></li> <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(1)\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Applied</a></li> <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(1)\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Approved</a></li> <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(2)\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Completed</a></li> <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(3)\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Closed</a></li> <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(-1)\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">All</a></li> </ul> </div> </div> </div> <!-- filter and main contain row --> <div class=\"row toggleclass_side\" sidebar-directive=\"state\"> <div class=\"col-sm-3 left_side_filter_section\"> <h3 class=\"filter_area\"> <!-- <i class=\"fa fa-filter\"></i> --> <img src=\"styles/images/filters-icon.png\" alt=\"\" width=\"20\" class=\"fa-filters\"> <span class=\"hide_section\">Filters</span> <!-- <i class=\"fa fa-caret-left  leftarrow\" data-ng-init=\"state=true\" ng-click=\"state=!state\"></i> --> <i class=\"leftarrow\" uib-tooltip=\"Toggle filter \" ng-class=\"{'icon-arrow-left-circle':state,'icon-arrow-right-circle':!state}\" data-ng-init=\"state=true\" ng-click=\"state=!state\"></i> <!-- <i class=\"leftarrow\" ng-if=\"state\" uib-tooltip=\"close filter\" ng-class=\"{'fa fa-chevron-circle-left':state,'fa fa-chevron-circle-right':!state}\" data-ng-init=\"state=true\" ng-click=\"state=!state\" ></i> --> </h3> <div class=\"display_none_section\"> <v-accordion class=\"vAccordion--default\"> <!-- add expanded attribute to open the section --> <v-pane> <v-pane-header> Type of job </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> Casual / one off care <input type=\"checkbox\" value=\"0\" ng-click=\"toggleCaretype(0)\" ng-checked=\"selectedJobtype.indexOf(0) > -1\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> Regular care <input type=\"checkbox\" value=\"1\" ng-click=\"toggleCaretype(1)\" ng-checked=\"selectedJobtype.indexOf(1) > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane expanded> <v-pane-header> Type of caregiver </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"cg in caregivers\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"selectedCaregiver.indexOf(cg) > -1\" ng-click=\"toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> <!-- <label class=\"control control--checkbox\">\n" +
    "                        Baby sitter\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Nanny\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Au pair\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                        Tutor / educator\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label>\n" +
    "                     <label class=\"control control--checkbox\">\n" +
    "                       Cook\n" +
    "                        <input type=\"checkbox\"/>\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                     </label> --> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane> <v-pane-header> Type of care </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> Night time care <input type=\"checkbox\" value=\"night\" ng-checked=\"selectedCaretime.indexOf('night') > -1\" ng-click=\"toggleCaretime('night')\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> Day time care <input type=\"checkbox\" value=\"day\" ng-checked=\"selectedCaretime.indexOf('day') > -1\" ng-click=\"toggleCaretime('day')\"> <div class=\"control__indicator\"></div> </label> <div class=\"sub_checkbox_section\" ng-if=\"selectedCaretime.indexOf('day') > -1\"> <div class=\"check_box_section\"> <label class=\"control control--checkbox\"> Before school care <input type=\"checkbox\" value=\"Before school care\" ng-checked=\"selectedSession.indexOf('Before school care') > -1\" ng-click=\"toggleSession('Before school care')\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--checkbox\"> After school care <input type=\"checkbox\" value=\"After school care\" ng-checked=\"selectedSession.indexOf('After school care') > -1\" ng-click=\"toggleSession('After school care')\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> </v-pane-content> </v-pane> <!-- ============== --> <v-pane> <v-pane-header> Requirements </v-pane-header> <v-pane-content> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\" ng-repeat=\"req in requirements\"> {{req}} <input type=\"checkbox\" value=\"{{req}}\" ng-checked=\"selectedreq.indexOf(req) > -1\" ng-click=\"togglereq(req)\"> <div class=\"control__indicator\"></div> </label> <!-- <label class=\"control control--checkbox\">\n" +
    "                    Certificate II / III / IV in Children Services\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Diploma / Bachelors in Children Services\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    First Aid Trained\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Anaphylaxis Trained\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Police Clearance / Criminal Records Check\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Working with Children Check\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Proof of work experience\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                  Car / Driving\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Drivers License\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Independent Verification of the above\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Address Verified\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Identity Verified\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Verified Mobile Number\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Reference Check\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label>\n" +
    "                  <label class=\"control control--checkbox\">\n" +
    "                    Home Safety Check\n" +
    "                     <input type=\"checkbox\"/>\n" +
    "                     <div class=\"control__indicator\"></div>\n" +
    "                  </label> --> </div> </div> </v-pane-content> </v-pane> </v-accordion> </div> </div> <div class=\"col-sm-9 right_side_cen\" infinite-scroll=\"loadPages()\" infinite-scroll-distance=\"0\" infinite-scroll-parent=\"false\"> <!--search  --> <div class=\"search_section_joblist clearfix\"> <div class=\"col-md-4 pad-0 search_input\"> <input type=\"text\" autocomplete name=\"\" value=\"\" class=\"form-control\" ng-model=\"address.city\" placeholder=\"Suburb, City,\" ng-blur=\"self.scope.locselected = true\"> <img src=\"styles/images/search.png\" alt=\"\" class=\"search_icon\"> <ul class=\"dropdown-menu state_city_sub\" ng-if=\"!locselected && locations.length > 0\"> <li ng-repeat=\"loc in locations\"><a href=\"javascript:void(0)\" ng-click=\"selectLocation(loc)\">{{loc.state}},{{loc.city}}</a></li> </ul> </div> <div class=\"col-md-2 pad-0\"> <input type=\"text\" name=\"\" ng-disabled=\"true\" value=\"\" ng-model=\"address.state\" class=\"form-control\" placeholder=\"State\"> <!-- <ul class=\"dropdown-menu state_city_sub\" ng-if=\"!locselected && locations.length > 0\">\n" +
    "                                                    <li ng-repeat=\"loc in locations\"><a href=\"javascript:void(0)\" ng-click=\"selectLocation(loc)\">{{loc.state}},{{loc.city}}</a></li>\n" +
    "                                                    </ul> --> </div> <div class=\"col-md-2 pad-0\"> <input type=\"text\" name=\"\" ng-disabled=\"true\" value=\"\" ng-model=\"address.post_code\" class=\"form-control\" placeholder=\"Post Code\"> </div> <div class=\"col-md-2 pad-0 search_input\"> <input type=\"text\" uib-datepicker-popup=\"{{format}}\" ng-model=\"address.date\" is-open=\"status.opened\" min-date=\"minDate\" max-date=\"maxDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" ng-required=\"true\" close-text=\"Close\" placeholder=\"dd/mm/yyyy\" class=\"form-control\"> <img src=\"styles/images/date_ico.png\" alt=\"\" class=\"search_icon\" ng-click=\"open($event)\"> </div> <div class=\"col-md-2 pad-r-0\"> <button class=\"btn btn-success search_btn\" ng-click=\"searchbyLocation()\">Search</button> </div> </div> <div ng-if=\"joblist.length == 0\"> <h1 class=\"main_heading_nojob\"><i class=\"material-icons\">sentiment_very_dissatisfied</i></h1> <h1 class=\"main_heading_nojob p-t-0\">No Jobs found!!!</h1></div> <!-- single job list --> <div class=\"single_joblist\" ng-repeat=\"job in joblist\"> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\">{{job.care_name}}<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care</h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / oneoff care</h5> <!-- <h5 ng-if=\"job.care_type.indexOf('night') > -1\" class=\"care_type\">Night time care</h5> --> <h4 class=\"caregiver_name\">{{job.caregiver_type}}</h4> <!-- label section --> <!-- <div class=\"label_section_tags\">\n" +
    "                    <ul>\n" +
    "                      <li><span class=\"label label-default\">Night time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Day time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Before school care</span></li>\n" +
    "                    </ul>\n" +
    "                  </div> --> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label\"><i class=\"diw-moon-stars pull-left weather\"></i>Night time care</span></li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label\"><i class=\"diw-sun pull-left weather\"></i>Day time care</span></li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label\"><i class=\"fa fa-building-o pull-left school\"></i>Before school care</span></li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label\"><i class=\"fa fa-building pull-left school\"></i>After school care</span></li> </ul> </div> <!-- end label section --> <!-- location section --> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span>{{job.state}}, {{job.city}} - {{job.post_code}}</span> </div> <!-- End location section --> </div> <!-- end left detils section--> <div class=\"right_btn_section\"> <div class=\"text-center\"> <h5 class=\"budget_label\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker. We have already deducted M% Employment Service Fees and N% Workers Payment Processing Fees from the employer's budget for simplicity and transparency\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> <div class=\"apply_btn text-center\"> <button class=\"btn btn-default\" ng-click=\"submitBid()\">Submit Bid</button> </div> </div> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container\"> <div class=\"user_rating\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <a href=\"#\"> <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\"> </a> </div> <div class=\"media-body\"> <h4 class=\"media-heading\">{{job.user_name}}</h4> <jk-rating-stars rating=\"secondRate\" read-only=\"readOnly\"></jk-rating-stars> </div> </div> </div> <div class=\"date_section\"> <div class=\"day_selector\" ng-if=\"job.job_type == 1\"> <ul> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li> </ul> </div> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> <div class=\"job_time_date\"> <ul> <li><span class=\"datetime_job_list\"><i class=\"fa fa-calendar\"></i>{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></li> <li><span class=\"datetime_job_list\"><i class=\"fa fa-clock-o\"></i>{{job.time_budget.time | date:\"h:mm\"}}</span></li> </ul> </div> </div> <div class=\"view_detail_sec\"> <div class=\"pull-right text-center detail_job_link\"> <a href=\"javascript:void(0)\" class=\"view_job_link\" data-toggle=\"collapse\" data-target=\"#demo{{$index}}\">View job details<i class=\"fa fa-angle-down\"></i></a> </div> </div> </div> </div> <!-- end single job list --> <!-- view job details --> <div id=\"demo{{$index}}\" class=\"collapse\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"rate_section\"> <div class=\"col-md-7\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children</label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children</label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type</label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-5 rightside_boot_panel\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\">Rate per hour<span>${{job.time_budget.rate_per_hour}}<i class=\"icon-info\"></i></span></div> <div class=\"panel-body\">Number of hours<span>{{job.time_budget.hours_required}}</span></div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"job_detail_heading\">Requirement</h3> <div class=\"tag_section\"> <ul> <li ng-repeat=\"x in strtoarr(job.req.requirements) track by $index\"><span><i class=\"material-icons\">face</i>{{x}}</span></li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h2 class=\"line_section\"> <span class=\"line-center\">Other Details </span> </h2> <!-- <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p>\n" +
    "                  <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</p>\n" +
    "                  <p>- He/She should understand the<span> UI/UX considerations.</span></p> --> <div class=\"otherdetail_content\"> <p>{{job.req.other_requirement}}</p> </div> </div> </div> <div class=\"col-md-12 hide\"> <div class=\"other_details m-t-20\"> <h3 class=\"job_detail_heading\">Job owner notes</h3> <!-- <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p>\n" +
    "                  <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</p>\n" +
    "                  <p>- He/She should understand the<span> UI/UX considerations.</span></p> --> <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p> <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</span></span></p> <p>- He/She should understand the<span> UI/UX considerations.</span></p> </div> </div> </div> </div> <div class=\"footer_job_detail clearfix\"> <!-- <div class=\"col-md-12\">\n" +
    "                <ul class=\"main_footer_section\">\n" +
    "                  <li>JOB VIEWS <span>256</span></li>\n" +
    "                  <li>JOB APPLICANTS <span>90</span></li>\n" +
    "                </ul>\n" +
    "              </div> --> <div class=\"row\"> <div class=\"col-sm-6\"> <ul class=\"main_footer_section\"> <li>JOB VIEWS <span>256</span></li> <li>JOB APPLICANTS <span>90</span></li> </ul> </div> <div class=\"col-sm-6\"> <ul class=\"main_footer_section pull-right\"> <li><i class=\"material-icons\">mail</i>Send message </li> <li ng-click=\"submitBid()\"><i class=\"material-icons\">check_circle</i>Submit Bid </li> </ul> </div> </div> </div> </div> </div> </div> <!-- end of filter and main contain row --> </div> </div> <div class=\"col-sm-3 hidden-sm hidden-xs\"> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </div></section> <!-- footer section comes here --> <footer> <div class=\"container-fluid\"> <div class=\"row\"> <div class=\"col-md-3 col-sm-6\"> <h4>About Us</h4> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<span>Readmore..</span></p> </div> <div class=\"col-md-3 col-sm-6\"> <h4>Quick Links</h4> <p><i class=\"fa fa-circle-o\"></i>Home</p> <p><i class=\"fa fa-circle-o\"></i>Post Job</p> <p><i class=\"fa fa-circle-o\"></i>How it Works</p> <p><i class=\"fa fa-circle-o\"></i>My Jobs</p> </div> <div class=\"col-md-3 col-sm-6\"> <h4>Upcoming Blogs</h4> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <!-- <h4 class=\"heading_foot\">HOLY GHOST & FAITH CLINIC- TUESDAYS</h4>\n" +
    "            <h5 class=\"sub_heading_foot\">June 20</h5>\n" +
    "            <h5 class=\"sub_heading_foot\">lagos lagos queesland</h5> --> </div> </div> </div> <div class=\"col-md-3 col-sm-6\"> <h4>Following</h4> <ul class=\"following_section\"> <li><a href=\"#\"><img src=\"styles/images/facebook.png\" alt=\"\"></a></li> <li><a href=\"#\"><img src=\"styles/images/twitter.png\" alt=\"\"></a></li> <li><a href=\"#\"><img src=\"styles/images/linkedin.png\" alt=\"\"></a></li> </ul> </div> </div> </div> </footer>"
  );


  $templateCache.put('scripts/module/job/views/parentjoblist.html',
    "<!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> -->  <div class=\"upToScroll buttonShow\" ng-if=\"isScrolled > 0\"> <a class=\"scrollToTop icon-up-open-big\" title=\"Back to top\" ng-click=\"scrollTopHeader();\"> </a> </div> <div header ng-if=\"!adminLogged\"></div> <section id=\"job_list_section\"> <div class=\"container-fluid\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"first_row clearfix\"> <div class=\"col-sm-3\"> <!-- <h3 class=\"title_joblist\">My Jobs  <span class=\"fa fa-filter pull-right toggle_icon_pink\" ng-click=\"state=!state\" data-ng-init=\"state=true\" uib-tooltip=\"close filter\"></span></h3> --> <h3 class=\"title_joblist\">My Jobs</h3> </div> <div class=\"col-sm-9 top_tab_material\"> <ul class=\"nav nav-tabs\" role=\"tablist\"> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'draft'}\"><a href=\"javascript:void(0)\" aria-controls=\"home\" role=\"tab\" ng-click=\"parentTabchange('draft')\" data-toggle=\"tab\">Draft</a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'posted'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('posted')\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Posted</a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'quoted'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('quoted')\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Quoted <span class=\"badge orange_badge\" ng-if=\"status_counts.quoted_count__sum > 0\">{{status_counts.quoted_count__sum}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'confirmed'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('confirmed')\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Confirmed</a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'completed'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('completed')\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Completed <span class=\"badge orange_badge\" ng-if=\"status_counts.completed_count__sum > 0\">{{status_counts.completed_count__sum}}</span></a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'paid'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('paid')\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Paid</a></li> <li role=\"presentation\" ng-class=\"{'active':parentJobtab == 'closed'}\"><a href=\"javascript:void(0)\" ng-click=\"parentTabchange('closed')\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Closed</a></li> </ul> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-12\" infinite-scroll=\"loadparentJobs()\" infinite-scroll-distance=\"0\" infinite-scroll-parent=\"false\" infinite-scroll-disabled=\"jobcanCalled\"> <div ng-if=\"parentJoblist.length == 0 && !isLoading\" class=\"text-center no_jobs_section\"> <!-- <h1 class=\"main_heading_nojob \"><i class=\"fa fa-frown-o\"></i></h1> --> <img src=\"styles/images/child_cry.png\" ng-if=\"parentJobtab !='posted'\" alt=\"\" width=\"50\"> <h1 class=\"main_heading_nojob p-t-0 m-t-0\" ng-if=\"parentJobtab !='posted'\">No jobs available.</h1> <!-- <a class=\"btn cca-btn-fill hvr-wobble-horizontal \" ng-click=\"jopPopup()\">Post a Job</a> --> <div ng-if=\"parentJobtab =='posted'\"> <h1 class=\"first_job_msg\">Post your first job today for FREE</h1> <a class=\"btn hvr-wobble-horizontal hire_btn\" ng-click=\"jopPopup()\">start hiring now</a> </div> </div> <!-- quoted single --> <div class=\"single_joblist\" ng-repeat=\"job in parentJoblist\"> <input type=\"checkbox\" ng-hide=\"true\" ng-model=\"job.is_blocked\" ng-true-value=\"1\" ng-false-value=\"0\" ng-change=\"jobBlocker(job.jobid,job.is_blocked)\" ng-checked=\"job.is_blocked == 1\"> <div class=\"shape\" ng-if=\"job.quote_count > 0 && job.is_confirmed == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 2\"> <div class=\"shape-text1\"> Confirmed </div> </div> <div class=\"shape1\" ng-if=\"job.is_confirmed == 1 && job.job_status == 3\"> <div class=\"shape-text1 w_complt\"> Completed </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 4\"> <div class=\"shape-text1 paid_txt\"> Paid </div> </div> <div class=\"shape2\" ng-if=\"job.job_status == 7\"> <div class=\"shape-text1 paid_txt\"> Closed </div> </div> <p class=\"job_id\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\">{{job.caregiver_type}} <!-- <span class=\"approved_label\">Approved</span> --> </h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / One-off care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <!-- <h5 ng-if=\"job.care_type.indexOf('night') > -1\" class=\"care_type\">Night time care</h5> --> <h4 class=\"caregiver_name\">{{job.care_name}}</h4> <!-- label section --> <!-- <div class=\"label_section_tags\">\n" +
    "                    <ul>\n" +
    "                      <li><span class=\"label label-default\">Night time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Day time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Before school care</span></li>\n" +
    "                    </ul>\n" +
    "                  </div> --> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-nightcare_gray pull-left\"></i><span class=\"p-l-5\">Night time care</span></span> </li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-daycare_gray pull-left\"></i><span class=\"p-l-5\">Day time care</span></span> </li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-beforeschool_gray pull-left\"></i><span class=\"p-l-5\">Before school care</span></span> </li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-afterschool_gray pull-left\"></i><span class=\"p-l-5\">After school care</span></span> </li> </ul> </div> <!-- end label section --> <!-- location section --> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span> {{job.city}} , {{job.state}} {{job.post_code}}</span> </div> <!-- End location section --> </div> <!-- end left detils section--> <div class=\"right_btn_section\"> <!-- {{job.job_status}} --> <div class=\"text-center\"> <span ng-if=\"job.job_status == 0 || job.job_status == 1\"> <h5 class=\"budget_label\">Budget<i class=\"icon-info circle_info\" uib-popover=\"Estimated Budget = (Number of hours required * Rate per hour) + {{hm.jobtime.x}} % Platform Customer Service Fees +  {{hm.jobtime.y}} % Online Payment Fees +  {{hm.jobtime.z}} % GST\" popover-trigger=\"'mouseenter'\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> </span> <span ng-if=\"job.job_status == 2 || job.job_status == 3\"> <h5 class=\"budget_label p-t-15\">Funds transfered<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.funds_transfered | number:2}}</h4> </span> <span ng-if=\"job.job_status == 4\"> <h5 class=\"budget_label bl_payment p-t-15\">Payment Released<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.funds_transfered | number:2}}</h4> </span> <div class=\"apply_btn1 text-center\" ng-if=\"job.job_status == 4\"> <button class=\"btn btn-default btn-block\" ng-click=\"getFeedback(job.jobid,'client')\"> <!-- <i class=\"fa fa-edit p-r-5 user_name_confirm\"></i> --> My Feedback</button> <!-- <span>{{job.quote_count}}</span> --> </div> <div class=\"apply_btn1 text-center\" ng-if=\"job.quote_count > 0 && parentJobtab == 'quoted' && job.is_confirmed != 1\"> <button class=\"btn btn-default btn-block\" ng-click=\"viewJob(job.jobid)\">View Quotes</button> <span>{{job.quote_count}}</span> </div> <div class=\"apply_btn1 text-center\" ng-if=\"parentJobtab != 'quoted' && job.quote_count > 0 && job.is_confirmed != 1\"> <button class=\"btn btn-default btn-block\" ng-click=\"viewJob(job.jobid)\">View Job</button> <span>{{job.quote_count}}</span> </div> <div class=\"apply_btn1 text-center\" ng-if=\"job.quote_count == 0 || job.drafted\"> <button class=\"btn btn-default btn-block\" ng-click=\"editJob(job.jobid)\"> <!-- <i class=\"fa fa-edit p-r-5 user_name_confirm\"></i> --> Edit Job</button> <!-- <span>{{job.quote_count}}</span> --> </div> <div class=\"apply_btn1 text-center\" ng-if=\"job.job_status == 3\"> <button class=\"btn btn-default btn-block\" ng-click=\"releasePayment(job.jobid,job.confirmed_info.userid__id)\"> <!-- <i class=\"fa fa-edit p-r-5 user_name_confirm\"></i> --> Release payment</button> <!-- <span>{{job.quote_count}}</span> --> </div> </div> <!-- <div class=\"text-center\" ng-if=\"job_status == 1 || job.is_bidded\">\n" +
    "\n" +
    "                    <h5 class=\"budget_label\">My quote</h5>\n" +
    "                    <h4 class=\"text-center salary_job\">${{job.bid_info.quote}}</h4>\n" +
    "\n" +
    "                  </div> --> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container_parentjoblist\"> <div class=\"date_section\"> <h5 ng-if=\"job.job_type == 0\">Date</h5> <h5 ng-if=\"job.job_type == 1\">Start Date</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-calendar fs-12\"></i>&nbsp;{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></h4> </div> <div class=\"time_section\"> <h5>Start time</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-clock fs-12\"></i>&nbsp;{{job.time_budget.time | date:\"HH:MM\"}}</span></h4> </div> <div class=\"days_section\"> <h5><span><i class=\"fa fa-square fs-11 color_orange\"></i>&nbsp;</span>Required Days</h5> <div class=\"day_selector\" ng-if=\"job.job_type == 1\"> <ul> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li> </ul> </div> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> </div> <div class=\"viewjob_section\"> <div class=\"float-right detail_job_link\"> <a href=\"javascript:void(0)\" class=\"view_job_link\" data-toggle=\"collapse\" data-target=\"#demo{{$index}}\" ng-if=\"job.is_confirmed != 1\">View job details<i class=\"fa fa-angle-down\"></i></a> <a href=\"#/previewjob/{{job.jobid}}/jobdetails\" ng-if=\"job.is_confirmed == 1\" class=\"btn btn-xs btn-warning view_job_btn\">View job</a> </div> </div> </div> <!--  end new section--> <!-- <div class=\"rating_container_parentjoblist\">\n" +
    "                                <div class=\"date_section\">\n" +
    "                                <h5 ng-if=\"job.job_type == 0\">Date</h5>\n" +
    "                                <h5 ng-if=\"job.job_type == 1\">Start Date</h5>\n" +
    "                                <h4><span class=\"datetime_job_list\"><i class=\"fa fa-calendar\"></i>&nbsp;{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></h4>\n" +
    "\n" +
    "\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"time_section\">\n" +
    "                                  <h5>Start time</h5>\n" +
    "                                  <h4><span class=\"datetime_job_list\"><i class=\"fa fa-clock-o\"></i>&nbsp;{{job.time_budget.time | date:\"HH:MM\"}}</span></h4>\n" +
    "                                </div>\n" +
    "                                <div class=\"days_section\">\n" +
    "                                <h5>Days</h5>\n" +
    "                                  <div class=\"day_selector\" ng-if=\"job.job_type == 1\">\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t<ul>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t<div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\">\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t<ul>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 0}\">S</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 1}\">M</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 2}\">T</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 3}\">W</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 4}\">T</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 5}\">F</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 6}\">S</li>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"viewjob_section\">\n" +
    "                                  <div class=\"float-right  detail_job_link\">\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" class=\"view_job_link\" data-toggle=\"collapse\" data-target=\"#demo{{$index}}\" ng-if=\"job.is_confirmed != 1\">View job details<i class=\"fa fa-angle-down\"></i></a>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#/previewjob/{{job.jobid}}\" ng-if=\"job.is_confirmed == 1\" class=\"btn btn-xs btn-warning view_job_btn\">View job</a>\n" +
    "          \t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "                                </div>\n" +
    "                              </div> --> </div> <!-- <div class=\"rating_sec\">\n" +
    "                                <div class=\"rating_container rating_containernew_res\">\n" +
    "                                    <div class=\"user_rating\">\n" +
    "                                        <div class=\"media\">\n" +
    "                                            <div class=\"media-left media-middle\">\n" +
    "                                                <a href=\"javascript:void(0)\">\n" +
    "                                                    <span ng-if=\"job.client_profilepic != ''\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"media-object\" ng-src=\"images/profilepic/{{job.client_profpic}}\" ng-error-src=\"images/no_image_1.png\" alt=\"\">\n" +
    "                                                    </span>\n" +
    "                                                    <span ng-if=\"job.client_profilepic == ''\">\n" +
    "                                                    <img class=\"media-object\" ng-src=\"images/no_image_1.png\" alt=\"\">\n" +
    "                                                    </span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</a>\n" +
    "                                            </div>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"date_section\">\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"date_section_inner\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"user_rating user_ratinginner\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"media\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"media-body\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"media-heading\">{{job.user_name}}</h4>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"midtablecntt\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"day_selector\" ng-if=\"job.job_type == 1\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 0}\">S</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 1}\">M</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 2}\">T</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 3}\">W</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 4}\">T</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 5}\">F</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li ng-class=\"{'active':day == 6}\">S</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"job_time_date\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><span class=\"datetime_job_list\"><i class=\"fa fa-calendar\"></i>{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li><span class=\"datetime_job_list\"><i class=\"fa fa-clock-o\"></i>{{job.time_budget.time | date:\"HH:MM\"}}</span></li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"view_detail_sec\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"pull-right text-center detail_job_link\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" class=\"view_job_link\" data-toggle=\"collapse\" data-target=\"#demo{{$index}}\" ng-if=\"job.is_confirmed != 1\">View job details<i class=\"fa fa-angle-down\"></i></a>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#/previewjob/{{job.jobid}}\" ng-if=\"job.is_confirmed == 1\" class=\"btn btn-xs btn-warning view_job_btn\">View job</a>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t</div>\n" +
    "                                </div>\n" +
    "                            </div> --> <!-- end single job list --> <!-- view job details --> <div id=\"demo{{$index}}\" class=\"collapse\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"rate_section\"> <div class=\"col-md-7\"> <!-- <div class=\"form-group clearfix\">\n" +
    "                                                    <label class=\"control-label col-xs-5\">Number of children</label>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"col-xs-1 p-0\">:</span>\n" +
    "                                                    <div class=\" col-xs-6 rigprofcnt right_cntent\">\n" +
    "                                                        <span class=\"text_gray\">{{job.no_of_children}}</span>\n" +
    "                                                    </div>\n" +
    "                                                </div> --> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-7 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-5 rightside_boot_panel\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\">Rate per hour<span>${{job.time_budget.rate_per_hour}}</span></div> <div class=\"panel-body\">Number of hours<span>{{job.time_budget.hours_required}}</span></div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"job_detail_heading\">Requirements</h3> <div class=\"tag_section\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><span class=\"custom_requirement\">{{x.requirement}}</span></span> </li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h2 class=\"line_section\"> <span class=\"line-center\">Other Details </span> </h2> <div class=\"otherdetail_content\"> <p ng-bind-html=\"job.other_requirement\"></p> </div> </div> </div> <div class=\"col-md-12 hide\"> <div class=\"other_details m-t-20\"> <h3 class=\"job_detail_heading\">Job owner notes</h3> <!-- <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p>\n" +
    "                  <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</p>\n" +
    "                  <p>- He/She should understand the<span> UI/UX considerations.</span></p> --> <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p> <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</span></span></p> <p>- He/She should understand the<span> UI/UX considerations.</span></p> </div> </div> </div> </div> <div class=\"footer_job_detail clearfix\"> <!-- <div class=\"col-md-12\">\n" +
    "                <ul class=\"main_footer_section\">\n" +
    "                  <li>JOB VIEWS <span>256</span></li>\n" +
    "                  <li>JOB APPLICANTS <span>90</span></li>\n" +
    "                </ul>\n" +
    "              </div> --> <div class=\"row\"> <div class=\"full_bottom clearfix\"> <div class=\"col-md-5 col-sm-5\"> <ul class=\"left_side_detail_ul\"> <li>JOB VIEWS <span class=\"badge\">{{job.job_view_count}}</span></li> <li>JOB APPLICANTS <span class=\"badge\">{{job.bidded_users.length}}</span></li> </ul> </div> <div class=\"col-md-7 col-sm-7\"> <ul class=\"right_side_detail_ul\"> <li><a href=\"#/previewjob/{{job.jobid}}/messages\" class=\"btn btn-xs btn-warning\"><span class=\"fa fa-envelope-open\"></span> View Message</a></li> <!-- <li><a href=\"javascript:void(0)\" class=\"btn btn-xs btn-warning\" ng-click=\"sentMessage(job.user_id,job.jobid)\"><span class=\"fa fa-paper-plane\"></span> Send Message</a></li> --> <li ng-if=\"job_status != 1 && ! job.is_bidded\"><a href=\"javascript:void(0)\" class=\"btn btn-xs btn-warning\" ng-click=\"submitBid()\"><span class=\"fa fa-check-circle\"></span> Submit Bid</a></li> </ul> </div> </div> <!-- <div class=\"col-sm-6\">\n" +
    "                  <ul class=\"main_footer_section\">\n" +
    "                    <li>JOB VIEWS <span>256</span></li>\n" +
    "                    <li>JOB APPLICANTS <span>90</span></li>\n" +
    "                  </ul>\n" +
    "\n" +
    "                </div> --> </div> </div> </div> </div> <div class=\"text-center\" ng-if=\"isLoading\"> <div id=\"loadFacebookG\"> <div id=\"blockG_1\" class=\"facebook_blockG\"></div> <div id=\"blockG_2\" class=\"facebook_blockG\"></div> <div id=\"blockG_3\" class=\"facebook_blockG\"></div> </div> <p class=\"text-center loader_text\">Loading...</p> </div> <!--  End Quotes single--> </div> </div> </div> <div class=\"col-sm-3 hidden-sm hidden-xs\"> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </div> </section> <div footer ng-if=\"!adminLogged\"></div>"
  );


  $templateCache.put('scripts/module/job/views/preview_bak.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <section id=\"job_list_section\"> <div class=\"container-fluid\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"first_row clearfix\"> <div class=\"col-sm-12\"> <div class=\"row\"> <div class=\"col-sm-9\"> <h3 class=\"title_joblist\">View Job </h3> </div> <div class=\"col-sm-3\"> <a class=\"back_btn hvr-wobble-horizontal\" href=\"#/parentjoblist\"><i class=\"fa fa-chevron-left\"></i>Back to quoted jobs list</a> </div> </div> </div> <!-- <div class=\"col-sm-9 top_tab_material\">\n" +
    "            <ul class=\"nav nav-tabs\" role=\"tablist\">\n" +
    "            <li role=\"presentation\" ><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(0)\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Draft</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(1)\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Posted</a></li>\n" +
    "            <li role=\"presentation\" class=\"active\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(1)\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">Quoted</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(2)\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Confirmed</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(3)\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Completed</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"javascript:void(0)\" ng-click=\"searchbyStatus(-1)\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">Closed</a></li>\n" +
    "            </ul>\n" +
    "          </div> --> </div> </div> <div class=\"row\"> <div class=\"col-sm-12\"> <!-- quoted single --> <div class=\"single_joblist\"> <div class=\"shape\" ng-if=\"job.quote_count > 0 && job.is_confirmed == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.is_confirmed == 1\"> <div class=\"shape-text1\"> Confirmed </div> </div> <p class=\"job_id\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\">{{job.care_name}} <!-- <span class=\"approved_label\">Approved</span> --> </h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / One-off care<span class=\"label label-primary time_label\">{{job.time_budget.hours_required}} hrs</span></h5> <!-- <h5 ng-if=\"job.care_type.indexOf('night') > -1\" class=\"care_type\">Night time care</h5> --> <h4 class=\"caregiver_name\">{{job.caregiver_type}}</h4> <!-- label section --> <!-- <div class=\"label_section_tags\">\n" +
    "                    <ul>\n" +
    "                      <li><span class=\"label label-default\">Night time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Day time care</span></li>\n" +
    "                      <li><span class=\"label label-default\">Before school care</span></li>\n" +
    "                    </ul>\n" +
    "                  </div> --> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label\"><i class=\"diw-moon-stars pull-left weather\"></i>Night time care</span></li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label\"><i class=\"diw-sun pull-left weather\"></i>Day time care</span></li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label\"><i class=\"fa fa-graduation-cap pull-left school\"></i>Before school care</span></li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label\"><i class=\"fa fa-graduation-cap pull-left school\"></i>After school care</span></li> </ul> </div> <!-- end label section --> <!-- location section --> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span>{{job.state}}, {{job.city}} - {{job.post_code}}</span> </div> <!-- End location section --> </div> <!-- end left detils section--> <div class=\"right_btn_section\"> <div class=\"text-center\" ng-if=\"job_status != 1 && !job.is_bidded\"> <h5 class=\"budget_label\" ng-if=\"job.is_confirmed == 0\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker.\"></i></h5> <h5 class=\"budget_label\" ng-if=\"job.is_confirmed == 1\">Funds transfered<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> <!-- <div class=\"apply_btn1 text-center\">\n" +
    "                       <button class=\"btn btn-default btn-block\">View Quotes</button>\n" +
    "                       <span>{{job.quote_count}}</span>\n" +
    "                    </div> --> </div> <!-- <div class=\"text-center\" ng-if=\"job_status == 1 || job.is_bidded\">\n" +
    "\n" +
    "                    <h5 class=\"budget_label\">My quote</h5>\n" +
    "                    <h4 class=\"text-center salary_job\">${{job.bid_info.quote}}</h4>\n" +
    "\n" +
    "                  </div> --> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container rating_containernew_res\"> <div class=\"user_rating\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <a href=\"#\"> <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\"> </a> </div> </div> </div> <div class=\"date_section\"> <div class=\"date_section_inner\"> <div class=\"user_rating user_ratinginner\"> <div class=\"media-body\"> <h4 class=\"media-heading\">{{job.user_name}}</h4> <!-- <jk-rating-stars rating=\"secondRate\" read-only=\"readOnly\" ></jk-rating-stars> --> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> </div> </div> <div class=\"midtablecntt\"> <div class=\"day_selector\" ng-if=\"job.job_type == 1\"> <ul> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Sunday') > -1}\">S</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Monday') > -1}\">M</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Tuesday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Wednesday') > -1}\">W</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Thursday') > -1}\">T</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Friday') > -1}\">F</li> <li ng-class=\"{'active':job.time_budget.days_required.indexOf('Saturday') > -1}\">S</li> </ul> </div> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> </div> <div class=\"job_time_date\"> <ul> <li><span class=\"datetime_job_list\"><i class=\"fa fa-calendar\"></i>{{job.time_budget.date | date:\"dd/MM/yyyy\"}}</span></li> <li><span class=\"datetime_job_list\"><i class=\"fa fa-clock-o\"></i>{{job.time_budget.time | date:\"HH:MM\"}}</span></li> </ul> </div> </div> <div class=\"view_detail_sec\"> <div class=\"pull-right text-center detail_job_link\"> <a href=\"javascript:void(0)\" class=\"view_job_link\" aria-expanded=\"true\" data-toggle=\"collapse\" data-target=\"#demo\">View job details<i class=\"fa fa-angle-down\"></i></a> </div> </div> </div> </div> </div> <!-- end single job list --> <!-- view job details --> <div id=\"demo\" class=\"collapse in\" aria-expanded=\"true\"> <div class=\"detail_parent_job_view clearfix\"> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"top_tab_material\"> <!-- Nav tabs --> <ul class=\"nav nav-tabs\" role=\"tablist\"> <li role=\"presentation\"><a data-target=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Job Details</a></li> <li role=\"presentation\"><a data-target=\"#profile\" aria-controls=\"Messages\" role=\"tab\" data-toggle=\"tab\">Messages</a></li> <li role=\"presentation\" class=\"active\" ng-if=\"job.bidded_users.length > 0\"><a data-target=\"#Quotes\" aria-expanded=\"true\" aria-controls=\"Quotes\" role=\"tab\" data-toggle=\"tab\">Quotes<span class=\"badge cunt_badge\">{{job.quote_count}}</span></a></li> <li role=\"presentation\" ng-if=\"job.is_confirmed == 1\"><a data-target=\"#confirmedjob\" id=\"confirmtab\" aria-expanded=\"true\" aria-controls=\"confirmedjob\" role=\"tab\" data-toggle=\"tab\">Confirmed worker</a></li> </ul> <!-- Tab panes --> <div class=\"tab-content\"> <div role=\"tabpanel\" class=\"tab-pane\" id=\"home\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"rate_section\"> <div class=\"col-md-7\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-4\">Number of children<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-8 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-4\">Age group of children<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-8 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-4\">Employer Type<span class=\"pull-right\">&nbsp;:</span></label> <div class=\"col-xs-8 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-5 rightside_boot_panel\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\">Rate per hour<span>${{job.time_budget.rate_per_hour}}<i class=\"icon-info\"></i></span></div> <div class=\"panel-body\">Number of hours<span>{{job.time_budget.hours_required}}</span></div> </div> <!-- <div class=\"panel panel-default\" ng-if=\"job.bidded_users.length > 0\">\n" +
    "                <div class=\"panel-body user_rating\" ng-repeat=\"confirmed in job.bidded_users\">\n" +
    "                    <div class=\"media\" ng-if=\"confirmed.is_confirmed == 1\">\n" +
    "                    <div class=\"media-left \">\n" +
    "                    <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\">\n" +
    "                    </div>\n" +
    "                    <div class=\"media-body\">\n" +
    "                      <h5 class=\"id_section1 p-b-5\">{{confirmed.userid__first_name}} {{confirmed.userid__last_name}}</h4>\n" +
    "                        <h5 class=\"id_section p-b-5\">Id: {{confirmed.userid__id}}</h5>\n" +
    "                        <h5 class=\"id_section1 p-b-5\">{{confirmed.userid__email}}</h5>\n" +
    "                        <h5 class=\"id_section1 p-b-5\">{{confirmed.userid__contactno}}</h5>\n" +
    "\n" +
    "                    </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "                </div> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"job_detail_heading\">Requirements</h3> <div class=\"tag_section\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i>{{x.requirement}}</span></li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                        <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h2 class=\"line_section\"> <span class=\"line-center\">Other Details </span> </h2> <div class=\"otherdetail_content\"> <p>{{job.other_requirement}}</p> </div> </div> </div> <div class=\"col-md-12 hide\"> <div class=\"other_details m-t-20\"> <h3 class=\"job_detail_heading\">Job owner notes</h3> <!-- <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p>\n" +
    "                  <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</p>\n" +
    "                  <p>- He/She should understand the<span> UI/UX considerations.</span></p> --> <p>-Experience in developing <span>cross platform mobile</span> using Ionic framework</p> <p>-Experience in developing mobile applications using <span>Cordova <span>(PhoneGap) is desirable.</span></span></p> <p>- He/She should understand the<span> UI/UX considerations.</span></p> </div> </div> </div> </div> </div> <div role=\"tabpanel\" class=\"tab-pane\" id=\"profile\"></div> <div role=\"tabpanel\" class=\"tab-pane active\" id=\"Quotes\"> <!-- quoted list section --> <div class=\"row\" ng-repeat=\"quote in job.bidded_users\"> <div class=\"single_quote_section\" ng-class=\"{'confirm_active':quote.is_confirmed==1}\"> <div class=\"quote_container\"> <div class=\"user_rating_section\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <a href=\"#\"> <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\"> </a> </div> <div class=\"media-body\"> <h4 class=\"media-heading\">{{quote.userid__first_name}} {{quote.userid__last_name}}</h4> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> </div> </div> </div> <div class=\"requirement_section\"> <div class=\"tag_section1 text-center\"> <ul> <li ng-repeat=\"x in job.req track by $index\" uib-tooltip=\"{{x.requirement}}\"><span><i class=\"{{x.icon_class}} pull-left\"></i></span></li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                  <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                  <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> <!-- <ul>\n" +
    "              <li ng-repeat=\"x in strtoarr(job.req.requirements) track by $index\"><span><i class=\"fa fa-user-circle\"></i></span></li>\n" +
    "\n" +
    "              </ul> --> </div> <div class=\"budget_quote_section text-left\"> <h5 class=\"budget_label m-t-0\">Quote<i class=\"icon-info\"></i></h5> <h4 class=\"salary_job\">${{quote.quote}}</h4> </div> <div class=\"confirm_quote_section\" ng-init=\"accord = false\"> <button class=\"btn btn-default confirm_booking\" ng-click=\"confirmBooking(quote,job.jobid)\" ng-if=\"job.is_confirmed == 0\">Confirm booking</button> <button class=\"btn btn-default confirm_btn\" ng-if=\"job.is_confirmed == 1\" ng-class=\"{'active':quote.is_confirmed == 0}\">Booking confirmed</button> <button data-toggle=\"collapse\" data-target=\"#demo1{{$index}}\" ng-click=\"accord= !accord\" class=\"btn btn-default\"> <i class=\"fa fa-chevron-down\" ng-class=\"{'hide':accord}\"></i> <i class=\"fa fa-chevron-up\" ng-class=\"{'hide':!accord}\"></i> </button> </div> </div> <div id=\"demo1{{$index}}\" class=\"collapse new_collpase\"> <div class=\"row\"> <div class=\"col-sm-8\"> <p class=\"bid_other_details\">{{quote.comment}}</p> </div> <div class=\"col-sm-4 rightside_boot_panel\"> <p class=\"emp_id\">Worker ID:<span>{{quote.userid__id}}</span></p> <div class=\"panel panel-default\"> <div class=\"panel-heading\">Hourly Rated<span class=\"ng-binding\">${{quote.rate_per_hour}}<i class=\"icon-info\"></i></span></div> <div class=\"panel-body\">Number of hours<span class=\"ng-binding\">{{job.time_budget.hours_required}}</span></div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <ul class=\"right_side_detail_ul1\"> <li><a href=\"javascript:void(0)\" class=\"btn btn-md btn-warning\" ng-click=\"sentMessage()\"> Send Message</a></li> <li><a href=\"javascript:void(0)\" class=\"btn btn-md btn-warning\"> View Reviews</a></li> <li><a href=\"javascript:void(0)\" class=\"btn btn-md btn-warning\"> View Profile</a></li> </ul> </div> </div> </div> </div> </div> <!-- End quoted list section --> </div> <div role=\"tabpanel\" class=\"tab-pane\" id=\"confirmedjob\"> <div class=\"first_section success_container\" ng-repeat=\"confirmed in job.bidded_users\"> <div class=\"row\"> <div class=\"confirm_section_overall\" ng-if=\"confirmed.is_confirmed == 1\"> <div class=\"col-sm-12 clearfix\"> <p class=\"emp_id pull-left\">Worker ID:<span>{{confirmed.userid__id}}</span></p> </div> <div class=\"col-sm-12 custom_align\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <img src=\"styles/images/user.jpg\" class=\"media-object confirmed_user_photo img-thumbnail\"> </div> <div class=\"media-body\"> <h4 class=\"media-heading\">{{confirmed.userid__first_name}} {{confirmed.userid__last_name}}</h4> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half class=\"gray_star\"></input-stars> <h5 class=\"id_section1\">{{confirmed.userid__email}}</h5> <h5 class=\"id_section1\">{{confirmed.userid__contactno}}</h5> </div> </div> </div> <div class=\"col-sm-12 confirm_active\"> <!-- <div class=\"col-sm-12 requirement_section orange_requirement\">\n" +
    "                <ul>\n" +
    "                  <li ng-repeat=\"x in strtoarr(job.req.requirements) track by $index\"><span><i class=\"fa fa-user-circle\"></i></span></li>\n" +
    "\n" +
    "                </ul>\n" +
    "              </div> --> <div class=\"tag_section1 m-t-20\"> <b>Requirements:</b> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i></span></li> <!-- <li><span><i class=\"material-icons\">directions_car</i>Car/Driving</span></li>\n" +
    "                  <li><span><i class=\"material-icons\">school</i>Diploma / Bachelors</span></li>\n" +
    "                  <li><span><i class=\"material-icons\">art_track</i>Permanent Residency</span></li> --> </ul> </div> </div> <div class=\"col-sm-12\"> <b>Decription:</b> <p class=\"bid_other_details\">This is the public profile description of the worker. This is the public profile description of the worker. This is the public profile description of the worker. </p> </div> <!-- <div class=\"col-sm-4\">\n" +
    "            <div class=\"text-center\">\n" +
    "                <img  src=\"styles/images/user.jpg\" alt=\"\" class=\"confirmed_user_photo\">\n" +
    "                <h4 class=\"media-heading\">{{confirmed.userid__first_name}} {{confirmed.userid__last_name}}</h4>\n" +
    "                  <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half class=\"gray_star\"></input-stars>\n" +
    "                  <h5 class=\"id_section1 \">{{confirmed.userid__email}}</h5>\n" +
    "                  <h5 class=\"id_section1 \">{{confirmed.userid__contactno}}</h5>\n" +
    "            </div>\n" +
    "\n" +
    "          </div> --> <!-- <div class=\"col-sm-8\">\n" +
    "            <div class=\"row\">\n" +
    "              <div class=\"col-sm-12 requirement_section orange_requirement\">\n" +
    "                <ul>\n" +
    "                  <li ng-repeat=\"x in strtoarr(job.req.requirements) track by $index\"><span><i class=\"fa fa-user-circle\"></i></span></li>\n" +
    "\n" +
    "                </ul>\n" +
    "\n" +
    "              </div>\n" +
    "              <div class=\"col-sm-12\">\n" +
    "                <p class=\"bid_other_details\">This is the public profile description of the worker. This is the public profile description of the worker. This is the public profile description of the worker.\n" +
    "</p>\n" +
    "\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "          </div> --> </div> </div> <!-- <div class=\"user_rating\" ng-if=\"confirmed.is_confirmed == 1\">\n" +
    "                      <div class=\"media\">\n" +
    "                          <div class=\"media-left media-top\">\n" +
    "                              <a href=\"#\">\n" +
    "                                  <img class=\"media-object\" src=\"styles/images/user.jpg\" alt=\"\">\n" +
    "                              </a>\n" +
    "                          </div>\n" +
    "                          <div class=\"media-body\">\n" +
    "                            <h4 class=\"media-heading\">{{confirmed.userid__first_name}} {{confirmed.userid__last_name}}</h4>\n" +
    "                            <h4 class=\"id_section \">ID :{{confirmed.userid__id}}</h4>\n" +
    "                            <h5 class=\"id_section1 \">{{confirmed.userid__email}}</h5>\n" +
    "                            <h5 class=\"id_section1 \">{{confirmed.userid__contactno}}</h5>\n" +
    "\n" +
    "                          </div>\n" +
    "                      </div>\n" +
    "                    </div>   --> </div> </div> </div> </div> </div> </div> <!-- single quote view --> <!--  end single quote--> </div> </div> </div> <!--  End Quotes single--> </div> </div> </div> <div class=\"col-sm-3 hidden-sm hidden-xs\"> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </div> </section> <div footer></div>"
  );


  $templateCache.put('scripts/module/job/views/previewjob.html',
    "<div header ng-if=\"!adminLogged\"></div> <section id=\"job_list_section\" x-sticky-boundary=\"\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"top_row clearfix\"> <div class=\"col-sm-12\"> <div class=\"row\"> <div class=\"col-sm-8\"> <ol class=\"breadcrumb\"> <li><a href=\"javascript:void(0)\">My Jobs</a></li> <li class=\"active\">View Job</li> </ol> </div> <div class=\"col-sm-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"!adminLogged\" href=\"#/parentjoblist/{{prevtab}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back to {{prevtab}} jobs list</a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"adminLogged\" href=\"{{adminUrl}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back</a> </div> </div> </div> </div> </div> <div uib-alert ng-repeat=\"alert in paymentAlerts\" ng-class=\"'alert-' + (alert.type || 'warning')\" close=\"closeAlert($index)\" class=\"uib-alert-design\"> <!--  {{alert.name}}  {{alert.msg}} --> <h4 ng-if=\"alert.type=='danger'\">Payment error</h4> <h4 ng-if=\"alert.type=='success'\">Payment Success</h4> <p class=\"alert_name\">{{alert.name}}</p> <p class=\"alert_msg\">{{alert.msg}}</p> </div> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"single_joblist\"> <div class=\"shape\" ng-if=\"job.quote_count > 0 && job.is_confirmed == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 2\"> <div class=\"shape-text1\"> Confirmed </div> </div> <div class=\"shape1\" ng-if=\"job.is_confirmed == 1 && job.job_status == 3\"> <div class=\"shape-text1 w_complt\"> Completed </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 4\"> <div class=\"shape-text1 paid_txt\"> Paid </div> </div> <div class=\"shape2\" ng-if=\"job.job_status == 7\"> <div class=\"shape-text1 paid_txt\"> Closed </div> </div> <button class=\"btn btn-default close_job_btn\" ng-click=\"closeJob(job.jobid, job)\" ng-if=\"job.job_status == 4 && adminLogged\"> Close job </button> <p class=\"job_id ng-binding\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\"> {{job.caregiver_type}}</h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\"></span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / One-off care<span class=\"label label-primary time_label\"></span></h5> <h4 class=\"caregiver_name\">{{job.care_name}}</h4> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-nightcare_gray pull-left\"></i><span class=\"p-l-5\">Night time care</span></span> </li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-daycare_gray pull-left\"></i><span class=\"p-l-5\">Day time care</span></span> </li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-beforeschool_gray pull-left\"></i><span class=\"p-l-5\">Before school care</span></span> </li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-afterschool_gray pull-left\"></i><span class=\"p-l-5\">After school care</span></span> </li> </ul> </div> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span>{{job.state}}, {{job.city}} - {{job.post_code}}</span> </div> </div> <div class=\"right_btn_section\"> <div class=\"text-center\"> <span ng-if=\"job.job_status == 0 || job.job_status == 1\"> <h5 class=\"budget_label p-t-15\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> </span> <span ng-if=\"job.job_status == 2 || job.job_status == 3\"> <h5 class=\"budget_label p-t-15\">Funds transfered<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.funds_transfered | number:2}}</h4> </span> <span ng-if=\"job.job_status == 4\"> <h5 class=\"budget_label p-t-15\">Payment released<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.funds_transfered | number:2}}</h4> </span> </div> <div class=\"apply_btn1 text-center\" ng-if=\"job.job_status == 3\"> <button class=\"btn btn-default btn-block\" ng-click=\"releasePayment(job.jobid,job.confirmed_info.userid__id)\"> Release payment </button> </div> <div class=\"apply_btn1 text-center\" ng-if=\"job.job_status == 4\"> <button class=\"btn btn-default btn-block\" ng-click=\"getFeedback(job.jobid,'client')\"> My Feedback </button> </div> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container_parentjoblist\"> <div class=\"date_section\"> <h5 ng-if=\"job.job_type == 0\">Date</h5> <h5 ng-if=\"job.job_type == 1\">Start Date</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-calendar fs-12\"></i>&nbsp;{{job.time_budget.date | date:\"dd-MM-yyyy\"}}</span></h4> </div> <div class=\"time_section text-center\"> <h5>Start Time</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-clock fs-12\"></i>&nbsp;{{job.time_budget.time | date:\"HH:MM\"}}</span></h4> </div> <div class=\"viewjob_section text-center\"> <h5>Duration</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-hourglass fs-12\"></i>&nbsp;{{job.time_budget.hours_required}} hrs</span></h4> </div> <div class=\"days_section\"> <h5><span><i class=\"fa fa-square fs-11 color_orange\"></i>&nbsp;</span>Required Days</h5> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> </div> </div> </div> <!-- end rating section --> <!--  vertical tab section--> <div class=\"detail_vertical_side\"> <div class=\"col-md-12 vertical_tab m-t-20\"> <div class=\"tabs_wrapper\"> <ul class=\"tabs\"> <li rel=\"tab1\" ng-click=\"switchTab('jobdetails')\" ng-class=\"{'active':currentTabpreview == 'jobdetails'}\" id=\"personal\"><i class=\"fa fa-briefcase\"></i>&nbsp;Job Details </li> <li rel=\"tab2\" ng-click=\"switchTab('messages')\" ng-class=\"{'active':currentTabpreview == 'messages'}\" id=\"messages\"><i class=\"fa fa-envelope\"></i>&nbsp;Messages</li> <li rel=\"tab3\" ng-click=\"switchTab('quotes')\" ng-class=\"{'active':currentTabpreview == 'quotes'}\" id=\"quotes\"><i class=\"fa fa-money\"></i>&nbsp;Quotes&nbsp;<span class=\"badge orange_badge\">{{job.quote_count}}</span></li> <li rel=\"tab4\" ng-if=\"job.is_confirmed == 1\" ng-click=\"switchTab('confirmed_worker',job.confirmed_info.userid__id)\" ng-class=\"{'active':currentTabpreview == 'confirmed_worker'}\" id=\"confirmed_worker\"><i class=\"fa fa-address-card-o\"></i>&nbsp;Confirmed Worker</li> <li rel=\"tab5\" ng-click=\"switchTab('workerfeed')\" ng-if=\"job.has_review_given\" ng-class=\"{'active':currentTabpreview == 'workerfeed'}\" id=\"workerfeed\"><i class=\"fa fa-address-card-o\"></i>&nbsp;Worker's feedback</li> </ul> <div class=\"tab_container\"> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab1\" ng-click=\"switchTab('jobdetails')\">Job Details</h3> <div id=\"tab1\" class=\"tab_content wqeqwe1weq\" ng-if=\"currentTabpreview == 'jobdetails'\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"job_detail_firsttab\"> <div class=\"col-md-12\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-3 rightside_boot_panel\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"requirement_heading\">Requirements</h3> <div class=\"tag_section tag_section_tab\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><b class=\"req_list_b\">{{x.requirement}}</b></span></li> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h3 class=\"requirement_heading\">Other Details</h3> <div class=\"otherdetail_content\"> <p ng-bind-html=\"job.other_requirement\" class=\"other_tab_p\"></p> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab2\" ng-click=\"switchTab('messages')\">Messages</h3> <div id=\"tab2\" class=\"tab_content fgdfgdf2\" ng-if=\"currentTabpreview == 'messages'\"> <div class=\"\"> <div class=\"private_message_section private_message_sectionnew pp_msg\"> <div class=\"row\"> <div class=\"col-md-12 toggle_button_switch\"> <!-- <toggle-switch ng-model=\"switchStatus\" knob-label=\"Change\" on-label=\"Private\" off-label=\"Public\">\n" +
    "                                                            </toggle-switch> --> <div class=\"toggleWrapper\"> <input type=\"checkbox\" ng-model=\"messageMode\" ng-true-value=\"'public'\" ng-false-value=\"'private'\" ng-click=\"resetMsgs(messageMode)\" id=\"dn\" class=\"dn\"> <label for=\"dn\" class=\"toggle\"><span class=\"toggle__handler\"></span></label> </div> <!-- <ul class=\"nav nav-pills pull-right\">\n" +
    "                                                          <li class=\"active\"><a data-target=\"#public\" ng-click=\"resetMsgs('public')\" aria-controls=\"public\" role=\"tab\" data-toggle=\"pill\"> Public</a></li>\n" +
    "                                                          <li><a data-target=\"#private\" aria-controls=\"private\" ng-click=\"resetMsgs('private')\" role=\"tab\" data-toggle=\"pill\"> Private</a></li>\n" +
    "\n" +
    "                                                          </ul> --> </div> </div> <div> <!-- Nav tabs --> <!-- Tab panes --> <div class=\"tab-content\"> <div ng-if=\"messageMode == 'public'\"> <div class=\"row\"> <form name=\"messageForm\" ng-submit=\"clientSentmsg('public',job.jobid)\"> <div class=\"col-md-12\"> <textarea ng-model=\"input.message\" name=\"message\" class=\"form-control counted m-b-10\" name=\"message\" placeholder=\"Enter your message\" maxlength=\"320\" limit-to=\"320\" rows=\"3\" ng-required=\"true\"></textarea> <!-- <div class=\"errormsg\" ng-if=\"messageForm.message.$invalid\">\n" +
    "                                                                                <p ng-if=\"messageForm.message.$error.required\">*Message cannot be empty</p>\n" +
    "                                                                            </div> --> <div class=\"row\"> <div class=\"col-sm-6\"> <h6 class=\"pull-left character_count\">{{ 320 - input.message.length}} characters remaining</h6> </div> <div class=\"col-sm-6 messagesubbtn\"> <button type=\"submit\" class=\"btn btn-default submit_btn_pvj pull-right\">Submit</button> </div> </div> </div> </form> <div class=\"col-md-12 m-t-20\"> <div class=\"public_message_div clearfix\"> <div class=\"single_public_message single_public_messagenew\" ng-repeat=\"message in messages\"> <div class=\"media m-b-20\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{message.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{message.from_id__first_name}} {{message.from_id__last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{message.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{message.message}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"!showReplybox[$index]\" ng-click=\"showReplybox[$index] = true\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"showReplybox[$index]\" ng-click=\"showReplybox[$index] = !showReplybox[$index]\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(message.on_date_time) && currentUser == message.from_id__id\" ng-click=\"deleteMessage(message.id, message.from_id__id)\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.reply\"> <span class=\"input-group-addon\" ng-click=\"sentReply(message.id,message); showReplybox[$index] = false;\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"secondary_comment_section secondary_comment_sectionnew\" ng-repeat=\"reply in message.replies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{reply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\" ng-init=\"showsubReplybox[$index] = false\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{reply.from_user_first_name}} {{reply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{reply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{reply.reply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\" ng-click=\"showsubReplybox[$index] = !showsubReplybox[$index]\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(reply.on_date_time) && currentUser == reply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteReply(reply.id)\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showsubReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.subreply\"> <span class=\"input-group-addon\" ng-click=\"sentsubReply(reply.id,reply);showsubReplybox[$index] = false\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"third_comment_section third_comment_sectionnew\" ng-repeat=\"subreply in reply.subreplies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{subreply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{subreply.from_user_first_name}} {{subreply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{subreply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{subreply.subreply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(subreply.on_date_time) && currentUser == subreply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteSubreply(subreply.id)\"></i></a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div ng-if=\"messageMode == 'private'\"> <div class=\"row\"> <div class=\"col-md-5 left_side_chat_section pad-right-0\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"panel panel-default\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"search_user_section search_user_section_new\"> <input type=\"text\" placeholder=\"Search user...\" ng-model=\"privateSearch\" ng-keyup=\"getQuoterslist(job.jobid,privateSearch)\" name=\"\" value=\"\" class=\"form-control\"> <i class=\"fa fa-search\"></i> </div> </div> </div> <div ng-if=\"filteredUsers.length == 0\"> <h1 class=\"main_heading_nojob\"><i class=\"fa fa-frown-o\"></i></h1> <h1 class=\"main_heading_nojob p-t-0\">No Users found!!!</h1> </div> <div class=\"panel-body\" ng-scrollbars ng-scrollbars-config=\"callback.leftprivate\" ng-scrollbars-update=\"callback.updateScrollbar\"> <table class=\"table table-hover personal-task\"> <tbody> <tr class=\"\" ng-repeat=\"bidders in filteredUsers = (quoters)\" ng-click=\"getMessages(job.jobid,'private',bidders.userid__id,$index)\" ng-class=\"{'gray_bg_lite':activePrivateuser == bidders.userid__id}\"> <td width=\"10%\"> <span class=\"profile-ava\"> <img alt=\"\" class=\"simple\" src=\"images/profilepic/{{bidders.userid__profpic}}\" ng-error-src=\"/images/no_image_1.png\"> </span> </td> <td width=\"80%\"> <p class=\"profile-name\">{{bidders.userid__first_name}} {{bidders.userid__last_name}} <span ng-if=\"bidders.un_read_count > 0\" class=\"badge orange_badge pull-right\">{{bidders.un_read_count}}</span></p> <p class=\"profile-occupation\">Worker ID: {{bidders.userid__id}}</p> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> <div class=\"col-md-7 pad-left-0\"> <div class=\"right_side_chat_section\"> <div class=\"panel panel-default\"> <div class=\"panel-body\" ng-scrollbars ng-scrollbars-config=\"rightprivate\"> <div class=\"chat\" id=\"pvtContainter\"> <ul class=\"col-xs-12 p-0 m-0\"> <li class=\"privatemsgsection single_public_message single_public_messagenew\" id=\"single_private_msg\" ng-repeat=\"privatemsg in messages\" ng-class=\"{'otheruser':currentUser != privatemsg.from_id__id,'its_meyou':currentUser == privatemsg.from_id__id}\"> <div class=\"media\"> <div class=\"media-leftside\"> <a class=\"user\" href=\"javascript:void(0)\"><img alt=\"\" ng-src=\"images/profilepic/{{privatemsg.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\"></a> </div> <div class=\"media-rightside\"> <div class=\"message\"> <p class=\"m-0 p-0 col-md-12 col-xs-12 paraprvmsg\"> {{privatemsg.message}} </p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(privatemsg.on_date_time) && currentUser == privatemsg.from_id__id\" ng-click=\"deleteMessage(privatemsg.id, privatemsg.from_id__id,'private')\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <span class=\"datetimeset_spn m-0 p-0 col-md-12 col-xs-12 text-right\">{{privatemsg.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> </div> </div> </li> </ul> </div> </div> <div class=\"panel-footer\"> <form name=\"pvtmsgForm\" ng-submit=\"clientSentmsg('private',job.jobid,selectedUser)\"> <div class=\"input-group\"> <input id=\"btn-input\" type=\"text\" class=\"form-control input-sm\" placeholder=\"Type your message here...\" name=\"message\" ng-model=\"input.message\"> <span class=\"input-group-btn\"> <button type=\"submit\" class=\"btn btn-warning btn-sm btn-chat\" id=\"btn-chat\" ng-submit=\"clientSentmsg('private',job.jobid,selectedUser)\" ng-disabled=\"noToid\"> <i class=\"fa fa-paper-plane\"></i></button> </span> </div> </form> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab3\" ng-click=\"switchTab('quotes')\">Quotes</h3> <div id=\"tab3\" class=\"tab_content rrrrgd3fgdf\" ng-if=\"currentTabpreview == 'quotes'\"> <!-- quote recent --> <div class=\"row main_quote_vertical_container\" ng-repeat=\"quote in job.bidded_users\"> <div class=\"col-sm-12\"> <div class=\"verical_tab_preview_job p-10\" ng-class=\"{'apply_bg':accord}\"> <div class=\"single_quote_user\"> <div class=\"first_section\"> <div class=\"media\"> <div class=\"media-left media-middle\"> <img ng-src=\"images/profilepic/{{quote.userid__profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-body\"> <h4 class=\"media-heading quoter_name white_space_property\">{{quote.username | cut:true:15:' ...'}}</h4> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> <p class=\"rate_per_hour\">Rate/hour&nbsp;&nbsp;<span>${{quote.rate_per_hour}}</span></p> </div> </div> </div> <div class=\"second_section\"> <!-- quote value comes here --> <p class=\"quote_label\">Quote:&nbsp;<i class=\"icon-info\" uib-tooltip=\"This is the amount quoted by the worker for this job\"></i></p> <h5 class=\"quote_value\">${{quote.funds_transfered | number : 2}}</h5> <!-- quote value ends here --> </div> <div class=\"third_section\"> <!-- button and expand section --> <div class=\"text-right\"> <!-- <a class=\"btn confirm_quote_btn\" href=\"javascript:void(0)\" ng-click=\"confirmBooking(quote,job.jobid)\" ng-if=\"job.is_confirmed == 0\">Confirm Worker</a>\n" +
    "                                                              <a class=\"btn confirm_quote_btn_label\" disabled ng-if=\"job.job_status == 2 && quote.is_confirmed == 1\" ng-class=\"{'active':quote.is_confirmed == 0}\">Confirmed</a>\n" +
    "                                                              <a href=\"javascript:void(0)\" class=\"btn confirm_quote_btn_label\" ng-if=\"job.job_status == 4\" ng-class=\"{'active':quote.is_confirmed == 0}\">paid</a> --> <a href=\"javascript:void(0)\" class=\"btn btn-success btn-sm confirmed_btn\" disabled ng-if=\"quote.is_confirmed == 1\" ng-class=\"{'active':quote.is_confirmed == 0}\">Confirmed</a> <a href=\"javascript:void(0)\" class=\"btn btn-success btn-sm paid_btn\" ng-if=\"job.job_status == 4 && quote.is_confirmed == 1\" ng-class=\"{'active':quote.is_confirmed == 0}\">Paid</a> <a class=\"btn btn-success btn-sm confir_btn\" href=\"javascript:void(0)\" ng-click=\"confirmBooking(quote,job.jobid)\" ng-if=\"job.is_confirmed == 0\">Confirm Worker</a> <a class=\"btn btn-default btn-sm expand_btn\" data-toggle=\"collapse\" data-target=\"#demo1{{$index}}\" ng-click=\"accord= !accord\">Details&nbsp; <i class=\"icon-arrow-down\" ng-class=\"{'hide':accord}\"></i> <i class=\"icon-arrow-up\" ng-class=\"{'hide':!accord}\"></i></a> </div> <!-- button and expand section ends here --> </div> </div> <div id=\"demo1{{$index}}\" class=\"collapse new_collpase\"> <div class=\"row\"> <div class=\"col-md-9 col-sm-9\"> <div class=\"content_area_comment\"> <p class=\"bid_other_details\">{{quote.comment}}</p> </div> </div> <div class=\"col-md-3 col-sm-3\"> <div class=\"more_actions_detail_quote\"> <!-- href=\"#/viewworker/{{quote.userid__id}}\" --> <button class=\"btn btn-default btn-sm\" ng-click=\"gotoWorkerprofile(quote.userid__id,job.jobid)\"><i class=\"fa fa-eye\"></i>&nbsp;View Profile</button> <button class=\"btn btn-default btn-sm\"><i class=\"fa fa-weixin\"></i>&nbsp;View Reviews</button> <a class=\"btn btn-default btn-sm\" ng-click=\"sentMessage(quote.userid__id,job.jobid)\"><i class=\"fa fa-envelope\"></i>&nbsp;Send Message</a> </div> <!-- <div class=\"more_actions_detail_quote\">\n" +
    "                                                                  <button type=\"button\" class=\"btn btn-warning btn-circle\" uib-tooltip=\"View Profile\"><i class=\"fa fa-eye\"></i></button>\n" +
    "                                                                  <button type=\"button\" class=\"btn btn-primary btn-circle\" uib-tooltip=\"View Reviews\"><i class=\"fa fa-weixin\"></i></button>\n" +
    "                                                                  <button type=\"button\" class=\"btn btn-success btn-circle\" uib-tooltip=\"Sent Message\"><i class=\"fa fa-envelope\"></i></button>\n" +
    "                                                                </div> --> </div> </div> </div> </div> </div> </div> <!--  end quote recent --> <!-- quote section end here --> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab4\" ng-if=\"job.is_confirmed == 1\" ng-click=\"switchTab('confirmed_worker',job.confirmed_info.userid__id)\">Confirmed Worker</h3> <div id=\"tab4\" class=\"tab_content worker_myprofile_view\" ng-if=\"currentTabpreview == 'confirmed_worker'\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-4 col-sm-4\"> <div class=\"profile-pic\"> <div class=\"profile-border\"> <img src=\"/images/profilepic/{{personal.profpic}}\" ng-if=\"personal.profpic\" alt=\"profilepicture\"> <img src=\"/images/no_image_found.png\" ng-if=\"!personal.profpic\" alt=\"profilepicture_not_found\"> </div> </div> <div class=\"text-center worker_rating\"> <h5 class=\"intro-title1\"><span class=\"first_name_css\">{{personal.first_name}}</span> <span class=\"last_name_css\">{{personal.last_name}}</span></h5> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> <h5 class=\"intro-gender\" ng-if=\"personal.age && personal.age !='not specified'\">{{personal.age}} years old &nbsp;<span ng-if=\"personal.gender\">{{personal.gender | capitalize}}</span></h5> </div> </div> <div class=\"col-md-8 col-sm-8\"> <div class=\"single_right_side_container\"> <div class=\"row\"> <div class=\"single_right_side_data clearfix\"> <div class=\"col-sm-12\"> <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2> </div> <div class=\"col-sm-12\"> <div class=\"location_section\"> <i class=\"icon-directions\" class=\"pull-left\"></i> <span class=\"ng-binding\" ng-if=\"personal.address\"> {{personal.address}}</span> <span class=\"ng-binding\" ng-if=\"!personal.address\"> - </span> </div> </div> <div class=\"col-sm-12 m-t-10\"> <div class=\"location_section\"> <i class=\"icon-location-pin\" class=\"pull-left\"></i> <span class=\"ng-binding\" ng-if=\"personal.post_code\"> {{personal.city}},{{personal.state}} {{personal.post_code}}</span> <span class=\"ng-binding\" ng-if=\"!personal.post_code\">-</span> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_data clearfix\"> <div class=\"col-sm-12\"> <div> <strong class=\"label_myprofile\">Preferred job type</strong> <p class=\"value_myprofile\"><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span></p> </div> <div class=\"m-t-10\"> <strong class=\"label_myprofile\">Preferred Age Groups </strong> <p class=\"value_myprofile\">{{preference.children_age}}</p> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_data clearfix bb-none\"> <div class=\"col-sm-6\"> <p class=\"value_myprofile\"><i class=\"icon-envelope\"></i>&nbsp;{{personal.email}}</p> </div> <div class=\"col-sm-6\"> <p class=\"value_myprofile\"><i class=\"icon-phone\"></i>&nbsp;{{personal.country_code}} {{personal.contactno}}</p> </div> </div> </div> </div> </div> </div> </div> </div> <!-- my bio section --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <h2 class=\"h3 u-heading-v3__title\">About Me</h2> <p class=\"mybio_p\" ng-if=\"personal.about\">{{personal.about}}</p> <p class=\"mybio_p\" ng-if=\"!personal.about\">No details provided by the worker</p> </div> </div> </div> </div> <!-- end my bio section --> <!--  availability--> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Availability</h2> </div> <div class=\"availability_section m-t-30\"> <div class=\"table-responsive\"> <table class=\"table table-bordered table-striped\"> <thead> <tr> <th style=\"width:30%\">&nbsp;</th> <th style=\"width:10%\">M</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">W</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">F</th> <th style=\"width:10%\">S</th> <th style=\"width:10%\">S</th> </tr> </thead> <tbody> <tr> <td>Before School care <br><span class=\"time_care\">6am - 9am</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.before_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.before_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Day care <br><span class=\"time_care\">9am - 3pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.day_care.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.day_care.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>After School care <br><span class=\"time_care\">3pm - 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.after_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.after_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Overnight care <br><span class=\"time_care\">After 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.overnight.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.overnight.indexOf(day) > -1\"></i> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> </div> <!--  End availability--> <div class=\"row\" ng-if=\"emp_history.length >0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Work Experience</h2> </div> <div class=\"no-data_found\" ng-if=\"emp_history.length == 0\"> <p><i class=\"fa fa-exclamation-triangle\"></i>&nbsp;No record found</p> </div> <div class=\"single_job_experience\" ng-repeat=\"history in emp_history\"> <h3 class=\"section-item-title-1\">{{history.company_name}}</h3> <h4 class=\"job_designation\">{{history.title}} - <span class=\"job-date\"> <span>{{months[history.from_month].name}} {{history.from_year}} - <span ng-if=\"history.to_month != 'null' && history.to_year != 'null'\">{{months[history.to_month].name}} {{history.to_year}}</span> <span ng-if=\"history.to_month == 'null' || history.to_year == 'null' || !history.to_month || !history.to_year\">Present</span> </span></span> </h4> <h4 class=\"location\">{{history.location}}</h4> <p class=\"mybio_p\">{{history.description}}</p> </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"requirements.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">credentials</h2> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Trust') > -1\"> <h4>Trust</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                                                      <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\">\n" +
    "                                                                        <div class=\"disp_profile_table\" >\n" +
    "                                                                           <div class=\"icon_class_profile\">\n" +
    "                                                                              <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                                                           </div>\n" +
    "                                                                           <div class=\"icon_class_content\">\n" +
    "                                                                              <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                                                           </div>\n" +
    "                                                                        </div>\n" +
    "                                                                      </li>\n" +
    "                                                                    </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Residency') > -1\"> <h4>Residency</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                                                    <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\">\n" +
    "                                                                      <div class=\"disp_profile_table\" >\n" +
    "                                                                         <div class=\"icon_class_profile\">\n" +
    "                                                                            <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                                                         </div>\n" +
    "                                                                         <div class=\"icon_class_content\">\n" +
    "                                                                            <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                                                         </div>\n" +
    "                                                                      </div>\n" +
    "                                                                    </li>\n" +
    "                                                                  </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Training and experience') > -1\"> <h4>Training & Education</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                                                    <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\">\n" +
    "                                                                      <div class=\"disp_profile_table\" >\n" +
    "                                                                         <div class=\"icon_class_profile\">\n" +
    "                                                                            <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                                                         </div>\n" +
    "                                                                         <div class=\"icon_class_content\">\n" +
    "                                                                            <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                                                         </div>\n" +
    "                                                                      </div>\n" +
    "                                                                    </li>\n" +
    "                                                                  </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Driving') > -1\"> <h4>Driving</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                                                    <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\">\n" +
    "                                                                      <div class=\"disp_profile_table\" >\n" +
    "                                                                         <div class=\"icon_class_profile\">\n" +
    "                                                                            <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                                                         </div>\n" +
    "                                                                         <div class=\"icon_class_content\">\n" +
    "                                                                            <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                                                         </div>\n" +
    "                                                                      </div>\n" +
    "\n" +
    "                                                                    </li>\n" +
    "                                                                  </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Others') > -1\"> <h4>Others</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                                                      <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\">\n" +
    "                                                                        <div class=\"disp_profile_table\" >\n" +
    "                                                                           <div class=\"icon_class_profile\">\n" +
    "                                                                              <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                                                           </div>\n" +
    "                                                                           <div class=\"icon_class_content\">\n" +
    "                                                                              <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                                                           </div>\n" +
    "                                                                        </div>\n" +
    "                                                                      </li>\n" +
    "                                                                    </ul> --> </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"others.interest\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Others</h2> </div> <div class=\"others_section_view\"> <h4>Interests & Favourites</h4> <p class=\"mybio_p\">{{others.interest}}</p> </div> <div class=\"others_section_view\" ng-if=\"others.household_chores.length > 0\"> <h4>Household Chores</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"chore in others.household_chores\"> <p class=\"chore_value\"><i class=\"fa fa-check\"></i>&nbsp;{{chore | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "                                                                        <li ng-repeat=\"chore in others.household_chores\"><i class=\"fa fa-check\"></i>&nbsp;{{chore}}</li>\n" +
    "                                                                     </ul> --> </div> <div class=\"others_section_view\" ng-if=\"others.languages.length > 0\"> <h4>Languages</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"language in others.languages\"> <p class=\"chore_value\"><i class=\"fa fa-check\"></i>&nbsp;{{language | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "                                                                        <li ng-repeat=\"language in others.languages\"><i class=\"fa fa-check\"></i>&nbsp;{{language}}</li>\n" +
    "                                                                     </ul> --> </div> <div class=\"others_section_view\"> <h4>Emergency Contact</h4> <p class=\"reference_p_view\" ng-repeat=\"emergency in emp_emergencycontact\"><i class=\"fa fa-check\"></i>&nbsp;&nbsp;{{emergency.first_name}} {{emergency.last_name}} <span class=\"contact_no_css\">( {{emergency.contactno}} )</span></p> <div ng-if=\"emp_emergencycontact.length == 0\"> No data found!! </div> </div> </div> </div> </div> </div> <!-- References --> <div class=\"row\" ng-if=\"references.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">References</h2> </div> <div class=\"table-responsive reference_table\"> <table class=\"table table-bordered table-striped\"> <tr> <th>Name</th> <th>Email</th> <th>Phone Number</th> </tr> <tr ng-repeat=\"ref in references\"> <td>{{ref.first_name}} {{ref.last_name}}</td> <td>{{ref.email}}</td> <td>{{ref.contactno}}</td> </tr> </table> </div> <!-- <div class=\"single_reference_section_view\" ng-repeat=\"ref in references\">\n" +
    "                                                                  <a href=\"/assets/documents/references/{{ref.document}}\" download=\"{{ref.document}}\" ng-if=\"ref.document !='null' && ref.document\"><img src=\"styles/images/dd.png\" width=\"16\" uib-tooltip=\"Download\"></a>\n" +
    "                                                                  <h4><i class=\"icon-user\"></i>&nbsp;{{ref.first_name}} {{ref.last_name}} |&nbsp;<i class=\"icon-envelope-open\"></i>&nbsp;{{ref.email}}&nbsp;|&nbsp;<i class=\"icon-phone\"></i>&nbsp;{{ref.contactno}}</h4>\n" +
    "\n" +
    "\n" +
    "                                                               </div> --> </div> </div> </div> </div> <!--  End Reference--> <!--  others--> <!--  End Credential--> </div> </div> <!-- <div class=\"first_section success_container\">\n" +
    "                                                      <div class=\"row\">\n" +
    "                                                          <div class=\"col-md-12\">\n" +
    "                                                              <div class=\"main_profile_confirm_section\">\n" +
    "\n" +
    "                                                                <div class=\"row\">\n" +
    "                                                                  <div class=\"col-sm-3 col-xs-12 text-center\">\n" +
    "                                                                    <div class=\"profile-userpic\" ng-if=\"job.confirmed_info.userid__profpic\">\n" +
    "                                                                        <img ng-src=\"/images/profilepic/{{job.confirmed_info.userid__profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"confirmed_user_photo \">\n" +
    "                                                                    </div>\n" +
    "                                                                    <div class=\"profile-userpic\" ng-if=\"!job.confirmed_info.userid__profpic\">\n" +
    "                                                                        <img ng-src=\"/images/no_image_1.png\" class=\"confirmed_user_photo \">\n" +
    "                                                                    </div>\n" +
    "\n" +
    "                                                                  </div>\n" +
    "                                                                  <div class=\"col-sm-5 col-xs-12\">\n" +
    "                                                                    <div class=\"profile-usertitle\">\n" +
    "                                                                        <div class=\"profile-usertitle-name \">\n" +
    "                                                                            {{job.confirmed_info.userid__first_name}} {{job.confirmed_info.userid__last_name}}\n" +
    "                                                                        </div>\n" +
    "                                                                        <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars>\n" +
    "                                                                        <h5 class=\"id_section1  ng-binding\"><i class=\"fa fa-envelope\"></i>&nbsp;{{job.confirmed_info.userid__email}}</h5>\n" +
    "                                                                    </div>\n" +
    "\n" +
    "\n" +
    "                                                                  </div>\n" +
    "                                                                  <div class=\"col-sm-4 col-xs-12 text-center\">\n" +
    "                                                                    <h5 class=\"id_section1  ng-binding\"><i class=\"fa fa-phone\"></i>&nbsp;{{job.confirmed_info.userid__country_code__code}}{{job.confirmed_info.userid__contactno}}</h5>\n" +
    "                                                                    <ul class=\"action_buttons_confirm\">\n" +
    "                                                                      <li ><a class=\"btn hvr-float-shadow view_profile_btn\" href=\"\" uib-tooltip=\"View Profile\"><i class=\"fa fa-user\"></i></a></li>\n" +
    "                                                                      <li><a class=\"btn hvr-float-shadow view_review_btn\" href=\"\" uib-tooltip=\"View Reviews\"><i class=\"fa fa-comments\"></i></a></li>\n" +
    "                                                                    </ul>\n" +
    "\n" +
    "\n" +
    "                                                                  </div>\n" +
    "\n" +
    "                                                                </div>\n" +
    "\n" +
    "                                                              </div>\n" +
    "                                                              <div class=\"requirement_section m-t-20\">\n" +
    "                                                                <h3 class=\"requirement_heading\">Requirements</h3>\n" +
    "\n" +
    "                                                                  <div class=\"tag_section tag_sectionneww tag_section_tab \">\n" +
    "                                                                      <ul>\n" +
    "                                                                          <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><b class=\"req_list_b\">{{x.requirement}}</b></span></li>\n" +
    "                                                                      </ul>\n" +
    "                                                                  </div>\n" +
    "                                                              </div>\n" +
    "                                                              <div ng-if=\"job.confirmed_info.userid__about\">\n" +
    "                                                                <h3 class=\"requirement_heading\">Description</h3>\n" +
    "                                                                <p class=\"bid_other_details other_tab_p\">{{job.confirmed_info.userid__about}}</p>\n" +
    "                                                              </div>\n" +
    "                                                          </div>\n" +
    "                                                      </div>\n" +
    "                                                  </div> --> </div> <div id=\"tab5\" class=\"tab_content\" ng-if=\"currentTabpreview == 'workerfeed'\"> <!-- <h3 class=\"requirement_heading\">Worker's Feedback</h3> --> <form name=\"feedbackForm\"> <div class=\"postjob_type_section\"> <div class=\"form-group\"> <h4>Rating:&nbsp;<span class=\"ratingstars\"><input-stars max=\"5\" ng-attr-readonly=\"true\" name=\"star\" ng-model=\"job.client_review.star\" allow-half ng-required=\"true\"></input-stars></span></h4> </div> <div class=\"form-group m-t-30\"> <span class=\"review_msg_btn\">Review Message</span> <span><textarea class=\"form-control post_forum_textarea\" height=\"100\" name=\"review\" ng-model=\"job.client_review.review\" ng-readonly=\"true\" ng-required=\"true\"></textarea></span> <div class=\"errormsg\" ng-if=\"feedbackForm.$submitted && feedbackForm.review.$invalid\"> <!-- <p ng-if=\"feedbackForm.review.$error.required\">Please enter your review </p> --> </div> </div> </div> </form> </div> </div> </div> </div> </div> <!--  End vertical tab section--> </div> </div> </div> </div> </div> <div class=\"col-md-3 hidden-sm hidden-xs\"> <!-- <div class=\"right_side_section_joblist\">\n" +
    "            <div class=\"ad_section\">\n" +
    "                <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"right_side_section_joblist\">\n" +
    "            <div class=\"ad_section\">\n" +
    "                <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "            </div>\n" +
    "        </div> --> <div x-sticky=\"\" set-class-when-at-top=\"ad-fixed\"> <div ad2></div> </div> </div>  </section>"
  );


  $templateCache.put('scripts/module/job/views/worker_preview_job.html',
    "<div header ng-if=\"!adminLogged\"></div> <section id=\"job_list_section\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"top_row clearfix\"> <div class=\"col-sm-12\"> <div class=\"row\"> <div class=\"col-sm-8\"> <ol class=\"breadcrumb\"> <li><a href=\"javascript:void(0)\">My Jobs</a></li> <li class=\"active\">View Job</li> </ol> </div> <div class=\"col-sm-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"!adminLogged\" href=\"#/joblist/{{workerprevtab}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back to {{workerprevtab}} jobs list</a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"adminLogged\" href=\"{{adminUrl}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back</a> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"single_joblist\"> <div class=\"shape\" ng-if=\"job.quote_count > 0 && job.is_confirmed == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 2\"> <div class=\"shape-text1\"> Confirmed </div> </div> <div class=\"shape1\" ng-if=\"job.is_confirmed == 1 && job.job_status == 3\"> <div class=\"shape-text1 w_complt\"> Completed </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 4\"> <div class=\"shape-text1 paid_txt\"> Paid </div> </div> <div class=\"shape2\" ng-if=\"job.job_status == 7\"> <div class=\"shape-text1 paid_txt\"> Closed </div> </div> <p class=\"job_id ng-binding\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\"> {{job.caregiver_type}}</h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\"></span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / One-off care<span class=\"label label-primary time_label\"></span></h5> <h4 class=\"caregiver_name\">{{job.care_name}}</h4> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-nightcare_gray pull-left\"></i><span class=\"p-l-5\">Night time care</span></span> </li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-daycare_gray pull-left\"></i><span class=\"p-l-5\">Day time care</span></span> </li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-beforeschool_gray pull-left\"></i><span class=\"p-l-5\">Before school care</span></span> </li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-afterschool_gray pull-left\"></i><span class=\"p-l-5\">After school care</span></span> </li> </ul> </div> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span>{{job.state}}, {{job.city}} - {{job.post_code}}</span> </div> </div> <div class=\"right_btn_section\"> <div class=\"text-center\" ng-if=\"job_status != 1 && !job.is_bidded\"> <span ng-if=\"job.is_confirmed == 0\"> <h5 class=\"budget_label\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> </span> <span ng-if=\"job.job_status == 2 || job.job_status == 3\"> <h5 class=\"budget_label p-t-15\">Funds transfered<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.quote}}</h4> </span> <span ng-if=\"job.job_status == 4\"> <h5 class=\"budget_label p-t-15\">Payment released<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.quote}}</h4> </span> <span class=\"apply_btn c_fb text-center w_fb_btn\" ng-if=\"job.job_status == 4\"> <button class=\"btn btn-default\" ng-if=\"!job.has_review_given\" ng-click=\"openFeedbackpopup(job.jobid,job.user_id,'worker',job)\">Rate Client</button> <button class=\"btn btn-default\" ng-if=\"job.has_review_given\" ng-click=\"getFeedback(job.jobid,'client')\">My Feedback</button> <button class=\"btn btn-default btn-block\" ng-click=\"closeJob(job.jobid,job)\" ng-if=\"job.job_status == 4 && adminLogged\"> Close job </button> </span> </div> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container_parentjoblist\"> <div class=\"date_section\"> <h5 ng-if=\"job.job_type == 0\">Date</h5> <h5 ng-if=\"job.job_type == 1\">Start Date</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-calendar fs-12\"></i>&nbsp;{{job.time_budget.date | date:\"dd-MM-yyyy\"}}</span></h4> </div> <div class=\"time_section text-center\"> <h5>Start Time</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-clock fs-12\"></i>&nbsp;{{job.time_budget.time | date:\"HH:MM\"}}</span></h4> </div> <div class=\"viewjob_section text-center\"> <h5>Duration</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-hourglass fs-12\"></i>&nbsp;{{job.time_budget.hours_required}} hrs</span></h4> </div> <div class=\"days_section\"> <h5><span><i class=\"fa fa-square fs-11 color_orange\"></i>&nbsp;</span>Required Days</h5> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> </div> </div> </div> <!-- end rating section --> <!--  vertical tab section--> <div class=\"detail_vertical_side\"> <div class=\"col-md-12 vertical_tab m-t-20\"> <div class=\"tabs_wrapper\"> <ul class=\"tabs\"> <li rel=\"tab1\" ng-click=\"switchTab('jobdetails')\" ng-class=\"{'active':currentTabpreview == 'jobdetails'}\" id=\"personal\"><i class=\"fa fa-briefcase\"></i>&nbsp;Job Details </li> <li rel=\"tab2\" ng-click=\"switchTab('messages')\" ng-class=\"{'active':currentTabpreview == 'messages'}\" id=\"messages\"><i class=\"fa fa-envelope\"></i>&nbsp;Messages</li> <li rel=\"tab3\" ng-if=\"job.job_status == 4\" ng-click=\"switchTab('clientfeed');getFeedback(job.jobid,'worker',1)\" ng-class=\"{'active':currentTabpreview == 'clientfeed'}\" id=\"clientfeed\"><i class=\"fa fa-comments\"></i>&nbsp;Client's Feedback</li> <li rel=\"tab4\" ng-if=\"job.is_confirmed == 1\" ng-click=\"switchTab('contactinfo')\" ng-class=\"{'active':currentTabpreview == 'contactinfo'}\" id=\"contactinfo\"><i class=\"fa fa-address-card\"></i>&nbsp;Contact Information</li> </ul> <div class=\"tab_container\"> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab1\" ng-click=\"switchTab('jobdetails')\">Job Details</h3> <div id=\"tab1\" class=\"tab_content wqeqwe1weq\" ng-if=\"currentTabpreview == 'jobdetails'\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"job_detail_firsttab\"> <div class=\"col-md-12\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-3 rightside_boot_panel\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"requirement_heading\">Requirements</h3> <div class=\"tag_section tag_section_tab\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><b class=\"req_list_b\">{{x.requirement}}</b></span></li> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h3 class=\"requirement_heading\">Other Details</h3> <div class=\"otherdetail_content\"> <p ng-bind-html=\"job.other_requirement\" class=\"other_tab_p\"></p> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab2\" ng-click=\"switchTab('messages')\">Messages</h3> <div id=\"tab2\" class=\"tab_content fgdfgdf2\" ng-if=\"currentTabpreview == 'messages'\"> <div class=\"\"> <div class=\"private_message_section private_message_sectionnew pp_msg\"> <div class=\"row\"> <div class=\"col-md-12 pills_design toggle_button_switch\" ng-if=\"job.job_status >= 1\"> <!-- <toggle-switch ng-model=\"switchStatus\" knob-label=\"Change\" on-label=\"Private\" off-label=\"Public\">\n" +
    "                                                            </toggle-switch> --> <div class=\"toggleWrapper\"> <input type=\"checkbox\" ng-model=\"messageMode\" ng-true-value=\"'public'\" ng-false-value=\"'private'\" ng-click=\"resetMsgs(messageMode,job.user_id)\" id=\"dn\" class=\"dn\"> <label for=\"dn\" class=\"toggle\"><span class=\"toggle__handler\"></span></label> </div> <!-- <ul class=\"nav nav-pills pull-right\">\n" +
    "                                                          <li class=\"active\"><a data-target=\"#public\" ng-click=\"resetMsgs('public')\" aria-controls=\"public\" role=\"tab\" data-toggle=\"pill\"> Public</a></li>\n" +
    "                                                          <li><a data-target=\"#private\" aria-controls=\"private\" ng-click=\"resetMsgs('private')\" role=\"tab\" data-toggle=\"pill\"> Private</a></li>\n" +
    "\n" +
    "                                                          </ul> --> </div> </div> <div> <!-- Nav tabs --> <!-- Tab panes --> <div class=\"tab-content\"> <div ng-if=\"messageMode == 'public'\"> <div class=\"row\"> <div class=\"col-md-12\"> <form name=\"messageForm\" ng-submit=\"clientSentmsg('public',job.jobid)\"> <div class=\"col-md-12\"> <textarea ng-model=\"input.message\" name=\"message\" class=\"form-control counted m-b-10\" name=\"message\" placeholder=\"Enter your message\" maxlength=\"320\" limit-to=\"320\" rows=\"3\" ng-required=\"true\"></textarea> <!-- <div class=\"errormsg\" ng-if=\"messageForm.message.$invalid\">\n" +
    "                                                                                <p ng-if=\"messageForm.message.$error.required\">*Message cannot be empty</p>\n" +
    "                                                                            </div> --> <div class=\"row\"> <div class=\"col-sm-6\"> <h6 class=\"pull-left character_count\">{{ 320 - input.message.length}} characters remaining</h6> </div> <div class=\"col-sm-6 messagesubbtn\"> <button type=\"submit\" class=\"btn btn-default submit_btn_pvj pull-right\">Submit</button> </div> </div> </div> </form> </div> <div class=\"col-md-12 m-t-20\"> <div class=\"public_message_div clearfix\" ng-if=\"messages.length > 0\"> <div class=\"single_public_message single_public_messagenew\" ng-repeat=\"message in messages\"> <div class=\"media m-b-20\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{message.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{message.from_id__first_name}} {{message.from_id__last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{message.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{message.message}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"!showReplybox[$index]\" ng-click=\"showReplybox[$index] = true\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"showReplybox[$index]\" ng-click=\"showReplybox[$index] = !showReplybox[$index]\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(message.on_date_time) && currentUser == message.from_id__id\" ng-click=\"deleteMessage(message.id, message.from_id__id)\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.reply\"> <span class=\"input-group-addon\" ng-click=\"sentReply(message.id,message);showReplybox[$index] = false;\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"secondary_comment_section secondary_comment_sectionnew\" ng-repeat=\"reply in message.replies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{reply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\" ng-init=\"showsubReplybox[$index] = false\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{reply.from_user_first_name}} {{reply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{reply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{reply.reply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\" ng-click=\"showsubReplybox[$index] = !showsubReplybox[$index]\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(reply.on_date_time) && currentUser == reply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteReply(reply.id)\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showsubReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.subreply\"> <span class=\"input-group-addon\" ng-click=\"sentsubReply(reply.id,reply)\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"third_comment_section third_comment_sectionnew\" ng-repeat=\"subreply in reply.subreplies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{subreply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{subreply.from_user_first_name}} {{subreply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{subreply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{subreply.subreply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(subreply.on_date_time) && currentUser == subreply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteSubreply(subreply.id)\"></i></a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div ng-if=\"messageMode == 'private'\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"right_side_chat_section clearfix\"> <div class=\"panel panel-default col-xs-12 p-0\"> <div class=\"panel-body\" id=\"pvtSection\" scrolltobottom ng-scrollbars ng-scrollbars-update=\"updatepvtScrollbar\" ng-scrollbars-config=\"leftprivate\"> <div class=\"chat\" scroll-to-bottom=\"messages\"> <ul class=\"col-xs-12 p-0 m-0\"> <li id=\"privateMsg{{$index}}\" class=\"privatemsgsection single_public_message single_public_messagenew\" ng-repeat=\"privatemsg in messages\" ng-class=\"{'otheruser':currentUser != privatemsg.from_id__id,'its_meyou':currentUser == privatemsg.from_id__id}\"> <div class=\"media\"> <div class=\"media-leftside\"> <a class=\"user\" href=\"javascript:void(0)\"><img alt=\"\" ng-src=\"images/profilepic/{{privatemsg.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\"></a> </div> <div class=\"media-rightside\"> <div class=\"message\"> <p class=\"m-0 p-0 col-md-12 col-xs-12 paraprvmsg\"> {{privatemsg.message}} </p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(privatemsg.on_date_time) && currentUser == privatemsg.from_id__id\" ng-click=\"deleteMessage(privatemsg.id, privatemsg.from_id__id,'private')\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <span class=\"datetimeset_spn m-0 p-0 col-md-12 col-xs-12 text-right\">{{privatemsg.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> </div> </div> </li> </ul> </div> </div> <div class=\"panel-footer\"> <form name=\"pvtmessageForm\" ng-submit=\"clientSentmsg('private' ,job.jobid, job.user_id)\"> <div class=\"input-group\"> <input id=\"btn-input\" type=\"text\" autocomplete=\"off\" class=\"form-control input-sm\" placeholder=\"Type your message here...\" ng-required=\"true\" ng-model=\"input.message\"> <span class=\"input-group-btn\"> <button type=\"submit\" class=\"btn btn-warning btn-sm btn-chat\" id=\"btn-chat\"> <i class=\"fa fa-paper-plane\"></i></button> </span> </div> </form> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" ng-if=\"job.job_status == 4\" rel=\"tab3\" ng-click=\"switchTab('clientfeed');getFeedback(job.jobid,'worker',1)\">Client's Feedback</h3> <div id=\"tab3\" class=\"tab_content\" ng-if=\"currentTabpreview == 'clientfeed'\"> <form name=\"feedbackForm\"> <div class=\"postjob_type_section\"> <div class=\"form-group\"> <h4>Rating</h4> <span class=\"ratingstars\"><input-stars max=\"5\" ng-attr-readonly=\"{{disableFeedback}}\" name=\"star\" ng-model=\"clientfeedback.star\" allow-half ng-required=\"true\"></input-stars></span> </div> <div class=\"form-group\"> <h4>Feedback of you by the client</h4> <span><textarea class=\"form-control post_forum_textarea\" height=\"100\" name=\"review\" ng-model=\"clientfeedback.review\" ng-readonly=\"disableFeedback\" ng-required=\"true\"></textarea></span> <div class=\"errormsg\" ng-if=\"feedbackForm.$submitted && feedbackForm.review.$invalid\"> <p ng-if=\"feedbackForm.review.$error.required\">Please enter your review </p> </div> </div> </div> </form> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab4\" ng-click=\"switchTab('contactinfo')\">Contact Information</h3> <div id=\"tab4\" class=\"tab_content\" ng-if=\"currentTabpreview == 'contactinfo'\"> <!-- contact information  --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"contact_info text-center\"> <div class=\"text-center\"> <img ng-src=\"/images/profilepic/{{job.user_profilepic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\"> </div> <h4>{{job.user_name}}</h4> <h5><i class=\"fa fa-envelope\"></i>&nbsp;&nbsp;{{job.user_email}}</h5> <h5><i class=\"fa fa-phone\"></i>&nbsp;&nbsp;{{job.user_contactno}}</h5> <h4></h4> </div> </div> </div> <!-- contact information ends here --> </div> </div> </div> </div> </div> <!--  End vertical tab section--> </div> </div> </div> </div> </div> <div class=\"col-md-3 hidden-sm hidden-xs\"> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div>  </section>"
  );


  $templateCache.put('scripts/module/job/views/workerpreview_backup.html',
    "<div header ng-if=\"!adminLogged\"></div> <section id=\"job_list_section\" x-sticky-boundary=\"\"> <div class=\"row-fluid\"> <div class=\"col-md-9 col-sm-12\"> <div class=\"row\"> <div class=\"top_row clearfix\"> <div class=\"col-sm-12\"> <div class=\"row\"> <div class=\"col-sm-8\"> <ol class=\"breadcrumb\"> <li><a href=\"javascript:void(0)\">My Jobs</a></li> <li class=\"active\">View Job</li> </ol> </div> <div class=\"col-sm-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"!adminLogged\" href=\"#/joblist/{{workerprevtab}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back to {{workerprevtab}} jobs list</a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"adminLogged\" href=\"{{adminUrl}}\"><i class=\"fa fa-reply\"></i>&nbsp;Back</a> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"single_joblist\"> <div class=\"shape\" ng-if=\"job.quote_count > 0 && job.is_confirmed == 0\"> <div class=\"shape-text\"> Quoted </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 2\"> <div class=\"shape-text1\"> Confirmed </div> </div> <div class=\"shape1\" ng-if=\"job.is_confirmed == 1 && job.job_status == 3\"> <div class=\"shape-text1 w_complt\"> Completed </div> </div> <div class=\"shape1\" ng-if=\"job.job_status == 4\"> <div class=\"shape-text1 paid_txt\"> Paid </div> </div> <div class=\"shape2\" ng-if=\"job.job_status == 7\"> <div class=\"shape-text1 paid_txt\"> Closed </div> </div> <p class=\"job_id ng-binding\"><span>Job ID:</span>&nbsp;{{job.jobid}}</p> <div class=\"detail_job_list_container\"> <div class=\"detail_section\"> <h4 class=\"agency_name\"> {{job.caregiver_type}}</h4> <h5 class=\"care_type\" ng-if=\"job.job_type == 1\">Regular care<span class=\"label label-primary time_label\"></span></h5> <h5 class=\"care_type\" ng-if=\"job.job_type == 0\">Casual / One-off care<span class=\"label label-primary time_label\"></span></h5> <h4 class=\"caregiver_name\">{{job.care_name}}</h4> <div class=\"label_section\"> <ul> <li ng-if=\"job.care_type.indexOf('night') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-nightcare_gray pull-left\"></i><span class=\"p-l-5\">Night time care</span></span> </li> <li ng-if=\"job.care_type.indexOf('day') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-daycare_gray pull-left\"></i><span class=\"p-l-5\">Day time care</span></span> </li> <li ng-if=\"job.case_session.indexOf('Before school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-beforeschool_gray pull-left\"></i><span class=\"p-l-5\">Before school care</span></span> </li> <li ng-if=\"job.case_session.indexOf('After school care') > -1\"><span class=\"icon_label pull-left\"><i class=\"bg-afterschool_gray pull-left\"></i><span class=\"p-l-5\">After school care</span></span> </li> </ul> </div> <div class=\"location_section\" ng-show=\"job.location=='0' && job.state != ''\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span>{{job.state}}, {{job.city}} - {{job.post_code}}</span> </div> </div> <div class=\"right_btn_section\"> <div class=\"text-center\" ng-if=\"job_status != 1 && !job.is_bidded\"> <span ng-if=\"job.is_confirmed == 0\"> <h5 class=\"budget_label\">Budget<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget available for payment to a worker.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.time_budget.budget}}</h4> </span> <span ng-if=\"job.job_status == 2 || job.job_status == 3\"> <h5 class=\"budget_label p-t-15\">Funds transfered<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.quote}}</h4> </span> <span ng-if=\"job.job_status == 4\"> <h5 class=\"budget_label p-t-15\">Payment released<i class=\"icon-info circle_info\" uib-tooltip=\"This is the net budget transfer by you for this job.\"></i></h5> <h4 class=\"text-center salary_job\">${{job.confirmed_info.quote}}</h4> </span> <span class=\"apply_btn c_fb text-center w_fb_btn\" ng-if=\"job.job_status == 4\"> <button class=\"btn btn-default\" ng-if=\"!job.has_review_given\" ng-click=\"openFeedbackpopup(job.jobid,job.user_id,'worker',job)\">Rate Client</button> <button class=\"btn btn-default\" ng-if=\"job.has_review_given\" ng-click=\"getFeedback(job.jobid,'client')\">My Feedback</button> <button class=\"btn btn-default btn-block\" ng-click=\"closeJob(job.jobid,job)\" ng-if=\"job.job_status == 4 && adminLogged\"> Close job </button> </span> </div> </div> </div> <!-- rating section --> <div class=\"rating_sec\"> <div class=\"rating_container_parentjoblist\"> <div class=\"date_section\"> <h5 ng-if=\"job.job_type == 0\">Date</h5> <h5 ng-if=\"job.job_type == 1\">Start Date</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-calendar fs-12\"></i>&nbsp;{{job.time_budget.date | date:\"dd-MM-yyyy\"}}</span></h4> </div> <div class=\"time_section text-center\"> <h5>Start Time</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-clock fs-12\"></i>&nbsp;{{job.time_budget.time | date:\"HH:MM\"}}</span></h4> </div> <div class=\"viewjob_section text-center\"> <h5>Duration</h5> <h4><span class=\"datetime_job_list\"><i class=\"icon-hourglass fs-12\"></i>&nbsp;{{job.time_budget.hours_required}} hrs</span></h4> </div> <div class=\"days_section\"> <h5><span><i class=\"fa fa-square fs-11 color_orange\"></i>&nbsp;</span>Required Days</h5> <div class=\"day_selector\" ng-if=\"job.job_type == 0\" ng-init=\"day = getdayFromdate(job.time_budget.date)\"> <ul> <li ng-class=\"{'active':day == 0}\">S</li> <li ng-class=\"{'active':day == 1}\">M</li> <li ng-class=\"{'active':day == 2}\">T</li> <li ng-class=\"{'active':day == 3}\">W</li> <li ng-class=\"{'active':day == 4}\">T</li> <li ng-class=\"{'active':day == 5}\">F</li> <li ng-class=\"{'active':day == 6}\">S</li> </ul> </div> </div> </div> </div> <!-- end rating section --> <!--  vertical tab section--> <div class=\"detail_vertical_side\"> <div class=\"col-md-12 vertical_tab m-t-20\"> <div class=\"tabs_wrapper\"> <ul class=\"tabs\"> <li rel=\"tab1\" ng-click=\"switchTab('jobdetails')\" ng-class=\"{'active':currentTabpreview == 'jobdetails'}\" id=\"personal\"><i class=\"fa fa-briefcase\"></i>&nbsp;Job Details </li> <li rel=\"tab2\" ng-click=\"switchTab('messages')\" ng-class=\"{'active':currentTabpreview == 'messages'}\" id=\"messages\"><i class=\"fa fa-envelope\"></i>&nbsp;Messages</li> <li rel=\"tab3\" ng-if=\"job.job_status == 4\" ng-click=\"switchTab('clientfeed');getFeedback(job.jobid,'worker',1)\" ng-class=\"{'active':currentTabpreview == 'clientfeed'}\" id=\"clientfeed\"><i class=\"fa fa-comments\"></i>&nbsp;Client's Feedback</li> <li rel=\"tab4\" ng-if=\"job.is_confirmed == 1\" ng-click=\"switchTab('contactinfo')\" ng-class=\"{'active':currentTabpreview == 'contactinfo'}\" id=\"contactinfo\"><i class=\"fa fa-address-card\"></i>&nbsp;Contact Information</li> </ul> <div class=\"tab_container\"> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab1\" ng-click=\"switchTab('jobdetails')\">Job Details</h3> <div id=\"tab1\" class=\"tab_content wqeqwe1weq\" ng-if=\"currentTabpreview == 'jobdetails'\"> <div class=\"detail_job_view\"> <div class=\"row\"> <div class=\"job_detail_firsttab\"> <div class=\"col-md-12\"> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Number of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.no_of_children}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Age group of children</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">{{job.children_age}}</span> </div> </div> <div class=\"form-group clearfix\"> <label class=\"control-label col-xs-5\">Employer Type</label> <span class=\"col-xs-1 p-0\">:</span> <div class=\"col-xs-6 rigprofcnt right_cntent\"> <span class=\"text_gray\">Parent </span> </div> </div> </div> <div class=\"col-md-3 rightside_boot_panel\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"requirement_section\"> <h3 class=\"requirement_heading\">Requirements</h3> <div class=\"tag_section tag_section_tab\"> <ul> <li ng-repeat=\"x in job.req track by $index\"><span><i class=\"{{x.icon_class}} pull-left\"></i><b class=\"req_list_b\">{{x.requirement}}</b></span></li> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"other_details\"> <h3 class=\"requirement_heading\">Other Details</h3> <div class=\"otherdetail_content\"> <p ng-bind-html=\"job.other_requirement\" class=\"other_tab_p\"></p> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab2\" ng-click=\"switchTab('messages')\">Messages</h3> <div id=\"tab2\" class=\"tab_content fgdfgdf2\" ng-if=\"currentTabpreview == 'messages'\"> <div class=\"\"> <div class=\"private_message_section private_message_sectionnew pp_msg\"> <div class=\"row\"> <div class=\"col-md-12 pills_design toggle_button_switch\" ng-if=\"job.job_status >= 1\"> <!-- <toggle-switch ng-model=\"switchStatus\" knob-label=\"Change\" on-label=\"Private\" off-label=\"Public\">\n" +
    "                                                            </toggle-switch> --> <div class=\"toggleWrapper\"> <input type=\"checkbox\" ng-model=\"messageMode\" ng-true-value=\"'public'\" ng-false-value=\"'private'\" ng-click=\"resetMsgs(messageMode,job.user_id)\" id=\"dn\" class=\"dn\"> <label for=\"dn\" class=\"toggle\"><span class=\"toggle__handler\"></span></label> </div> <!-- <ul class=\"nav nav-pills pull-right\">\n" +
    "                                                          <li class=\"active\"><a data-target=\"#public\" ng-click=\"resetMsgs('public')\" aria-controls=\"public\" role=\"tab\" data-toggle=\"pill\"> Public</a></li>\n" +
    "                                                          <li><a data-target=\"#private\" aria-controls=\"private\" ng-click=\"resetMsgs('private')\" role=\"tab\" data-toggle=\"pill\"> Private</a></li>\n" +
    "\n" +
    "                                                          </ul> --> </div> </div> <div> <!-- Nav tabs --> <!-- Tab panes --> <div class=\"tab-content\"> <div ng-if=\"messageMode == 'public'\"> <div class=\"row\"> <div class=\"col-md-12\"> <form name=\"messageForm\" ng-submit=\"clientSentmsg('public',job.jobid)\"> <div class=\"col-md-12\"> <textarea ng-model=\"input.message\" name=\"message\" class=\"form-control counted m-b-10\" name=\"message\" placeholder=\"Enter your message\" maxlength=\"320\" limit-to=\"320\" rows=\"3\" ng-required=\"true\"></textarea> <!-- <div class=\"errormsg\" ng-if=\"messageForm.message.$invalid\">\n" +
    "                                                                                <p ng-if=\"messageForm.message.$error.required\">*Message cannot be empty</p>\n" +
    "                                                                            </div> --> <div class=\"row\"> <div class=\"col-sm-6\"> <h6 class=\"pull-left character_count\">{{ 320 - input.message.length}} characters remaining</h6> </div> <div class=\"col-sm-6 messagesubbtn\"> <button type=\"submit\" class=\"btn btn-default submit_btn_pvj pull-right\">Submit</button> </div> </div> </div> </form> </div> <div class=\"col-md-12 m-t-20\"> <div class=\"public_message_div clearfix\" ng-if=\"messages.length > 0\"> <div class=\"single_public_message single_public_messagenew\" ng-repeat=\"message in messages\"> <div class=\"media m-b-20\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{message.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{message.from_id__first_name}} {{message.from_id__last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{message.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{message.message}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"!showReplybox[$index]\" ng-click=\"showReplybox[$index] = true\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"showReplybox[$index]\" ng-click=\"showReplybox[$index] = !showReplybox[$index]\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(message.on_date_time) && currentUser == message.from_id__id\" ng-click=\"deleteMessage(message.id, message.from_id__id)\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.reply\"> <span class=\"input-group-addon\" ng-click=\"sentReply(message.id,message);showReplybox[$index] = false;\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"secondary_comment_section secondary_comment_sectionnew\" ng-repeat=\"reply in message.replies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{reply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\" ng-init=\"showsubReplybox[$index] = false\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{reply.from_user_first_name}} {{reply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{reply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{reply.reply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" uib-tooltip=\"Reply\"><i class=\"fa fa-reply\" ng-click=\"showsubReplybox[$index] = !showsubReplybox[$index]\"></i></a> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(reply.on_date_time) && currentUser == reply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteReply(reply.id)\"></i></a> </div> <div class=\"reply_public_msg_input\" ng-if=\"showsubReplybox[$index]\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"input.subreply\"> <span class=\"input-group-addon\" ng-click=\"sentsubReply(reply.id,reply)\"><i class=\"fa fa-paper-plane\"></i></span> </div> </div> </div> </div> <div class=\"third_comment_section third_comment_sectionnew\" ng-repeat=\"subreply in reply.subreplies\"> <div class=\"media\"> <div class=\"media-leftside\"> <img ng-src=\"images/profilepic/{{subreply.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\" class=\"media-object\" style=\"width:60px\"> </div> <div class=\"media-rightside\"> <div class=\"col-md-12 col-xs-12 p-0\"> <h4 class=\"media-heading col-md-6 col-xs-12 p-0\">{{subreply.from_user_first_name}} {{subreply.from_user_last_name}}</h4> <span class=\"datetimespnsls col-md-6 col-xs-12 p-0\">{{subreply.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> <p class=\"message_public_p\">{{subreply.subreply}}</p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(subreply.on_date_time) && currentUser == subreply.from_id__id\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\" ng-click=\"deleteSubreply(subreply.id)\"></i></a> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div ng-if=\"messageMode == 'private'\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"right_side_chat_section clearfix\"> <div class=\"panel panel-default col-xs-12 p-0\"> <div class=\"panel-body\" id=\"pvtSection\" scrolltobottom ng-scrollbars ng-scrollbars-update=\"updatepvtScrollbar\" ng-scrollbars-config=\"leftprivate\"> <div class=\"chat\" scroll-to-bottom=\"messages\"> <ul class=\"col-xs-12 p-0 m-0\"> <li id=\"privateMsg{{$index}}\" class=\"privatemsgsection single_public_message single_public_messagenew\" ng-repeat=\"privatemsg in messages\" ng-class=\"{'otheruser':currentUser != privatemsg.from_id__id,'its_meyou':currentUser == privatemsg.from_id__id}\"> <div class=\"media\"> <div class=\"media-leftside\"> <a class=\"user\" href=\"javascript:void(0)\"><img alt=\"\" ng-src=\"images/profilepic/{{privatemsg.from_id_profpic}}\" ng-error-src=\"/images/no_image_1.png\"></a> </div> <div class=\"media-rightside\"> <div class=\"message\"> <p class=\"m-0 p-0 col-md-12 col-xs-12 paraprvmsg\"> {{privatemsg.message}} </p> <div class=\"activity-meta\"> <a href=\"javascript:void(0)\" ng-if=\"checkDeleteTimer(privatemsg.on_date_time) && currentUser == privatemsg.from_id__id\" ng-click=\"deleteMessage(privatemsg.id, privatemsg.from_id__id,'private')\" uib-tooltip=\"Delete\"><i class=\"fa fa-trash\"></i></a> </div> <span class=\"datetimeset_spn m-0 p-0 col-md-12 col-xs-12 text-right\">{{privatemsg.on_date_time | date:'dd-MM-yyyy hh:mm:ss'}}</span> </div> </div> </div> </li> </ul> </div> </div> <div class=\"panel-footer\"> <form name=\"pvtmessageForm\" ng-submit=\"clientSentmsg('private' ,job.jobid, job.user_id)\"> <div class=\"input-group\"> <input id=\"btn-input\" type=\"text\" autocomplete=\"off\" class=\"form-control input-sm\" placeholder=\"Type your message here...\" ng-required=\"true\" ng-model=\"input.message\"> <span class=\"input-group-btn\"> <button type=\"submit\" class=\"btn btn-warning btn-sm btn-chat\" id=\"btn-chat\"> <i class=\"fa fa-paper-plane\"></i></button> </span> </div> </form> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <h3 class=\"d_active tab_drawer_heading\" ng-if=\"job.job_status == 4\" rel=\"tab3\" ng-click=\"switchTab('clientfeed');getFeedback(job.jobid,'worker',1)\">Client's Feedback</h3> <div id=\"tab3\" class=\"tab_content\" ng-if=\"currentTabpreview == 'clientfeed'\"> <form name=\"feedbackForm\"> <div class=\"postjob_type_section\"> <div class=\"form-group\"> <h4>Rating</h4> <span class=\"ratingstars\"><input-stars max=\"5\" ng-attr-readonly=\"{{disableFeedback}}\" name=\"star\" ng-model=\"clientfeedback.star\" allow-half ng-required=\"true\"></input-stars></span> </div> <div class=\"form-group\"> <h4>Feedback of you by the client</h4> <span><textarea class=\"form-control post_forum_textarea\" height=\"100\" name=\"review\" ng-model=\"clientfeedback.review\" ng-readonly=\"disableFeedback\" ng-required=\"true\"></textarea></span> <div class=\"errormsg\" ng-if=\"feedbackForm.$submitted && feedbackForm.review.$invalid\"> <p ng-if=\"feedbackForm.review.$error.required\">Please enter your review </p> </div> </div> </div> </form> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab4\" ng-click=\"switchTab('contactinfo')\">Contact Information</h3> <div id=\"tab4\" class=\"tab_content\" ng-if=\"currentTabpreview == 'contactinfo'\"> <!-- contact information  --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"contact_info text-center\"> <div class=\"text-center\"> <img ng-src=\"/images/profilepic/{{job.user_profilepic}}\" ng-error-src=\"/images/no_image_1.png\" alt=\"\"> </div> <h4>{{job.user_name}}</h4> <h5><i class=\"fa fa-envelope\"></i>&nbsp;&nbsp;{{job.user_email}}</h5> <h5><i class=\"fa fa-phone\"></i>&nbsp;&nbsp;{{job.user_contactno}}</h5> <h4></h4> </div> </div> </div> <!-- contact information ends here --> </div> </div> </div> </div> </div> <!--  End vertical tab section--> </div> </div> </div> </div> <div class=\"col-md-3 hidden-sm hidden-xs\"> <!-- <div class=\"right_side_section_joblist\">\n" +
    "            <div class=\"ad_section\">\n" +
    "                <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"right_side_section_joblist\">\n" +
    "            <div class=\"ad_section\">\n" +
    "                <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "            </div>\n" +
    "        </div> --> <div x-sticky=\"\" set-class-when-at-top=\"ad-fixed\"> <div ad2></div> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/settings/views/settings.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <!-- <section id=\"job_list_section\">\n" +
    "  <div class=\"container-fluid\">\n" +
    "    <div class=\"row-fluid\">\n" +
    "      <div class=\"col-md-9 col-sm-12\">\n" +
    "        <div class=\"row\">\n" +
    "        <input type=\"checkbox\" ng-model=\"settings.job_email\" ng-checked=\"setting.job_email == 1\" ng-click=\"changeSetting()\">\n" +
    "        I need email whenever new job is posted\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</section> --> <div class=\"content_section\"> <section id=\"job_list_section\"> <div class=\"settings_general\"> <div class=\"row\"> <div class=\"col-md-9\"> <div class=\"change_password\"> <h3 class=\"heading\">Settings</h3> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"panel panel-default material-panel-default\"> <!-- Default panel contents --> <!-- List group --> <ul class=\"list-group\"> <li class=\"list-group-item\"> I need email whenever a new job is posted <div class=\"material-switch pull-right\"> <input ng-model=\"settings.job_email\" ng-checked=\"setting.job_email == 1\" ng-click=\"changeSetting()\" id=\"emailsetting\" type=\"checkbox\"> <label for=\"emailsetting\" class=\"label-success\"></label> </div> </li> </ul> </div> </div> </div> </div> <div class=\"col-md-3 hidden-sm hidden-xs\"> <div class=\"\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </div> </section> </div> <!-- footer section comes here --> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/templates/confirmbox.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">If you close the process, you will no longer be able to use this save data.Would you like to terminate the process?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/user/templates/cropimage.html',
    "<div class=\"modal-body\"> <div class=\"cropArea\"> <img-crop image=\"myImage\" result-image=\"myCroppedImage\" area-min-size=\"200\" area-type=\"square\" result-image-size=\"150\" on-load-done=\"testFunc()\"></img-crop> </div> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/user/templates/docDelete.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">Are you sure you want to remove this requirement?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "            padding-bottom: 0px;\n" +
    "        }\n" +
    "        .bootbox  .modal-dialog{\n" +
    "          width:500px;\n" +
    "        }\n" +
    "        \n" +
    "        /* @media screen and (max-width: 767px) {\n" +
    "          .bootbox  .modal-dialog{\n" +
    "            width:auto;\n" +
    "          }\n" +
    "        } */</style>"
  );


  $templateCache.put('scripts/module/user/templates/emergency.html',
    "<div class=\"modal-body\"> <div> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12 add_experience_form\"> <form class=\"m-t-30\" name=\"emergencyForm\" novalidate ng-submit=\"saveEmergencycontact(emergencyForm,EmergencyAction)\"> <h4 class=\"title_modal\">{{EmergencyAction}} Emergency contact</h4> <div class=\"form-group\"> <label for=\"title\">First name</label> <input type=\"text\" class=\"form-control\" name=\"first_name\" ng-model=\"emergency.first_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(emergencyForm.$submitted || emergencyForm.first_name.$dirty) && emergencyForm.first_name.$invalid\"> <p ng-if=\"emergencyForm.first_name.$error.required\">Please enter first name</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Last name</label> <input type=\"text\" class=\"form-control\" name=\"last_name\" ng-model=\"emergency.last_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(emergencyForm.$submitted || emergencyForm.last_name.$dirty) && emergencyForm.last_name.$invalid\"> <p ng-if=\"emergencyForm.last_name.$error.required\">Please enter last name</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Email</label> <input type=\"text\" class=\"form-control\" name=\"email\" ng-model=\"emergency.email\" ng-required=\"true\" ng-pattern=\"/^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$/\"> <div class=\"errormsg\" ng-if=\"(emergencyForm.$submitted || emergencyForm.email.$dirty) && emergencyForm.email.$invalid\"> <p ng-if=\"emergencyForm.email.$error.required\">Please enter email</p> <p ng-if=\"emergencyForm.email.$error.pattern\">Please enter a valid email</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Phone number</label> <input type=\"text\" class=\"form-control\" name=\"contactno\" ng-model=\"emergency.contactno\" ng-required=\"true\" ng-pattern=\"/^[0-9]*$/\" ng-minlength=\"7\" ng-maxlength=\"15\"> <div class=\"errormsg\" ng-if=\"(emergencyForm.$submitted || emergencyForm.contactno.$dirty) && emergencyForm.contactno.$invalid\"> <p ng-if=\"emergencyForm.contactno.$error.required\">Please enter phone number</p> <p ng-if=\"emergencyForm.contactno.$error.pattern\">Phone number cannot contain alphabets</p> <p ng-if=\"emergencyForm.contactno.$error.maxlength\">Invalid phone number</p> <p ng-if=\"emergencyForm.contactno.$error.minlength\">Invalid phone number</p> </div> </div> <!-- <div class=\"col-md-12\">\n" +
    "                        <div class=\"upload_section_popup\">\n" +
    "                        <h4>Reference document</h4>\n" +
    "                        <h5>Add or link to external documents, photos, sites, videos, and presentations.</h5>\n" +
    "                        <button class=\"upload_btn btn btn-default\" ngf-select=\"refDocumentupload($file)\" ng-model=\"referenceDoc\" name=\"proof_file\">upload</button>\n" +
    "                        <span ng-bind=\"reference.document\" ng-if=\"reference.document != 'null'\"></span>\n" +
    "                      </div>\n" +
    "\n" +
    "                      </div> --> <div class=\"row\"> <div class=\"col-sm-12 m-t-20 text-right\"> <div class=\"action_section\"> <button type=\"button\" class=\"btn btn btn-default loginbtn\" ng-click=\"cancelEmergency(emergencyForm)\">Cancel</button> <button type=\"submit\" class=\"btn btn btn-primary signupbtn\" ng-disabled=\"emerSaving\">Save</button> </div> </div> </div> </form> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('scripts/module/user/templates/experience_history.html',
    "<div class=\"modal-body\"> <div> <ng-form name=\"experienceForm\" novalidate> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12 add_experience_form\"> <form class=\"m-t-30\" name=\"experienceForm\" novalidate ng-submit=\"saveHistory(experienceForm)\"> <h4 class=\"title_modal\">Add Experience</h4> <div class=\"form-group\"> <label for=\"title\">Title</label> <input type=\"text\" class=\"form-control\" name=\"title\" ng-model=\"history.title\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.title.$dirty) && experienceForm.title.$invalid\"> <p ng-if=\"experienceForm.title.$error.required\">Please enter title</p> </div> </div> <div class=\"form-group\"> <label for=\"companyname\">Company Name</label> <input type=\"text\" class=\"form-control\" name=\"companyname\" ng-model=\"history.company_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.companyname.$dirty) && experienceForm.companyname.$invalid\"> <p ng-if=\"experienceForm.companyname.$error.required\">Please enter company name</p> </div> </div> <div class=\"form-group pos_rel_exp\"> <label for=\"location\">Location</label> <p class=\"icon_load_spin\"> <input type=\"text\" autocomplete=\"off\" class=\"form-control location_input_style\" ng-model=\"history.location\" name=\"location\" class=\"form-control text-box\" uib-typeahead=\"location as location.viewval for location in getLocations($viewValue)\" typeahead-wait-ms=\"800\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"setHistorylocation($item,true)\"> <span class=\"ajax_loader1\" ng-if=\"history.location\" title=\"Clear location\" ng-click=\"history.location = '';history.city=false;history.state=false;history.post_code=false\"><i class=\"fa fa-times-circle\"></i></span> <span class=\"ajax_loader1\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.location.$dirty) && (!history.post_code || !history.location) && filteredLocations.length == 0 && !experienceForm.location.$error.required\"> <p>Please enter valid location</p> </div> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.location.$dirty) && experienceForm.location.$invalid && experienceForm.location.$error.required\"> <p>Please enter valid location</p> </div> </p> </div> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"row\"> <div class=\"col-sm-12\"> <label for=\"from_month\">From</label> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" chosen disable-search=\"true\" name=\"from_month\" ng-model=\"history.from_month\" id=\"from_month\" ng-required=\"true\" convert-to-number> <option value=\"\">Month</option> <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_month.$dirty) && experienceForm.from_month.$invalid\"> <p ng-if=\"experienceForm.from_month.$error.required\">Please enter from month</p> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"from_year\" chosen disable-search=\"true\" name=\"from_year\" ng-model=\"history.from_year\" ng-required=\"true\" convert-to-number> <option value=\"\">Year</option> <option ng-repeat=\"year in years\" value=\"{{year}}\">{{year}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_year.$dirty) && experienceForm.from_year.$invalid\"> <p ng-if=\"experienceForm.from_year.$error.required\">Please enter from year</p> </div> </div> </div> </div> </div> <div class=\"col-sm-6\" ng-if=\"history.currentlyWorking == 0\"> <div class=\"row\"> <div class=\"col-md-12\"> <label for=\"to_month\">To</label> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"to_month\" chosen disable-search=\"true\" name=\"to_month\" ng-model=\"history.to_month\" ng-required=\"history.currentlyWorking == 0\" convert-to-number> <option value=\"\">Month</option> <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_month.$dirty) && experienceForm.to_month.$invalid\"> <p ng-if=\"experienceForm.to_month.$error.required\">Please enter To month</p> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"to_year\" chosen disable-search=\"true\" name=\"to_year\" convert-to-number ng-model=\"history.to_year\" ng-required=\"history.currentlyWorking == 0\"> <option value=\"\">Year</option> <option ng-repeat=\"year in getToyear(history.from_year)\" value=\"{{year}}\">{{year}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_year.$dirty) && experienceForm.to_year.$invalid\"> <p ng-if=\"experienceForm.to_year.$error.required\">Please enter To year</p> </div> </div> </div> </div> <!-- <div class=\"form-group chosen_style\">\n" +
    "                      <label for=\"to_month\">To</label>\n" +
    "                      <select class=\"form-control\" id=\"to_month\" chosen disable-search=\"true\" name=\"to_month\" ng-model=\"history.to_month\" ng-required=\"history.currentlyWorking == 0\" convert-to-number>\n" +
    "                        <option value=\"\">Month</option>\n" +
    "                        <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option>\n" +
    "                      </select>\n" +
    "                      <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_month.$dirty) && experienceForm.to_month.$invalid\">\n" +
    "                      <p ng-if=\"experienceForm.to_month.$error.required\">Please enter To month</p>\n" +
    "                  </div>\n" +
    "                      </div>\n" +
    "                      <div class=\"form-group chosen_style\">\n" +
    "                      <select class=\"form-control\" id=\"to_year\" chosen disable-search=\"true\" name=\"to_year\" convert-to-number ng-model=\"history.to_year\" ng-required=\"history.currentlyWorking == 0\">\n" +
    "                        <option value=\"\">Year</option>\n" +
    "                       <option ng-repeat=\"year in getToyear(history.from_year)\" value=\"{{year}}\">{{year}}</option>\n" +
    "                      </select>\n" +
    "                      <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_year.$dirty) && experienceForm.to_year.$invalid\">\n" +
    "                        <p ng-if=\"experienceForm.to_year.$error.required\">Please enter To year</p>\n" +
    "                      </div>\n" +
    "                      </div> --> </div> <div ng-if=\"history.currentlyWorking == 1\"> <label class=\"\">To</label> <div> <label class=\"decorate_present\">Present</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-12 check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> I currently work here <input type=\"checkbox\" ng-model=\"history.currentlyWorking\" ng-true-value=\"1\" ng-false-value=\"0\" ng-click=\"history.to_month = '';history.to_year='';\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"form-group\"> <label for=\"companyname\">Description</label> <textarea class=\"form-control\" ng-model=\"history.description\" ng-required=\"true\" name=\"description\">\n" +
    "                      </textarea> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.description.$dirty) && experienceForm.description.$invalid\"> <p ng-if=\"experienceForm.description.$error.required\">Please enter description</p> <!-- <p ng-if=\"experienceForm.description.$error.minlength\">Must above 200 characters</p>\n" +
    "                        <p ng-if=\"experienceForm.description.$error.maxlength\">Must below 500 characters</p> --> </div> </div> </div> <!-- <div class=\"col-md-12\">\n" +
    "                        <div class=\"upload_section_popup\">\n" +
    "                        <h4>Media</h4>\n" +
    "                        <h5>Add or link to external documents, photos, sites, videos, and presentations.</h5>\n" +
    "                        <button class=\"upload_btn btn btn-default\" ngf-select=\"proofUpload($file)\" ng-model=\"file\" name=\"proof_file\">upload</button>\n" +
    "                        {{history.proof}}\n" +
    "                      </div>\n" +
    "\n" +
    "                      </div> --> </div> <div class=\"row\"> <div class=\"col-sm-12 m-t-20 text-right\"> <div class=\"action_section\"> <button type=\"button\" class=\"btn btn btn-default loginbtn\" ng-click=\"cancelHistory()\">Cancel</button> <button type=\"submit\" class=\"btn btn btn-primary signupbtn\" ng-disabled=\"historySaving\">Save</button> </div> </div> </div> </form></div>  <!-- <div class=\"group\">\n" +
    "                  <input type=\"text\" ng-pattern=\"/^[1-9]\\d*$/\" ng-model=\"experience.title\" ng-required=\"true\" name=\"title\">\n" +
    "                  <span class=\"highlight\"></span>\n" +
    "                  <span class=\"bar\"></span>\n" +
    "                  <label>Title</label>\n" +
    "                  <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.title.$dirty) && experienceForm.title.$invalid\">\n" +
    "                     <p ng-if=\"experienceForm.title.$error.required\">Please enter the Title</p>\n" +
    "                     <p ng-if=\"experienceForm.title.$error.pattern\">Rate per hour must be in number and should not be 0</p>\n" +
    "                  </div>\n" +
    "               </div> --> </div> </div> </ng-form></div>  </div>"
  );


  $templateCache.put('scripts/module/user/templates/reference.html',
    "<div class=\"modal-body\"> <div> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12 add_experience_form\"> <form class=\"m-t-30\" name=\"referenceForm\" novalidate ng-submit=\"saveReference(referenceForm,refAction)\"> <h4 class=\"title_modal\">{{refAction}} reference</h4> <div class=\"form-group\"> <label for=\"title\">First name</label> <input type=\"text\" class=\"form-control\" name=\"name\" ng-model=\"reference.first_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(referenceForm.$submitted || referenceForm.name.$dirty) && referenceForm.name.$invalid\"> <p ng-if=\"referenceForm.name.$error.required\">Please enter name</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Last name</label> <input type=\"text\" class=\"form-control\" name=\"name\" ng-model=\"reference.last_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(referenceForm.$submitted || referenceForm.name.$dirty) && referenceForm.name.$invalid\"> <p ng-if=\"referenceForm.name.$error.required\">Please enter name</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Email</label> <input type=\"text\" class=\"form-control\" name=\"email\" ng-model=\"reference.email\" ng-required=\"true\" ng-pattern=\"/^[a-z]+[a-z0-9._]+@[a-z]+\\.[a-z.]{2,5}$/\"> <div class=\"errormsg\" ng-if=\"(referenceForm.$submitted || referenceForm.email.$dirty) && referenceForm.email.$invalid\"> <p ng-if=\"referenceForm.email.$error.required\">Please enter email</p> <p ng-if=\"referenceForm.email.$error.pattern\">Please enter a valid email</p> </div> </div> <div class=\"form-group\"> <label for=\"title\">Phone number</label> <input type=\"text\" class=\"form-control\" name=\"contactno\" ng-model=\"reference.contactno\" ng-required=\"true\" ng-pattern=\"/^[0-9]*$/\" ng-minlength=\"7\" ng-maxlength=\"15\"> <div class=\"errormsg\" ng-if=\"(referenceForm.$submitted || referenceForm.contactno.$dirty) && referenceForm.contactno.$invalid\"> <p ng-if=\"referenceForm.contactno.$error.required\">Please enter phone number</p> <p ng-if=\"referenceForm.contactno.$error.pattern\">Phone number must contain numbers</p> <p ng-if=\"referenceForm.contactno.$error.minlength\">Invalid phone number</p> <p ng-if=\"referenceForm.contactno.$error.maxlength\">Invalid phone number</p> </div> </div> <div class=\"col-md-12 p-0\"> <div class=\"upload_section_popup\"> <h4>Upload Reference document</h4> <button class=\"upload_btn btn btn-default\" ng-if=\"!reference.document\" ngf-select=\"refDocumentupload($file)\" ng-model=\"file\" ngf-accept=\"'.doc,.docx,.pdf,.png,.jpeg,.jpg,.svg'\" ngf-pattern=\"'.doc,.docx,.pdf,.png,.jpeg,.jpg,.svg'\" name=\"file\">upload</button> <span ng-bind=\"file.name\" class=\"file_name_upload\"></span> <a ng-if=\"file.name\" href=\"javascript:void(0)\"><i class=\"fa fa-times orange_color\" ng-click=\"removerefFile()\"></i></a> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"reference.progress\" ng-show=\"reference.fileUploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> <p class=\"comment_line_css\">* png, pdf, doc, docx, jpeg, jpg, svg</p> </div> <a class=\"btn down_load_resume_btn\" ng-if=\"reference.document && !file.name\" href=\"assets/documents/references/{{reference.document}}\" download=\"{{reference.document}}\">Download document</a> <a href=\"javascript:void(0)\" ng-click=\"deleteReferenceDoc(reference.id,reference); reference.document = null\" ng-if=\"reference.document && !referenceDoc.name\"><i class=\"icon-trash\" uib-tooltip=\"Delete document\"></i></a> </div> <div class=\"row\"> <div class=\"col-sm-12 m-t-20 text-right\"> <div class=\"action_section\"> <button type=\"button\" class=\"btn btn btn-default loginbtn\" ng-click=\"cancelReference()\">Cancel</button> <button type=\"submit\" class=\"btn btn btn-primary signupbtn\" ng-disabled=\"referenceSaving\">Save</button> </div> </div> </div> </form> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('scripts/module/user/templates/signup.html',
    "<div class=\"modal-body\"> <div class=\"row\"> <div class=\"loader_div ng-hide\" ng-show=\"sm.checkemailLoad\"> <div class=\"child_loader\"> <i class=\"fa fa-circle-o-notch fa-spin\" style=\"font-size: 30px\"></i> </div> </div> <div class=\"col-md-12 signup_form\"> <h3 class=\"title_modal\">Signup</h3> <h5 class=\"sub-header-popup\">for your account today</h5> <div ng-show=\"sm.signupStep == 'step1'\"> <form class=\"login_form\" name=\"signup_form\" ng-submit=\"sm.addEmail(signup_form)\" novalidate> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" autocomplete=\"off\" ng-change=\"sm.checkemail()\" name=\"email\" ng-required=\"true\" ng-model=\"sm.user.email\" ng-pattern=\"/^[a-zA-Z]+[a-z0-9._A-Z]+@[a-z]+\\.[a-z.]{2,5}$/\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Email</label> <p ng-if=\"!signup_form.email.$error.required && !sm.emailunique\" class=\"errormsg\">Email Already exists</p> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.email.$touched) && signup_form.email.$invalid\"> <p ng-if=\"signup_form.email.$error.required\">Email is required</p> <p ng-if=\"signup_form.email.$error.pattern\">Please enter a valid email</p> </div> </div> </div> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"password\" ng-model-options=\"{ 'updateOn': 'blur'}\" ng-readonly=\"sm.checkemailLoad\" name=\"password\" ng-minlength=\"6\" ng-model=\"sm.user.password\" required autocomplete=\"off\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label ng-class=\"{'label_email_valid':sm.checkemailLoad}\">Password</label> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.password.$dirty) && signup_form.password.$invalid\"> <p ng-if=\"signup_form.password.$error.required\">Password is required</p> <p ng-if=\"signup_form.password.$error.pattern\">Password should contain an Uppercase, a Lowercase and a number</p> <p ng-if=\"signup_form.password.$error.minlength\">Try a password with at least 6 characters.</p> </div> </div> </div> <button type=\"submit\" class=\"btn btn-default join_button hvr-wobble-horizontal\">Join</button> <p class=\"no_account_link\">Already have an account? <span class=\"signup_link\" ng-click=\"sm.callLoginpopup()\">Login </span> </p> <h2 class=\"line_section hide\"> <span class=\"line-center\">OR </span> </h2> <div class=\"social_signup_section text-center m-t-10 hide\"> <p>Sign In using your social media accounts </p> <ul class=\"clearfix\"> <li class=\"social-facebook cursor-pointer\"> <em class=\"fa fa-facebook\"> </em> </li> <li class=\"social-gplus cursor-pointer\"> <em class=\"fa fa-google-plus\"> </em> </li> <li class=\"social-linkedin cursor-pointer\"> <em class=\"fa fa-linkedin\"> </em> </li> </ul> </div> </form> </div> <!-- step1 ends --> <div class=\"\" ng-show=\"sm.signupStep == 'step2'\"> <form class=\"login_form\" name=\"step2_form\" ng-submit=\"sm.addName(step2_form)\" novalidate> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"first_name\" required ng-model=\"sm.user.first_name\" ng-pattern=\"/^[a-zA-Z]*$/\" ng-maxlength=\"20\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>First name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.first_name.$dirty) && step2_form.first_name.$invalid\"> <p ng-if=\"step2_form.first_name.$error.required\">Please enter first name</p> <p ng-if=\"step2_form.first_name.$error.maxlength\">Must below 20 characters</p> <p ng-if=\"step2_form.first_name.$error.pattern\">Name must be alphabetic</p> </div> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"last_name\" ng-model=\"sm.user.last_name\" ng-pattern=\"/^[a-zA-Z]*$/\" ng-maxlength=\"20\" required> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Last name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.last_name.$dirty) && step2_form.last_name.$invalid\"> <p ng-if=\"step2_form.last_name.$error.required\">Please enter last name</p> <p ng-if=\"step2_form.last_name.$error.maxlength\">Must below 20 characters</p> <p ng-if=\"step2_form.last_name.$error.pattern\">Name must be alphabetic</p> </div> </div> </div> </div> <div class=\"col-sm-12 postjob_type_section stp_two_b_spc\"> <!-- <h4 class=\"\">Please enter your mobile number.</h4> --> <div class=\"phone_section phone_section1\"> <!-- second section signup --> <div class=\"row\"> <div class=\"col-md-12\"> <label class=\"label_signup\">Select your Number</label> </div> <div class=\"col-md-6 col-sm-6\"> <div class=\"group group1 chosen_style n_flg country_code_style\" title=\"{{sm.selectedCode.country_name}}\"> <span class=\"flag_number\"> <i class=\"{{sm.selectedCode.flag_class}}\"></i> {{sm.selectedCode.code}}&nbsp;<span>{{sm.selectedCode.country_name}}</span> </span> <div class=\"dropdown countrycodes_spn\"> <a aria-expanded=\"false\" aria-haspopup=\"true\" role=\"button\" data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"javascript:void(0)\"> <span id=\"selected\"></span><span class=\"caret\"></span></a> <ul class=\"dropdown-menu country-codes\"> <li ng-repeat=\"cc in sm.countryCodes\" id=\"{{$index}}option\"><a href=\"javascript:void(0)\" ng-click=\"sm.selectCode(cc,$index)\"><i class=\"{{cc.flag_class}}\"></i><span class=\"contr_name\">{{cc.country_name}}</span> <b class=\"c_code\">{{cc.code}}</b></a></li> </ul> </div> </div> </div> <div class=\"col-md-6 col-sm-6\"> <div class=\"group\"> <input type=\"text\" ng-model=\"sm.user.contactno\" name=\"contactno\" ng-pattern=\"/^[0-9]*$/\" ng-minlength=\"7\" ng-maxlength=\"15\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Contact Number</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\"> <p ng-if=\"step2_form.contactno.$error.required\">Please enter contactno</p> <p ng-if=\"step2_form.contactno.$error.pattern\">Phone number should not be a letter</p> <p ng-if=\"step2_form.contactno.$error.maxlength\">Must below 15 digits</p> <p ng-if=\"step2_form.contactno.$error.minlength\">Must above 7 digits</p> </div> </div> </div> </div> <!-- end second section end --> </div> </div> <div class=\"\"> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"sm.signupStep = 'step1'\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> </div> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <button type=\"submit\" class=\"btn postjob_button hvr-wobble-horizontal\">Continue</button> </div> </div> </div> </form> </div> <!-- step2 ends --> <div class=\"\" ng-show=\"sm.signupStep == 'step3'\"> <form name=\"step3_form\" novalidate ng-submit=\"sm.signup(step3_form)\"> <div class=\"row\"> <div class=\"col-sm-12 postjob_type_section\"> <h4 class=\"text-left m-t-30\">What is your main purpose for signing up?</h4> </div> </div> <!-- RADIO BUTTON STARTS HERE --> <div class=\"radio_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-12\"> <label class=\"control control--radio\"> I am a client. I need childcare workers. <input type=\"radio\" name=\"purpose\" value=\"0\" ng-model=\"sm.user.purpose\" ng-checked=\"true\" class=\"ng-pristine ng-untouched ng-valid ng-not-empty\" ng-click=\"sm.user.selectedProfile = [];\" ng-checked=\"sm.user.purpose == 0 || sm.from_postjob\" style=\"\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-12\"> <label class=\"control control--radio\"> I am a childcare worker. I need childcare jobs. <input type=\"radio\" value=\"1\" ng-model=\"sm.user.purpose\" ng-disabled=\"sm.from_postjob\" ng-click=\"sm.user.profile_suits = '';\" name=\"purpose\" class=\"ng-pristine ng-untouched ng-valid ng-not-empty\" style=\"\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <!-- RADIO BUTTON ENDS HERE --> <div class=\"row\"> <div class=\"col-sm-12 postjob_type_section\"> <h4>Which profile suits you best?</h4> </div> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == '0'\"> <div class=\"radio_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I am a parent <input type=\"radio\" name=\"profile\" ng-model=\"sm.user.profile_suits\" ng-checked=\"sm.user.profile_suits == 'parent'\" ng-required=\"sm.user.purpose == '0'\" value=\"parent\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare center / kindergarten <input type=\"radio\" value=\"carecenter\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == '0'\" ng-checked=\"sm.user.profile_suits == 'carecenter'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare agency <input type=\"radio\" value=\"agency\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == '0'\" ng-checked=\"sm.user.profile_suits == 'agency'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.profile.$error.required\"> <p>Please Select what your Profile Suits</p> </div> </div> </div> </div> <div class=\"second_signup\"> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == '1'\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cg in sm.caregivers\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"sm.user.selectedProfile.indexOf(cg) > -1\" ng-click=\"sm.toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && sm.user.selectedProfile.length == 0\"> <p>Please Select what your Profile Suits</p> </div> </div> </div> <div class=\"col-sm-12 botom_signup_section\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I Agree to the <a href=\"javascript:void(0)\" class=\"orange_text\">Terms of service</a> and <a href=\"javascript:void(0)\" class=\"orange_text\">Privacy policy</a> <input type=\"checkbox\" name=\"terms\" value=\"1\" ng-model=\"sm.users.terms\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.terms.$error.required\"> <p>Agree the terms and conditions to continue</p> </div> </div> </div> <div class=\"\"> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a type=\"button\" class=\"previous_link_style\" ng-click=\"sm.signupStep = 'step2'\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Previous</a> </div> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <button type=\"submit\" class=\"btn postjob_button hvr-wobble-horizontal\">Continue</button> </div> </div> </div> </form> </div> </div> </div> </div> <style>.bootbox .modal-dialog {\n" +
    "    width: 500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "    .bootbox .modal-dialog {\n" +
    "        width: auto;\n" +
    "    }\n" +
    "\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/user/templates/signup1.html',
    "<div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-12 signup_form\"> <h3 class=\"title_modal\">Signup</h3> <h5 class=\"sub-header-popup\">for your account today</h5> <div ng-show=\"sm.signupStep == 'step1'\"> <form class=\"login_form\" name=\"signup_form\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"email\" ng-blur=\"sm.checkemail()\" name=\"email\" required ng-model=\"sm.user.email\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Email</label> <p ng-if=\"!signup_form.email.$error.required && !sm.emailunique\" class=\"errormsg\">Email Already exists</p> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.email.$dirty) && signup_form.email.$invalid\"> <p ng-if=\"signup_form.email.$error.required\">Please enter Email to continue</p> <p ng-if=\"signup_form.email.$error.email\">Please enter a valid email</p> </div> </div> </div> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"password\" name=\"password\" ng-model=\"sm.user.password\" required> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Password</label> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.password.$dirty) && signup_form.password.$invalid\"> <p ng-if=\"signup_form.password.$error.required\">Please enter password to continue</p> <p ng-if=\"signup_form.password.$error.pattern\">Password should contain an Uppercase, a Lowercase and a number</p> </div> </div> </div> <button type=\"button\" ng-click=\"sm.addEmail(signup_form)\" class=\"btn btn-default join_button hvr-wobble-horizontal\">Join</button> <p class=\"no_account_link\">Already have an account? <span class=\"signup_link\" ng-click=\"sm.callLoginpopup()\">Login </span> </p> <h2 class=\"line_section hide\"> <span class=\"line-center\">OR </span> </h2> <div class=\"social_signup_section text-center m-t-10 hide\"> <p>Sign In using your social media accounts </p> <ul class=\"clearfix\"> <li class=\"social-facebook cursor-pointer\"> <em class=\"fa fa-facebook\"> </em> </li> <li class=\"social-gplus cursor-pointer\"> <em class=\"fa fa-google-plus\"> </em> </li> <li class=\"social-linkedin cursor-pointer\"> <em class=\"fa fa-linkedin\"> </em> </li> </ul> </div> </form> </div> <!-- step1 ends --> <div class=\"\" ng-show=\"sm.signupStep == 'step2'\"> <form class=\"login_form\" name=\"step2_form\" novalidate> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"first_name\" required ng-model=\"sm.user.first_name\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>First name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.first_name.$dirty) && step2_form.first_name.$invalid\"> <p ng-if=\"step2_form.first_name.$error.required\">Please enter first name</p> </div> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"last_name\" ng-model=\"sm.user.last_name\" required> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Last name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.last_name.$dirty) && step2_form.last_name.$invalid\"> <p ng-if=\"step2_form.last_name.$error.required\">Please enter last name</p> </div> </div> </div> </div> <div class=\"col-sm-12 postjob_type_section stp_two_b_spc\"> <!-- <h4 class=\"\">Please enter your mobile number.</h4> --> <div class=\"phone_section phone_section1\"> <!-- <input type=\"text\" name=\"contactno\" ng-model=\"sm.user.contactno\" data-auto-format=\"true\" ng-intl-tel-input ng-required=\"true\">\n" +
    "\n" +
    "                    <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\">\n" +
    "                    <p ng-if=\"step2_form.contactno.$error.required\">Please enter contact number</p>\n" +
    "                    <p ng-if=\"!step2_form.contactno.$error.required && step2_form.contactno.$invalid\">Invalid contact number</p>\n" +
    "                  </div> --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"group group1 chosen_style n_flg\"> <!--<i class=\"selectedCode.flag_class\"></i>--> <span class=\"flag_number\"> <i class=\"{{selectedCode.flag_class}}\"></i> {{selectedCode.code}} </span> <div class=\"dropdown\"> <a aria-expanded=\"false\" aria-haspopup=\"true\" role=\"button\" data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"javascript:void(0)\"> <span id=\"selected\"></span><span class=\"caret\"></span></a> <ul class=\"dropdown-menu country-codes\"> <li ng-repeat=\"cc in sm.countryCodes\" id=\"{{$index}}option\"><a href=\"javascript:void(0)\" ng-click=\"selectCode(cc,$index)\"><i class=\"{{cc.flag_class}}\"></i>{{cc.country_name}} <b class=\"c_code\">{{cc.code}}</b></a></li> </ul> </div> </div> </div> <div class=\"col-md-6 group group1\"> <input type=\"text\" ng-model=\"sm.user.contactno\" name=\"contactno\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\"> <p ng-if=\"step2_form.contactno.$error.required\">Please enter contactno</p> <p ng-if=\"!step2_form.contactno.$error.required && step2_form.contactno.$invalid\">Invalid contact number</p> </div> </div> </div> </div> <!-- <input type=\"text\" name=\"contactno\" ng-model=\"sm.user.contactno\" data-auto-format=\"true\" ng-intl-tel-input ng-required=\"true\">\n" +
    "                  <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\">\n" +
    "                  <p ng-if=\"step2_form.contactno.$error.required\">Please enter contact number</p>\n" +
    "                  <p ng-if=\"!step2_form.contactno.$error.required && step2_form.contactno.$invalid\">Invalid contact number</p>\n" +
    "                  </div> --> </div> <!--<div class=\"col-sm-12 postjob_type_section\">\n" +
    "                  <h4 class=\"text-center \">What is your main purpose for signing up?</h4>\n" +
    "                  <div class=\"row\">\n" +
    "                    <div class=\"col-sm-6\">\n" +
    "                      <button class=\"btn btn-info childcareworker_btn\"  ng-click=\"sm.addName(step2_form,0);\">I need child care<br>workers</button>\n" +
    "\n" +
    "\n" +
    "                    </div>\n" +
    "                    <div class=\"col-sm-6\">\n" +
    "                      <button class=\"btn btn-info childcareworker_btn\" ng-disabled=\"sm.from_postjob\" ng-click=\"sm.addName(step2_form,1);\">I need child care<br>jobs</button>\n" +
    "                      <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.purpose.$dirty) && step2_form.$invalid\">\n" +
    "                          <p ng-if=\"step2_form.purpose.$error.required\">Please Select the purpose to continue</p>\n" +
    "                      </div>\n" +
    "\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "                </div>--> <div class=\"\"> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a href=\"javascript:void(0)\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"sm.signupStep = 'step1'\">Back</a> </div> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a href=\"javascript:void(0)\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"sm.addName(step2_form);\">Continue</a> </div> </div> </div> </form> </div> <!-- step2 ends --> <div class=\"\" ng-show=\"sm.signupStep == 'step3'\"> <form name=\"step3_form\" novalidate> <div class=\"row\"> <div class=\"col-sm-12 postjob_type_section\"> <h4 class=\"text-left m-t-30\">What is your main purpose for signing up?</h4> </div> </div> <!-- RADIO BUTTON STARTS HERE --> <div class=\"radio_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\"> <label class=\"control control--radio\"> I need child care workers <input type=\"radio\" name=\"purpose\" value=\"0\" ng-model=\"sm.user.purpose\" ng-checked=\"true\" class=\"ng-pristine ng-untouched ng-valid ng-not-empty\" ng-click=\"sm.user.selectedProfile = [];\" checked style=\"\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-6\"> <label class=\"control control--radio\"> I need child care jobs <input type=\"radio\" value=\"1\" ng-model=\"sm.user.purpose\" ng-disabled=\"sm.from_postjob\" ng-click=\"sm.user.profile_suits = '';\" name=\"purpose\" class=\"ng-pristine ng-untouched ng-valid ng-not-empty\" style=\"\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <!-- RADIO BUTTON ENDS HERE --> <div class=\"row\"> <!--  <div class=\"m-t-20\">\n" +
    "              <div class=\"col-sm-6\">\n" +
    "                <button class=\"btn btn-info childcareworker_btn\" ng-class=\"{'active':sm.user.purpose == 0}\" ng-disabled=\"true\">I need child care<br>workers</button>\n" +
    "              </div>\n" +
    "              <div class=\"col-sm-6\">\n" +
    "                <button class=\"btn btn-info childcareworker_btn\" ng-class=\"{'active':sm.user.purpose == 1}\" ng-disabled=\"true\">I need child care<br>jobs</button>\n" +
    "              </div>\n" +
    "            </div> --> <div class=\"col-sm-12 postjob_type_section\"> <h4>Which profile suits you best?</h4> </div> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == 0\"> <div class=\"radio_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I am a parent <input type=\"radio\" name=\"profile\" ng-model=\"sm.user.profile_suits\" ng-checked=\"sm.user.profile_suits == 'parent'\" ng-required=\"sm.user.purpose == 0\" value=\"parent\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare center / kindergarten <input type=\"radio\" value=\"carecenter\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == 0\" ng-checked=\"sm.user.profile_suits == 'carecenter'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare agency <input type=\"radio\" value=\"agency\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == 0\" ng-checked=\"sm.user.profile_suits == 'agency'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.profile.$error.required\"> <p>Please Select what your Profile Suits</p> </div> </div> </div> </div> <div class=\"second_signup\"> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == 1\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cg in sm.caregivers\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"sm.user.selectedProfile.indexOf(cg) > -1\" ng-click=\"sm.toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && sm.user.selectedProfile.length == 0\"> <p>Please Select what your Profile Suits</p> </div> <!-- <div class=\"radio_section\">\n" +
    "                  <div class=\"control-group\">\n" +
    "                    <label class=\"control control--radio\" ng-repeat=\"caregiver in sm.caregivers\">\n" +
    "                        {{caregiver}}\n" +
    "                        <input type=\"radio\" name=\"profile1\" ng-model=\"sm.user.profile_suits\" ng-checked=\"sm.user.profile_suits == '{{caregiver}}'\" ng-required=\"sm.user.purpose == 1\" value=\"{{caregiver}}\" />\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                    </label>\n" +
    "\n" +
    "                    <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.profile1.$error.required\">\n" +
    "                        <p>Please Select your Profile</p>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "                </div> --> </div> </div> <div class=\"col-sm-12 botom_signup_section\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I Agree to the <a href=\"javascript:void(0)\" class=\"orange_text\">Terms of service</a> and <a href=\"javascript:void(0)\" class=\"orange_text\">Privacy policy</a> <input type=\"checkbox\" name=\"terms\" value=\"1\" ng-model=\"sm.users.terms\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.terms.$error.required\"> <p>Agree the terms and conditions to continue</p> </div> </div> </div> <div class=\"\"> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a href=\"javascript:void(0)\" class=\"btn btn-primary hvr-wobble-horizontal previous_btn\" ng-click=\"sm.signupStep = 'step2'\">Back</a> </div> <div class=\"col-md-6 col-sm-6 col-xs-6\"> <a href=\"javascript:void(0)\" class=\"btn postjob_button hvr-wobble-horizontal\" ng-click=\"sm.signup(step3_form)\">Continue</a> </div> </div> <!-- <div class=\"col-sm-12\" ng-show=\"sm.step3\">\n" +
    "                  <a href=\"javascript:void(0)\" class=\"btn btn-primary join_button hvr-wobble-horizontal\" ng-click=\"sm.signup(step3_form)\">Continue</a>\n" +
    "              </div> --> </div> </form> </div> </div> </div> </div> <style>.bootbox .modal-dialog {\n" +
    "    width: 500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "    .bootbox .modal-dialog {\n" +
    "        width: auto;\n" +
    "    }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/user/templates/signup_backup.html',
    "<div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-12 signup_form\"> <h3 class=\"title_modal\">Signup</h3> <h5 class=\"sub-header-popup\">for your account today</h5> <div ng-show=\"sm.step1\"> <form class=\"login_form\" name=\"signup_form\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"email\" ng-blur=\"sm.checkemail()\" name=\"email\" required ng-model=\"sm.user.email\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Email</label> <p ng-if=\"!signup_form.email.$error.required && !sm.emailunique\" class=\"errormsg\">Email Already exists</p> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.email.$dirty) && signup_form.email.$invalid\"> <p ng-if=\"signup_form.email.$error.required\">Please enter Email to continue</p> <p ng-if=\"signup_form.email.$error.email\">Please enter a valid email</p> </div> </div> </div> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"password\" name=\"password\" ng-model=\"sm.user.password\" required> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Password</label> <div class=\"errormsg\" ng-if=\"(signup_form.$submitted || signup_form.password.$dirty) && signup_form.password.$invalid\"> <p ng-if=\"signup_form.password.$error.required\">Please enter password to continue</p> <p ng-if=\"signup_form.password.$error.pattern\">Password should contain an Uppercase, a Lowercase and a number</p> </div> </div> </div> <button type=\"button\" ng-click=\"sm.addEmail(signup_form)\" class=\"btn btn-default join_button hvr-wobble-horizontal\">Join</button> <p class=\"no_account_link\">Already have an account? <span class=\"signup_link\" ng-click=\"sm.callLoginpopup()\">Login </span> </p> <h2 class=\"line_section hide\"> <span class=\"line-center\">OR </span> </h2> <div class=\"social_signup_section text-center m-t-10 hide\"> <p>Sign In using your social media accounts </p> <ul class=\"clearfix\"> <li class=\"social-facebook cursor-pointer\"> <em class=\"fa fa-facebook\"> </em> </li> <li class=\"social-gplus cursor-pointer\"> <em class=\"fa fa-google-plus\"> </em> </li> <li class=\"social-linkedin cursor-pointer\"> <em class=\"fa fa-linkedin\"> </em> </li> </ul> </div> </form> </div> <!-- step1 ends --> <div class=\"\" ng-show=\"sm.step2\"> <form class=\"login_form\" name=\"step2_form\" novalidate> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"first_name\" required ng-model=\"sm.user.first_name\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>First name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.first_name.$dirty) && step2_form.first_name.$invalid\"> <p ng-if=\"step2_form.first_name.$error.required\">Please enter first name</p> </div> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"m-t-40\"> <div class=\"group\"> <input type=\"text\" name=\"last_name\" ng-model=\"sm.user.last_name\" required> <span class=\"highlight\"></span> <span class=\"bar\"></span> <label>Last name</label> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.last_name.$dirty) && step2_form.last_name.$invalid\"> <p ng-if=\"step2_form.last_name.$error.required\">Please enter last name</p> </div> </div> </div> </div> <div class=\"col-sm-12 postjob_type_section\"> <!-- <h4 class=\"\">Please enter your mobile number.</h4> --> <div class=\"phone_section\"> <div class=\"group group1 chosen_style\"> <select chosen disable-search=\"true\" ng-required=\"true\" placeholder-text-single=\"Select country code\" ng-model=\"sm.user.country_code\"> <option ng-repeat=\"cc in sm.countryCodes\" value=\"{{cc.id}}\"><i class=\"{{cc.flag_class}}\"></i>{{cc.country_name}} {{cc.code}}</option> </select> </div> <input type=\"text\" name=\"contactno\" ng-model=\"sm.user.contactno\" data-auto-format=\"true\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\"> <p ng-if=\"step2_form.contactno.$error.required\">Please enter contact number</p> <p ng-if=\"!step2_form.contactno.$error.required && step2_form.contactno.$invalid\">Invalid contact number</p> </div> </div> <!-- <input type=\"text\" name=\"contactno\" ng-model=\"sm.user.contactno\" data-auto-format=\"true\" ng-intl-tel-input ng-required=\"true\">\n" +
    "                  <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.contactno.$dirty) && step2_form.contactno.$invalid\">\n" +
    "                  <p ng-if=\"step2_form.contactno.$error.required\">Please enter contact number</p>\n" +
    "                  <p ng-if=\"!step2_form.contactno.$error.required && step2_form.contactno.$invalid\">Invalid contact number</p>\n" +
    "                  </div> --> </div> <div class=\"col-sm-12 postjob_type_section\"> <h4 class=\"text-center\">What is your main purpose for signing up?</h4> <div class=\"row\"> <div class=\"col-sm-6\"> <button class=\"btn btn-info childcareworker_btn\" ng-click=\"sm.addName(step2_form,0);\">I need child care<br>workers</button> </div> <div class=\"col-sm-6\"> <button class=\"btn btn-info childcareworker_btn\" ng-disabled=\"sm.from_postjob\" ng-click=\"sm.addName(step2_form,1);\">I need child care<br>jobs</button> <div class=\"errormsg\" ng-if=\"(step2_form.$submitted || step2_form.purpose.$dirty) && step2_form.$invalid\"> <p ng-if=\"step2_form.purpose.$error.required\">Please Select the purpose to continue</p> </div> </div> </div> <!-- <input class=\"hide\" type=\"radio\" ng-pattern=\"/^[0-9]*$/\" value=\"0\" name=\"purpose\" ng-required=\"true\" ng-model=\"sm.user.purpose\" ng-click=\"sm.addName(step2_form)\"  ng-checked=\"sm.user.purpose == 0\">\n" +
    "                  <input class=\"hide\" type=\"radio\" ng-pattern=\"/^[0-9]*$/\" value=\"1\" name=\"purpose\" ng-required=\"true\" ng-model=\"sm.user.purpose\" ng-click=\"sm.addName(step2_form)\"  ng-checked=\"sm.user.purpose == 1\"> --> </div> </div> </form> </div> <!-- step2 ends --> <div class=\"\" ng-show=\"sm.step3\"> <form name=\"step3_form\" novalidate> <div class=\"row\"> <div class=\"m-t-20\"> <div class=\"col-sm-6\"> <button class=\"btn btn-info childcareworker_btn\" ng-class=\"{'active':sm.user.purpose == 0}\" ng-disabled=\"true\">I need child care<br>workers</button> </div> <div class=\"col-sm-6\"> <button class=\"btn btn-info childcareworker_btn\" ng-class=\"{'active':sm.user.purpose == 1}\" ng-disabled=\"true\">I need child care<br>jobs</button> </div> </div> <div class=\"col-sm-12 postjob_type_section\"> <h4>Which profile suits you best?</h4> </div> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == 0\"> <div class=\"radio_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I am a parent <input type=\"radio\" name=\"profile\" ng-model=\"sm.user.profile_suits\" ng-checked=\"sm.user.profile_suits == 'parent'\" ng-required=\"sm.user.purpose == 0\" value=\"parent\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare center / kindergarten <input type=\"radio\" value=\"carecenter\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == 0\" ng-checked=\"sm.user.profile_suits == 'carecenter'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <label class=\"control control--radio\"> I am from childcare agency <input type=\"radio\" value=\"agency\" ng-model=\"sm.user.profile_suits\" ng-required=\"sm.user.purpose == 0\" ng-checked=\"sm.user.profile_suits == 'agency'\" name=\"profile\"> <div class=\"control__indicator\"></div> </label> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.profile.$error.required\"> <p>Please Select what your Profile Suits</p> </div> </div> </div> </div> <div class=\"second_signup\"> <div class=\"col-sm-12\" ng-if=\"sm.user.purpose == 1\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cg in sm.caregivers\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" name=\"selectedCG[]\" value=\"{{cg}}\" ng-checked=\"sm.user.selectedProfile.indexOf(cg) > -1\" ng-click=\"sm.toggleCaregiver(cg)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && sm.user.selectedProfile.length == 0\"> <p>Please Select what your Profile Suits</p> </div> <!-- <div class=\"radio_section\">\n" +
    "                  <div class=\"control-group\">\n" +
    "                    <label class=\"control control--radio\" ng-repeat=\"caregiver in sm.caregivers\">\n" +
    "                        {{caregiver}}\n" +
    "                        <input type=\"radio\" name=\"profile1\" ng-model=\"sm.user.profile_suits\" ng-checked=\"sm.user.profile_suits == '{{caregiver}}'\" ng-required=\"sm.user.purpose == 1\" value=\"{{caregiver}}\" />\n" +
    "                        <div class=\"control__indicator\"></div>\n" +
    "                    </label>\n" +
    "\n" +
    "                    <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.profile1.$error.required\">\n" +
    "                        <p>Please Select your Profile</p>\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "                </div> --> </div> </div> <div class=\"col-sm-12 botom_signup_section\"> <div class=\"check_box_section\"> <div class=\"control-group\"> <label class=\"control control--radio\"> I Agree to the <a href=\"javascript:void(0)\" class=\"orange_text\">Terms of service</a> and <a href=\"javascript:void(0)\" class=\"orange_text\">Privacy policy</a> <input type=\"checkbox\" name=\"terms\" value=\"1\" ng-model=\"sm.users.terms\" ng-required=\"true\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"errormsg\" ng-if=\"step3_form.$submitted && step3_form.terms.$error.required\"> <p>Agree the terms and conditions to continue</p> </div> </div> </div> <div class=\"col-sm-12\" ng-show=\"sm.step3\"> <a href=\"javascript:void(0)\" class=\"btn btn-primary join_button hvr-wobble-horizontal\" ng-click=\"sm.signup(step3_form)\">Continue</a> </div> </div> </form> </div> </div> </div> </div> <style>.bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/module/user/templates/signup_success.html',
    "<div class=\"modal-body\"> <h2 class=\"text-center almost_there\">Success</h2> <p class=\"final_step_postjob text-center\">Your request to signup has been submitted successfully. Please check your email (inbox / Junk / Clutter) folders to activate your account.</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/module/user/templates/updatehistory.html',
    "<div class=\"modal-body\"> <div> <ng-form name=\"experienceForm\" novalidate> <div class=\"postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12 add_experience_form\"> <form class=\"m-t-30\" name=\"experienceForm\" novalidate ng-submit=\"updateHistory(experienceForm)\"> <h4 class=\"title_modal\">Edit Experience</h4> <div class=\"form-group\"> <label for=\"title\">Title</label> <input type=\"text\" class=\"form-control\" name=\"title\" ng-model=\"history.title\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.title.$dirty) && experienceForm.title.$invalid\"> <p ng-if=\"experienceForm.title.$error.required\">Please enter title</p> </div> </div> <div class=\"form-group\"> <label for=\"companyname\">Company Name</label> <input type=\"text\" class=\"form-control\" name=\"companyname\" ng-model=\"history.company_name\" ng-required=\"true\"> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.companyname.$dirty) && experienceForm.companyname.$invalid\"> <p ng-if=\"experienceForm.companyname.$error.required\">Please enter company name</p> </div> </div> <div class=\"form-group pos_rel_exp\"> <label for=\"location\">Location</label> <p class=\"icon_load_spin\"> <input type=\"text\" class=\"form-control\" ng-model=\"history.location\" name=\"location\" class=\"form-control text-box\" uib-typeahead=\"location as location.viewval for location in getLocations($viewValue)\" typeahead-wait-ms=\"600\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"setHistorylocation($item,true)\" autocomplete=\"off\"> <span class=\"ajax_loader1\" ng-if=\"history.location\" title=\"Clear location\" ng-click=\"history.location = '';history.city='';history.state='';history.post_code=''\"><i class=\"fa fa-times-circle\"></i></span> <span class=\"ajax_loader1\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> </p> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.location.$dirty) && (!history.post_code || !history.location) && filteredLocations.length == 0 && !experienceForm.location.$error.required\"> <p>Please enter valid location</p> </div> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.location.$dirty) && experienceForm.location.$invalid && experienceForm.location.$error.required\"> <p>Please enter valid location</p> </div> <!-- <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.location.$dirty) && (!history.location.suburb || !history.location.state || !history.location.postcode) && !experienceForm.location.$error.required && filteredLocations.length == 0\">\n" +
    "                  <p>Invalid city, state and post code</p>\n" +
    "                  </div> --> <div class=\"row\"> <div class=\"col-sm-6\"> <div class=\"row\"> <div class=\"col-sm-12\"> <label for=\"from_month\">From</label> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" chosen disable-search=\"true\" name=\"from_month\" ng-model=\"history.from_month\" id=\"from_month\" ng-required=\"true\" convert-to-number> <option value=\"\">Month</option> <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_month.$dirty) && experienceForm.from_month.$invalid\"> <p ng-if=\"experienceForm.from_month.$error.required\">Please enter from month</p> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"from_year\" chosen disable-search=\"true\" name=\"from_year\" ng-model=\"history.from_year\" ng-required=\"true\" convert-to-number> <option value=\"\">Year</option> <option ng-repeat=\"year in years\" value=\"{{year}}\">{{year}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_year.$dirty) && experienceForm.from_year.$invalid\"> <p ng-if=\"experienceForm.from_year.$error.required\">Please enter from year</p> </div> </div> </div> </div> <!-- <div class=\"form-group chosen_style\">\n" +
    "                      <label for=\"from_month\">From</label>\n" +
    "                      <select class=\"form-control\" chosen disable-search=\"true\" name=\"from_month\" ng-model=\"history.from_month\" id=\"from_month\" ng-required=\"true\" convert-to-number>\n" +
    "                        <option>Month</option>\n" +
    "                        <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option>\n" +
    "\n" +
    "                      </select>\n" +
    "                      <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_month.$dirty) && experienceForm.from_month.$invalid\">\n" +
    "                      <p ng-if=\"experienceForm.from_month.$error.required\">Please enter from month</p>\n" +
    "                  </div>\n" +
    "                      </div>\n" +
    "                      <div class=\"form-group chosen_style\">\n" +
    "                      <select class=\"form-control\" id=\"from_year\" chosen disable-search=\"true\" name=\"from_year\" ng-model=\"history.from_year\" ng-required=\"true\" convert-to-number>\n" +
    "                        <option>Year</option>\n" +
    "                        <option ng-repeat=\"year in years\" value=\"{{year}}\">{{year}}</option>\n" +
    "                      </select>\n" +
    "                      <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.from_year.$dirty) && experienceForm.from_year.$invalid\">\n" +
    "                      <p ng-if=\"experienceForm.from_year.$error.required\">Please enter from year</p>\n" +
    "                  </div>\n" +
    "                      </div> --> </div> <div class=\"col-sm-6\" ng-if=\"history.currentlyWorking == 0\"> <div class=\"row\"> <div class=\"col-md-12\"> <label for=\"to_month\">To</label> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"to_month\" chosen disable-search=\"true\" name=\"to_month\" ng-model=\"history.to_month\" ng-required=\"history.currentlyWorking == 0\" convert-to-number> <option>Month</option> <option ng-repeat=\"month in months\" value=\"{{month.value}}\">{{month.name}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_month.$dirty) && experienceForm.to_month.$invalid\"> <p ng-if=\"experienceForm.to_month.$error.required\">Please enter To month</p> </div> </div> </div> <div class=\"col-sm-6\"> <div class=\"form-group chosen_style\"> <select class=\"form-control\" id=\"to_year\" chosen disable-search=\"true\" name=\"to_year\" convert-to-number ng-model=\"history.to_year\" ng-required=\"history.currentlyWorking == 0\"> <option>Year</option> <option ng-repeat=\"year in years\" value=\"{{year}}\">{{year}}</option> </select> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.to_year.$dirty) && experienceForm.to_year.$invalid\"> <p ng-if=\"experienceForm.to_year.$error.required\">Please enter To year</p> </div> </div> </div> </div> </div> <div ng-if=\"history.currentlyWorking == 1\"> <label class=\"\">To</label> <div> <label class=\"decorate_present\">Present</label> </div> </div> <!-- <div ng-if=\"history.currentlyWorking == 1\">\n" +
    "                      <p>Present</p>\n" +
    "                    </div> --> </div> <div class=\"row\"> <div class=\"col-sm-12 check_box_section\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> I currently work here <input type=\"checkbox\" ng-model=\"history.currentlyWorking\" ng-true-value=\"1\" ng-false-value=\"0\" ng-click=\"history.to_month = '';history.to_year='';\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"form-group\"> <label for=\"companyname\">Description</label> <textarea class=\"form-control\" ng-model=\"history.description\" ng-required=\"true\" name=\"description\">\n" +
    "                      </textarea> <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.description.$dirty) && experienceForm.description.$invalid\"> <p ng-if=\"experienceForm.description.$error.required\">Please enter description</p> <p ng-if=\"experienceForm.description.$error.minlength\">Must above 200 characters</p> <p ng-if=\"experienceForm.description.$error.maxlength\">Must below 500 characters</p> </div> </div> </div> <!-- <div class=\"col-md-12\">\n" +
    "                        <div class=\"upload_section_popup\">\n" +
    "                        <h4>Media</h4>\n" +
    "                        <h5>Add or link to external documents, photos, sites, videos, and presentations.</h5>\n" +
    "                        <button class=\"upload_btn btn btn-default\" ngf-select=\"proofUpload($file)\" ng-model=\"file\" name=\"proof_file\">upload</button>\n" +
    "                        {{history.proof}}\n" +
    "                      </div>\n" +
    "\n" +
    "                      </div> --> </div> <div class=\"row\"> <div class=\"col-sm-12 m-t-20 text-right\"> <div class=\"action_section\"> <button type=\"button\" class=\"btn btn btn-default loginbtn\" ng-click=\"cancelHistory()\">Cancel</button> <button type=\"submit\" class=\"btn btn btn-primary signupbtn\" ng-disabled=\"experienceForm.$submitted && experienceForm.$valid && historySaving\">Save</button> </div> </div> </div> </div> </form> <!-- <div class=\"group\">\n" +
    "                  <input type=\"text\" ng-pattern=\"/^[1-9]\\d*$/\" ng-model=\"experience.title\" ng-required=\"true\" name=\"title\">\n" +
    "                  <span class=\"highlight\"></span>\n" +
    "                  <span class=\"bar\"></span>\n" +
    "                  <label>Title</label>\n" +
    "                  <div class=\"errormsg\" ng-if=\"(experienceForm.$submitted || experienceForm.title.$dirty) && experienceForm.title.$invalid\">\n" +
    "                     <p ng-if=\"experienceForm.title.$error.required\">Please enter the Title</p>\n" +
    "                     <p ng-if=\"experienceForm.title.$error.pattern\">Rate per hour must be in number and should not be 0</p>\n" +
    "                  </div>\n" +
    "               </div> --> </div> </div> </div> </ng-form> </div></div>"
  );


  $templateCache.put('scripts/module/user/views/changepassword.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <div class=\"content_section\"> <section class=\"change_password\"> <div class=\"row\"> <div class=\"col-md-9\"> <h3 class=\"heading\">Change Password</h3> <!--  Change password form--> <div class=\"row\"> <div class=\"col-sm-8 postjob_type_section change_password_form\"> <form name=\"changepwd_form\" ng-submit=\"updatePassword(changepwd_form)\" novalidate> <div class=\"m-t-10 m-b-10\"> <div class=\"group\"> <h4 class=\"m-b-0\">Current Password</h4> <input type=\"password\" ng-model=\"changePassword.oldpassword\" name=\"oldpassword\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"wrngOldpwd\"> <p>Old password is invalid!!</p> </div> <div class=\"errormsg\" ng-if=\"(changepwd_form.$submitted || changepwd_form.oldpassword.$dirty) &&changepwd_form.oldpassword.$invalid\"> <p ng-if=\"changepwd_form.oldpassword.$error.required\">Please enter current password</p> </div> </div> </div> <div class=\"m-t-10 m-b-10\"> <div class=\"group\"> <h4 class=\"m-b-0\">New Password</h4> <input type=\"password\" ng-model=\"changePassword.password\" ng-required=\"true\" ng-minlength=\"6\" name=\"password\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(changepwd_form.$submitted || changepwd_form.password.$dirty) &&changepwd_form.password.$invalid\"> <p ng-if=\"changepwd_form.password.$error.required\">Please enter new password</p> <p ng-if=\"changepwd_form.password.$error.minlength\">Password must contain atleast 6 characters</p> </div> </div> </div> <div class=\"m-t-10 m-b-10\"> <div class=\"group\"> <h4 class=\"m-b-0\">Confirm New Password</h4> <input type=\"password\" name=\"confirmpassword\" ng-model=\"changePassword.confirmPassword\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"passmatch && !changepwd_form.confirmpassword.$error.required && changePassword.password != changePassword.confirmPassword\"> <p>Confirm password must be same as password</p> </div> <div class=\"errormsg\" ng-if=\"(changepwd_form.$submitted || changepwd_form.confirmpassword.$dirty) && changepwd_form.confirmpassword.$invalid\"> <p ng-if=\"changepwd_form.confirmpassword.$error.required\">Please enter confirm password</p> </div> </div> </div> <div class=\"button_footer pull-right m-t-20\"> <a class=\"btn btn btn-default cancel_footer_btn hvr-float-shadow\" href=\"javascript:void(0)\" ng-click=\"cancelChangepwd()\">Cancel</a> <button type=\"submit\" class=\"btn btn btn-primary save_footer_btn hvr-float-shadow\">Save</button> </div> </form> </div> </div> <!-- <form>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"email\">Email address:</label>\n" +
    "    <input type=\"email\" class=\"form-control\" id=\"email\">\n" +
    "  </div>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"pwd\">Password:</label>\n" +
    "    <input type=\"password\" class=\"form-control\" id=\"pwd\">\n" +
    "  </div>\n" +
    "</form> --> <!--  End of change password form--> </div> <div class=\"col-md-3 hidden-sm hidden-xs\"> <div class=\"\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </section></div>   <!-- footer section comes here --> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/views/myaccount.html',
    "<div header></div> <section> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h2 class=\"cmn-heading\"><i class=\"fa fa-user-o\"></i>My Account</h2> <form class=\"profileDetailails_form\" ng-submit=\"updatePassword(ChangepasswordForm)\" name=\"ChangepasswordForm\" method=\"get\" novalidate> <div class=\"col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0 myaccount-page\"> <button ui-sref=\"registerBusiness\" type=\"button\" class=\"btn btn-green btn-block\"><i class=\"fa fa-gears\"></i> register as business</button> <div class=\"form-login\"> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Email address</label> </div> <div class=\"col-xs-12\"> <input type=\"text\" class=\"form-control text-box\"> </div> </div> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">New password</label> </div> <div class=\"col-xs-12\"> <input required name=\"password\" ng-model=\"changePassword.password\" ng-maxlength=\"50\" type=\"password\" ng-pattern=\"/^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!@#$%^&*-_]{8,}$/\" class=\"form-control text-box\"> <label for=\"newpassword\" ng-show=\"passwordsubmit && ChangepasswordForm.password.$error.required\" class=\"error\">{{formValidation.required}}</label> <label for=\"password\" ng-show=\"passwordsubmit && ChangepasswordForm.password.$error.pattern\" class=\"error\">{{formValidation.passwordValidation}}</label> <label for=\"password\" ng-show=\"passwordsubmit && ChangepasswordForm.password.$touched && ChangepasswordForm.password.$error.minlength\" class=\"error\">{{formValidation.passwordMin}}</label> <label for=\"password\" ng-show=\"passwordsubmit && ChangepasswordForm.password.$touched && ChangepasswordForm.password.$error.maxlength\" class=\"error\">{{formValidation.passwordMax}}</label> </div> </div> <div class=\"form-group row\"> <div class=\"col-xs-12\"> <label class=\"control-label\">Confirm new password</label> </div> <div class=\"col-xs-12\"> <input name=\"repeat_password\" ng-model=\"changePassword.repeat_password\" type=\"password\" required compare-to=\"changePassword.password\" class=\"form-control text-box\"> </div> </div> </div> </div> <div class=\"col-xs-12 text-center\"> <button type=\"submit\" ng-click=\"passwordsubmit = true\" class=\"btn btn-search\">save</button> </div> </form> </div> </div> </div> </section>"
  );


  $templateCache.put('scripts/module/user/views/myprofile.html',
    "<!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"></div> --> <div header></div> <div class=\"content_section\"> <!-- my profile section start here --> <div class=\"row\"> <div class=\"col-md-8\"> <ol class=\"breadcrumb\"> <li class=\"active\">My Profile</li> </ol> </div> <div class=\"col-md-4\"> <a class=\"edit_profile_btn hvr-float-shadow\" target=\"_blank\" href=\"#/viewworker/{{personal.id}}\"><i class=\"icon-globe\"></i>&nbsp;<span>My public profile</span></a> <a class=\"edit_profile_btn hvr-float-shadow m-r-5\" href=\"#/updateprofile/personal\"><i class=\"icon-pencil\"></i>&nbsp;<span>Update my profile</span></a> </div> </div> <div class=\"myprofile_section clearfix\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-9\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/personal\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-4 col-sm-4\"> <div class=\"profile-pic\"> <div class=\"profile-border\"> <img src=\"/images/profilepic/{{personal.profpic}}\" ng-if=\"personal.profpic\" alt=\"profilepicture\" class=\"m-t-4\"> <img src=\"/images/no_image_found.png\" ng-if=\"!personal.profpic\" alt=\"profilepicture_not_found\" class=\"m-t-4\"> </div> </div> <div class=\"text-center worker_rating\"> <h5 class=\"intro-title1\"><span class=\"first_name_css\">{{personal.first_name}}</span> <span class=\"last_name_css\">{{personal.last_name}}</span></h5> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" ng-attr-readonly=\"true\" allow-half></input-stars> <h5 class=\"intro-gender\" ng-if=\"personal.age && personal.age !='not specified'\">{{personal.age}} years old &nbsp;<span ng-if=\"personal.gender\">{{personal.gender | capitalize}}</span></h5> </div> </div> <div class=\"col-md-8 col-sm-8\"> <div class=\"single_right_side_container\"> <div class=\"row\"> <div class=\"single_right_side_data clearfix\"> <div class=\"col-sm-12\"> <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2> </div> <div class=\"col-sm-12\"> <div class=\"location_section\"> <i class=\"icon-directions\" class=\"pull-left\"></i> <span class=\"ng-binding\" ng-if=\"personal.address\"> {{personal.address}}</span> <span class=\"ng-binding\" ng-if=\"!personal.address\"> - </span> </div> </div> <div class=\"col-sm-12 m-t-10\"> <div class=\"location_section\"> <i class=\"icon-location-pin\" class=\"pull-left\"></i> <span class=\"ng-binding\" ng-if=\"personal.post_code\"> {{personal.city}},{{personal.state}} {{personal.post_code}}</span> <span class=\"ng-binding\" ng-if=\"!personal.post_code\">-</span> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_data clearfix\"> <div class=\"col-sm-12\"> <div> <strong class=\"label_myprofile\">Preferred job type</strong> <p class=\"value_myprofile\"><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span></p> </div> <div class=\"m-t-10\"> <strong class=\"label_myprofile\">Preferred Age Groups </strong> <p class=\"value_myprofile\" ng-if=\"preference.children_age.length > 0\">{{preference.children_age}}</p> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_data clearfix bb-none\"> <div class=\"col-sm-6\"> <p class=\"value_myprofile\"><i class=\"icon-envelope\"></i>&nbsp;{{personal.email}}</p> </div> <div class=\"col-sm-6\"> <p class=\"value_myprofile\"><i class=\"icon-phone\"></i>&nbsp;{{personal.country_code}} {{personal.contactno}}</p> </div> </div> </div> <!-- <div class=\"row\">\n" +
    "                               <div class=\"single_right_side_myprofile\">\n" +
    "                                 <div class=\"col-sm-12\">\n" +
    "                                   <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2>\n" +
    "                                 </div>\n" +
    "                               </div>\n" +
    "                             </div> --> <!-- <div class=\"row\">\n" +
    "                               <div class=\"single_right_side_myprofile \">\n" +
    "\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <div class=\"location_section\">\n" +
    "                                      <img src=\"styles/images/street.jpg\" alt=\"\" class=\"pull-left\">\n" +
    "                                      <span class=\"ng-binding\" ng-if=\"personal.address\"> {{personal.address}}</span>\n" +
    "                                      <span class=\"ng-binding\" ng-if=\"!personal.address\"> - </span>\n" +
    "\n" +
    "                                  </div>\n" +
    "\n" +
    "\n" +
    "                               </div>\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <div class=\"location_section\">\n" +
    "                                      <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\">\n" +
    "                                      <span class=\"ng-binding\" ng-if=\"personal.post_code\"> {{personal.city}},{{personal.state}} {{personal.post_code}}</span>\n" +
    "                                      <span class=\"ng-binding\" ng-if=\"!personal.post_code\">-</span>\n" +
    "                                  </div>\n" +
    "                                </div>\n" +
    "\n" +
    "                               </div>\n" +
    "                             </div> --> <!-- <div class=\"row\">\n" +
    "                               <div class=\"single_right_side_myprofile \">\n" +
    "\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <strong class=\"label_myprofile\">Preferred job type</strong>\n" +
    "                                 <p class=\"value_myprofile\"><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span></p>\n" +
    "\n" +
    "                               </div>\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <strong class=\"label_myprofile\">Preferred Age Groups </strong>\n" +
    "                                 <p class=\"value_myprofile\">{{preference.children_age}}</p>\n" +
    "\n" +
    "                               </div>\n" +
    "                             </div>\n" +
    "                             </div> --> <!-- <div class=\"row\">\n" +
    "                               <div class=\"single_right_side_myprofile bb_none\">\n" +
    "\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <strong class=\"label_myprofile\">Email ID</strong>\n" +
    "                                 <p class=\"value_myprofile\">{{personal.email}}</p>\n" +
    "\n" +
    "                               </div>\n" +
    "                               <div class=\"col-sm-6 col-xs-12\">\n" +
    "                                 <strong class=\"label_myprofile\">Mobile Number</strong>\n" +
    "                                 <p class=\"value_myprofile\">{{personal.country_code}} {{personal.contactno}}</p>\n" +
    "\n" +
    "                               </div>\n" +
    "                             </div>\n" +
    "                             </div> --> </div> </div> </div> </div> </div> <!-- my bio section --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/personal\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <h2 class=\"h3 u-heading-v3__title\">About Me</h2> <p class=\"mybio_p\" ng-if=\"personal.about\" ng-bind-html=\"personal.about\"></p> <p class=\"mybio_p\" ng-if=\"!personal.about\">Edit your profile to show your details</p> </div> </div> </div> </div> <!-- end my bio section --> <!--  availability--> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/availability\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Availability</h2> </div> <div class=\"availability_section m-t-30\"> <div class=\"table-responsive\"> <table class=\"table table-bordered table-striped\"> <thead> <tr> <th style=\"width:30%\">&nbsp;</th> <th style=\"width:10%\">M</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">W</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">F</th> <th style=\"width:10%\">S</th> <th style=\"width:10%\">S</th> </tr> </thead> <tbody> <tr> <td>Before School care <br><span class=\"time_care\">6am - 9am</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.before_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.before_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Day care <br><span class=\"time_care\">9am - 3pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.day_care.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.day_care.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>After School care <br><span class=\"time_care\">3pm - 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.after_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.after_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Overnight care <br><span class=\"time_care\">After 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.overnight.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.overnight.indexOf(day) > -1\"></i> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> </div> <!--  End availability--> <div class=\"row\" ng-if=\"emp_history.length >0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/history\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Work Experience</h2> </div> <div class=\"no-data_found hide\"> <p><i class=\"fa fa-exclamation-triangle\"></i>&nbsp;No record found</p> </div> <div class=\"single_job_experience\" ng-repeat=\"history in emp_history\"> <h3 class=\"section-item-title-1\">{{history.company_name}}</h3> <h4 class=\"job_designation\">{{history.title}} - <span class=\"job-date\"> <span>{{months[history.frommonth].name}} {{history.fromyear}} - <span ng-if=\"history.to_month != 'null' && history.to_year != 'null'\">{{months[history.to_month].name}} {{history.to_year}}</span> <span ng-if=\"history.to_month == 'null' || history.to_year == 'null' || !history.to_month || !history.to_year\">Present</span> </span></span> </h4> <h4 class=\"location\" ng-if=\"history.post_code\">{{history.location}}</h4> <p class=\"mybio_p\">{{history.description}}</p> </div> <div ng-if=\"emp_history.length == 0\"> No data found!! </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"requirements.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/credential\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">credentials</h2> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Trust') > -1\"> <h4>Trust</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a ng-if=\"req.document_name\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                  <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\">\n" +
    "                                    <div class=\"disp_profile_table\" >\n" +
    "                                       <div class=\"icon_class_profile\">\n" +
    "                                          <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                       </div>\n" +
    "                                       <div class=\"icon_class_content\">\n" +
    "                                          <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                       </div>\n" +
    "                                    </div>\n" +
    "                                  </li>\n" +
    "                                </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Residency') > -1\"> <h4>Residency</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a ng-if=\"req.document_name\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\">\n" +
    "                                  <div class=\"disp_profile_table\" >\n" +
    "                                     <div class=\"icon_class_profile\">\n" +
    "                                        <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                     </div>\n" +
    "                                     <div class=\"icon_class_content\">\n" +
    "                                        <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                     </div>\n" +
    "                                  </div>\n" +
    "                                </li>\n" +
    "                              </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Training and experience') > -1\"> <h4>Training & Education</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a ng-if=\"req.document_name\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\">\n" +
    "                                  <div class=\"disp_profile_table\" >\n" +
    "                                     <div class=\"icon_class_profile\">\n" +
    "                                        <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                     </div>\n" +
    "                                     <div class=\"icon_class_content\">\n" +
    "                                        <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                     </div>\n" +
    "                                  </div>\n" +
    "                                </li>\n" +
    "                              </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Driving') > -1\"> <h4>Driving</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a ng-if=\"req.document_name\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\">\n" +
    "                                  <div class=\"disp_profile_table\" >\n" +
    "                                     <div class=\"icon_class_profile\">\n" +
    "                                        <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                     </div>\n" +
    "                                     <div class=\"icon_class_content\">\n" +
    "                                        <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                     </div>\n" +
    "                                  </div>\n" +
    "\n" +
    "                                </li>\n" +
    "                              </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Others') > -1\"> <h4>Others</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a ng-if=\"req.document_name\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "                                  <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\">\n" +
    "                                    <div class=\"disp_profile_table\" >\n" +
    "                                       <div class=\"icon_class_profile\">\n" +
    "                                          <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "                                       </div>\n" +
    "                                       <div class=\"icon_class_content\">\n" +
    "                                          <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "                                       </div>\n" +
    "                                    </div>\n" +
    "                                  </li>\n" +
    "                                </ul> --> </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"others.interest\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/others\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Others</h2> </div> <div class=\"others_section_view\"> <h4>Interests & Favourites</h4> <p class=\"mybio_p\" ng-bind-html=\"others.interest\"></p> </div> <div class=\"others_section_view\" ng-if=\"others.household_chores.length > 0\"> <h4>Household Chores</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"chore in others.household_chores\"> <p class=\"chore_value\"><i class=\"fa fa-check\"></i>&nbsp;{{chore | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "                                    <li ng-repeat=\"chore in others.household_chores\"><i class=\"fa fa-check\"></i>&nbsp;{{chore}}</li>\n" +
    "                                 </ul> --> </div> <div class=\"others_section_view\" ng-if=\"others.languages.length > 0\"> <h4>Languages</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"language in others.languages\"> <p class=\"chore_value\" ng-if=\"language\"><i class=\"fa fa-check\"></i>&nbsp;{{language | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "                                    <li ng-repeat=\"language in others.languages\"><i class=\"fa fa-check\"></i>&nbsp;{{language}}</li>\n" +
    "                                 </ul> --> </div> <!-- <div class=\"others_section_view\">\n" +
    "                                 <h4>Emergency Contact</h4>\n" +
    "\n" +
    "                                 <p class=\"reference_p_view\" ng-repeat=\"emergency in emp_emergencycontact\"><i class=\"fa fa-check\"></i>&nbsp;&nbsp;{{emergency.first_name}} {{emergency.last_name}} <span class=\"contact_no_css\">( {{emergency.contactno}} )</span></p>\n" +
    "                                 <div ng-if=\"emp_emergencycontact.length == 0\">\n" +
    "                                    No data found!!\n" +
    "                                 </div>\n" +
    "                              </div> --> </div> </div> </div> </div> <!-- emergency contact --> <div class=\"row\" ng-if=\"emp_emergencycontact.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/others\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Emergency Contact</h2> </div> <div class=\"table-responsive reference_table\"> <table class=\"table table-bordered table-striped\"> <tr> <th><i class=\"fa fa-user-o\"></i>&nbsp;Name</th> <th><i class=\"fa fa-envelope-o\"></i>&nbsp;Email</th> <th><i class=\"icon-phone\"></i>&nbsp;Phone Number</th> </tr> <tr ng-repeat=\"emergency in emp_emergencycontact\"> <td>{{emergency.first_name}} {{emergency.last_name}}</td> <td>{{emergency.email}}</td> <td>{{emergency.contactno}}</td> </tr> </table> </div> </div> </div> </div> </div> <!-- resume section --> <div class=\"row\" ng-if=\"others.resume\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/others\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Resume</h2> </div> <p class=\"download-resume-section\"><i class=\"fa fa-file-text-o\"></i>&nbsp;{{others.resume}} <span><a href=\"assets/documents/resumes/{{others.resume}}\" download=\"{{others.resume}}\" uib-tooltip=\"Download Resume\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span></p> <!-- <a class=\"btn down_load_resume_btn \" href=\"assets/documents/resumes/{{others.resume}}\" download=\"{{others.resume}}\">Download Resume</a> --> </div> </div> </div> </div> <!-- end resume section --> <!-- References --> <div class=\"row\" ng-if=\"references.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/reference\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">References</h2> </div> <div class=\"table-responsive reference_table\"> <table class=\"table table-bordered table-striped\"> <tr> <th><i class=\"fa fa-user-o\"></i>&nbsp;Name</th> <th><i class=\"fa fa-envelope-o\"></i>&nbsp;Email</th> <th><i class=\"icon-phone\"></i>&nbsp;Phone Number</th> </tr> <tr ng-repeat=\"ref in references\"> <td>{{ref.first_name}} {{ref.last_name}}</td> <td>{{ref.email}}</td> <td>{{ref.contactno}}</td> </tr> </table> </div> <!-- <div class=\"single_reference_section_view\" ng-repeat=\"ref in references\">\n" +
    "                              <a href=\"/assets/documents/references/{{ref.document}}\" download=\"{{ref.document}}\" ng-if=\"ref.document !='null' && ref.document\"><img src=\"styles/images/dd.png\" width=\"16\" uib-tooltip=\"Download\"></a>\n" +
    "                              <h4><i class=\"icon-user\"></i>&nbsp;{{ref.first_name}} {{ref.last_name}} |&nbsp;<i class=\"icon-envelope-open\"></i>&nbsp;{{ref.email}}&nbsp;|&nbsp;<i class=\"icon-phone\"></i>&nbsp;{{ref.contactno}}</h4>\n" +
    "\n" +
    "\n" +
    "                           </div> --> </div> </div> </div> </div> <!--  End Reference--> <!--  others--> <!--  End Credential--> </div> <div class=\"col-md-3\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">profile Completeness&nbsp;<span class=\"profile_complete_percent\">{{profileCompleteness}}%</span></h2> </div> <div class=\"complete_progress\"> <!-- <h5>Profile complete on {{profileCompleteness}}%</h5> --> <div class=\"progress\"> <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"{{profileCompleteness}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" ng-style=\"{ 'width': (profileCompleteness+'%')}\"> </div> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"right_side_section_joblist\"> <!-- <div class=\"ad_section\">\n" +
    "                        <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "                        </div> --> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/views/myprofile_bak.html',
    "<!-- <div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"></div> --> <div header></div> <div class=\"content_section\"> <!-- my profile section start here --> <div class=\"row\"> <div class=\"col-md-8\"> <ol class=\"breadcrumb\"> <li><a href=\"javascript:void(0)\">My Profile</a></li> <li class=\"active\">View Profile</li> </ol> </div> <div class=\"col-md-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" href=\"#/updateprofile/personal\"><i class=\"fa fa-pencil\"></i>&nbsp;<span>update Profile</span></a> </div> </div> <div class=\"myprofile_section clearfix\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-9\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/personal\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-4 col-sm-4\"> <div class=\"profile-pic\"> <div class=\"profile-border\"> <img src=\"/images/profilepic/{{personal.profpic}}\" ng-if=\"personal.profpic\" alt=\"profilepicture\"> <img src=\"/images/no_image_found.png\" ng-if=\"!personal.profpic\" alt=\"profilepicture_not_found\"> </div> </div> <div class=\"text-center worker_rating\"> <h5 class=\"intro-title1\"><span class=\"first_name_css\">{{personal.first_name}}</span> <span class=\"last_name_css\">{{personal.last_name}}</span></h5> <input-stars max=\"5\" ng-model=\"YourCtrl.property\" allow-half></input-stars> </div> </div> <div class=\"col-md-8 col-sm-8\"> <div class=\"single_right_side_container\"> <div class=\"row\"> <div class=\"single_right_side_myprofile\"> <div class=\"col-sm-12\"> <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2> <div class=\"location_section\"> <img src=\"styles/images/location.png\" alt=\"\" class=\"pull-left\"> <span class=\"ng-binding\" ng-if=\"personal.post_code\"> {{personal.city}}, {{personal.state}} {{personal.post_code}}</span> <span class=\"ng-binding\" ng-if=\"!personal.post_code\"> --- </span> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_myprofile\"> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Gender</strong> <p class=\"value_myprofile\" ng-if=\"personal.gender\">{{personal.gender}}</p> <p class=\"value_myprofile\" ng-if=\"!personal.gender\">--- </p> </div> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Age</strong> <p class=\"value_myprofile\" ng-if=\"personal.age\">{{personal.age}}</p> <p class=\"value_myprofile\" ng-if=\"!personal.age\">---</p> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_myprofile\"> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Email ID</strong> <p class=\"value_myprofile\">{{personal.email}}</p> </div> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Mobile Number</strong> <p class=\"value_myprofile\">{{personal.contry_code}} {{personal.contactno}}</p> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_myprofile bb_none\"> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Preferred job type</strong> <p class=\"value_myprofile\"><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span></p> </div> <div class=\"col-sm-6 col-xs-6\"> <strong class=\"label_myprofile\">Preferred Age Group </strong> <p class=\"value_myprofile\">{{preference.children_age}}</p> </div> </div> </div> <!-- <h1 class=\"intro-title1\"><span class=\"first_name_css\">{{personal.first_name}}</span> <span class=\"last_name_css\">{{personal.last_name}}</span></h1> --> <!-- <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2> --> <!-- <div class=\"single_preference_section\" ng-if=\"preference.children_age\">\n" +
    "                                <div class=\"single_preference_section\" ng-if=\"preference.job_type.length > 0\">\n" +
    "                                   <h4>Preferred job type(s)</h4>\n" +
    "                                   <p class=\"mybio_p\" ><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span> </p>\n" +
    "                                </div>\n" +
    "                                 <h4>Preferred Age Group</h4>\n" +
    "                                 <p class=\"mybio_p\">{{preference.children_age}}</p>\n" +
    "                              </div> --> </div> </div> </div> </div> </div> <!-- my bio section --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/personal\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <h2 class=\"h3 u-heading-v3__title\">About Me</h2> <p class=\"mybio_p\" ng-if=\"personal.about\">{{personal.about}}</p> <p class=\"mybio_p\" ng-if=\"!personal.about\">Edit your profile to show your details</p> </div> </div> </div> </div> <!-- end my bio section --> <!--  availability--> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/availability\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Availability</h2> </div> <div class=\"availability_section m-t-30\"> <div class=\"table-responsive\"> <table class=\"table table-bordered table-striped\"> <thead> <tr> <th style=\"width:30%\">&nbsp;</th> <th style=\"width:10%\">M</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">W</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">F</th> <th style=\"width:10%\">S</th> <th style=\"width:10%\">S</th> </tr> </thead> <tbody> <tr> <td>Before School care <br><span class=\"time_care\">6am - 9am</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.before_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.before_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Day care <br><span class=\"time_care\">9am - 3pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.day_care.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.day_care.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>After School care <br><span class=\"time_care\">3pm - 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.after_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.after_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Overnight care <br><span class=\"time_care\">After 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.overnight.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.overnight.indexOf(day) > -1\"></i> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> </div> <!--  End availability--> <div class=\"row\" ng-if=\"emp_history.length >0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/history\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Work Experience</h2> </div> <div class=\"no-data_found\"> <p><i class=\"fa fa-exclamation-triangle\"></i>&nbsp;No record found</p> </div> <div class=\"single_job_experience\" ng-repeat=\"history in emp_history\"> <h3 class=\"section-item-title-1\">{{history.company_name}}</h3> <h4 class=\"job_designation\">{{history.title}} - <span class=\"job-date\"> <span>{{months[history.from_month].name}} {{history.from_year}} - <span ng-if=\"history.to_month != 'null' && history.to_year != 'null'\">{{months[history.to_month].name}} {{history.to_year}}</span> <span ng-if=\"history.to_month == 'null' || history.to_year == 'null' || !history.to_month || !history.to_year\">Present</span> </span></span> </h4> <h4 class=\"location\">{{history.location}}</h4> <p class=\"mybio_p\">{{history.description}}</p> </div> <div ng-if=\"emp_history.length == 0\"> No data found!! </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"requirements.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/credential\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">credentials</h2> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Trust') > -1\"> <h4>Trust</h4> <ul class=\"credential_inline\"> <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\"> <div class=\"disp_profile_table\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span> </div> </div> </li> </ul> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Residency') > -1\"> <h4>Residency</h4> <ul class=\"credential_inline\"> <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\"> <div class=\"disp_profile_table\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span> </div> </div> </li> </ul> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Training and experience') > -1\"> <h4>Training & Education</h4> <ul class=\"credential_inline\"> <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\"> <div class=\"disp_profile_table\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span> </div> </div> </li> </ul> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Driving') > -1\"> <h4>Driving</h4> <ul class=\"credential_inline\"> <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\"> <div class=\"disp_profile_table\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span> </div> </div> </li> </ul> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Others') > -1\"> <h4>Others</h4> <ul class=\"credential_inline\"> <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\"> <div class=\"disp_profile_table\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span> </div> </div> </li> </ul> </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"others.interest\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/others\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Others</h2> </div> <div class=\"others_section_view\"> <h4>Interests & Favourites</h4> <p class=\"mybio_p\">{{others.interest}}</p> </div> <div class=\"others_section_view\" ng-if=\"others.household_chores.length > 0\"> <h4>Household Chores</h4> <ul class=\"household_ul\"> <li ng-repeat=\"chore in others.household_chores\"><i class=\"fa fa-check\"></i>&nbsp;{{chore}}</li> </ul> </div> <div class=\"others_section_view\" ng-if=\"others.languages.length > 0\"> <h4>Languages</h4> <ul class=\"household_ul\"> <li ng-repeat=\"language in others.languages\"><i class=\"fa fa-check\"></i>&nbsp;{{language}}</li> </ul> </div> <div class=\"others_section_view\"> <h4>Emergency Contact</h4> <!-- <div class=\"table-responsive reference_table\">\n" +
    "\n" +
    "                                 <table class=\"table table-bordered table-striped\">\n" +
    "                                   <tr>\n" +
    "                                     <th>Name</th>\n" +
    "                                     <th>Contact Number</th>\n" +
    "\n" +
    "                                   </tr>\n" +
    "                                   <tr ng-repeat=\"emergency in emp_emergencycontact\">\n" +
    "                                     <td>{{emergency.first_name}} {{emergency.last_name}}</td>\n" +
    "                                     <td>{{emergency.contactno}}</td>\n" +
    "\n" +
    "                                   </tr>\n" +
    "                                 </table>\n" +
    "                               </div> --> <p class=\"reference_p_view\" ng-repeat=\"emergency in emp_emergencycontact\"><i class=\"fa fa-check\"></i>&nbsp;&nbsp;{{emergency.first_name}} {{emergency.last_name}} <span class=\"contact_no_css\">( {{emergency.contactno}} )</span></p> <div ng-if=\"emp_emergencycontact.length == 0\"> No data found!! </div> </div> </div> </div> </div> </div> <!-- References --> <div class=\"row\" ng-if=\"references.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <p class=\"edit_icon_viewprofile\" uib-tooltip=\"Edit\"><a href=\"#/updateprofile/reference\"><i class=\"icon-pencil\"></i></a></p> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">References</h2> </div> <div class=\"table-responsive reference_table\"> <table class=\"table table-bordered table-striped\"> <tr> <th>Name</th> <th>Email</th> <th>Phone Number</th> </tr> <tr ng-repeat=\"ref in references\"> <td>{{ref.first_name}} {{ref.last_name}}</td> <td>{{ref.email}}</td> <td>{{ref.contactno}}</td> </tr> </table> </div> <!-- <div class=\"single_reference_section_view\" ng-repeat=\"ref in references\">\n" +
    "                              <a href=\"/assets/documents/references/{{ref.document}}\" download=\"{{ref.document}}\" ng-if=\"ref.document !='null' && ref.document\"><img src=\"styles/images/dd.png\" width=\"17\" uib-tooltip=\"Download\"></a>\n" +
    "                              <h4><i class=\"icon-user\"></i>&nbsp;{{ref.first_name}} {{ref.last_name}} |&nbsp;<i class=\"icon-envelope-open\"></i>&nbsp;{{ref.email}}&nbsp;|&nbsp;<i class=\"icon-phone\"></i>&nbsp;{{ref.contactno}}</h4>\n" +
    "\n" +
    "\n" +
    "                           </div> --> </div> </div> </div> </div> <!--  End Reference--> <!--  others--> <!--  End Credential--> </div> <div class=\"col-md-3\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">profile Completeness</h2> </div> <div class=\"complete_progress\"> <h5>Profile complete on {{profileCompleteness}}% <i class=\"icon-question\"></i></h5> <div class=\"progress\"> <div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"{{profileCompleteness}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" ng-style=\"{ 'width': (profileCompleteness+'%')}\"> </div> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"right_side_section_joblist\"> <div class=\"ad_section\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/views/parentprofile.html',
    "<div header ng-if=\"!adminLogged\"></div> <div class=\"content_section\"> <div class=\"row\"> <div class=\"col-md-12\" ng-if=\"!adminLogged\"> <ol class=\"breadcrumb\"> <li><a href=\"javascript:void(0)\">My Profile</a></li> <li class=\"active\">View Profile</li> </ol> </div> <div class=\"col-md-12\" ng-if=\"adminLogged\"> <ol class=\"breadcrumb\"> <li><a href=\"{{adminUrl}}\">Admin home</a></li> <li class=\"active\">View Profile</li> </ol> </div> <div class=\"col-md-9\"> <div class=\"my_profile_view\"> <div class=\"row\"> <div class=\"col-md-7 col-sm-7\"> <div class=\"row\"> <div class=\"m-b-10 clearfix\"> <div class=\"col-md-4 col-sm-5\"> <h5 class=\"parent_label\">First Name <span class=\"pull-right hidden-xs\">:</span></h5> </div> <div class=\"col-md-8 col-sm-7\"> <h5 class=\"\"><a href=\"javascript:void(0)\" editable-text=\"clientUser.first_name\" onaftersave=\"updateClientdata(clientUser)\" onbeforesave=\"clientDatavalidation(clientUser)\" e-pattern=\"[a-zA-Z]*\" e-maxlength=\"20\" e-title=\"Must be a string upto 20 characters\">{{ clientUser.first_name || \"empty\" }}</a></h5> </div> </div> </div> <div class=\"row\"> <div class=\"m-b-10 clearfix\"> <div class=\"col-md-4 col-sm-5\"> <h5 class=\"parent_label\">Last Name <span class=\"pull-right hidden-xs\">:</span></h5> </div> <div class=\"col-md-8 col-sm-7\"> <h5 class=\"\"><a href=\"javascript:void(0)\" editable-text=\"clientUser.last_name\" onaftersave=\"updateClientdata(clientUser)\" e-pattern=\"[a-zA-Z]*\" e-maxlength=\"20\" e-title=\"Must be a string upto 20 characters\">{{ clientUser.last_name || \"empty\" }}</a></h5> </div> </div> </div> <div class=\"row\"> <div class=\"m-b-10 clearfix\"> <div class=\"col-md-4 col-sm-5\"> <h5 class=\"parent_label\">Email Address <span class=\"pull-right hidden-xs\">:</span></h5> </div> <div class=\"col-md-8 col-sm-7\"> <h5 class=\"\"><a href=\"javascript:void(0)\" editable-email=\"clientUser.email\" onaftersave=\"updateClientdata(clientUser)\" edit-disabled=\"true\">{{ clientUser.email || \"empty\" }}</a></h5> </div> </div> </div> <div class=\"row\"> <div class=\"m-b-10 clearfix\"> <div class=\"col-md-4 col-sm-5\"> <h5 class=\"parent_label\">Country Code <span class=\"pull-right hidden-xs\">:</span></h5> </div> <div class=\"col-md-8 col-sm-7\"> <h5 class=\"\"><a href=\"javascript:void(0)\" editable-select=\"clientUser.country_code__id\" e-ng-options=\"s.id as s.code for s in countryCodes\" onaftersave=\"updateClientdata(clientUser)\"> <span class=\"\"> <i class=\"{{clientUser.country_code__flag_class}}\"></i>{{ clientUser.country_code__code || \"empty\" }}</span></a></h5>  </div> </div> </div> <div class=\"row\"> <div class=\"m-b-10 clearfix\"> <div class=\"col-md-4 col-sm-5\"> <h5 class=\"parent_label\">Phone Number <span class=\"pull-right hidden-xs\">:</span></h5> </div> <div class=\"col-md-8 col-sm-7\"> <h5 class=\"\"><a href=\"javascript:void(0)\" editable-text=\"clientUser.contactno\" e-pattern=\"\\d*\" e-title=\"Phone number must be digits\" e-maxlength=\"15\" onaftersave=\"updateClientdata(clientUser)\">{{ clientUser.contactno || \"empty\" }}</a></h5> </div> </div> </div> </div> <div class=\"col-md-5 col-sm-5\"> <div class=\"\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"profile_picture_section text-center\"> <div ng-if=\"!croppingImage && clientUser.profpic\"> <img ngf-src=\"'/images/profilepic/'+clientUser.profpic\" alt=\"\" class=\"img-responsive\"> </div> <div ng-if=\"croppingImage\"> <img ngf-thumbnail=\"myCroppedImage\" alt=\"\" class=\"img-responsive\"> </div> <div> <img ngf-thumbnail=\"'/images/no_image_1.png'\" alt=\"\" ng-if=\"!clientUser.profpic && !myCroppedImage\" class=\"img-responsive\"> </div> <div class=\"upload_btn\"> <p class=\"update_image_icon text-center\"> <input type=\"file\" accept=\"image/x-png,image/jpeg\" id=\"fileInput\" class=\"display_none\"> <button class=\"profile_pic_update_btn\" onclick=\"document.getElementById('fileInput').click()\"><i class=\"\"></i>Update Photo</button> </p> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"ad_section col-sm-hidden\"> <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\"> </div> </div> </div> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/views/public_profile.html',
    "<div header ng-if=\"!adminLogged\"></div> <div class=\"content_section\"> <div class=\"row\"> <div class=\"col-md-8\" ng-show=\"!adminLogged && (userType == '0' || userType == 'parent' || !userType)\"> <ol class=\"breadcrumb\"> <li><a href=\"#/login\">Home</a></li> <li><a href=\"#/findcarer\">Find a carer</a></li> <li class=\"active\">Public Profile</li> </ol> </div> <div class=\"col-md-8\" ng-show=\"(userType == '1' || userType == 'worker')\"> <ol class=\"breadcrumb\"> <li><a href=\"#/myprofile\">My Profile</a></li> <li class=\"active\">Public Profile</li> </ol> </div> <div class=\"col-md-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"!adminLogged && !previousJobid && (userType == '0' || userType == 'parent' || !userType) \" href=\"#/findcarer\"><i class=\"fa fa-reply\"></i>&nbsp;<span>Back to List</span></a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"!adminLogged && (userType == '1' || userType == 'worker')\" href=\"#/myprofile\"><i class=\"fa fa-reply\"></i>&nbsp;<span>Back to My profile</span></a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"previousJobid\" href=\"#/previewjob/{{previousJobid}}/quotes\" ng-click=\"removeCookie('previousJobid')\"><i class=\"fa fa-reply\"></i>&nbsp;<span>Back to Preview job</span></a> <a class=\"edit_profile_btn hvr-wobble-horizontal\" ng-if=\"adminLogged\" href=\"{{adminUrl}}\"><i class=\"fa fa-reply\"></i>&nbsp;<span>Back to List</span></a> </div> </div> <!-- public profile starts here --> <div class=\"myprofile_section clearfix\" x-sticky-boundary=\"\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-9\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-4 col-sm-4\"> <div class=\"profile-pic\"> <div class=\"profile-border\"> <img src=\"/images/profilepic/{{personal.profpic}}\" ng-if=\"personal.profpic\" alt=\"profilepicture\" class=\"m-t-4\"> <img src=\"/images/no_image_1.png\" ng-if=\"!personal.profpic\" alt=\"profilepicture_not_found\" class=\"m-t-4\"> </div> </div> <div class=\"text-center worker_rating\"> <button ng-click=\"openMessagepopup(personal.id)\" class=\"btn btn-success public_contact_btn hvr-wobble-horizontal m-t-10\" ng-if=\"(userType != '1' && userType != 'worker')\">Contact</button> <h5 class=\"intro-gender\" ng-if=\"personal.age && personal.age !='not specified'\">{{personal.age}} years old &nbsp;<span ng-if=\"personal.gender\">{{personal.gender | capitalize}}</span></h5> </div> </div> <div class=\"col-md-8 col-sm-8\"> <div class=\"single_right_side_container\"> <div class=\"row\"> <div class=\"single_right_side_data clearfix\"> <div class=\"col-sm-12\"> <h5 class=\"intro-title1\"><span class=\"first_name_css\">{{personal.first_name}}</span> <span class=\"last_name_css\">{{personal.last_name}} &nbsp; <span class=\"worker_rating\"><input-stars max=\"5\" ng-model=\"YourCtrl.property\" ng-attr-readonly=\"true\" allow-half></input-stars></span></span></h5> <h2 class=\"intro-title2\" ng-bind=\"preference.caregivers.join(' / ')\"></h2> </div> <!-- <div class=\"col-sm-12\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"location_section\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-directions\" class=\"pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ng-binding\" ng-if=\"personal.address\"> {{personal.address}}</span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ng-binding\" ng-if=\"!personal.address\"> - </span>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div> --> <div class=\"col-sm-12 m-t-10\"> <div class=\"location_section\"> <i class=\"icon-location-pin\" class=\"pull-left\"></i> <span class=\"ng-binding\" ng-if=\"personal.post_code\"> {{personal.city}},{{personal.state}} {{personal.post_code}}</span> <span class=\"ng-binding\" ng-if=\"!personal.post_code\">-</span> </div> </div> </div> </div> <div class=\"row\"> <div class=\"single_right_side_data clearfix bb-none\"> <div class=\"col-sm-12\"> <div> <strong class=\"label_myprofile\">Preferred job type</strong> <p class=\"value_myprofile\"><span ng-if=\"preference.job_type.indexOf('casual') > -1\">Casual / One-offcare</span> <span ng-if=\"preference.job_type.length > 1\">, </span> <span ng-if=\"preference.job_type.indexOf('regular') > -1\">Regular care</span></p> </div> <div class=\"m-t-10\" ng-if=\"preference.children_age.length > 0\"> <strong class=\"label_myprofile\">Preferred Age Groups </strong> <p class=\"value_myprofile\" ng-if=\"preference.children_age.length > 0\">{{preference.children_age}}</p> </div> </div> </div> </div> <!-- <div class=\"row\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"single_right_side_data clearfix bb-none\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"value_myprofile\"><i class=\"icon-envelope\"></i>&nbsp;{{personal.email}}</p>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-6\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"value_myprofile\"><i class=\"icon-phone\"></i>&nbsp;{{personal.country_code}} {{personal.contactno}}</p>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div> --> <div class=\"row\"> <div class=\"col-md-12\"> </div> </div> </div> </div> </div> </div> </div> <!-- my bio section --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <h2 class=\"h3 u-heading-v3__title\">About Me</h2> <p class=\"mybio_p\" ng-if=\"personal.about\" ng-bind-html=\"personal.about\"></p> <p class=\"mybio_p\" ng-if=\"!personal.about\">Content Not available</p> </div> </div> </div> </div> <!-- end my bio section --> <!--  availability--> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Availability</h2> </div> <div class=\"availability_section m-t-30\"> <div class=\"table-responsive\"> <table class=\"table table-bordered table-striped\"> <thead> <tr> <th style=\"width:30%\">&nbsp;</th> <th style=\"width:10%\">M</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">W</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">F</th> <th style=\"width:10%\">S</th> <th style=\"width:10%\">S</th> </tr> </thead> <tbody> <tr> <td>Before School care <br><span class=\"time_care\">6am - 9am</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.before_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.before_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Day care <br><span class=\"time_care\">9am - 3pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.day_care.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.day_care.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>After School care <br><span class=\"time_care\">3pm - 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.after_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.after_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Overnight care <br><span class=\"time_care\">After 7pm</span> </td> <td ng-repeat=\"day in Days\"> <i class=\"not-available\" ng-if=\"availability.overnight.indexOf(day) == -1\"></i> <i class=\"fa fa-check\" ng-if=\"availability.overnight.indexOf(day) > -1\"></i> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> </div> <!--  End availability--> <div class=\"row\" ng-if=\"emp_history.length >0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Work Experience</h2> </div> <div class=\"no-data_found hide\"> <p><i class=\"fa fa-exclamation-triangle\"></i>&nbsp;No record found</p> </div> <div class=\"single_job_experience\" ng-repeat=\"history in emp_history\"> <h3 class=\"section-item-title-1\">{{history.company_name}}</h3> <h4 class=\"job_designation\">{{history.title}} - <span class=\"job-date\"> <span>{{months[history.from_month].name}} {{history.from_year}} - <span ng-if=\"history.to_month != 'null' && history.to_year != 'null'\">{{months[history.to_month].name}} {{history.to_year}}</span> <span ng-if=\"history.to_month == 'null' || history.to_year == 'null' || !history.to_month || !history.to_year\">Present</span> </span></span> </h4> <h4 class=\"location\">{{history.location}}</h4> <p class=\"mybio_p\">{{history.description}}</p> </div> <div ng-if=\"emp_history.length == 0\"> No data found!! </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"requirements.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">credentials</h2> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Trust') > -1\"> <h4>Trust</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\" ng-if=\"adminLogged && (userType == '0' || userType == 'parent')\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Trust' }\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"disp_profile_table\" >\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_profile\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_content\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Residency') > -1\"> <h4>Residency</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\" ng-if=\"adminLogged && (userType == '0' || userType == 'parent')\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Residency' }\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"disp_profile_table\" >\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_profile\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_content\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Training and experience') > -1\"> <h4>Training & Education</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\" ng-if=\"adminLogged && (userType == '0' || userType == 'parent')\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Training and experience' }\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"disp_profile_table\" >\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_profile\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_content\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Driving') > -1\"> <h4>Driving</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\" ng-if=\"adminLogged && (userType == '0' || userType == 'parent')\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Driving' }\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"disp_profile_table\" >\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_profile\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_content\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t </ul> --> </div> <div class=\"single_credential_section\" ng-if=\"availableCreds.indexOf('Others') > -1\"> <h4>Others</h4> <div class=\"row\"> <div class=\"col-md-6 col-sm-12 col-xs-12\" ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\"> <div class=\"m-b-20\"> <div class=\"icon_class_profile\"> <i class=\"{{req.requirement_id__icon_class}} pull-left\"></i> </div> <div class=\"icon_class_content\"> <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\" ng-if=\"adminLogged && (userType == '0' || userType == 'parent')\"><img src=\"styles/images/blue.png\" alt=\"\" width=\"16\"></a></span> </div> </div> </div> </div> <!-- <ul class=\"credential_inline\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"req in requirements | filter: { requirement_id__category: 'Others' }\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <div class=\"disp_profile_table\" >\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_profile\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"{{req.requirement_id__icon_class}}  pull-left\"></i>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"icon_class_content\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <span>{{req.requirement_id__requirement}}&nbsp;&nbsp;<a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" uib-tooltip=\"Download\"><i class=\"fa fa-download\"></i></a></span>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </div>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t </ul> --> </div> </div> </div> </div> </div> <div class=\"row\" ng-if=\"others.interest\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">Others</h2> </div> <div class=\"others_section_view\"> <h4>Interests & Favourites</h4> <p class=\"mybio_p\" ng-bind-html=\"others.interest\"></p> </div> <div class=\"others_section_view\" ng-if=\"others.household_chores.length > 0\"> <h4>Household Chores</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"chore in others.household_chores\"> <p class=\"chore_value\"><i class=\"fa fa-check\"></i>&nbsp;{{chore | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"chore in others.household_chores\"><i class=\"fa fa-check\"></i>&nbsp;{{chore}}</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul> --> </div> <div class=\"others_section_view\" ng-if=\"others.languages.length > 0\"> <h4>Languages</h4> <div class=\"row\"> <div class=\"col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"language in others.languages\"> <p class=\"chore_value\"><i class=\"fa fa-check\"></i>&nbsp;{{language | capitalize}}</p> </div> </div> <!-- <ul class=\"household_ul\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t <li ng-repeat=\"language in others.languages\"><i class=\"fa fa-check\"></i>&nbsp;{{language}}</li>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul> --> </div> <div class=\"others_section_view\" ng-if=\"emp_emergencycontact.length > 0\"> <h4>Emergency Contact</h4> <p class=\"reference_p_view\" ng-repeat=\"emergency in emp_emergencycontact\"><i class=\"fa fa-check\"></i>&nbsp;&nbsp;{{emergency.first_name}} {{emergency.last_name}} <span class=\"contact_no_css\">( {{emergency.contactno}} )</span></p> <div ng-if=\"emp_emergencycontact.length == 0\"> No data found!! </div> </div> </div> </div> </div> </div> <!-- References --> <div class=\"row\" ng-if=\"references.length > 0\"> <div class=\"col-md-12\"> <div class=\"my_profile_view clearfix\"> <div class=\"col-md-12\"> <div class=\"u-heading-v3\"> <h2 class=\"h3 u-heading-v3__title\">References</h2> </div> <div class=\"table-responsive reference_table\"> <table class=\"table table-bordered table-striped\"> <tr> <th>Name</th> <th>Email</th> <th>Phone Number</th> </tr> <tr ng-repeat=\"ref in references\"> <td>{{ref.first_name}} {{ref.last_name}}</td> <td>{{ref.email}}</td> <td>{{ref.contactno}}</td> </tr> </table> </div> <!-- <div class=\"single_reference_section_view\" ng-repeat=\"ref in references\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t <a href=\"/assets/documents/references/{{ref.document}}\" download=\"{{ref.document}}\" ng-if=\"ref.document !='null' && ref.document\"><img src=\"styles/images/dd.png\" width=\"16\" uib-tooltip=\"Download\"></a>\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t <h4><i class=\"icon-user\"></i>&nbsp;{{ref.first_name}} {{ref.last_name}} |&nbsp;<i class=\"icon-envelope-open\"></i>&nbsp;{{ref.email}}&nbsp;|&nbsp;<i class=\"icon-phone\"></i>&nbsp;{{ref.contactno}}</h4>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t</div> --> </div> </div> </div> </div> <!--  End Reference--> <!--  others--> <!--  End Credential--> </div> <div class=\"col-md-3\"> <!-- <div class=\"ad_section\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "\t\t\t\t\t\t\t\t\t\t\t </div> --> <div x-sticky=\"\" ng-if=\"!userType\" set-class-when-at-top=\"ad-fixed\"> <div ad1></div> </div> <div x-sticky=\"\" ng-if=\"userType\" set-class-when-at-top=\"ad-fixed\"> <div ad2></div> </div> </div> </div> </div> </div> </div> </div> <div footer></div>"
  );


  $templateCache.put('scripts/module/user/views/updateprofile.html',
    "<!-- <div class=\"pod_loader_img\">\n" +
    "                <div class=\"loader\"></div>\n" +
    "            </div> --> <div header></div> <div class=\"content_section\"> <section class=\"updateprofile\"> <div class=\"row\"> <div class=\"col-md-8 col-sm-8 col-sm-12\"> <ol class=\"breadcrumb\"> <li><a href=\"#/myprofile\">My Profile</a></li> <li class=\"active\">Update Profile</li> </ol> </div> <div class=\"col-md-4 col-sm-4\"> <a class=\"edit_profile_btn hvr-wobble-horizontal\" href=\"#/myprofile\"><i class=\"fa fa-reply\"></i>&nbsp;<span>View Profile</span></a> </div> </div> <div class=\"privacy_lock_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <section class=\"notif notif-notice security_information\"> <ul> <li> <i class=\"icon-lock privacy_i\"></i> </li> <li> <p>We will keep the following information private, Which will not be shown as part of your public profile: Email Id, Mobile Number, Address, Attached documents of your Credentials, Emergency Contact Details, Reference and Resume.This information will be shared with clients only after your booking is confirmed and payment is transferred to our secured holding account.</p> </li> </ul> <!-- <p><i class=\"icon-lock privacy_i\"></i>&nbsp;We will keep the following information private, Which will not be shown as part of your public profile: Email Id, Mobile Number, Address, Attached documents of your Credentials, Emergency Contact Details, Reference and Resume.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account.</p> --> </section> </div> </div> </div> <div class=\"updateprofile_section\"> <div class=\"row\"> <div class=\"col-md-9\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"tabs_wrapper\"> <ul class=\"tabs\"> <li rel=\"tab1\" ng-click=\"switchTab('personal')\" ng-class=\"{'active':currentTab == 'personal'}\" id=\"personal\"><i class=\"fa fa-user-o\"></i>&nbsp;About Yourself</li> <li rel=\"tab2\" ng-click=\"switchTab('preference')\" ng-class=\"{'active':currentTab == 'preference'}\" id=\"preference\"><i class=\"icon-wrench\"></i>&nbsp;Preferences</li> <li rel=\"tab3\" ng-click=\"switchTab('availability')\" ng-class=\"{'active':currentTab == 'availability'}\" id=\"availability\"><i class=\"icon-clock\"></i>&nbsp;Availability</li> <li rel=\"tab4\" ng-click=\"switchTab('credential')\" ng-class=\"{'active':currentTab == 'credential'}\" id=\"credential\"><i class=\"fa fa-address-card-o\"></i>&nbsp;Credentials</li> <li rel=\"tab5\" ng-click=\"switchTab('history')\" ng-class=\"{'active':currentTab == 'history'}\" id=\"history\"><i class=\"icon-briefcase\"></i>&nbsp;Employment History</li> <li rel=\"tab6\" ng-click=\"switchTab('others')\" ng-class=\"{'active':currentTab == 'others'}\" id=\"others\"><i class=\"icon-question\"></i>&nbsp;Others</li> <li rel=\"tab7\" ng-click=\"switchTab('reference')\" ng-class=\"{'active':currentTab == 'reference'}\" id=\"reference\"><i class=\"icon-people\"></i>&nbsp;References</li> <!-- <li rel=\"tab8\" ng-click=\"switchTab('bankinfo')\" ng-class=\"{'active':currentTab == 'bankinfo'}\" id=\"bankinfo\"><i class=\"fa fa-university\"></i>&nbsp;Bank Information</li> --> </ul> <div class=\"tab_container\"> <div class=\"loader_div ng-hide\" ng-show=\"generalLoader\"> <div class=\"child_loader\"> <i class=\"fa fa-spinner fa-spin\" style=\"font-size: 30px\"></i> </div> </div> <h3 class=\"d_active tab_drawer_heading\" rel=\"tab1\" ng-click=\"switchTab('personal')\">About Yourself<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'personal','icon-arrow-down':currentTab != 'personal'}\"></i></h3> <div id=\"tab1\" class=\"tab_content\" ng-if=\"currentTab == 'personal'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"personalForm\" novalidate> <div class=\"update_profile_step1 postjob_type_section1\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <h5 class=\"update_heading\">About Yourself</h5> --> <!-- <h1 class=\"main_heading prefer_icon\">About Yourself</h1> --> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">First Name</h4> <input type=\"text\" name=\"first_name\" ng-model=\"personal.first_name\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.first_name.$dirty) && personalForm.first_name.$invalid\"> <p ng-if=\"personalForm.first_name.$error.required\">Please enter first name</p> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Last Name</h4> <input type=\"text\" ng-model=\"personal.last_name\" name=\"last_name\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.last_name.$dirty) && personalForm.last_name.$invalid\"> <p ng-if=\"personalForm.last_name.$error.required\">Please enter last name</p> </div> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <h4 class=\"m-b-0\">Mobile Number&nbsp;<i class=\"icon-lock privacy_i\" uib-tooltip=\"We will keep this information private, Which will not be shown has part of your public profile.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account\" tooltip-placement=\"right\"></i></h4> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"group group1 chosen_style n_flg flag_custom_style\" title=\"{{selectedCode.country_name}}\"> <!--<i class=\"selectedCode.flag_class\"></i>--> <span class=\"flag_number\"> <i class=\"{{selectedCode.flag_class}}\"></i> {{selectedCode.code}}&nbsp;{{selectedCode.country_name}} </span> <div class=\"dropdown countrycodes_spn\"> <a aria-expanded=\"false\" aria-haspopup=\"true\" role=\"button\" data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"javascript:void(0)\"> <span id=\"selected\"></span><span class=\"caret\"></span></a> <ul class=\"dropdown-menu country-codes\"> <li ng-repeat=\"cc in countryCodes\" id=\"{{$index}}option\"><a href=\"javascript:void(0)\" ng-click=\"selectCode(cc,$index)\"><i class=\"{{cc.flag_class}}\"></i><span class=\"contr_name\">{{cc.country_name}}</span> <b class=\"c_code\">{{cc.code}}</b></a></li> </ul> </div> </div> </div> <div class=\"col-md-6 group group1\"> <input type=\"text\" ng-model=\"personal.contactno\" name=\"contactno\" ng-maxlength=\"15\" ng-minlength=\"7\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.contactno.$dirty) && personalForm.contactno.$invalid\"> <p ng-if=\"personalForm.contactno.$error.required\">Please enter contact number</p> <p ng-if=\"personalForm.contactno.$error.minlength\">Invalid contact number</p> <p ng-if=\"personalForm.contactno.$error.maxlength\">Invalid contact number</p> <!-- <p ng-if=\"!personalForm.contactno.$error.required && personalForm.contactno.$invalid\">Invalid contact number</p> --> </div> </div> </div> <!-- <div class=\"group group1 chosen_style\">\n" +
    "                                                                                        <i class=\"selectedCode.flag_class\"></i>{{selectedCode.code}}\n" +
    "                                                                                        <div class=\"dropdown\">\n" +
    "                                                                                        <a aria-expanded=\"false\" aria-haspopup=\"true\" role=\"button\" data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"javascript:void(0)\">\n" +
    "                                                                                        <span id=\"selected\">Select country code</span><span class=\"caret\"></span></a>\n" +
    "                                                                                        <ul class=\"dropdown-menu country-codes\">\n" +
    "                                                                                           <li ng-repeat=\"cc in countryCodes\" id=\"{{$index}}option\"><a href=\"javascript:void(0)\" ng-click=\"selectCode(cc,$index)\"><i class=\"{{cc.flag_class}}\"></i>{{cc.country_name}} {{cc.code}}</a></li>\n" +
    "                                                                                        </ul>\n" +
    "                                                                                        </div>\n" +
    "                                                                                        </div> --> <!-- <input type=\"text\" ng-model=\"personal.contactno\" name=\"contactno\" ng-required=\"true\">\n" +
    "                                                                            <span class=\"highlight\"></span>\n" +
    "                                                                            <span class=\"bar\"></span>\n" +
    "                                                                            <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.contactno.$dirty) && personalForm.contactno.$invalid\">\n" +
    "                                                                                <p ng-if=\"personalForm.contactno.$error.required\">Please enter contactno</p>\n" +
    "                                                                                <p ng-if=\"!personalForm.contactno.$error.required && personalForm.contactno.$invalid\">Invalid contact number</p>\n" +
    "                                                                            </div> --> </div> <div class=\"col-md-6\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Email&nbsp;<i class=\"icon-lock privacy_i\" uib-tooltip=\"We will keep this information private, Which will not be shown has part of your public profile.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account\" tooltip-placement=\"right\"></i> </h4> <input type=\"email\" ng-model=\"personal.email\" name=\"email\" ng-disabled=\"true\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.email.$dirty) && personalForm.email.$invalid\"> <p ng-if=\"personalForm.email.$error.required\">Please enter email</p> </div> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Address&nbsp;<i class=\"icon-lock privacy_i\" uib-tooltip=\"We will keep this information private, Which will not be shown has part of your public profile.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account\" tooltip-placement=\"right\"></i></h4> <input type=\"text\" ng-model=\"personal.address\" name=\"address\" ng-required=\"true\" maxlength=\"500\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.address.$dirty) && personalForm.address.$invalid\"> <p ng-if=\"personalForm.address.$error.required\">Please enter address</p> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Suburb, State Postal code</h4> <p class=\"icon_load_spin\"> <!-- <input type=\"text\" ng-model=\"personal.fullAddress\" name=\"location\" uib-typeahead=\"location as location.viewval for location in filteredLocations\" typeahead-wait-ms=\"600\" typeahead-on-select=\"setTag($item,true)\" ng-keyup=\"getLocations(personal.fullAddress)\" ng-required=\"true\" autocomplete=\"off\"/> --> <input type=\"text\" ng-model=\"personal.fullAddress\" name=\"location\" uib-typeahead=\"location as location.viewval for location in getLocations($viewValue)\" typeahead-wait-ms=\"500\" placeholder=\"Suburb,state postcode\" typeahead-on-select=\"setTag($item,true)\" ng-change=\"clearLocationCall()\" ng-required=\"true\" autocomplete=\"off\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <span class=\"ajax_loader\" ng-if=\"locLoading\"><i class=\"fa fa-spinner fa-spin\"></i></span> <span class=\"ajax_loader color_red\" ng-if=\"personal.fullAddress && !locLoading\" title=\"Clear location\" ng-click=\"personal.fullAddress = '';personal.city='';personal.state='';personal.post_code=''\"><i class=\"fa fa-times-circle\"></i></span> </p> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.location.$dirty) && personalForm.location.$invalid\"> <p ng-if=\"personalForm.location.$error.required\">Please enter location</p> </div> <div class=\"errormsg\"> <p ng-if=\"!personal.post_code && (personalForm.$submitted || personalForm.location.$dirty) && !personalForm.location.$error.required\">Invalid location</p> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"group group1 chosen_style\"> <h4 class=\"m-b-0\">Gender</h4> <select class=\"\" name=\"gender\" ng-model=\"personal.gender\" chosen disable-search=\"true\" ng-required=\"false\"> <option value=\"\">Select</option> <option value=\"male\">Male</option> <option value=\"female\">Female</option> <option value=\"trans\">Trans*</option> <option value=\"not_specified\">Prefer not to specify</option> </select> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.gender.$dirty) && personalForm.gender.$invalid\"> <p ng-if=\"personalForm.gender.$error.required\">Please select gender</p> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"group group1 chosen_style\"> <h4 class=\"m-b-0\">Year of birth</h4> <select class=\"\" chosen ng-model=\"personal.year_of_birth\" name=\"year_of_birth\" ng-change=\"calculateAge(personal.year_of_birth)\" disable-search=\"true\" ng-required=\"false\" convert-to-number> <option value=\"0\">Select</option> <option ng-repeat=\"year in years\" value=\"{{year}}\">{{year}}</option> </select> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.year_of_birth.$dirty) && personalForm.year_of_birth.$invalid\"> <p ng-if=\"personalForm.year_of_birth.$error.required\">Please select Year</p> </div> </div> </div> <div class=\"col-md-2\" ng-if=\"personal.age\"> <!--  <h4 class=\"m-b-0 not-available\">Age</h4> --> <h4 class=\"custom_age custom_agenew\">Age&nbsp;<span class=\"\">{{personal.age}}</span></h4> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"group group1 chosen_style\"> <h4 class=\"m-b-0\">Number of years of relevant experience</h4> <select class=\"\" chosen ng-model=\"personal.relevant_year\" name=\"relevant_year\" disable-search=\"true\" ng-required=\"false\"> <option value=\"\">Select</option> <option value=\"0-2\">0-2 years</option> <option value=\"2-4\">2-4 years</option> <option value=\"4-6\">4-6 years</option> <option value=\"6-8\">6-8 years</option> <option value=\"8-10\">8-10 years</option> <option value=\"10+\">more than 10 years</option> </select> <!-- <input type=\"text\" name=\"relevant_year\" ng-model=\"personal.relevant_year\" ng-required=\"true\" ng-pattern=\"/[0-9]$/\"> --> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.relevant_year.$dirty) && personalForm.relevant_year.$invalid\"> <p ng-if=\"personalForm.relevant_year.$error.required\">Please enter years of relevant experience</p> <p ng-if=\"personalForm.relevant_year.$error.pattern\">Years should be in number</p> </div> </div> </div> <div class=\"col-md-12 editor_global_style\"> <h4 class=\"m-b-0\">Please tell us about yourself</h4> <!-- <div  class=\"m-t-20\" ng-model=\"personal.about\" text-angular ta-toolbar=\"[['justifyLeft','justifyCenter'],['bold','italics','underline']]\" name=\"about\" ng-required=\"false\"></div> --> <div class=\"m-t-20\"> <textarea ckeditor=\"ckoptions\" ng-model=\"personal.about\" ng-required=\"false\" name=\"about\" height=\"200\"></textarea> </div> <!-- <div class=\"form-control update_textarea\" ng-model=\"personal.about\" text-angular ta-toolbar=\"[['justifyLeft','justifyCenter'],['bold','italics','underline']]\" name=\"about\" ng-required=\"false\"></div> --> <div class=\"errormsg\" ng-if=\"(personalForm.$submitted || personalForm.about.$dirty) && personalForm.about.$invalid\"> <p ng-if=\"personalForm.about.$error.required\">Please Tell about yourself</p> <p ng-if=\"personalForm.about.$error.minlength\">Must above 200 characters</p> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 pull-right action_section_update_profile btnprofedit\"> <button type=\"button\" class=\"hvr-float-shadow btn pull-right btn-default custom_btn_update\" ng-click=\"saveProfile(personalForm,'preference')\">Save & Next <i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"saveProfile(personalForm,0)\" class=\"hvr-float-shadow btn pull-right btn-default custom_btn_update_back\">Save & Exit</button> </div> </div> </ng-form> </div> </div> </div> <!-- #tab1 --> <h3 class=\"tab_drawer_heading\" rel=\"tab2\" ng-click=\"switchTab('preference')\">Preferences<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'preference','icon-arrow-down':currentTab != 'preference'}\"></i></h3> <div id=\"tab2\" class=\"tab_content\" ng-if=\"currentTab == 'preference'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"prefForm\" class=\"\"> <div class=\"update_profile_step2 postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <h5 class=\"update_heading\">Preferences</h5> --> <!-- <h1 class=\"main_heading prefer_icon\">Preferences</h1> --> </div> <div class=\"col-sm-12\"> <h4 class=\"m-b-0\">What types of jobs are you looking for ?</h4> <p class=\"manadarory_style\">you can select multiple types</p> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\"> <label class=\"control control--checkbox\"> Casual / One-offcare <input type=\"checkbox\" ng-checked=\"preference.job_type.indexOf('casual') > -1\" name=\"job_type\" value=\"0\" ng-click=\"toggleJobtype('casual')\"> <div class=\"control__indicator\"></div> </label> </div> <div class=\"col-sm-6\"> <label class=\"control control--checkbox\"> Regular care <input type=\"checkbox\" ng-checked=\"preference.job_type.indexOf('regular') > -1\" name=\"job_type\" ng-click=\"toggleJobtype('regular')\" value=\"1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> </div> <div class=\"col-sm-12\"> <h4 class=\"m-b-0\">What type/s of caregiver roles suit you?</h4> <p class=\"manadarory_style\">you can select multiple types</p> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"cg in caregiversType\"> <label class=\"control control--checkbox\"> {{cg}} <input type=\"checkbox\" ng-click=\"togglecaregiver(cg)\" ng-checked=\"preference.caregivers.indexOf(cg) > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> </div> <div class=\"col-sm-12\"> <h4 class=\"m-b-0\">Preferred Age group of children</h4> <p class=\"manadarory_style\">you can select multiple Age group</p> <div class=\"check_box_section\"> <div class=\"control-group\"> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"ca in childrenAge\"> <label class=\"control control--checkbox\"> {{ca}} <input type=\"checkbox\" ng-click=\"toggleAge(ca)\" ng-checked=\"preference.children_age.indexOf(ca) > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 pull-right action_section_update_profile btnprofedit\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"savePref(prefForm,'availability')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"savePref(prefForm,0)\" class=\"btn pull-right btn-default hvr-float-shadow custom_btn_update_back\">Save & Exit</button> <button href=\"#\" class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('personal')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> </div> </div> </div> </ng-form> </div> </div> </div> <!-- #tab2 --> <h3 class=\"tab_drawer_heading\" rel=\"tab3\" ng-click=\"switchTab('availability')\">Availability<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'availability','icon-arrow-down':currentTab != 'availability'}\"></i></h3> <div id=\"tab3\" class=\"tab_content\" ng-if=\"currentTab == 'availability'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"avaialbilityform\"> <div class=\"update_profile_step3 postjob_type_section\"> <div class=\"row\"> <div class=\"col-md-12 postjob_type_section\"> <!-- <h5 class=\"update_heading\">Availabilty</h5> --> <h4 class=\"text-orange\">* Please select your available days and time slots.</h4> </div> <div class=\"col-sm-12 availability_section m-t-10\"> <div class=\"table-responsive\"> <table class=\"table table-bordered\"> <thead> <tr> <th style=\"width:30%\">&nbsp;</th> <th style=\"width:10%\">M</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">W</th> <th style=\"width:10%\">T</th> <th style=\"width:10%\">F</th> <th style=\"width:10%\">S</th> <th style=\"width:10%\">S</th> </tr> </thead> <tbody> <tr> <td>Before School Care <br><span>6am - 9am</span> </td> <td ng-repeat=\"day in Days\"> <!-- <input type=\"checkbox\" name=\"\" ng-click=\"toggleBeforesch(day)\" ng-checked=\"availability.before_school.indexOf(day) > -1\"> --> <i class=\"fa fa-square gray_i\" ng-click=\"toggleBeforesch(day)\" ng-if=\"availability.before_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check-square\" ng-click=\"toggleBeforesch(day)\" ng-if=\"availability.before_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Day Care <br><span>9am - 3pm</span> </td> <td ng-repeat=\"day in Days\"> <!-- <input type=\"checkbox\" name=\"\" ng-click=\"toggleDaycare(day)\" ng-checked=\"availability.day_care.indexOf(day) > -1\"> --> <i class=\"fa fa-square gray_i\" ng-click=\"toggleDaycare(day)\" ng-if=\"availability.day_care.indexOf(day) == -1\"></i> <i class=\"fa fa-check-square\" ng-click=\"toggleDaycare(day)\" ng-if=\"availability.day_care.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>After School Care <br><span>3pm - 7pm</span> </td> <td ng-repeat=\"day in Days\"> <!-- <input type=\"checkbox\" name=\"\" ng-click=\"toggleAftersch(day)\" ng-checked=\"availability.after_school.indexOf(day) > -1\"> --> <i class=\"fa fa-square gray_i\" ng-click=\"toggleAftersch(day)\" ng-if=\"availability.after_school.indexOf(day) == -1\"></i> <i class=\"fa fa-check-square\" ng-click=\"toggleAftersch(day)\" ng-if=\"availability.after_school.indexOf(day) > -1\"></i> </td> </tr> <tr> <td>Overnight Care <br><span>After 7pm</span> </td> <td ng-repeat=\"day in Days\"> <!-- <input type=\"checkbox\" name=\"\" ng-click=\"toggleOvernight(day)\" ng-checked=\"availability.overnight.indexOf(day) > -1\"> --> <i class=\"fa fa-square gray_i\" ng-click=\"toggleOvernight(day)\" ng-if=\"availability.overnight.indexOf(day) == -1\" ng-click=\"toggleOvernight(day)\"></i> <i class=\"fa fa-check-square\" ng-click=\"toggleOvernight(day)\" ng-if=\"availability.overnight.indexOf(day) > -1\" ng-click=\"toggleOvernight(day)\"></i> </td> </tr> </tbody> </table> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 pull-right action_section_update_profile btnprofedit\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"saveAvailability(avaialbilityform,'credential')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"saveAvailability(avaialbilityform,0)\" class=\"hvr-float-shadow btn pull-right btn-default custom_btn_update_back\">Save & Exit</button> <button href=\"#\" class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('preference')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> <!-- <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"saveAvailability(avaialbilityform,'credential')\">Save & Next <i class=\"icon-arrow-right\"></i></button>\n" +
    "                                                <button href=\"#\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"switchTab('preference')\"><i class=\"icon-arrow-left\"></i>Back</button>\n" +
    "                                                <button href=\"#\" ng-click=\"saveAvailability(avaialbilityform,0)\" class=\"btn pull-right btn-default custom_btn_update\">Save & Exit</button> --> </div> </div> </div> </ng-form> </div> </div> </div> <!-- #tab3 --> <h3 class=\"tab_drawer_heading\" rel=\"tab4\" ng-click=\"switchTab('credential')\">Credentials<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'credential','icon-arrow-down':currentTab != 'credential'}\"></i></h3> <div id=\"tab4\" class=\"tab_content\" ng-if=\"currentTab == 'credential'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"avaialbilityform1\" ng-validate=\"personalOptions\" class=\"update_profile_step2\"> <div class=\"update_profile_step4 postjob_type_section1\"> <div class=\"row\"> <div class=\"col-md-12 postjob_type_section\"> <!-- <h5 class=\"update_heading\">Credentials</h5> --> <!-- <h1 class=\"main_heading prefer_icon\">Credentials</h1> --> <!-- <h5 class=\"update_heading\">Availabilty</h5> --> <h4 class=\"text-orange\">* Please upload your credentials.</h4> <div class=\"row\"> <div class=\"col-md-12\"> <p>Trust</p> <div class=\"check_box_section\"> <div class=\"control-group\" ng-repeat=\"req in reqs | filter: { category: 'Trust' }\" ng-init=\"req.progress = 0;\"> <label class=\"control control--checkbox\" ng-init=\"cbId='trust_'+$index;\"> {{req.requirement}}&nbsp;<a href=\"javascript:void(0)\" ngf-select=\"saveDoc($file,req)\" ng-model=\"file\" name=\"file\" class=\"attach_btn\" ng-if=\"req.has_proof==1 && !req.document_name && req.has_req && !req.uploading\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Attach</a>&nbsp; <!-- <a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;View Attachment</a>&nbsp;\n" +
    "                                                                                    <a href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name\" ng-click=\"removeDoc(userid,req)\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Delete Attachment</a>&nbsp; --> <a class=\"action_update_credential m-r-5\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name && !req.uploading\" uib-tooltip=\"View attachment\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential\" href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name && !req.uploading\" ng-click=\"removeDoc(userid,req)\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> <input type=\"checkbox\" ng-checked=\"req.has_req\" ng-attr-id=\"cbId\" ng-click=\"uploadProof(req.id,req,cbId)\" ng-disabled=\"req.document_name && req.has_req\" ng-class=\"{'cb_disable':req.document_name && req.has_req}\"> <div class=\"control__indicator\"></div> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"req.progress\" ng-show=\"req.uploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> </label> </div> </div> </div> <div class=\"col-md-12\"> <p>Residency</p> <div class=\"check_box_section\"> <div class=\"control-group\" ng-repeat=\"req in reqs | filter: { category: 'Residency' }\"> <label class=\"control control--checkbox\" ng-init=\"cbId='trust_'+$index;\"> {{req.requirement}}&nbsp;<a href=\"javascript:void(0)\" ngf-select=\"saveDoc($file,req)\" ng-model=\"file\" name=\"file\" class=\"attach_btn\" ng-if=\"req.has_proof==1 && !req.document_name && req.has_req && !req.uploading\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Attach</a>&nbsp; <!-- <a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;View Attachment</a>&nbsp;\n" +
    "                                                                                    <a href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name\" ng-click=\"removeDoc(userid,req)\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Delete Attachment</a>&nbsp; --> <a class=\"action_update_credential m-r-5\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name && !req.uploading\" uib-tooltip=\"View attachment\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential\" href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name && !req.uploading\" ng-click=\"removeDoc(userid,req)\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> <input type=\"checkbox\" ng-checked=\"req.has_req\" ng-attr-id=\"cbId\" ng-click=\"uploadProof(req.id,req,cbId)\" ng-disabled=\"req.document_name && req.has_req\" ng-class=\"{'cb_disable':req.document_name && req.has_req}\"> <div class=\"control__indicator\"></div> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"req.progress\" ng-show=\"req.uploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> </label> </div> </div> </div> <div class=\"col-md-12\"> <p>Driving</p> <div class=\"check_box_section\"> <div class=\"control-group\" ng-repeat=\"req in reqs | filter: { category: 'Driving' }\"> <label class=\"control control--checkbox\" ng-init=\"cbId='trust_'+$index;\"> {{req.requirement}}&nbsp;<a href=\"javascript:void(0)\" ngf-select=\"saveDoc($file,req)\" ng-model=\"file\" name=\"file\" class=\"attach_btn\" ng-if=\"req.has_proof==1 && !req.document_name && req.has_req && !req.uploading\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Attach</a>&nbsp; <!-- <a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;View Attachment</a>&nbsp;\n" +
    "                                                                                    <a href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name\" ng-click=\"removeDoc(userid,req)\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Delete Attachment</a>&nbsp; --> <a class=\"action_update_credential m-r-5\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name && !req.uploading\" uib-tooltip=\"View attachment\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential\" href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name && !req.uploading\" ng-click=\"removeDoc(userid,req)\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> <input type=\"checkbox\" ng-checked=\"req.has_req\" ng-attr-id=\"cbId\" ng-click=\"uploadProof(req.id,req,cbId)\" ng-disabled=\"req.document_name && req.has_req\" ng-class=\"{'cb_disable':req.document_name && req.has_req}\"> <div class=\"control__indicator\"></div> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"req.progress\" ng-show=\"req.uploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> </label> </div> </div> </div> <div class=\"col-md-12\"> <p>Training & Education</p> <div class=\"check_box_section\"> <div class=\"control-group\" ng-repeat=\"req in reqs | filter: { category: 'Training and experience' }\"> <label class=\"control control--checkbox\" ng-init=\"cbId='trust_'+$index;\"> {{req.requirement}}&nbsp;<a href=\"javascript:void(0)\" ngf-select=\"saveDoc($file,req)\" ng-model=\"file\" name=\"file\" class=\"attach_btn\" ng-if=\"req.has_proof==1 && !req.document_name && req.has_req && !req.uploading\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Attach</a>&nbsp; <!-- <a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;View Attachment</a>&nbsp;\n" +
    "                                                                                    <a href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name\" ng-click=\"removeDoc(userid,req)\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Delete Attachment</a>&nbsp; --> <a class=\"action_update_credential m-r-5\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name && !req.uploading\" uib-tooltip=\"View attachment\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential\" href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name && !req.uploading\" ng-click=\"removeDoc(userid,req)\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> <input type=\"checkbox\" ng-checked=\"req.has_req\" ng-attr-id=\"cbId\" ng-click=\"uploadProof(req.id,req,cbId)\" ng-disabled=\"req.document_name && req.has_req\" ng-class=\"{'cb_disable':req.document_name && req.has_req}\"> <div class=\"control__indicator\"></div> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"req.progress\" ng-show=\"req.uploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> </label> </div> </div> </div> <div class=\"col-md-12\"> <p>Others</p> <div class=\"check_box_section\"> <div class=\"control-group\" ng-repeat=\"req in reqs | filter: { category: 'Others' }\"> <label class=\"control control--checkbox\" ng-init=\"cbId='trust_'+$index;\"> {{req.requirement}}&nbsp;<a href=\"javascript:void(0)\" ngf-select=\"saveDoc($file,req)\" ng-model=\"file\" name=\"file\" class=\"attach_btn\" ng-if=\"req.has_proof==1 && !req.document_name && req.has_req && !req.uploading\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Attach</a>&nbsp; <!-- <a href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;View Attachment</a>&nbsp;\n" +
    "                                                                                    <a href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name\" ng-click=\"removeDoc(userid,req)\">&nbsp;<i class=\"fa fa-paperclip\"></i>&nbsp;Delete Attachment</a>&nbsp; --> <a class=\"action_update_credential m-r-5\" href=\"/assets/documents/requirements/{{req.document_name}}\" download=\"{{req.document_name}}\" ng-if=\"req.has_req && req.document_name && !req.uploading\" uib-tooltip=\"View attachment\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential\" href=\"javascript:void(0)\" ng-if=\"req.has_req && req.document_name && !req.uploading\" ng-click=\"removeDoc(userid,req)\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> <input type=\"checkbox\" ng-checked=\"req.has_req\" ng-attr-id=\"cbId\" ng-click=\"uploadProof(req.id,req,cbId)\" ng-disabled=\"req.document_name && req.has_req\" ng-class=\"{'cb_disable':req.document_name && req.has_req}\"> <div class=\"control__indicator\"></div> <span class=\"file_upload_progress\"> <uib-progressbar max=\"100\" value=\"req.progress\" ng-show=\"req.uploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </span> </label> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 action_section_update_profile btnprofedit\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"switchTab('history')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"switchTab(0)\" class=\"btn pull-right btn-default hvr-float-shadow custom_btn_update_back\">Save & Exit</button> <button href=\"#\" class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('availability')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> <!-- <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"switchTab('history')\">Save & Next <i class=\"icon-arrow-right\"></i></button>\n" +
    "                                                      <button href=\"#\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"switchTab('availability')\"><i class=\"icon-arrow-left\"></i>Back</button>\n" +
    "                                                      <button href=\"#\" ng-click=\"switchTab(0)\" class=\"btn pull-right btn-default custom_btn_update\">Save & Exit</button> --> </div> </div> </div> </div> </div> </ng-form> </div> </div> </div> <!-- #tab4 --> <h3 class=\"tab_drawer_heading\" rel=\"tab5\" ng-click=\"switchTab('history')\">Employment History<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'history','icon-arrow-down':currentTab != 'history'}\"></i></h3> <div id=\"tab5\" class=\"tab_content\" ng-if=\"currentTab == 'history'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"avaialbilityform1\" ng-validate=\"personalOptions\" class=\"update_profile_step2\"> <div class=\"update_profile_step5 postjob_type_section1\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <h5 class=\"update_heading\">Employment History</h5> --> <!-- <h1 class=\"main_heading prefer_icon\">Employment History</h1> --> <div class=\"row\"> <div class=\"col-xs-12\"> <p class=\"add_exp_update clearfix\"><a href=\"javascript:void(0)\" ng-click=\"addExperience()\" class=\"pull-right hvr-wobble-horizontal\"><i class=\"fa fa-plus\"></i>&nbsp;Add Experience</a></p> </div> <div class=\"col-xs-12\"> <div class=\"single_job_experience\" ng-repeat=\"history in emp_history\"> <h3 class=\"section-item-title-1\">{{history.company_name}}</h3> <!-- btn group comes here --> <ul class=\"action_exp_i_css\"> <li><i class=\"icon-pencil\" uib-tooltip=\"Edit\" ng-click=\"editHistory(history)\"></i></li> <li><i class=\"icon-trash\" uib-tooltip=\"Delete\" ng-click=\"deleteHistory(history.id,$index)\"></i></li> </ul> <!-- end btn group --> <!-- <i class=\"icon-pencil pull-right edit-icon-exp\" uib-tooltip=\"Edit\" ng-click=\"editHistory(history)\"></i> --> <h4 class=\"job_designation\">{{history.title}} - <span class=\"job-date\"> <span>{{months[history.from_month-1].name}} {{history.fromyear}} - <span ng-if=\"history.to_month != 'null' && history.to_year != 'null'\">{{months[history.to_month].name}} {{history.to_year}}</span> <span ng-if=\"history.to_month == 'null' || history.to_year == 'null' || !history.to_month || !history.to_year\">Present</span> </span></span> </h4> <h4 class=\"location\" ng-if=\"history.post_code\">{{history.location}}</h4> <p class=\"mybio_p\">{{history.description}}</p> </div> <div ng-if=\"emp_history.length == 0\"> <h4 class=\"text-orange\">* Please click Add Experience to update your experience history here.</h4> </div> </div> <!-- <div class=\"col-xs-12\">\n" +
    "                                                      <h5 class=\"pull-right add_exp_update\"><a href=\"javascript:void(0)\" ng-click=\"addExperience()\"><i class=\"icon-plus\"></i>&nbsp;Add Experience</a></h5>\n" +
    "                                                      </div> --> <!-- <div  ng-repeat=\"hist in emp_history\">\n" +
    "                                                      <div class=\"col-sm-10 col-xs-10\">\n" +
    "                                                         <div class=\"experience_pane\">\n" +
    "                                                            <div class=\"media\">\n" +
    "                                                               <div class=\"media-left media-top\">\n" +
    "                                                                  <img src=\"https://s-media-cache-ak0.pinimg.com/originals/71/c3/ae/71c3aea3769929953b878adc3dd2bda6.jpg\" class=\"media-object\" style=\"width:60px\">\n" +
    "                                                               </div>\n" +
    "                                                               <div class=\"media-body\">\n" +
    "                                                                  <h4 class=\"media-heading\">{{hist.title}}</h4>\n" +
    "                                                                  <p>{{hist.company_name}}</p>\n" +
    "                                                                  <h5 class=\"year_exp\">{{hist.from_year}} - {{hist.to_year}}<span ng-if=\"!hist.to_year || hist.to_year == ''\">Present</span>\n" +
    "\n" +
    "                                                                  </h5>\n" +
    "                                                               </div>\n" +
    "                                                            </div>\n" +
    "                                                         </div>\n" +
    "                                                      </div>\n" +
    "                                                      <div class=\"col-sm-2 xs-2\">\n" +
    "                                                         <i class=\"icon-pencil pull-right edit-icon-exp\" uib-tooltip=\"Edit\" ng-click=\"editHistory(hist)\"></i>\n" +
    "                                                      </div>\n" +
    "\n" +
    "\n" +
    "                                                      </div> --> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 action_section_update_profile btnprofedit\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"switchTab('others')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"switchTab(0)\" class=\"btn pull-right btn-default hvr-float-shadow custom_btn_update_back\">Save & Exit</button> <button href=\"#\" class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('credential')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> </div> </div> </div> </div> </div> </ng-form> </div> </div> </div> <!-- #tab6 --> <h3 class=\"tab_drawer_heading\" rel=\"tab6\" ng-click=\"switchTab('others')\">Others<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'others','icon-arrow-down':currentTab != 'others'}\"></i></h3> <div id=\"tab6\" class=\"tab_content\" ng-if=\"currentTab == 'others'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"othersForm\" class=\"update_profile_step2\"> <div class=\"update_profile_step6 postjob_type_section1\"> <div class=\"row\"> <div class=\"col-md-12 check_box_section\"> <!-- <h5 class=\"update_heading\">Others</h5> --> <!-- <h1 class=\"main_heading prefer_icon\">Others</h1> --> <p class=\"sub_heading_update\">Interests & Favourites</p> <div class=\"decription_others_update editor_global_style\"> <!-- <div text-angular ta-toolbar=\"[['justifyLeft','justifyCenter'],['bold','italics','underline']]\" ng-model=\"others.interest\" name=\"interest\" ng-required=\"false\"></div> --> <p class=\"manadarory_style\">Tell us about your favourite School subjects, Kids TV characters, programs, movies, books, games Sports, Music, instruments that you are interested in.</p> <div class=\"m-t-10\"> <textarea ckeditor=\"ckoptions\" ng-model=\"others.interest\" ng-required=\"false\" name=\"interest\" height=\"100\"></textarea> </div> <div class=\"errormsg\" ng-if=\"(othersForm.$submitted || othersForm.interest.$dirty) && othersForm.interest.$invalid\"> <p ng-if=\"othersForm.interest.$error.required\">Please enter your interests & favourites</p> <p ng-if=\"othersForm.interest.$error.minlength\">Must above 200 characters</p> </div> </div> <p class=\"sub_heading_update\">Household Chores</p> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"chore in householdChores\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> {{chore.chore}} <input type=\"checkbox\" ng-click=\"toggleChores(chore.chore)\" ng-checked=\"others.household_chores.indexOf(chore.chore) > -1\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> <hr> <p class=\"sub_heading_update\">languages</p> <div class=\"row\"> <div class=\"col-sm-6\" ng-repeat=\"language in languages\"> <div class=\"control-group\"> <label class=\"control control--checkbox\"> {{language.language}} <input type=\"checkbox\" ng-checked=\"others.languages.indexOf(language.language) > -1\" ng-click=\"toggleLanguages(language.language)\"> <div class=\"control__indicator\"></div> </label> </div> </div> </div> <hr> <div class=\"row\"> <div class=\"col-sm-12\"> <p class=\"sub_heading_update\">Emergency contact&nbsp;<i class=\"icon-lock privacy_i\" uib-tooltip=\"We will keep this information private, Which will not be shown has part of your public profile.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account\" tooltip-placement=\"right\"></i></p> <p class=\"add_exp_update clearfix\"><a href=\"javascript:void(0)\" ng-click=\"openEcontact('Add')\" class=\"\"><i class=\"icon-plus\"></i>&nbsp;Add Emergency contact</a></p> <h4 ng-if=\"emp_emergencycontact.length == 0\">* Please click Add Emergency Contact to update your emergency contact detail here.</h4> </div> <div class=\"col-md-12 m-t-20\"> <div class=\"table-responsive reference_table color_orange\"> <table class=\"table table-bordered table-striped\" ng-if=\"emp_emergencycontact.length != 0\"> <tbody><tr> <th><i class=\"fa fa-user-o\"></i>&nbsp;Name</th> <th><i class=\"fa fa-envelope-o\"></i>&nbsp;Email</th> <th><i class=\"icon-phone\"></i>&nbsp;Phone Number</th> <th>&nbsp;</th> </tr> <tr ng-repeat=\"emergency in emp_emergencycontact\"> <td>{{emergency.first_name}} {{emergency.last_name}}</td> <td>{{emergency.email}}</td> <td>{{emergency.contactno}}</td> <td class=\"text-center cursor_pointer\"><i class=\"icon-pencil\" uib-tooltip=\"edit\" ng-click=\"openEcontact('Edit',emergency)\"></i> <i class=\"icon-trash\" uib-tooltip=\"Delete\" ng-click=\"deleteEmerContact(emergency.id,$index)\"></i></td> </tr> </tbody></table> </div> </div> </div> <hr> <div class=\"row\"> <div class=\"col-md-12\"> <p class=\"sub_heading_update\">Upload resume&nbsp;<i class=\"icon-lock privacy_i\" uib-tooltip=\"We will keep this information private, Which will not be shown has part of your public profile.This information will be shared  with  clients only after your booking is confirmed and payment is transferred to our secured holding account\" tooltip-placement=\"right\"></i></p> <p class=\"file_upload_progress1\"> <uib-progressbar max=\"100\" value=\"others.progress\" ng-show=\"others.resumeUploading\"><span style=\"color:white; white-space:nowrap\"></span></uib-progressbar> </p> <div ng-if=\"!others.resume\"> <span class=\"add_exp_update\"> <a href=\"javascript:void(0)\" ng-if=\"!others.resumeUploading\" ngf-select=\"saveResume($file)\" ngf-accept=\"'.doc,.docx,.pdf,.png,.jpeg,.jpg,.svg'\" ngf-pattern=\"'.doc,.docx,.pdf,.png,.jpeg,.jpg,.svg'\" ng-model=\"resumeFile\" name=\"resumefile\" class=\"\">Upload</a> </span> </div> <p class=\"download-resume-section\" ng-if=\"others.resume\"><i class=\"fa fa-file-text-o\"></i>&nbsp;{{others.resume}} <a class=\"action_update_credential m-r-5 ng-scope\" ng-if=\"others.resume\" href=\"assets/documents/resumes/{{others.resume}}\" download=\"{{others.resume}}\" uib-tooltip=\"View resume\"><i class=\"icon-eye\"></i></a> <a class=\"action_update_credential m-r-5 ng-scope\" href=\"javascript:void(0)\" ng-click=\"deleteResume(userid)\" ng-if=\"others.resume\" uib-tooltip=\"Delete attachment\"><i class=\"icon-trash\"></i></a> </p> <!-- <a class=\"btn down_load_resume_btn\" ng-if=\"others.resume\" href=\"assets/documents/resumes/{{others.resume}}\" download=\"{{others.resume}}\">Download Resume</a>\n" +
    "                                                                      <a class=\"btn del_load_resume_btn\" href=\"javascript:void(0)\" ng-click=\"deleteResume(userid)\" ng-if=\"others.resume\">Delete Resume</a> --> <p class=\"comment_line_css\">* png, pdf, doc, docx, jpeg, jpg, svg</p> </div> </div> <hr> <div class=\"row\"> <span ng-bind=\"moment(date)\"></span> <div class=\"col-md-12 col-xs-12 pull-right action_section_update_profile m-t-40\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12 action_section_update_profile btnprofedit\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"saveOthers(othersForm,'reference')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button href=\"#\" ng-click=\"saveOthers(othersForm,0)\" class=\"hvr-float-shadow btn pull-right btn-default custom_btn_update_back\">Save & Exit</button> <button href=\"#\" class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('history')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> <!-- <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"saveOthers(othersForm,'reference')\">Save & Next <i class=\"icon-arrow-right\"></i></button>\n" +
    "                                                      <button class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"switchTab('history')\"><i class=\"icon-arrow-left\"></i>Back</button>\n" +
    "                                                      <button ng-click=\"saveOthers(othersForm,0)\" class=\"btn pull-right btn-default custom_btn_update\">Save & Exit</button> --> </div> </div> </div> </div> </div> </ng-form> </div> </div> </div> <!-- #tab3 --> <h3 class=\"tab_drawer_heading\" rel=\"tab7\" ng-click=\"switchTab('reference')\">References<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'reference','icon-arrow-down':currentTab != 'reference'}\"></i></h3> <div id=\"tab7\" class=\"tab_content\" ng-if=\"currentTab == 'reference'\"> <div class=\"row\"> <div class=\"col-md-12\"> <ng-form name=\"avaialbilityform1\" ng-validate=\"personalOptions\" class=\"update_profile_step2\" ng-if=\"currentTab == 'reference'\"> <div class=\"update_profile_step7 postjob_type_section1\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <h1 class=\"main_heading prefer_icon\">References</h1> --> <div class=\"row\"> <div class=\"col-xs-12\"> <p class=\"add_exp_update clearfix\"><a href=\"javascript:void(0)\" ng-click=\"openReference('add')\" class=\"pull-right\"><i class=\"icon-plus\"></i>&nbsp;Add Reference</a></p> <h4 ng-if=\"references.length == 0\" class=\"text-orange\">* Please click Add Reference to update your reference detail here.</h4> </div> <div class=\"col-sm-12 m-t-20\"> <div class=\"table-responsive reference_table color_orange\"> <table class=\"table table-bordered table-striped\" ng-if=\"references.length != 0\"> <tbody><tr> <th><i class=\"fa fa-user-o\"></i>&nbsp;Name</th> <th><i class=\"fa fa-envelope-o\"></i>&nbsp;Email</th> <th><i class=\"icon-phone\"></i>&nbsp;Phone Number</th> <th>&nbsp;</th> </tr> <!-- ngRepeat: ref in references --><tr ng-repeat=\"ref in references\"> <td>{{ref.first_name}} {{ref.last_name}}</td> <td>{{ref.email}}</td> <td>{{ref.contactno}}</td> <td class=\"text-center cursor_pointer\"><i class=\"icon-pencil\" uib-tooltip=\"edit\" ng-click=\"openReference('edit',ref)\"></i> <i class=\"icon-trash\" uib-tooltip=\"Delete\" ng-click=\"deleteReference(ref.id,$index)\"></i></td> </tr><!-- end ngRepeat: ref in references --> </tbody></table> </div> <!-- <div ng-repeat=\"ref in references\">\n" +
    "                                                                            <div class=\"reference_section\">\n" +
    "                                                                                <p><i class=\"icon-user\"></i>&nbsp;&nbsp;{{ref.first_name}} {{ref.last_name}} <i class=\"icon-pencil pull-right \" uib-tooltip=\"edit\" ng-click=\"openReference('edit',ref)\"></i></p>\n" +
    "                                                                                <p><i class=\"icon-envelope\"></i>&nbsp;&nbsp;{{ref.email}}</p>\n" +
    "                                                                                <p><i class=\"icon-phone\"></i>&nbsp;&nbsp;{{ref.contactno}}</p>\n" +
    "                                                                            </div>\n" +
    "                                                                            <hr>\n" +
    "                                                                        </div> --> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 col-xs-12 action_section_update_profile btnprofedit\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12\"> <button ng-click=\"switchTab(0)\" class=\"btn pull-right btn-default hvr-float-shadow custom_btn_update_back\">Save & Exit</button> <button class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('others')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> </div> </div> </ng-form> </div> </div> </div> <h3 class=\"tab_drawer_heading hide\" rel=\"tab8\" ng-click=\"switchTab('bankinfo')\">Bank Information<i class=\"pull-right\" ng-class=\"{'icon-arrow-up':currentTab == 'bankinfo','icon-arrow-down':currentTab != 'bankinfo'}\"></i></h3> <div id=\"tab8\" class=\"tab_content hide\" ng-if=\"currentTab == 'bankinfo'\"> <div class=\"row\"> <ng-form name=\"bankForm\"> <div class=\"update_profile_step8 postjob_type_section1\"> <div class=\"col-md-12\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Bank Name</h4> <input type=\"text\" name=\"bank_name\" ng-model=\"bankDetails.bank_name\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(bankForm.$submitted || bankForm.bank_name.$dirty) && bankForm.bank_name.$invalid\"> <p ng-if=\"bankForm.bank_name.$error.required\">Please enter bank name</p> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">BSB Number </h4> <input type=\"text\" name=\"bsb_no\" ng-model=\"bankDetails.bsb_no\" ng-required=\"true\" ng-pattern=\"/^[0-9]*$\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(bankForm.$submitted || bankForm.bsb_no.$dirty) && bankForm.bsb_no.$invalid\"> <p ng-if=\"bankForm.bsb_no.$error.required\">Please BSB number</p> <p ng-if=\"bankForm.bsb_no.$error.pattern\">BSB number must be digits</p> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"group group1\"> <h4 class=\"m-b-0\">Account Number</h4> <input type=\"text\" name=\"account_no\" ng-model=\"bankDetails.account_no\" ng-required=\"true\" ng-pattern=\"/^[0-9]*$\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(bankForm.$submitted || bankForm.account_no.$dirty) && bankForm.account_no.$invalid\"> <p ng-if=\"bankForm.account_no.$error.required\">Please enter account number</p> <p ng-if=\"bankForm.account_no.$error.pattern\">Account number must be digits</p> </div> </div> </div> </div> <div class=\"col-md-12 col-xs-12 pull-right action_section_update_profile\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-12 action_section_update_profile btnprofedit\"> <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update hvr-float-shadow\" ng-click=\"saveBankdetails(bankForm,'reference')\">Save & Next &nbsp;<i class=\"icon-arrow-right-circle\"></i></button> <button ng-click=\"saveBankdetails(bankForm,0)\" class=\"hvr-float-shadow btn pull-right btn-default custom_btn_update_back\">Save & Exit</button> <button class=\"btn btn-default custom_btn_update_back hvr-float-shadow pull-left\" ng-click=\"switchTab('history')\"><i class=\"icon-arrow-left-circle\"></i>&nbsp;Back</button> </div> </div> <!-- <button type=\"button\" class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"saveOthers(othersForm,'reference')\">Save & Next <i class=\"icon-arrow-right\"></i></button>\n" +
    "                                <button class=\"btn pull-right btn-default custom_btn_update\" ng-click=\"switchTab('history')\"><i class=\"icon-arrow-left\"></i>Back</button>\n" +
    "                                <button ng-click=\"saveOthers(othersForm,0)\" class=\"btn pull-right btn-default custom_btn_update\">Save & Exit</button> --> </div> </ng-form> </div> </div> </div> <!-- .tab_container --> </div> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"profile_completion_update_profile_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"profile_picture_section text-center\"> <!-- <img src=\"http://freelance.thansettakij.com/images/anonymous.gif\" alt=\"\" class=\"img-responsive\" ng-if=\"!file\"> --> <div ng-if=\"!croppingImage && personal.profpic\"> <img ngf-src=\"'/images/profilepic/'+personal.profpic\" alt=\"\" class=\"img-responsive\"> </div> <div ng-if=\"croppingImage\"> <img ngf-thumbnail=\"myCroppedImage\" alt=\"\" class=\"img-responsive\"> </div> <div> <img ngf-thumbnail=\"'/images/no_image_1.png'\" alt=\"\" ng-if=\"!personal.profpic && !myCroppedImage\" class=\"img-responsive\"> </div> <div class=\"upload_btn\"> <p class=\"update_image_icon text-center\"> <!-- <i class=\"fa fa-upload profile_edit_icon \" ></i>\n" +
    "                             Upload picture --> <input type=\"file\" id=\"fileInput\" accept=\"image/*\" class=\"display_none\"> <button class=\"profile_pic_update_btn\" onclick=\"document.getElementById('fileInput').click()\"><i class=\"\"></i>Update Photo</button> </p> </div> <!-- <div class=\"cropArea\">\n" +
    "<img-crop image=\"myImage\" result-image=\"myCroppedImage\" area-min-size=\"200\" area-type=\"square\" result-image-size=\"150\" on-load-done=\"testFunc()\"></img-crop>\n" +
    "</div> --> </div> <div class=\"col-sm-12 completion_section_profile complete_progress\"> <h5>Profile completion {{profileCompleteness}}% </h5> <div class=\"progress\"> <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"{{profileCompleteness}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" ng-style=\"{ 'width': (profileCompleteness+'%')}\"> </div> </div> </div> </div> </div> </div> <!-- <div class=\"ad_section col-sm-hidden\">\n" +
    "                        <img src=\"styles/images/right_side_banner.png\" alt=\"\" width=\"100%\">\n" +
    "                    </div> --> </div> </div> </div> </section> </div> <div footer></div> <script>// tabbed content\n" +
    "// http://www.entheosweb.com/tutorials/css/tabs.asp\n" +
    "// $(\".tab_content\").hide();\n" +
    "// $(\".tab_content:first\").show();\n" +
    "\n" +
    "/* if in tab mode */\n" +
    "//  $(\"ul.tabs li\").click(function() {\n" +
    "\n" +
    "//    $(\".tab_content\").hide();\n" +
    "//    var activeTab = $(this).attr(\"rel\");\n" +
    "//    $(\"#\"+activeTab).fadeIn();\n" +
    "\n" +
    "//    $(\"ul.tabs li\").removeClass(\"active\");\n" +
    "//    $(this).addClass(\"active\");\n" +
    "\n" +
    "// $(\".tab_drawer_heading\").removeClass(\"d_active\");\n" +
    "// $(\".tab_drawer_heading[rel^='\"+activeTab+\"']\").addClass(\"d_active\");\n" +
    "\n" +
    "//  /*$(\".tabs\").css(\"margin-top\", function(){\n" +
    "//     return ($(\".tab_container\").outerHeight() - $(\".tabs\").outerHeight() ) / 2;\n" +
    "//  });*/\n" +
    "//  });\n" +
    "$(\".tab_container\").css(\"min-height\", function() {\n" +
    "    return $(\".tabs\").outerHeight() + 50;\n" +
    "});\n" +
    "/* if in drawer mode */\n" +
    "// $(\".tab_drawer_heading\").click(function() {\n" +
    "\n" +
    "//      $(\".tab_content\").hide();\n" +
    "//      var d_activeTab = $(this).attr(\"rel\");\n" +
    "//      $(\"#\"+d_activeTab).fadeIn();\n" +
    "\n" +
    "//   $(\".tab_drawer_heading\").removeClass(\"d_active\");\n" +
    "//      $(this).addClass(\"d_active\");\n" +
    "\n" +
    "//   $(\"ul.tabs li\").removeClass(\"active\");\n" +
    "//   $(\"ul.tabs li[rel^='\"+d_activeTab+\"']\").addClass(\"active\");\n" +
    "//    });\n" +
    "\n" +
    "\n" +
    "/* Extra class \"tab_last\"\n" +
    "   to add border to bottom side\n" +
    "   of last tab\n" +
    "$('ul.tabs li').last().addClass(\"tab_last\");*/\n" +
    "$('.dropdown-menu a').click(function() {\n" +
    "    $('#selected').text($(this).text());\n" +
    "});</script>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/plugins/wsc/dialogs/ciframe.html',
    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><!--\n" +
    "Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.html or http://ckeditor.com/license\n" +
    "--> <html> <head> <title></title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> <script type=\"text/javascript\">function gup( name )\n" +
    "{\n" +
    "\tname = name.replace( /[\\[]/, '\\\\\\[' ).replace( /[\\]]/, '\\\\\\]' ) ;\n" +
    "\tvar regexS = '[\\\\?&]' + name + '=([^&#]*)' ;\n" +
    "\tvar regex = new RegExp( regexS ) ;\n" +
    "\tvar results = regex.exec( window.location.href ) ;\n" +
    "\n" +
    "\tif ( results )\n" +
    "\t\treturn results[ 1 ] ;\n" +
    "\telse\n" +
    "\t\treturn '' ;\n" +
    "}\n" +
    "\n" +
    "var interval;\n" +
    "\n" +
    "function sendData2Master()\n" +
    "{\n" +
    "\tvar destination = window.parent.parent ;\n" +
    "\ttry\n" +
    "\t{\n" +
    "\t\tif ( destination.XDTMaster )\n" +
    "\t\t{\n" +
    "\t\t\tvar t = destination.XDTMaster.read( [ gup( 'cmd' ), gup( 'data' ) ] ) ;\n" +
    "\t\t\twindow.clearInterval( interval ) ;\n" +
    "\t\t}\n" +
    "\t}\n" +
    "\tcatch (e) {}\n" +
    "}\n" +
    "\n" +
    "function OnMessage (event) {\n" +
    "\t        var message = event.data;\n" +
    "\t        var destination = window.parent.parent;\n" +
    "\t        destination.XDTMaster.read( [ 'end', message, 'fpm' ] ) ;\n" +
    "}\n" +
    "\n" +
    "function listenPostMessage() {\n" +
    "    if (window.addEventListener) { // all browsers except IE before version 9\n" +
    "            window.addEventListener (\"message\", OnMessage, false);\n" +
    "    }else {\n" +
    "            if (window.attachEvent) { // IE before version 9\n" +
    "                        window.attachEvent(\"onmessage\", OnMessage);\n" +
    "                }\n" +
    "        }\n" +
    "}\n" +
    "\n" +
    "function onLoad()\n" +
    "{\n" +
    "\tinterval = window.setInterval( sendData2Master, 100 );\n" +
    "\tlistenPostMessage();\n" +
    "}</script> </head> <body onload=\"onLoad()\"><p></p></body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/plugins/wsc/dialogs/tmpFrameset.html',
    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\"><!--\n" +
    "Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.html or http://ckeditor.com/license\n" +
    "--> <html> <head> <title></title> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> <script type=\"text/javascript\">function doLoadScript( url )\n" +
    "{\n" +
    "\tif ( !url )\n" +
    "\t\treturn false ;\n" +
    "\n" +
    "\tvar s = document.createElement( \"script\" ) ;\n" +
    "\ts.type = \"text/javascript\" ;\n" +
    "\ts.src = url ;\n" +
    "\tdocument.getElementsByTagName( \"head\" )[ 0 ].appendChild( s ) ;\n" +
    "\n" +
    "\treturn true ;\n" +
    "}\n" +
    "\n" +
    "var opener;\n" +
    "function tryLoad()\n" +
    "{\n" +
    "\topener = window.parent;\n" +
    "\n" +
    "\t// get access to global parameters\n" +
    "\tvar oParams = window.opener.oldFramesetPageParams;\n" +
    "\n" +
    "\t// make frameset rows string prepare\n" +
    "\tvar sFramesetRows = ( parseInt( oParams.firstframeh, 10 ) || '30') + \",*,\" + ( parseInt( oParams.thirdframeh, 10 ) || '150' ) + ',0' ;\n" +
    "\tdocument.getElementById( 'itFrameset' ).rows = sFramesetRows ;\n" +
    "\n" +
    "\t// dynamic including init frames and crossdomain transport code\n" +
    "\t// from config sproxy_js_frameset url\n" +
    "\tvar addScriptUrl = oParams.sproxy_js_frameset ;\n" +
    "\tdoLoadScript( addScriptUrl ) ;\n" +
    "}</script> </head> <frameset id=\"itFrameset\" onload=\"tryLoad()\" border=\"0\" rows=\"30,*,*,0\"> <frame scrolling=\"no\" framespacing=\"0\" frameborder=\"0\" noresize marginheight=\"0\" marginwidth=\"2\" src=\"\" name=\"navbar\"> <frame scrolling=\"auto\" framespacing=\"0\" frameborder=\"0\" noresize marginheight=\"0\" marginwidth=\"0\" src=\"\" name=\"mid\"> <frame scrolling=\"no\" framespacing=\"0\" frameborder=\"0\" noresize marginheight=\"1\" marginwidth=\"1\" src=\"\" name=\"bot\"> <frame scrolling=\"no\" framespacing=\"0\" frameborder=\"0\" noresize marginheight=\"1\" marginwidth=\"1\" src=\"\" name=\"spellsuggestall\"> </frameset> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/index.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>CKEditor Sample</title> <script src=\"../ckeditor.js\"></script> <script src=\"js/sample.js\"></script> <link rel=\"stylesheet\" href=\"css/samples.css\"> <link rel=\"stylesheet\" href=\"toolbarconfigurator/lib/codemirror/neo.css\"> </head> <body id=\"main\"> <nav class=\"navigation-a\"> <div class=\"grid-container\"> <ul class=\"navigation-a-left grid-width-70\"> <li><a href=\"https://ckeditor.com/ckeditor-4/\">Project Homepage</a></li> <li><a href=\"https://github.com/ckeditor/ckeditor-dev/issues\">I found a bug</a></li> <li><a href=\"http://github.com/ckeditor/ckeditor-dev\" class=\"icon-pos-right icon-navigation-a-github\">Fork CKEditor on GitHub</a></li> </ul> <ul class=\"navigation-a-right grid-width-30\"> <li><a href=\"https://ckeditor.com/blog/\">CKEditor Blog</a></li> </ul> </div> </nav> <header class=\"header-a\"> <div class=\"grid-container\"> <h1 class=\"header-a-logo grid-width-30\"> <a href=\"index.html\"><img src=\"img/logo.png\" alt=\"CKEditor Sample\"></a> </h1> <nav class=\"navigation-b grid-width-70\"> <ul> <li><a href=\"index.html\" class=\"button-a button-a-background\">Start</a></li> <li><a href=\"toolbarconfigurator/index.html\" class=\"button-a\">Toolbar configurator <span class=\"balloon-a balloon-a-nw\">Edit your toolbar now!</span></a></li> </ul> </nav> </div> </header> <main> <div class=\"adjoined-top\"> <div class=\"grid-container\"> <div class=\"content grid-width-100\"> <h1>Congratulations!</h1> <p> If you can see CKEditor below, it means that the installation succeeded. You can now try out your new editor version, see its features, and when you are ready to move on, check some of the <a href=\"#sample-customize\">most useful resources</a> recommended below. </p> </div> </div> </div> <div class=\"adjoined-bottom\"> <div class=\"grid-container\"> <div class=\"grid-width-100\"> <div id=\"editor\"> <h1>Hello world!</h1> <p>I'm an instance of <a href=\"https://ckeditor.com\">CKEditor</a>.</p> </div> </div> </div> </div> <div class=\"grid-container\"> <div class=\"content grid-width-100\"> <section id=\"sample-customize\"> <h2>Customize Your Editor</h2> <p>Modular build and <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_configuration\">numerous configuration options</a> give you nearly endless possibilities to customize CKEditor. Replace the content of your <code><a href=\"../config.js\">config.js</a></code> file with the following code and refresh this page (<strong>remember to clear the browser cache</strong>)!</p> <pre class=\"cm-s-neo CodeMirror\"><code><span style=\"padding-right: 0.1px\"><span class=\"cm-variable\">CKEDITOR</span>.<span class=\"cm-property\">editorConfig</span> <span class=\"cm-operator\">=</span> <span class=\"cm-keyword\">function</span>( <span class=\"cm-def\">config</span> ) {</span>\n" +
    "<span style=\"padding-right: 0.1px\"><span class=\"cm-tab\">\t</span><span class=\"cm-variable-2\">config</span>.<span class=\"cm-property\">language</span> <span class=\"cm-operator\">=</span> <span class=\"cm-string\">'es'</span>;</span>\n" +
    "<span style=\"padding-right: 0.1px\"><span class=\"cm-tab\">\t</span><span class=\"cm-variable-2\">config</span>.<span class=\"cm-property\">uiColor</span> <span class=\"cm-operator\">=</span> <span class=\"cm-string\">'#F7B42C'</span>;</span>\n" +
    "<span style=\"padding-right: 0.1px\"><span class=\"cm-tab\">\t</span><span class=\"cm-variable-2\">config</span>.<span class=\"cm-property\">height</span> <span class=\"cm-operator\">=</span> <span class=\"cm-number\">300</span>;</span>\n" +
    "<span style=\"padding-right: 0.1px\"><span class=\"cm-tab\">\t</span><span class=\"cm-variable-2\">config</span>.<span class=\"cm-property\">toolbarCanCollapse</span> <span class=\"cm-operator\">=</span> <span class=\"cm-atom\">true</span>;</span>\n" +
    "<span style=\"padding-right: 0.1px\">};</span></code></pre> </section> <section> <h2>Toolbar Configuration</h2> <p>If you want to reorder toolbar buttons or remove some of them, check <a href=\"toolbarconfigurator/index.html\">this handy tool</a>!</p> </section> <section> <h2>More Samples!</h2> <p>Visit the <a href=\"https://sdk.ckeditor.com\">CKEditor SDK</a> for a huge collection of samples showcasing editor features, with source code readily available to copy and use in your own implementation.</p> </section> <section> <h2>Developer's Guide</h2> <p>The most important resource for all developers working with CKEditor, integrating it with their websites and applications, and customizing to their needs. You can start from here:</p> <ul> <li><a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_installation\">Getting Started</a> &ndash; Explains most crucial editor concepts and practices as well as the installation process and integration with your website.</li> <li><a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_advanced_installation\">Advanced Installation Concepts</a> &ndash; Describes how to upgrade, install additional components (plugins, skins), or create a custom build.</li> </ul> <p>When you have the basics sorted out, feel free to browse some more advanced sections like:</p> <ul> <li><a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_features\">Functionality Overview</a> &ndash; Descriptions and samples of various editor features.</li> <li><a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/plugin_sdk_intro\">Plugin SDK</a>, <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/widget_sdk_intro\">Widget SDK</a>, and <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/skin_sdk_intro\">Skin SDK</a> &ndash; Useful when you want to create your own editor components.</li> </ul> </section> <section> <h2>CKEditor JavaScript API</h2> <p>CKEditor boasts a rich <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api\">JavaScript API</a> that you can use to adjust the editor to your needs and integrate it with your website or application.</p> </section> </div> </div> </main> <footer class=\"footer-a grid-container\"> <div class=\"grid-container\"> <p class=\"grid-width-100\"> CKEditor &ndash; The text editor for the Internet &ndash; <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p class=\"grid-width-100\" id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> &ndash; Frederico Knabben. All rights reserved. </p> </div> </footer> <script>initSample();</script> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/ajax.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Ajax &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> <script>var editor, html = '';\n" +
    "\n" +
    "\t\tfunction createEditor() {\n" +
    "\t\t\tif ( editor )\n" +
    "\t\t\t\treturn;\n" +
    "\n" +
    "\t\t\t// Create a new editor inside the <div id=\"editor\">, setting its value to html\n" +
    "\t\t\tvar config = {};\n" +
    "\t\t\teditor = CKEDITOR.appendTo( 'editor', config, html );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tfunction removeEditor() {\n" +
    "\t\t\tif ( !editor )\n" +
    "\t\t\t\treturn;\n" +
    "\n" +
    "\t\t\t// Retrieve the editor contents. In an Ajax application, this data would be\n" +
    "\t\t\t// sent to the server or used in any other way.\n" +
    "\t\t\tdocument.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();\n" +
    "\t\t\tdocument.getElementById( 'contents' ).style.display = '';\n" +
    "\n" +
    "\t\t\t// Destroy the editor.\n" +
    "\t\t\teditor.destroy();\n" +
    "\t\t\teditor = null;\n" +
    "\t\t}</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Create and Destroy Editor Instances for Ajax Applications </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/saveajax.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to create and destroy CKEditor instances on the fly. After the removal of CKEditor the content created inside the editing area will be displayed in a <code>&lt;div&gt;</code> element. </p> <p> For details of how to create this setup check the source code of this sample page for JavaScript code responsible for the creation and destruction of a CKEditor instance. </p> </div> <p>Click the buttons to create and remove a CKEditor instance.</p> <p> <input onclick=\"createEditor()\" type=\"button\" value=\"Create Editor\"> <input onclick=\"removeEditor()\" type=\"button\" value=\"Remove Editor\"> </p> <!-- This div will hold the editor. --> <div id=\"editor\"> </div> <div id=\"contents\" style=\"display: none\"> <p> Edited Contents: </p> <!-- This div will be used to display the editor contents. --> <div id=\"editorcontents\"> </div> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/api.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>API Usage &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <script>// The instanceReady event is fired, when an instance of CKEditor has finished\n" +
    "// its initialization.\n" +
    "CKEDITOR.on( 'instanceReady', function( ev ) {\n" +
    "\t// Show the editor name and description in the browser status bar.\n" +
    "\tdocument.getElementById( 'eMessage' ).innerHTML = 'Instance <code>' + ev.editor.name + '<\\/code> loaded.';\n" +
    "\n" +
    "\t// Show this sample buttons.\n" +
    "\tdocument.getElementById( 'eButtons' ).style.display = 'block';\n" +
    "});\n" +
    "\n" +
    "function InsertHTML() {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\tvar value = document.getElementById( 'htmlArea' ).value;\n" +
    "\n" +
    "\t// Check the active editing mode.\n" +
    "\tif ( editor.mode == 'wysiwyg' )\n" +
    "\t{\n" +
    "\t\t// Insert HTML code.\n" +
    "\t\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-insertHtml\n" +
    "\t\teditor.insertHtml( value );\n" +
    "\t}\n" +
    "\telse\n" +
    "\t\talert( 'You must be in WYSIWYG mode!' );\n" +
    "}\n" +
    "\n" +
    "function InsertText() {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\tvar value = document.getElementById( 'txtArea' ).value;\n" +
    "\n" +
    "\t// Check the active editing mode.\n" +
    "\tif ( editor.mode == 'wysiwyg' )\n" +
    "\t{\n" +
    "\t\t// Insert as plain text.\n" +
    "\t\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-insertText\n" +
    "\t\teditor.insertText( value );\n" +
    "\t}\n" +
    "\telse\n" +
    "\t\talert( 'You must be in WYSIWYG mode!' );\n" +
    "}\n" +
    "\n" +
    "function SetContents() {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\tvar value = document.getElementById( 'htmlArea' ).value;\n" +
    "\n" +
    "\t// Set editor contents (replace current contents).\n" +
    "\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-setData\n" +
    "\teditor.setData( value );\n" +
    "}\n" +
    "\n" +
    "function GetContents() {\n" +
    "\t// Get the editor instance that you want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\n" +
    "\t// Get editor contents\n" +
    "\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-getData\n" +
    "\talert( editor.getData() );\n" +
    "}\n" +
    "\n" +
    "function ExecuteCommand( commandName ) {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\n" +
    "\t// Check the active editing mode.\n" +
    "\tif ( editor.mode == 'wysiwyg' )\n" +
    "\t{\n" +
    "\t\t// Execute the command.\n" +
    "\t\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-execCommand\n" +
    "\t\teditor.execCommand( commandName );\n" +
    "\t}\n" +
    "\telse\n" +
    "\t\talert( 'You must be in WYSIWYG mode!' );\n" +
    "}\n" +
    "\n" +
    "function CheckDirty() {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\t// Checks whether the current editor contents present changes when compared\n" +
    "\t// to the contents loaded into the editor at startup\n" +
    "\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-checkDirty\n" +
    "\talert( editor.checkDirty() );\n" +
    "}\n" +
    "\n" +
    "function ResetDirty() {\n" +
    "\t// Get the editor instance that we want to interact with.\n" +
    "\tvar editor = CKEDITOR.instances.editor1;\n" +
    "\t// Resets the \"dirty state\" of the editor (see CheckDirty())\n" +
    "\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-resetDirty\n" +
    "\teditor.resetDirty();\n" +
    "\talert( 'The \"IsDirty\" status has been reset' );\n" +
    "}\n" +
    "\n" +
    "function Focus() {\n" +
    "\tCKEDITOR.instances.editor1.focus();\n" +
    "}\n" +
    "\n" +
    "function onFocus() {\n" +
    "\tdocument.getElementById( 'eMessage' ).innerHTML = '<b>' + this.name + ' is focused </b>';\n" +
    "}\n" +
    "\n" +
    "function onBlur() {\n" +
    "\tdocument.getElementById( 'eMessage' ).innerHTML = this.name + ' lost focus';\n" +
    "}</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Using CKEditor JavaScript API </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/api.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to use the <a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor\">CKEditor JavaScript API</a> to interact with the editor at runtime. </p> <p> For details on how to create this setup check the source code of this sample page. </p> </div> <!-- This <div> holds alert messages to be display in the sample page. --> <div id=\"alerts\"> <noscript><p> <strong>CKEditor requires JavaScript to run</strong>. In a browser with no JavaScript support, like yours, you should still see the contents (HTML data) and you should be able to edit it normally, without a rich editor interface. </p></noscript> </div> <form action=\"../../../samples/sample_posteddata.php\" method=\"post\"> <textarea cols=\"100\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>// Replace the <textarea id=\"editor1\"> with an CKEditor instance.\n" +
    "\t\t\tCKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\ton: {\n" +
    "\t\t\t\t\tfocus: onFocus,\n" +
    "\t\t\t\t\tblur: onBlur,\n" +
    "\n" +
    "\t\t\t\t\t// Check for availability of corresponding plugins.\n" +
    "\t\t\t\t\tpluginsLoaded: function( evt ) {\n" +
    "\t\t\t\t\t\tvar doc = CKEDITOR.document, ed = evt.editor;\n" +
    "\t\t\t\t\t\tif ( !ed.getCommand( 'bold' ) )\n" +
    "\t\t\t\t\t\t\tdoc.getById( 'exec-bold' ).hide();\n" +
    "\t\t\t\t\t\tif ( !ed.getCommand( 'link' ) )\n" +
    "\t\t\t\t\t\t\tdoc.getById( 'exec-link' ).hide();\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t}\n" +
    "\t\t\t});</script> <p id=\"eMessage\"> </p> <div id=\"eButtons\" style=\"display: none\"> <input id=\"exec-bold\" onclick=\"ExecuteCommand('bold')\" type=\"button\" value=\"Execute &quot;bold&quot; Command\"> <input id=\"exec-link\" onclick=\"ExecuteCommand('link')\" type=\"button\" value=\"Execute &quot;link&quot; Command\"> <input onclick=\"Focus()\" type=\"button\" value=\"Focus\"> <br><br> <input onclick=\"InsertHTML()\" type=\"button\" value=\"Insert HTML\"> <input onclick=\"SetContents()\" type=\"button\" value=\"Set Editor Contents\"> <input onclick=\"GetContents()\" type=\"button\" value=\"Get Editor Contents (HTML)\"> <br> <textarea cols=\"100\" id=\"htmlArea\" rows=\"3\">&lt;h2&gt;Test&lt;/h2&gt;&lt;p&gt;This is some &lt;a href=\"/Test1.html\"&gt;sample&lt;/a&gt; HTML code.&lt;/p&gt;</textarea> <br> <br> <input onclick=\"InsertText()\" type=\"button\" value=\"Insert Text\"> <br> <textarea cols=\"100\" id=\"txtArea\" rows=\"3\">   First line with some leading whitespaces.\n" +
    "\n" +
    "Second line of text preceded by two line breaks.</textarea> <br> <br> <input onclick=\"CheckDirty()\" type=\"button\" value=\"checkDirty()\"> <input onclick=\"ResetDirty()\" type=\"button\" value=\"resetDirty()\"> </div> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/appendto.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Append To Page Element Using JavaScript Code &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Append To Page Element Using JavaScript Code </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div id=\"section1\"> <div class=\"description\"> <p> The <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR-method-appendTo\">CKEDITOR.appendTo()</a></code> method serves to to place editors inside existing DOM elements. Unlike <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR-method-replace\">CKEDITOR.replace()</a></code>, a target container to be replaced is no longer necessary. A new editor instance is inserted directly wherever it is desired. </p> <pre class=\"samples\">CKEDITOR.appendTo( '<em>container_id</em>',\n" +
    "\t{ /* Configuration options to be used. */ }\n" +
    "\t'Editor content to be used.'\n" +
    ");</pre> </div> <script>// This call can be placed at any point after the\n" +
    "\t\t\t// DOM element to append CKEditor to or inside the <head><script>\n" +
    "\t\t\t// in a window.onload event handler.\n" +
    "\n" +
    "\t\t\t// Append a CKEditor instance using the default configuration and the\n" +
    "\t\t\t// provided content to the <div> element of ID \"section1\".\n" +
    "\t\t\tCKEDITOR.appendTo( 'section1',\n" +
    "\t\t\t\tnull,\n" +
    "\t\t\t\t'<p>This is some <strong>sample text</strong>. You are using <a href=\"https://ckeditor.com/\">CKEditor</a>.</p>'\n" +
    "\t\t\t);</script> </div> <br> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/datafiltering.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Data Filtering &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> <script>// Remove advanced tabs for all editors.\n" +
    "\t\tCKEDITOR.config.removeDialogTabs = 'image:advanced;link:advanced;flash:advanced;creatediv:advanced;editdiv:advanced';</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Data Filtering and Features Activation </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/acf.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample page demonstrates the idea of Advanced Content Filter (<abbr title=\"Advanced Content Filter\">ACF</abbr>), a sophisticated tool that takes control over what kind of data is accepted by the editor and what kind of output is produced. </p> <h2>When and what is being filtered?</h2> <p> <abbr title=\"Advanced Content Filter\">ACF</abbr> controls <strong>every single source of data</strong> that comes to the editor. It process both HTML that is inserted manually (i.e. pasted by the user) and programmatically like: </p> <pre class=\"samples\">\n" +
    "editor.setData( '&lt;p&gt;Hello world!&lt;/p&gt;' );\n" +
    "</pre> <p> <abbr title=\"Advanced Content Filter\">ACF</abbr> discards invalid, useless HTML tags and attributes so the editor remains \"clean\" during runtime. <abbr title=\"Advanced Content Filter\">ACF</abbr> behaviour can be configured and adjusted for a particular case to prevent the output HTML (i.e. in CMS systems) from being polluted. This kind of filtering is a first, client-side line of defense against \"<a href=\"http://en.wikipedia.org/wiki/Tag_soup\">tag soups</a>\", the tool that precisely restricts which tags, attributes and styles are allowed (desired). When properly configured, <abbr title=\"Advanced Content Filter\">ACF</abbr> is an easy and fast way to produce a high-quality, intentionally filtered HTML. </p> <h3>How to configure or disable ACF?</h3> <p> Advanced Content Filter is enabled by default, working in \"automatic mode\", yet it provides a set of easy rules that allow adjusting filtering rules and disabling the entire feature when necessary. The config property responsible for this feature is <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\">config.allowedContent</a></code>. </p> <p> By \"automatic mode\" is meant that loaded plugins decide which kind of content is enabled and which is not. For example, if the link plugin is loaded it implies that <code>&lt;a&gt;</code> tag is automatically allowed. Each plugin is given a set of predefined <abbr title=\"Advanced Content Filter\">ACF</abbr> rules that control the editor until <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\"> config.allowedContent</a></code> is defined manually. </p> <p> Let's assume our intention is to restrict the editor to accept (produce) <strong>paragraphs only: no attributes, no styles, no other tags</strong>. With <abbr title=\"Advanced Content Filter\">ACF</abbr> this is very simple. Basically set <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\"> config.allowedContent</a></code> to <code>'p'</code>: </p> <pre class=\"samples\">\n" +
    "var editor = CKEDITOR.replace( <em>textarea_id</em>, {\n" +
    "\t<strong>allowedContent: 'p'</strong>\n" +
    "} );\n" +
    "</pre> <p> Now try to play with allowed content: </p> <pre class=\"samples\">\n" +
    "// Trying to insert disallowed tag and attribute.\n" +
    "editor.setData( '&lt;p <strong>style=\"color: red\"</strong>&gt;Hello <strong>&lt;em&gt;world&lt;/em&gt;</strong>!&lt;/p&gt;' );\n" +
    "alert( editor.getData() );\n" +
    "\n" +
    "// Filtered data is returned.\n" +
    "\"&lt;p&gt;Hello world!&lt;/p&gt;\"\n" +
    "</pre> <p> What happened? Since <code>config.allowedContent: 'p'</code> is set the editor assumes that only plain <code>&lt;p&gt;</code> are accepted. Nothing more. This is why <code>style</code> attribute and <code>&lt;em&gt;</code> tag are gone. The same filtering would happen if we pasted disallowed HTML into this editor. </p> <p> This is just a small sample of what <abbr title=\"Advanced Content Filter\">ACF</abbr> can do. To know more, please refer to the sample section below and <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_advanced_content_filter\">the official Advanced Content Filter guide</a>. </p> <p> You may, of course, want CKEditor to avoid filtering of any kind. To get rid of <abbr title=\"Advanced Content Filter\">ACF</abbr>, basically set <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\"> config.allowedContent</a></code> to <code>true</code> like this: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( <em>textarea_id</em>, {\n" +
    "\t<strong>allowedContent: true</strong>\n" +
    "} );\n" +
    "</pre> <h2>Beyond data flow: Features activation</h2> <p> <abbr title=\"Advanced Content Filter\">ACF</abbr> is far more than <abbr title=\"Input/Output\">I/O</abbr> control: the entire <abbr title=\"User Interface\">UI</abbr> of the editor is adjusted to what filters restrict. For example: if <code>&lt;a&gt;</code> tag is <strong>disallowed</strong> by <abbr title=\"Advanced Content Filter\">ACF</abbr>, then accordingly <code>link</code> command, toolbar button and link dialog are also disabled. Editor is smart: it knows which features must be removed from the interface to match filtering rules. </p> <p> CKEditor can be far more specific. If <code>&lt;a&gt;</code> tag is <strong>allowed</strong> by filtering rules to be used but it is restricted to have only one attribute (<code>href</code>) <code>config.allowedContent = 'a[!href]'</code>, then \"Target\" tab of the link dialog is automatically disabled as <code>target</code> attribute isn't included in <abbr title=\"Advanced Content Filter\">ACF</abbr> rules for <code>&lt;a&gt;</code>. This behaviour applies to dialog fields, context menus and toolbar buttons. </p> <h2>Sample configurations</h2> <p> There are several editor instances below that present different <abbr title=\"Advanced Content Filter\">ACF</abbr> setups. <strong>All of them, except the inline instance, share the same HTML content</strong> to visualize how different filtering rules affect the same input data. </p> </div> <div> <label for=\"editor1\"> Editor 1: </label> <div class=\"description\"> <p> This editor is using default configuration (\"automatic mode\"). It means that <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\"> config.allowedContent</a></code> is defined by loaded plugins. Each plugin extends filtering rules to make it's own associated content available for the user. </p> </div> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor1' );</script> </div> <br> <div> <label for=\"editor2\"> Editor 2: </label> <div class=\"description\"> <p> This editor is using a custom configuration for <abbr title=\"Advanced Content Filter\">ACF</abbr>: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor2', {\n" +
    "\tallowedContent:\n" +
    "\t\t'h1 h2 h3 p blockquote strong em;' +\n" +
    "\t\t'a[!href];' +\n" +
    "\t\t'img(left,right)[!src,alt,width,height];' +\n" +
    "\t\t'table tr th td caption;' +\n" +
    "\t\t'span{!font-family};' +'\n" +
    "\t\t'span{!color};' +\n" +
    "\t\t'span(!marker);' +\n" +
    "\t\t'del ins'\n" +
    "} );\n" +
    "</pre> <p> The following rules may require additional explanation: </p> <ul> <li> <code>h1 h2 h3 p blockquote strong em</code> - These tags are accepted by the editor. Any tag attributes will be discarded. </li> <li> <code>a[!href]</code> - <code>href</code> attribute is obligatory for <code>&lt;a&gt;</code> tag. Tags without this attribute are disarded. No other attribute will be accepted. </li> <li> <code>img(left,right)[!src,alt,width,height]</code> - <code>src</code> attribute is obligatory for <code>&lt;img&gt;</code> tag. <code>alt</code>, <code>width</code>, <code>height</code> and <code>class</code> attributes are accepted but <code>class</code> must be either <code>class=\"left\"</code> or <code>class=\"right\"</code> </li> <li> <code>table tr th td caption</code> - These tags are accepted by the editor. Any tag attributes will be discarded. </li> <li> <code>span{!font-family}</code>, <code>span{!color}</code>, <code>span(!marker)</code> - <code>&lt;span&gt;</code> tags will be accepted if either <code>font-family</code> or <code>color</code> style is set or <code>class=\"marker\"</code> is present. </li> <li> <code>del ins</code> - These tags are accepted by the editor. Any tag attributes will be discarded. </li> </ul> <p> Please note that <strong><abbr title=\"User Interface\">UI</abbr> of the editor is different</strong>. It's a response to what happened to the filters. Since <code>text-align</code> isn't allowed, the align toolbar is gone. The same thing happened to subscript/superscript, strike, underline (<code>&lt;u&gt;</code>, <code>&lt;sub&gt;</code>, <code>&lt;sup&gt;</code> are disallowed by <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-allowedContent\"> config.allowedContent</a></code>) and many other buttons. </p> </div> <textarea cols=\"80\" id=\"editor2\" name=\"editor2\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor2', {\n" +
    "\t\t\t\tallowedContent:\n" +
    "\t\t\t\t\t'h1 h2 h3 p blockquote strong em;' +\n" +
    "\t\t\t\t\t'a[!href];' +\n" +
    "\t\t\t\t\t'img(left,right)[!src,alt,width,height];' +\n" +
    "\t\t\t\t\t'table tr th td caption;' +\n" +
    "\t\t\t\t\t'span{!font-family};' +\n" +
    "\t\t\t\t\t'span{!color};' +\n" +
    "\t\t\t\t\t'span(!marker);' +\n" +
    "\t\t\t\t\t'del ins'\n" +
    "\t\t\t} );</script> </div> <br> <div> <label for=\"editor3\"> Editor 3: </label> <div class=\"description\"> <p> This editor is using a custom configuration for <abbr title=\"Advanced Content Filter\">ACF</abbr>. Note that filters can be configured as an object literal as an alternative to a string-based definition. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor3', {\n" +
    "\tallowedContent: {\n" +
    "\t\t'b i ul ol big small': true,\n" +
    "\t\t'h1 h2 h3 p blockquote li': {\n" +
    "\t\t\tstyles: 'text-align'\n" +
    "\t\t},\n" +
    "\t\ta: { attributes: '!href,target' },\n" +
    "\t\timg: {\n" +
    "\t\t\tattributes: '!src,alt',\n" +
    "\t\t\tstyles: 'width,height',\n" +
    "\t\t\tclasses: 'left,right'\n" +
    "\t\t}\n" +
    "\t}\n" +
    "} );\n" +
    "</pre> </div> <textarea cols=\"80\" id=\"editor3\" name=\"editor3\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor3', {\n" +
    "\t\t\t\tallowedContent: {\n" +
    "\t\t\t\t\t'b i ul ol big small': true,\n" +
    "\t\t\t\t\t'h1 h2 h3 p blockquote li': {\n" +
    "\t\t\t\t\t\tstyles: 'text-align'\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\ta: { attributes: '!href,target' },\n" +
    "\t\t\t\t\timg: {\n" +
    "\t\t\t\t\t\tattributes: '!src,alt',\n" +
    "\t\t\t\t\t\tstyles: 'width,height',\n" +
    "\t\t\t\t\t\tclasses: 'left,right'\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t}\n" +
    "\t\t\t} );</script> </div> <br> <div> <label for=\"editor4\"> Editor 4: </label> <div class=\"description\"> <p> This editor is using a custom set of plugins and buttons. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor4', {\n" +
    "\tremovePlugins: 'bidi,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley',\n" +
    "\tremoveButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',\n" +
    "\tformat_tags: 'p;h1;h2;h3;pre;address'\n" +
    "} );\n" +
    "</pre> <p> As you can see, removing plugins and buttons implies filtering. Several tags are not allowed in the editor because there's no plugin/button that is responsible for creating and editing this kind of content (for example: the image is missing because of <code>removeButtons: 'Image'</code>). The conclusion is that <abbr title=\"Advanced Content Filter\">ACF</abbr> works \"backwards\" as well: <strong>modifying <abbr title=\"User Interface\">UI</abbr> elements is changing allowed content rules</strong>. </p> </div> <textarea cols=\"80\" id=\"editor4\" name=\"editor4\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor4', {\n" +
    "\t\t\t\tremovePlugins: 'bidi,div,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley',\n" +
    "\t\t\t\tremoveButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',\n" +
    "\t\t\t\tformat_tags: 'p;h1;h2;h3;pre;address'\n" +
    "\t\t\t} );</script> </div> <br> <div> <label for=\"editor5\"> Editor 5: </label> <div class=\"description\"> <p> This editor is built on editable <code>&lt;h1&gt;</code> element. <abbr title=\"Advanced Content Filter\">ACF</abbr> takes care of what can be included in <code>&lt;h1&gt;</code>. Note that there are no block styles in Styles combo. Also why lists, indentation, blockquote, div, form and other buttons are missing. </p> <p> <abbr title=\"Advanced Content Filter\">ACF</abbr> makes sure that no disallowed tags will come to <code>&lt;h1&gt;</code> so the final markup is valid. If the user tried to paste some invalid HTML into this editor (let's say a list), it would be automatically converted into plain text. </p> </div> <h1 id=\"editor5\" contenteditable=\"true\"> <em>Apollo 11</em> was the spaceflight that landed the first humans, Americans <a href=\"http://en.wikipedia.org/wiki/Neil_Armstrong\" title=\"Neil Armstrong\">Neil Armstrong</a> and <a href=\"http://en.wikipedia.org/wiki/Buzz_Aldrin\" title=\"Buzz Aldrin\">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. </h1> </div> <br> <div> <label for=\"editor3\"> Editor 6: </label> <div class=\"description\"> <p> This editor is using a custom configuration for <abbr title=\"Advanced Content Filter\">ACF</abbr>. It's using the <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_disallowed_content\" rel=\"noopener noreferrer\" target=\"_blank\"> Disallowed Content</a> property of the filter to eliminate all <code>title</code> attributes. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor6', {\n" +
    "\tallowedContent: {\n" +
    "\t\t'b i ul ol big small': true,\n" +
    "\t\t'h1 h2 h3 p blockquote li': {\n" +
    "\t\t\tstyles: 'text-align'\n" +
    "\t\t},\n" +
    "\t\ta: {attributes: '!href,target'},\n" +
    "\t\timg: {\n" +
    "\t\t\tattributes: '!src,alt',\n" +
    "\t\t\tstyles: 'width,height',\n" +
    "\t\t\tclasses: 'left,right'\n" +
    "\t\t}\n" +
    "\t},\n" +
    "\tdisallowedContent: '*{title*}'\n" +
    "} );\n" +
    "</pre> </div> <textarea cols=\"80\" id=\"editor6\" name=\"editor6\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor6', {\n" +
    "\t\t\t\tallowedContent: {\n" +
    "\t\t\t\t\t'b i ul ol big small': true,\n" +
    "\t\t\t\t\t'h1 h2 h3 p blockquote li': {\n" +
    "\t\t\t\t\t\tstyles: 'text-align'\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\ta: {attributes: '!href,target'},\n" +
    "\t\t\t\t\timg: {\n" +
    "\t\t\t\t\t\tattributes: '!src,alt',\n" +
    "\t\t\t\t\t\tstyles: 'width,height',\n" +
    "\t\t\t\t\t\tclasses: 'left,right'\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t},\n" +
    "\t\t\t\tdisallowedContent: '*{title*}'\n" +
    "\t\t\t} );</script> </div> <br> <div> <label for=\"editor7\"> Editor 7: </label> <div class=\"description\"> <p> This editor is using a custom configuration for <abbr title=\"Advanced Content Filter\">ACF</abbr>. It's using the <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_disallowed_content\" rel=\"noopener noreferrer\" target=\"_blank\"> Disallowed Content</a> property of the filter to eliminate all <code>a</code> and <code>img</code> tags, while allowing all other tags. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor7', {\n" +
    "\tallowedContent: {\n" +
    "\t\t// Allow all content.\n" +
    "\t\t$1: {\n" +
    "\t\t\telements: CKEDITOR.dtd,\n" +
    "\t\t\tattributes: true,\n" +
    "\t\t\tstyles: true,\n" +
    "\t\t\tclasses: true\n" +
    "\t\t}\n" +
    "\t},\n" +
    "\tdisallowedContent: 'img a'\n" +
    "} );\n" +
    "</pre> </div> <textarea cols=\"80\" id=\"editor7\" name=\"editor7\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor7', {\n" +
    "\t\t\t\tallowedContent: {\n" +
    "\t\t\t\t\t// allow all content\n" +
    "\t\t\t\t\t$1: {\n" +
    "\t\t\t\t\t\telements: CKEDITOR.dtd,\n" +
    "\t\t\t\t\t\tattributes: true,\n" +
    "\t\t\t\t\t\tstyles: true,\n" +
    "\t\t\t\t\t\tclasses: true\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t},\n" +
    "\t\t\t\tdisallowedContent: 'img a'\n" +
    "\t\t\t} );</script> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/dialog/dialog.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Using API to Customize Dialog Windows &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"../../../samples/old/sample.css\"> <meta name=\"ckeditor-sample-name\" content=\"Using the JavaScript API to customize dialog windows\"> <meta name=\"ckeditor-sample-group\" content=\"Advanced Samples\"> <meta name=\"ckeditor-sample-description\" content=\"Using the dialog windows API to customize dialog windows without changing the original editor code.\"> <style>.cke_button__mybutton_icon\n" +
    "\t\t{\n" +
    "\t\t\tdisplay: none !important;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t.cke_button__mybutton_label\n" +
    "\t\t{\n" +
    "\t\t\tdisplay: inline !important;\n" +
    "\t\t}</style> <script>CKEDITOR.on( 'instanceCreated', function( ev ){\n" +
    "\t\t\tvar editor = ev.editor;\n" +
    "\n" +
    "\t\t\t// Listen for the \"pluginsLoaded\" event, so we are sure that the\n" +
    "\t\t\t// \"dialog\" plugin has been loaded and we are able to do our\n" +
    "\t\t\t// customizations.\n" +
    "\t\t\teditor.on( 'pluginsLoaded', function() {\n" +
    "\n" +
    "\t\t\t\t// If our custom dialog has not been registered, do that now.\n" +
    "\t\t\t\tif ( !CKEDITOR.dialog.exists( 'myDialog' ) ) {\n" +
    "\t\t\t\t\t// We need to do the following trick to find out the dialog\n" +
    "\t\t\t\t\t// definition file URL path. In the real world, you would simply\n" +
    "\t\t\t\t\t// point to an absolute path directly, like \"/mydir/mydialog.js\".\n" +
    "\t\t\t\t\tvar href = document.location.href.split( '/' );\n" +
    "\t\t\t\t\thref.pop();\n" +
    "\t\t\t\t\thref.push( 'assets/my_dialog.js' );\n" +
    "\t\t\t\t\thref = href.join( '/' );\n" +
    "\n" +
    "\t\t\t\t\t// Finally, register the dialog.\n" +
    "\t\t\t\t\tCKEDITOR.dialog.add( 'myDialog', href );\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t// Register the command used to open the dialog.\n" +
    "\t\t\t\teditor.addCommand( 'myDialogCmd', new CKEDITOR.dialogCommand( 'myDialog' ) );\n" +
    "\n" +
    "\t\t\t\t// Add the a custom toolbar buttons, which fires the above\n" +
    "\t\t\t\t// command..\n" +
    "\t\t\t\teditor.ui.add( 'MyButton', CKEDITOR.UI_BUTTON, {\n" +
    "\t\t\t\t\tlabel: 'My Dialog',\n" +
    "\t\t\t\t\tcommand: 'myDialogCmd'\n" +
    "\t\t\t\t});\n" +
    "\t\t\t});\n" +
    "\t\t});\n" +
    "\n" +
    "\t\t// When opening a dialog, its \"definition\" is created for it, for\n" +
    "\t\t// each editor instance. The \"dialogDefinition\" event is then\n" +
    "\t\t// fired. We should use this event to make customizations to the\n" +
    "\t\t// definition of existing dialogs.\n" +
    "\t\tCKEDITOR.on( 'dialogDefinition', function( ev ) {\n" +
    "\t\t\t// Take the dialog name and its definition from the event data.\n" +
    "\t\t\tvar dialogName = ev.data.name;\n" +
    "\t\t\tvar dialogDefinition = ev.data.definition;\n" +
    "\n" +
    "\t\t\t// Check if the definition is from the dialog we're\n" +
    "\t\t\t// interested on (the \"Link\" dialog).\n" +
    "\t\t\tif ( dialogName == 'myDialog' && ev.editor.name == 'editor2' ) {\n" +
    "\t\t\t\t// Get a reference to the \"Link Info\" tab.\n" +
    "\t\t\t\tvar infoTab = dialogDefinition.getContents( 'tab1' );\n" +
    "\n" +
    "\t\t\t\t// Add a new text field to the \"tab1\" tab page.\n" +
    "\t\t\t\tinfoTab.add( {\n" +
    "\t\t\t\t\ttype: 'text',\n" +
    "\t\t\t\t\tlabel: 'My Custom Field',\n" +
    "\t\t\t\t\tid: 'customField',\n" +
    "\t\t\t\t\t'default': 'Sample!',\n" +
    "\t\t\t\t\tvalidate: function() {\n" +
    "\t\t\t\t\t\tif ( ( /\\d/ ).test( this.getValue() ) )\n" +
    "\t\t\t\t\t\t\treturn 'My Custom Field must not contain digits';\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t});\n" +
    "\n" +
    "\t\t\t\t// Remove the \"select1\" field from the \"tab1\" tab.\n" +
    "\t\t\t\tinfoTab.remove( 'select1' );\n" +
    "\n" +
    "\t\t\t\t// Set the default value for \"input1\" field.\n" +
    "\t\t\t\tvar input1 = infoTab.get( 'input1' );\n" +
    "\t\t\t\tinput1[ 'default' ] = 'www.example.com';\n" +
    "\n" +
    "\t\t\t\t// Remove the \"tab2\" tab page.\n" +
    "\t\t\t\tdialogDefinition.removeContents( 'tab2' );\n" +
    "\n" +
    "\t\t\t\t// Add a new tab to the \"Link\" dialog.\n" +
    "\t\t\t\tdialogDefinition.addContents( {\n" +
    "\t\t\t\t\tid: 'customTab',\n" +
    "\t\t\t\t\tlabel: 'My Tab',\n" +
    "\t\t\t\t\taccessKey: 'M',\n" +
    "\t\t\t\t\telements: [\n" +
    "\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\tid: 'myField1',\n" +
    "\t\t\t\t\t\t\ttype: 'text',\n" +
    "\t\t\t\t\t\t\tlabel: 'My Text Field'\n" +
    "\t\t\t\t\t\t},\n" +
    "\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\tid: 'myField2',\n" +
    "\t\t\t\t\t\t\ttype: 'text',\n" +
    "\t\t\t\t\t\t\tlabel: 'Another Text Field'\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t]\n" +
    "\t\t\t\t});\n" +
    "\n" +
    "\t\t\t\t// Provide the focus handler to start initial focus in \"customField\" field.\n" +
    "\t\t\t\tdialogDefinition.onFocus = function() {\n" +
    "\t\t\t\t\tvar urlField = this.getContentElement( 'tab1', 'customField' );\n" +
    "\t\t\t\t\turlField.select();\n" +
    "\t\t\t\t};\n" +
    "\t\t\t}\n" +
    "\t\t});\n" +
    "\n" +
    "\t\tvar config = {\n" +
    "\t\t\textraPlugins: 'dialog',\n" +
    "\t\t\ttoolbar: [ [ 'MyButton' ] ]\n" +
    "\t\t};</script> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Using CKEditor Dialog API </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to use the <a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.dialog\">CKEditor Dialog API</a> to customize CKEditor dialog windows without changing the original editor code. The following customizations are being done in the example below: </p> <p> For details on how to create this setup check the source code of this sample page. </p> </div> <p>A custom dialog is added to the editors using the <code>pluginsLoaded</code> event, from an external <a target=\"_blank\" href=\"assets/my_dialog.js\">dialog definition file</a>:</p> <ol> <li><strong>Creating a custom dialog window</strong> &ndash; \"My Dialog\" dialog window opened with the \"My Dialog\" toolbar button.</li> <li><strong>Creating a custom button</strong> &ndash; Add button to open the dialog with \"My Dialog\" toolbar button.</li> </ol> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>// Replace the <textarea id=\"editor1\"> with an CKEditor instance.\n" +
    "\t\tCKEDITOR.replace( 'editor1', config );</script> <p>The below editor modify the dialog definition of the above added dialog using the <code>dialogDefinition</code> event:</p> <ol> <li><strong>Adding dialog tab</strong> &ndash; Add new tab \"My Tab\" to dialog window.</li> <li><strong>Removing a dialog window tab</strong> &ndash; Remove \"Second Tab\" page from the dialog window.</li> <li><strong>Adding dialog window fields</strong> &ndash; Add \"My Custom Field\" to the dialog window.</li> <li><strong>Removing dialog window field</strong> &ndash; Remove \"Select Field\" selection field from the dialog window.</li> <li><strong>Setting default values for dialog window fields</strong> &ndash; Set default value of \"Text Field\" text field. </li> <li><strong>Setup initial focus for dialog window</strong> &ndash; Put initial focus on \"My Custom Field\" text field. </li> </ol> <textarea cols=\"80\" id=\"editor2\" name=\"editor2\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>// Replace the <textarea id=\"editor1\"> with an CKEditor instance.\n" +
    "\t\tCKEDITOR.replace( 'editor2', config );</script> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/divreplace.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Replace DIV &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>div.editable\n" +
    "\t\t{\n" +
    "\t\t\tborder: solid 2px transparent;\n" +
    "\t\t\tpadding-left: 15px;\n" +
    "\t\t\tpadding-right: 15px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tdiv.editable:hover\n" +
    "\t\t{\n" +
    "\t\t\tborder-color: black;\n" +
    "\t\t}</style> <script>// Uncomment the following code to test the \"Timeout Loading Method\".\n" +
    "\t\t// CKEDITOR.loadFullCoreTimeout = 5;\n" +
    "\n" +
    "\t\twindow.onload = function() {\n" +
    "\t\t\t// Listen to the double click event.\n" +
    "\t\t\tif ( window.addEventListener )\n" +
    "\t\t\t\tdocument.body.addEventListener( 'dblclick', onDoubleClick, false );\n" +
    "\t\t\telse if ( window.attachEvent )\n" +
    "\t\t\t\tdocument.body.attachEvent( 'ondblclick', onDoubleClick );\n" +
    "\n" +
    "\t\t};\n" +
    "\n" +
    "\t\tfunction onDoubleClick( ev ) {\n" +
    "\t\t\t// Get the element which fired the event. This is not necessarily the\n" +
    "\t\t\t// element to which the event has been attached.\n" +
    "\t\t\tvar element = ev.target || ev.srcElement;\n" +
    "\n" +
    "\t\t\t// Find out the div that holds this element.\n" +
    "\t\t\tvar name;\n" +
    "\n" +
    "\t\t\tdo {\n" +
    "\t\t\t\telement = element.parentNode;\n" +
    "\t\t\t}\n" +
    "\t\t\twhile ( element && ( name = element.nodeName.toLowerCase() ) &&\n" +
    "\t\t\t\t( name != 'div' || element.className.indexOf( 'editable' ) == -1 ) && name != 'body' );\n" +
    "\n" +
    "\t\t\tif ( name == 'div' && element.className.indexOf( 'editable' ) != -1 )\n" +
    "\t\t\t\treplaceDiv( element );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tvar editor;\n" +
    "\n" +
    "\t\tfunction replaceDiv( div ) {\n" +
    "\t\t\tif ( editor )\n" +
    "\t\t\t\teditor.destroy();\n" +
    "\n" +
    "\t\t\teditor = CKEDITOR.replace( div );\n" +
    "\t\t}</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Replace DIV with CKEditor on the Fly </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to automatically replace <code>&lt;div&gt;</code> elements with a CKEditor instance on the fly, following user's doubleclick. The content that was previously placed inside the <code>&lt;div&gt;</code> element will now be moved into CKEditor editing area. </p> <p> For details on how to create this setup check the source code of this sample page. </p> </div> <p> Double-click any of the following <code>&lt;div&gt;</code> elements to transform them into editor instances. </p> <div class=\"editable\"> <h3> Part 1 </h3> <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras et ipsum quis mi semper accumsan. Integer pretium dui id massa. Suspendisse in nisl sit amet urna rutrum imperdiet. Nulla eu tellus. Donec ante nisi, ullamcorper quis, fringilla nec, sagittis eleifend, pede. Nulla commodo interdum massa. Donec id metus. Fusce eu ipsum. Suspendisse auctor. Phasellus fermentum porttitor risus. </p> </div> <div class=\"editable\"> <h3> Part 2 </h3> <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras et ipsum quis mi semper accumsan. Integer pretium dui id massa. Suspendisse in nisl sit amet urna rutrum imperdiet. Nulla eu tellus. Donec ante nisi, ullamcorper quis, fringilla nec, sagittis eleifend, pede. Nulla commodo interdum massa. Donec id metus. Fusce eu ipsum. Suspendisse auctor. Phasellus fermentum porttitor risus. </p> <p> Donec velit. Mauris massa. Vestibulum non nulla. Nam suscipit arcu nec elit. Phasellus sollicitudin iaculis ante. Ut non mauris et sapien tincidunt adipiscing. Vestibulum vitae leo. Suspendisse nec mi tristique nulla laoreet vulputate. </p> </div> <div class=\"editable\"> <h3> Part 3 </h3> <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras et ipsum quis mi semper accumsan. Integer pretium dui id massa. Suspendisse in nisl sit amet urna rutrum imperdiet. Nulla eu tellus. Donec ante nisi, ullamcorper quis, fringilla nec, sagittis eleifend, pede. Nulla commodo interdum massa. Donec id metus. Fusce eu ipsum. Suspendisse auctor. Phasellus fermentum porttitor risus. </p> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/enterkey/enterkey.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>ENTER Key Configuration &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <link href=\"../../../samples/old/sample.css\" rel=\"stylesheet\"> <meta name=\"ckeditor-sample-name\" content=\"Using the &quot;Enter&quot; key in CKEditor\"> <meta name=\"ckeditor-sample-group\" content=\"Advanced Samples\"> <meta name=\"ckeditor-sample-description\" content=\"Configuring the behavior of &lt;em&gt;Enter&lt;/em&gt; and &lt;em&gt;Shift+Enter&lt;/em&gt; keys.\"> <script>var editor;\n" +
    "\n" +
    "\t\tfunction changeEnter() {\n" +
    "\t\t\t// If we already have an editor, let's destroy it first.\n" +
    "\t\t\tif ( editor )\n" +
    "\t\t\t\teditor.destroy( true );\n" +
    "\n" +
    "\t\t\t// Create the editor again, with the appropriate settings.\n" +
    "\t\t\teditor = CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\textraPlugins: 'enterkey',\n" +
    "\t\t\t\tenterMode: Number( document.getElementById( 'xEnter' ).value ),\n" +
    "\t\t\t\tshiftEnterMode: Number( document.getElementById( 'xShiftEnter' ).value )\n" +
    "\t\t\t});\n" +
    "\t\t}\n" +
    "\n" +
    "\t\twindow.onload = changeEnter;</script> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; ENTER Key Configuration </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/enterkey.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to configure the <em>Enter</em> and <em>Shift+Enter</em> keys to perform actions specified in the <a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-enterMode\"><code>enterMode</code></a> and <a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-shiftEnterMode\"><code>shiftEnterMode</code></a> parameters, respectively. You can choose from the following options: </p> <ul class=\"samples\"> <li><strong><code>ENTER_P</code></strong> &ndash; new <code>&lt;p&gt;</code> paragraphs are created;</li> <li><strong><code>ENTER_BR</code></strong> &ndash; lines are broken with <code>&lt;br&gt;</code> elements;</li> <li><strong><code>ENTER_DIV</code></strong> &ndash; new <code>&lt;div&gt;</code> blocks are created.</li> </ul> <p> The sample code below shows how to configure CKEditor to create a <code>&lt;div&gt;</code> block when <em>Enter</em> key is pressed. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\t<strong>enterMode: CKEDITOR.ENTER_DIV</strong>\n" +
    "});</pre> <p> Note that <code><em>textarea_id</em></code> in the code above is the <code>id</code> attribute of the <code>&lt;textarea&gt;</code> element to be replaced. </p> </div> <div style=\"float: left; margin-right: 20px\"> When <em>Enter</em> is pressed:<br> <select id=\"xEnter\" onchange=\"changeEnter()\"> <option selected value=\"1\">Create a new &lt;P&gt; (recommended)</option> <option value=\"3\">Create a new &lt;DIV&gt;</option> <option value=\"2\">Break the line with a &lt;BR&gt;</option> </select> </div> <div style=\"float: left\"> When <em>Shift+Enter</em> is pressed:<br> <select id=\"xShiftEnter\" onchange=\"changeEnter()\"> <option value=\"1\">Create a new &lt;P&gt;</option> <option value=\"3\">Create a new &lt;DIV&gt;</option> <option selected value=\"2\">Break the line with a &lt;BR&gt; (recommended)</option> </select> </div> <br style=\"clear: both\"> <form action=\"../../../samples/sample_posteddata.php\" method=\"post\"> <p> <br> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.</textarea> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/htmlwriter/outputforflash.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Output for Flash &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <script src=\"../../../samples/old/sample.js\"></script> <script src=\"assets/outputforflash/swfobject.js\"></script> <link href=\"../../../samples/old/sample.css\" rel=\"stylesheet\"> <meta name=\"ckeditor-sample-required-plugins\" content=\"sourcearea\"> <meta name=\"ckeditor-sample-name\" content=\"Output for Flash\"> <meta name=\"ckeditor-sample-group\" content=\"Advanced Samples\"> <meta name=\"ckeditor-sample-description\" content=\"Configuring CKEditor to produce HTML code that can be used with Adobe Flash.\"> <style>.alert\n" +
    "\t\t{\n" +
    "\t\t\tbackground: #ffa84c;\n" +
    "\t\t\tpadding: 10px 15px;\n" +
    "\t\t\tfont-weight: bold;\n" +
    "\t\t\tdisplay: block;\n" +
    "\t\t\tmargin-bottom: 20px;\n" +
    "\t\t}</style> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Producing Flash Compliant HTML Output </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to configure CKEditor to output HTML code that can be used with <a class=\"samples\" href=\"http://www.adobe.com/livedocs/flash/9.0/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&amp;file=00000922.html\"> Adobe Flash</a>. The code will contain a subset of standard HTML elements like <code>&lt;b&gt;</code>, <code>&lt;i&gt;</code>, and <code>&lt;p&gt;</code> as well as HTML attributes. </p> <p> To add a CKEditor instance outputting Flash compliant HTML code, load the editor using a standard JavaScript call, and define CKEditor features to use HTML elements and attributes. </p> <p> For details on how to create this setup check the source code of this sample page. </p> </div> <p> To see how it works, create some content in the editing area of CKEditor on the left and send it to the Flash object on the right side of the page by using the <strong>Send to Flash</strong> button. </p> <table style=\"width: 100%; border-spacing: 0; border-collapse:collapse\"> <tr> <td style=\"width: 100%\"> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;&lt;b&gt;&lt;font size=&quot;18&quot; style=&quot;font-size:18px;&quot;&gt;Flash and HTML&lt;/font&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;It is possible to have &lt;a href=&quot;https://ckeditor.com&quot;&gt;CKEditor&lt;/a&gt; creating content that will be later loaded inside &lt;b&gt;Flash&lt;/b&gt; objects and animations.&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;Flash has a few limitations when dealing with HTML:&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;It has limited support on tags.&lt;/li&gt;&lt;li&gt;There is no margin between block elements, like paragraphs.&lt;/li&gt;&lt;/ul&gt;</textarea> <script>if ( document.location.protocol == 'file:' )\n" +
    "\t\t\t\t\t\talert( 'Warning: This samples does not work when loaded from local filesystem' +\n" +
    "\t\t\t\t\t\t\t'due to security restrictions implemented in Flash.' +\n" +
    "\t\t\t\t\t\t\t'\\n\\nPlease load the sample from a web server instead.' );\n" +
    "\n" +
    "\t\t\t\t\tvar editor = CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Ensure that htmlwriter plugin, which is required for this sample, is loaded.\n" +
    "\t\t\t\t\t\t */\n" +
    "\t\t\t\t\t\textraPlugins: 'htmlwriter',\n" +
    "\n" +
    "\t\t\t\t\t\theight: 290,\n" +
    "\t\t\t\t\t\twidth: '100%',\n" +
    "\t\t\t\t\t\ttoolbar: [\n" +
    "\t\t\t\t\t\t\t[ 'Source', '-', 'Bold', 'Italic', 'Underline', '-', 'BulletedList', '-', 'Link', 'Unlink' ],\n" +
    "\t\t\t\t\t\t\t[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],\n" +
    "\t\t\t\t\t\t\t'/',\n" +
    "\t\t\t\t\t\t\t[ 'Font', 'FontSize' ],\n" +
    "\t\t\t\t\t\t\t[ 'TextColor', '-', 'About' ]\n" +
    "\t\t\t\t\t\t],\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Style sheet for the contents\n" +
    "\t\t\t\t\t\t */\n" +
    "\t\t\t\t\t\tcontentsCss: 'body {color:#000; background-color#FFF; font-family: Arial; font-size:80%;} p, ol, ul {margin-top: 0px; margin-bottom: 0px;}',\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Quirks doctype\n" +
    "\t\t\t\t\t\t */\n" +
    "\t\t\t\t\t\tdocType: '<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">',\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Core styles.\n" +
    "\t\t\t\t\t\t */\n" +
    "\t\t\t\t\t\tcoreStyles_bold: { element: 'b' },\n" +
    "\t\t\t\t\t\tcoreStyles_italic: { element: 'i' },\n" +
    "\t\t\t\t\t\tcoreStyles_underline: { element: 'u' },\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Font face.\n" +
    "\t\t\t\t\t\t */\n" +
    "\n" +
    "\t\t\t\t\t\t// Define the way font elements will be applied to the document. The \"font\"\n" +
    "\t\t\t\t\t\t// element will be used.\n" +
    "\t\t\t\t\t\tfont_style: {\n" +
    "\t\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\t\tattributes: { 'face': '#(family)' }\n" +
    "\t\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Font sizes.\n" +
    "\t\t\t\t\t\t */\n" +
    "\n" +
    "\t\t\t\t\t\t// The CSS part of the font sizes isn't used by Flash, it is there to get the\n" +
    "\t\t\t\t\t\t// font rendered correctly in CKEditor.\n" +
    "\t\t\t\t\t\tfontSize_sizes: '8px/8;9px/9;10px/10;11px/11;12px/12;14px/14;16px/16;18px/18;20px/20;22px/22;24px/24;26px/26;28px/28;36px/36;48px/48;72px/72',\n" +
    "\t\t\t\t\t\tfontSize_style: {\n" +
    "\t\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\t\tattributes: { 'size': '#(size)' },\n" +
    "\t\t\t\t\t\t\tstyles: { 'font-size': '#(size)px' }\n" +
    "\t\t\t\t\t\t} ,\n" +
    "\n" +
    "\t\t\t\t\t\t/*\n" +
    "\t\t\t\t\t\t * Font colors.\n" +
    "\t\t\t\t\t\t */\n" +
    "\t\t\t\t\t\tcolorButton_enableMore: true,\n" +
    "\n" +
    "\t\t\t\t\t\tcolorButton_foreStyle: {\n" +
    "\t\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\t\tattributes: { 'color': '#(color)' }\n" +
    "\t\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t\tcolorButton_backStyle: {\n" +
    "\t\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\t\tstyles: { 'background-color': '#(color)' }\n" +
    "\t\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t\ton: { 'instanceReady': configureFlashOutput }\n" +
    "\t\t\t\t\t});\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Adjust the behavior of the dataProcessor to match the\n" +
    "\t\t\t\t\t * requirements of Flash\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tfunction configureFlashOutput( ev ) {\n" +
    "\t\t\t\t\t\tvar editor = ev.editor,\n" +
    "\t\t\t\t\t\t\tdataProcessor = editor.dataProcessor,\n" +
    "\t\t\t\t\t\t\thtmlFilter = dataProcessor && dataProcessor.htmlFilter;\n" +
    "\n" +
    "\t\t\t\t\t\t// Out self closing tags the HTML4 way, like <br>.\n" +
    "\t\t\t\t\t\tdataProcessor.writer.selfClosingEnd = '>';\n" +
    "\n" +
    "\t\t\t\t\t\t// Make output formatting match Flash expectations\n" +
    "\t\t\t\t\t\tvar dtd = CKEDITOR.dtd;\n" +
    "\t\t\t\t\t\tfor ( var e in CKEDITOR.tools.extend( {}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent ) ) {\n" +
    "\t\t\t\t\t\t\tdataProcessor.writer.setRules( e, {\n" +
    "\t\t\t\t\t\t\t\tindent: false,\n" +
    "\t\t\t\t\t\t\t\tbreakBeforeOpen: false,\n" +
    "\t\t\t\t\t\t\t\tbreakAfterOpen: false,\n" +
    "\t\t\t\t\t\t\t\tbreakBeforeClose: false,\n" +
    "\t\t\t\t\t\t\t\tbreakAfterClose: false\n" +
    "\t\t\t\t\t\t\t});\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\tdataProcessor.writer.setRules( 'br', {\n" +
    "\t\t\t\t\t\t\tindent: false,\n" +
    "\t\t\t\t\t\t\tbreakBeforeOpen: false,\n" +
    "\t\t\t\t\t\t\tbreakAfterOpen: false,\n" +
    "\t\t\t\t\t\t\tbreakBeforeClose: false,\n" +
    "\t\t\t\t\t\t\tbreakAfterClose: false\n" +
    "\t\t\t\t\t\t});\n" +
    "\n" +
    "\t\t\t\t\t\t// Output properties as attributes, not styles.\n" +
    "\t\t\t\t\t\thtmlFilter.addRules( {\n" +
    "\t\t\t\t\t\t\telements: {\n" +
    "\t\t\t\t\t\t\t\t$: function( element ) {\n" +
    "\t\t\t\t\t\t\t\t\tvar style, match, width, height, align;\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t// Output dimensions of images as width and height\n" +
    "\t\t\t\t\t\t\t\t\tif ( element.name == 'img' ) {\n" +
    "\t\t\t\t\t\t\t\t\t\tstyle = element.attributes.style;\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tif ( style ) {\n" +
    "\t\t\t\t\t\t\t\t\t\t\t// Get the width from the style.\n" +
    "\t\t\t\t\t\t\t\t\t\t\tmatch = ( /(?:^|\\s)width\\s*:\\s*(\\d+)px/i ).exec( style );\n" +
    "\t\t\t\t\t\t\t\t\t\t\twidth = match && match[1];\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t// Get the height from the style.\n" +
    "\t\t\t\t\t\t\t\t\t\t\tmatch = ( /(?:^|\\s)height\\s*:\\s*(\\d+)px/i ).exec( style );\n" +
    "\t\t\t\t\t\t\t\t\t\t\theight = match && match[1];\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\tif ( width ) {\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.style = element.attributes.style.replace( /(?:^|\\s)width\\s*:\\s*(\\d+)px;?/i , '' );\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.width = width;\n" +
    "\t\t\t\t\t\t\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\tif ( height ) {\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.style = element.attributes.style.replace( /(?:^|\\s)height\\s*:\\s*(\\d+)px;?/i , '' );\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.height = height;\n" +
    "\t\t\t\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t// Output alignment of paragraphs using align\n" +
    "\t\t\t\t\t\t\t\t\tif ( element.name == 'p' ) {\n" +
    "\t\t\t\t\t\t\t\t\t\tstyle = element.attributes.style;\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tif ( style ) {\n" +
    "\t\t\t\t\t\t\t\t\t\t\t// Get the align from the style.\n" +
    "\t\t\t\t\t\t\t\t\t\t\tmatch = ( /(?:^|\\s)text-align\\s*:\\s*(\\w*);?/i ).exec( style );\n" +
    "\t\t\t\t\t\t\t\t\t\t\talign = match && match[1];\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\tif ( align ) {\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.style = element.attributes.style.replace( /(?:^|\\s)text-align\\s*:\\s*(\\w*);?/i , '' );\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\telement.attributes.align = align;\n" +
    "\t\t\t\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\tif ( element.attributes.style === '' )\n" +
    "\t\t\t\t\t\t\t\t\t\tdelete element.attributes.style;\n" +
    "\n" +
    "\t\t\t\t\t\t\t\t\treturn element;\n" +
    "\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t});\n" +
    "\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\tfunction sendToFlash() {\n" +
    "\t\t\t\t\t\tvar html = CKEDITOR.instances.editor1.getData() ;\n" +
    "\n" +
    "\t\t\t\t\t\t// Quick fix for link color.\n" +
    "\t\t\t\t\t\thtml = html.replace( /<a /g, '<font color=\"#0000FF\"><u><a ' )\n" +
    "\t\t\t\t\t\thtml = html.replace( /<\\/a>/g, '</a></u></font>' )\n" +
    "\n" +
    "\t\t\t\t\t\tvar flash = document.getElementById( 'ckFlashContainer' ) ;\n" +
    "\t\t\t\t\t\tflash.setData( html ) ;\n" +
    "\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\tCKEDITOR.domReady( function() {\n" +
    "\t\t\t\t\t\tif ( !swfobject.hasFlashPlayerVersion( '8' ) ) {\n" +
    "\t\t\t\t\t\t\tCKEDITOR.dom.element.createFromHtml( '<span class=\"alert\">' +\n" +
    "\t\t\t\t\t\t\t\t\t'At least Adobe Flash Player 8 is required to run this sample. ' +\n" +
    "\t\t\t\t\t\t\t\t\t'You can download it from <a href=\"http://get.adobe.com/flashplayer\">Adobe\\'s website</a>.' +\n" +
    "\t\t\t\t\t\t\t\t'</span>' ).insertBefore( editor.element );\n" +
    "\t\t\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t\t\tswfobject.embedSWF(\n" +
    "\t\t\t\t\t\t\t'assets/outputforflash/outputforflash.swf',\n" +
    "\t\t\t\t\t\t\t'ckFlashContainer',\n" +
    "\t\t\t\t\t\t\t'550',\n" +
    "\t\t\t\t\t\t\t'400',\n" +
    "\t\t\t\t\t\t\t'8',\n" +
    "\t\t\t\t\t\t\t{ wmode: 'transparent' }\n" +
    "\t\t\t\t\t\t);\n" +
    "\t\t\t\t\t});</script> <p> <input type=\"button\" value=\"Send to Flash\" onclick=\"sendToFlash()\"> </p> </td> <td style=\"vertical-align: top; padding-left: 20px\"> <div id=\"ckFlashContainer\"></div> </td> </tr> </table> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/htmlwriter/outputhtml.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>HTML Compliant Output &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <script src=\"../../../samples/old/sample.js\"></script> <link href=\"../../../samples/old/sample.css\" rel=\"stylesheet\"> <meta name=\"ckeditor-sample-required-plugins\" content=\"sourcearea\"> <meta name=\"ckeditor-sample-name\" content=\"Output HTML\"> <meta name=\"ckeditor-sample-group\" content=\"Advanced Samples\"> <meta name=\"ckeditor-sample-description\" content=\"Configuring CKEditor to produce legacy HTML 4 code.\"> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Producing HTML Compliant Output </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to configure CKEditor to output valid <a class=\"samples\" href=\"http://www.w3.org/TR/html401/\">HTML 4.01</a> code. Traditional HTML elements like <code>&lt;b&gt;</code>, <code>&lt;i&gt;</code>, and <code>&lt;font&gt;</code> are used in place of <code>&lt;strong&gt;</code>, <code>&lt;em&gt;</code>, and CSS styles. </p> <p> To add a CKEditor instance outputting legacy HTML 4.01 code, load the editor using a standard JavaScript call, and define CKEditor features to use the HTML compliant elements and attributes. </p> <p> A snippet of the configuration code can be seen below; check the source of this page for full definition: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\tcoreStyles_bold: { element: 'b' },\n" +
    "\tcoreStyles_italic: { element: 'i' },\n" +
    "\n" +
    "\tfontSize_style: {\n" +
    "\t\telement: 'font',\n" +
    "\t\tattributes: { 'size': '#(size)' }\n" +
    "\t}\n" +
    "\n" +
    "\t...\n" +
    "});</pre> </div> <form action=\"../../../samples/sample_posteddata.php\" method=\"post\"> <p> <label for=\"editor1\"> Editor 1: </label> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;b&gt;sample text&lt;/b&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Ensure that htmlwriter plugin, which is required for this sample, is loaded.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\textraPlugins: 'htmlwriter',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Style sheet for the contents\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tcontentsCss: 'body {color:#000; background-color#:FFF;}',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Simple HTML5 doctype\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tdocType: '<!DOCTYPE HTML>',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Allowed content rules which beside limiting allowed HTML\n" +
    "\t\t\t\t\t * will also take care of transforming styles to attributes\n" +
    "\t\t\t\t\t * (currently only for img - see transformation rules defined below).\n" +
    "\t\t\t\t\t *\n" +
    "\t\t\t\t\t * Read more: https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_advanced_content_filter\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tallowedContent:\n" +
    "\t\t\t\t\t\t'h1 h2 h3 p pre[align]; ' +\n" +
    "\t\t\t\t\t\t'blockquote code kbd samp var del ins cite q b i u strike ul ol li hr table tbody tr td th caption; ' +\n" +
    "\t\t\t\t\t\t'img[!src,alt,align,width,height]; font[!face]; font[!family]; font[!color]; font[!size]; font{!background-color}; a[!href]; a[!name]',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Core styles.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tcoreStyles_bold: { element: 'b' },\n" +
    "\t\t\t\t\tcoreStyles_italic: { element: 'i' },\n" +
    "\t\t\t\t\tcoreStyles_underline: { element: 'u' },\n" +
    "\t\t\t\t\tcoreStyles_strike: { element: 'strike' },\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font face.\n" +
    "\t\t\t\t\t */\n" +
    "\n" +
    "\t\t\t\t\t// Define the way font elements will be applied to the document.\n" +
    "\t\t\t\t\t// The \"font\" element will be used.\n" +
    "\t\t\t\t\tfont_style: {\n" +
    "\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\tattributes: { 'face': '#(family)' }\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font sizes.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tfontSize_sizes: 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',\n" +
    "\t\t\t\t\tfontSize_style: {\n" +
    "\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\tattributes: { 'size': '#(size)' }\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font colors.\n" +
    "\t\t\t\t\t */\n" +
    "\n" +
    "\t\t\t\t\tcolorButton_foreStyle: {\n" +
    "\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\tattributes: { 'color': '#(color)' }\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\tcolorButton_backStyle: {\n" +
    "\t\t\t\t\t\telement: 'font',\n" +
    "\t\t\t\t\t\tstyles: { 'background-color': '#(color)' }\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Styles combo.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tstylesSet: [\n" +
    "\t\t\t\t\t\t{ name: 'Computer Code', element: 'code' },\n" +
    "\t\t\t\t\t\t{ name: 'Keyboard Phrase', element: 'kbd' },\n" +
    "\t\t\t\t\t\t{ name: 'Sample Text', element: 'samp' },\n" +
    "\t\t\t\t\t\t{ name: 'Variable', element: 'var' },\n" +
    "\t\t\t\t\t\t{ name: 'Deleted Text', element: 'del' },\n" +
    "\t\t\t\t\t\t{ name: 'Inserted Text', element: 'ins' },\n" +
    "\t\t\t\t\t\t{ name: 'Cited Work', element: 'cite' },\n" +
    "\t\t\t\t\t\t{ name: 'Inline Quotation', element: 'q' }\n" +
    "\t\t\t\t\t],\n" +
    "\n" +
    "\t\t\t\t\ton: {\n" +
    "\t\t\t\t\t\tpluginsLoaded: configureTransformations,\n" +
    "\t\t\t\t\t\tloaded: configureHtmlWriter\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t});\n" +
    "\n" +
    "\t\t\t\t/*\n" +
    "\t\t\t\t * Add missing content transformations.\n" +
    "\t\t\t\t */\n" +
    "\t\t\t\tfunction configureTransformations( evt ) {\n" +
    "\t\t\t\t\tvar editor = evt.editor;\n" +
    "\n" +
    "\t\t\t\t\teditor.dataProcessor.htmlFilter.addRules( {\n" +
    "\t\t\t\t\t\tattributes: {\n" +
    "\t\t\t\t\t\t\tstyle: function( value, element ) {\n" +
    "\t\t\t\t\t\t\t\t// Return #RGB for background and border colors\n" +
    "\t\t\t\t\t\t\t\treturn CKEDITOR.tools.convertRgbToHex( value );\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t} );\n" +
    "\n" +
    "\t\t\t\t\t// Default automatic content transformations do not yet take care of\n" +
    "\t\t\t\t\t// align attributes on blocks, so we need to add our own transformation rules.\n" +
    "\t\t\t\t\tfunction alignToAttribute( element ) {\n" +
    "\t\t\t\t\t\tif ( element.styles[ 'text-align' ] ) {\n" +
    "\t\t\t\t\t\t\telement.attributes.align = element.styles[ 'text-align' ];\n" +
    "\t\t\t\t\t\t\tdelete element.styles[ 'text-align' ];\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t\teditor.filter.addTransformations( [\n" +
    "\t\t\t\t\t\t[ { element: 'p',\tright: alignToAttribute } ],\n" +
    "\t\t\t\t\t\t[ { element: 'h1',\tright: alignToAttribute } ],\n" +
    "\t\t\t\t\t\t[ { element: 'h2',\tright: alignToAttribute } ],\n" +
    "\t\t\t\t\t\t[ { element: 'h3',\tright: alignToAttribute } ],\n" +
    "\t\t\t\t\t\t[ { element: 'pre',\tright: alignToAttribute } ]\n" +
    "\t\t\t\t\t] );\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t/*\n" +
    "\t\t\t\t * Adjust the behavior of htmlWriter to make it output HTML like FCKeditor.\n" +
    "\t\t\t\t */\n" +
    "\t\t\t\tfunction configureHtmlWriter( evt ) {\n" +
    "\t\t\t\t\tvar editor = evt.editor,\n" +
    "\t\t\t\t\t\tdataProcessor = editor.dataProcessor;\n" +
    "\n" +
    "\t\t\t\t\t// Out self closing tags the HTML4 way, like <br>.\n" +
    "\t\t\t\t\tdataProcessor.writer.selfClosingEnd = '>';\n" +
    "\n" +
    "\t\t\t\t\t// Make output formatting behave similar to FCKeditor.\n" +
    "\t\t\t\t\tvar dtd = CKEDITOR.dtd;\n" +
    "\t\t\t\t\tfor ( var e in CKEDITOR.tools.extend( {}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent ) ) {\n" +
    "\t\t\t\t\t\tdataProcessor.writer.setRules( e, {\n" +
    "\t\t\t\t\t\t\tindent: true,\n" +
    "\t\t\t\t\t\t\tbreakBeforeOpen: true,\n" +
    "\t\t\t\t\t\t\tbreakAfterOpen: false,\n" +
    "\t\t\t\t\t\t\tbreakBeforeClose: !dtd[ e ][ '#' ],\n" +
    "\t\t\t\t\t\t\tbreakAfterClose: true\n" +
    "\t\t\t\t\t\t});\n" +
    "\t\t\t\t\t}\n" +
    "\t\t\t\t}</script> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/index.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>CKEditor Samples</title> <link rel=\"stylesheet\" href=\"sample.css\"> </head> <body> <h1 class=\"samples\"> CKEditor Samples </h1> <div class=\"warning deprecated\"> These samples are not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"twoColumns\"> <div class=\"twoColumnsLeft\"> <h2 class=\"samples\"> Basic Samples </h2> <dl class=\"samples\"> <dt><a class=\"samples\" href=\"replacebyclass.html\">Replace textarea elements by class name</a></dt> <dd>Automatic replacement of all textarea elements of a given class with a CKEditor instance.</dd> <dt><a class=\"samples\" href=\"replacebycode.html\">Replace textarea elements by code</a></dt> <dd>Replacement of textarea elements with CKEditor instances by using a JavaScript call.</dd> <dt><a class=\"samples\" href=\"jquery.html\">Create editors with jQuery</a></dt> <dd>Creating standard and inline CKEditor instances with jQuery adapter.</dd> </dl> <h2 class=\"samples\"> Basic Customization </h2> <dl class=\"samples\"> <dt><a class=\"samples\" href=\"uicolor.html\">User Interface color</a></dt> <dd>Changing CKEditor User Interface color and adding a toolbar button that lets the user set the UI color.</dd> <dt><a class=\"samples\" href=\"uilanguages.html\">User Interface languages</a></dt> <dd>Changing CKEditor User Interface language and adding a drop-down list that lets the user choose the UI language.</dd> </dl> <h2 class=\"samples\">Plugins</h2> <dl class=\"samples\"> <dt><a class=\"samples\" href=\"magicline/magicline.html\">Magicline plugin</a></dt> <dd>Using the Magicline plugin to access difficult focus spaces.</dd> <dt><a class=\"samples\" href=\"wysiwygarea/fullpage.html\">Full page support</a></dt> <dd>CKEditor inserted with a JavaScript call and used to edit the whole page from &lt;html&gt; to &lt;/html&gt;.</dd> </dl> </div> <div class=\"twoColumnsRight\"> <h2 class=\"samples\"> Inline Editing </h2> <dl class=\"samples\"> <dt><a class=\"samples\" href=\"inlineall.html\">Massive inline editor creation</a></dt> <dd>Turn all elements with <code>contentEditable = true</code> attribute into inline editors.</dd> <dt><a class=\"samples\" href=\"inlinebycode.html\">Convert element into an inline editor by code</a></dt> <dd>Conversion of DOM elements into inline CKEditor instances by using a JavaScript call.</dd> <dt><a class=\"samples\" href=\"inlinetextarea.html\">Replace textarea with inline editor</a> <span class=\"new\">New!</span></dt> <dd>A form with a textarea that is replaced by an inline editor at runtime.</dd> </dl> <h2 class=\"samples\"> Advanced Samples </h2> <dl class=\"samples\"> <dt><a class=\"samples\" href=\"datafiltering.html\">Data filtering and features activation</a> <span class=\"new\">New!</span></dt> <dd>Data filtering and automatic features activation basing on configuration.</dd> <dt><a class=\"samples\" href=\"divreplace.html\">Replace DIV elements on the fly</a></dt> <dd>Transforming a <code>div</code> element into an instance of CKEditor with a mouse click.</dd> <dt><a class=\"samples\" href=\"appendto.html\">Append editor instances</a></dt> <dd>Appending editor instances to existing DOM elements.</dd> <dt><a class=\"samples\" href=\"ajax.html\">Create and destroy editor instances for Ajax applications</a></dt> <dd>Creating and destroying CKEditor instances on the fly and saving the contents entered into the editor window.</dd> <dt><a class=\"samples\" href=\"api.html\">Basic usage of the API</a></dt> <dd>Using the CKEditor JavaScript API to interact with the editor at runtime.</dd> <dt><a class=\"samples\" href=\"xhtmlstyle.html\">XHTML-compliant style</a></dt> <dd>Configuring CKEditor to produce XHTML 1.1 compliant attributes and styles.</dd> <dt><a class=\"samples\" href=\"readonly.html\">Read-only mode</a></dt> <dd>Using the readOnly API to block introducing changes to the editor contents.</dd> <dt><a class=\"samples\" href=\"tabindex.html\">\"Tab\" key-based navigation</a></dt> <dd>Navigating among editor instances with tab key.</dd> <dt><a class=\"samples\" href=\"dialog/dialog.html\">Using the JavaScript API to customize dialog windows</a></dt> <dd>Using the dialog windows API to customize dialog windows without changing the original editor code.</dd> <dt><a class=\"samples\" href=\"enterkey/enterkey.html\">Using the &quot;Enter&quot; key in CKEditor</a></dt> <dd>Configuring the behavior of <em>Enter</em> and <em>Shift+Enter</em> keys.</dd> <dt><a class=\"samples\" href=\"htmlwriter/outputforflash.html\">Output for Flash</a></dt> <dd>Configuring CKEditor to produce HTML code that can be used with Adobe Flash.</dd> <dt><a class=\"samples\" href=\"htmlwriter/outputhtml.html\">Output HTML</a></dt> <dd>Configuring CKEditor to produce legacy HTML 4 code.</dd> <dt><a class=\"samples\" href=\"toolbar/toolbar.html\">Toolbar Configurations</a></dt> <dd>Configuring CKEditor to display full or custom toolbar layout.</dd> </dl> </div> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/inlineall.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Massive inline editing &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <script>// This code is generally not necessary, but it is here to demonstrate\n" +
    "\t\t// how to customize specific editor instances on the fly. This fits well\n" +
    "\t\t// this demo because we have editable elements (like headers) that\n" +
    "\t\t// require less features.\n" +
    "\n" +
    "\t\t// The \"instanceCreated\" event is fired for every editor instance created.\n" +
    "\t\tCKEDITOR.on( 'instanceCreated', function( event ) {\n" +
    "\t\t\tvar editor = event.editor,\n" +
    "\t\t\t\telement = editor.element;\n" +
    "\n" +
    "\t\t\t// Customize editors for headers and tag list.\n" +
    "\t\t\t// These editors don't need features like smileys, templates, iframes etc.\n" +
    "\t\t\tif ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {\n" +
    "\t\t\t\t// Customize the editor configurations on \"configLoaded\" event,\n" +
    "\t\t\t\t// which is fired after the configuration file loading and\n" +
    "\t\t\t\t// execution. This makes it possible to change the\n" +
    "\t\t\t\t// configurations before the editor initialization takes place.\n" +
    "\t\t\t\teditor.on( 'configLoaded', function() {\n" +
    "\n" +
    "\t\t\t\t\t// Remove unnecessary plugins to make the editor simpler.\n" +
    "\t\t\t\t\teditor.config.removePlugins = 'colorbutton,find,flash,font,' +\n" +
    "\t\t\t\t\t\t'forms,iframe,image,newpage,removeformat,' +\n" +
    "\t\t\t\t\t\t'smiley,specialchar,stylescombo,templates';\n" +
    "\n" +
    "\t\t\t\t\t// Rearrange the layout of the toolbar.\n" +
    "\t\t\t\t\teditor.config.toolbarGroups = [\n" +
    "\t\t\t\t\t\t{ name: 'editing',\t\tgroups: [ 'basicstyles', 'links' ] },\n" +
    "\t\t\t\t\t\t{ name: 'undo' },\n" +
    "\t\t\t\t\t\t{ name: 'clipboard',\tgroups: [ 'selection', 'clipboard' ] },\n" +
    "\t\t\t\t\t\t{ name: 'about' }\n" +
    "\t\t\t\t\t];\n" +
    "\t\t\t\t});\n" +
    "\t\t\t}\n" +
    "\t\t});</script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>/* The following styles are just to make the page look nice. */\n" +
    "\n" +
    "\t\t/* Workaround to show Arial Black in Firefox. */\n" +
    "\t\t@font-face\n" +
    "\t\t{\n" +
    "\t\t\tfont-family: 'arial-black';\n" +
    "\t\t\tsrc: local('Arial Black');\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t*[contenteditable=\"true\"]\n" +
    "\t\t{\n" +
    "\t\t\tpadding: 10px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#container\n" +
    "\t\t{\n" +
    "\t\t\twidth: 960px;\n" +
    "\t\t\tmargin: 30px auto 0;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#header\n" +
    "\t\t{\n" +
    "\t\t\toverflow: hidden;\n" +
    "\t\t\tpadding: 0 0 30px;\n" +
    "\t\t\tborder-bottom: 5px solid #05B2D2;\n" +
    "\t\t\tposition: relative;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerLeft,\n" +
    "\t\t#headerRight\n" +
    "\t\t{\n" +
    "\t\t\twidth: 49%;\n" +
    "\t\t\toverflow: hidden;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerLeft\n" +
    "\t\t{\n" +
    "\t\t\tfloat: left;\n" +
    "\t\t\tpadding: 10px 1px 1px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerLeft h2,\n" +
    "\t\t#headerLeft h3\n" +
    "\t\t{\n" +
    "\t\t\ttext-align: right;\n" +
    "\t\t\tmargin: 0;\n" +
    "\t\t\toverflow: hidden;\n" +
    "\t\t\tfont-weight: normal;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerLeft h2\n" +
    "\t\t{\n" +
    "\t\t\tfont-family: \"Arial Black\",arial-black;\n" +
    "\t\t\tfont-size: 4.6em;\n" +
    "\t\t\tline-height: 1.1;\n" +
    "\t\t\ttext-transform: uppercase;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerLeft h3\n" +
    "\t\t{\n" +
    "\t\t\tfont-size: 2.3em;\n" +
    "\t\t\tline-height: 1.1;\n" +
    "\t\t\tmargin: .2em 0 0;\n" +
    "\t\t\tcolor: #666;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerRight\n" +
    "\t\t{\n" +
    "\t\t\tfloat: right;\n" +
    "\t\t\tpadding: 1px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerRight p\n" +
    "\t\t{\n" +
    "\t\t\tline-height: 1.8;\n" +
    "\t\t\ttext-align: justify;\n" +
    "\t\t\tmargin: 0;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerRight p + p\n" +
    "\t\t{\n" +
    "\t\t\tmargin-top: 20px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#headerRight > div\n" +
    "\t\t{\n" +
    "\t\t\tpadding: 20px;\n" +
    "\t\t\tmargin: 0 0 0 30px;\n" +
    "\t\t\tfont-size: 1.4em;\n" +
    "\t\t\tcolor: #666;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns\n" +
    "\t\t{\n" +
    "\t\t\tcolor: #333;\n" +
    "\t\t\toverflow: hidden;\n" +
    "\t\t\tpadding: 20px 0;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns > div\n" +
    "\t\t{\n" +
    "\t\t\tfloat: left;\n" +
    "\t\t\twidth: 33.3%;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns #column1 > div\n" +
    "\t\t{\n" +
    "\t\t\tmargin-left: 1px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns #column3 > div\n" +
    "\t\t{\n" +
    "\t\t\tmargin-right: 1px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns > div > div\n" +
    "\t\t{\n" +
    "\t\t\tmargin: 0px 10px;\n" +
    "\t\t\tpadding: 10px 20px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#columns blockquote\n" +
    "\t\t{\n" +
    "\t\t\tmargin-left: 15px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#tagLine\n" +
    "\t\t{\n" +
    "\t\t\tborder-top: 5px solid #05B2D2;\n" +
    "\t\t\tpadding-top: 20px;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t#taglist {\n" +
    "\t\t\tdisplay: inline-block;\n" +
    "\t\t\tmargin-left: 20px;\n" +
    "\t\t\tfont-weight: bold;\n" +
    "\t\t\tmargin: 0 0 0 20px;\n" +
    "\t\t}</style> </head> <body> <div> <h1 class=\"samples\"><a href=\"index.html\">CKEditor Samples</a> &raquo; Massive inline editing</h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/inline.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p>This sample page demonstrates the inline editing feature - CKEditor instances will be created automatically from page elements with <strong>contentEditable</strong> attribute set to value <strong>true</strong>:</p> <pre class=\"samples\">&lt;div <strong>contenteditable=\"true</strong>\" &gt; ... &lt;/div&gt;</pre> <p>Click inside of any element below to start editing.</p> </div> </div> <div id=\"container\"> <div id=\"header\"> <div id=\"headerLeft\"> <h2 id=\"sampleTitle\" contenteditable=\"true\"> CKEditor<br> Goes Inline! </h2> <h3 contenteditable=\"true\"> Lorem ipsum dolor sit amet dolor duis blandit vestibulum faucibus a, tortor. </h3> </div> <div id=\"headerRight\"> <div contenteditable=\"true\"> <p> Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. </p> <p> Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac. </p> </div> </div> </div> <div id=\"columns\"> <div id=\"column1\"> <div contenteditable=\"true\"> <h3> Fusce vitae porttitor </h3> <p> <strong> Lorem ipsum dolor sit amet dolor. Duis blandit vestibulum faucibus a, tortor. </strong> </p> <p> Proin nunc justo felis mollis tincidunt, risus risus pede, posuere cubilia Curae, Nullam euismod, enim. Etiam nibh ultricies dolor ac dignissim erat volutpat. Vivamus fermentum <a href=\"https://ckeditor.com/\">nisl nulla sem in</a> metus. Maecenas wisi. Donec nec erat volutpat. </p> <blockquote> <p> Fusce vitae porttitor a, euismod convallis nisl, blandit risus tortor, pretium. Vehicula vitae, imperdiet vel, ornare enim vel sodales rutrum </p> </blockquote> <blockquote> <p> Libero nunc, rhoncus ante ipsum non ipsum. Nunc eleifend pede turpis id sollicitudin fringilla. Phasellus ultrices, velit ac arcu. </p> </blockquote> <p>Pellentesque nunc. Donec suscipit erat. Pellentesque habitant morbi tristique ullamcorper.</p> <p><s>Mauris mattis feugiat lectus nec mauris. Nullam vitae ante.</s></p> </div> </div> <div id=\"column2\"> <div contenteditable=\"true\"> <h3> Integer condimentum sit amet </h3> <p> <strong>Aenean nonummy a, mattis varius. Cras aliquet.</strong> Praesent <a href=\"https://ckeditor.com/\">magna non mattis ac, rhoncus nunc</a>, rhoncus eget, cursus pulvinar mollis.</p> <p>Proin id nibh. Sed eu libero posuere sed, lectus. Phasellus dui gravida gravida feugiat mattis ac, felis.</p> <p>Integer condimentum sit amet, tempor elit odio, a dolor non ante at sapien. Sed ac lectus. Nulla ligula quis eleifend mi, id leo velit pede cursus arcu id nulla ac lectus. Phasellus vestibulum. Nunc viverra enim quis diam.</p> </div> <div contenteditable=\"true\"> <h3> Praesent wisi accumsan sit amet nibh </h3> <p>Donec ullamcorper, risus tortor, pretium porttitor. Morbi quam quis lectus non leo.</p> <p style=\"margin-left: 40px\">Integer faucibus scelerisque. Proin faucibus at, aliquet vulputate, odio at eros. Fusce <a href=\"https://ckeditor.com/\">gravida, erat vitae augue</a>. Fusce urna fringilla gravida.</p> <p>In hac habitasse platea dictumst. Praesent wisi accumsan sit amet nibh. Maecenas orci luctus a, lacinia quam sem, posuere commodo, odio condimentum tempor, pede semper risus. Suspendisse pede. In hac habitasse platea dictumst. Nam sed laoreet sit amet erat. Integer.</p> </div> </div> <div id=\"column3\"> <div contenteditable=\"true\"> <p> <img src=\"assets/inlineall/logo.png\" alt=\"CKEditor logo\" style=\"float:left\"> </p> <p>Quisque justo neque, mattis sed, fermentum ultrices <strong>posuere cubilia Curae</strong>, Vestibulum elit metus, quis placerat ut, lectus. Ut sagittis, nunc libero, egestas consequat lobortis velit rutrum ut, faucibus turpis. Fusce porttitor, nulla quis turpis. Nullam laoreet vel, consectetuer tellus suscipit ultricies, hendrerit wisi. Donec odio nec velit ac nunc sit amet, accumsan cursus aliquet. Vestibulum ante sit amet sagittis mi.</p> <h3> Nullam laoreet vel consectetuer tellus suscipit </h3> <ul> <li>Ut sagittis, nunc libero, egestas consequat lobortis velit rutrum ut, faucibus turpis.</li> <li>Fusce porttitor, nulla quis turpis. Nullam laoreet vel, consectetuer tellus suscipit ultricies, hendrerit wisi.</li> <li>Mauris eget tellus. Donec non felis. Nam eget dolor. Vestibulum enim. Donec.</li> </ul> <p>Quisque justo neque, mattis sed, <a href=\"https://ckeditor.com/\">fermentum ultrices posuere cubilia</a> Curae, Vestibulum elit metus, quis placerat ut, lectus.</p> <p>Nullam laoreet vel, consectetuer tellus suscipit ultricies, hendrerit wisi. Ut sagittis, nunc libero, egestas consequat lobortis velit rutrum ut, faucibus turpis. Fusce porttitor, nulla quis turpis.</p> <p>Donec odio nec velit ac nunc sit amet, accumsan cursus aliquet. Vestibulum ante sit amet sagittis mi. Sed in nonummy faucibus turpis. Mauris eget tellus. Donec non felis. Nam eget dolor. Vestibulum enim. Donec.</p> </div> </div> </div> <div id=\"tagLine\"> Tags of this article: <p id=\"taglist\" contenteditable=\"true\"> inline, editing, floating, CKEditor </p> </div> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\"> https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/inlinebycode.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Inline Editing by Code &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>#editable\n" +
    "\t\t{\n" +
    "\t\t\tpadding: 10px;\n" +
    "\t\t\tfloat: left;\n" +
    "\t\t}</style> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Inline Editing by Code </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/inline.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to create an inline editor instance of CKEditor. It is created with a JavaScript call using the following code: </p> <pre class=\"samples\">\n" +
    "// This property tells CKEditor to not activate every element with contenteditable=true element.\n" +
    "CKEDITOR.disableAutoInline = true;\n" +
    "\n" +
    "var editor = CKEDITOR.inline( document.getElementById( 'editable' ) );\n" +
    "</pre> <p> Note that <code>editable</code> in the code above is the <code>id</code> attribute of the <code>&lt;div&gt;</code> element to be converted into an inline instance. </p> </div> <div id=\"editable\" contenteditable=\"true\"> <h1><img alt=\"Saturn V carrying Apollo 11\" class=\"right\" src=\"assets/sample.jpg\"> Apollo 11</h1> <p><b>Apollo 11</b> was the spaceflight that landed the first humans, Americans <a href=\"http://en.wikipedia.org/wiki/Neil_Armstrong\" title=\"Neil Armstrong\">Neil Armstrong</a> and <a href=\"http://en.wikipedia.org/wiki/Buzz_Aldrin\" title=\"Buzz Aldrin\">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p> <p>Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href=\"http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)\" title=\"Michael Collins (astronaut)\">Michael Collins</a>, piloted the <a href=\"http://en.wikipedia.org/wiki/Apollo_Command/Service_Module\" title=\"Apollo Command/Service Module\">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p> <h2>Broadcasting and <em>quotes</em> <a id=\"quotes\" name=\"quotes\"></a></h2> <p>Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:</p> <blockquote> <p>One small step for [a] man, one giant leap for mankind.</p> </blockquote> <p>Apollo 11 effectively ended the <a href=\"http://en.wikipedia.org/wiki/Space_Race\" title=\"Space Race\">Space Race</a> and fulfilled a national goal proposed in 1961 by the late U.S. President <a href=\"http://en.wikipedia.org/wiki/John_F._Kennedy\" title=\"John F. Kennedy\">John F. Kennedy</a> in a speech before the United States Congress:</p> <blockquote> <p>[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.</p> </blockquote> <h2>Technical details <a id=\"tech-details\" name=\"tech-details\"></a></h2> <table align=\"right\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse;margin:10px 0 10px 15px\"> <caption><strong>Mission crew</strong></caption> <thead> <tr> <th scope=\"col\">Position</th> <th scope=\"col\">Astronaut</th> </tr> </thead> <tbody> <tr> <td>Commander</td> <td>Neil A. Armstrong</td> </tr> <tr> <td>Command Module Pilot</td> <td>Michael Collins</td> </tr> <tr> <td>Lunar Module Pilot</td> <td>Edwin &quot;Buzz&quot; E. Aldrin, Jr.</td> </tr> </tbody> </table> <p>Launched by a <strong>Saturn V</strong> rocket from <a href=\"http://en.wikipedia.org/wiki/Kennedy_Space_Center\" title=\"Kennedy Space Center\">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href=\"http://en.wikipedia.org/wiki/NASA\" title=\"NASA\">NASA</a>&#39;s Apollo program. The Apollo spacecraft had three parts:</p> <ol> <li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li> <li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li> <li><strong>Lunar Module</strong> for landing on the Moon.</li> </ol> <p>After being sent to the Moon by the Saturn V&#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href=\"http://en.wikipedia.org/wiki/Mare_Tranquillitatis\" title=\"Mare Tranquillitatis\">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href=\"http://en.wikipedia.org/wiki/Pacific_Ocean\" title=\"Pacific Ocean\">Pacific Ocean</a> on July 24.</p> <hr> <p style=\"text-align: right\"><small>Source: <a href=\"http://en.wikipedia.org/wiki/Apollo_11\">Wikipedia.org</a></small></p> </div> <script>// We need to turn off the automatic editor creation first.\n" +
    "\t\tCKEDITOR.disableAutoInline = true;\n" +
    "\n" +
    "\t\tvar editor = CKEDITOR.inline( 'editable' );</script> <div id=\"footer\"> <hr> <p contenteditable=\"true\"> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\"> https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/inlinetextarea.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Replace Textarea with Inline Editor &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>/* Style the CKEditor element to look like a textfield */\n" +
    "\t\t.cke_textarea_inline\n" +
    "\t\t{\n" +
    "\t\t\tpadding: 10px;\n" +
    "\t\t\theight: 200px;\n" +
    "\t\t\toverflow: auto;\n" +
    "\n" +
    "\t\t\tborder: 1px solid gray;\n" +
    "\t\t\t-webkit-appearance: textfield;\n" +
    "\t\t}</style> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Replace Textarea with Inline Editor </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/inline.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> You can also create an inline editor from a <code>textarea</code> element. In this case the <code>textarea</code> will be replaced by a <code>div</code> element with inline editing enabled. </p> <pre class=\"samples\">\n" +
    "// \"article-body\" is the name of a textarea element.\n" +
    "var editor = CKEDITOR.inline( 'article-body' );\n" +
    "</pre> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <h2>This is a sample form with some fields</h2> <p> Title:<br> <input type=\"text\" name=\"title\" value=\"Sample Form\"></p> <p> Article Body (Textarea converted to CKEditor):<br> <textarea name=\"article-body\" style=\"height: 200px\">\n" +
    "\t\t\t\t&lt;h2&gt;Technical details &lt;a id=\"tech-details\" name=\"tech-details\"&gt;&lt;/a&gt;&lt;/h2&gt;\n" +
    "\n" +
    "\t\t\t\t&lt;table align=\"right\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse;margin:10px 0 10px 15px;\"&gt;\n" +
    "\t\t\t\t\t&lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt;\n" +
    "\t\t\t\t\t&lt;thead&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;th scope=\"col\"&gt;Position&lt;/th&gt;\n" +
    "\t\t\t\t\t\t&lt;th scope=\"col\"&gt;Astronaut&lt;/th&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;/thead&gt;\n" +
    "\t\t\t\t\t&lt;tbody&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Commander&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Neil A. Armstrong&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Command Module Pilot&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Michael Collins&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Lunar Module Pilot&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;Edwin &quot;Buzz&quot; E. Aldrin, Jr.&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;/tbody&gt;\n" +
    "\t\t\t\t&lt;/table&gt;\n" +
    "\n" +
    "\t\t\t\t&lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=\"http://en.wikipedia.org/wiki/Kennedy_Space_Center\" title=\"Kennedy Space Center\"&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=\"http://en.wikipedia.org/wiki/NASA\" title=\"NASA\"&gt;NASA&lt;/a&gt;&#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t\t&lt;ol&gt;\n" +
    "\t\t\t\t\t&lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt;\n" +
    "\t\t\t\t\t&lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt;\n" +
    "\t\t\t\t\t&lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt;\n" +
    "\t\t\t\t&lt;/ol&gt;\n" +
    "\n" +
    "\t\t\t\t&lt;p&gt;After being sent to the Moon by the Saturn V&#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=\"http://en.wikipedia.org/wiki/Mare_Tranquillitatis\" title=\"Mare Tranquillitatis\"&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=\"http://en.wikipedia.org/wiki/Pacific_Ocean\" title=\"Pacific Ocean\"&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t\t&lt;hr /&gt;\n" +
    "\t\t\t\t&lt;p style=\"text-align: right;\"&gt;&lt;small&gt;Source: &lt;a href=\"http://en.wikipedia.org/wiki/Apollo_11\"&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t\t</textarea> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <script>CKEDITOR.inline( 'article-body' );</script> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\"> https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/jquery.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>jQuery Adapter &mdash; CKEditor Sample</title> <script src=\"http://code.jquery.com/jquery-1.11.0.min.js\"></script> <script src=\"../../ckeditor.js\"></script> <script src=\"../../adapters/jquery.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>#editable\n" +
    "\t\t{\n" +
    "\t\t\tpadding: 10px;\n" +
    "\t\t\tfloat: left;\n" +
    "\t\t}</style> <script>CKEDITOR.disableAutoInline = true;\n" +
    "\n" +
    "\t\t$( document ).ready( function() {\n" +
    "\t\t\t$( '#editor1' ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.\n" +
    "\t\t\t$( '#editable' ).ckeditor(); // Use CKEDITOR.inline().\n" +
    "\t\t} );\n" +
    "\n" +
    "\t\tfunction setValue() {\n" +
    "\t\t\t$( '#editor1' ).val( $( 'input#val' ).val() );\n" +
    "\t\t}</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\" id=\"a-test\">CKEditor Samples</a> &raquo; Create Editors with jQuery </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <form action=\"sample_posteddata.php\" method=\"post\"> <div class=\"description\"> <p> This sample shows how to use the <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_jquery\">jQuery adapter</a>. Note that you have to include both CKEditor and jQuery scripts before including the adapter. </p> <pre class=\"samples\">\n" +
    "&lt;script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"&gt;&lt;/script&gt;\n" +
    "&lt;script src=\"/ckedit../../ckeditor.js\"&gt;&lt;/script&gt;\n" +
    "&lt;script src=\"/ckeditor/adapters/jquery.js\"&gt;&lt;/script&gt;\n" +
    "</pre> <p>Then you can replace HTML elements with a CKEditor instance using the <code>ckeditor()</code> method.</p> <pre class=\"samples\">\n" +
    "$( document ).ready( function() {\n" +
    "\t$( 'textarea#editor1' ).ckeditor();\n" +
    "} );\n" +
    "</pre> </div> <h2 class=\"samples\">Inline Example</h2> <div id=\"editable\" contenteditable=\"true\"> <p><img alt=\"Saturn V carrying Apollo 11\" class=\"right\" src=\"assets/sample.jpg\"><b>Apollo 11</b> was the spaceflight that landed the first humans, Americans <a href=\"http://en.wikipedia.org/wiki/Neil_Armstrong\" title=\"Neil Armstrong\">Neil Armstrong</a> and <a href=\"http://en.wikipedia.org/wiki/Buzz_Aldrin\" title=\"Buzz Aldrin\">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p> <p>Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href=\"http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)\" title=\"Michael Collins (astronaut)\">Michael Collins</a>, piloted the <a href=\"http://en.wikipedia.org/wiki/Apollo_Command/Service_Module\" title=\"Apollo Command/Service Module\">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth. </p><p>Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:</p> <blockquote><p>One small step for [a] man, one giant leap for mankind.</p></blockquote> <p>Apollo 11 effectively ended the <a href=\"http://en.wikipedia.org/wiki/Space_Race\" title=\"Space Race\">Space Race</a> and fulfilled a national goal proposed in 1961 by the late U.S. President <a href=\"http://en.wikipedia.org/wiki/John_F._Kennedy\" title=\"John F. Kennedy\">John F. Kennedy</a> in a speech before the United States Congress:</p> <blockquote><p>[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.</p></blockquote> </div> <br style=\"clear: both\"> <h2 class=\"samples\">Classic (iframe-based) Example</h2> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t&lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <p style=\"overflow: hidden\"> <input style=\"float: left\" type=\"submit\" value=\"Submit\"> <span style=\"float: right\"> <input type=\"text\" id=\"val\" value=\"I'm using jQuery val()!\" size=\"30\"> <input onclick=\"setValue()\" type=\"button\" value=\"Set value\"> </span> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/magicline/magicline.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Using Magicline plugin &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"../../../samples/old/sample.css\"> <meta name=\"ckeditor-sample-name\" content=\"Magicline plugin\"> <meta name=\"ckeditor-sample-group\" content=\"Plugins\"> <meta name=\"ckeditor-sample-description\" content=\"Using the Magicline plugin to access difficult focus spaces.\"> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Using Magicline plugin </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/magicline.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows the advantages of <strong>Magicline</strong> plugin which is to enhance the editing process. Thanks to this plugin, a number of difficult focus spaces which are inaccessible due to browser issues can now be focused. </p> <p> <strong>Magicline</strong> plugin shows a red line with a handler which, when clicked, inserts a paragraph and allows typing. To see this, focus an editor and move your mouse above the focus space you want to access. The plugin is enabled by default so no additional configuration is necessary. </p> </div> <div> <label for=\"editor1\"> Editor 1: </label> <div class=\"description\"> <p> This editor uses a default <strong>Magicline</strong> setup. </p> </div> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 100%; &quot;&gt;\n" +
    "\t\t\t\t&lt;tbody&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;This table&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;is the&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;very first&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;element of the document.&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;We are still&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;able to acces&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;the space before it.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;\n" +
    "\t\t\t\t\t\t&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 100%; &quot;&gt;\n" +
    "\t\t\t\t\t\t\t&lt;tbody&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t\t\t\t&lt;td&gt;This table is inside of a cell of another table.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t\t\t\t&lt;td&gt;We can type&amp;nbsp;either before or after it though.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t\t\t&lt;/tbody&gt;\n" +
    "\t\t\t\t\t\t&lt;/table&gt;\n" +
    "\t\t\t\t\t\t&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t&lt;/tbody&gt;\n" +
    "\t\t\t&lt;/table&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Two succesive horizontal lines (&lt;tt&gt;HR&lt;/tt&gt; tags). We can access the space in between:&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;hr /&gt;\n" +
    "\t\t\t&lt;hr /&gt;\n" +
    "\t\t\t&lt;ol&gt;\n" +
    "\t\t\t\t&lt;li&gt;This numbered list...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...is a neighbour of a horizontal line...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...and another list.&lt;/li&gt;\n" +
    "\t\t\t&lt;/ol&gt;\n" +
    "\n" +
    "\t\t\t&lt;ul&gt;\n" +
    "\t\t\t\t&lt;li&gt;We can type between the lists...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...thanks to &lt;strong&gt;Magicline&lt;/strong&gt;.&lt;/li&gt;\n" +
    "\t\t\t&lt;/ul&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Lorem ipsum dolor sit amet dui. Morbi vel turpis. Nullam et leo. Etiam rutrum, urna tellus dui vel tincidunt mattis egestas, justo fringilla vel, massa. Phasellus.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Quisque iaculis, dui lectus varius vitae, tortor. Proin lacus. Pellentesque ac lacus. Aenean nonummy commodo nec, pede. Etiam blandit risus elit.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Ut pretium. Vestibulum rutrum in, adipiscing elit. Sed in quam in purus sem vitae pede. Pellentesque bibendum, urna sem vel risus. Vivamus posuere metus. Aliquam gravida iaculis nisl. Nam enim. Aliquam erat ac lacus tellus ac felis.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;div style=&quot;border: 2px dashed green; background: #ddd; text-align: center;&quot;&gt;\n" +
    "\t\t\t&lt;p&gt;This text is wrapped in a&amp;nbsp;&lt;tt&gt;DIV&lt;/tt&gt;&amp;nbsp;element. We can type after this element though.&lt;/p&gt;\n" +
    "\t\t\t&lt;/div&gt;\n" +
    "\t\t</textarea> <script>// This call can be placed at any point after the\n" +
    "\t\t\t// <textarea>, or inside a <head><script> in a\n" +
    "\t\t\t// window.onload event handler.\n" +
    "\n" +
    "\t\t\tCKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\textraPlugins: 'magicline',\t// Ensure that magicline plugin, which is required for this sample, is loaded.\n" +
    "\t\t\t\tallowedContent: true\t\t// Switch off the ACF, so very complex content created to\n" +
    "\t\t\t\t\t\t\t\t\t\t\t// show magicline's power isn't filtered.\n" +
    "\t\t\t} );</script> </div> <br> <div> <label for=\"editor2\"> Editor 2: </label> <div class=\"description\"> <p> This editor is using a blue line. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( 'editor2', {\n" +
    "\tmagicline_color: 'blue'\n" +
    "});</pre> </div> <textarea cols=\"80\" id=\"editor2\" name=\"editor2\" rows=\"10\">\n" +
    "\t\t\t&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 100%; &quot;&gt;\n" +
    "\t\t\t\t&lt;tbody&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;This table&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;is the&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;very first&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;element of the document.&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;We are still&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;able to acces&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;the space before it.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t&lt;td&gt;\n" +
    "\t\t\t\t\t\t&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 100%; &quot;&gt;\n" +
    "\t\t\t\t\t\t\t&lt;tbody&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t\t\t\t&lt;td&gt;This table is inside of a cell of another table.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;tr&gt;\n" +
    "\t\t\t\t\t\t\t\t\t&lt;td&gt;We can type&amp;nbsp;either before or after it though.&lt;/td&gt;\n" +
    "\t\t\t\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t\t\t\t&lt;/tbody&gt;\n" +
    "\t\t\t\t\t\t&lt;/table&gt;\n" +
    "\t\t\t\t\t\t&lt;/td&gt;\n" +
    "\t\t\t\t\t&lt;/tr&gt;\n" +
    "\t\t\t\t&lt;/tbody&gt;\n" +
    "\t\t\t&lt;/table&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Two succesive horizontal lines (&lt;tt&gt;HR&lt;/tt&gt; tags). We can access the space in between:&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;hr /&gt;\n" +
    "\t\t\t&lt;hr /&gt;\n" +
    "\t\t\t&lt;ol&gt;\n" +
    "\t\t\t\t&lt;li&gt;This numbered list...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...is a neighbour of a horizontal line...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...and another list.&lt;/li&gt;\n" +
    "\t\t\t&lt;/ol&gt;\n" +
    "\n" +
    "\t\t\t&lt;ul&gt;\n" +
    "\t\t\t\t&lt;li&gt;We can type between the lists...&lt;/li&gt;\n" +
    "\t\t\t\t&lt;li&gt;...thanks to &lt;strong&gt;Magicline&lt;/strong&gt;.&lt;/li&gt;\n" +
    "\t\t\t&lt;/ul&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Lorem ipsum dolor sit amet dui. Morbi vel turpis. Nullam et leo. Etiam rutrum, urna tellus dui vel tincidunt mattis egestas, justo fringilla vel, massa. Phasellus.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Quisque iaculis, dui lectus varius vitae, tortor. Proin lacus. Pellentesque ac lacus. Aenean nonummy commodo nec, pede. Etiam blandit risus elit.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;p&gt;Ut pretium. Vestibulum rutrum in, adipiscing elit. Sed in quam in purus sem vitae pede. Pellentesque bibendum, urna sem vel risus. Vivamus posuere metus. Aliquam gravida iaculis nisl. Nam enim. Aliquam erat ac lacus tellus ac felis.&lt;/p&gt;\n" +
    "\n" +
    "\t\t\t&lt;div style=&quot;border: 2px dashed green; background: #ddd; text-align: center;&quot;&gt;\n" +
    "\t\t\t&lt;p&gt;This text is wrapped in a&amp;nbsp;&lt;tt&gt;DIV&lt;/tt&gt;&amp;nbsp;element. We can type after this element though.&lt;/p&gt;\n" +
    "\t\t\t&lt;/div&gt;\n" +
    "\t\t</textarea> <script>// This call can be placed at any point after the\n" +
    "\t\t\t// <textarea>, or inside a <head><script> in a\n" +
    "\t\t\t// window.onload event handler.\n" +
    "\n" +
    "\t\t\tCKEDITOR.replace( 'editor2', {\n" +
    "\t\t\t\textraPlugins: 'magicline',\t// Ensure that magicline plugin, which is required for this sample, is loaded.\n" +
    "\t\t\t\tmagicline_color: 'blue',\t// Blue line\n" +
    "\t\t\t\tallowedContent: true\t\t// Switch off the ACF, so very complex content created to\n" +
    "\t\t\t\t\t\t\t\t\t\t\t// show magicline's power isn't filtered.\n" +
    "\t\t\t});</script> </div> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/readonly.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Using the CKEditor Read-Only API &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> <script>var editor;\n" +
    "\n" +
    "\t\t// The instanceReady event is fired, when an instance of CKEditor has finished\n" +
    "\t\t// its initialization.\n" +
    "\t\tCKEDITOR.on( 'instanceReady', function( ev ) {\n" +
    "\t\t\teditor = ev.editor;\n" +
    "\n" +
    "\t\t\t// Show this \"on\" button.\n" +
    "\t\t\tdocument.getElementById( 'readOnlyOn' ).style.display = '';\n" +
    "\n" +
    "\t\t\t// Event fired when the readOnly property changes.\n" +
    "\t\t\teditor.on( 'readOnly', function() {\n" +
    "\t\t\t\tdocument.getElementById( 'readOnlyOn' ).style.display = this.readOnly ? 'none' : '';\n" +
    "\t\t\t\tdocument.getElementById( 'readOnlyOff' ).style.display = this.readOnly ? '' : 'none';\n" +
    "\t\t\t});\n" +
    "\t\t});\n" +
    "\n" +
    "\t\tfunction toggleReadOnly( isReadOnly ) {\n" +
    "\t\t\t// Change the read-only state of the editor.\n" +
    "\t\t\t// https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-setReadOnly\n" +
    "\t\t\teditor.setReadOnly( isReadOnly );\n" +
    "\t\t}</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Using the CKEditor Read-Only API </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/readonly.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to use the <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.editor-method-setReadOnly\">setReadOnly</a></code> API to put editor into the read-only state that makes it impossible for users to change the editor contents. </p> <p> For details on how to create this setup check the source code of this sample page. </p> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <p> <textarea class=\"ckeditor\" id=\"editor1\" name=\"editor1\" cols=\"100\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> </p> <p> <input id=\"readOnlyOn\" onclick=\"toggleReadOnly()\" type=\"button\" value=\"Make it read-only\" style=\"display:none\"> <input id=\"readOnlyOff\" onclick=\"toggleReadOnly( false )\" type=\"button\" value=\"Make it editable again\" style=\"display:none\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/replacebyclass.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Replace Textareas by Class Name &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Replace Textarea Elements by Class Name </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"https://sdk.ckeditor.com/\">brand new samples in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to automatically replace all <code>&lt;textarea&gt;</code> elements of a given class with a CKEditor instance. </p> <p> To replace a <code>&lt;textarea&gt;</code> element, simply assign it the <code>ckeditor</code> class, as in the code below: </p> <pre class=\"samples\">\n" +
    "&lt;textarea <strong>class=\"ckeditor</strong>\" name=\"editor1\"&gt;&lt;/textarea&gt;\n" +
    "</pre> <p> Note that other <code>&lt;textarea&gt;</code> attributes (like <code>id</code> or <code>name</code>) need to be adjusted to your document. </p> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <p> <label for=\"editor1\"> Editor 1: </label> <textarea class=\"ckeditor\" cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t\t</textarea> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/replacebycode.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Replace Textarea by Code &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Replace Textarea Elements Using JavaScript Code </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/classic.html\">brand new version in CKEditor SDK</a>. </div> <form action=\"sample_posteddata.php\" method=\"post\"> <div class=\"description\"> <p> This editor is using an <code>&lt;iframe&gt;</code> element-based editing area, provided by the <strong>Wysiwygarea</strong> plugin. </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>' )\n" +
    "</pre> </div> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img alt=&quot;Saturn V carrying Apollo 11&quot; class=&quot;right&quot; src=&quot;assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>// This call can be placed at any point after the\n" +
    "\t\t\t// <textarea>, or inside a <head><script> in a\n" +
    "\t\t\t// window.onload event handler.\n" +
    "\n" +
    "\t\t\t// Replace the <textarea id=\"editor\"> with an CKEditor\n" +
    "\t\t\t// instance, using default configurations.\n" +
    "\n" +
    "\t\t\tCKEDITOR.replace( 'editor1' );</script> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/tabindex.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>TAB Key-Based Navigation &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> <style>.cke_focused,\n" +
    "\t\t.cke_editable.cke_focused\n" +
    "\t\t{\n" +
    "\t\t\toutline: 3px dotted blue !important;\n" +
    "\t\t\t*border: 3px dotted blue !important;\t/* For IE7 */\n" +
    "\t\t}</style> <script>CKEDITOR.on( 'instanceReady', function( evt ) {\n" +
    "\t\t\tvar editor = evt.editor;\n" +
    "\t\t\teditor.setData( 'This editor has it\\'s tabIndex set to <strong>' + editor.tabIndex + '</strong>' );\n" +
    "\n" +
    "\t\t\t// Apply focus class name.\n" +
    "\t\t\teditor.on( 'focus', function() {\n" +
    "\t\t\t\teditor.container.addClass( 'cke_focused' );\n" +
    "\t\t\t});\n" +
    "\t\t\teditor.on( 'blur', function() {\n" +
    "\t\t\t\teditor.container.removeClass( 'cke_focused' );\n" +
    "\t\t\t});\n" +
    "\n" +
    "\t\t\t// Put startup focus on the first editor in tab order.\n" +
    "\t\t\tif ( editor.tabIndex == 1 )\n" +
    "\t\t\t\teditor.focus();\n" +
    "\t\t});</script> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; TAB Key-Based Navigation </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/tabindex.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how tab key navigation among editor instances is affected by the <code>tabIndex</code> attribute from the original page element. Use TAB key to move between the editors. </p> </div> <p> <textarea class=\"ckeditor\" cols=\"80\" id=\"editor4\" rows=\"10\" tabindex=\"1\"></textarea> </p> <div class=\"ckeditor\" contenteditable=\"true\" id=\"editor1\" tabindex=\"4\"></div> <p> <textarea class=\"ckeditor\" cols=\"80\" id=\"editor2\" rows=\"10\" tabindex=\"2\"></textarea> </p> <p> <textarea class=\"ckeditor\" cols=\"80\" id=\"editor3\" rows=\"10\" tabindex=\"3\"></textarea> </p> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/toolbar/toolbar.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Toolbar Configuration &mdash; CKEditor Sample</title> <meta name=\"ckeditor-sample-name\" content=\"Toolbar Configurations\"> <meta name=\"ckeditor-sample-group\" content=\"Advanced Samples\"> <meta name=\"ckeditor-sample-description\" content=\"Configuring CKEditor to display full or custom toolbar layout.\"> <script src=\"../../../ckeditor.js\"></script> <link href=\"../../../samples/old/sample.css\" rel=\"stylesheet\"> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Toolbar Configuration </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out the <a href=\"../../../samples/toolbarconfigurator/index.html#basic\">brand new CKEditor Toolbar Configurator</a>. </div> <div class=\"description\"> <p> This sample page demonstrates editor with loaded <a href=\"#fullToolbar\">full toolbar</a> (all registered buttons) and, if current editor's configuration modifies default settings, also editor with <a href=\"#currentToolbar\">modified toolbar</a>. </p> <p>Since CKEditor 4 there are two ways to configure toolbar buttons.</p> <h2 class=\"samples\">By <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-toolbar\">config.toolbar</a></h2> <p> You can explicitly define which buttons are displayed in which groups and in which order. This is the more precise setting, but less flexible. If newly added plugin adds its own button you'll have to add it manually to your <code>config.toolbar</code> setting as well. </p> <p>To add a CKEditor instance with custom toolbar setting, insert the following JavaScript call to your code:</p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( <em>'textarea_id'</em>, {\n" +
    "\t<strong>toolbar:</strong> [\n" +
    "\t\t{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },\t// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.\n" +
    "\t\t[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],\t\t\t// Defines toolbar group without name.\n" +
    "\t\t'/',\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t// Line break - next group will be placed in new line.\n" +
    "\t\t{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }\n" +
    "\t]\n" +
    "});</pre> <h2 class=\"samples\">By <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-toolbarGroups\">config.toolbarGroups</a></h2> <p> You can define which groups of buttons (like e.g. <code>basicstyles</code>, <code>clipboard</code> and <code>forms</code>) are displayed and in which order. Registered buttons are associated with toolbar groups by <code>toolbar</code> property in their definition. This setting's advantage is that you don't have to modify toolbar configuration when adding/removing plugins which register their own buttons. </p> <p>To add a CKEditor instance with custom toolbar groups setting, insert the following JavaScript call to your code:</p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( <em>'textarea_id'</em>, {\n" +
    "\t<strong>toolbarGroups:</strong> [\n" +
    "\t\t{ name: 'document',\t   groups: [ 'mode', 'document' ] },\t\t\t// Displays document group with its two subgroups.\n" +
    " \t\t{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },\t\t\t// Group's name will be used to create voice label.\n" +
    " \t\t'/',\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t// Line break - next group will be placed in new line.\n" +
    " \t\t{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },\n" +
    " \t\t{ name: 'links' }\n" +
    "\t]\n" +
    "\n" +
    "\t// NOTE: Remember to leave 'toolbar' property with the default value (null).\n" +
    "});</pre> </div> <div id=\"currentToolbar\" style=\"display: none\"> <h2 class=\"samples\">Current toolbar configuration</h2> <p>Below you can see editor with current toolbar definition.</p> <textarea cols=\"80\" id=\"editorCurrent\" name=\"editorCurrent\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <pre id=\"editorCurrentCfg\" class=\"samples\"></pre> </div> <div id=\"fullToolbar\"> <h2 class=\"samples\">Full toolbar configuration</h2> <p>Below you can see editor with full toolbar, generated automatically by the editor.</p> <p> <strong>Note</strong>: To create editor instance with full toolbar you don't have to set anything. Just leave <code>toolbar</code> and <code>toolbarGroups</code> with the default, <code>null</code> values. </p> <textarea cols=\"80\" id=\"editorFull\" name=\"editorFull\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <pre id=\"editorFullCfg\" class=\"samples\"></pre> </div> <script>(function() {\n" +
    "\t'use strict';\n" +
    "\n" +
    "\tvar buttonsNames;\n" +
    "\n" +
    "\tCKEDITOR.config.extraPlugins = 'toolbar';\n" +
    "\n" +
    "\tCKEDITOR.on( 'instanceReady', function( evt ) {\n" +
    "\t\tvar editor = evt.editor,\n" +
    "\t\t\teditorCurrent = editor.name == 'editorCurrent',\n" +
    "\t\t\tdefaultToolbar = !( editor.config.toolbar || editor.config.toolbarGroups || editor.config.removeButtons ),\n" +
    "\t\t\tpre = CKEDITOR.document.getById( editor.name + 'Cfg' ),\n" +
    "\t\t\toutput = '';\n" +
    "\n" +
    "\t\tif ( editorCurrent ) {\n" +
    "\t\t\t// If default toolbar configuration has been modified, show \"current toolbar\" section.\n" +
    "\t\t\tif ( !defaultToolbar )\n" +
    "\t\t\t\tCKEDITOR.document.getById( 'currentToolbar' ).show();\n" +
    "\t\t\telse\n" +
    "\t\t\t\treturn;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tif ( !buttonsNames )\n" +
    "\t\t\tbuttonsNames = createButtonsNamesHash( editor.ui.items );\n" +
    "\n" +
    "\t\t// Toolbar isn't set explicitly, so it was created automatically from toolbarGroups.\n" +
    "\t\tif ( !editor.config.toolbar ) {\n" +
    "\t\t\toutput +=\n" +
    "\t\t\t\t'// Toolbar configuration generated automatically by the editor based on config.toolbarGroups.\\n' +\n" +
    "\t\t\t\tdumpToolbarConfiguration( editor ) +\n" +
    "\t\t\t\t'\\n\\n' +\n" +
    "\t\t\t\t'// Toolbar groups configuration.\\n' +\n" +
    "\t\t\t\tdumpToolbarConfiguration( editor, true )\n" +
    "\t\t}\n" +
    "\t\t// Toolbar groups doesn't count in this case - print only toolbar.\n" +
    "\t\telse {\n" +
    "\t\t\toutput += '// Toolbar configuration.\\n' +\n" +
    "\t\t\t\tdumpToolbarConfiguration( editor );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\t// Recreate to avoid old IE from loosing whitespaces on filling <pre> content.\n" +
    "\t\tvar preOutput = pre.getOuterHtml().replace( /(?=<\\/)/, output );\n" +
    "\t\tCKEDITOR.dom.element.createFromHtml( preOutput ).replace( pre );\n" +
    "\t} );\n" +
    "\n" +
    "\tCKEDITOR.replace( 'editorCurrent', { height: 100 } );\n" +
    "\tCKEDITOR.replace( 'editorFull', {\n" +
    "\t\t// Reset toolbar settings, so full toolbar will be generated automatically.\n" +
    "\t\ttoolbar: null,\n" +
    "\t\ttoolbarGroups: null,\n" +
    "\t\tremoveButtons: null,\n" +
    "\t\theight: 100\n" +
    "\t} );\n" +
    "\n" +
    "\tfunction dumpToolbarConfiguration( editor, printGroups ) {\n" +
    "\t\tvar output = [],\n" +
    "\t\t\ttoolbar = editor.toolbar;\n" +
    "\n" +
    "\t\tfor ( var i = 0; i < toolbar.length; ++i ) {\n" +
    "\t\t\tvar group = dumpToolbarGroup( toolbar[ i ], printGroups );\n" +
    "\t\t\tif ( group )\n" +
    "\t\t\t\toutput.push( group );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\treturn 'config.toolbar' + ( printGroups ? 'Groups' : '' ) + ' = [\\n\\t' + output.join( ',\\n\\t' ) + '\\n];';\n" +
    "\t}\n" +
    "\n" +
    "\tfunction dumpToolbarGroup( group, printGroups ) {\n" +
    "\t\tvar output = [];\n" +
    "\n" +
    "\t\tif ( typeof group == 'string' )\n" +
    "\t\t\treturn '\\'' + group + '\\'';\n" +
    "\t\tif ( CKEDITOR.tools.isArray( group ) )\n" +
    "\t\t\treturn dumpToolbarItems( group );\n" +
    "\t\t// Skip group when printing entire toolbar configuration and there are no items in this group.\n" +
    "\t\tif ( !printGroups && !group.items )\n" +
    "\t\t\treturn;\n" +
    "\n" +
    "\t\tif ( group.name )\n" +
    "\t\t\toutput.push( 'name: \\'' + group.name + '\\'' );\n" +
    "\n" +
    "\t\tif ( group.groups )\n" +
    "\t\t\toutput.push( 'groups: ' + dumpToolbarItems( group.groups ) );\n" +
    "\n" +
    "\t\tif ( !printGroups )\n" +
    "\t\t\toutput.push( 'items: ' + dumpToolbarItems( group.items ) );\n" +
    "\n" +
    "\t\treturn '{ ' + output.join( ', ' ) + ' }';\n" +
    "\t}\n" +
    "\n" +
    "\tfunction dumpToolbarItems( items ) {\n" +
    "\t\tif ( typeof items == 'string' )\n" +
    "\t\t\treturn '\\'' + items + '\\'';\n" +
    "\n" +
    "\t\tvar names = [],\n" +
    "\t\t\ti, item;\n" +
    "\n" +
    "\t\tfor ( var i = 0; i < items.length; ++i ) {\n" +
    "\t\t\titem = items[ i ];\n" +
    "\t\t\tif ( typeof item == 'string' )\n" +
    "\t\t\t\tnames.push( item );\n" +
    "\t\t\telse {\n" +
    "\t\t\t\tif ( item.type == CKEDITOR.UI_SEPARATOR )\n" +
    "\t\t\t\t\tnames.push( '-' );\n" +
    "\t\t\t\telse\n" +
    "\t\t\t\t\tnames.push( buttonsNames[ item.name ] );\n" +
    "\t\t\t}\n" +
    "\t\t}\n" +
    "\n" +
    "\t\treturn '[ \\'' + names.join( '\\', \\'' ) + '\\' ]';\n" +
    "\t}\n" +
    "\n" +
    "\t// Creates { 'lowercased': 'LowerCased' } buttons names hash.\n" +
    "\tfunction createButtonsNamesHash( items ) {\n" +
    "\t\tvar hash = {},\n" +
    "\t\t\tname;\n" +
    "\n" +
    "\t\tfor ( name in items ) {\n" +
    "\t\t\thash[ items[ name ].name ] = name;\n" +
    "\t\t}\n" +
    "\n" +
    "\t\treturn hash;\n" +
    "\t}\n" +
    "\n" +
    "})();</script> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/uicolor.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>UI Color Picker &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; UI Color </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/uicolor.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to automatically replace <code>&lt;textarea&gt;</code> elements with a CKEditor instance with an option to change the color of its user interface.<br> <strong>Note:</strong>The UI skin color feature depends on the CKEditor skin compatibility. The Moono and Kama skins are examples of skins that work with it. </p> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <p> This editor instance has a UI color value defined in configuration to change the skin color, To specify the color of the user interface, set the <code>uiColor</code> property: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\t<strong>uiColor: '#14B8C4'</strong>\n" +
    "});</pre> <p> Note that <code><em>textarea_id</em></code> in the code above is the <code>id</code> attribute of the <code>&lt;textarea&gt;</code> element to be replaced. </p> <p> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>// Replace the <textarea id=\"editor\"> with an CKEditor\n" +
    "\t\t\t// instance, using default configurations.\n" +
    "\t\t\tCKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\tuiColor: '#14B8C4',\n" +
    "\t\t\t\ttoolbar: [\n" +
    "\t\t\t\t\t[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],\n" +
    "\t\t\t\t\t[ 'FontSize', 'TextColor', 'BGColor' ]\n" +
    "\t\t\t\t]\n" +
    "\t\t\t});</script> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/uilanguages.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>User Interface Globalization &mdash; CKEditor Sample</title> <script src=\"../../ckeditor.js\"></script> <script src=\"assets/uilanguages/languages.js\"></script> <link rel=\"stylesheet\" href=\"sample.css\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; User Interface Languages </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/uilanguages.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to automatically replace <code>&lt;textarea&gt;</code> elements with a CKEditor instance with an option to change the language of its user interface. </p> <p> It pulls the language list from CKEditor <code>_languages.js</code> file that contains the list of supported languages and creates a drop-down list that lets the user change the UI language. </p> <p> By default, CKEditor automatically localizes the editor to the language of the user. The UI language can be controlled with two configuration options: <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-language\">language</a></code> and <code><a class=\"samples\" href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-defaultLanguage\"> defaultLanguage</a></code>. The <code>defaultLanguage</code> setting specifies the default CKEditor language to be used when a localization suitable for user's settings is not available. </p> <p> To specify the user interface language that will be used no matter what language is specified in user's browser or operating system, set the <code>language</code> property: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\t// Load the German interface.\n" +
    "\t<strong>language: 'de'</strong>\n" +
    "});</pre> <p> Note that <code><em>textarea_id</em></code> in the code above is the <code>id</code> attribute of the <code>&lt;textarea&gt;</code> element to be replaced. </p> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <p> Available languages (<span id=\"count\"> </span> languages!):<br> <script>document.write( '<select disabled=\"disabled\" id=\"languages\" onchange=\"createEditor( this.value );\">' );\n" +
    "\n" +
    "\t\t\t\t// Get the language list from the _languages.js file.\n" +
    "\t\t\t\tfor ( var i = 0 ; i < window.CKEDITOR_LANGS.length ; i++ ) {\n" +
    "\t\t\t\t\tdocument.write(\n" +
    "\t\t\t\t\t\t'<option value=\"' + window.CKEDITOR_LANGS[i].code + '\">' +\n" +
    "\t\t\t\t\t\t\twindow.CKEDITOR_LANGS[i].name +\n" +
    "\t\t\t\t\t\t'</option>' );\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\tdocument.write( '</select>' );</script> <br> <span style=\"color: #888888\"> (You may see strange characters if your system does not support the selected language) </span> </p> <p> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>// Set the number of languages.\n" +
    "\t\t\t\tdocument.getElementById( 'count' ).innerHTML = window.CKEDITOR_LANGS.length;\n" +
    "\n" +
    "\t\t\t\tvar editor;\n" +
    "\n" +
    "\t\t\t\tfunction createEditor( languageCode ) {\n" +
    "\t\t\t\t\tif ( editor )\n" +
    "\t\t\t\t\t\teditor.destroy();\n" +
    "\n" +
    "\t\t\t\t\t// Replace the <textarea id=\"editor\"> with an CKEditor\n" +
    "\t\t\t\t\t// instance, using default configurations.\n" +
    "\t\t\t\t\teditor = CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\t\t\tlanguage: languageCode,\n" +
    "\n" +
    "\t\t\t\t\t\ton: {\n" +
    "\t\t\t\t\t\t\tinstanceReady: function() {\n" +
    "\t\t\t\t\t\t\t\t// Wait for the editor to be ready to set\n" +
    "\t\t\t\t\t\t\t\t// the language combo.\n" +
    "\t\t\t\t\t\t\t\tvar languages = document.getElementById( 'languages' );\n" +
    "\t\t\t\t\t\t\t\tlanguages.value = this.langCode;\n" +
    "\t\t\t\t\t\t\t\tlanguages.disabled = false;\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t});\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t// At page startup, load the default language:\n" +
    "\t\t\t\tcreateEditor( '' );</script> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/wysiwygarea/fullpage.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>Full Page Editing &mdash; CKEditor Sample</title> <script src=\"../../../ckeditor.js\"></script> <script src=\"../../../samples/old/sample.js\"></script> <link rel=\"stylesheet\" href=\"../../../samples/old/sample.css\"> <meta name=\"ckeditor-sample-required-plugins\" content=\"sourcearea\"> <meta name=\"ckeditor-sample-name\" content=\"Full page support\"> <meta name=\"ckeditor-sample-group\" content=\"Plugins\"> <meta name=\"ckeditor-sample-description\" content=\"CKEditor inserted with a JavaScript call and used to edit the whole page from &lt;html&gt; to &lt;/html&gt;.\"> </head> <body> <h1 class=\"samples\"> <a href=\"../../../samples/old/index.html\">CKEditor Samples</a> &raquo; Full Page Editing </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/fullpage.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to configure CKEditor to edit entire HTML pages, from the <code>&lt;html&gt;</code> tag to the <code>&lt;/html&gt;</code> tag. </p> <p> The CKEditor instance below is inserted with a JavaScript call using the following code: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\t<strong>fullPage: true</strong>,\n" +
    "\t<strong>allowedContent: true</strong>\n" +
    "});\n" +
    "</pre> <p> Note that <code><em>textarea_id</em></code> in the code above is the <code>id</code> attribute of the <code>&lt;textarea&gt;</code> element to be replaced. </p> <p> The <code><em>allowedContent</em></code> in the code above is set to <code>true</code> to disable content filtering. Setting this option is not obligatory, but in full page mode there is a strong chance that one may want be able to freely enter any HTML content in source mode without any limitations. </p> </div> <form action=\"../../../samples/sample_posteddata.php\" method=\"post\"> <label for=\"editor1\"> CKEditor output the entire page including content outside of <code>&lt;body&gt;</code> element, so content like meta and title can be changed: </label> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">\n" +
    "\t\t\t&lt;h1&gt;&lt;img align=&quot;right&quot; alt=&quot;Saturn V carrying Apollo 11&quot; src=&quot;../../../samples/old/assets/sample.jpg&quot;/&gt; Apollo 11&lt;/h1&gt; &lt;p&gt;&lt;b&gt;Apollo 11&lt;/b&gt; was the spaceflight that landed the first humans, Americans &lt;a href=&quot;http://en.wikipedia.org/wiki/Neil_Armstrong&quot; title=&quot;Neil Armstrong&quot;&gt;Neil Armstrong&lt;/a&gt; and &lt;a href=&quot;http://en.wikipedia.org/wiki/Buzz_Aldrin&quot; title=&quot;Buzz Aldrin&quot;&gt;Buzz Aldrin&lt;/a&gt;, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.&lt;/p&gt; &lt;p&gt;Armstrong spent about &lt;s&gt;three and a half&lt;/s&gt; two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&amp;nbsp;kg) of lunar material for return to Earth. A third member of the mission, &lt;a href=&quot;http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)&quot; title=&quot;Michael Collins (astronaut)&quot;&gt;Michael Collins&lt;/a&gt;, piloted the &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_Command/Service_Module&quot; title=&quot;Apollo Command/Service Module&quot;&gt;command&lt;/a&gt; spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.&lt;/p&gt; &lt;h2&gt;Broadcasting and &lt;em&gt;quotes&lt;/em&gt; &lt;a id=&quot;quotes&quot; name=&quot;quotes&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;p&gt;Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;One small step for [a] man, one giant leap for mankind.&lt;/p&gt;&lt;/blockquote&gt; &lt;p&gt;Apollo 11 effectively ended the &lt;a href=&quot;http://en.wikipedia.org/wiki/Space_Race&quot; title=&quot;Space Race&quot;&gt;Space Race&lt;/a&gt; and fulfilled a national goal proposed in 1961 by the late U.S. President &lt;a href=&quot;http://en.wikipedia.org/wiki/John_F._Kennedy&quot; title=&quot;John F. Kennedy&quot;&gt;John F. Kennedy&lt;/a&gt; in a speech before the United States Congress:&lt;/p&gt; &lt;blockquote&gt;&lt;p&gt;[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.&lt;/p&gt;&lt;/blockquote&gt; &lt;h2&gt;Technical details &lt;a id=&quot;tech-details&quot; name=&quot;tech-details&quot;&gt;&lt;/a&gt;&lt;/h2&gt; &lt;table align=&quot;right&quot; border=&quot;1&quot; bordercolor=&quot;#ccc&quot; cellpadding=&quot;5&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse:collapse;margin:10px 0 10px 15px;&quot;&gt; &lt;caption&gt;&lt;strong&gt;Mission crew&lt;/strong&gt;&lt;/caption&gt; &lt;thead&gt; &lt;tr&gt; &lt;th scope=&quot;col&quot;&gt;Position&lt;/th&gt; &lt;th scope=&quot;col&quot;&gt;Astronaut&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;td&gt;Commander&lt;/td&gt; &lt;td&gt;Neil A. Armstrong&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Command Module Pilot&lt;/td&gt; &lt;td&gt;Michael Collins&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;td&gt;Lunar Module Pilot&lt;/td&gt; &lt;td&gt;Edwin &amp;quot;Buzz&amp;quot; E. Aldrin, Jr.&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;p&gt;Launched by a &lt;strong&gt;Saturn V&lt;/strong&gt; rocket from &lt;a href=&quot;http://en.wikipedia.org/wiki/Kennedy_Space_Center&quot; title=&quot;Kennedy Space Center&quot;&gt;Kennedy Space Center&lt;/a&gt; in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of &lt;a href=&quot;http://en.wikipedia.org/wiki/NASA&quot; title=&quot;NASA&quot;&gt;NASA&lt;/a&gt;&amp;#39;s Apollo program. The Apollo spacecraft had three parts:&lt;/p&gt; &lt;ol&gt; &lt;li&gt;&lt;strong&gt;Command Module&lt;/strong&gt; with a cabin for the three astronauts which was the only part which landed back on Earth&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Service Module&lt;/strong&gt; which supported the Command Module with propulsion, electrical power, oxygen and water&lt;/li&gt; &lt;li&gt;&lt;strong&gt;Lunar Module&lt;/strong&gt; for landing on the Moon.&lt;/li&gt; &lt;/ol&gt; &lt;p&gt;After being sent to the Moon by the Saturn V&amp;#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Mare_Tranquillitatis&quot; title=&quot;Mare Tranquillitatis&quot;&gt;Sea of Tranquility&lt;/a&gt;. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the &lt;a href=&quot;http://en.wikipedia.org/wiki/Pacific_Ocean&quot; title=&quot;Pacific Ocean&quot;&gt;Pacific Ocean&lt;/a&gt; on July 24.&lt;/p&gt; &lt;hr/&gt; &lt;p style=&quot;text-align: right;&quot;&gt;&lt;small&gt;Source: &lt;a href=&quot;http://en.wikipedia.org/wiki/Apollo_11&quot;&gt;Wikipedia.org&lt;/a&gt;&lt;/small&gt;&lt;/p&gt;\n" +
    "\t\t</textarea> <script>CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\tfullPage: true,\n" +
    "\t\t\t\tallowedContent: true,\n" +
    "\t\t\t\textraPlugins: 'wysiwygarea'\n" +
    "\t\t\t});</script> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/old/xhtmlstyle.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--> <html> <head> <meta charset=\"utf-8\"> <title>XHTML Compliant Output &mdash; CKEditor Sample</title> <meta name=\"ckeditor-sample-required-plugins\" content=\"sourcearea\"> <script src=\"../../ckeditor.js\"></script> <script src=\"sample.js\"></script> <link href=\"sample.css\" rel=\"stylesheet\"> </head> <body> <h1 class=\"samples\"> <a href=\"index.html\">CKEditor Samples</a> &raquo; Producing XHTML Compliant Output </h1> <div class=\"warning deprecated\"> This sample is not maintained anymore. Check out its <a href=\"https://sdk.ckeditor.com/samples/basicstyles.html\">brand new version in CKEditor SDK</a>. </div> <div class=\"description\"> <p> This sample shows how to configure CKEditor to output valid <a class=\"samples\" href=\"http://www.w3.org/TR/xhtml11/\">XHTML 1.1</a> code. Deprecated elements (<code>&lt;font&gt;</code>, <code>&lt;u&gt;</code>) or attributes (<code>size</code>, <code>face</code>) will be replaced with XHTML compliant code. </p> <p> To add a CKEditor instance outputting valid XHTML code, load the editor using a standard JavaScript call and define CKEditor features to use the XHTML compliant elements and styles. </p> <p> A snippet of the configuration code can be seen below; check the source of this page for full definition: </p> <pre class=\"samples\">\n" +
    "CKEDITOR.replace( '<em>textarea_id</em>', {\n" +
    "\tcontentsCss: 'assets/outputxhtml.css',\n" +
    "\n" +
    "\tcoreStyles_bold: {\n" +
    "\t\telement: 'span',\n" +
    "\t\tattributes: { 'class': 'Bold' }\n" +
    "\t},\n" +
    "\tcoreStyles_italic: {\n" +
    "\t\telement: 'span',\n" +
    "\t\tattributes: { 'class': 'Italic' }\n" +
    "\t},\n" +
    "\n" +
    "\t...\n" +
    "});</pre> </div> <form action=\"sample_posteddata.php\" method=\"post\"> <p> <label for=\"editor1\"> Editor 1: </label> <textarea cols=\"80\" id=\"editor1\" name=\"editor1\" rows=\"10\">&lt;p&gt;This is some &lt;span class=\"Bold\"&gt;sample text&lt;/span&gt;. You are using &lt;a href=\"https://ckeditor.com/\"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea> <script>CKEDITOR.replace( 'editor1', {\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Style sheet for the contents\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tcontentsCss: 'assets/outputxhtml/outputxhtml.css',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Special allowed content rules for spans used by\n" +
    "\t\t\t\t\t * font face, size, and color buttons.\n" +
    "\t\t\t\t\t *\n" +
    "\t\t\t\t\t * Note: all rules have been written separately so\n" +
    "\t\t\t\t\t * it was possible to specify required classes.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\textraAllowedContent: 'span(!FontColor1);span(!FontColor2);span(!FontColor3);' +\n" +
    "\t\t\t\t\t\t'span(!FontColor1BG);span(!FontColor2BG);span(!FontColor3BG);' +\n" +
    "\t\t\t\t\t\t'span(!FontComic);span(!FontCourier);span(!FontTimes);' +\n" +
    "\t\t\t\t\t\t'span(!FontSmaller);span(!FontLarger);span(!FontSmall);span(!FontBig);span(!FontDouble)',\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Core styles.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tcoreStyles_bold: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'Bold' }\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\tcoreStyles_italic: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'Italic' }\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\tcoreStyles_underline: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'Underline' }\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\tcoreStyles_strike: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'StrikeThrough' },\n" +
    "\t\t\t\t\t\toverrides: 'strike'\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\tcoreStyles_subscript: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'Subscript' },\n" +
    "\t\t\t\t\t\toverrides: 'sub'\n" +
    "\t\t\t\t\t},\n" +
    "\t\t\t\t\tcoreStyles_superscript: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': 'Superscript' },\n" +
    "\t\t\t\t\t\toverrides: 'sup'\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font face.\n" +
    "\t\t\t\t\t */\n" +
    "\n" +
    "\t\t\t\t\t// List of fonts available in the toolbar combo. Each font definition is\n" +
    "\t\t\t\t\t// separated by a semi-colon (;). We are using class names here, so each font\n" +
    "\t\t\t\t\t// is defined by {Combo Label}/{Class Name}.\n" +
    "\t\t\t\t\tfont_names: 'Comic Sans MS/FontComic;Courier New/FontCourier;Times New Roman/FontTimes',\n" +
    "\n" +
    "\t\t\t\t\t// Define the way font elements will be applied to the document. The \"span\"\n" +
    "\t\t\t\t\t// element will be used. When a font is selected, the font name defined in the\n" +
    "\t\t\t\t\t// above list is passed to this definition with the name \"Font\", being it\n" +
    "\t\t\t\t\t// injected in the \"class\" attribute.\n" +
    "\t\t\t\t\t// We must also instruct the editor to replace span elements that are used to\n" +
    "\t\t\t\t\t// set the font (Overrides).\n" +
    "\t\t\t\t\tfont_style: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': '#(family)' },\n" +
    "\t\t\t\t\t\toverrides: [\n" +
    "\t\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\t\t\tattributes: {\n" +
    "\t\t\t\t\t\t\t\t\t'class': /^Font(?:Comic|Courier|Times)$/\n" +
    "\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t]\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font sizes.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tfontSize_sizes: 'Smaller/FontSmaller;Larger/FontLarger;8pt/FontSmall;14pt/FontBig;Double Size/FontDouble',\n" +
    "\t\t\t\t\tfontSize_style: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': '#(size)' },\n" +
    "\t\t\t\t\t\toverrides: [\n" +
    "\t\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\t\t\tattributes: {\n" +
    "\t\t\t\t\t\t\t\t\t'class': /^Font(?:Smaller|Larger|Small|Big|Double)$/\n" +
    "\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t]\n" +
    "\t\t\t\t\t} ,\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Font colors.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tcolorButton_enableMore: false,\n" +
    "\n" +
    "\t\t\t\t\tcolorButton_colors: 'FontColor1/FF9900,FontColor2/0066CC,FontColor3/F00',\n" +
    "\t\t\t\t\tcolorButton_foreStyle: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': '#(color)' },\n" +
    "\t\t\t\t\t\toverrides: [\n" +
    "\t\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\t\t\tattributes: {\n" +
    "\t\t\t\t\t\t\t\t\t'class': /^FontColor(?:1|2|3)$/\n" +
    "\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t]\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\tcolorButton_backStyle: {\n" +
    "\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\tattributes: { 'class': '#(color)BG' },\n" +
    "\t\t\t\t\t\toverrides: [\n" +
    "\t\t\t\t\t\t\t{\n" +
    "\t\t\t\t\t\t\t\telement: 'span',\n" +
    "\t\t\t\t\t\t\t\tattributes: {\n" +
    "\t\t\t\t\t\t\t\t\t'class': /^FontColor(?:1|2|3)BG$/\n" +
    "\t\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t\t]\n" +
    "\t\t\t\t\t},\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Indentation.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tindentClasses: [ 'Indent1', 'Indent2', 'Indent3' ],\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Paragraph justification.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tjustifyClasses: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull' ],\n" +
    "\n" +
    "\t\t\t\t\t/*\n" +
    "\t\t\t\t\t * Styles combo.\n" +
    "\t\t\t\t\t */\n" +
    "\t\t\t\t\tstylesSet: [\n" +
    "\t\t\t\t\t\t{ name: 'Strong Emphasis', element: 'strong' },\n" +
    "\t\t\t\t\t\t{ name: 'Emphasis', element: 'em' },\n" +
    "\n" +
    "\t\t\t\t\t\t{ name: 'Computer Code', element: 'code' },\n" +
    "\t\t\t\t\t\t{ name: 'Keyboard Phrase', element: 'kbd' },\n" +
    "\t\t\t\t\t\t{ name: 'Sample Text', element: 'samp' },\n" +
    "\t\t\t\t\t\t{ name: 'Variable', element: 'var' },\n" +
    "\n" +
    "\t\t\t\t\t\t{ name: 'Deleted Text', element: 'del' },\n" +
    "\t\t\t\t\t\t{ name: 'Inserted Text', element: 'ins' },\n" +
    "\n" +
    "\t\t\t\t\t\t{ name: 'Cited Work', element: 'cite' },\n" +
    "\t\t\t\t\t\t{ name: 'Inline Quotation', element: 'q' }\n" +
    "\t\t\t\t\t]\n" +
    "\t\t\t\t});</script> </p> <p> <input type=\"submit\" value=\"Submit\"> </p> </form> <div id=\"footer\"> <hr> <p> CKEditor - The text editor for the Internet - <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> - Frederico Knabben. All rights reserved. </p> </div> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/directives/ckeditor/samples/toolbarconfigurator/index.html',
    "<!DOCTYPE html><!--\n" +
    "Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.\n" +
    "For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license\n" +
    "--><!--[if IE 8]><html class=\"ie8\"><![endif]--><!--[if gt IE 8]><html><![endif]--><!--[if !IE]><!--><html><!--<![endif]--> <head> <meta charset=\"utf-8\"> <title>Toolbar Configurator</title> <script src=\"../../ckeditor.js\"></script> <script>if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )\n" +
    "\t\t\tCKEDITOR.tools.enableHtml5Elements( document );</script> <link rel=\"stylesheet\" href=\"lib/codemirror/codemirror.css\"> <link rel=\"stylesheet\" href=\"lib/codemirror/show-hint.css\"> <link rel=\"stylesheet\" href=\"lib/codemirror/neo.css\"> <link rel=\"stylesheet\" href=\"css/fontello.css\"> <link rel=\"stylesheet\" href=\"../css/samples.css\"> </head> <body id=\"toolbar\"> <nav class=\"navigation-a\"> <div class=\"grid-container\"> <ul class=\"navigation-a-left grid-width-70\"> <li><a href=\"https://ckeditor.com/ckeditor-4/\">Project Homepage</a></li> <li><a href=\"https://github.com/ckeditor/ckeditor-dev/issues\">I found a bug</a></li> <li><a href=\"http://github.com/ckeditor/ckeditor-dev\" class=\"icon-pos-right icon-navigation-a-github\">Fork CKEditor on GitHub</a></li> </ul> <ul class=\"navigation-a-right grid-width-30\"> <li><a href=\"https://ckeditor.com/blog/\">CKEditor Blog</a></li> </ul> </div> </nav> <header class=\"header-a\"> <div class=\"grid-container\"> <h1 class=\"header-a-logo grid-width-30\"> <a href=\"../index.html\"><img src=\"../img/logo.png\" alt=\"CKEditor Logo\"></a> </h1> <nav class=\"navigation-b grid-width-70\"> <ul> <li><a href=\"../index.html\" class=\"button-a\">Start</a></li> <li><a href=\"index.html\" class=\"button-a button-a-background\">Toolbar configurator</a></li> </ul> </nav> </div> </header> <main> <div class=\"adjoined-top\"> <div class=\"grid-container\"> <div class=\"content grid-width-100\"> <div class=\"grid-container-nested\"> <h1 class=\"grid-width-60\"> Toolbar Configurator <a href=\"#help-content\" type=\"button\" title=\"Configurator help\" id=\"help\" class=\"button-a button-a-background button-a-no-text icon-pos-left icon-question-mark\">Help</a> </h1> <div class=\"grid-width-40 grid-switch-magic\"> <div class=\"switch\"> <span class=\"balloon-a balloon-a-se\">Select configurator type</span> <input type=\"radio\" name=\"radio\" data-num=\"1\" id=\"radio-basic\"> <input type=\"radio\" name=\"radio\" data-num=\"2\" id=\"radio-advanced\"> <label data-for=\"1\" for=\"radio-basic\">Basic</label> <span class=\"switch-inner\"> <span class=\"handler\"></span> </span> <label data-for=\"2\" for=\"radio-advanced\">Advanced</label> </div> </div> </div> </div> </div> </div> <div class=\"adjoined-bottom\"> <div class=\"grid-container\"> <div class=\"grid-width-100\"> <div class=\"editors-container\"> <div id=\"editor-basic\"></div> <div id=\"editor-advanced\"></div> </div> </div> </div> </div> <div class=\"grid-container configurator\"> <div class=\"content grid-width-100\"> <div class=\"configurator\"> <div> <div id=\"toolbarModifierWrapper\"></div> </div> </div> </div> </div> <div id=\"help-content\"> <div class=\"grid-container\"> <div class=\"grid-width-100\"> <h2>What Am I Doing Here?</h2> <div class=\"grid-container grid-container-nested\"> <div class=\"basic\"> <div class=\"grid-width-50\"> <p>Arrange <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-toolbarGroups\">toolbar groups</a>, toggle <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-removeButtons\">button visibility</a> according to your needs and get your toolbar configuration.</p> <p>You can replace the content of the <a href=\"../../config.js\"><code>config.js</code></a> file with the generated configuration. If you already set some configuration options you will need to merge both configurations.</p> </div> <div class=\"grid-width-50\"> <p>Read more about different ways of <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_configuration\">setting configuration</a> and do not forget about <strong>clearing browser cache</strong>.</p> <p>Arranging toolbar groups is the recommended way of configuring the toolbar, but if you need more freedom you can use the <a href=\"#advanced\">advanced configurator</a>.</p> </div> </div> <div class=\"advanced\" style=\"display: none\"> <div class=\"grid-width-50\"> <p>With this code editor you can edit your <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-toolbar\">toolbar configuration</a> live.</p> <p>You can replace the content of the <a href=\"../../config.js\"><code>config.js</code></a> file with the generated configuration. If you already set some configuration options you will need to merge both configurations.</p> </div> <div class=\"grid-width-50\"> <p>Read more about different ways of <a href=\"https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_configuration\">setting configuration</a> and do not forget about <strong>clearing browser cache</strong>.</p> </div> </div> </div> <p class=\"grid-container grid-container-nested\"> <button type=\"button\" class=\"help-content-close grid-width-100 button-a button-a-background\">Got it. Let's play!</button> </p> </div> </div> </div> </main> <footer class=\"footer-a grid-container\"> <p class=\"grid-width-100\"> CKEditor &ndash; The text editor for the Internet &ndash; <a class=\"samples\" href=\"https://ckeditor.com/\">https://ckeditor.com</a> </p> <p class=\"grid-width-100\" id=\"copy\"> Copyright &copy; 2003-2017, <a class=\"samples\" href=\"https://cksource.com/\">CKSource</a> &ndash; Frederico Knabben. All rights reserved. </p> </footer> <script src=\"lib/codemirror/codemirror.js\"></script> <script src=\"lib/codemirror/javascript.js\"></script> <script src=\"lib/codemirror/show-hint.js\"></script> <script src=\"js/fulltoolbareditor.js\"></script> <script src=\"js/abstracttoolbarmodifier.js\"></script> <script src=\"js/toolbarmodifier.js\"></script> <script src=\"js/toolbartextmodifier.js\"></script> <script src=\"../js/sf.js\"></script> <script>( function() {\n" +
    "\t\t'use strict';\n" +
    "\n" +
    "\t\tvar mode = ( window.location.hash.substr( 1 ) === 'advanced' ) ? 'advanced' : 'basic',\n" +
    "\t\t\tconfiguratorSection = CKEDITOR.document.findOne( 'main > .grid-container.configurator' ),\n" +
    "\t\t\tbasicInstruction = CKEDITOR.document.findOne( '#help-content .basic' ),\n" +
    "\t\t\tadvancedInstruction = CKEDITOR.document.findOne( '#help-content .advanced' ),\n" +
    "\n" +
    "\t\t\t// Configurator mode switcher.\n" +
    "\t\t\tmodeSwitchBasic = CKEDITOR.document.getById( 'radio-basic' ),\n" +
    "\t\t\tmodeSwitchAdvanced = CKEDITOR.document.getById( 'radio-advanced' );\n" +
    "\n" +
    "\t\t// Initial setup\n" +
    "\t\tfunction updateSwitcher() {\n" +
    "\t\t\tif ( mode === 'advanced' ) {\n" +
    "\t\t\t\tmodeSwitchAdvanced.$.checked = true;\n" +
    "\t\t\t} else {\n" +
    "\t\t\t\tmodeSwitchBasic.$.checked = true;\n" +
    "\t\t\t}\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tupdateSwitcher();\n" +
    "\n" +
    "\t\tCKEDITOR.document.getWindow().on( 'hashchange', function( e ) {\n" +
    "\t\t\tvar hash = window.location.hash.substr( 1 );\n" +
    "\t\t\tif ( !( hash === 'advanced' || hash === 'basic' ) ) {\n" +
    "\t\t\t\treturn;\n" +
    "\t\t\t}\n" +
    "\t\t\tmode = hash;\n" +
    "\t\t\tonToolbarsDone( mode );\n" +
    "\t\t} );\n" +
    "\n" +
    "\t\tCKEDITOR.document.getWindow().on( 'resize', function() {\n" +
    "\t\t\tupdateToolbar( ( mode === 'basic' ? toolbarModifier : toolbarTextModifier )[ 'editorInstance' ] );\n" +
    "\t\t} );\n" +
    "\n" +
    "\t\tfunction onRefresh( modifier ) {\n" +
    "\t\t\tmodifier = modifier || this;\n" +
    "\n" +
    "\t\t\tif ( mode === 'basic' && modifier instanceof ToolbarConfigurator.ToolbarTextModifier ) {\n" +
    "\t\t\t\treturn;\n" +
    "\t\t\t}\n" +
    "\n" +
    "\t\t\t// CodeMirror container becomes visible, so we need to refresh and to avoid rendering problems.\n" +
    "\t\t\tif ( mode === 'advanced' && modifier instanceof ToolbarConfigurator.ToolbarTextModifier ) {\n" +
    "\t\t\t\tmodifier.codeContainer.refresh();\n" +
    "\t\t\t}\n" +
    "\n" +
    "\t\t\tupdateToolbar( modifier.editorInstance );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tfunction updateToolbar( editor ) {\n" +
    "\t\t\tvar editorContainer = editor.container;\n" +
    "\n" +
    "\t\t\t// Not always editor is loaded.\n" +
    "\t\t\tif ( !editorContainer ) {\n" +
    "\t\t\t\treturn;\n" +
    "\t\t\t}\n" +
    "\n" +
    "\t\t\tvar displayStyle = editorContainer.getStyle( 'display' );\n" +
    "\n" +
    "\t\t\teditorContainer.setStyle( 'display', 'block' );\n" +
    "\n" +
    "\t\t\tvar newHeight = editorContainer.getSize( 'height' );\n" +
    "\n" +
    "\t\t\tvar newMarginTop = parseInt( editorContainer.getComputedStyle( 'margin-top' ), 10 );\n" +
    "\t\t\tnewMarginTop = ( isNaN( newMarginTop ) ? 0 : Number( newMarginTop ) );\n" +
    "\n" +
    "\t\t\tvar newMarginBottom = parseInt( editorContainer.getComputedStyle( 'margin-bottom' ), 10 );\n" +
    "\t\t\tnewMarginBottom = ( isNaN( newMarginBottom ) ? 0 : Number( newMarginBottom ) );\n" +
    "\n" +
    "\t\t\tvar result = newHeight + newMarginTop + newMarginBottom;\n" +
    "\n" +
    "\t\t\teditorContainer.setStyle( 'display', displayStyle );\n" +
    "\n" +
    "\t\t\teditor.container.getAscendant( 'div' ).setStyle( 'height', result + 'px' );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tvar toolbarModifier = new ToolbarConfigurator.ToolbarModifier( 'editor-basic' );\n" +
    "\n" +
    "\t\tvar done = 0;\n" +
    "\t\ttoolbarModifier.init( onToolbarInit );\n" +
    "\t\ttoolbarModifier.onRefresh = onRefresh;\n" +
    "\n" +
    "\t\tCKEDITOR.document.getById( 'toolbarModifierWrapper' ).append( toolbarModifier.mainContainer );\n" +
    "\n" +
    "\t\tvar toolbarTextModifier = new ToolbarConfigurator.ToolbarTextModifier( 'editor-advanced' );\n" +
    "\t\ttoolbarTextModifier.init( onToolbarInit );\n" +
    "\t\ttoolbarTextModifier.onRefresh = onRefresh;\n" +
    "\n" +
    "\t\tfunction onToolbarInit() {\n" +
    "\t\t\tif ( ++done === 2 ) {\n" +
    "\t\t\t\tonToolbarsDone();\n" +
    "\n" +
    "\t\t\t\tpositionSticky.watch( CKEDITOR.document.findOne( '.toolbar' ), function() {\n" +
    "\t\t\t\t\treturn mode === 'advanced';\n" +
    "\t\t\t\t} );\n" +
    "\t\t\t}\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tfunction onToolbarsDone() {\n" +
    "\t\t\tif ( mode === 'basic' ) {\n" +
    "\t\t\t\ttoggleModeBasic( false );\n" +
    "\t\t\t} else {\n" +
    "\t\t\t\ttoggleModeAdvanced( false );\n" +
    "\t\t\t}\n" +
    "\n" +
    "\t\t\tupdateSwitcher();\n" +
    "\n" +
    "\t\t\tsetTimeout( function() {\n" +
    "\t\t\t\tCKEDITOR.document.findOne( '.editors-container' ).addClass( 'active' );\n" +
    "\t\t\t\tCKEDITOR.document.findOne( '#toolbarModifierWrapper' ).addClass( 'active' );\n" +
    "\t\t\t}, 200 );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tCKEDITOR.document.getById( 'toolbarModifierWrapper' ).append( toolbarTextModifier.mainContainer );\n" +
    "\n" +
    "\t\tfunction toogleModeSwitch( onElement, offElement, onModifier, offModifier ) {\n" +
    "\t\t\tonElement.addClass( 'fancy-button-active' );\n" +
    "\t\t\toffElement.removeClass( 'fancy-button-active' );\n" +
    "\n" +
    "\t\t\tonModifier.showUI();\n" +
    "\t\t\toffModifier.hideUI();\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tfunction toggleModeBasic( callOnRefresh ) {\n" +
    "\t\t\tcallOnRefresh = ( callOnRefresh !== false );\n" +
    "\t\t\tmode = 'basic';\n" +
    "\t\t\twindow.location.hash = '#basic';\n" +
    "\t\t\ttoogleModeSwitch( modeSwitchBasic, modeSwitchAdvanced, toolbarModifier, toolbarTextModifier );\n" +
    "\n" +
    "\t\t\tconfiguratorSection.removeClass( 'freed-width' );\n" +
    "\t\t\tbasicInstruction.show();\n" +
    "\t\t\tadvancedInstruction.hide();\n" +
    "\n" +
    "\t\t\tcallOnRefresh && onRefresh( toolbarModifier );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tfunction toggleModeAdvanced( callOnRefresh ) {\n" +
    "\t\t\tcallOnRefresh = ( callOnRefresh !== false );\n" +
    "\t\t\tmode = 'advanced';\n" +
    "\t\t\twindow.location.hash = '#advanced';\n" +
    "\t\t\ttoogleModeSwitch( modeSwitchAdvanced, modeSwitchBasic, toolbarTextModifier, toolbarModifier );\n" +
    "\n" +
    "\t\t\tconfiguratorSection.addClass( 'freed-width' );\n" +
    "\t\t\tadvancedInstruction.show();\n" +
    "\t\t\tbasicInstruction.hide();\n" +
    "\n" +
    "\t\t\tcallOnRefresh && onRefresh( toolbarTextModifier );\n" +
    "\t\t}\n" +
    "\n" +
    "\t\tmodeSwitchBasic.on( 'click', toggleModeBasic );\n" +
    "\t\tmodeSwitchAdvanced.on( 'click', toggleModeAdvanced );\n" +
    "\n" +
    "\t\t//\n" +
    "\t\t// Position:sticky for the toolbar.\n" +
    "\t\t//\n" +
    "\n" +
    "\t\t// Will make elements behave like they were styled with position:sticky.\n" +
    "\t\tvar positionSticky = {\n" +
    "\t\t\t// Store object: {\n" +
    "\t\t\t// \t\telement: CKEDITOR.dom.element, // Element which will float.\n" +
    "\t\t\t// \t\tplaceholder: CKEDITOR.dom.element, // Placeholder which is place to prevent page bounce.\n" +
    "\t\t\t// \t\tisFixed: boolean // Whether element float now.\n" +
    "\t\t\t// }\n" +
    "\t\t\twatched: [],\n" +
    "\n" +
    "\t\t\tactive: [],\n" +
    "\n" +
    "\t\t\tstaticContainer: null,\n" +
    "\n" +
    "\t\t\tinit: function() {\n" +
    "\t\t\t\tvar element = CKEDITOR.dom.element.createFromHtml(\n" +
    "\t\t\t\t\t'<div class=\"staticContainer\">' +\n" +
    "\t\t\t\t\t\t'<div class=\"grid-container\" >' +\n" +
    "\t\t\t\t\t\t\t'<div class=\"grid-width-100\">' +\n" +
    "\t\t\t\t\t\t\t\t'<div class=\"inner\"></div>' +\n" +
    "\t\t\t\t\t\t\t'</div>' +\n" +
    "\t\t\t\t\t\t'</div>' +\n" +
    "\t\t\t\t\t'</div>' );\n" +
    "\n" +
    "\t\t\t\tthis.staticContainer = element.findOne( '.inner' );\n" +
    "\n" +
    "\t\t\t\tCKEDITOR.document.getBody().append( element );\n" +
    "\t\t\t},\n" +
    "\n" +
    "\t\t\twatch: function( element, preventFunc ) {\n" +
    "\t\t\t\tthis.watched.push( {\n" +
    "\t\t\t\t\telement: element,\n" +
    "\t\t\t\t\tplaceholder: new CKEDITOR.dom.element( 'div' ),\n" +
    "\t\t\t\t\tisFixed: false,\n" +
    "\t\t\t\t\tpreventFunc: preventFunc\n" +
    "\t\t\t\t} );\n" +
    "\t\t\t},\n" +
    "\n" +
    "\t\t\tcheckAll: function() {\n" +
    "\t\t\t\tfor ( var i = 0; i < this.watched.length; i++ ) {\n" +
    "\t\t\t\t\tthis.check( this.watched[ i ] );\n" +
    "\t\t\t\t}\n" +
    "\t\t\t},\n" +
    "\n" +
    "\t\t\tcheck: function( element ) {\n" +
    "\t\t\t\tvar isFixed = element.isFixed;\n" +
    "\t\t\t\tvar shouldBeFixed = this.shouldBeFixed( element );\n" +
    "\n" +
    "\t\t\t\t// Nothing to be done.\n" +
    "\t\t\t\tif ( isFixed === shouldBeFixed ) {\n" +
    "\t\t\t\t\treturn;\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\tvar placeholder = element.placeholder;\n" +
    "\n" +
    "\t\t\t\tif ( isFixed ) {\n" +
    "\t\t\t\t\t// Unfixing.\n" +
    "\n" +
    "\t\t\t\t\telement.element.insertBefore( placeholder );\n" +
    "\t\t\t\t\tplaceholder.remove();\n" +
    "\n" +
    "\t\t\t\t\telement.element.removeStyle( 'margin' );\n" +
    "\n" +
    "\t\t\t\t\tthis.active.splice( CKEDITOR.tools.indexOf( this.active, element ), 1 );\n" +
    "\n" +
    "\t\t\t\t} else {\n" +
    "\t\t\t\t\t// Fixing.\n" +
    "\t\t\t\t\tplaceholder.setStyle( 'width', element.element.getSize( 'width' ) + 'px' );\n" +
    "\t\t\t\t\tplaceholder.setStyle( 'height', element.element.getSize( 'height' ) + 'px' );\n" +
    "\t\t\t\t\tplaceholder.setStyle( 'margin-bottom', element.element.getComputedStyle( 'margin-bottom' ) );\n" +
    "\t\t\t\t\tplaceholder.setStyle( 'display', element.element.getComputedStyle( 'display' ) );\n" +
    "\t\t\t\t\tplaceholder.insertAfter( element.element );\n" +
    "\n" +
    "\t\t\t\t\tthis.staticContainer.append( element.element );\n" +
    "\n" +
    "\t\t\t\t\tthis.active.push( element );\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\telement.isFixed = !element.isFixed;\n" +
    "\t\t\t},\n" +
    "\n" +
    "\t\t\tshouldBeFixed: function( element ) {\n" +
    "\t\t\t\tif ( element.preventFunc && element.preventFunc() ) {\n" +
    "\t\t\t\t\treturn false;\n" +
    "\t\t\t\t}\n" +
    "\n" +
    "\t\t\t\t// If element is already fixed we are checking it's placeholder.\n" +
    "\t\t\t\tvar related = ( element.isFixed ? element.placeholder : element.element ),\n" +
    "\t\t\t\t\tclientRect = related.$.getBoundingClientRect(),\n" +
    "\t\t\t\t\tstaticHeight = this.staticContainer.getSize('height' ),\n" +
    "\t\t\t\t\telemHeight = element.element.getSize( 'height' );\n" +
    "\n" +
    "\t\t\t\tif ( element.isFixed ) {\n" +
    "\t\t\t\t\treturn ( clientRect.top + elemHeight < staticHeight );\n" +
    "\t\t\t\t} else {\n" +
    "\t\t\t\t\treturn ( clientRect.top < staticHeight );\n" +
    "\t\t\t\t}\n" +
    "\t\t\t}\n" +
    "\t\t};\n" +
    "\n" +
    "\t\tpositionSticky.init();\n" +
    "\n" +
    "\t\tCKEDITOR.document.getWindow().on( 'scroll',\n" +
    "\t\t\tnew CKEDITOR.tools.eventsBuffer( 100, positionSticky.checkAll, positionSticky ).input\n" +
    "\t\t);\n" +
    "\n" +
    "\t\t// Make the toolbar sticky.\n" +
    "\t\tpositionSticky.watch( CKEDITOR.document.findOne( '.editors-container' ) );\n" +
    "\n" +
    "\t\t// Help button and help-content.\n" +
    "\t\t( function() {\n" +
    "\t\t\tvar helpButton = CKEDITOR.document.getById( 'help' ),\n" +
    "\t\t\t\thelpContent = CKEDITOR.document.getById( 'help-content' );\n" +
    "\n" +
    "\t\t\t// Don't show help button on IE8 because it's unsupported by Pico Modal.\n" +
    "\t\t\tif ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {\n" +
    "\t\t\t\thelpButton.hide();\n" +
    "\t\t\t} else {\n" +
    "\t\t\t\t// Display help modal when the button is clicked.\n" +
    "\t\t\t\thelpButton.on( 'click', function( evt ) {\n" +
    "\t\t\t\t\tSF.modal( {\n" +
    "\t\t\t\t\t\t// Clone modal content from DOM.\n" +
    "\t\t\t\t\t\tcontent: helpContent.getHtml(),\n" +
    "\n" +
    "\t\t\t\t\t\tafterCreate: function( modal ) {\n" +
    "\t\t\t\t\t\t\t// Enable modal content button to close the modal.\n" +
    "\t\t\t\t\t\t\tnew CKEDITOR.dom.element( modal.modalElem() ).findOne( '.help-content-close' ).once( 'click', modal.close );\n" +
    "\t\t\t\t\t\t}\n" +
    "\t\t\t\t\t} ).show();\n" +
    "\t\t\t\t} );\n" +
    "\t\t\t}\n" +
    "\t\t} )();\n" +
    "\t} )();</script> </body> </html>"
  );


  $templateCache.put('scripts/sharedModule/templates/loader.html',
    "<div class=\"pod_loader_img\"> <div class=\"loader\"> sadsadsadsa </div> </div>"
  );


  $templateCache.put('scripts/sharedModule/templates/logoutConfirm.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">Are you sure you want to logout?</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "/* @media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "} */</style>"
  );


  $templateCache.put('scripts/sharedModule/templates/stillprogress.html',
    "<div class=\"modal-body\"> <p class=\"final_step_postjob text-center\">My profile for parent is still under progress</p> </div> <style>.bootbox-body .modal-body, .modal-body{\n" +
    "\tpadding-bottom: 0px;\n" +
    "}\n" +
    ".bootbox  .modal-dialog{\n" +
    "  width:500px;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width: 767px) {\n" +
    "  .bootbox  .modal-dialog{\n" +
    "    width:auto;\n" +
    "  }\n" +
    "}</style>"
  );


  $templateCache.put('scripts/sharedModule/views/app-ads/ad1.html',
    "<div class=\"welcome_ad\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-hidden\"> <p class=\"white_text text-center\">Qualified childcare workers, Babysitters, Nannies, Au-Pairs, Tutors, Educators & Cooks</p> <a href=\"#\" class=\"ad_button orange\">Sign up now</a> <h3 class=\"text-center\"> to earn $ 50 welcome bonus </h3> <div class=\"text-center\"> <img src=\"styles/images/50dollar.jpg\" alt=\"50dollar\" class=\"img-responsive\"> </div> </div> </div> </div>"
  );


  $templateCache.put('scripts/sharedModule/views/app-ads/ad2.html',
    "<div class=\"welcome_ad\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 col-xs-hidden\"> <p class=\"white_text text-center\">If you have any friends, who are looking for part-time or casual work as Qualified childcare worker, Babysitter, Nanny, Au-Pair, Tutor, Educator & Cooks, please refer them to us and </p> <h3 class=\"text-center\"> Earn $ 20 referral bonus </h3> <div class=\"text-center\"> <img src=\"styles/images/20aud.jpg\" alt=\"20dollar\" class=\"img-responsive\"> </div> <a href=\"#\" class=\"ad_button orange\">Refer your friends now! </a> </div> </div> </div>"
  );


  $templateCache.put('scripts/sharedModule/views/app-ads/ad3.html',
    "<div> <h1>Ad 3</h1> </div>"
  );


  $templateCache.put('scripts/sharedModule/views/footer.html',
    "<footer> <!-- <div class=\"cloud-divider \">\n" +
    "\n" +
    "        <svg id=\"deco-clouds\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"100%\" height=\"100\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"none\">\n" +
    "           <path d=\"M-5 100 Q 0 20 5 100 Z\n" +
    "              M0 100 Q 5 0 10 100\n" +
    "              M5 100 Q 10 30 15 100\n" +
    "              M10 100 Q 15 10 20 100\n" +
    "              M15 100 Q 20 30 25 100\n" +
    "              M20 100 Q 25 -10 30 100\n" +
    "              M25 100 Q 30 10 35 100\n" +
    "              M30 100 Q 35 30 40 100\n" +
    "              M35 100 Q 40 10 45 100\n" +
    "              M40 100 Q 45 50 50 100\n" +
    "              M45 100 Q 50 20 55 100\n" +
    "              M50 100 Q 55 40 60 100\n" +
    "              M55 100 Q 60 60 65 100\n" +
    "              M60 100 Q 65 50 70 100\n" +
    "              M65 100 Q 70 20 75 100\n" +
    "              M70 100 Q 75 45 80 100\n" +
    "              M75 100 Q 80 30 85 100\n" +
    "              M80 100 Q 85 20 90 100\n" +
    "              M85 100 Q 90 50 95 100\n" +
    "              M90 100 Q 95 25 100 100\n" +
    "              M95 100 Q 100 15 105 100 Z\">\n" +
    "           </path>\n" +
    "        </svg>\n" +
    "     </div> --> <div class=\"primary_footer\"><!--  cloud svg--> <!--  End cloud svg--> <div class=\"container-fluid\"> <!-- <div class=\"row\">\n" +
    "      <div class=\"col-md-3 col-sm-6\">\n" +
    "        <div class=\"kidsworld_footer_widget\">\n" +
    "          <div class=\"kidsworld_widget_content\">\n" +
    "            <h3><span>About Lovelychildcare</span></h3>\n" +
    "\n" +
    "            <p>Donec earum rerum hic tenetur ans sapiente delectus, ut aut reiciendise voluptat maiores alias consequaturs aut perferendis doloribus asperiores.<span>Readmore..</span></p>\n" +
    "            <div class=\"row\">\n" +
    "              <div class=\"col-md-12 social_footer \">\n" +
    "                <a class=\"btn btn-social-icon btn-bitbucket hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-facebook\"></i></a>\n" +
    "                <a class=\"btn btn-social-icon btn-dropbox hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-twitter\"></i></a>\n" +
    "                <a class=\"btn btn-social-icon btn-flickr hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-google-plus\"></i></a>\n" +
    "                <a class=\"btn btn-social-icon btn-foursquare hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-foursquare\"></i></a>\n" +
    "              </div>\n" +
    "            </div>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "      <div class=\"col-md-3 col-sm-6\">\n" +
    "        <div class=\"kidsworld_footer_widget\">\n" +
    "          <div class=\"kidsworld_widget_content\">\n" +
    "            <h3><span>Latest Post</span></h3>\n" +
    "            <div class=\"media\">\n" +
    "            <div class=\"media-left\">\n" +
    "            <img src=\"styles/images/papa.jpg\" class=\"media-object footer_blog_icon\" >\n" +
    "            </div>\n" +
    "            <div class=\"media-body\">\n" +
    "            <p class=\"media-heading\">Lorem ipsum dolor sit amet...</p>\n" +
    "            <h6 class=\"date_footer\">June 20, 2017</h6>\n" +
    "\n" +
    "            </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"media\">\n" +
    "            <div class=\"media-left\">\n" +
    "            <img src=\"styles/images/papa.jpg\" class=\"media-object footer_blog_icon\" >\n" +
    "            </div>\n" +
    "            <div class=\"media-body\">\n" +
    "            <p class=\"media-heading\">Lorem ipsum dolor sit amet...</p>\n" +
    "            <h6 class=\"date_footer\">June 20, 2017</h6>\n" +
    "\n" +
    "            </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"media\">\n" +
    "            <div class=\"media-left\">\n" +
    "            <img src=\"styles/images/papa.jpg\" class=\"media-object footer_blog_icon\" >\n" +
    "            </div>\n" +
    "            <div class=\"media-body\">\n" +
    "            <p class=\"media-heading\">Lorem ipsum dolor sit amet...</p>\n" +
    "            <h6 class=\"date_footer\">June 20, 2017</h6>\n" +
    "\n" +
    "            </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "      </div>\n" +
    "      <div class=\"col-md-3 col-sm-6\">\n" +
    "        <div class=\"kidsworld_footer_widget\">\n" +
    "          <div class=\"kidsworld_widget_content\">\n" +
    "            <h3><span>Contact</span></h3>\n" +
    "            <ul class=\"footer-contacts color_teel\">\n" +
    "            <li><i class=\"icon-location-pin\"></i> 24th Street, New York, USA</li>\n" +
    "            <li><i class=\"icon-phone\"></i> +1 659 6597 365</li>\n" +
    "            <li><i class=\"fa fa-envelope-open-o\"></i> <a href=\"mailto:lovelychildcare@gmail.com\">lovelychildcare@gmail.com</a></li>\n" +
    "            </ul>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"col-md-3 col-sm-6\">\n" +
    "        <div class=\"kidsworld_footer_widget\">\n" +
    "          <div class=\"kidsworld_widget_content\">\n" +
    "            <h3><span>Quick Links</span></h3>\n" +
    "            <ul class=\"footer-contacts\">\n" +
    "            <li><a href=\"javascript:void(0);\"><i class=\"icon-arrow-right-circle pull-left\"></i>Home </a></li>\n" +
    "            <li><a href=\"javascript:void(0);\"><i class=\"icon-arrow-right-circle pull-left\"></i>Post Job </a></li>\n" +
    "            <li><a href=\"javascript:void(0);\"><i class=\"icon-arrow-right-circle pull-left\"></i>My Jobs </a></li>\n" +
    "            <li><a href=\"javascript:void(0);\"><i class=\"icon-arrow-right-circle pull-left\"></i>How it Works</a> </li>\n" +
    "\n" +
    "            </ul>\n" +
    "\n" +
    "          </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "\n" +
    "    </div> --> <div class=\"row\"> <div class=\"col-md-7 col-sm-7 col-xs-12 widget_footer\"> <h4 class=\"widget_title\">About Lovelychildcare</h4> <p class=\"text-justify\">Lovely Childcare is an online network and marketplace, which connects parents, childcare centres & agencies With qualified childcare professionals, babysitters, nannies, au-pairs, tutors, educators and cooks. Clients seeking carers can sign up and post a job for free. Carers seeking work can sign up and apply for jobs. Clients can interact with the interested workers and select the most suitable worker and confirm booking. </p> <p class=\"text-justify\">Lovely Childcare is 100% Australian owned family business with children of our own. We are based in Melbourne and we have over 10 years of experience in children services. Our service is fast and friendly. We also support various charities.</p> <p class=\"text-justify\"><span class=\"disclamimer_sec\">Disclaimer: </span>Lovely Childcare is not a childcare centre or an agency.</p> </div> <!-- <div class=\"col-md-3 col-sm-6 col-xs-12 widget_footer widget_recent_entries\">\n" +
    "          <h4 class=\"widget_title\">Latest Posts</h4>\n" +
    "          <ul>\n" +
    "\t\t\t\t\t<li>\n" +
    "\t\t\t\t<a href=\"http://smartyschool.stylemixthemes.com/we-are-at-independent-schools-show/\">We are at Independent Schools Show</a>\n" +
    "\t\t\t\t\t\t\t<span class=\"post-date\">December 1, 2015</span>\n" +
    "\t\t\t\t\t\t</li>\n" +
    "\t\t\t\t\t<li>\n" +
    "\t\t\t\t<a href=\"http://smartyschool.stylemixthemes.com/double-olympian-makes-waves-at-our-school/\">Double Olympian makes waves at our school</a>\n" +
    "\t\t\t\t\t\t\t<span class=\"post-date\">December 1, 2015</span>\n" +
    "\t\t\t\t\t\t</li>\n" +
    "\t\t\t\t\t<li>\n" +
    "\t\t\t\t<a href=\"http://smartyschool.stylemixthemes.com/high-school-students-share-shakespeares-stage/\">High School students share Shakespeare’s stage</a>\n" +
    "\t\t\t\t\t\t\t<span class=\"post-date\">December 1, 2015</span>\n" +
    "\t\t\t\t\t\t</li>\n" +
    "\t\t\t\t</ul>\n" +
    "        </div> --> <!-- <div class=\"col-md-3 col-sm-6 col-xs-12 widget_footer\">\n" +
    "          <h4 class=\"widget_title\">Quick Links</h4>\n" +
    "          <ul class=\"footer-contacts\">\n" +
    "\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>Post Job </a></li>\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>My Jobs </a></li>\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>How it Works</a> </li>\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>Privacy Policy</a> </li>\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>FAQ</a> </li>\n" +
    "          <li><a href=\"javascript:void(0);\"><i class=\"fa fa-angle-double-right pull-left\"></i>Terms & Conditions</a> </li>\n" +
    "\n" +
    "          </ul>\n" +
    "\n" +
    "        </div> --> <div class=\"col-md-1 col-sm-1 col-xs-12\"> </div> <div class=\"col-md-4 col-sm-4 col-xs-12 widget_footer\"> <h4 class=\"widget_title\">Get in Touch</h4> <ul class=\"footer-contacts color_teel\"> <li><i class=\"icon-location-pin\"></i> 67 Baltimore Dr Point Cook VIC 3030 </li> <li><i class=\"icon-phone\"></i> 03 9395 2303 </li> <li><i class=\"fa fa-envelope-open-o\"></i> <a href=\"mailto:admin@lovelychildcare.com\">admin@lovelychildcare.com</a></li> </ul> <div class=\"row\"> <div class=\"col-md-12 social_footer hide\"> <a class=\"btn btn-social-icon btn-bitbucket hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-facebook\"></i></a> <a class=\"btn btn-social-icon btn-dropbox hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-twitter\"></i></a> <a class=\"btn btn-social-icon btn-flickr hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-google-plus\"></i></a> <a class=\"btn btn-social-icon btn-foursquare hvr-float-shadow\" href=\"javascript:void(0);\"><i class=\"fa fa-foursquare\"></i></a> </div> </div> </div> </div> </div> </div> <div class=\"container-fluid p-0\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"secondary_footer_copy text-center\"> <div> <img src=\"styles/images/logo.png\" alt=\"\" width=\"150\"> </div> Copyright 2018 © Lovely Childcare Network | All Rights Reserved | <a href=\"#\" class=\"disclamimer_sec\">Terms of Use</a> | <a href=\"#\" class=\"disclamimer_sec\">Privacy Policy</a> </div> </div> <!-- <div class=\"col-xs-6 text-center\">\n" +
    "    </div> --> </div> </div> </footer>"
  );


  $templateCache.put('scripts/sharedModule/views/header.html',
    "<header id=\"scroll_top\"> <div class=\"pre-header hidden-xs\"> <div class=\"container-fluid\"> <div class=\"row\"> <div class=\"col-md-12 col-sm-12 additional-nav\"> <ul class=\"list-unstyled list-inline pull-right\"> <li><i class=\"fa fa-envelope\"></i><span>admin@lovelychildcare.com</span></li> <li><i class=\"fa fa-phone\"></i><span>+1 659 6597 365</span></li> </ul> </div> </div> </div> </div> <!-- ================================================================================================ --> <!-- ========================================Navigation bar============================================--> <!-- ================================================================================================ --> <div class=\"navigation_section\" set-class-when-at-top=\"fixed-top-header\"> <div class=\"navbar navbar-default\"> <div class=\"navbar-header\"> <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\"> <span class=\"icon-bar\"> </span> <span class=\"icon-bar\"> </span> <span class=\"icon-bar\"> </span> </button> <a class=\"navbar-brand\" href=\"index.html\"> <img src=\"styles/images/logo.png\" alt=\"logo\" class=\"img-responsive\"> </a> </div> <div class=\"navbar-collapse collapse\" ng-controller=\"HomeCtrl as hm\"> <ul class=\"nav navbar-nav navbar-right\" ng-if=\"!logged_in\"> <li class=\"\"> <a href=\"\" id=\"postjobLink\" class=\"postjob_link\" ng-click=\"hm.jopPopup()\">Post Job </a> </li> <!-- <li >\n" +
    "                     <a href=\"#/joblist\">My Jobs\n" +
    "                     </a>\n" +
    "                  </li> --> <li> <a href=\"#/howitworks\" ng-class=\"{'active':currentState == 'howitworks'}\">How it Works </a> </li> <li> <a href=\"#/findcarer\" ng-class=\"{'active':currentState == 'findcarer'}\">Find a Carer </a> </li> <li ng-controller=\"AuthCtrl as vm\"> <a href=\"javascript:void(0)\" id=\"loginLink\" ng-click=\"vm.callloginPopup()\">Login </a> </li> <li ng-controller=\"SignupCtrl as sm\"> <a href=\"javascript:void(0)\" ng-click=\"sm.openSignup()\" id=\"signupLink\">Signup </a> </li> </ul> <ul class=\"nav navbar-nav navbar-right\" ng-if=\"logged_in\"> <li ng-if=\"purpose == 'parent'\"> <a href=\"\" class=\"postjob_link\" ng-click=\"hm.jopPopup()\">Post Job </a> </li> <li> <a href=\"#/findcarer\" ng-if=\"purpose != 'worker'\" ng-class=\"{'active':currentState == 'findcarer'}\">Find a Carer </a> </li> <li> <a href=\"{{myjobhref}}\" ng-class=\"{'active':currentState == 'joblist' || currentState == 'parentjoblist' || currentState == 'previewjob'}\">My Jobs </a> </li> <li> <a href=\"#/myprofile\" ng-if=\"purpose == 'worker'\" ng-class=\"{'active':currentState == 'myprofile'}\">My Profile </a> <a href=\"#/parentprofile\" ng-if=\"purpose == 'parent'\" ng-class=\"{'active':currentState == 'parentprofile'}\">My Profile </a> <!-- <a href=\"javascript:void(0)\" ng-if=\"purpose == 'parent'\" ng-class=\"{'active':currentState == 'myprofile'}\" ng-click=\"underProgress()\">My Profile\n" +
    "                     </a> --> </li> <!-- <li>\n" +
    "                     <a href=\"javascript:void(0)\" ng-click=\"logout()\">Logout\n" +
    "                     </a>\n" +
    "                  </li> --> <li class=\"dropdown\"> <a href=\"javascript:void(0);\" class=\"dropdown-toggle user_name_header\" data-toggle=\"dropdown\"> <span class=\"thumb-sm avatar pull-left\" ng-if=\"loginuser_profilepic\"> <img ng-src=\"images/profilepic/{{loginuser_profilepic}}\"> </span> <span class=\"thumb-sm avatar pull-left\" ng-if=\"!loginuser_profilepic\"> <img ngf-thumbnail=\"'/images/no_image_found.png'\"> </span> {{login_user}} <b class=\"caret\"></b> </a> <ul class=\"dropdown-menu customdropdown\"> <span class=\"arrow top\"></span> <!-- <li><a href=\"javascript:void(0);\"><i class=\"icon-user p-r-5\"></i>My Profile</a></li> --> <li><a href=\"javascript:void(0)\" ng-click=\"gotoChangepasswd()\"><i class=\"icon-lock-open p-r-5\"></i>Change Password</a></li> <li ng-if=\"purpose == 'worker'\"><a href=\"#/settings\"><i class=\"icon-wrench p-r-5\"></i>Settings</a></li> <li><a href=\"javascript:void(0);\" ng-click=\"logout()\"><i class=\"icon-power p-r-5\"></i>Logout</a></li> </ul> </li> <!-- <li class=\"dropdown\">\n" +
    "                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\"><i class=\"icon-settings\"></i>\n" +
    "                    <span class=\"caret\"></span></a>\n" +
    "                    <ul class=\"dropdown-menu customdropdown\">\n" +
    "                    <li><a href=\"javascript:void(0);\"><i class=\"icon-user p-r-5\"></i>My Profile</a></li>\n" +
    "                    <li><a href=\"#/changepassword\"><i class=\"icon-lock-open p-r-5\"></i>Change Password</a></li>\n" +
    "                    <li><a href=\"#/settings\"><i class=\"icon-wrench p-r-5\"></i>Settings</a></li>\n" +
    "                    <li><a href=\"javascript:void(0);\" ng-click=\"logout()\"><i class=\"icon-power p-r-5\"></i>Logout</a></li>\n" +
    "                    </ul>\n" +
    "                    </li> --> </ul> </div> </div> </div> <!--=================================== End Navigation Bar ==========================================================--> </header>"
  );


  $templateCache.put('scripts/sharedModule/views/howitwork.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <div class=\"content_section\"> <div class=\"row\"> <div class=\"col-md-12\"> <h1 class=\"text-center heading_howitwork\">How it works for Carer</h1> <img src=\"styles/images/how.png\" alt=\"\" class=\"img-responsive\"> </div> </div> </div> <!-- footer section comes here --> <div footer></div>"
  );


  $templateCache.put('scripts/sharedModule/views/newpassword.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <div class=\"content_section\"> <section class=\"change_password\"> <div class=\"row\"> <div class=\"col-xs-12\"> <h3 class=\"heading\">Change Password</h3> <!--  Change password form--> <div class=\"row\"> <div class=\"col-sm-6 postjob_type_section change_password_form\"> <form name=\"changepwd_form\" ng-submit=\"updateNewpassword(changepwd_form)\" novalidate> <div class=\"m-t-10 m-b-10\"> <div class=\"group\"> <h4 class=\"m-b-0\">New Password</h4> <input type=\"password\" ng-model=\"changePassword.password\" ng-minlength=\"7\" ng-required=\"true\" name=\"password\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"(changepwd_form.$submitted || changepwd_form.password.$dirty) &&changepwd_form.password.$invalid\"> <p ng-if=\"changepwd_form.password.$error.required\">Please enter new password</p> <p ng-if=\"changepwd_form.password.$error.minlength\">Password should contain more than 6 characters</p> </div> </div> </div> <div class=\"m-t-10 m-b-10\"> <div class=\"group\"> <h4 class=\"m-b-0\">Confirm New Password</h4> <input type=\"password\" name=\"confirmpassword\" ng-model=\"changePassword.confirmPassword\" ng-required=\"true\"> <span class=\"highlight\"></span> <span class=\"bar\"></span> <div class=\"errormsg\" ng-if=\"passmatch && !changepwd_form.confirmpassword.$error.required && changePassword.confirmPassword != changePassword.password\"> <p>Confirm password must be same as password</p> </div> <div class=\"errormsg\" ng-if=\"(changepwd_form.$submitted || changepwd_form.confirmpassword.$dirty) && changepwd_form.confirmpassword.$invalid\"> <p ng-if=\"changepwd_form.confirmpassword.$error.required\">Please enter confirm password</p> </div> </div> </div> <div class=\"button_footer pull-right m-t-20\"> <button type=\"button\" class=\"btn btn btn-default cancel_footer_btn hvr-float-shadow\" ng-click=\"resetPassword(changepwd_form)\">Cancel</button> <button type=\"submit\" class=\"btn btn btn-primary save_footer_btn hvr-float-shadow\">Save</button> </div> </form> </div> </div> <!-- <form>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"email\">Email address:</label>\n" +
    "    <input type=\"email\" class=\"form-control\" id=\"email\">\n" +
    "  </div>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"pwd\">Password:</label>\n" +
    "    <input type=\"password\" class=\"form-control\" id=\"pwd\">\n" +
    "  </div>\n" +
    "</form> --> <!--  End of change password form--> </div> </div> </section></div>   <!-- footer section comes here --> <div footer></div>"
  );


  $templateCache.put('scripts/sharedModule/views/settings.html',
    "<div ng-http-loader template=\"scripts/sharedModule/templates/loader.html\"> </div> <div header></div> <!-- main section comes here --> <div class=\"content_section\"> <section class=\"seetings_page change_password\"> <div class=\"row\"> <div class=\"col-xs-12\"> <h3 class=\"heading\">Notification & Settings</h3> </div> </div> </section> </div> <!-- footer section comes here --> <div footer></div>"
  );

}]);
